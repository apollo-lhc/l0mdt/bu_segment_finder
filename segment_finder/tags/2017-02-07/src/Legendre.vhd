-------------------------------------------------------------------------------
-- This module contains the theta slices for the legendre transform and the
-- module for the pipelining of the computation of the legendre transform
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.LEGENDRE_IO.all;
use work.Trig_lookup.all;

entity Legendre is  
  port (
    sc_clk      : in std_logic;
    sc_address  : in std_logic_vector(31 downto 0);
    sc_data_in  : in std_logic_vector(35 downto 0);
    sc_data_out : out std_logic_vector(35 downto 0);
    sc_load     : in std_logic;
    
    clk         : in std_logic;
    reset_sync  : in std_logic;

    --ROI interface
    process_ROI : in std_logic;
    alpha_seed  : in std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1       downto 0);  -- mRadians
    z0_seed     : in std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);  -- mm
    --(extra -1 is for bumping this up to a signed value)
        
    --FIFO interface    
    hit_FIFO_in       : in std_logic_vector((3*SCALED_COORDINATE_WIDTH)-1 downto 0); -- r,y,x (mm)
    hit_FIFO_in_valid : in std_logic;
    hit_FIFO_empty    : in std_logic;
    hit_FIFO_read     : out std_logic;

    --ROI result
    fit_valid    : out std_logic;
    fit_z0       : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
    fit_alpha    : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0));
  
end entity Legendre;

architecture behavioral of Legendre is

  -------------------------------------------------------------------------------
  -- theta slice objects
  -------------------------------------------------------------------------------
  -------------------------------------------------------------------------------
  -- constants
  constant THETA_BIN_COUNT : integer := 2**THETA_BIN_COUNT_WIDTH;
  constant R_BIN_COUNT2     : integer := 2**R_BIN_COUNT_WIDTH;

  component compute_pipeline is
    generic (
      COORDINATE_WIDTH       : integer;
      TRIG_WIDTH             : integer;
      BINNING_BITSHIFT_COUNT : integer;
      BIN_WIDTH              : integer);
    port (
      clk              : in  std_logic;
      data_in_valid    : in  std_logic;
      data_in_last     : in  std_logic;
      tube_x           : in  signed(COORDINATE_WIDTH-1 downto 0);
      cos              : in  signed(TRIG_WIDTH-1 downto 0);
      tube_y           : in  signed(COORDINATE_WIDTH-1 downto 0);
      sin              : in  signed(TRIG_WIDTH-1 downto 0);
      R                : in  signed(COORDINATE_WIDTH- 1 downto 0);
      R_pm             : in  std_logic;
      r0               : in  signed(COORDINATE_WIDTH- 1 downto 0);
      data_out_valid   : out std_logic;
      data_out_last    : out std_logic;
      bin              : out unsigned(BIN_WIDTH-1 downto 0);
      bin_out_of_range : out std_logic);
  end component compute_pipeline;
  
  component legendre_theta_slice is
    generic (
      R_BIN_COUNT_WIDTH : integer;
      COUNT_BIT_WIDTH   : integer);
    port (
      clk           : in  std_logic;
      reset_sync    : in  std_logic;
      incr_rbin     : in  std_logic;
      rbin          : in  unsigned(R_BIN_COUNT_WIDTH - 1 downto 0);
      max_value     : out unsigned(COUNT_BIT_WIDTH   - 1 downto 0);
      max_bin       : out unsigned(R_BIN_COUNT_WIDTH - 1 downto 0));
  end component legendre_theta_slice;

  component TrigTable is
    generic (
      THETA_STARTING_VALUE    : integer; 
      THETA_BIN_COUNT_WIDTH   : integer;         
      BLOCKRAM_BITS_PER_THETA : integer; 
      BITS_USED_PER_SAMPLE    : integer; 
      FUNDAMENTAL_RAM_WIDTH   : integer);
    port (
      load_clk      : in  std_logic;
      load_data_in  : in  std_logic_vector((2*TRIG_WIDTH)-1 downto 0);
      load_data_out : out std_logic_vector((2*TRIG_WIDTH)-1 downto 0);
      load_address  : in  std_logic_vector(THETA_BIN_COUNT_WIDTH -1 downto 0) := (others=> '0');
      load_strobe   : in  std_logic;
      clk           : in  std_logic;
      theta         : in  signed(THETA_BIN_COUNT_WIDTH-1 downto 0);
      cosine        : out trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0);
      sine          : out trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0));
  end component TrigTable;

--  constant TRIG_LINE_SAMPLE_COUNT_BITWIDTH : integer := floor(log2((FUNDAMENTAL_RAM_WIDTH/BITS_USED_PER_SAMPLE)*(BITS_USED_PER_SAMPLE/TRIG_WIDTH)));
--  constant TRIG_LINE_SAMPLE_COUNT          : integer := 2**TRIG_LINE_SAMPLE_COUNT_BITWIDTH;
  
  signal cos : trig_array_t(THETA_BIN_COUNT - 1 downto 0) := (others => (others => '0'));
  signal sin : trig_array_t(THETA_BIN_COUNT - 1 downto 0) := (others => (others => '0'));
  
  -------------------------
  -- ROI BUffers
  signal ROI_alpha_seed  : std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1       downto 0);
  signal ROI_z0_seed     : std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
  
  -------------------------
  -- Compute pipeline
  constant BINNING_BITSHIFT_COUNT : integer := 1;
  type hit_state_t is (HIT_STATE_IDLE,HIT_STATE_NEW_HIT_P,HIT_STATE_NEW_HIT_N);
  signal hit_state : hit_state_t := HIT_STATE_IDLE;
  signal data_in_valid  : std_logic := '0';
  signal data_out_valid : std_logic_vector(THETA_BIN_COUNT -1 downto 0) := (others => '0');
  signal data_in_last : std_logic := '0';
  signal data_out_last : std_logic_vector(THETA_BIN_COUNT -1 downto 0) := (others => '0');
  signal tube_x : signed(SCALED_COORDINATE_WIDTH -1 downto 0) := (others => '0');
  signal tube_y : signed(SCALED_COORDINATE_WIDTH -1 downto 0) := (others => '0');
  signal R      : signed(SCALED_COORDINATE_WIDTH -1 downto 0) := (others => '0');
  signal R_pm   : std_logic := '0';
  signal r0            : signed(SCALED_COORDINATE_WIDTH + TRIG_WIDTH -1 downto 0) := (others => '0');
  signal r0_lowest_bin : signed(SCALED_COORDINATE_WIDTH -1 downto 0) := (others => '0');

  type rbin_array_t is array (integer range <>) of unsigned(R_BIN_COUNT_WIDTH - 1 downto 0);
  signal rbin   : rbin_array_t(THETA_BIN_COUNT - 1 downto 0) := (others => (others => '0'));
  signal bin_out_of_range : std_logic_vector(THETA_BIN_COUNT -1 downto 0) := (others => '0');

  signal incr_bin : std_logic_vector(THETA_BIN_COUNT - 1 downto 0) := (others => '0');
  
  -------------------------
  -- Legendre outputs
  type legendre_count_array_t is array (integer range <>) of unsigned(COUNT_BIT_WIDTH - 1 downto 0);
  signal max_value : legendre_count_array_t(THETA_BIN_COUNT -1 downto 0) := (others => (others => '0'));
  signal max_r_bin : rbin_array_t(THETA_BIN_COUNT -1 downto 0) := (others => (others => '0'));

  -------------------------
  -- Theta signals
  type signed_array_t is array (integer range <>) of signed(TRIG_TABLE_BIN_COUNT_WIDTH -1 downto 0);
  type unsigned_array_t is array (integer range <>) of unsigned(THETA_BIN_COUNT_WIDTH -1 downto 0);
  
  signal theta_fill_value : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  signal theta_fill_value_delay : signed_array_t(1 downto 0) := (others => (others => '0'));
  
  signal theta_fill_bin_value : unsigned(THETA_BIN_COUNT_WIDTH - 1 downto 0);
  signal theta_fill_bin_value_delay : unsigned_array_t(1 downto 0) := (others => (others => '0'));
  
  signal theta_lower_bound_inclusive : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');  
  signal theta_upper_bound_exclusive : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  
  signal cosine_lookup : trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0) := (others => (others => '0'));
  signal sine_lookup : trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0) := (others => (others => '0'));

  -------------------------
  -- Max search signals
  signal legendre_level : integer := 0;
  signal max_value_compare : legendre_count_array_t(THETA_BIN_COUNT/2 -1 downto 0) := (others => (others => '0'));
  signal max_r_bin_compare : rbin_array_t(THETA_BIN_COUNT/2 -1 downto 0) := (others => (others => '0'));
  signal max_theta_bin_compare : rbin_array_t(THETA_BIN_COUNT/2 -1 downto 0) := (others => (others => '0'));

 
  
  -------------------------
  -- Result computations
  signal computed_alpha : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  signal computed_z0 : signed(SCALED_COORDINATE_WIDTH - 1 downto 0) := (others => '0');
  signal computed_fit_valid : std_logic := '0';

  

  -------------------------
  -- other
  signal hit_FIFO_read_local : std_logic := '0';
  signal reset_legendre_matrix : std_logic := '0';
  signal error_pulse_roi_not_finished : std_logic := '0';
  signal process_ROI_old : std_logic := '0';
  
  -------------------------
  -- State machine
  type ROI_state_t is (ROI_STATE_IDLE,
                       ROI_STATE_NEW_ROI,
                       ROI_STATE_SETUP_ROI,
                       ROI_STATE_BUILD_TRIG_TABLES_0,
                       ROI_STATE_BUILD_TRIG_TABLES_1,
                       ROI_STATE_BUILD_TRIG_TABLES_OTHERS,
                       ROI_STATE_PROCESS_HITS,
                       ROI_STATE_FIND_LEGENDRE_MAX,
                       ROI_STATE_COMPUTE_Z0_AND_ALPHA_1,
                       ROI_STATE_COMPUTE_Z0_AND_ALPHA_2);
  signal ROI_state : ROI_state_t := ROI_STATE_IDLE;

begin  -- architecture behavioral


  -------------------------------------------------------------------------------
  -- Construct our processor
  theta_slice_generator: for iTheta in THETA_BIN_COUNT-1 downto 0 generate
    -----------------------------------------------------------------------------
    --Generate the DSP pipeline for this theta slice
    compute_pipeline_1: entity work.compute_pipeline
      generic map (
        COORDINATE_WIDTH       => SCALED_COORDINATE_WIDTH,
        TRIG_WIDTH             => TRIG_WIDTH,
        BINNING_BITSHIFT_COUNT => BINNING_BITSHIFT_COUNT, 
        BIN_WIDTH              => R_BIN_COUNT_WIDTH)
      port map (
        clk              => clk,
        data_in_valid    => data_in_valid,
        data_in_last     => data_in_last,
        tube_x           => tube_x,
        cos              => cos(iTheta),
        tube_y           => tube_y,
        sin              => sin(iTheta),
        R                => R,
        R_pm             => R_pm,
        r0               => r0_lowest_bin,
        data_out_valid   => data_out_valid(iTheta),
        data_out_last    => data_out_last(iTheta),
        bin              => rbin(iTheta),
        bin_out_of_range => bin_out_of_range(iTheta));
    
    -----------------------------------------------------------------------------
    --Generate the rbins for this theta slice
    incr_bin(iTheta) <= data_out_valid(iTheta) and (not bin_out_of_range(iTheta));
    legendre_theta_slice_1: entity work.legendre_theta_slice
      generic map (
        R_BIN_COUNT_WIDTH => R_BIN_COUNT_WIDTH,
        COUNT_BIT_WIDTH   => COUNT_BIT_WIDTH)
      port map (
        clk           => clk,
        reset_sync    => reset_legendre_matrix,
        incr_rbin     => incr_bin(iTheta),
        rbin          => rbin(iTheta),
        max_value     => max_value(iTheta),
        max_bin       => max_r_bin(iTheta));
  end generate theta_slice_generator;


  -------------------------------------------------------------------------------
  -- The trig table to use to look up values to use in processing
  TrigTable_1: entity work.TrigTable
--    generic map (
--    THETA_STARTING_VALUE    => TRIG_TABLE_THETA_START, 
--    THETA_BIN_COUNT_WIDTH   => TRIG_TABLE_BIN_COUNT_WIDTH)
    port map (
      load_clk      => sc_clk,
      load_data_in  => sc_data_in((2*TRIG_WIDTH)-1 downto 0),
      load_data_out => sc_data_out((2*TRIG_WIDTH)-1 downto 0),
      load_address  => sc_address(TRIG_TABLE_BIN_COUNT_WIDTH-1 downto 0),
      load_strobe   => sc_load,
      clk           => clk,
      theta         => theta_fill_value,
      cosine        => cosine_lookup,
      sine          => sine_lookup);


  

  -------------------------------------------------------------------------------
  -- Operate state machine that controls all processing of incomming hits

  fit_z0    <= std_logic_vector(computed_z0);
  fit_alpha <= std_logic_vector(computed_alpha);
  fit_valid <= computed_fit_valid;
  hit_FIFO_read <= hit_FIFO_read_local;
  
  process_ROI_control: process (clk) is
  begin  -- process process_ROI_control
    if clk'event and clk = '1' then  -- rising clock edge
      process_ROI_old <= process_ROI;
      
      -- reset inactive pulses
      reset_legendre_matrix <= '0';
      error_pulse_ROI_NOT_FINISHED <= '0';

      -- ROI processing state machine
      if process_ROI = '1' and process_ROI_old = '0' then
        --NEW ROI, start processing
        ROI_state <= ROI_STATE_NEW_ROI;
        if ROI_STATE /= ROI_STATE_IDLE then
          error_pulse_ROI_NOT_FINISHED <= '1';
        end if;
      else
        case ROI_state is
          when ROI_STATE_IDLE =>
            -- do nothing
          when ROI_STATE_NEW_ROI =>
            -- capture the ROI parameters
            -- look-up legendre bounds
--            ROI_z0_seed <= z0_seed & COORDINATE_SCALING_BITS; --Multiplying by
            ROI_z0_seed <= z0_seed; --Multiplying by
                                                              --COORDINATE_SCALE_FACTOR
                                                              --(8)
            ROI_alpha_seed <= alpha_seed;
            ROI_state <= ROI_STATE_SETUP_ROI;            
          when ROI_STATE_SETUP_ROI =>
            -- reset the legendre histogram           
            reset_legendre_matrix <= '1';

            --Invalidate the old output
            computed_fit_valid <= '0';
            
            -- start filling in the trig tables
            ROI_state <= ROI_STATE_BUILD_TRIG_TABLES_0;
            
          when ROI_STATE_BUILD_TRIG_TABLES_0 =>
            --Compute r0
            r0 <= signed(ROI_z0_seed) * sine_lookup(to_integer(unsigned(ROI_alpha_seed(BLOCKRAM_BITS_PER_THETA -1 downto 0))));
            ROI_state <= ROI_STATE_BUILD_TRIG_TABLES_1;

          when ROI_STATE_BUILD_TRIG_TABLES_1 =>
            --Compute r0
            -- Compute the value of the lowest r0 bin for computation
            r0 <= r0 - x"40000";                -- 128 * 2048 for shift by 128
                                                -- times the theta scale factor
                                                -- scaling 
            ROI_state <= ROI_STATE_BUILD_TRIG_TABLES_OTHERS;
            
          when ROI_STATE_BUILD_TRIG_TABLES_OTHERS =>
            -- begin reading trig tables
            if (theta_fill_value_delay(1) + to_signed(TRIG_LOOKUP_LINE_COUNT,TRIG_TABLE_BIN_COUNT_WIDTH)) > theta_upper_bound_exclusive then  
              --We are done filling the tables, so move on.
              ROI_state <= ROI_STATE_PROCESS_HITS;

            end if;
          when ROI_STATE_PROCESS_HITS =>
            r0_lowest_bin <= r0(TRIG_WIDTH-1 + SCALED_COORDINATE_WIDTH-1 downto TRIG_WIDTH-1);

            -- enable the hit processor until all hits have hit the compute pipeline.
            if process_ROI = '0' and hit_FIFO_empty = '1' and hit_state = HIT_STATE_NEW_HIT_N then
              ROI_state <= ROI_STATE_FIND_LEGENDRE_MAX;
            end if;
          when ROI_STATE_FIND_LEGENDRE_MAX =>
            --Wait in this state until all computations have finished and then
            --do the comparison tree to locate the maximum.
            --!!!!!TODO!!!!!  We probaly need one more theta position to allow
            --for no maximum (0 hits) and also a check to make sure the maximum value
            --is greater than N where N is the minimum number of hit tubes. 
            if legendre_level = 1 then
              ROI_state <= ROI_STATE_COMPUTE_Z0_AND_ALPHA_1;
            end if;
            computed_alpha <=  theta_lower_bound_inclusive;
          when ROI_STATE_COMPUTE_Z0_AND_ALPHA_1 =>
            --DO the first parts of computing Z0 and alpha

            --compute the full theta from the thetabin and the theta lower bound
--            computed_alpha <= signed("00000"&std_logic_vector(max_theta_bin_compare(0))) + theta_lower_bound_inclusive;
            
            computed_alpha <= computed_alpha + signed(std_logic_vector(('0'&max_theta_bin_compare(0))));

            --Compute the full r from the r0_lowest_bin and the max r bin (must be
            --bitshifted by BINNING_BITSHIFT_COUNT)
--            computed_z0    <= signed(std_logic_vector(max_r_bin_compare(0))&'0') + r0_lowest_bin(SCALED_COORDINATE_WIDTH -1 downto COORDINATE_SCALE_FACTOR_BITS);
            computed_z0    <= signed(std_logic_vector(max_r_bin_compare(0))&'0') + r0_lowest_bin(SCALED_COORDINATE_WIDTH -1 downto 0);
            
            ROI_state <= ROI_STATE_COMPUTE_Z0_AND_ALPHA_2;

            if cos(to_integer(max_theta_bin_compare(0))) = 0 then
              computed_fit_valid <= '0';
              ROI_state <= ROI_STATE_IDLE;
            end if;
            
          when ROI_STATE_COMPUTE_Z0_AND_ALPHA_2 =>
            computed_fit_valid <= '1';
            
            --Convert theta to alpha by adding Pi/2
            computed_alpha <= computed_alpha + PI_OVER_TWO_MRAD;
            --Get z0 by dividing r0 by cos(theta_fit)
            computed_z0 <= computed_z0/cos(to_integer(max_theta_bin_compare(0)));                              
            ROI_state <= ROI_STATE_IDLE;
            
          when others => ROI_state <= ROI_STATE_IDLE;
        end case;
      end if;

    end if;
  end process process_ROI_control;


  -------------------------------------------------------------------------------
  -- Operate state machine that controls the processing and pipelining of the
  -- incomming hits.
  -- This only runs when the master ROI_state is in ROI_STATE_PROCESS_HITS
  process_hit: process (clk) is
  begin  -- process process_hit
    if clk'event and clk = '1' then  -- rising clock edge
      -- turn off pulses
      hit_FIFO_read_local <= '0';
      data_in_valid <= '0';
      
      if ROI_state = ROI_STATE_PROCESS_HITS then
        case hit_state is
          when HIT_STATE_IDLE =>
            -- Wait for a new hit to process and then read from the FIFO
            if hit_FIFO_empty = '0' then
              hit_FIFO_read_local <= '1';
              hit_state <= HIT_STATE_NEW_HIT_P;
            end if;
          when HIT_STATE_NEW_HIT_P =>
            --Wait for data valid from FIFO and process the +R hit
            if hit_FIFO_in_valid = '1' then
--              tube_x <= (others => '0');
--              tube_x(SCALED_COORDINATE_WIDTH -1 downto COORDINATE_SCALE_FACTOR_BITS) <= signed(hit_FIFO_in((1 * COORDINATE_WIDTH) - 1 downto (0 * COORDINATE_WIDTH)));
--              tube_y <= (others => '0');
--              tube_y(SCALED_COORDINATE_WIDTH -1 downto COORDINATE_SCALE_FACTOR_BITS) <= signed(hit_FIFO_in((2 * COORDINATE_WIDTH) - 1 downto (1 * COORDINATE_WIDTH)));
--              R <= (others => '0');
--              R(SCALED_COORDINATE_WIDTH -1 downto COORDINATE_SCALE_FACTOR_BITS) <= signed(hit_FIFO_in((3 * COORDINATE_WIDTH) - 1 downto (2 * COORDINATE_WIDTH)));

              tube_x <= signed(hit_FIFO_in((1 * SCALED_COORDINATE_WIDTH) - 1 downto (0 * SCALED_COORDINATE_WIDTH)));
              tube_y <= signed(hit_FIFO_in((2 * SCALED_COORDINATE_WIDTH) - 1 downto (1 * SCALED_COORDINATE_WIDTH)));
              R      <= signed(hit_FIFO_in((3 * SCALED_COORDINATE_WIDTH) - 1 downto (2 * SCALED_COORDINATE_WIDTH)));
              
              R_pm   <= '0'; -- do the +R case
              data_in_valid <= '1';
              data_in_last <= '0';
              
              --state machine
              hit_state <= HIT_STATE_NEW_HIT_N;
              --FIFO pipelining
              if hit_FIFO_empty = '0' then
                --Start reading the next sample if we can
                hit_FIFO_read_local <= '1';                
              end if;                           
            end if;
          when HIT_STATE_NEW_HIT_N =>
            -- Process the -R hit
            R_pm   <= '1'; -- do the-+R case
            data_in_valid <= '1';            
            data_in_last <= '0';
            if process_ROI = '0' and hit_FIFO_empty = '1' then
              data_in_last <= '1';
            end if;

            
            --state machine
            if hit_FIFO_read_local = '1' then
              -- the last state alread started the next FIFO read, so go to the
              -- HIT_STATE_NEW_HIT_P state to process it. (probably ready next
              -- clock, yay!)
              hit_state <= HIT_STATE_NEW_HIT_P;
            elsif hit_FIFO_empty = '0' then
              -- the FIFO has data in it, and the previous state hasn't started
              -- to read it, so we should.
              -- Do the read and move back to the HIT_STATE_NEW_HIT_P to
              -- process it.  Should only be one tick of idle in HIT_P. :-/
              hit_FIFO_read_local <= '1';              
              hit_state <= HIT_STATE_NEW_HIT_P;
            else
              -- The fifo is empty and we should move to the idle state to wait
              -- for a new hit
              hit_state <= HIT_STATE_IDLE;
            end if;
          when others => null;
        end case;
      end if;
    end if;
  end process process_hit;

  -------------------------------------------------------------------------------
  -- Process that waits for all processing to be done and then finds the
  -- legendre max
  find_legendre_max: process (clk) is
  begin  -- process LEGENDRE_MAX_FIND
    if clk'event and clk = '1' then  -- rising clock edge
      if ROI_STATE = ROI_STATE_NEW_ROI then
        --reset this value for the ROI_STATE_FIND_LEGENRE_MAX state
        legendre_level <= THETA_BIN_COUNT_WIDTH-1;
      elsif ROI_STATE = ROI_STATE_FIND_LEGENDRE_MAX then
        --Wait for all comptuation to be done.
        if and_reduce(data_out_last) = '1' then          


          --Do the comparisons, which the number of halves every time, until we
          --get to legendre_level being 1, which causes the master state
          --machine to move to the next state.
          --At that point the maximum bin is in max_bin_compare(0)
          legendre_level <= legendre_level -1;
          if legendre_level = THETA_BIN_COUNT_WIDTH-1 then
--            for iCompare in 2**(legendre_level) -1 downto 0 loop
            for iCompare in (2**(THETA_BIN_COUNT_WIDTH-1)) -1 downto 0 loop
              if max_value(2*iCompare+1) > max_value(2*iCompare) then
                max_value_compare(iCompare) <= max_value(2*iCompare+1);
                max_r_bin_compare(iCompare) <= max_r_bin(2*iCompare+1);
                max_theta_bin_compare(iCompare) <= to_unsigned(2*iCompare+1,R_BIN_COUNT_WIDTH);
              else
                max_value_compare(iCompare)     <= max_value(2*iCompare);
                max_r_bin_compare(iCompare)     <= max_r_bin(2*iCompare);
                max_theta_bin_compare(iCompare) <= to_unsigned(2*iCompare,R_BIN_COUNT_WIDTH);                
              end if;
            end loop;  -- iCompare 
          else
--            for iCompare in 2**(legendre_level) -1 downto 0 loop
            for iCompare in (2**(THETA_BIN_COUNT_WIDTH-2)) -1 downto 0 loop
              if max_value_compare(2*iCompare+1) > max_value_compare(2*iCompare) then
                max_value_compare(iCompare)     <= max_value_compare(2*iCompare+1);
                max_r_bin_compare(iCompare)     <= max_r_bin_compare(2*iCompare+1);
                max_theta_bin_compare(iCompare) <= max_theta_bin_compare(2*iCompare+1);
              else
                max_value_compare(iCompare)     <= max_value_compare(2*iCompare);
                max_r_bin_compare(iCompare)     <= max_r_bin_compare(2*iCompare);
                max_theta_bin_compare(iCompare) <= max_theta_bin_compare(2*iCompare);                
              end if;
            end loop;  -- iCompare
          end if;          
        end if;
      end if;
    end if;
  end process find_legendre_max;
  

  -------------------------------------------------------------------------------
  -- Process that fills the cosine and sine tables for the Legendre transform
  -- This only operates when the master roi_state is in ROT_STATE_BUILD_TRIG_TABLES
  -- The master is notified that this is done when theta_fill_value is greater
  -- than theta_upper_bound_exclusive
  build_trig_tables: process (clk) is
  begin  -- process build_trig_tables
    if clk'event and clk = '1' then  -- rising clock edge

      -- Delay the theta look-up values since the results are delayed
      theta_fill_value_delay(1) <= theta_fill_value_delay(0);
      theta_fill_value_delay(0) <= theta_fill_value;
      theta_fill_bin_value_delay(1) <= theta_fill_bin_value_delay(0);
      theta_fill_bin_value_delay(0) <= theta_fill_bin_value;
      
      case ROI_STATE is
        when ROI_STATE_NEW_ROI =>
          ----------------------------------------------------------------------
          --Capture the trig values of the alpha angle
          theta_fill_value <= signed(alpha_seed);
          
        when ROI_STATE_SETUP_ROI =>
          ----------------------------------------------------------------------
          -- Begin pipeline of theta lookups

          --setup the processing of the trig tables (valid two clocks later)
          theta_fill_value     <= signed(ROI_alpha_seed) + to_signed(-1570,TRIG_TABLE_BIN_COUNT_WIDTH) - to_signed(THETA_BIN_COUNT/2,TRIG_TABLE_BIN_COUNT_WIDTH);
          theta_fill_bin_value <= (others => '0');
          
          -- compute the lower theta bount from alpha: theta = alpha - 90 - THETA_BIN_COUNT/2
          theta_lower_bound_inclusive <= signed(ROI_alpha_seed) + to_signed(-1570,TRIG_TABLE_BIN_COUNT_WIDTH) + to_signed(-THETA_BIN_COUNT/2,TRIG_TABLE_BIN_COUNT_WIDTH);
          -- alpha - 90 + THETA_BIN_COUNT/2
          theta_upper_bound_exclusive <= signed(ROI_alpha_seed) + to_signed(-1570,TRIG_TABLE_BIN_COUNT_WIDTH) + to_signed( THETA_BIN_COUNT/2,TRIG_TABLE_BIN_COUNT_WIDTH);
          
        when ROI_STATE_BUILD_TRIG_TABLES_0 =>
          ----------------------------------------------------------------------
          -- Continue pipelining theta lookups.
          -- The theta values are valid starting on the next clock.

          -- move the fill bin forward.
          -- Since we just started, theta_fill_bin_value is zero and we just
          -- need to add on how many words from the last
          -- 2**BLOCKRAMBITS_PER_THETA line we actuall used.
          theta_fill_bin_value <= to_unsigned(2**BLOCKRAM_BITS_PER_THETA,THETA_BIN_COUNT_WIDTH) - unsigned((theta_fill_value(BLOCKRAM_BITS_PER_THETA-1 downto 0)));
          --Move to the next theta lookup value. Since we are reading lines
          --that are 2**BLOCKRAM_BITS_PER_THETA long, we just incements the
          --bits above those by one and zero the lower bits to get the correct
          --lookup.
          theta_fill_value <= theta_fill_value + to_signed(2**BLOCKRAM_BITS_PER_THETA,THETA_BIN_COUNT_WIDTH);
          theta_fill_value(BLOCKRAM_BITS_PER_THETA-1  downto 0) <= (others => '0');
          
        when ROI_STATE_BUILD_TRIG_TABLES_1 =>
          ----------------------------------------------------------------------
          -- Continue pipelining theta lookups and handle the first lookup
          -- results separately since they are special
          -- move to the next round number
          theta_fill_bin_value <= theta_fill_bin_value + to_unsigned(2**BLOCKRAM_BITS_PER_THETA,THETA_BIN_COUNT_WIDTH);
          theta_fill_value     <= theta_fill_value     + to_signed(2**BLOCKRAM_BITS_PER_THETA,TRIG_TABLE_BIN_COUNT_WIDTH);
          
          -- loop over the trig_line index from the last sample downto the
          -- first useful sample (iValue)
          for iValue in TRIG_LOOKUP_LINE_COUNT -1 downto 0 loop
            --Look over the values in the lookup line that we need
            if iValue < to_integer(theta_fill_bin_value) then
              cos(iValue) <= cosine_lookup( to_integer(unsigned(std_logic_vector(theta_lower_bound_inclusive(BLOCKRAM_BITS_PER_THETA-1 downto 0)))) + iValue);
              sin(iValue) <=   sine_lookup( to_integer(unsigned(std_logic_vector(theta_lower_bound_inclusive(BLOCKRAM_BITS_PER_THETA-1 downto 0)))) + iValue);
            end if;
          end loop;  -- iValue          
          
        when ROI_STATE_BUILD_TRIG_TABLES_OTHERS =>
          ----------------------------------------------------------------------
          -- Continue pipelining theta lookups and start copying the now valid
          -- lookup outputs
          

                    
          --Process the results (that have been delayed)
          if (theta_fill_value_delay(0) + TRIG_LOOKUP_LINE_COUNT) <= theta_upper_bound_exclusive then                      
            -- move to the next round number
            theta_fill_bin_value <= theta_fill_bin_value + to_unsigned(2**BLOCKRAM_BITS_PER_THETA,THETA_BIN_COUNT_WIDTH);
            theta_fill_value     <= theta_fill_value     + to_signed(2**BLOCKRAM_BITS_PER_THETA,TRIG_TABLE_BIN_COUNT_WIDTH);


            -- loop over the trig_line index from the last sample downto the
            -- first useful sample (iValue)
            for iValue in TRIG_LOOKUP_LINE_COUNT -1 downto 0 loop
              --cosine line slice is the trig bits for this sample, so
              --  iValue*TRIG_WIDTH +/- stuff to get the TRIG_WIDTH bits
              --cosine index is in 0 to THETA_BIN_COUNT -1 space
              --  it will containg theta_fill_bin + (TRIG_LOOKUP_LINE_COUNT-1) - iValue
              cos(to_integer(theta_fill_bin_value_delay(0)) + iValue ) <= cosine_lookup(iValue);
              --same logic for sine
              sin(to_integer(theta_fill_bin_value_delay(0)) + iValue ) <=   sine_lookup(iValue);
            end loop;  -- iValue
          elsif (theta_fill_value_delay(0) ) <= theta_upper_bound_exclusive then                      
            -- If there are any more bins to fill, do this.
            --Loop over the trig_line index from the last useful sample downto 0
            for iValue in ((TRIG_LOOKUP_LINE_COUNT) -1) downto 0 loop
              if iValue < (THETA_BIN_COUNT - theta_fill_bin_value_delay(0)) then
                
                cos(to_integer(theta_fill_bin_value_delay(0) + to_unsigned(iValue,7))) <= cosine_lookup(iValue);
                sin(to_integer(theta_fill_bin_value_delay(0) + to_unsigned(iValue,7))) <=   sine_lookup(iValue);
                
              end if;
            end loop;  -- iValue
          end if;

        --- when lookup for finalization value
        when others => null;
      end case;
    end if;
  end process build_trig_tables;
  
  
end architecture behavioral;
