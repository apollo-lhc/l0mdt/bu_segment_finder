-------------------------------------------------------------------------------
-- This is a container for the rbin counters for a specific theta value.
-- It receives a pipeline of r values and incrments the appropriate bin,
-- keeping track of the maximum bin.
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

entity legendre_theta_slice is
  
  generic (
    R_BIN_COUNT_WIDTH : integer := 7;
    COUNT_BIT_WIDTH   : integer := 4);  
  port (
    clk            : in std_logic;
    reset_sync     : in std_logic;
    incr_rbin      : in std_logic;
    rbin           : in unsigned(R_BIN_COUNT_WIDTH - 1 downto 0);
    max_value      : out unsigned(COUNT_BIT_WIDTH - 1   downto 0);
    max_bin        : out unsigned(R_BIN_COUNT_WIDTH - 1 downto 0));

end entity legendre_theta_slice;

architecture behavioral of legendre_theta_slice is
  --Counter for each bin of this theta slice 
  component bin_counter is
    generic (
      DATA_WIDTH : integer);
    port (
      clk         : in  std_logic;
      reset_sync  : in  std_logic;
      increment   : in  std_logic;
      count       : out unsigned(DATA_WIDTH-1 downto 0));
  end component bin_counter;

  -------------------------------------------------------------------------------
  -- bin control signals
  -------------------------------------------------------------------------------
  constant R_BIN_COUNT : integer := 2**R_BIN_COUNT_WIDTH;
  --Accessor to the bin contents
  type count_array_t is array (R_BIN_COUNT-1 downto 0) of unsigned(COUNT_BIT_WIDTH-1 downto 0);
  signal bin_content : count_array_t := (others => (others =>'0'));

  --mask for incrimenting a bin
  signal r_increment : std_logic_vector(R_BIN_COUNT -1 downto 0) := (others => '0');

  -- reset counters to zero
  signal reset_theta_slice : std_logic := '0';

  constant MAX_COUNT_VALUE : unsigned(COUNT_BIT_WIDTH -1 downto 0) := (others => '1');
 
  -------------------------------------------------------------------------------
  -- Count processing signals
  -------------------------------------------------------------------------------
  signal max_value_buffer : unsigned(COUNT_BIT_WIDTH   - 1 downto 0) := (others => '0');
  signal max_value_local  : unsigned(COUNT_BIT_WIDTH   - 1 downto 0) := (others => '0');
  signal max_bin_buffer   : unsigned(R_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  signal max_bin_local    : unsigned(R_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');

  -------------------------------------------------------------------------------
  -- statistics signals
  -------------------------------------------------------------------------------
  signal ERROR_PULSE_at_max_value : std_logic := '0';
  
begin  -- architecture behavioral


  
  --Array of counters to act like bins
  r_bin_counters: for iBin in R_BIN_COUNT -1 downto 0 generate
    bin_counter_1: entity work.bin_counter
      generic map (
        DATA_WIDTH => COUNT_BIT_WIDTH)
      port map (
        clk         => clk,
        reset_sync  => reset_theta_slice,
        increment   => r_increment(iBin),
        count       => bin_content(iBin));
  end generate r_bin_counters;



  --Output the max value and max_bin when in the done state
  max_value <= max_value_buffer;
  max_bin   <= max_bin_buffer;
  
  fill_process: process (clk) is
  begin  -- process fill_process
    if clk'event and clk = '1' then  -- rising clock edge
      --Reset pulses from this process
      reset_theta_slice <= '0';
      ERROR_PULSE_at_max_value <= '0';

      --reset the count increment mask
      r_increment <= (others => '0');

      --output the current max values
      max_value_buffer <= max_value_local;
      max_bin_buffer <= max_bin_local;

      if reset_sync = '1' then
        --Reset our bins
        reset_theta_slice <= '1';

        --Reset the max values
        max_value_buffer <= (others => '0');
        max_bin_buffer <= (others => '0');        
      elsif incr_rbin = '1' then

        --Increment the bin (counter handles overflow)
        r_increment(to_integer(rbin)) <= '1';
        
        --If the previous max value was the incremented bin's last value,
        --then we know this bin is now the (new) maximum
        if bin_content(to_integer(rbin)) = max_value_local then
          -- save this bin as the maximum r bin
          max_bin_local <= rbin;
          
          --Save the new maximum value (rbin's + 1), but we should check
          --if we overflow.
          if max_value_local = MAX_COUNT_VALUE then
            --send an error pulse to count the number of times we reach the
            --max bin count, we NEVER should.                  
            ERROR_PULSE_at_max_value <= '1';
          else
            --only incrment the max value if we can
            max_value_local <= max_value_local + 1;                  
          end if;                
        end if;
      end if;
    end if;      
  end process fill_process;

end architecture behavioral;
