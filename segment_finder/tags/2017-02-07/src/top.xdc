create_clock -add -name sys_clk_pin -period 3.125 -waveform {0 1.5625} [get_ports clk]

create_clock -add -name load_clk_pin -period 20 -waveform {0 10} [get_ports sc_clk]
