#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

//Stringify macros from: https://www.guyrutenberg.com/2008/12/20/expanding-macros-into-string-constants-in-c/
#define STR_EXPAND(tok) #tok
#define STR(tok) STR_EXPAND(tok)

#define OUTPUT_BITS 12
//const int SCALING_VALUE = 0x1<<12;
#define INPUT_BIT_COUNT 12
#define BLOCKRAM_BIT_COUNT 4
#define BLOCKRAM_COUNT (0x1<<BLOCKRAM_BIT_COUNT)


//TODO, update this and promote input_bits constant to #define so we can put that in as a constant here
char header[] =								\
  "library IEEE;\n"							\
  "use IEEE.STD_LOGIC_1164.all;\n"					\
  "use IEEE.numeric_std.all;\n"					\
  "package Trig_Lookup is\n"					\
  "  constant TRIG_TABLE_BIN_COUNT_WIDTH : integer := " STR(INPUT_BIT_COUNT) ";\n" \
  "  constant TRIG_TABLE_THETA_START     : integer := -1571;\n" \
  "  constant PI_OVER_TWO_MRAD           : integer := 1571;\n"\
  "  constant TRIG_WIDTH                 : integer := " STR(OUTPUT_BITS) ";\n" \
  "\n"\
  "  constant FUNDAMENTAL_RAM_WIDTH      : integer :=  36;\n"		\
  "  constant BLOCKRAM_BITS_PER_THETA    : integer :=  "STR(BLOCKRAM_BIT_COUNT)";\n" \
  "  constant BLOCKRAM_BITS_MASK         : std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH-1 downto 0) :=  std_logic_vector(to_unsigned((2**BLOCKRAM_BITS_PER_THETA)-1,TRIG_TABLE_BIN_COUNT_WIDTH));\n"		\
  "  constant TRIG_LOOKUP_LINE_COUNT     : integer :=  2**BLOCKRAM_BITS_PER_THETA;\n"		\
  "  constant BITS_USED_PER_SAMPLE       : integer :=  16;\n"		\
  "  type ram_block_t is array (integer range (2**(TRIG_TABLE_BIN_COUNT_WIDTH-BLOCKRAM_BITS_PER_THETA))-1 downto 0) of std_logic_vector(FUNDAMENTAL_RAM_WIDTH -1 downto 0);\n";  
  

int16_t GetCos(double theta){
  const int max_value = 0x1<<(OUTPUT_BITS-1);
  int16_t value = (int16_t)(cos(theta*0.001)*((float)max_value));
  //check if the value is above a signed OUTPUT_BITS bit integer
  if (value > (max_value-1)){
    value = max_value-1;
  } else if (value < -1*max_value){
    value = -1*max_value;
  }
  return value;
}

int16_t GetSin(double theta){
  const int max_value = 0x1<<(OUTPUT_BITS-1);
  int16_t value = (int16_t)(sin(theta*0.001)*((float)max_value));
  //check if the value is above a signed OUTPUT_BITS bit integer
  if (value > (max_value-1)){
    value = max_value-1;
  } else if (value < -1*max_value){
    value = -1*max_value;
  }
  return value;
  return value;
}

int main(){
  int total_angles = 0x1<<INPUT_BIT_COUNT;
  int total_lines = total_angles/BLOCKRAM_COUNT;

  printf("Generating %d blockram tables with %d values for a total of %d angles\n",BLOCKRAM_COUNT,total_lines,total_angles);

  //Allocate memory to store comptued values
  int32_t ** table = (int32_t **) malloc(sizeof(int32_t*)*BLOCKRAM_COUNT);
  if(table == NULL){
    printf("Bad table alloc");
    return 1;
  }
  for(int iTable = 0; iTable < BLOCKRAM_COUNT;iTable++){
    table[iTable] = (int32_t *) malloc(sizeof(int32_t)*total_lines);
  }
  assert(OUTPUT_BITS <= 16);
  //Generate lookup table
  const int16_t theta_0 = -1*(0x1<<(INPUT_BIT_COUNT-1));//-2048;//-1570;
  int16_t theta = theta_0;
  printf("Generating trig values for angles from %d to %d\n",theta_0,theta_0+total_angles);
  const uint32_t output_mask = (0x1 << (OUTPUT_BITS))-1;
  const uint32_t blockram_mask = (0x1 << (BLOCKRAM_BIT_COUNT))-1;
  const uint32_t blockram_address_mask = (0x1 << (INPUT_BIT_COUNT -BLOCKRAM_BIT_COUNT ))-1;
  printf("Ouput mask: 0x%04X\n",output_mask);
  printf("Blockram mask: 0x%04X\n",blockram_mask);
  printf("Blockram address mask: 0x%04X\n",blockram_address_mask);
  for(;theta < (theta_0 + total_angles);theta++){
    //compute trig values for this theta
    uint32_t cos_val = (uint32_t) (GetCos(theta) & output_mask);
    uint32_t sin_val = (uint32_t) (GetSin(theta) & output_mask);
    //FInd where to store these values of sine and cosine
    uint8_t block_ram = (uint8_t) ((theta)&blockram_mask);
    uint8_t block_ram_address = (uint8_t) (((theta) >> BLOCKRAM_BIT_COUNT)&blockram_address_mask);
    table[block_ram][block_ram_address] = cos_val << 16 | sin_val;
    //printf("%d %d\n",theta,sin_val);
  }


  //Write lookup table
  FILE * tableFile = fopen("trig_table_lookup_tables.vhd","w");
  fprintf(tableFile,header);
  for(int iTable = 0; iTable < BLOCKRAM_COUNT; iTable++){
    fprintf(tableFile,  "  constant TRIG_TABLE_%02d : ram_block_t := (",iTable);
    for(int iEntry =  total_lines-1; iEntry >0 ; iEntry--){
      fprintf(tableFile,"                                           x\"0%08X\",\n"   ,table[iTable][iEntry]);
    }
    fprintf(tableFile,  "                                           x\"0%08X\");\n\n",table[iTable][0]);
  }

  fprintf(tableFile,"end package Trig_Lookup;");
  
  return 0;
}
