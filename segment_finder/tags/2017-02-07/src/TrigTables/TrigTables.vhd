-------------------------------------------------------------------------------
-- This module stores and looks up blocks of sine and cosine values in milli-radians
-- The values of the widths of read should be optimized for the fundamental
-- size of the RAM in the FPGA
-- Dan Gastler
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.LEGENDRE_IO.all;
use work.Trig_lookup.all;

entity TrigTable is

  port (
    load_clk      : in  std_logic;
    load_data_in  : in  std_logic_vector((2*TRIG_WIDTH)-1 downto 0);
    load_data_out : out std_logic_vector((2*TRIG_WIDTH)-1 downto 0);
    load_address  : in  std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH -1 downto 0) := (others=> '0');
    load_strobe   : in  std_logic;
    
    clk           : in  std_logic;
    theta         : in  signed(TRIG_TABLE_BIN_COUNT_WIDTH-1 downto 0);
    cosine        : out trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0);
    sine          : out trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0)
    );

end entity TrigTable;

architecture behavioral of TrigTable is
  --stuffing two values per blockram
--  constant BLOCKRAMS_PER_THETA : integer := 2**BLOCKRAM_BITS_PER_THETA;

  constant RAM_ENTRY_COUNT_WIDTH : integer := TRIG_TABLE_BIN_COUNT_WIDTH - BLOCKRAM_BITS_PER_THETA;
  constant RAM_ENTRY_COUNT       : integer := 2**(ram_entry_count_width);

  
  
  
  signal TRIG_RAM_00 : ram_block_t := TRIG_TABLE_00;
  signal TRIG_RAM_01 : ram_block_t := TRIG_TABLE_01;
  signal TRIG_RAM_02 : ram_block_t := TRIG_TABLE_02;
  signal TRIG_RAM_03 : ram_block_t := TRIG_TABLE_03;
  signal TRIG_RAM_04 : ram_block_t := TRIG_TABLE_04;
  signal TRIG_RAM_05 : ram_block_t := TRIG_TABLE_05;
  signal TRIG_RAM_06 : ram_block_t := TRIG_TABLE_06;
  signal TRIG_RAM_07 : ram_block_t := TRIG_TABLE_07;
  signal TRIG_RAM_08 : ram_block_t := TRIG_TABLE_08;
  signal TRIG_RAM_09 : ram_block_t := TRIG_TABLE_09;
  signal TRIG_RAM_10 : ram_block_t := TRIG_TABLE_10;
  signal TRIG_RAM_11 : ram_block_t := TRIG_TABLE_11;
  signal TRIG_RAM_12 : ram_block_t := TRIG_TABLE_12;
  signal TRIG_RAM_13 : ram_block_t := TRIG_TABLE_13;
  signal TRIG_RAM_14 : ram_block_t := TRIG_TABLE_14;
  signal TRIG_RAM_15 : ram_block_t := TRIG_TABLE_15;
  

  signal lookup_theta     : std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - BLOCKRAM_BITS_PER_THETA -1  downto 0);
  signal lookup_theta_int : integer;

  signal load_bin : integer range 2**(TRIG_TABLE_BIN_COUNT_WIDTH -2*BLOCKRAM_BITS_PER_THETA) -1 downto 0;  
  signal load_blockram : integer range 2**(BLOCKRAM_BITS_PER_THETA)-1 downto 0;
  
  attribute ram_style : string;
  attribute ram_style of TRIG_RAM_00 : signal is "block";
  attribute ram_style of TRIG_RAM_01 : signal is "block";
  attribute ram_style of TRIG_RAM_02 : signal is "block";
  attribute ram_style of TRIG_RAM_03 : signal is "block";
  attribute ram_style of TRIG_RAM_04 : signal is "block";
  attribute ram_style of TRIG_RAM_05 : signal is "block";
  attribute ram_style of TRIG_RAM_06 : signal is "block";
  attribute ram_style of TRIG_RAM_07 : signal is "block";
  attribute ram_style of TRIG_RAM_08 : signal is "block";
  attribute ram_style of TRIG_RAM_09 : signal is "block";
  attribute ram_style of TRIG_RAM_10 : signal is "block";
  attribute ram_style of TRIG_RAM_11 : signal is "block";
  attribute ram_style of TRIG_RAM_12 : signal is "block";
  attribute ram_style of TRIG_RAM_13 : signal is "block";
  attribute ram_style of TRIG_RAM_14 : signal is "block";
  attribute ram_style of TRIG_RAM_15 : signal is "block";


  
  
begin  -- architecture behavioral

  lookup_theta <= std_logic_vector(theta(TRIG_TABLE_BIN_COUNT_WIDTH -1 downto BLOCKRAM_BITS_PER_THETA));
  lookup_theta_int <= to_integer(unsigned(lookup_theta));
  
  trig_block_ram: process (clk) is
  begin  -- process trig_block_ram
    if clk'event and clk = '1' then  -- rising clock edge
      cosine(0)  <= signed(TRIG_RAM_00(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(0)  <= signed(TRIG_RAM_00(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(1)  <= signed(TRIG_RAM_01(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(1)  <= signed(TRIG_RAM_01(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(2)  <= signed(TRIG_RAM_02(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(2)  <= signed(TRIG_RAM_02(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(3)  <= signed(TRIG_RAM_03(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(3)  <= signed(TRIG_RAM_03(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(4)  <= signed(TRIG_RAM_04(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(4)  <= signed(TRIG_RAM_04(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(5)  <= signed(TRIG_RAM_05(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(5)  <= signed(TRIG_RAM_05(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(6)  <= signed(TRIG_RAM_06(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(6)  <= signed(TRIG_RAM_06(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(7)  <= signed(TRIG_RAM_07(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(7)  <= signed(TRIG_RAM_07(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(8)  <= signed(TRIG_RAM_08(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(8)  <= signed(TRIG_RAM_08(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(9)  <= signed(TRIG_RAM_09(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(9)  <= signed(TRIG_RAM_09(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(10) <= signed(TRIG_RAM_10(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(10) <= signed(TRIG_RAM_10(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(11) <= signed(TRIG_RAM_11(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(11) <= signed(TRIG_RAM_11(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(12) <= signed(TRIG_RAM_12(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(12) <= signed(TRIG_RAM_12(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(13) <= signed(TRIG_RAM_13(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(13) <= signed(TRIG_RAM_13(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(14) <= signed(TRIG_RAM_14(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(14) <= signed(TRIG_RAM_14(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));
      cosine(15) <= signed(TRIG_RAM_15(lookup_theta_int)(BITS_USED_PER_SAMPLE + TRIG_WIDTH -1 downto BITS_USED_PER_SAMPLE));
        sine(15) <= signed(TRIG_RAM_15(lookup_theta_int)(                       TRIG_WIDTH -1 downto 0));

    end if;
  end process trig_block_ram;
  
  load_bin <= to_integer(unsigned(load_address(TRIG_TABLE_BIN_COUNT_WIDTH-1 downto BLOCKRAM_BITS_PER_THETA)));
  load_blockram <= to_integer(unsigned(load_address(BLOCKRAM_BITS_PER_THETA -1 downto 0)));
  load_interface: process (load_clk) is
  begin  -- process load_interface
    if load_clk'event and load_clk = '1' then  -- rising clock edge
      if load_strobe = '1' then
        case load_blockram is
          when 0 =>  TRIG_RAM_00(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 1 =>  TRIG_RAM_01(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 2 =>  TRIG_RAM_02(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 3 =>  TRIG_RAM_03(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 4 =>  TRIG_RAM_04(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 5 =>  TRIG_RAM_05(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 6 =>  TRIG_RAM_06(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 7 =>  TRIG_RAM_07(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 8 =>  TRIG_RAM_08(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 9 =>  TRIG_RAM_09(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 10 => TRIG_RAM_10(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 11 => TRIG_RAM_11(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 12 => TRIG_RAM_12(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 13 => TRIG_RAM_13(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 14 => TRIG_RAM_14(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);
          when 15 => TRIG_RAM_15(load_bin) <= x"0"&x"0"&load_data_in(23 downto 12)&x"0"&load_data_in(11 downto 0);

          when others => null;
        end case;

      end if;
    end if;
  end process load_interface;

end architecture behavioral;
