set cosFile [open "cosine.txt" "w"]
set sinFile [open "sine.txt" "w"]

#for {set theta 0} {$theta < 4096} {incr theta} {
for {set theta -2048} {$theta < 2048} {incr theta} {

    set lookup [expr [expr $theta &0xFF0 ] >> 4] 
    #cosine
    set cmd "get_value -radix dec {/testbench_top/UUT/TrigTable_1/TRIG_RAM_00($lookup)[27:16]}"
    set val [eval $cmd]
    set str "$theta $lookup $val"
    puts $cosFile $str
    #sine
    set cmd "get_value -radix dec {/testbench_top/UUT/TrigTable_1/TRIG_RAM_00($lookup)[11:0]}"
    set val [eval $cmd]
    set str "$theta $lookup $val"
    puts $sinFile $str
    
}




close $cosFile
close $sinFile
