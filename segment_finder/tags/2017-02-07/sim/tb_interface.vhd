library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Legendre_IO.all;

entity tb_interface is
  
  port (
    clk                : in  std_logic;
    reset_sync         : in  std_logic;
    din                : in std_logic_vector(44 downto 0);
    wr_en              : in std_logic;

    hit_FIFO_read      : in std_logic;
    hit_FIFO_in        : out std_logic_vector((3*SCALED_COORDINATE_WIDTH)-1 downto 0);
    hit_FIFO_in_valid  : out std_logic;
    hit_FIFO_empty     : out std_logic
    );
  
end entity tb_interface;

architecture behavioral of tb_interface is

  component TestBenchFIFO is
    port (
      clk         : IN  STD_LOGIC;
      srst        : IN  STD_LOGIC;
      din         : IN  STD_LOGIC_VECTOR(44 DOWNTO 0);
      wr_en       : IN  STD_LOGIC;
      rd_en       : IN  STD_LOGIC;
      dout        : OUT STD_LOGIC_VECTOR(44 DOWNTO 0);
      full        : OUT STD_LOGIC;
      empty       : OUT STD_LOGIC;
      valid       : OUT STD_LOGIC;
      wr_rst_busy : OUT STD_LOGIC;
      rd_rst_busy : OUT STD_LOGIC);
  end component TestBenchFIFO;

begin  -- architecture behavioral
 
  TestBenchFIFO_1: entity work.TestBenchFIFO
    port map (
      clk         => clk,
      srst        => reset_sync,
      din         => din,
      wr_en       => wr_en,
      rd_en       => hit_FIFO_read,
      dout        => hit_FIFO_in,
      full        => open,
      empty       => hit_FIFO_empty,
      valid       => hit_FIFO_in_valid,
      wr_rst_busy => open,
      rd_rst_busy => open);

end architecture behavioral;
