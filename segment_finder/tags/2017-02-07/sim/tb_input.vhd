library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Legendre_IO.all;
use work.Trig_lookup.all;

entity tb_input is
  
  port (
    clk                : in  std_logic;
    address            : in  unsigned(15 downto 0);
    reset_sync         : out std_logic;
    process_ROI        : out std_logic;
    alpha_seed         : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0);
    z0_seed            : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
    din                : out std_logic_vector(44 downto 0);
    wr_en              : out std_logic
    );
  
end entity tb_input;

architecture behavioral of tb_input is
begin  -- architecture behavioral

  input_generation: process (clk) is
  begin  -- process input_generation
    if clk'event and clk = '1' then  -- rising clock edge
      case address is
        when x"0000" =>
          wr_en <= '0';
          process_ROI <= '0';
          reset_sync <= '1';
        when x"0001" =>
          reset_sync <= '0';
        when x"0002" =>
          process_ROI <= '1';
          alpha_seed  <= x"4e8";
          z0_seed     <= "111"&x"ce0";
        when x"0003" =>
          din(44 downto 30) <= "000"&x"04b";
          din(29 downto 15) <= "000"&x"640";
          din(14 downto  0) <= "111"&x"e98";
          wr_en <= '1';
        when x"0004" =>
          wr_en <= '0';
        when x"0005" =>
          din(44 downto 30) <= "000"&x"005";
          din(29 downto 15) <= "000"&x"640";
          din(14 downto  0) <= "000"&x"258";
          wr_en <= '1';
        when x"0006" =>
          wr_en <= '0';
        when x"0007" =>
          din(44 downto 30) <= "000"&x"034";
          din(29 downto 15) <= "000"&x"4a0";
          din(14 downto  0) <= "111"&x"e98";
          wr_en <= '1';
        when x"0008" =>
          wr_en <= '0';
        when x"0009" =>
          din(44 downto 30) <= "000"&x"013";
          din(29 downto 15) <= "111"&x"ce0";
          din(14 downto  0) <= "111"&x"bc8";
          wr_en <= '1';
        when x"000a" =>
          wr_en <= '0';
        when x"000b" =>
          din(44 downto 30) <= "000"&x"06d";
          din(29 downto 15) <= "111"&x"b41";
          din(14 downto  0) <= "111"&x"bc8";
          wr_en <= '1';
        when x"000c" =>
          wr_en <= '0';
        when x"000d" =>
          din(44 downto 30) <= "000"&x"066";
          din(29 downto 15) <= "000"&x"570";
          din(14 downto  0) <= "111"&x"f10";
          wr_en <= '1';
        when x"000e" =>
          wr_en <= '0';
        when x"000f" =>
          din(44 downto 30) <= "000"&x"044";
          din(29 downto 15) <= "111"&x"c11";
          din(14 downto  0) <= "111"&x"b50";
          wr_en <= '1';
        when x"0010" =>
          wr_en <= '0';
        when x"0011" =>
          process_ROI <= '0';
        when others => null;
      end case;
    end if;
  end process input_generation;
  
end architecture behavioral;
