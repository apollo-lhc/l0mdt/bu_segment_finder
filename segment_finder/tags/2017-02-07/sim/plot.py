import numpy as np
import matplotlib.pyplot as plt

matrix = np.zeros((128, 128))
for line in open("matrix.txt"):
    line = line.split()
    if len(line) == 3:
        x = int(line[0])
        y = int(line[1])
        z = int(line[2])
        matrix[y][x] = z

#plt.imshow(matrix,interpolation='none')
plt.imshow(matrix,interpolation='none',origin='lower')
plt.show()    
