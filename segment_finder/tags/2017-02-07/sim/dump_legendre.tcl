set matrixFile [open "matrix.txt" "w"]

for {set theta 0} {$theta < 128} {incr theta} {
    for {set r 0} {$r < 128} {incr r} {
	set cmd "get_value -radix unsigned {/testbench_top/UUT/\\theta_slice_generator($theta)\\/legendre_theta_slice_1/\\r_bin_counters($r)\\/bin_counter_1/local_count}"
	set val [eval $cmd]
	set str "$theta $r $val"
	puts $matrixFile $str
    }
}




#puts $matrixFile $data
close $matrixFile
