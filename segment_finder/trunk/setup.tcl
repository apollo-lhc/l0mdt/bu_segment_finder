#

create_project -force -part xcku115-flva1517-3-e top ./proj

read_xdc src/top.xdc
read_vhdl src/Legendre.vhd
read_vhdl src/DSPs/compute_pipeline.vhd
read_vhdl src/LegendreMatrix/bin_counter.vhd
read_vhdl src/LegendreMatrix/theta_slice.vhd
read_vhdl src/LegendreMatrix/TrigTables.vhd 
