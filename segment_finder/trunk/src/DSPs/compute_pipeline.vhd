----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- DSP pipeline to computer out he legendre transform for one slice of theta.
-- pipeline computation of r = x_i * Cos(t) + y_i * Sin(t) - r0 +/- R_i
-- UNITS:
--  tube_x,tube_y, and R are in units of mm * 8 (coordinate scaling)
--    2^11 is TRIG_SCALING_FACTOR
--  sin/cos (t) are in the range of 2^11 * (-1 to 1) or -2048 to 2048
--  r0 is in the same units as x,y, and R
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

                         
entity compute_pipeline is
  
  generic (
    COORDINATE_WIDTH       : integer := 12;
    TRIG_WIDTH             : integer := 12;
    BINNING_BITSHIFT_COUNT : integer := 1;
    BIN_WIDTH              : integer := 7);

  port (
    clk              : in std_logic;
    data_in_valid    : in std_logic;
    data_in_last     : in std_logic;
    tube_x           : in  signed(COORDINATE_WIDTH-1 downto 0);
    cos              : in  signed(TRIG_WIDTH-1 downto 0);
    tube_y           : in  signed(COORDINATE_WIDTH-1 downto 0);
    sin              : in  signed(TRIG_WIDTH-1 downto 0);
    R                : in  signed(COORDINATE_WIDTH- 1 downto 0);
    R_pm             : in  std_logic;
    r0               : in  signed(COORDINATE_WIDTH- 1 downto 0);
--    data_out_valid   : out std_logic;
    data_out_last    : out std_logic;
    bin_even         : out std_logic_vector(BIN_WIDTH-1 downto 0);
    bin_even_valid   : out std_logic;
--    bin_even_OOR     : out std_logic;
    bin_odd          : out std_logic_vector(BIN_WIDTH-1 downto 0);
    bin_odd_valid    : out std_logic;
--    bin_odd_OOR      : out std_logic;
    primary_bin_even : out std_logic
    );

end entity compute_pipeline;

architecture arch of compute_pipeline is

  constant MULT_WIDTH : integer := COORDINATE_WIDTH+TRIG_WIDTH;
  type coordinate_delay_t is array (integer range <>) of signed(COORDINATE_WIDTH -1 downto 0);
  type trig_delay_t is array (integer range <>) of signed(TRIG_WIDTH -1 downto 0);
  type mult_delay_t is array (integer range <>) of signed(MULT_WIDTH -1 downto 0);

  
  signal tube_x_delay : coordinate_delay_t(1 downto 0) := (others => (others =>'0'));
  signal tube_y_delay : coordinate_delay_t(2 downto 0) := (others => (others =>'0'));
  signal cosine_delay : trig_delay_t      (1 downto 0) := (others => (others =>'0'));
  signal sine_delay   : trig_delay_t      (2 downto 0) := (others => (others =>'0'));
  signal R_r0_delay      : mult_delay_t      (2 downto 0) := (others => (others =>'0'));
  signal data_valid_delay : std_logic_vector(5 downto 0) := (others => '0');
  signal bin_even_valid_delay : std_logic := '0';
  signal bin_odd_valid_delay : std_logic := '0';
  signal data_last_delay  : std_logic_vector(5 downto 0) := (others => '0');
  
    
  signal mult_x         : signed(MULT_WIDTH-1 downto 0);
  signal mult_x_sub     : signed(MULT_WIDTH-1 downto 0);
  signal mult_y         : signed(MULT_WIDTH-1 downto 0);
  signal mult_y_add     : signed(MULT_WIDTH-1 downto 0);
  
  signal final_r : std_logic_vector(MULT_WIDTH-1 downto 0);
  signal final_r_plus_1  : std_logic_vector(MULT_WIDTH-1 downto 0);
  signal final_r_minus_1 : std_logic_vector(MULT_WIDTH-1 downto 0);
  
  constant final_r_add_1 : signed(MULT_WIDTH-1 downto 0) := ( (TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT) => '1', others => '0');
  
  signal r0_scaled : signed(MULT_WIDTH-1 downto 0);
  signal R_scaled  : signed(MULT_WIDTH-1 downto 0);

  attribute use_dsp48          : string;
  attribute use_dsp48 of mult_y_add : signal is "yes";
  attribute use_dsp48 of mult_x_sub : signal is "yes";

  signal data_out_valid_delay   : std_logic;
  signal data_out_last_delay    : std_logic;
  signal bin_even_delay         : std_logic_vector(BIN_WIDTH-1 downto 0);
  signal bin_even_OOR_delay     : std_logic;
  signal bin_odd_delay          : std_logic_vector(BIN_WIDTH-1 downto 0);
  signal bin_odd_OOR_delay      : std_logic;
  signal primary_bin_even_delay : std_logic;

  signal data_out_valid_delay2   : std_logic;
  signal data_out_last_delay2    : std_logic;
  signal bin_even_delay2         : std_logic_vector(BIN_WIDTH-1 downto 0);
  signal bin_even_OOR_delay2     : std_logic;
  signal bin_odd_delay2          : std_logic_vector(BIN_WIDTH-1 downto 0);
  signal bin_odd_OOR_delay2      : std_logic;
  signal primary_bin_even_delay2 : std_logic;
  
begin  -- architecture arch

  --These must be scaled to match the COORDINATE_WIDTH * TRIG_WIDTH of the
  --x*cos and y*sin terms.
  --Since they are signed, we multiply by 2048 which is 2^11 instead of
  --2^TRIG_WDITH (2^12)
  r0_scaled(MULT_WIDTH-1) <= r0(COORDINATE_WIDTH-1);
  r0_scaled(MULT_WIDTH-2 downto TRIG_WIDTH-1) <= r0;
  r0_scaled(TRIG_WIDTH-2 downto 0) <= (others => '0');

  R_scaled(MULT_WIDTH-1) <= R(COORDINATE_WIDTH-1);
  R_scaled(MULT_WIDTH-2 downto TRIG_WIDTH-1) <= R;
  R_scaled(TRIG_WIDTH-2 downto 0) <= (others => '0');

  computation: process (clk) is
  begin  -- process mult
    if clk'event and clk = '1' then  -- rising clock edge

      -- pipeline delays
      tube_x_delay(1)          <= tube_x_delay(0);
      tube_x_delay(0)          <= tube_x;           
      tube_y_delay(2 downto 1) <= tube_y_delay(1 downto 0);
      tube_y_delay(0)          <= tube_y;
      cosine_delay(1)          <= cosine_delay(0);
      cosine_delay(0)          <= cos;
      sine_delay(2 downto 1)   <= sine_delay(1 downto 0);
      sine_delay(0)            <= sin;
      -- The R_r0 delay is a little different since we have to bitshift the
      -- incomming value to account for it being added on before we divide by
      -- the trig scaling (bitshift)
      R_r0_delay(2 downto 1) <= R_r0_delay(1 downto 0);
      R_r0_delay(0)(TRIG_WIDTH-1 downto 0) <= (others => '0');     
      if R_pm = '0' then
        -- Handle the xcos + ysin +R - r0 case
        -- We will do xcos + ysin - (r0 - R)
--        R_r0_delay(0)(MULT_WIDTH -1 downto TRIG_WIDTH) <= r0 - R;
        R_r0_delay(0) <= r0_scaled - R_scaled;
--        R_r0_delay(0) <= signed( r0(COORDINATE_WIDTH-1) & r0 & "000" & x"00") - signed( R(COORDINATE_WIDTH-1) & R & "000" & x"000");

      else
        -- Handle the xcos + ysin -R - r0 case
        -- We will do xcos + ysin - (r0 + R)
        --      R_r0_delay(0)(MULT_WIDTH -1 downto TRIG_WIDTH) <= r0 + R;
        R_r0_delay(0) <= r0_scaled + R_scaled;
      end if;

      data_valid_delay <= data_valid_delay(4 downto 0) & data_in_valid;
      data_last_delay <= data_last_delay(4 downto 0) & data_in_last;   
      
      --Pipelines are used to max out the speed of the DSPs
      
      -- pipeline layer 1
      --   just pipeline
      -- pipeline layer 2
      --   just pipeline
      -- pipeline layer 3
      mult_x <= (tube_x_delay(1) * cosine_delay(1));
      -- pipeline layer 4
      mult_x_sub <= mult_x - R_r0_delay(2);
      mult_y     <= tube_y_delay(2) * sine_delay(2);
      -- pipeline layer 5
      mult_y_add <= mult_x_sub + mult_y;
      --pipeline layer 6      
      final_r <= std_logic_vector(mult_y_add);
      final_r_plus_1  <= std_logic_vector(mult_y_add + final_r_add_1);
      final_r_minus_1 <= std_logic_vector(mult_y_add - final_r_add_1);


      ---------------------------------------------------------------------------
      -- Binning notes
      ---------------------------------------------------------------------------
      --Turn this into a bin number in "r".
      --This is done by bitshifting away the TRIG_WIDTH that scaled the trig
      --functions and bitshifting by a bin width scaling factor (power of 2, BINNING_BITSHIFT_COUNT)
      -- this also needs a -1 on the TRIG_WIDTH since it include the sign bit
      -- along with the scaling.
      --bin <= unsigned(final_r(BIN_WIDTH + TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT -1  downto TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT));

      --The bin is our of range if it is positive and any bit above the highest bit
      --used for "bin" is a 1, so or_reduce detects this
      --This is also out of range if it is negative, in which case atleast the MSB
      --must be a 1, so an or_reduce will detect this
      -- also needs the -1 on the TRIG_WIDTH
      -- bin_out_of_range <= or_reduce(final_r(COORDINATE_WIDTH+TRIG_WIDTH - 1 downto BIN_WIDTH + TRIG_WIDTH-1  + BINNING_BITSHIFT_COUNT));
      
      --Determin if the primary computed bin is odd or even
      if final_r(TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT) = '0' then
        -----------------------------------------------------
        --Primary bin is even
        -----------------------------------------------------
        bin_even_delay <= final_r(BIN_WIDTH + TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT -1  downto TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT);
        bin_even_OOR_delay <= or_reduce(final_r(COORDINATE_WIDTH+TRIG_WIDTH - 1 downto BIN_WIDTH + TRIG_WIDTH-1  + BINNING_BITSHIFT_COUNT));
        primary_bin_even_delay <= '1';

        
        --Determine the secondary bin by checking the next lower bit to the
        --primary bin value
        if final_r(TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT-1) = '0' then
          -------------------------------
          --We round down, so use minus_1 value
          -------------------------------
          bin_odd_delay <= final_r_minus_1(BIN_WIDTH + TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT -1  downto TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT);
          bin_odd_OOR_delay <= or_reduce(final_r_minus_1(COORDINATE_WIDTH+TRIG_WIDTH - 1 downto BIN_WIDTH + TRIG_WIDTH-1  + BINNING_BITSHIFT_COUNT));
        else
          -------------------------------
          --We round up, so use plus_1 value
          -------------------------------
          bin_odd_delay <= final_r_plus_1(BIN_WIDTH + TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT -1  downto TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT);
          bin_odd_OOR_delay <= or_reduce(final_r_plus_1(COORDINATE_WIDTH+TRIG_WIDTH - 1 downto BIN_WIDTH + TRIG_WIDTH-1  + BINNING_BITSHIFT_COUNT));
          
        end if;
      else
        -----------------------------------------------------
        --Primary bin is odd
        -----------------------------------------------------
        bin_odd_delay <= final_r(BIN_WIDTH + TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT -1  downto TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT);
        bin_odd_OOR_delay <= or_reduce(final_r(COORDINATE_WIDTH+TRIG_WIDTH - 1 downto BIN_WIDTH + TRIG_WIDTH-1  + BINNING_BITSHIFT_COUNT));
        primary_bin_even_delay <= '0';

        --Determine the secondary bin by checking the next lower bit to the
        --primary bin value
        if final_r(TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT-1) = '0' then
          -------------------------------
          --We round down, so use minus_1 value
          -------------------------------
          bin_even_delay <= final_r_minus_1(BIN_WIDTH + TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT -1  downto TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT);
          bin_even_OOR_delay <= or_reduce(final_r_minus_1(COORDINATE_WIDTH+TRIG_WIDTH - 1 downto BIN_WIDTH + TRIG_WIDTH-1  + BINNING_BITSHIFT_COUNT));
        else
          -------------------------------
          --We round up, so use plus_1 value
          -------------------------------
          bin_even_delay <= final_r_plus_1(BIN_WIDTH + TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT -1  downto TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT);
          bin_even_OOR_delay <= or_reduce(final_r_plus_1(COORDINATE_WIDTH+TRIG_WIDTH - 1 downto BIN_WIDTH + TRIG_WIDTH-1  + BINNING_BITSHIFT_COUNT));
          
        end if;
      end if;
     
      --Data valid and last pipeline
--      data_out_valid_delay <= data_valid_delay(5);
      data_out_last_delay <= data_last_delay(5);


      
--      data_out_valid_delay2 <=   data_out_valid_delay;
--      data_out_last_delay2 <=    data_out_last_delay;    
--      bin_even_delay2 <=         bin_even_delay;         
--      bin_even_OOR_delay2 <=     bin_even_OOR_delay;     
--      bin_odd_delay2 <=          bin_odd_delay;          
--      bin_odd_OOR_delay2 <=      bin_odd_OOR_delay;      
--      primary_bin_even_delay2 <= primary_bin_even_delay; 


--      data_out_valid   <= data_out_valid_delay;
      
      data_out_last    <= data_out_last_delay;    
      bin_even         <= bin_even_delay;
      bin_even_valid   <= data_valid_delay(5) and (not bin_even_OOR_delay);
      --bin_even_OOR     <= bin_even_OOR_delay;     
      bin_odd          <= bin_odd_delay;
      bin_odd_valid    <= data_valid_delay(5) and (not bin_odd_OOR_delay);
--      bin_odd_OOR      <= bin_odd_OOR_delay;      
      primary_bin_even <= primary_bin_even_delay; 

      
      
    end if;
  end process computation;

  
end architecture arch;
