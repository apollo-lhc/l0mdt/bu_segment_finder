-------------------------------------------------------------------------------
-- Search using multiple layers of quad muxes
-- This should be generalized to non powers of 4
--   This would involve promoting some entries to the next level.
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity quad_select is
  
  generic (
    LAYER_COUNT : integer := 3;
    DATA_WIDTH  : integer := 4
    );

  port (
    D   : in  std_logic_vector((4**(LAYER_COUNT)) *DATA_WIDTH -1 downto 0);
    sel : in  std_logic_vector(2*LAYER_COUNT -1 downto 0);
    Q   : out std_logic_vector(DATA_WIDTH -1 downto 0));

end entity quad_select;

architecture behavioral of quad_select is
  component quad_mux is
    port (
      D_0 : in  std_logic;
      D_1 : in  std_logic;
      D_2 : in  std_logic;
      D_3 : in  std_logic;
      sel : in  std_logic_vector(1 downto 0);
      Q   : out std_logic);
  end component quad_mux;
  
  constant MUX_WIDTH : integer := 4;
  constant MUX_BIT_WIDTH : integer := 2;--log2(MUX_WIDTH);
  type slv_array_t is array (LAYER_COUNT downto 0) of std_logic_vector((4**(LAYER_COUNT)) *DATA_WIDTH -1 downto 0);
  signal layer_data : slv_array_t;
  
begin  -- architecture behavioral

  layer_data(LAYER_COUNT) <= D;
  
  layers: for iLayer in LAYER_COUNT -1 downto 0 generate
    
    layer: for i in (MUX_WIDTH**iLayer) -1 downto 0 generate
      
      layer_bit: for iBit in DATA_WIDTH -1 downto 0  generate
        
        quad_mux_1: entity work.quad_mux
          port map (
            --              source layer |  Which data word    | Which bit of that word |
            D_0 => layer_data(iLayer + 1)( ( (MUX_WIDTH*i + 0) * DATA_WIDTH) + iBit),
            D_1 => layer_data(iLayer + 1)( ( (MUX_WIDTH*i + 1) * DATA_WIDTH) + iBit),
            D_2 => layer_data(iLayer + 1)( ( (MUX_WIDTH*i + 2) * DATA_WIDTH) + iBit),
            D_3 => layer_data(iLayer + 1)( ( (MUX_WIDTH*i + 3) * DATA_WIDTH) + iBit),
            sel => sel( (MUX_BIT_WIDTH*(LAYER_COUNT-iLayer)) - 1 downto MUX_BIT_WIDTH*(LAYER_COUNT-iLayer-1)),
            --                dest layer |  which data word| which bit
            Q   => layer_data(iLayer    )( (i* DATA_WIDTH) + iBit));
      end generate layer_bit;
    end generate layer;    
  end generate layers;

  Q <= layer_data(0)(DATA_WIDTH -1 downto 0);
  
end architecture behavioral;
