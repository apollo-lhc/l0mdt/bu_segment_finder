-------------------------------------------------------------------------------
-- Legendre simple counter
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;


entity bin_counter is

  generic (
    DATA_WIDTH  : integer          := 4
    );
  port (
    clk         : in  std_logic;
    reset_async  : in  std_logic;
    increment   : in  std_logic;
    count       : out unsigned(DATA_WIDTH-1 downto 0)
    );

end entity bin_counter;

architecture behavioral of bin_counter is
  constant ZERO_VALUE : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
  constant MAX_VALUE  : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '1');
  signal local_count  : std_logic_vector(DATA_WIDTH-1 downto 0) := ZERO_VALUE;
  signal local_count_latched  : std_logic_vector(DATA_WIDTH-1 downto 0) := ZERO_VALUE;

begin  -- architecture behavioral

  count <= unsigned(local_count_latched);
  


  counter_bits: process (local_count_latched) is
  begin  -- process counter_bits
    case local_count_latched is
      when x"f" =>
        local_count <= x"f";                   
      when others =>
        local_count <= std_logic_vector(unsigned(local_count_latched) + 1);
    end case;
  end process counter_bits;


  bit_loop: for iBit in 3 downto 0 generate    
    count_FFs: process (clk,reset_async) is
    begin  -- process count_FFs
      if reset_async = '1' then               -- asynchronous reset (active high)
        local_count_latched(iBit) <= '0';
      elsif clk'event and clk = '1' then  -- rising clock edge
        if increment = '1' then
          local_count_latched(iBit) <= local_count(iBit);        
        end if;
      end if;
    end process count_FFs;
    
  end generate bit_loop;
  

end architecture behavioral;

