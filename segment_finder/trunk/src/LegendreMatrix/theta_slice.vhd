-------------------------------------------------------------------------------
-- This is a container for the rbin counters for a specific theta value.
-- It receives a pipeline of r values and incrments the appropriate bin,
-- keeping track of the maximum bin.
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

entity legendre_theta_slice is
  
  generic (
    R_BIN_COUNT_WIDTH : integer := 7;
    COUNT_BIT_WIDTH   : integer := 4);  
  port (
    clk              : in  std_logic;
    reset_sync       : in  std_logic;
    reset_async      : in  std_logic;
    incr_rbin_even   : in  std_logic;
    rbin_even        : in  std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0);
    incr_rbin_odd    : in  std_logic;
    rbin_odd         : in  std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0);
    primary_bin_even : in std_logic;
    max_value        : out std_logic_vector(COUNT_BIT_WIDTH   - 1 downto 0);
    max_bin          : out std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0));

end entity legendre_theta_slice;

architecture behavioral of legendre_theta_slice is
  --Counter for each bin of this theta slice 
  component bin_counter is
    generic (
      DATA_WIDTH : integer);
    port (
      clk         : in  std_logic;
      reset_async  : in  std_logic;
      increment   : in  std_logic;
      count       : out unsigned(DATA_WIDTH-1 downto 0));
  end component bin_counter;

  component quad_select is
    generic (
      LAYER_COUNT : integer;
      DATA_WIDTH  : integer);
    port (
      D   : in  std_logic_vector((4**LAYER_COUNT) *DATA_WIDTH -1 downto 0);
      sel : in  std_logic_vector(2*LAYER_COUNT -1 downto 0);
      Q   : out std_logic_vector(DATA_WIDTH -1 downto 0));
  end component quad_select;
  
  -------------------------------------------------------------------------------
  -- bin control signals
  -------------------------------------------------------------------------------
  constant R_BIN_COUNT : integer := 2**R_BIN_COUNT_WIDTH;
  --Accessor to the bin contents
  type count_array_t is array (R_BIN_COUNT-1 downto 0) of unsigned(COUNT_BIT_WIDTH-1 downto 0);
  signal bin_content : count_array_t := (others => (others =>'0'));
  --Odd even splitting signals
  signal odd_bins         : std_logic_vector(COUNT_BIT_WIDTH*R_BIN_COUNT/2 -1 downto 0);
  signal even_bins        : std_logic_vector(COUNT_BIT_WIDTH*R_BIN_COUNT/2 -1 downto 0);
  signal odd_bin_content  : std_logic_vector(COUNT_BIT_WIDTH-1 downto 0);
  signal even_bin_content : std_logic_vector(COUNT_BIT_WIDTH-1 downto 0);

  signal odd_bin_content_last  : std_logic_vector(COUNT_BIT_WIDTH-1 downto 0);
  signal even_bin_content_last : std_logic_vector(COUNT_BIT_WIDTH-1 downto 0);


  signal incr_rbin_even_last   : std_logic;
  signal rbin_even_last        : std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0);
  signal incr_rbin_odd_last    : std_logic;
  signal rbin_odd_last         : std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0);
  signal primary_bin_even_last : std_logic;



  
  
  
  --mask for incrimenting a bin
  signal r_increment : std_logic_vector(R_BIN_COUNT -1 downto 0) := (others => '0');
  signal r_odd_incr : std_logic_vector(R_BIN_COUNT/2 -1 downto 0) := (others => '0');
  signal r_even_incr : std_logic_vector(R_BIN_COUNT/2 -1 downto 0) := (others => '0');

  -- reset counters to zero
  signal reset_theta_slice : std_logic := '0';

  constant MAX_COUNT_VALUE : std_logic_vector(COUNT_BIT_WIDTH -1 downto 0) := (others => '1');
 
  -------------------------------------------------------------------------------
  -- Count processing signals
  -------------------------------------------------------------------------------
  signal max_value_buffer : std_logic_vector(COUNT_BIT_WIDTH   - 1 downto 0) := (others => '0');
  signal max_value_local  : std_logic_vector(COUNT_BIT_WIDTH   - 1 downto 0) := (others => '0');
  signal max_bin_buffer   : std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  signal max_bin_local    : std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');

  -------------------------------------------------------------------------------
  -- statistics signals
  -------------------------------------------------------------------------------
  signal ERROR_PULSE_at_max_value : std_logic := '0';
  
begin  -- architecture behavioral


  ------------------------------------------------------------------------------
  --Array of counters to act like bins
  ------------------------------------------------------------------------------
  r_bin_counters: for iBin in R_BIN_COUNT -1 downto 0 generate
    bin_counter_1: entity work.bin_counter
      generic map (
        DATA_WIDTH => COUNT_BIT_WIDTH)
      port map (
        clk         => clk,
        reset_async  => reset_theta_slice,--reset_async,--reset_theta_slice,
        increment   => r_increment(iBin),
        count       => bin_content(iBin));
  end generate r_bin_counters;

  ------------------------------------------------------------------------------
  -- Lookup of current values from the bins
  -- This is broken up into odd and even bin numbers
  ------------------------------------------------------------------------------
  odd_even_splitting: for i_bin_pair in R_BIN_COUNT/2 -1 downto 0 generate
    odd_bins ( ((i_bin_pair+1) * COUNT_BIT_WIDTH)-1 downto i_bin_pair*COUNT_BIT_WIDTH) <= std_logic_vector(bin_content(i_bin_pair*2 + 1));
    even_bins( ((i_bin_pair+1) * COUNT_BIT_WIDTH)-1 downto i_bin_pair*COUNT_BIT_WIDTH) <= std_logic_vector(bin_content(i_bin_pair*2    ));
    r_increment(i_bin_pair*2 + 0) <= r_even_incr(i_bin_pair);
    r_increment(i_bin_pair*2 + 1) <= r_odd_incr(i_bin_pair);
  end generate odd_even_splitting;


  quad_select_even: entity work.quad_select
    generic map (
      LAYER_COUNT => 3,
      DATA_WIDTH  => COUNT_BIT_WIDTH)
    port map (
      D   => even_bins,
      sel => rbin_even(R_BIN_COUNT_WIDTH-1 downto 1),
      Q   => even_bin_content);

  quad_select_odd: entity work.quad_select
    generic map (
      LAYER_COUNT => 3,
      DATA_WIDTH  => COUNT_BIT_WIDTH)
    port map (
      D   => odd_bins,
      sel => rbin_odd(R_BIN_COUNT_WIDTH-1 downto 1),
      Q   => odd_bin_content);
  

  --Output the max value and max_bin when in the done state
  max_value <= max_value_buffer;
  max_bin   <= max_bin_buffer;
  
  fill_process: process (clk,reset_async) is
  begin  -- process fill_process
    if reset_async = '1' then
      --Reset our bins
--      reset_theta_slice <= '1';
      
      --Reset the max values
--      max_value_buffer <= (others => '0');
--      max_bin_buffer   <= (others => '0');
--      max_value_local  <= (others => '0');
--      max_bin_local    <= (others => '0');

    elsif clk'event and clk = '1' then  -- rising clock edge
      --Reset pulses from this process
--      reset_theta_slice <= '0';
      ERROR_PULSE_at_max_value <= '0';

      --reset the count increment mask
      r_even_incr <= (others => '0');
      r_odd_incr <= (others => '0');

      --output the current max values
      max_value_buffer <= max_value_local;
      max_bin_buffer <= max_bin_local;







      
      --Buffer for next clock tick
      incr_rbin_even_last <= incr_rbin_even;
      incr_rbin_odd_last  <= incr_rbin_odd;




      
      if reset_sync = '1' then
--        --Reset our bins
        reset_theta_slice <= '1';


        max_value_buffer <= (others => '0');
        max_bin_buffer   <= (others => '0');
        max_value_local  <= (others => '0');
        max_bin_local    <= (others => '0');

        --Buffer for next clock tick
        incr_rbin_even_last <= '0';
        incr_rbin_odd_last  <= '0';
  

--
--        --Reset the max values
--        max_value_buffer <= (others => '0');
--        max_bin_buffer <= (others => '0');        
      elsif incr_rbin_even = '1' or incr_rbin_odd = '1' then

        ---------------------------------
        -- Increment the bins
        ---------------------------------
                
        --Increment the bin (counter handles overflow)
        r_even_incr(to_integer(unsigned(rbin_even(R_BIN_COUNT_WIDTH -1 downto 1)))) <= incr_rbin_even;
        r_odd_incr (to_integer(unsigned(rbin_odd (R_BIN_COUNT_WIDTH -1 downto 1)))) <= incr_rbin_odd;


--        --Buffer for next clock tick
--        incr_rbin_even_last <= incr_rbin_even;
--        incr_rbin_odd_last  <= incr_rbin_odd;

        rbin_even_last <= rbin_even;
        rbin_odd_last <= rbin_odd;

        odd_bin_content_last <= odd_bin_content;
        even_bin_content_last <= even_bin_content;

        primary_bin_even_last <= primary_bin_even;
--      else
--        incr_rbin_even_last <= '0';
--        incr_rbin_odd_last  <= '0';
        
      end if;

        










        
      if incr_rbin_even_last = '1' or incr_rbin_odd_last = '1'then
        ---------------------------------
        -- Update the max value (if needed)
        ---------------------------------
        if ( (incr_rbin_odd_last  = '1' and odd_bin_content_last  = std_logic_vector(max_value_local)) or
             (incr_rbin_even_last = '1' and even_bin_content_last = std_logic_vector(max_value_local)) ) then
          --Save the new maximum value (rbin's + 1), but we should check
          -- if we overflow.
          if max_value_local = MAX_COUNT_VALUE then
            --send an error pulse to count the number of times we reach the
            --max bin count, we NEVER should.                  
            ERROR_PULSE_at_max_value <= '1';
          else
            --only incrment the max value if we can
            max_value_local <= std_logic_vector(unsigned(max_value_local) + 1);                  
          end if;                                       
        end if;

        ---------------------------------
        --Determin the new max rbin (if needed)
        ---------------------------------
        if ( (incr_rbin_even_last = '1' and even_bin_content_last = std_logic_vector(max_value_local) ) and
             (incr_rbin_odd_last  = '1' and odd_bin_content_last  = std_logic_vector(max_value_local)) ) then
          --Both odd and even are at the max, so we check the priority
          if primary_bin_even_last = '1' then
            -- save even bin as the maximum r bin
            max_bin_local <= rbin_even_last;
          else
            -- save odd bin as the maximum r bin
            max_bin_local <= rbin_odd_last;            
          end if;
        elsif (incr_rbin_even_last = '1' and even_bin_content_last = std_logic_vector(max_value_local) ) then
          -- save even bin as the maximum r bin
          max_bin_local <= rbin_even_last;
        elsif (incr_rbin_odd_last = '1' and odd_bin_content_last = std_logic_vector(max_value_local) ) then
          -- save odd bin as the maximum r bin
          max_bin_local <= rbin_odd_last;
        end if;
        
      end if;
    end if;      
  end process fill_process;

end architecture behavioral;
