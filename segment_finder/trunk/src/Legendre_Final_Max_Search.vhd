-------------------------------------------------------------------------------
-- This module searches an array of values and y positions and returns
-- the entry and the (value/ y pos pair) for the maximum valued entry
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity Legendre_Final_Max_Search is
  generic (
    VALUE_BIT_WIDTH    : integer;
    POSITION_BIT_WIDTH : integer;
    ENTRIES_BIT_WIDTH  : integer);
  port (    
    clk          : in  std_logic;
    start_pulse  : in  std_logic;
    values       : in  std_logic_vector((VALUE_BIT_WIDTH    * (2**ENTRIES_BIT_WIDTH)) -1 downto 0);
    val_position : in  std_logic_vector((POSITION_BIT_WIDTH * (2**ENTRIES_BIT_WIDTH)) -1 downto 0);
    done         : out std_logic;
    max_value    : out std_logic_vector(VALUE_BIT_WIDTH    -1 downto 0);
    max_position : out std_logic_vector(POSITION_BIT_WIDTH -1 downto 0);
    max_entry    : out std_logic_vector(ENTRIES_BIT_WIDTH  -1 downto 0)
    );
  
end entity Legendre_Final_Max_Search;

architecture behavioral of Legendre_Final_Max_Search is
  constant ENTRY_COUNT : integer := 2**ENTRIES_BIT_WIDTH;

  -------------------------
  -- Max search signals
  signal legendre_level : integer := 0;

  type value_array_t    is array (integer range <>) of unsigned(VALUE_BIT_WIDTH-1    downto 0);
  type position_array_t is array (integer range <>) of std_logic_vector(POSITION_BIT_WIDTH-1 downto 0);
  type entry_array_t    is array (integer range <>) of integer range (2**ENTRIES_BIT_WIDTH)-1  downto 0;
  signal value_cache    : value_array_t(ENTRY_COUNT -1 downto 0)    := (others => (others => '0'));
  signal position_cache : position_array_t(ENTRY_COUNT -1 downto 0) := (others => (others => '0'));
  signal entry_cache    : entry_array_t(ENTRY_COUNT -1 downto 0)    := (others => 0);

  signal processing : std_logic := '0';

begin  -- architecture behavioral

 
  
  -------------------------------------------------------------------------------
  -- Process that waits for all processing to be done and then finds the
  -- legendre max
  done <= (not start_pulse) and (not processing);
  find_legendre_max: process (clk) is
  begin  -- process LEGENDRE_MAX_FIND
    if clk'event and clk = '1' then  -- rising clock edge
      if start_pulse = '1' and legendre_level = ENTRIES_BIT_WIDTH-1 then
        --Take one clock tick to latch the external values
        legendre_level <= ENTRIES_BIT_WIDTH;
        for iEntry in ENTRY_COUNT -1 downto 0 loop
          value_cache(iEntry)    <= unsigned(values((iEntry+1)*VALUE_BIT_WIDTH -1 downto iEntry*VALUE_BIT_WIDTH));
          position_cache(iEntry) <= val_position((iEntry+1)*POSITION_BIT_WIDTH -1 downto iEntry*POSITION_BIT_WIDTH);
          entry_cache(iEntry)    <= iEntry;
        end loop;  -- iEntry
        processing <= '1';
      elsif legendre_level /= 0 then              
        --Do the comparisons, which the number of halves every time, until we
        --get to legendre_level being 1, which causes the master state
        --machine to move to the next state.
        --At that point the maximum bin is in max_bin_compare(0)
        legendre_level <= legendre_level -1;        
        for iCompare in (ENTRY_COUNT/2) -1 downto 0 loop
          if value_cache(2*iCompare+1) > value_cache(2*iCompare) then
            value_cache(iCompare)    <= value_cache(2*iCompare+1);
            position_cache(iCompare) <= position_cache(2*iCompare+1);
            entry_cache(iCompare)    <= entry_cache(2*iCompare+1);
          else
            value_cache(iCompare)    <= value_cache(2*iCompare);
            position_cache(iCompare) <= position_cache(2*iCompare);
            entry_cache(iCompare)    <= entry_cache(2*iCompare);
          end if;
        end loop;  -- iCompare
      else
        max_value    <= std_logic_vector(value_cache(0));
        max_position <= position_cache(0);
        max_entry    <= std_logic_vector(to_unsigned(entry_cache(0),ENTRIES_BIT_WIDTH));
        processing   <= '0';                  
      end if;
    end if;
  end process find_legendre_max;
  
end architecture behavioral;
