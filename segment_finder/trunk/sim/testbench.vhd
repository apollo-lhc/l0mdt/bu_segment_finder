library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.Legendre_IO.all;
use ieee.numeric_std.all;
use work.Trig_lookup.all;

entity testbench_top is
  
  port (
    fit_valid : out std_logic;
    fit_z0       : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
    fit_alpha    : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0));   
end entity testbench_top;

architecture behavioral of testbench_top is
  component Legendre is
    port (
      clk               : in  std_logic;
      reset_sync        : in  std_logic;
      process_ROI       : in  std_logic;
      alpha_seed        : in  std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0);
      z0_seed           : in  std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
      hit_FIFO_in       : in  std_logic_vector((3*SCALED_COORDINATE_WIDTH)-1 downto 0);
      hit_FIFO_in_valid : in  std_logic;
      hit_FIFO_empty    : in  std_logic;
      hit_FIFO_read     : out std_logic;
      fit_valid         : out std_logic;
      fit_z0            : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
      fit_alpha         : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0));
  end component Legendre;

  component Legendre_new is
    port (
      clk               : in  std_logic;
      reset_sync        : in  std_logic;
      process_ROI       : in  std_logic;
      alpha_seed        : in  std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0);
      z0_seed           : in  std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
      hit_FIFO_in       : in  std_logic_vector((3*SCALED_COORDINATE_WIDTH)-1 downto 0);
      hit_FIFO_in_valid : in  std_logic;
      hit_FIFO_empty    : in  std_logic;
      hit_FIFO_read     : out std_logic;
      fit_valid         : out std_logic;
      fit_z0            : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
      fit_alpha         : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0));
  end component Legendre_new;

  component tb_input is
    port (
      clk               : in  std_logic;
      address           : in  unsigned(15 downto 0);
      reset_sync        : out std_logic;
      process_ROI       : out std_logic;
      alpha_seed        : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0);
      z0_seed           : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
      din               : out std_logic_vector(44 downto 0);
      wr_en             : out std_logic);
  end component tb_input;

  component tb_interface is
    port (
      clk               : in  std_logic;
      reset_sync        : in  std_logic;
      din               : in  std_logic_vector(44 downto 0);
      wr_en             : in  std_logic;

      hit_FIFO_read     : in  std_logic;
      hit_FIFO_in       : out std_logic_vector((3*SCALED_COORDINATE_WIDTH)-1 downto 0);
      hit_FIFO_in_valid : out std_logic;
      hit_FIFO_empty    : out std_logic);    
  end component tb_interface;
  
  signal reset_sync        : std_logic;
  signal process_ROI       : std_logic;
  signal alpha_seed        : std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0);
  signal z0_seed           : std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
  signal hit_FIFO_in       : std_logic_vector((3*SCALED_COORDINATE_WIDTH)-1 downto 0);
  signal hit_FIFO_in_valid : std_logic;
  signal hit_FIFO_empty    : std_logic;
  signal hit_FIFO_read     : std_logic;

  signal din : std_logic_vector(44 downto 0) := (others => '0');
  signal wr_en : std_logic := '0';

  signal clk       :  std_logic := '1';

  signal counter : unsigned(15 downto 0) := x"0000";
  constant counter_end : unsigned(15 downto 0) := x"ffff";
  
begin  -- architecture behavioral

  clk <= not clk after 1.5625 ns;

  address_counter: process (clk) is
  begin  -- process address_counter
    if clk'event and clk = '1' then  -- rising clock edge
      if counter /= counter_end then
        counter <= counter + 1;        
      end if;      
    end if;
  end process address_counter;
  
  tb_input_1: entity work.tb_input
    port map (
      clk               => clk,
      address           => counter,
      reset_sync        => reset_sync,
      process_ROI       => process_ROI,
      alpha_seed        => alpha_seed,
      z0_seed           => z0_seed,
      din               => din,
      wr_en             => wr_en);

  tb_interface_1: entity work.tb_interface
    port map (
      clk               => clk,
      reset_sync        => reset_sync,
      hit_FIFO_in       => hit_FIFO_in,
      hit_FIFO_in_valid => hit_FIFO_in_valid,
      hit_FIFO_empty    => hit_FIFO_empty,
      hit_FIFO_read     => hit_FIFO_read,
      din               => din,
      wr_en             => wr_en);
  
  UUT: entity work.Legendre_new
    port map (
      clk               => clk,
      reset_sync        => reset_sync,
      process_ROI       => process_ROI,
      alpha_seed        => alpha_seed,
      z0_seed           => z0_seed,
      hit_FIFO_in       => hit_FIFO_in,
      hit_FIFO_in_valid => hit_FIFO_in_valid,
      hit_FIFO_empty    => hit_FIFO_empty,
      hit_FIFO_read     => hit_FIFO_read,
      fit_valid         => fit_valid,
      fit_z0            => fit_z0,
      fit_alpha         => fit_alpha);

end architecture behavioral;
