import numpy as np
import matplotlib.pyplot as plt

#get these from meta file
r_size = 128
theta_size = 128

#load the coordinate axis info
axisFile = open("matrix_meta.txt")
theta_0, d_theta  = [float(x) for x in axisFile.readline().split()]
r_0, d_r  = [float(x) for x in axisFile.readline().split()]
print theta_0, d_theta
print r_0,d_r

r_max = r_0 + (d_r * r_size)
theta_max = theta_0 + (d_theta * theta_size)



matrix = np.zeros((r_size, theta_size))
for line in open("matrix.txt"):
    line = line.split()
    if len(line) == 3:
        x = int(line[0])
        y = int(line[1])
        z = int(line[2])
        matrix[y][x] = z

aspect_ratio = float(theta_max - theta_0)/float(r_max - r_0)

#plt.imshow(matrix,interpolation='none')
plt.imshow(matrix,interpolation='none',origin='lower',extent=[theta_0,theta_max,r_0,r_max],aspect=aspect_ratio)
plt.title('FPGA')
plt.xlabel(r'$\theta$-bin (mrad)')
plt.ylabel('r-bin (mm)')
plt.show()    
