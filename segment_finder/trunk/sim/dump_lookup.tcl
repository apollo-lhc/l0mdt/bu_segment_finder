set trigFile [open "trig_lookup.txt" "w"]

for {set bin 0} {$bin < 128} {incr bin} {

    #cosine
    set cmd "get_value -radix dec {/testbench_top/UUT/cos($bin)}"
    set cos [eval $cmd]
    set cmd "get_value -radix dec {/testbench_top/UUT/sin($bin)}"
    set sin [eval $cmd]
    set str "$bin $cos $sin"
    puts $trigFile $str
    
}

close $trigFile
