set matrixFile [open "matrix.txt" "w"]

for {set theta 0} {$theta < 128} {incr theta} {
    for {set r 0} {$r < 128} {incr r} {
	set cmd "get_value -radix unsigned {/testbench_top/UUT/\\theta_slice_generator($theta)\\/legendre_theta_slice_1/\\r_bin_counters($r)\\/bin_counter_1/local_count_latched}"
	set val [eval $cmd]
	set str "$theta $r $val"
	puts $matrixFile $str
    }
}

close $matrixFile

#get meta info about legendre transform
set matrixFile [open "matrix_meta.txt" "w"]

#get theta
set cmd "get_value -radix dec {/testbench_top/UUT/theta_lower_bound_inclusive}"
set val [eval $cmd]
set str "$val 1"
puts $matrixFile $str


#get r0
set cmd "get_value -radix dec {/testbench_top/UUT/r0}"
set val [eval $cmd]
set val [expr $val >> [expr 12+3]]
#print out r0 and the bin width
set str "$val 0.25"
puts $matrixFile $str 

close $matrixFile
