// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.3 (lin64) Build 1682563 Mon Oct 10 19:07:26 MDT 2016
// Date        : Tue Jun 13 10:18:01 2017
// Host        : volta running 64-bit Debian GNU/Linux 9.0 (n/a)
// Command     : write_verilog -force -mode synth_stub
//               /home/dan/work/ATLAS/atlas-phase-2-muon-upgrade.firmware/track_finder/segment_finder/trunk/proj/top.srcs/sources_1/ip/TRIG_RAM/TRIG_RAM_stub.v
// Design      : TRIG_RAM
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku115-flvf1924-2-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_3_4,Vivado 2016.3" *)
module TRIG_RAM(clka, ena, addra, douta)
/* synthesis syn_black_box black_box_pad_pin="clka,ena,addra[7:0],douta[35:0]" */;
  input clka;
  input ena;
  input [7:0]addra;
  output [35:0]douta;
endmodule
