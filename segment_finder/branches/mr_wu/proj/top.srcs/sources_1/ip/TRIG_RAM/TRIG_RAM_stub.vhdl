-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.3 (lin64) Build 1682563 Mon Oct 10 19:07:26 MDT 2016
-- Date        : Tue Jun 13 10:18:01 2017
-- Host        : volta running 64-bit Debian GNU/Linux 9.0 (n/a)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/dan/work/ATLAS/atlas-phase-2-muon-upgrade.firmware/track_finder/segment_finder/trunk/proj/top.srcs/sources_1/ip/TRIG_RAM/TRIG_RAM_stub.vhdl
-- Design      : TRIG_RAM
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TRIG_RAM is
  Port ( 
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 7 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 35 downto 0 )
  );

end TRIG_RAM;

architecture stub of TRIG_RAM is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clka,ena,addra[7:0],douta[35:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "blk_mem_gen_v8_3_4,Vivado 2016.3";
begin
end;
