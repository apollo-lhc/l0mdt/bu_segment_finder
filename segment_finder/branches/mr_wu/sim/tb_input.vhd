library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Legendre_IO.all;
use work.Trig_lookup.all;

entity tb_input is
  
  port (
    clk                : in  std_logic;
    address            : in  unsigned(15 downto 0);
    reset_sync         : out std_logic;
    process_ROI        : out std_logic;
    alpha_seed         : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0);
    z0_seed            : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
    din                : out std_logic_vector(44 downto 0);
    wr_en              : out std_logic
    );
  
end entity tb_input;

architecture behavioral of tb_input is
begin  -- architecture behavioral

  input_generation: process (clk) is
  begin  -- process input_generation
    if clk'event and clk = '1' then  -- rising clock edge
      case address is
        when x"0000" =>
          wr_en <= '0';
          process_ROI <= '0';
          reset_sync <= '1';
        when x"0001" =>
          reset_sync <= '0';
        when x"0002" =>
          process_ROI <= '1';
          alpha_seed  <= "010010001110";
          z0_seed     <= "000000111110000";
        when x"0003" =>
          din(44 downto 30) <= "000000000010000";
          din(29 downto 15) <= "000000011110000";
          din(14 downto  0) <= "000000111100000";
          wr_en <= '1';
        when x"0004" =>
          wr_en <= '0';
        when x"0005" =>
          din(44 downto 30) <= "000000000010111";
          din(29 downto 15) <= "000001101100000";
          din(14 downto  0) <= "000001101001000";
          wr_en <= '1';
        when x"0006" =>
          wr_en <= '0';
        when x"0007" =>
          din(44 downto 30) <= "000000000110011";
          din(29 downto 15) <= "000001010010000";
          din(14 downto  0) <= "000001011010000";
          wr_en <= '1';
        when x"0008" =>
          wr_en <= '0';
        when x"0009" =>
          din(44 downto 30) <= "000000001010000";
          din(29 downto 15) <= "000000111000000";
          din(14 downto  0) <= "000001001011000";
          wr_en <= '1';
        when x"000a" =>
          wr_en <= '0';
        when x"000b" =>
          din(44 downto 30) <= "000000001100001";
          din(29 downto 15) <= "000000011110000";
          din(14 downto  0) <= "000000011110000";
          wr_en <= '1';
        when x"000c" =>
          wr_en <= '0';
        when x"000d" =>
          din(44 downto 30) <= "000000001110000";
          din(29 downto 15) <= "000000011110000";
          din(14 downto  0) <= "000001011010000";
          wr_en <= '1';
        when x"000e" =>
          wr_en <= '0';
        when x"000f" =>
          din(44 downto 30) <= "000000000100111";
          din(29 downto 15) <= "000101001110000";
          din(14 downto  0) <= "000011010010001";
          wr_en <= '1';
        when x"0010" =>
          wr_en <= '0';
        when x"0011" =>
          din(44 downto 30) <= "000000000000011";
          din(29 downto 15) <= "000100110100000";
          din(14 downto  0) <= "000011000011001";
          wr_en <= '1';
        when x"0012" =>
          wr_en <= '0';
        when x"0013" =>
          din(44 downto 30) <= "000000001000100";
          din(29 downto 15) <= "000101101000000";
          din(14 downto  0) <= "000011100001001";
          wr_en <= '1';
        when x"0014" =>
          wr_en <= '0';
        when x"0015" =>
          din(44 downto 30) <= "000000001100001";
          din(29 downto 15) <= "000110000010001";
          din(14 downto  0) <= "000011110000010";
          wr_en <= '1';
        when x"0016" =>
          wr_en <= '0';
        when x"0017" =>
          din(44 downto 30) <= "000000001100000";
          din(29 downto 15) <= "000000111000000";
          din(14 downto  0) <= "001011011111110";
          wr_en <= '1';
        when x"0018" =>
          wr_en <= '0';
        when x"0019" =>
          din(44 downto 30) <= "000000001100111";
          din(29 downto 15) <= "000101101000000";
          din(14 downto  0) <= "001011111101111";
          wr_en <= '1';
        when x"001a" =>
          wr_en <= '0';
        when x"001b" =>
          process_ROI <= '0';
        when others => null;
      end case;
    end if;
  end process input_generation;
  
end architecture behavioral;
