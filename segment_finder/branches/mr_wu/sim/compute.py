import math
z0_seed = -101 # "100s um"
alpha_seed = 1256 # mrad


ROI_z0_seed = z0_seed * 8
print ROI_z0_seed

#SCALED_COORDINATE_WIDTH + TRIG_WIDTH -1 downto 0
r0 = ROI_z0_seed * int(2048*math.sin(alpha_seed/1000.0)) - 128*2048
print r0
#SCALED_COORDINATE_WIDTH + TRIG_WIDTH -1 downto 0
#r0_lowest_bin = ((r0 >> 11)) - 128
r0_lowest_bin = ((r0 >> 11))
print r0_lowest_bin
print ''

tube_x = int((0xfd2-0xfff) * 8)
tube_y = 0xc8 * 8
tube_r = 0x9 * 8

cos = -1589
sin = -1291

print tube_x
print cos
print tube_y
print sin
print tube_r
print r0_lowest_bin


R_r0_delay = (r0_lowest_bin-tube_r)*2048

mult_x = tube_x*cos
mult_x_sub = mult_x - R_r0_delay
mult_y = tube_y * sin
mult_y_add = mult_x_sub + mult_y

print ''
print R_r0_delay
print mult_x
print mult_x_sub
print mult_y
print mult_y_add
print ''
print hex(mult_y_add)
r_bin = (mult_y_add >> 13)&0x7 #(7 + 12+1 -1 downto 12 + 1) = 19 downto 13

bin_out_of_range = not (not(mult_y_add >> 20)) #(7+12+1)

print r_bin
print bin_out_of_range
