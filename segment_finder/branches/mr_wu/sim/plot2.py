import numpy as np
import matplotlib.pyplot as plt

sim_matrix = np.zeros((128, 128))
comp_matrix = np.zeros((128, 128))

#load the data from matrix.txt and build a matrix out of it
for line in open("matrix_cpu.txt"):
    line = line.split()
    if len(line) == 3:
        x = int(line[0])
        y = int(line[1])
        z = int(line[2])
        comp_matrix[y][x] = z

#same as above
for line in open("matrix.txt"):
    line = line.split()
    if len(line) == 3:
        x = int(line[0])
        y = int(line[1])
        z = int(line[2])
        sim_matrix[y][x] = z

#plt.imshow(matrix,interpolation='none')
plt.subplots_adjust(bottom=0.05,right=0.95,left=0.05,top=0.95)
plt.subplot(121,frameon=False)
plt.imshow(comp_matrix,interpolation='none',origin='lower',vmin=0,vmax=6)
plt.title('CPU')
plt.xlabel(r'$\theta$-bin')
plt.ylabel('r-bin')

plt.subplot(122,frameon=False)
plt.imshow(sim_matrix,interpolation='none',origin='lower',vmin=0,vmax=6)
plt.title('FPGA')
plt.xlabel(r'$\theta$-bin')
plt.ylabel('r-bin')



plt.show()    
