#!/usr/bin/env python

COORDINATE_SCALING_FACTOR = 8

hdl_begin = "library IEEE;\n"\
            "use IEEE.std_logic_1164.all;\n"\
            "use ieee.numeric_std.all;\n"\
            "use work.Legendre_IO.all;\n"\
            "use work.Trig_lookup.all;\n"\
            "\n"\
            "entity tb_input is\n"\
            "  \n"\
            "  port (\n"\
            "    clk                : in  std_logic;\n"\
            "    address            : in  unsigned(15 downto 0);\n"\
            "    reset_sync         : out std_logic;\n"\
            "    process_ROI        : out std_logic;\n"\
            "    alpha_seed         : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0);\n"\
            "    z0_seed            : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);\n"\
            "    din                : out std_logic_vector(44 downto 0);\n"\
            "    wr_en              : out std_logic\n"\
            "    );\n"\
            "  \n"\
            "end entity tb_input;\n"\
            "\n"\
            "architecture behavioral of tb_input is\n"\
            "begin  -- architecture behavioral\n"\
            "\n"\
            "  input_generation: process (clk) is\n"\
            "  begin  -- process input_generation\n"\
            "    if clk'event and clk = '1' then  -- rising clock edge\n"\
            "      case address is\n"\
            "        when x\"0000\" =>\n"\
            "          wr_en <= '0';\n"\
            "          process_ROI <= '0';\n"\
            "          reset_sync <= '1';\n"\
            "        when x\"0001\" =>\n"\
            "          reset_sync <= '0';\n"
hdl_end = "        when others => null;\n"\
          "      end case;\n"\
          "    end if;\n"\
          "  end process input_generation;\n"\
          "  \n"\
          "end architecture behavioral;\n"


#open output vhdl file
outputFile=open("tb_input.vhd","w")
#write the beginning of the vhdl file
outputFile.write(hdl_begin);




#generate data from an initial ROI
state=2
track_file = open("track.dat","r")
track_parameters = track_file.read()
#get the track angle in milliradians
alpha = int(float(track_parameters.split()[0]))
#get the z0 in 100s of um
z0 = int(COORDINATE_SCALING_FACTOR*float(track_parameters.split()[1]))
if z0 < 0:
    z0 = 0x7FFF + z0 + 1
track_file.close()
#print alpha,z0
#write the clock tick that starts the roi processing
outputFile.write("        when x\"0002\" =>\n")
outputFile.write("          process_ROI <= '1';\n")
outputFile.write("          alpha_seed  <= \""+bin(alpha)[2:].zfill(12)+"\";\n")
outputFile.write("          z0_seed     <= \""+bin(z0   )[2:].zfill(15)+"\";\n")

#outputFile.write("          alpha_seed  <= x\""+hex(alpha)[2:].zfill(3)+"\";\n")
#outputFile.write("          z0_seed     <= x\""+hex(z0   )[2:].zfill(3)+"\";\n")
state=state+1


#read in the hits
hit_file = open("hits.dat","r")
#hit_file = open("one_hit.dat","r")
for line in hit_file:
    val = line.split()
    if len(val) == 3:
        if val[2] != '0':
            #print val
            x = int(COORDINATE_SCALING_FACTOR*float(val[0]))
            if x < 0:
                x = 0x7FFF + x +1
#            x = hex(x)[2:].zfill(4)
            x = bin(x)[2:].zfill(15)

            y = int(COORDINATE_SCALING_FACTOR*float(val[1]))
            if y < 0:
                y = 0x7FFF + y + 1
#            y = hex(y)[2:].zfill(4)
            y = bin(y)[2:].zfill(15)

            r = int(COORDINATE_SCALING_FACTOR*float(val[2]))
            if r < 0:
                r = 0x7FFF + r + 1
#            r = hex(r)[2:].zfill(4)
            r = bin(r)[2:].zfill(15)

            outputFile.write("        when x\""+hex(state)[2:].zfill(4) +"\" =>\n")
            outputFile.write("          din(44 downto 30) <= \""+r+"\";\n")
            outputFile.write("          din(29 downto 15) <= \""+y+"\";\n")
            outputFile.write("          din(14 downto  0) <= \""+x+"\";\n")
#            outputFile.write("          din(44 downto 30) <= x\""+r+"\";\n")
#            outputFile.write("          din(29 downto 15) <= x\""+y+"\";\n")
#            outputFile.write("          din(14 downto  0) <= x\""+x+"\";\n")
            outputFile.write("          wr_en <= '1';\n")
            state=state+1
            outputFile.write("        when x\""+hex(state)[2:].zfill(4) +"\" =>\n")
            outputFile.write("          wr_en <= '0';\n")
            state=state+1


outputFile.write("        when x\""+hex(state)[2:].zfill(4) +"\" =>\n")
outputFile.write("          process_ROI <= '0';\n")

#output the end of the vhdl file
outputFile.write(hdl_end);
#close file
outputFile.close();
