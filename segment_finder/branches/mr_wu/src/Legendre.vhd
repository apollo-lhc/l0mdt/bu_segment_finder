-------------------------------------------------------------------------------
-- This module contains the theta slices for the legendre transform and the
-- module for the pipelining of the computation of the legendre transform
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.LEGENDRE_IO.all;
use work.Trig_lookup.all;

entity Legendre_new is  
  port (
    
    clk         : in std_logic;
    reset_sync  : in std_logic;

    --ROI interface
    process_ROI : in std_logic;
    alpha_seed  : in std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1       downto 0);  -- mRadians
    z0_seed     : in std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);  -- mm
    --(extra -1 is for bumping this up to a signed value)
        
    --FIFO interface    
    hit_FIFO_in       : in std_logic_vector((3*SCALED_COORDINATE_WIDTH)-1 downto 0); -- r,y,x (mm)
    hit_FIFO_in_valid : in std_logic;
    hit_FIFO_empty    : in std_logic;
    hit_FIFO_read     : out std_logic;

    --ROI result
    fit_valid    : out std_logic;
    fit_z0       : out std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
    fit_alpha    : out std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0));
  
end entity Legendre_new;

architecture behavioral of Legendre_new is

  -------------------------------------------------------------------------------
  -- theta slice objects
  -------------------------------------------------------------------------------
  -------------------------------------------------------------------------------
  -- constants
  constant THETA_BIN_COUNT : integer := 2**THETA_BIN_COUNT_WIDTH;
  constant R_BIN_COUNT2     : integer := 2**R_BIN_COUNT_WIDTH;

  component compute_pipeline is
    generic (
      COORDINATE_WIDTH       : integer;
      TRIG_WIDTH             : integer;
      BINNING_BITSHIFT_COUNT : integer;
      BIN_WIDTH              : integer);
    port (
      clk              : in  std_logic;
      data_in_valid    : in  std_logic;
      data_in_last     : in  std_logic;
      tube_x           : in  signed(COORDINATE_WIDTH-1 downto 0);
      cos              : in  signed(TRIG_WIDTH-1 downto 0);
      tube_y           : in  signed(COORDINATE_WIDTH-1 downto 0);
      sin              : in  signed(TRIG_WIDTH-1 downto 0);
      R                : in  signed(COORDINATE_WIDTH- 1 downto 0);
      R_pm             : in  std_logic;
      r0               : in  signed(COORDINATE_WIDTH- 1 downto 0);
--      data_out_valid   : out std_logic;
      data_out_last    : out std_logic;
      bin_even         : out std_logic_vector(BIN_WIDTH-1 downto 0);
      bin_even_valid   : out std_logic;
--      bin_even_OOR     : out std_logic;
      bin_odd          : out std_logic_vector(BIN_WIDTH-1 downto 0);
      bin_odd_valid    : out std_logic;
--      bin_odd_OOR      : out std_logic;
      primary_bin_even : out std_logic
      );
  end component compute_pipeline;
  
  component legendre_theta_slice is
    generic (
      R_BIN_COUNT_WIDTH : integer;
      COUNT_BIT_WIDTH   : integer);
    port (
      clk              : in  std_logic;
      reset_sync       : in  std_logic;
      reset_async       : in  std_logic;
      incr_rbin_even   : in std_logic;
      rbin_even        : in std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0);
      incr_rbin_odd    : in std_logic;
      rbin_odd         : in std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0);
      primary_bin_even : in std_logic;
      max_value        : out std_logic_vector(COUNT_BIT_WIDTH   - 1 downto 0);
      max_bin          : out std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0));
  end component legendre_theta_slice;

  component TrigTable is
    generic (
      THETA_STARTING_VALUE    : integer; 
      THETA_BIN_COUNT_WIDTH   : integer;         
      BLOCKRAM_BITS_PER_THETA : integer; 
      BITS_USED_PER_SAMPLE    : integer; 
      FUNDAMENTAL_RAM_WIDTH   : integer);
    port (
      clk           : in  std_logic;
      theta         : in  signed(THETA_BIN_COUNT_WIDTH-1 downto 0);
      cosine        : out trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0);
      sine          : out trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0));
  end component TrigTable;

  component Legendre_Final_Max_Search is
    generic (
      VALUE_BIT_WIDTH    : integer;
      POSITION_BIT_WIDTH : integer;
      ENTRIES_BIT_WIDTH  : integer);
    port (
      clk          : in  std_logic;
      start_pulse  : in  std_logic;
      values       : in  std_logic_vector((VALUE_BIT_WIDTH * (2**ENTRIES_BIT_WIDTH)) -1 downto 0);
      val_position : in  std_logic_vector((POSITION_BIT_WIDTH * (2**ENTRIES_BIT_WIDTH)) -1 downto 0);
      done         : out std_logic;
      max_value    : out std_logic_vector(VALUE_BIT_WIDTH -1 downto 0);
      max_position : out std_logic_vector(POSITION_BIT_WIDTH -1 downto 0);
      max_entry    : out std_logic_vector(ENTRIES_BIT_WIDTH -1 downto 0));
  end component Legendre_Final_Max_Search;
  
--  constant TRIG_LINE_SAMPLE_COUNT_BITWIDTH : integer := floor(log2((FUNDAMENTAL_RAM_WIDTH/BITS_USED_PER_SAMPLE)*(BITS_USED_PER_SAMPLE/TRIG_WIDTH)));
--  constant TRIG_LINE_SAMPLE_COUNT          : integer := 2**TRIG_LINE_SAMPLE_COUNT_BITWIDTH;
  
  signal cos : trig_array_t(THETA_BIN_COUNT - 1 downto 0) := (others => (others => '0'));
  signal sin : trig_array_t(THETA_BIN_COUNT - 1 downto 0) := (others => (others => '0'));
  
  -------------------------
  -- ROI BUffers
  signal ROI_alpha_seed  : std_logic_vector(TRIG_TABLE_BIN_COUNT_WIDTH - 1       downto 0);
  signal ROI_z0_seed     : std_logic_vector(SCALED_COORDINATE_WIDTH - 1 downto 0);
  
  -------------------------
  -- Compute pipeline
  constant BINNING_BITSHIFT_COUNT : integer := 1;
  type hit_state_t is (HIT_STATE_IDLE,HIT_STATE_NEW_HIT_P,HIT_STATE_NEW_HIT_N);
  signal hit_state : hit_state_t := HIT_STATE_IDLE;
  signal data_in_valid  : std_logic := '0';
  signal data_out_valid : std_logic_vector(THETA_BIN_COUNT -1 downto 0) := (others => '0');
  signal data_in_last : std_logic := '0';
  signal data_out_last : std_logic_vector(THETA_BIN_COUNT -1 downto 0) := (others => '0');
  signal tube_x : signed(SCALED_COORDINATE_WIDTH -1 downto 0) := (others => '0');
  signal tube_y : signed(SCALED_COORDINATE_WIDTH -1 downto 0) := (others => '0');
  signal R      : signed(SCALED_COORDINATE_WIDTH -1 downto 0) := (others => '0');
  signal R_pm   : std_logic := '0';
  signal r0            : signed(SCALED_COORDINATE_WIDTH + TRIG_WIDTH -1 downto 0) := (others => '0');
  signal r0_mult       : signed(SCALED_COORDINATE_WIDTH + TRIG_WIDTH -1 downto 0) := (others => '0');
  signal r0_lowest_bin : signed(SCALED_COORDINATE_WIDTH -1 downto 0) := (others => '0');
  signal sin_alpha : signed(TRIG_WIDTH-1 downto 0);
  
  type rbin_array_t is array (integer range <>) of std_logic_vector(R_BIN_COUNT_WIDTH - 1 downto 0);
  signal rbin_even        : rbin_array_t(THETA_BIN_COUNT - 1 downto 0) := (others => (others => '0'));
  signal rbin_even_OOR    : std_logic_vector(THETA_BIN_COUNT -1 downto 0) := (others => '0');
  signal rbin_odd         : rbin_array_t(THETA_BIN_COUNT - 1 downto 0) := (others => (others => '0'));
  signal rbin_odd_OOR     : std_logic_vector(THETA_BIN_COUNT - 1 downto 0) := (others => '0');
  signal primary_bin_even : std_logic_vector(THETA_BIN_COUNT - 1 downto 0) := (others => '0');  
  signal incr_bin_even    : std_logic_vector(THETA_BIN_COUNT - 1 downto 0) := (others => '0');
  signal incr_bin_odd     : std_logic_vector(THETA_BIN_COUNT - 1 downto 0) := (others => '0');
  
  -------------------------
  -- Legendre outputs
  type legendre_count_array_t is array (integer range <>) of std_logic_vector(COUNT_BIT_WIDTH - 1 downto 0);  
  signal max_value : std_logic_vector((COUNT_BIT_WIDTH*THETA_BIN_COUNT) -1 downto 0)   := (others => '0');
  signal max_r_bin : std_logic_vector((R_BIN_COUNT_WIDTH*THETA_BIN_COUNT) -1 downto 0) := (others => '0');

  -------------------------
  -- Theta signals
  type signed_array_t is array (integer range <>) of signed(TRIG_TABLE_BIN_COUNT_WIDTH -1 downto 0);
  type unsigned_array_t is array (integer range <>) of unsigned(THETA_BIN_COUNT_WIDTH -1 downto 0);
  
  signal theta_fill_value : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  signal theta_fill_value_delay : signed_array_t(2 downto 0) := (others => (others => '0'));
  
  signal theta_fill_bin_value : unsigned(THETA_BIN_COUNT_WIDTH - 1 downto 0);
  signal theta_fill_bin_value_delay : unsigned_array_t(2 downto 0) := (others => (others => '0'));
  
  signal theta_lower_bound_inclusive : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');  
  signal theta_upper_bound_exclusive : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  signal theta_upper_bound_exclusive_other : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');

  signal cos_buffer : trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0) := (others => (others => '0'));
  signal sin_buffer : trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0) := (others => (others => '0'));
  signal cos_buffer2 : trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0) := (others => (others => '0'));
  signal sin_buffer2 : trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0) := (others => (others => '0'));
  signal cosine_lookup : trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0) := (others => (others => '0'));
  signal sine_lookup : trig_array_t(TRIG_LOOKUP_LINE_COUNT -1 downto 0) := (others => (others => '0'));

  -------------------------
  -- Max search signals
  signal max_search_start :std_logic := '0';
  signal Legendre_max_done : std_logic := '0';
  signal Legendre_max_bin_value : std_logic_vector(COUNT_BIT_WIDTH-1 downto 0);
  signal Legendre_max_bin_r : std_logic_vector(R_BIN_COUNT_WIDTH-1 downto 0);
  signal Legendre_max_bin_theta : std_logic_vector(THETA_BIN_COUNT_WIDTH-1 downto 0);

  
  -------------------------
  -- Result computations
  signal computed_alpha : signed(TRIG_TABLE_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  signal computed_z0 : signed(TRIG_WIDTH + SCALED_COORDINATE_WIDTH - 1 downto 0) := (others => '0');
  signal computed_fit_valid : std_logic := '0';

  

  -------------------------
  -- other
  signal hit_FIFO_read_local : std_logic := '0';
  signal reset_legendre_matrix : std_logic := '0';
  signal reset_legendre_matrix_delay : std_logic := '0';
  signal error_pulse_roi_not_finished : std_logic := '0';
  signal process_ROI_old : std_logic := '0';
  
  -------------------------
  -- State machine
  type ROI_state_t is (ROI_STATE_IDLE,
                       ROI_STATE_NEW_ROI,
                       ROI_STATE_SETUP_ROI,
                       ROI_STATE_BUILD_TRIG_TABLES_0,
                       ROI_STATE_BUILD_TRIG_TABLES_1,
                       ROI_STATE_BUILD_TRIG_TABLES_2,
                       ROI_STATE_BUILD_TRIG_TABLES_OTHERS,
                       ROI_STATE_PROCESS_HITS,
                       ROI_STATE_FIND_LEGENDRE_MAX,
                       ROI_STATE_COMPUTE_Z0_AND_ALPHA_1,
                       ROI_STATE_COMPUTE_Z0_AND_ALPHA_2,
                       ROI_STATE_COMPUTE_Z0_AND_ALPHA_3);
  signal ROI_state : ROI_state_t := ROI_STATE_IDLE;

begin  -- architecture behavioral


  -------------------------------------------------------------------------------
  -- Construct our processor
  theta_slice_generator: for iTheta in THETA_BIN_COUNT-1 downto 0 generate
    -----------------------------------------------------------------------------
    --Generate the DSP pipeline for this theta slice
    compute_pipeline_1: entity work.compute_pipeline
      generic map (
        COORDINATE_WIDTH       => SCALED_COORDINATE_WIDTH,
        TRIG_WIDTH             => TRIG_WIDTH,
        BINNING_BITSHIFT_COUNT => BINNING_BITSHIFT_COUNT, 
        BIN_WIDTH              => R_BIN_COUNT_WIDTH)
      port map (
        clk              => clk,
        data_in_valid    => data_in_valid,
        data_in_last     => data_in_last,
        tube_x           => tube_x,
        cos              => cos(iTheta),
        tube_y           => tube_y,
        sin              => sin(iTheta),
        R                => R,
        R_pm             => R_pm,
        r0               => r0_lowest_bin,
--        data_out_valid   => data_out_valid(iTheta),
        data_out_last    => data_out_last(iTheta),
        bin_even         => rbin_even(iTheta),
        bin_even_valid   => incr_bin_even(iTheta),
--        bin_even_OOR     => rbin_even_OOR(iTheta),
        bin_odd          => rbin_odd(iTheta),
        bin_odd_valid    => incr_bin_odd(iTheta),
--        bin_odd_OOR      => rbin_odd_OOR(iTheta),
        primary_bin_even => primary_bin_even(iTheta));
    
    -----------------------------------------------------------------------------
    --Generate the rbins for this theta slice
--    incr_bin_even(iTheta) <= data_out_valid(iTheta) and (not rbin_even_OOR(iTheta));
--    incr_bin_odd(iTheta)  <= data_out_valid(iTheta) and (not  rbin_odd_OOR(iTheta));
    
    legendre_theta_slice_1: entity work.legendre_theta_slice
      generic map (
        R_BIN_COUNT_WIDTH => R_BIN_COUNT_WIDTH,
        COUNT_BIT_WIDTH   => COUNT_BIT_WIDTH)
      port map (
        clk            => clk,
--        reset_sync     => reset_legendre_matrix_delay,
        reset_sync     => reset_legendre_matrix,
        reset_async    => reset_legendre_matrix,
        incr_rbin_even => incr_bin_even(iTheta),
        rbin_even      => rbin_even(iTheta),
        incr_rbin_odd  => incr_bin_odd(iTheta),
        rbin_odd       => rbin_odd(iTheta),
        primary_bin_even => primary_bin_even(iTheta),
        max_value      => max_value((iTheta+1)*COUNT_BIT_WIDTH -1 downto iTheta*COUNT_BIT_WIDTH ),
        max_bin        => max_r_bin((iTheta+1)*R_BIN_COUNT_WIDTH -1 downto iTheta*R_BIN_COUNT_WIDTH));
  end generate theta_slice_generator;

--  reset_delay: process (clk) is
--  begin  -- process reset_delay
--    if clk'event and clk = '1' then  -- rising clock edge      
----      reset_legendre_matrix_delay <= reset_legendre_matrix;
--    end if;
--  end process reset_delay;
  

  -------------------------------------------------------------------------------
  -- The trig table to use to look up values to use in processing
  TrigTable_1: entity work.TrigTable
--    generic map (
--    THETA_STARTING_VALUE    => TRIG_TABLE_THETA_START, 
--    THETA_BIN_COUNT_WIDTH   => TRIG_TABLE_BIN_COUNT_WIDTH)
    port map (
      clk           => clk,
      theta         => theta_fill_value,
      cosine        => cosine_lookup,
      sine          => sine_lookup);


  

  -------------------------------------------------------------------------------
  -- Operate state machine that controls all processing of incomming hits

  fit_z0    <= std_logic_vector(computed_z0(SCALED_COORDINATE_WIDTH -1 downto 0));
  fit_alpha <= std_logic_vector(computed_alpha);
  fit_valid <= computed_fit_valid;
  hit_FIFO_read <= hit_FIFO_read_local;
  
  process_ROI_control: process (clk) is
  begin  -- process process_ROI_control
    if clk'event and clk = '1' then  -- rising clock edge
      process_ROI_old <= process_ROI;
      
      -- reset inactive pulses
      reset_legendre_matrix <= '0';
      error_pulse_ROI_NOT_FINISHED <= '0';

      if reset_sync = '1' then
        --reset
        ROI_state <= ROI_STATE_IDLE;
      elsif process_ROI = '1' and process_ROI_old = '0' then
        --NEW ROI, start processing
        ROI_state <= ROI_STATE_NEW_ROI;
        if ROI_STATE /= ROI_STATE_IDLE then
          error_pulse_ROI_NOT_FINISHED <= '1';
        end if;
      else
        -- ROI processing state machine
        case ROI_state is
          when ROI_STATE_IDLE =>
            -- do nothing
          when ROI_STATE_NEW_ROI =>
            -- capture the ROI parameters
            -- look-up legendre bounds
--            ROI_z0_seed <= z0_seed & COORDINATE_SCALING_BITS; --Multiplying by
            ROI_z0_seed <= z0_seed; --Multiplying by
                                                              --COORDINATE_SCALE_FACTOR
                                                              --(8)
            ROI_alpha_seed <= alpha_seed;
            ROI_state <= ROI_STATE_SETUP_ROI;            

            -- reset the legendre histogram           
            reset_legendre_matrix <= '1';

          when ROI_STATE_SETUP_ROI =>
            --Invalidate the old output
            computed_fit_valid <= '0';
            
            -- start filling in the trig tables
            ROI_state <= ROI_STATE_BUILD_TRIG_TABLES_0;
            
          when ROI_STATE_BUILD_TRIG_TABLES_0=>
            --Compute r0
            --r0 <= signed(ROI_z0_seed);
            sin_alpha <= sine_lookup(to_integer(unsigned(ROI_alpha_seed(BLOCKRAM_BITS_PER_THETA -1 downto 0))));
            ROI_state <= ROI_STATE_BUILD_TRIG_TABLES_1;
          when ROI_STATE_BUILD_TRIG_TABLES_1 =>
            r0_mult <= signed(ROI_z0_seed)*sin_alpha;
            

            ROI_state <= ROI_STATE_BUILD_TRIG_TABLES_2;
          when ROI_STATE_BUILD_TRIG_TABLES_2 =>
            --Compute r0
            -- Compute the value of the lowest r0 bin for computation
            r0 <= r0_mult - x"40000";                -- 128 * 2048 for shift by 128
                                                -- times the theta scale factor
                                                -- scaling 
            ROI_state <= ROI_STATE_BUILD_TRIG_TABLES_OTHERS;                        
          when ROI_STATE_BUILD_TRIG_TABLES_OTHERS =>
            -- begin reading trig tables
            --if (theta_fill_value_delay(1) + to_signed(TRIG_LOOKUP_LINE_COUNT,TRIG_TABLE_BIN_COUNT_WIDTH)) > theta_upper_bound_exclusive then
            
            if theta_fill_value_delay(1)  > theta_upper_bound_exclusive_other then  
              --We are done filling the tables, so move on.
              ROI_state <= ROI_STATE_PROCESS_HITS;

            end if;
          when ROI_STATE_PROCESS_HITS =>
            r0_lowest_bin <= r0(TRIG_WIDTH-1 + SCALED_COORDINATE_WIDTH-1 downto TRIG_WIDTH-1);

            -- enable the hit processor until all hits have hit the compute pipeline.
            if process_ROI = '0' and hit_FIFO_empty = '1' and hit_state = HIT_STATE_NEW_HIT_N then
              ROI_state <= ROI_STATE_FIND_LEGENDRE_MAX;
              max_search_start <= '1';              
            end if;
          when ROI_STATE_FIND_LEGENDRE_MAX =>
            max_search_start <= '0';
            --Wait in this state until all computations have finished and then
            --do the comparison tree to locate the maximum.
            --!!!!!TODO!!!!!  We probaly need one more theta position to allow
            --for no maximum (0 hits) and also a check to make sure the maximum value
            --is greater than N where N is the minimum number of hit tubes. 
            if Legendre_max_done = '1' then
              ROI_state <= ROI_STATE_COMPUTE_Z0_AND_ALPHA_1;
            end if;
            --Start the computation of alpha by adding Pi/2 to the theta value
            --of the lowest bin
            computed_alpha <= theta_lower_bound_inclusive + PI_OVER_TWO_MRAD;
            --Start the computation of z0 by getting the value of the lowest r
            --bin.
            computed_z0(SCALED_COORDINATE_WIDTH -1 downto 0)  <= r0_lowest_bin(SCALED_COORDINATE_WIDTH -1 downto 0);
            computed_z0(SCALED_COORDINATE_WIDTH + TRIG_WIDTH -1 downto SCALED_COORDINATE_WIDTH) <= (others => r0_lowest_bin(SCALED_COORDINATE_WIDTH-1));
            
          when ROI_STATE_COMPUTE_Z0_AND_ALPHA_1 =>

            -- Continue computing alpha by adding the bin of the fit max to it,
            -- this will now give alpha
            -- This comptuation is also done in the build_trig_tables process
            -- to get the sine value we need for computing z0
            computed_alpha <=  computed_alpha + signed(std_logic_vector(('0'&Legendre_max_bin_theta)));

            --Compute the full r from the r0_lowest_bin and the max r bin (must be
            --bitshifted by BINNING_BITSHIFT_COUNT)
            computed_z0(SCALED_COORDINATE_WIDTH + TRIG_WIDTH -1 -1 downto TRIG_WIDTH-1) <= signed(computed_z0(SCALED_COORDINATE_WIDTH -1 downto 0))
                                                                                           + signed(Legendre_max_bin_r & '0');
            computed_z0(TRIG_WIDTH -1 -1 downto 0) <= (others => '0');
            
            ROI_state <= ROI_STATE_COMPUTE_Z0_AND_ALPHA_2;
          when ROI_STATE_COMPUTE_Z0_AND_ALPHA_2 =>
            --Right now the trig table is looking up the sine value for alpha
            ROI_state <= ROI_STATE_COMPUTE_Z0_AND_ALPHA_3;
          when ROI_STATE_COMPUTE_Z0_AND_ALPHA_3 =>
            computed_fit_valid <= '1';
            
            --Get z0 by dividing r0 by sin(theta_fit)
            --           computed_z0 <= computed_z0/sine_lookup(to_integer(unsigned(computed_alpha(BLOCKRAM_BITS_PER_THETA -1 downto 0))));
            computed_z0 <= computed_z0; -- for new we are assuming the next
                                        -- stange can do the trig division
            ROI_state <= ROI_STATE_IDLE;
            
          when others => ROI_state <= ROI_STATE_IDLE;
        end case;
      end if;

    end if;
  end process process_ROI_control;


  -------------------------------------------------------------------------------
  -- Operate state machine that controls the processing and pipelining of the
  -- incomming hits.
  -- This only runs when the master ROI_state is in ROI_STATE_PROCESS_HITS
  process_hit: process (clk) is
  begin  -- process process_hit
    if clk'event and clk = '1' then  -- rising clock edge
      -- turn off pulses
      hit_FIFO_read_local <= '0';
      data_in_valid <= '0';
      
      if ROI_state = ROI_STATE_PROCESS_HITS then
        case hit_state is
          when HIT_STATE_IDLE =>
            -- Wait for a new hit to process and then read from the FIFO
            if hit_FIFO_empty = '0' then
              hit_FIFO_read_local <= '1';
              hit_state <= HIT_STATE_NEW_HIT_P;
            end if;
          when HIT_STATE_NEW_HIT_P =>
            --Wait for data valid from FIFO and process the +R hit
            if hit_FIFO_in_valid = '1' then

              tube_x <= signed(hit_FIFO_in((1 * SCALED_COORDINATE_WIDTH) - 1 downto (0 * SCALED_COORDINATE_WIDTH)));
              tube_y <= signed(hit_FIFO_in((2 * SCALED_COORDINATE_WIDTH) - 1 downto (1 * SCALED_COORDINATE_WIDTH)));
              R      <= signed(hit_FIFO_in((3 * SCALED_COORDINATE_WIDTH) - 1 downto (2 * SCALED_COORDINATE_WIDTH)));
              
              R_pm   <= '0'; -- do the +R case
              data_in_valid <= '1';
              data_in_last <= '0';
              
              --state machine
              hit_state <= HIT_STATE_NEW_HIT_N;
              --FIFO pipelining
              if hit_FIFO_empty = '0' then
                --Start reading the next sample if we can
                hit_FIFO_read_local <= '1';                
              end if;                           
            end if;
          when HIT_STATE_NEW_HIT_N =>
            -- Process the -R hit
            R_pm   <= '1'; -- do the-+R case
            data_in_valid <= '1';            
            data_in_last <= '0';
            if process_ROI = '0' and hit_FIFO_empty = '1' then
              data_in_last <= '1';
            end if;

            
            --state machine
            if hit_FIFO_read_local = '1' then
              -- the last state alread started the next FIFO read, so go to the
              -- HIT_STATE_NEW_HIT_P state to process it. (probably ready next
              -- clock, yay!)
              hit_state <= HIT_STATE_NEW_HIT_P;
            elsif hit_FIFO_empty = '0' then
              -- the FIFO has data in it, and the previous state hasn't started
              -- to read it, so we should.
              -- Do the read and move back to the HIT_STATE_NEW_HIT_P to
              -- process it.  Should only be one tick of idle in HIT_P. :-/
              hit_FIFO_read_local <= '1';              
              hit_state <= HIT_STATE_NEW_HIT_P;
            else
              -- The fifo is empty and we should move to the idle state to wait
              -- for a new hit
              hit_state <= HIT_STATE_IDLE;
            end if;
          when others => null;
        end case;
      end if;
    end if;
  end process process_hit;

  -------------------------------------------------------------------------------
  -- Process that waits for all processing to be done and then finds the
  -- legendre max
  Legendre_Final_Max_Search_1: entity work.Legendre_Final_Max_Search
    generic map (
      VALUE_BIT_WIDTH    => COUNT_BIT_WIDTH,
      POSITION_BIT_WIDTH => R_BIN_COUNT_WIDTH,
      ENTRIES_BIT_WIDTH  => THETA_BIN_COUNT_WIDTH)
    port map (
      clk          => clk,
      start_pulse  => max_search_start,
      values       => max_value,
      val_position => max_r_bin,
      done         => Legendre_max_done,
      max_value    => Legendre_max_bin_value,
      max_position => Legendre_max_bin_r,
      max_entry    => Legendre_max_bin_theta);

  -------------------------------------------------------------------------------
  -- Process that fills the cosine and sine tables for the Legendre transform
  -- This only operates when the master roi_state is in ROT_STATE_BUILD_TRIG_TABLES
  -- The master is notified that this is done when theta_fill_value is greater
  -- than theta_upper_bound_exclusive
  build_trig_tables: process (clk) is
  begin  -- process build_trig_tables
    if clk'event and clk = '1' then  -- rising clock edge

      -- Delay the theta look-up values since the results are delayed
--      theta_fill_value_delay(2) <= theta_fill_value_delay(1);
      theta_fill_value_delay(1) <= theta_fill_value_delay(0);
      theta_fill_value_delay(0) <= theta_fill_value;
--      theta_fill_bin_value_delay(2) <= theta_fill_bin_value_delay(1);
      theta_fill_bin_value_delay(1) <= theta_fill_bin_value_delay(0);
      theta_fill_bin_value_delay(0) <= theta_fill_bin_value;
      
      case ROI_STATE is
        when ROI_STATE_NEW_ROI =>
          ----------------------------------------------------------------------
          --Capture the trig values of the alpha angle
          theta_fill_value <= signed(alpha_seed);
          
        when ROI_STATE_SETUP_ROI =>
          ----------------------------------------------------------------------
          -- Begin pipeline of theta lookups

          --setup the processing of the trig tables (valid two clocks later)
          theta_fill_value     <= signed(ROI_alpha_seed) + to_signed(-1570,TRIG_TABLE_BIN_COUNT_WIDTH) - to_signed(THETA_BIN_COUNT/2,TRIG_TABLE_BIN_COUNT_WIDTH);
          theta_fill_bin_value <= (others => '0');
          
          -- compute the lower theta bount from alpha: theta = alpha - 90 - THETA_BIN_COUNT/2
          theta_lower_bound_inclusive <= signed(ROI_alpha_seed) + to_signed(-1570,TRIG_TABLE_BIN_COUNT_WIDTH) + to_signed(-THETA_BIN_COUNT/2,TRIG_TABLE_BIN_COUNT_WIDTH);
          -- alpha - 90 + THETA_BIN_COUNT/2
          theta_upper_bound_exclusive <= signed(ROI_alpha_seed) + to_signed(-1570,TRIG_TABLE_BIN_COUNT_WIDTH) + to_signed( THETA_BIN_COUNT/2,TRIG_TABLE_BIN_COUNT_WIDTH);
          theta_upper_bound_exclusive_other <= signed(ROI_alpha_seed) + to_signed(-1570,TRIG_TABLE_BIN_COUNT_WIDTH) + to_signed( THETA_BIN_COUNT/2,TRIG_TABLE_BIN_COUNT_WIDTH) - to_signed(TRIG_LOOKUP_LINE_COUNT,TRIG_TABLE_BIN_COUNT_WIDTH);


        when ROI_STATE_BUILD_TRIG_TABLES_1 | ROI_STATE_BUILD_TRIG_TABLES_2 | ROI_STATE_BUILD_TRIG_TABLES_OTHERS =>

          --Handle adding in new lookups into the trig tables
          for iThetaAlignment in TRIG_LOOKUP_LINE_COUNT -1 downto 0 loop
            if theta_fill_value(BLOCKRAM_BITS_PER_THETA-1 downto 0) = to_signed(iThetaAlignment,BLOCKRAM_BITS_PER_THETA) then
              --handle the part of the lookup that is early
              for iTheta in  TRIG_LOOKUP_LINE_COUNT -1 downto iThetaAlignment loop
                --buffer early words
                cos_buffer2(iTheta-iThetaAlignment) <= cosine_lookup(iTheta);
                sin_buffer2(iTheta-iThetaAlignment) <=   sine_lookup(iTheta);
                --output buffered early words
                cos_buffer(iTheta-iThetaAlignment) <= cos_buffer2(iTheta - iThetaAlignment);
                sin_buffer(iTheta-iThetaAlignment) <= sin_buffer2(iTheta - iThetaAlignment);
                
--                cos(THETA_BIN_COUNT -TRIG_LOOKUP_LINE_COUNT + iTheta) <= cos_buffer(iTheta);
--                sin(THETA_BIN_COUNT -TRIG_LOOKUP_LINE_COUNT + iTheta) <= sin_buffer(iTheta);
              end loop;  -- iTheta
              --handle the part of the word that is in time
              for iTheta in iThetaAlignment -1 downto 0 loop
                --Ex map sends iTheta = iThetaAlignment-1 to TRIG_LOOUP_LINE_COUNT-1
                cos_buffer(iTheta+(TRIG_LOOKUP_LINE_COUNT - iThetaAlignment)) <= cosine_lookup(iTheta);
                sin_buffer(iTheta+(TRIG_LOOKUP_LINE_COUNT - iThetaAlignment)) <= sine_lookup(iTheta);

--                cos(THETA_BIN_COUNT -TRIG_LOOKUP_LINE_COUNT + iTheta) <= cosine_lookup(iTheta);
--                sin(THETA_BIN_COUNT -TRIG_LOOKUP_LINE_COUNT + iTheta) <= sine_lookup(iTheta);
              end loop;  -- iTheta
            end if;
          end loop;  -- iThetaAlignment

          for iTheta in TRIG_LOOKUP_LINE_COUNT -1 downto 0 loop
            cos(THETA_BIN_COUNT -TRIG_LOOKUP_LINE_COUNT + iTheta) <= cos_buffer(iTheta);
            sin(THETA_BIN_COUNT -TRIG_LOOKUP_LINE_COUNT + iTheta) <= sin_buffer(iTheta);
          end loop;  -- iTheta
                    
          --Shift the lookup lines down 
          for iThetaLine in (THETA_BIN_COUNT/TRIG_LOOKUP_LINE_COUNT)-2 downto 0 loop
            for iTheta in TRIG_LOOKUP_LINE_COUNT -1 downto 0 loop
              cos(TRIG_LOOKUP_LINE_COUNT*iThetaLine + iTheta) <= cos(TRIG_LOOKUP_LINE_COUNT*(iThetaLine+1) + iTheta);
              sin(TRIG_LOOKUP_LINE_COUNT*iThetaLine + iTheta) <= sin(TRIG_LOOKUP_LINE_COUNT*(iThetaLine+1) + iTheta);
            end loop;  -- iTheta
          end loop;  -- iThetaLine

          --move forward in theta
          theta_fill_value <= theta_fill_value + to_signed(2**BLOCKRAM_BITS_PER_THETA,THETA_BIN_COUNT_WIDTH);
          
        when ROI_STATE_COMPUTE_Z0_AND_ALPHA_1 =>
          theta_fill_value <=  computed_alpha + signed(std_logic_vector(('0'&Legendre_max_bin_theta)));
        when others => null;
      end case;
    end if;
  end process build_trig_tables;
  
  
end architecture behavioral;
