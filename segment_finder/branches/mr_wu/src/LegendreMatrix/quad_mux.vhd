-------------------------------------------------------------------------------
-- Four bit mux
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity quad_mux is
  
  port (
    D_0 : in  std_logic;
    D_1 : in  std_logic;
    D_2 : in  std_logic;
    D_3 : in  std_logic;
    sel : in  std_logic_vector(1 downto 0);
    Q   : out std_logic);

end entity quad_mux;

architecture behavioral of quad_mux is

begin  -- architecture behavioral

  mux: process (D_0,D_1,D_2,D_3,sel) is
  begin  -- process mux
    case sel is
      when "00" =>
        Q <= D_0;
      when "01" =>
        Q <= D_1;
      when "10" =>
        Q <= D_2;        
      when "11" =>
        Q <= D_3;
      when others => null;
    end case;
  end process mux;

end architecture behavioral;
