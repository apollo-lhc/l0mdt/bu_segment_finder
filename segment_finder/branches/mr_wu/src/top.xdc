#create_clock -add -name sys_clk_pin -period 3.125 -waveform {0 1.5625} [get_ports clk_p[1]]

#give the reset of the Legendre matrix 4 320Mhz clock ticks to get reset.  There are about 16 before they need to be
#set_max_delay 12.5 -from [get_pins Legendre*/reset_legendre_matrix_reg/Q] -to [get_pins {Legendre_new_1/theta_slice_generator[*].legendre_theta_slice_1/r_bin_counters[*].bin_counter_1/bit_loop[*].local_count_latched_reg[*]/CLR}]
#set_max_delay 12.5 -from [Legendre_*/reset_legendre_matrix_reg/C] -to [get_pins {Legendre_new_1/theta_slice_generator[*].legendre_theta_slice_1/r_bin_counters[*].bin_counter_1/bit_loop[*].local_count_latched_reg[*]/CLR}]


set_property PACKAGE_PIN AT29 [get_ports {clk_p[1]}]
set_property PACKAGE_PIN AU29 [get_ports {clk_p[0]}]
set_property PACKAGE_PIN AL29 [get_ports {roi_trig_p[1]}]
set_property PACKAGE_PIN AL30 [get_ports {roi_trig_p[0]}]
set_property PACKAGE_PIN AM29 [get_ports {roi_data_p[1]}]
set_property PACKAGE_PIN AN29 [get_ports {roi_data_p[0]}]
set_property PACKAGE_PIN AM30 [get_ports {hit_FIFO_in_p[1]}]
set_property PACKAGE_PIN AM31 [get_ports {hit_FIFO_in_p[0]}]
set_property PACKAGE_PIN AP28 [get_ports {hit_FIFO_in_valid_p[1]}]
set_property PACKAGE_PIN AP29 [get_ports {hit_FIFO_in_valid_p[0]}]
set_property PACKAGE_PIN AP30 [get_ports {hit_FIFO_empty_p[1]}]
set_property PACKAGE_PIN AR31 [get_ports {hit_FIFO_empty_p[0]}]
set_property PACKAGE_PIN AR32 [get_ports {hit_FIFO_read_p[1]}]
set_property PACKAGE_PIN AR33 [get_ports {hit_FIFO_read_p[0]}]
set_property PACKAGE_PIN AN32 [get_ports {fit_valid_p[1]}]
set_property PACKAGE_PIN AP33 [get_ports {fit_valid_p[0]}]
set_property PACKAGE_PIN AT32 [get_ports {fit_data_p[1]}]
set_property PACKAGE_PIN AU32 [get_ports {fit_data_p[0]}]

set_property PACKAGE_PIN AV31 [get_ports {reset_p[1]}]
set_property PACKAGE_PIN AV32 [get_ports {reset_p[0]}]
