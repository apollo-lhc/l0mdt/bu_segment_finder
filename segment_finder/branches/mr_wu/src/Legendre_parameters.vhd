----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
--Fundamental trig parameters
use work.Trig_Lookup.all;

package Legendre_IO is
  constant R_BIN_COUNT_WIDTH      : integer := 7;
  constant COUNT_BIT_WIDTH        : integer := 4;
  constant THETA_BIN_COUNT_WIDTH  : integer := 7;

  constant COORDINATE_WIDTH       : integer := 12;  -- 11 + 1 sign bit
  constant COORDINATE_SCALE_FACTOR_BITS : integer := 3;
  constant COORDINATE_SCALE_FACTOR : integer := 2**COORDINATE_SCALE_FACTOR_BITS;
  constant SCALED_COORDINATE_WIDTH : integer := COORDINATE_WIDTH+COORDINATE_SCALE_FACTOR_BITS;
  constant COORDINATE_SCALING_BITS : std_logic_vector(COORDINATE_SCALE_FACTOR_BITS -1 downto 0) := (others => '0');
  
  constant TRIG_SCALING_FACTOR    : integer := 2**(TRIG_WIDTH-1);
  
  type trig_array_t is array (integer range <>) of signed(TRIG_WIDTH-1 downto 0);
  type coordinate_array_t is array (integer range <>) of signed(COORDINATE_WIDTH-1 downto 0);  

end package Legendre_IO;
