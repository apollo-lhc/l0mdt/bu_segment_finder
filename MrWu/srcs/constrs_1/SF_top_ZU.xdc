create_clock -period 2.2 -name clk [get_pins {i_buf_clk/O}]
create_clock -period 2.2 -name clk_gated0 [get_pins {g_clk_gated[0].i_BUFGCE/O}]
create_clock -period 2.2 -name clk_gated1 [get_pins {g_clk_gated[1].i_BUFGCE/O}]
create_clock -period 2.2 -name clk_gated2 [get_pins {g_clk_gated[2].i_BUFGCE/O}]
create_clock -period 2.2 -name clk_gated3 [get_pins {g_clk_gated[3].i_BUFGCE/O}]
create_clock -period 2.2 -name clk_gated4 [get_pins {g_clk_gated[4].i_BUFGCE/O}]
create_clock -period 2.2 -name clk_gated5 [get_pins {g_clk_gated[5].i_BUFGCE/O}]
create_clock -period 2.2 -name clk_gated6 [get_pins {g_clk_gated[6].i_BUFGCE/O}]
create_clock -period 2.2 -name clk_gated7 [get_pins {g_clk_gated[7].i_BUFGCE/O}]
create_clock -period 4.4 -name sysclk [get_pins {i_buf_sysclk/O}]
create_clock -period 20.00 -name freerun_clk [get_pins {i_buf_free_run_clk/O}]
set_property PACKAGE_PIN G16 [get_ports sysclkp_in]
set_property PACKAGE_PIN F15 [get_ports sysclkn_in]
set_property package_pin AH11 [get_ports gtrefclkn_in[0]]
set_property package_pin AH12 [get_ports gtrefclkp_in[0]]
set_property package_pin Y11 [get_ports gtrefclkn_in[1]]
set_property package_pin Y12 [get_ports gtrefclkp_in[1]]
set_property package_pin AB35 [get_ports gtrefclkn_in[2]]
set_property package_pin AB34 [get_ports gtrefclkp_in[2]]
set_property package_pin W33 [get_ports gtrefclkn_in[3]]
set_property package_pin W32 [get_ports gtrefclkp_in[3]]
set_property package_pin R33 [get_ports gtrefclkn_in[4]]
set_property package_pin R32 [get_ports gtrefclkp_in[4]]
set_property package_pin L33 [get_ports gtrefclkn_in[5]]
set_property package_pin L32 [get_ports gtrefclkp_in[5]]
set_property IOSTANDARD LVDS [get_ports sysclkp_in]
set_property DIFF_TERM_ADV TERM_100 [get_ports sysclkp_in]
create_clock -name mgtrefclk0 -period 10.0 [get_ports gtrefclkp_in[0]]
create_clock -name mgtrefclk1 -period 10.0 [get_ports gtrefclkp_in[1]]
create_clock -name mgtrefclk2 -period 6.206 [get_ports gtrefclkp_in[2]]
create_clock -name mgtrefclk3 -period 6.206 [get_ports gtrefclkp_in[3]]
create_clock -name mgtrefclk4 -period 6.206 [get_ports gtrefclkp_in[4]]
create_clock -name mgtrefclk5 -period 6.206 [get_ports gtrefclkp_in[5]]
set_clock_groups -asynchronous -group [get_clocks clk]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks mgtrefclk0]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks mgtrefclk1]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks mgtrefclk2]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks mgtrefclk3]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks mgtrefclk4]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks mgtrefclk5]
set_clock_groups -asynchronous -group [get_clocks freerun_clk]
set_clock_groups -asynchronous -group [get_clocks sysclk]
set_clock_groups -asynchronous -group [get_clocks clk_gated0]
set_clock_groups -asynchronous -group [get_clocks clk_gated1]
set_clock_groups -asynchronous -group [get_clocks clk_gated2]
set_clock_groups -asynchronous -group [get_clocks clk_gated3]
set_clock_groups -asynchronous -group [get_clocks clk_gated4]
set_clock_groups -asynchronous -group [get_clocks clk_gated5]
set_clock_groups -asynchronous -group [get_clocks clk_gated6]
set_clock_groups -asynchronous -group [get_clocks clk_gated7]
set_multicycle_path 3 -setup -from [get_pins */*/*/r_e_reg[*]/C] -to [get_pins */g_mux16_r*/*/dout_reg/D]
set_multicycle_path 3 -setup -from [get_pins */*/*/r_o_reg[*]/C] -to [get_pins */g_mux16_r*/*/dout_reg/D]
set_multicycle_path 3 -setup -from [get_pins */g_mux16_r*/*/dout_reg/C] -to [get_pins */g_mux16_DSP*/dout_reg/D]
set_multicycle_path 2 -hold -from [get_pins */*/*/r_e_reg[*]/C] -to [get_pins */g_mux16_r*/*/dout_reg/D]
set_multicycle_path 2 -hold -from [get_pins */*/*/r_o_reg[*]/C] -to [get_pins */g_mux16_r*/*/dout_reg/D]
set_multicycle_path 2 -hold -from [get_pins */g_mux16_r*/*/dout_reg/C] -to [get_pins */g_mux16_DSP*/dout_reg/D]
set_multicycle_path 2 -setup -from [get_pins */sine_din_reg[*]/C]
set_multicycle_path 1 -hold -from [get_pins */sine_din_reg[*]/C]
set_false_path -from [get_cells */load_uRAM_reg -include_replicated_objects]
set_false_path -from [get_cells */r_offset_reg[*] -include_replicated_objects]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *bit_synchronizer*inst/i_in_meta_reg}]
set_false_path -to [get_pins -filter {REF_PIN_NAME=~*D} -of_objects [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_meta*}]]
set_false_path -to [get_pins -filter {REF_PIN_NAME=~*PRE} -of_objects [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_meta*}]]
set_false_path -to [get_pins -filter {REF_PIN_NAME=~*PRE} -of_objects [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_sync1*}]]
set_false_path -to [get_pins -filter {REF_PIN_NAME=~*PRE} -of_objects [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_sync2*}]]
set_false_path -to [get_pins -filter {REF_PIN_NAME=~*PRE} -of_objects [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_sync3*}]]
set_false_path -to [get_pins -filter {REF_PIN_NAME=~*PRE} -of_objects [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_out*}]]


set_false_path -to [get_cells -hierarchical -filter {NAME =~ *gtwiz_userclk_tx_inst/*gtwiz_userclk_tx_active_*_reg}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *gtwiz_userclk_rx_inst/*gtwiz_userclk_rx_active_*_reg}]
