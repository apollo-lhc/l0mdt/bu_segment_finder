----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/05/2018 12:33:15 PM
-- Design Name: 
-- Module Name: dual_theta_slice - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity sine_rom_init is
    Port ( clk : in STD_LOGIC;
           MMCM_locked : in STD_LOGIC;
           fifo_busy : in STD_LOGIC;
					 dout : out STD_LOGIC_VECTOR (12 downto 0);
					 addr : out STD_LOGIC_VECTOR (11 downto 0);
           dout_valid : out STD_LOGIC;
           load_uRAM : out STD_LOGIC;
           we_uRAM : out STD_LOGIC;
           init_done : out STD_LOGIC
           );
end sine_rom_init;

architecture Behavioral of sine_rom_init is
COMPONENT sine_ROM
  PORT (
    clka : IN STD_LOGIC;
    addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(12 DOWNTO 0)
  );
END COMPONENT;
signal douta : std_logic_vector(12 downto 0) := (others => '0');
signal addra : std_logic_vector(11 downto 0) := (others => '0');
signal addr_i : std_logic_vector(11 downto 0) := (others => '0');
signal dout_valid_i : std_logic;
--signal init_done_i : std_logic;
signal load_uRAM_i : std_logic := '0';
signal end_of_load_uRAM : std_logic := '0';
signal next_word : std_logic;
signal last_word : std_logic;
--signal end_of_load_uRAM_dl : std_logic;
signal fifo_busy_q : std_logic;
signal en_cntr : std_logic;
signal cntr : std_logic_vector(2 downto 0) := (others => '0');

begin
i_sine_rom : sine_ROM
	PORT MAP (
		clka => clk,
		addra => addra,
		douta => douta
	);
process(clk, MMCM_locked)
begin
	if(MMCM_locked = '0')then
		fifo_busy_q <= '0';
		load_uRAM <= '0';
		load_uRAM_i <= '0';
		en_cntr <= '0';
		last_word <= '0';
		init_done <= '0';
  elsif(clk'event and clk = '1')then
		fifo_busy_q <= fifo_busy;
		load_uRAM <= load_uRAM_i;
		if(fifo_busy = '0' and fifo_busy_q = '1')then
			en_cntr <= '1';
		elsif(next_word = '1' and or_reduce(addra) = '0')then
			en_cntr <= '0';
		end if;
		last_word <= next_word and not or_reduce(addra);
		if(fifo_busy = '1' and fifo_busy_q = '0')then
			load_uRAM_i <= '1';
		elsif(end_of_load_uRAM = '1')then
			load_uRAM_i <= '0';
		end if;
		if(end_of_load_uRAM = '1')then
			init_done <= '1';
		end if;
  end if;
end process;
process(clk)
begin
  if(clk'event and clk = '1')then
		dout <= douta;
		if(en_cntr = '0')then
			addra <= (others => '0');
		elsif(cntr = "101")then
			addra <= addra + x"f81";
		elsif(cntr(2) = '0')then
		  if(cntr(1 downto 0) = "11")then
        addra <= addra + x"050";
		  else
        addra <= addra + x"010";
			end if;
		end if;
		if(en_cntr = '0' or (cntr(2) = '1' and cntr(0) = '1'))then
			cntr <= (others => '0');
		else
			cntr <= cntr + 1;
		end if;
		if(cntr = "000")then
			addr_i <= addra+x"0c0";
		end if;
		addr <= addr_i;
		if(cntr = "000" or en_cntr = '0')then
			dout_valid_i <= '0';
		else
			dout_valid_i <= '1';
		end if;
		dout_valid <= dout_valid_i;
		next_word <= cntr(2) and cntr(0);
		we_uRAM <= next_word;
  end if;
end process;
i_end_of_load_uRAM : LUT_delay generic map(N => 17)
port map (
   Q => end_of_load_uRAM,   -- 1-bit output: SRL Data
   CE => '1',   -- 1-bit input: Clock enable
   CLK => clk, -- 1-bit input: Clock
   D => last_word     -- 1-bit input: SRL Data
);
--i_init_done : LUT_delay generic map(N => 17)
--port map (
--   Q => end_of_load_uRAM_dl,     -- 1-bit output: SRL Data
--   CE => '1',   -- 1-bit input: Clock enable
--   CLK => clk, -- 1-bit input: Clock
--   D => end_of_load_uRAM     -- 1-bit input: SRL Data
--);
end Behavioral;
