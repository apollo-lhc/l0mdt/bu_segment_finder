----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/29/2018 01:25:36 PM
-- Design Name: 
-- Module Name: cosine_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;
Library xpm;
use xpm.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity seg_finder is
  Port (
			clk : in std_logic;
			sysclk : in std_logic;
			MMCM_locked : in std_logic;
			data_valid_in : in std_logic;
			fifo_reset : in STD_LOGIC;
			fifo_busy : out STD_LOGIC;
			reset_RAM_in : in STD_LOGIC;
			we_uRAM_in : in STD_LOGIC;
			load_uRAM_in : in STD_LOGIC;
--			sine_data_valid : in STD_LOGIC;
			sine_init_data: in STD_LOGIC_VECTOR (12 downto 0);
			theta_in: in STD_LOGIC_VECTOR (22 downto 0);-- this is in fact theta - phi0, it must be >= -0.96 radian  and < 0.96 radian. bit 22 is sign bit, bit 21 = 0.512 radian
			r_in : in STD_LOGIC_VECTOR (22 downto 0);-- positive number r0(5) = 0.25mm
			R_drift_in : in STD_LOGIC_VECTOR (9 downto 0);-- R_drift(4) = 0.25mm
			seg_pm : out STD_LOGIC_VECTOR (48 downto 0);-- 22 bit theta & 22 bit r & 4 bit count
			ready : out STD_LOGIC
		);
end seg_finder;

architecture Behavioral of seg_finder is
COMPONENT FIFO72X512
  PORT (
    srst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;
component cosine_gen
    Port ( clk : in STD_LOGIC;
           theta_in: in STD_LOGIC_VECTOR (22 downto 0);-- this is in fact theta - phi0, it must be >= -0.96 radian  and < 0.96 radian
           new_hit : in STD_LOGIC;-- valid hit present
           we_uRAM : in STD_LOGIC;
					 din : in STD_LOGIC_VECTOR (71 downto 0);
           cosine_h : out array33X21;
           cosine_l : out array33X11
           );
end component;
component r_calc
    Port ( clk : in STD_LOGIC;
           new_hit : in std_logic;
           reset : in std_logic;
           r0 : in STD_LOGIC_VECTOR (22 downto 0);-- positive number r0(5) = 0.25mm
           R_drift : in STD_LOGIC_VECTOR (9 downto 0);-- R_drift(4) = 0.25mm
           cosine_h : in STD_LOGIC_VECTOR (20 downto 0);-- negative number without sign bit(all 0 is -1.0)
           cosine_l : in STD_LOGIC_VECTOR (10 downto 0);-- number with sign bit
           r_offset : in STD_LOGIC_VECTOR (22 downto 0);-- positive number r0(5) = 0.25mm
           r_low_out : out STD_LOGIC_VECTOR (16 downto 0);
           r_high_out : out STD_LOGIC_VECTOR (16 downto 0));
end component;
component dual_slice
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           new_hit : in STD_LOGIC;
           r : in STD_LOGIC_VECTOR (16 downto 0);
           r_h : in STD_LOGIC_VECTOR (16 downto 0);
           r_e0 : out STD_LOGIC_VECTOR (7 downto 0);
           r_e1 : out STD_LOGIC_VECTOR (7 downto 0);
           r_o0 : out STD_LOGIC_VECTOR (7 downto 0);
           r_o1 : out STD_LOGIC_VECTOR (7 downto 0);
					 max : out STD_LOGIC_VECTOR (5 downto 0));
end component;
component MUX16
    Port ( clk : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (15 downto 0);
           s : in STD_LOGIC_VECTOR (3 downto 0);
           dout : out STD_LOGIC);
end component;
component MUX16X8
    Port ( clk : in STD_LOGIC;
           r_e : in array8X8;
           r_o : in array8X8;
           s : in STD_LOGIC_VECTOR (3 downto 0);
           dout : out STD_LOGIC_VECTOR (11 downto 0));
end component;
signal sine_din : std_logic_vector(71 downto 0) := (others => '0');
--signal load_uRAM : std_logic;
signal we_uRAM : std_logic;
signal cosine_h : array33X21;
signal cosine_l : array33X11;
signal r_low : array33X17;
signal r_high : array33X17;
signal max : array64X6;
signal r_e : array16X8X8;
signal r_o : array16X8X8;
signal MUX16X8_dout : array16X12;
signal maxa : array32X7;
signal maxb : array16X8;
signal maxc : array8X5;
signal maxd : array4X6;
signal maxe : array2X7;
signal maxf: std_logic_vector(7 downto 0) := (others => '0');
signal mux16_din : array12X16;
signal fifo_din : std_logic_vector(71 downto 0) := (others => '0');
signal fifo_dout : std_logic_vector(71 downto 0) := (others => '0');
signal fifo_dout_valid : std_logic := '0';
signal fifo_rd_rst_busy : std_logic := '0';
signal fifo_wr_rst_busy : std_logic := '0';
signal fifo_wr_rst_busy_q : std_logic := '0';
signal r0 : std_logic_vector(22 downto 0) := (others => '0');
signal r0_dl : std_logic_vector(22 downto 0) := (others => '0');
signal r_offset : std_logic_vector(22 downto 0) := (others => '0');
signal theta : std_logic_vector(22 downto 0) := (others => '0');
signal R_drift : std_logic_vector(9 downto 0) := (others => '0');
signal R_drift_dl : std_logic_vector(9 downto 0) := (others => '0');
signal fifo_reset_done : std_logic := '1';
signal fifo_empty : std_logic := '1';
signal fifo_rd_en : std_logic := '0';
signal load_ROI : std_logic := '0';
signal got_ROI : std_logic := '0';
signal RAM_reset_done : std_logic := '0';
signal ce_DSP_AB : std_logic := '0';
signal busy_cntr : std_logic_vector(5 downto 0) := (others => '0');
signal reset_RAM : std_logic := '1';
signal reset_RAM_cycle : std_logic := '0';
signal reset_RAM_cycle_dl : std_logic_vector(1 downto 0) := (others => '0');
signal new_hit : std_logic := '0';
signal new_hit_dl_q : std_logic_vector(3 downto 0) := (others => '0');
signal new_hit_dl : std_logic := '0';
signal DSP_AB : std_logic_vector(47 downto 0) := (others => '0');
signal DSP_C : std_logic_vector(47 downto 0) := (others => '0');
signal DSP_P : std_logic_vector(47 downto 0) := (others => '0');

begin
fifo_busy <= fifo_wr_rst_busy;
i_reset : xpm_cdc_pulse
   generic map (
      DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
      INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
      RST_USED => 1,       -- DECIMAL; 0=no reset, 1=implement reset
      SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
   )
   port map (
      dest_pulse => reset_RAM, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse
                                -- transfer is correctly initiated on src_pulse input. This output is
                                -- combinatorial unless REG_OUTPUT is set to 1.

      dest_clk => clk,     -- 1-bit input: Destination clock.
      dest_rst => fifo_rd_rst_busy,     -- 1-bit input: optional; required when RST_USED = 1
      src_clk => sysclk,       -- 1-bit input: Source clock.
      src_pulse => reset_RAM_in,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the
                                -- destination clock domain. The minimum gap between each pulse transfer must
                                -- be at the minimum 2*(larger(src_clk period, dest_clk period)). This is
                                -- measured between the falling edge of a src_pulse to the rising edge of the
                                -- next src_pulse. This minimum gap will guarantee that each rising edge of
                                -- src_pulse will generate a pulse the size of one dest_clk period in the
                                -- destination clock domain. When RST_USED = 1, pulse transfers will not be
                                -- guaranteed while src_rst and/or dest_rst are asserted.

      src_rst => fifo_wr_rst_busy        -- 1-bit input: optional; required when RST_USED = 1
   );
process(sysclk, MMCM_locked)
begin
	if(MMCM_locked = '0')then
		fifo_reset_done <= '0';
		fifo_wr_rst_busy_q <= '0';
		ready <= '0';
		busy_cntr <= (others => '1');
	elsif(sysclk'event and sysclk = '1')then
		fifo_wr_rst_busy_q <= fifo_wr_rst_busy;
		if(fifo_wr_rst_busy = '0' and fifo_wr_rst_busy_q = '1')then
			fifo_reset_done <= '1';
		end if;
		if(reset_RAM_in = '1' or data_valid_in = '1' or load_uRAM_in = '1' or fifo_reset_done = '0')then
			ready <= '0';
		elsif(or_reduce(busy_cntr) = '0')then
			ready <= '1';
		end if;
		if(reset_RAM_in = '1')then
			busy_cntr <= "100101";
		elsif(data_valid_in = '1')then
			busy_cntr <= "001101";
		elsif(or_reduce(busy_cntr) = '1')then
			busy_cntr <= busy_cntr - 1;
		end if;
	end if;
end process;
fifo_din(22 downto 0) <= theta_in;
fifo_din(32 downto 23) <= R_drift_in;
fifo_din(55 downto 33) <= r_in;
fifo_din(68 downto 56) <= sine_init_data;
--fifo_din(69) <= sine_data_valid;
fifo_din(70) <= we_uRAM_in;
--fifo_din(71) <= '0';
fifo_din(71) <= load_uRAM_in;
i_fifo_in : FIFO72X512
  PORT MAP (
    srst => fifo_reset,
    wr_clk => sysclk,
    rd_clk => clk,
    din => fifo_din,
    wr_en => data_valid_in,
    rd_en => fifo_rd_en,
    dout => fifo_dout,
    full => open,
    empty => fifo_empty,
    wr_rst_busy => fifo_wr_rst_busy,
    rd_rst_busy => fifo_rd_rst_busy
  );
process(clk, fifo_reset_done)
begin
	if(fifo_reset_done = '0')then
		reset_RAM_cycle <= '0';
		fifo_rd_en <= '0';
		fifo_dout_valid <= '0';
		new_hit <= '0';
		got_ROI <= '0';
		load_ROI <= '0';
		RAM_reset_done <= '0';
--		load_uRAM <= '0';
		we_uRAM <= '0';
  elsif(clk'event and clk = '1')then
		if(reset_RAM = '1')then
			reset_RAM_cycle <= '1';
		elsif(and_reduce(r_offset(11 downto 6)) = '1')then
			reset_RAM_cycle <= '0';
		end if;
		fifo_rd_en <= not fifo_empty and not fifo_rd_en;
		fifo_dout_valid <= fifo_rd_en;
		new_hit <= fifo_dout_valid and got_ROI;
		got_ROI <= not reset_RAM_cycle and (load_ROI or got_ROI);
		load_ROI <= fifo_dout_valid and RAM_reset_done;
		RAM_reset_done <= reset_RAM or (RAM_reset_done and not load_ROI);
--		if(fifo_dout_valid = '1')then
--			load_uRAM <= fifo_dout(71);
--		end if;
		we_uRAM <= fifo_dout(70) and fifo_dout_valid;
	end if;
end process;
process(clk)
begin
  if(clk'event and clk = '1')then
		if(reset_RAM_cycle = '1')then
			r_offset(11 downto 6) <= r_offset(11 downto 6) + 1;
		elsif(load_ROI = '1')then
			r_offset <= fifo_dout(55 downto 33);
		end if;
		if(fifo_dout_valid = '1' and fifo_dout(71) = '1' )then
			sine_din(64 downto 0) <= fifo_dout(68 downto 56) & sine_din(64 downto 13);
		end if;
		theta <= fifo_dout(22 downto 0);
		R_drift <= fifo_dout(32 downto 23);
		r0 <= fifo_dout(55 downto 33);
	end if;
end process;
i_cosine_gen: cosine_gen
  port map(
    clk => clk,
    theta_in => theta,
    new_hit => new_hit,
		we_uRAM => we_uRAM,
		din => sine_din,
    cosine_h => cosine_h,
    cosine_l => cosine_l
    );
g_r_calc : for i in 0 to 32 generate
	i_r_calc: r_calc
		port map(
			clk => clk,
			reset => reset_RAM_cycle_dl(0),
			new_hit => new_hit_dl,
			r0 => r0_dl,
			R_drift => R_drift_dl,
			cosine_h => cosine_h(i),
			cosine_l => cosine_l(i),
			r_offset => r_offset,
			r_low_out => r_low(i),
			r_high_out => r_high(i)
		);
end generate g_r_calc;
g_dual_slice : for i in 0 to 31 generate
	i_dual_slice: dual_slice
		port map(
			clk => clk,
			reset => reset_RAM_cycle_dl(1),
			new_hit => new_hit_dl_q(2),
			r => r_low(i),
			r_h => r_low(i+1),
			r_e0 => r_e(i/4)((2*i) mod 8),
			r_e1 => r_e(i/4)((2*i) mod 8 + 1),
			r_o0 => r_o(i/4)((2*i) mod 8),
			r_o1 => r_o(i/4)((2*i) mod 8 + 1),
			max => max(i)
		);
	i_dual_sliceH: dual_slice
		port map(
			clk => clk,
			reset => reset_RAM_cycle_dl(1),
			new_hit => new_hit_dl_q(3),
			r => r_high(i),
			r_h => r_high(i+1),
			r_e0 => r_e(i/4+8)((2*i) mod 8),
			r_e1 => r_e(i/4+8)((2*i) mod 8 + 1),
			r_o0 => r_o(i/4+8)((2*i) mod 8),
			r_o1 => r_o(i/4+8)((2*i) mod 8 + 1),
			max => max(i+32)
		);
end generate g_dual_slice;
i_reset_RAM_cycle_dl0 : LUT_delay generic map(N => 3)
port map (
   Q => reset_RAM_cycle_dl(0),     -- 1-bit output: SRL Data
   CE => '1',   -- 1-bit input: Clock enable
   CLK => clk, -- 1-bit input: Clock
   D => reset_RAM_cycle     -- 1-bit input: SRL Data
);
i_reset_RAM_cycle_dl1 : LUT_delay generic map(N => 6)
port map (
   Q => reset_RAM_cycle_dl(1),     -- 1-bit output: SRL Data
   CE => '1',   -- 1-bit input: Clock enable
   CLK => clk, -- 1-bit input: Clock
   D => reset_RAM_cycle     -- 1-bit input: SRL Data
);
i_new_hit_dl : LUT_delay generic map(N => 14)
port map (
   Q => new_hit_dl,     -- 1-bit output: SRL Data
   CE => '1',   -- 1-bit input: Clock enable
   CLK => clk, -- 1-bit input: Clock
   D => new_hit     -- 1-bit input: SRL Data
);
i_R_drift_dl : LUT_delayV generic map(N => 15, V => 10)
  port map (
     Q => R_drift_dl,     -- 1-bit output: SRL Data
     CE => '1',   -- 1-bit input: Clock enable
     CLK => clk, -- 1-bit input: Clock
     D => R_drift     -- 1-bit input: SRL Data
  );
i_r0_dl_l : LUT_delayV generic map(N => 10, V => 6)
  port map (
     Q => r0_dl(5 downto 0),     -- 1-bit output: SRL Data
     CE => '1',   -- 1-bit input: Clock enable
     CLK => clk, -- 1-bit input: Clock
     D => r0(5 downto 0)     -- 1-bit input: SRL Data
  );
i_r0_dl_h : LUT_delayV generic map(N => 11, V => 17)
  port map (
     Q => r0_dl(22 downto 6),     -- 1-bit output: SRL Data
     CE => '1',   -- 1-bit input: Clock enable
     CLK => clk, -- 1-bit input: Clock
     D => r0(22 downto 6)     -- 1-bit input: SRL Data
  );
g_mux16_r : for i in 0 to 15 generate
  i_mux16_r : MUX16X8
    port map(clk => clk, r_e => r_e(i), r_o => r_o(i), s => maxb(i)(7 downto 4), dout => MUX16X8_dout(i));
end generate g_mux16_r;
g_mux16_DSP_r : for i in 0 to 8 generate
  i_mux16 : MUX16
    port map(clk => clk, din => mux16_din(i), s => maxf(7 downto 4), dout => DSP_AB(i+29)); -- 9 r bins
  g_mux16_din_j : for j in 0 to 15 generate
    mux16_din(i)(j) <= MUX16X8_dout(j)(i);
  end generate g_mux16_din_j;
end generate g_mux16_DSP_r;
g_mux16_DSP_t : for i in 0 to 2 generate
  i_mux16 : MUX16
    port map(clk => clk, din => mux16_din(i+9), s => maxf(7 downto 4), dout => DSP_AB(i+12)); -- three theta LSB bins
  g_mux16_din_j : for j in 0 to 15 generate
    mux16_din(i+9)(j) <= MUX16X8_dout(j)(i+9);
  end generate g_mux16_din_j;
end generate g_mux16_DSP_t;
process(clk)
begin
  if(clk'event and clk = '1')then
    seg_pm(3 downto 0) <= maxf(3 downto 0);
    DSP_AB(18 downto 15) <= maxf(7 downto 4); -- four MSB theta bins
		new_hit_dl_q <= new_hit_dl_q(2 downto 0) & new_hit_dl;
		for i in 0 to 31 loop
			if(max(2*i+1)(3 downto 0) > max(2*i)(3 downto 0))then
				maxa(i) <= '1' & max(2*i+1);
			else
				maxa(i) <= '0' & max(2*i);
			end if;
		end loop;
		for i in 0 to 15 loop
			if(maxa(2*i+1)(3 downto 0) > maxa(2*i)(3 downto 0))then
				maxb(i) <= '1' & maxa(2*i+1);
			else
				maxb(i) <= '0' & maxa(2*i);
			end if;
		end loop;
		for i in 0 to 7 loop
      if(maxb(2*i+1)(3 downto 0) > maxb(2*i)(3 downto 0))then
        maxc(i) <= '1' & maxb(2*i+1)(3 downto 0);
      else
        maxc(i) <= '0' & maxb(2*i)(3 downto 0);
      end if;
    end loop;
    for i in 0 to 3 loop
      if(maxc(2*i+1)(3 downto 0) > maxc(2*i)(3 downto 0))then
        maxd(i) <= '1' & maxc(2*i+1);
      else
        maxd(i) <= '0' & maxc(2*i);
      end if;
    end loop;
    for i in 0 to 1 loop
      if(maxd(2*i+1)(3 downto 0) > maxd(2*i)(3 downto 0))then
        maxe(i) <= '1' & maxd(2*i+1);
      else
        maxe(i) <= '0' & maxd(2*i);
      end if;
    end loop;
    if(maxe(1)(3 downto 0) > maxe(0)(3 downto 0))then
      maxf <= '1' & maxe(1);
    else
      maxf <= '0' & maxe(0);
    end if;
  end if;
end process;
i_seg_pm : DSP48E2
   generic map (
      -- Feature Control Attributes: Data Path Selection
      AMULTSEL => "A",                   -- Selects A input to multiplier (A, AD)
      A_INPUT => "DIRECT",               -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
      BMULTSEL => "B",                   -- Selects B input to multiplier (AD, B)
      B_INPUT => "DIRECT",               -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
      PREADDINSEL => "A",                -- Selects input to pre-adder (A, B)
      RND => X"000000FD0000",            -- Rounding Constant
      USE_MULT => "NONE",            -- Select multiplier usage (DYNAMIC, MULTIPLY, NONE)
      USE_SIMD => "TWO24",               -- SIMD selection (FOUR12, ONE48, TWO24)
      USE_WIDEXOR => "FALSE",            -- Use the Wide XOR function (FALSE, TRUE)
      XORSIMD => "XOR24_48_96",          -- Mode of operation for the Wide XOR (XOR12, XOR24_48_96)
      -- Pattern Detector Attributes: Pattern Detection Configuration
      AUTORESET_PATDET => "NO_RESET",    -- NO_RESET, RESET_MATCH, RESET_NOT_MATCH
      AUTORESET_PRIORITY => "RESET",     -- Priority of AUTORESET vs. CEP (CEP, RESET).
      MASK => X"3fffffffffff",           -- 48-bit mask value for pattern detect (1=ignore)
      PATTERN => X"000000000000",        -- 48-bit pattern match for pattern detect
      SEL_MASK => "MASK",                -- C, MASK, ROUNDING_MODE1, ROUNDING_MODE2
      SEL_PATTERN => "PATTERN",          -- Select pattern value (C, PATTERN)
      USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect (NO_PATDET, PATDET)
      -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
      IS_ALUMODE_INVERTED => "0000",     -- Optional inversion for ALUMODE
      IS_CARRYIN_INVERTED => '0',        -- Optional inversion for CARRYIN
      IS_CLK_INVERTED => '0',            -- Optional inversion for CLK
      IS_INMODE_INVERTED => "00000",     -- Optional inversion for INMODE
      IS_OPMODE_INVERTED => "000000000", -- Optional inversion for OPMODE
      IS_RSTALLCARRYIN_INVERTED => '0',  -- Optional inversion for RSTALLCARRYIN
      IS_RSTALUMODE_INVERTED => '0',     -- Optional inversion for RSTALUMODE
      IS_RSTA_INVERTED => '0',           -- Optional inversion for RSTA
      IS_RSTB_INVERTED => '0',           -- Optional inversion for RSTB
      IS_RSTCTRL_INVERTED => '0',        -- Optional inversion for RSTCTRL
      IS_RSTC_INVERTED => '0',           -- Optional inversion for RSTC
      IS_RSTD_INVERTED => '0',           -- Optional inversion for RSTD
      IS_RSTINMODE_INVERTED => '0',      -- Optional inversion for RSTINMODE
      IS_RSTM_INVERTED => '0',           -- Optional inversion for RSTM
      IS_RSTP_INVERTED => '0',           -- Optional inversion for RSTP
      -- Register Control Attributes: Pipeline Register Configuration
      ACASCREG => 1,                     -- Number of pipeline stages between A/ACIN and ACOUT (0-2)
      ADREG => 1,                        -- Pipeline stages for pre-adder (0-1)
      ALUMODEREG => 1,                   -- Pipeline stages for ALUMODE (0-1)
      AREG => 1,                         -- Pipeline stages for A (0-2)
      BCASCREG => 1,                     -- Number of pipeline stages between B/BCIN and BCOUT (0-2)
      BREG => 1,                         -- Pipeline stages for B (0-2)
      CARRYINREG => 1,                   -- Pipeline stages for CARRYIN (0-1)
      CARRYINSELREG => 1,                -- Pipeline stages for CARRYINSEL (0-1)
      CREG => 1,                         -- Pipeline stages for C (0-1)
      DREG => 1,                         -- Pipeline stages for D (0-1)
      INMODEREG => 1,                    -- Pipeline stages for INMODE (0-1)
      MREG => 0,                         -- Multiplier pipeline stages (0-1)
      OPMODEREG => 1,                    -- Pipeline stages for OPMODE (0-1)
      PREG => 1                          -- Number of pipeline stages for P (0-1)
   )
   port map (
      -- Cascade outputs: Cascade Ports
      ACOUT => open,                   -- 30-bit output: A port cascade
      BCOUT => open,                   -- 18-bit output: B cascade
      CARRYCASCOUT => open,     -- 1-bit output: Cascade carry
      MULTSIGNOUT => open,       -- 1-bit output: Multiplier sign cascade
      PCOUT => open,                   -- 48-bit output: Cascade output
      -- Control outputs: Control Inputs/Status Bits
      OVERFLOW => open,             -- 1-bit output: Overflow in add/acc
      PATTERNBDETECT => open, -- 1-bit output: Pattern bar detect
      PATTERNDETECT => open,   -- 1-bit output: Pattern detect
      UNDERFLOW => open,           -- 1-bit output: Underflow in add/acc
      -- Data outputs: Data Ports
      CARRYOUT => open,             -- 4-bit output: Carry
      P => DSP_P,                           -- 48-bit output: Primary data
      XOROUT => open,                 -- 8-bit output: XOR data
      -- Cascade inputs: Cascade Ports
      ACIN => (others => '0'),                     -- 30-bit input: A cascade data
      BCIN => (others => '0'),                     -- 18-bit input: B cascade
      CARRYCASCIN => '0',       -- 1-bit input: Cascade carry
      MULTSIGNIN => '0',         -- 1-bit input: Multiplier sign cascade
      PCIN => (others => '0'),                     -- 48-bit input: P cascade
      -- Control inputs: Control Inputs/Status Bits
      ALUMODE => "0000",               -- 4-bit input: ALU control P = Z+W+X+Y+CIN
      CARRYINSEL => "000",         -- 3-bit input: Carry select
      CLK => clk,                       -- 1-bit input: Clock
      INMODE => "10001",                 -- 5-bit input: INMODE control
      OPMODE => "100001111",                 -- 9-bit input: Operation mode W = RND, Z = 0, Y = C, X = A:B
      -- Data inputs: Data Ports
      A => DSP_AB(47 downto 18),                           -- 30-bit input: A data
      B => DSP_AB(17 downto 0),                           -- 18-bit input: B data
      C => DSP_C,                           -- 48-bit input: C data
      CARRYIN => '0',               -- 1-bit input: Carry-in
      D => (others => '1'),                           -- 27-bit input: D data
      -- Reset/Clock Enable inputs: Reset/Clock Enable Inputs
      CEA1 => ce_DSP_AB,                     -- 1-bit input: Clock enable for 1st stage AREG
      CEA2 => '1',                     -- 1-bit input: Clock enable for 2nd stage AREG
      CEAD => '1',                     -- 1-bit input: Clock enable for ADREG
      CEALUMODE => '1',           -- 1-bit input: Clock enable for ALUMODE
      CEB1 => ce_DSP_AB,                     -- 1-bit input: Clock enable for 1st stage BREG
      CEB2 => '1',                     -- 1-bit input: Clock enable for 2nd stage BREG
      CEC => load_ROI,                       -- 1-bit input: Clock enable for CREG
      CECARRYIN => '1',           -- 1-bit input: Clock enable for CARRYINREG
      CECTRL => '1',                 -- 1-bit input: Clock enable for OPMODEREG and CARRYINSELREG
      CED => '1',                       -- 1-bit input: Clock enable for DREG
      CEINMODE => '1',             -- 1-bit input: Clock enable for INMODEREG
      CEM => '1',                       -- 1-bit input: Clock enable for MREG
      CEP => '1',                       -- 1-bit input: Clock enable for PREG
      RSTA => '0',                     -- 1-bit input: Reset for AREG
      RSTALLCARRYIN => '0',   -- 1-bit input: Reset for CARRYINREG
      RSTALUMODE => '0',         -- 1-bit input: Reset for ALUMODEREG
      RSTB => '0',                     -- 1-bit input: Reset for BREG
      RSTC => '0',                     -- 1-bit input: Reset for CREG
      RSTCTRL => '0',               -- 1-bit input: Reset for OPMODEREG and CARRYINSELREG
      RSTD => '0',                     -- 1-bit input: Reset for DREG and ADREG
      RSTINMODE => '0',           -- 1-bit input: Reset for INMODEREG
      RSTM => '0',                     -- 1-bit input: Reset for MREG
      RSTP => '0'                      -- 1-bit input: Reset for PREG
   );
DSP_C(46 downto 24) <= fifo_dout(55 downto 33); -- r_offset
DSP_C(23 downto 2) <= fifo_dout(22 downto 1); -- theta_ROI
seg_pm(48 downto 26) <= DSP_P(46 downto 24); -- r of segment found
seg_pm(25 downto 4) <= DSP_P(23 downto 2); -- theta of segment found
end Behavioral;
