----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/15/2018 10:38:22 AM
-- Design Name: 
-- Module Name: l0mdt_pack - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package l0mdt_pack is
component LUT_delay
  generic(N : integer; INIT : bit_vector := x"0000");
  port ( clk : in std_logic; d : in std_logic; ce : in  std_logic; q : out std_logic);
end component;
component LUT_delayV
  generic(N : integer; V : integer := 16);
  port ( clk : in std_logic; d : in std_logic_vector(V-1 downto 0); ce : in  std_logic; q : out std_logic_vector(V-1 downto 0));
end component;
function sign_extend (a : std_logic_vector; size: integer) return std_logic_vector;
constant ultraRAM_ADDR : std_logic_vector(31 downto 20) := x"123";
type array2X4 is array(0 to 1) of std_logic_vector(3 downto 0);
type array2X5 is array(0 to 1) of std_logic_vector(4 downto 0);
type array2X7 is array(0 to 1) of std_logic_vector(6 downto 0);
type array2X8 is array(0 to 1) of std_logic_vector(7 downto 0);
type array2X9 is array(0 to 1) of std_logic_vector(8 downto 0);
type array2X10 is array(0 to 1) of std_logic_vector(9 downto 0);
type array2X12 is array(0 to 1) of std_logic_vector(11 downto 0);
type array2X13 is array(0 to 1) of std_logic_vector(12 downto 0);
type array2X14 is array(0 to 1) of std_logic_vector(13 downto 0);
type array2X19 is array(0 to 1) of std_logic_vector(18 downto 0);
type array2X21 is array(0 to 1) of std_logic_vector(20 downto 0);
type array2X23 is array(0 to 1) of std_logic_vector(22 downto 0);
type array3X4 is array(0 to 2) of std_logic_vector(3 downto 0);
type array4X6 is array(0 to 3) of std_logic_vector(5 downto 0);
type array4X9 is array(0 to 3) of std_logic_vector(8 downto 0);
type array4X12 is array(0 to 3) of std_logic_vector(11 downto 0);
type array4X13 is array(0 to 3) of std_logic_vector(12 downto 0);
type array4X18 is array(0 to 3) of std_logic_vector(17 downto 0);
type array7X13 is array(0 to 6) of std_logic_vector(12 downto 0);
type array8X5 is array(0 to 7) of std_logic_vector(4 downto 0);
type array8X8 is array(0 to 7) of std_logic_vector(7 downto 0);
type array8X10 is array(0 to 7) of std_logic_vector(9 downto 0);
type array8X12 is array(0 to 7) of std_logic_vector(11 downto 0);
type array8X13 is array(0 to 7) of std_logic_vector(12 downto 0);
type array8X15 is array(0 to 7) of std_logic_vector(14 downto 0);
type array8X16 is array(0 to 7) of std_logic_vector(15 downto 0);
type array8X17 is array(0 to 7) of std_logic_vector(16 downto 0);
type array8X18 is array(0 to 7) of std_logic_vector(17 downto 0);
type array8X23 is array(0 to 7) of std_logic_vector(22 downto 0);
type array8X30 is array(0 to 7) of std_logic_vector(29 downto 0);
type array8X48 is array(0 to 7) of std_logic_vector(47 downto 0);
type array8X49 is array(0 to 7) of std_logic_vector(48 downto 0);
type array8X72 is array(0 to 7) of std_logic_vector(71 downto 0);
type array9X12 is array(0 to 8) of std_logic_vector(11 downto 0);
type array9X13 is array(0 to 8) of std_logic_vector(12 downto 0);
type array9X18 is array(0 to 8) of std_logic_vector(17 downto 0);
type array9X30 is array(0 to 8) of std_logic_vector(29 downto 0);
type array12X16 is array(0 to 11) of std_logic_vector(15 downto 0);
type array16X8 is array(0 to 15) of std_logic_vector(7 downto 0);
type array16X10 is array(0 to 15) of std_logic_vector(9 downto 0);
type array16X12 is array(0 to 15) of std_logic_vector(11 downto 0);
type array16X16 is array(0 to 15) of std_logic_vector(15 downto 0);
type array16X20 is array(0 to 15) of std_logic_vector(19 downto 0);
type array16X22 is array(0 to 15) of std_logic_vector(21 downto 0);
type array16X23 is array(0 to 15) of std_logic_vector(22 downto 0);
type array16X24 is array(0 to 15) of std_logic_vector(23 downto 0);
type array16X49 is array(0 to 15) of std_logic_vector(48 downto 0);
type array32X7 is array(0 to 31) of std_logic_vector(6 downto 0);
type array32X11 is array(0 to 31) of std_logic_vector(10 downto 0);
type array32X13 is array(0 to 31) of std_logic_vector(12 downto 0);
type array32X15 is array(0 to 31) of std_logic_vector(14 downto 0);
type array32X16 is array(0 to 31) of std_logic_vector(15 downto 0);
type array32X18 is array(0 to 31) of std_logic_vector(17 downto 0);
type array32X22 is array(0 to 31) of std_logic_vector(21 downto 0);
type array33X11 is array(0 to 32) of std_logic_vector(10 downto 0);
type array33X13 is array(0 to 32) of std_logic_vector(12 downto 0);
type array33X17 is array(0 to 32) of std_logic_vector(16 downto 0);
type array33X18 is array(0 to 32) of std_logic_vector(17 downto 0);
type array33X21 is array(0 to 32) of std_logic_vector(20 downto 0);
type array33X22 is array(0 to 32) of std_logic_vector(21 downto 0);
type array64X6 is array(0 to 63) of std_logic_vector(5 downto 0);
type array64X14 is array(0 to 63) of std_logic_vector(13 downto 0);
type array16X8X8 is array(0 to 15) of array8X8;
type Integer_array24 is array(0 to 23) of integer range 0 to 31;
type Integer_array9 is array(0 to 8) of integer range 0 to 63;
end l0mdt_pack;
package body l0mdt_pack is
function sign_extend (a : std_logic_vector; size: integer) return std_logic_vector is
variable vo : std_logic_vector(size-1 downto 0);
begin
  vo(a'length-1 downto 0) := a;
  for i in a'length to size -1 loop
    vo(i) := a(a'left);
  end loop;
  return vo;
end sign_extend;
end l0mdt_pack;
