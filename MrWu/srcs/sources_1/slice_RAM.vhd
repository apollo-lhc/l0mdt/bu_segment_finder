----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/01/2018 11:02:51 AM
-- Design Name: 
-- Module Name: slice_RAM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity slice_RAM is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC; -- synchronous to clk
           we : in STD_LOGIC_VECTOR (3 downto 0);
           ae : in STD_LOGIC_VECTOR (7 downto 0);
           ao : in STD_LOGIC_VECTOR (7 downto 0);
           r_e : out STD_LOGIC_VECTOR (7 downto 0);
           r_o : out STD_LOGIC_VECTOR (7 downto 0);
           max : out STD_LOGIC_VECTOR (4 downto 0));
end slice_RAM;

architecture Behavioral of slice_RAM is
component quad_adder
    Port ( reset : in  STD_LOGIC;
           di : in  STD_LOGIC_VECTOR (15 downto 0);
           ci : in  STD_LOGIC_VECTOR (3 downto 0);
           do : out  STD_LOGIC_VECTOR (15 downto 0));
end component;
signal ae_q : std_logic_vector(7 downto 0) := (others => '0');
signal ao_q : std_logic_vector(7 downto 0) := (others => '0');
signal we_q : std_logic_vector(1 downto 0) := (others => '0');
signal odd : std_logic_vector(3 downto 0) := (others => '0');
signal even : std_logic_vector(3 downto 0) := (others => '0');
signal do : std_logic_vector(31 downto 0) := (others => '0');
signal di : std_logic_vector(31 downto 0) := (others => '0');
signal MUXF7_S : std_logic_vector(1 downto 0) := (others => '0');
signal MUXF7_O : std_logic_vector(15 downto 0) := (others => '0');
signal MUXF8_S : std_logic_vector(1 downto 0) := (others => '0');
signal MUXF8_O : std_logic_vector(7 downto 0) := (others => '0');
signal do_q : std_logic_vector(7 downto 0) := (others => '0');
signal max_i : std_logic_vector(4 downto 0) := (others => '0');
begin
max <= max_i;
process(clk)
begin
  if(clk'event and clk = '1')then
    we_q(0) <= we(1) or we(0);
    we_q(1) <= we(3) or we(2);
		do_q <= MUXF8_O;
    ao_q <= ao;
    ae_q <= ae;
    if(do_q(7 downto 4) = max_i(3 downto 0) and we_q(1) = '1')then
      r_o <=  ao_q;
      max_i(4) <= '1';
    end if;
    if(do_q(3 downto 0) = max_i(3 downto 0) and we_q(0) = '1')then
      r_e <=  ae_q;
      max_i(4) <= '0';
    end if;
    if(reset = '1')then
      max_i(3 downto 0) <= (others => '0');
    elsif(max_i(3 downto 0) = x"f")then
    elsif((do_q(7 downto 4) = max_i(3 downto 0) and we_q(1) = '1') or
            (do_q(3 downto 0) = max_i(3 downto 0) and we_q(0) = '1'))then
      max_i(3 downto 0) <=  max_i(3 downto 0) + 1;
    end if;
  end if;
end process;
even(0) <= not ae(6);
even(1) <= ae(6);
even(2) <= not ae(6);
even(3) <= ae(6);
odd(0) <= not ao(6);
odd(1) <= ao(6);
odd(2) <= not ao(6);
odd(3) <= ao(6);
MUXF8_S(0) <= ae(7);
MUXF8_S(1) <= ao(7);
g_MUXF8: for i in 0 to 7 generate
  i_MUXF8 : MUXF8
  port map (
     O => MUXF8_O(i),   -- 1-bit output: Output of MUX
     I0 => MUXF7_O(i*2), -- 1-bit input: Connect to LUT6 output
     I1 => MUXF7_O(i*2+1), -- 1-bit input: Connect to LUT6 output
     S => MUXF8_S(i/4)    -- 1-bit input: Input select to MUX
  );
end generate;
MUXF7_S(0) <= ae(6);
MUXF7_S(1) <= ao(6);
g_MUXF7: for i in 0 to 15 generate
  i_MUXF7 : MUXF7
  port map (
     O => MUXF7_O(i),   -- 1-bit output: Output of MUX
     I0 => do(i*2), -- 1-bit input: Connect to LUT6 output
     I1 => do(i*2+1), -- 1-bit input: Connect to LUT6 output
     S => MUXF7_S(i/8)    -- 1-bit input: Input select to MUX
  );
end generate;
g_RAMe: for i in 0 to 15 generate
  i_RAMe : RAM64X1S
  port map (
     O => do(i),       -- RAM output
     A0 => ae(0),     -- RAM address[0] input
     A1 => ae(1),     -- RAM address[1] input
     A2 => ae(2),     -- RAM address[2] input
     A3 => ae(3),     -- RAM address[3] input
     A4 => ae(4),     -- RAM address[4] input
     A5 => ae(5),     -- RAM address[5] input
     D => di(i),       -- RAM data input
     WCLK => clk, -- Write clock input
     WE => we(i/8)      -- Write enable input
  );
end generate;
i_quad_adder_e : quad_adder
	port map(reset => reset, di => do(15 downto 0), ci => even, do => di(15 downto 0));
g_RAMo: for i in 0 to 15 generate
  i_RAMo : RAM64X1S
  port map (
     O => do(i+16),       -- RAM output
     A0 => ao(0),     -- RAM address[0] input
     A1 => ao(1),     -- RAM address[1] input
     A2 => ao(2),     -- RAM address[2] input
     A3 => ao(3),     -- RAM address[3] input
     A4 => ao(4),     -- RAM address[4] input
     A5 => ao(5),     -- RAM address[5] input
     D => di(i+16),       -- RAM data input
     WCLK => clk, -- Write clock input
     WE => we(i/8+2)      -- Write enable input
  );
end generate;
i_quad_adder_o : quad_adder
	port map(reset => reset, di => do(31 downto 16), ci => odd, do => di(31 downto 16));
end Behavioral;
