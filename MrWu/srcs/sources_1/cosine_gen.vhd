----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/15/2018 10:34:53 AM
-- Design Name: 
-- Module Name: cosine_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity cosine_gen is
    Port ( clk : in STD_LOGIC;
           theta_IN: in STD_LOGIC_VECTOR (22 downto 0);-- this is in fact theta - phi0, it must be >= -0.96 radian  and < 0.96 radian
           new_hit : in STD_LOGIC;-- valid hit present
           we_uRAM : in STD_LOGIC;
					 din : in STD_LOGIC_VECTOR (71 downto 0);
           cosine_h : out array33X21;
           cosine_l : out array33X11);
end cosine_gen;

architecture Behavioral of cosine_gen is
COMPONENT cosine_rom
  PORT (
    clka : IN STD_LOGIC;
    addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(335 DOWNTO 0);
    clkb : IN STD_LOGIC;
    addrb : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(335 DOWNTO 0)
  );
END COMPONENT;
COMPONENT cosine_romC
  PORT (
    clka : IN STD_LOGIC;
    addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(20 DOWNTO 0)
  );
END COMPONENT;
component cosine_l_gen
    Port ( clk : in STD_LOGIC;
           new_hit : in STD_LOGIC;
           theta0 : in STD_LOGIC_VECTOR (22 downto 0);-- this is actually theta - phi, the first theta bin. bit 22 is a sign bit
           we_uRAM : in STD_LOGIC;
					 din : in STD_LOGIC_VECTOR (71 downto 0);
           addr : in array2X12;
           cosine_l : out array33X11);
end component;
constant addr_offset : array4X12 := (x"f80",x"f40",x"000",x"040");
signal do : std_logic_vector(692 downto 0) := (others => '0');
signal theta : std_logic_vector(22 downto 0) := (others => '0');
signal theta_dl : std_logic_vector(22 downto 0) := (others => '0');
signal addr : array2X12;
signal cos_addr : array2X12;
begin
theta <= theta_in;
g_cosine: for i in 0 to 32 generate
	cosine_h(i) <= do(i*21+20 downto i*21);
end generate;
process(clk)
begin
  if(clk'event and clk = '1')then
    for i in 0 to 1 loop
			if(new_hit = '1')then
				addr(i) <= theta(22 downto 11) + addr_offset(i*2);
			else
				addr(i) <= theta(22 downto 11) + addr_offset(i*2+1);
			end if;
    end loop;
  end if;
end process;
i_cosine_rom : cosine_rom
  PORT MAP (
    clka => clk,
    addra => cos_addr(0),
    douta => do(335 downto 0),
    clkb => clk,
    addrb => cos_addr(1),
    doutb => do(671 downto 336)
  );
i_cosine_romC : cosine_romC
      PORT MAP (
        clka => clk,
        addra => cos_addr(0),
        douta => do(692 downto 672)
      );
g_cos_addr_i: for i in 0 to 1 generate
    i_cos_addr : LUT_delayV generic map(N => 6, V => 12)
      port map (
         Q => cos_addr(i),     -- 1-bit output: SRL Data
         CE => '1',   -- 1-bit input: Clock enable
         CLK => clk, -- 1-bit input: Clock
         D => addr(i)     -- 1-bit input: SRL Data
      );
end generate g_cos_addr_i;
i_theta_dl : LUT_delayV generic map(N => 9, V => 23)
      port map (
         Q => theta_dl,     -- 1-bit output: SRL Data
         CE => '1',   -- 1-bit input: Clock enable
         CLK => clk, -- 1-bit input: Clock
         D => theta     -- 1-bit input: SRL Data
      );
i_cosine_l: cosine_l_gen
  port map(clk => clk, new_hit => new_hit, theta0 => theta, we_uRAM => we_uRAM, din => din, addr => addr, cosine_l => cosine_l);
end Behavioral;
