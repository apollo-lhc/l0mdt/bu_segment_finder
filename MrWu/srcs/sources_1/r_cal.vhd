----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/05/2018 12:33:15 PM
-- Design Name: 
-- Module Name: dual_theta_slice - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity r_calc is
    Port ( clk : in STD_LOGIC;
					 reset : in STD_LOGIC;
					 new_hit : in STD_LOGIC;
           r0 : in STD_LOGIC_VECTOR (22 downto 0);-- positive number r0(5) = 0.25mm
           R_drift : in STD_LOGIC_VECTOR (9 downto 0);-- R_drift(4) = 0.25mm
           cosine_h : in STD_LOGIC_VECTOR (20 downto 0);-- negative number without sign bit(all 0 is -1.0)
           cosine_l : in STD_LOGIC_VECTOR (10 downto 0);-- number with sign bit
           r_offset : in STD_LOGIC_VECTOR (22 downto 0);-- positive number r0(5) = 0.25mm
           r_low_out : out STD_LOGIC_VECTOR (16 downto 0); -- theta bins 0-63
           r_high_out : out STD_LOGIC_VECTOR (16 downto 0)); -- theta bins 64-127
end r_calc;

architecture Behavioral of r_calc is
signal P : std_logic_vector(47 downto 0) := (others => '0');
signal PC : std_logic_vector(47 downto 0) := (others => '0');
signal A : std_logic_vector(29 downto 0) := (others => '1');
signal AC : std_logic_vector(29 downto 0) := (others => '0');
signal B : std_logic_vector(17 downto 0) := (others => '0');
signal B_up : std_logic_vector(17 downto 0) := (others => '0');
signal C : std_logic_vector(47 downto 0) := (others => '0');
signal D : std_logic_vector(26 downto 0) := (others => '0');
signal D_up : std_logic_vector(26 downto 0) := (others => '0');
signal R_drift_q : array2X10;
signal r_low : std_logic_vector(15 downto 0) := (others => '0');
signal r_high : std_logic_vector(15 downto 0) := (others => '0');
signal r_low_i : std_logic_vector(15 downto 0) := (others => '0');
signal r_high_i : std_logic_vector(15 downto 0) := (others => '0');
signal OOR : std_logic_vector(1 downto 0) := (others => '0');
signal new_hit_q : std_logic_vector(2 downto 0) := (others => '0');
signal reset_q : std_logic := '0';
begin
DSP48E2_low : DSP48E2
generic map (
   -- Feature Control Attributes: Data Path Selection
   AMULTSEL => "AD",                   -- Selects A input to multiplier (A, AD)
   A_INPUT => "DIRECT",               -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
   BMULTSEL => "B",                   -- Selects B input to multiplier (AD, B)
   B_INPUT => "DIRECT",               -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
   PREADDINSEL => "B",                -- Selects input to pre-adder (A, B)
--   RND => X"0000001FFFFF",            -- Rounding Constant round down by 0.125mm
   RND => X"000000000000",            -- Rounding Constant round down by 0.125mm
   USE_MULT => "MULTIPLY",            -- Select multiplier usage (DYNAMIC, MULTIPLY, NONE)
   USE_SIMD => "ONE48",               -- SIMD selection (FOUR12, ONE48, TWO24)
   USE_WIDEXOR => "FALSE",            -- Use the Wide XOR function (FALSE, TRUE)
   XORSIMD => "XOR24_48_96",          -- Mode of operation for the Wide XOR (XOR12, XOR24_48_96)
   -- Pattern Detector Attributes: Pattern Detection Configuration
   AUTORESET_PATDET => "NO_RESET",    -- NO_RESET, RESET_MATCH, RESET_NOT_MATCH
   AUTORESET_PRIORITY => "RESET",     -- Priority of AUTORESET vs. CEP (CEP, RESET).
   MASK => X"3fffffffffff",           -- 48-bit mask value for pattern detect (1=ignore)
   PATTERN => X"000000000000",        -- 48-bit pattern match for pattern detect
   SEL_MASK => "MASK",                -- C, MASK, ROUNDING_MODE1, ROUNDING_MODE2
   SEL_PATTERN => "PATTERN",          -- Select pattern value (C, PATTERN)
   USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect (NO_PATDET, PATDET)
   -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
   IS_ALUMODE_INVERTED => "0000",     -- Optional inversion for ALUMODE
   IS_CARRYIN_INVERTED => '0',        -- Optional inversion for CARRYIN
   IS_CLK_INVERTED => '0',            -- Optional inversion for CLK
   IS_INMODE_INVERTED => "00000",     -- Optional inversion for INMODE
   IS_OPMODE_INVERTED => "000000000", -- Optional inversion for OPMODE
   IS_RSTALLCARRYIN_INVERTED => '0',  -- Optional inversion for RSTALLCARRYIN
   IS_RSTALUMODE_INVERTED => '0',     -- Optional inversion for RSTALUMODE
   IS_RSTA_INVERTED => '0',           -- Optional inversion for RSTA
   IS_RSTB_INVERTED => '0',           -- Optional inversion for RSTB
   IS_RSTCTRL_INVERTED => '0',        -- Optional inversion for RSTCTRL
   IS_RSTC_INVERTED => '0',           -- Optional inversion for RSTC
   IS_RSTD_INVERTED => '0',           -- Optional inversion for RSTD
   IS_RSTINMODE_INVERTED => '0',      -- Optional inversion for RSTINMODE
   IS_RSTM_INVERTED => '0',           -- Optional inversion for RSTM
   IS_RSTP_INVERTED => '0',           -- Optional inversion for RSTP
   -- Register Control Attributes: Pipeline Register Configuration
   ACASCREG => 1,                     -- Number of pipeline stages between A/ACIN and ACOUT (0-2)
   ADREG => 1,                        -- Pipeline stages for pre-adder (0-1)
   ALUMODEREG => 0,                   -- Pipeline stages for ALUMODE (0-1)
   AREG => 1,                         -- Pipeline stages for A (0-2)
   BCASCREG => 1,                     -- Number of pipeline stages between B/BCIN and BCOUT (0-2)
   BREG => 1,                         -- Pipeline stages for B (0-2)
   CARRYINREG => 0,                   -- Pipeline stages for CARRYIN (0-1)
   CARRYINSELREG => 0,                -- Pipeline stages for CARRYINSEL (0-1)
   CREG => 1,                         -- Pipeline stages for C (0-1)
   DREG => 1,                         -- Pipeline stages for D (0-1)
   INMODEREG => 0,                    -- Pipeline stages for INMODE (0-1)
   MREG => 1,                         -- Multiplier pipeline stages (0-1)
   OPMODEREG => 0,                    -- Pipeline stages for OPMODE (0-1)
   PREG => 1                          -- Number of pipeline stages for P (0-1)
)
port map (
   -- Cascade outputs: Cascade Ports
   ACOUT => AC,                   -- 30-bit output: A port cascade
   BCOUT => open,                   -- 18-bit output: B cascade
   CARRYCASCOUT => open,     -- 1-bit output: Cascade carry
   MULTSIGNOUT => open,       -- 1-bit output: Multiplier sign cascade
   PCOUT => PC,                   -- 48-bit output: Cascade output
   -- Control outputs: Control Inputs/Status Bits
   OVERFLOW => open,             -- 1-bit output: Overflow in add/acc
   PATTERNBDETECT => open, -- 1-bit output: Pattern bar detect
   PATTERNDETECT => open,   -- 1-bit output: Pattern detect
   UNDERFLOW => open,           -- 1-bit output: Underflow in add/acc
   -- Data outputs: Data Ports
   CARRYOUT => open,             -- 4-bit output: Carry
   P => open,                           -- 48-bit output: Primary data
   XOROUT => open,                 -- 8-bit output: XOR data
   -- Cascade inputs: Cascade Ports
   ACIN => (others => '0'),                     -- 30-bit input: A cascade data
   BCIN => (others => '0'),                     -- 18-bit input: B cascade
   CARRYCASCIN => '0',       -- 1-bit input: Cascade carry
   MULTSIGNIN => '0',         -- 1-bit input: Multiplier sign cascade
   PCIN => (others => '0'),                     -- 48-bit input: P cascade
   -- Control inputs: Control Inputs/Status Bits
   ALUMODE => "0011",               -- 4-bit input: ALU control Z-(W+X+Y+CIN)
   CARRYINSEL => "000",         -- 3-bit input: Carry select
   CLK => clk,                       -- 1-bit input: Clock
   INMODE => "10101",                 -- 5-bit input: INMODE control
   OPMODE => "000000101",             -- 9-bit input: Operation W = 0, Z = 0, X+Y = M
   -- Data inputs: Data Ports
   A => A,                           -- 30-bit input: A data
   B => B,                           -- 18-bit input: B data
   C => (others => '1'),                           -- 48-bit input: C data
   CARRYIN => '0',               -- 1-bit input: Carry-in
   D => D,                           -- 27-bit input: D data
   -- Reset/Clock Enable inputs: Reset/Clock Enable Inputs
   CEA1 => '1',                     -- 1-bit input: Clock enable for 1st stage AREG
   CEA2 => '1',                     -- 1-bit input: Clock enable for 2nd stage AREG
   CEAD => '1',                     -- 1-bit input: Clock enable for ADREG
   CEALUMODE => '1',           -- 1-bit input: Clock enable for ALUMODE
   CEB1 => '1',                     -- 1-bit input: Clock enable for 1st stage BREG
   CEB2 => '1',                     -- 1-bit input: Clock enable for 2nd stage BREG
   CEC => '1',                       -- 1-bit input: Clock enable for CREG
   CECARRYIN => '1',           -- 1-bit input: Clock enable for CARRYINREG
   CECTRL => '1',                 -- 1-bit input: Clock enable for OPMODEREG and CARRYINSELREG
   CED => '1',                       -- 1-bit input: Clock enable for DREG
   CEINMODE => '1',             -- 1-bit input: Clock enable for INMODEREG
   CEM => '1',                       -- 1-bit input: Clock enable for MREG
   CEP => '1',                       -- 1-bit input: Clock enable for PREG
   RSTA => '0',                     -- 1-bit input: Reset for AREG
   RSTALLCARRYIN => '0',   -- 1-bit input: Reset for CARRYINREG
   RSTALUMODE => '0',         -- 1-bit input: Reset for ALUMODEREG
   RSTB => '0',                     -- 1-bit input: Reset for BREG
   RSTC => '0',                     -- 1-bit input: Reset for CREG
   RSTCTRL => '0',               -- 1-bit input: Reset for OPMODEREG and CARRYINSELREG
   RSTD => '0',                     -- 1-bit input: Reset for DREG and ADREG
   RSTINMODE => '0',           -- 1-bit input: Reset for INMODEREG
   RSTM => '0',                     -- 1-bit input: Reset for MREG
   RSTP => '0'                      -- 1-bit input: Reset for PREG
);
DSP48E2_up : DSP48E2
generic map (
   -- Feature Control Attributes: Data Path Selection
   AMULTSEL => "AD",                   -- Selects A input to multiplier (A, AD)
   A_INPUT => "CASCADE",               -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
   BMULTSEL => "B",                   -- Selects B input to multiplier (AD, B)
   B_INPUT => "DIRECT",               -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
   PREADDINSEL => "A",                -- Selects input to pre-adder (A, B)
   RND => X"000000000000",            -- Rounding Constant
   USE_MULT => "MULTIPLY",            -- Select multiplier usage (DYNAMIC, MULTIPLY, NONE)
   USE_SIMD => "ONE48",               -- SIMD selection (FOUR12, ONE48, TWO24)
   USE_WIDEXOR => "FALSE",            -- Use the Wide XOR function (FALSE, TRUE)
   XORSIMD => "XOR24_48_96",          -- Mode of operation for the Wide XOR (XOR12, XOR24_48_96)
   -- Pattern Detector Attributes: Pattern Detection Configuration
   AUTORESET_PATDET => "NO_RESET",    -- NO_RESET, RESET_MATCH, RESET_NOT_MATCH
   AUTORESET_PRIORITY => "RESET",     -- Priority of AUTORESET vs. CEP (CEP, RESET).
   MASK => X"3fffffffffff",           -- 48-bit mask value for pattern detect (1=ignore)
   PATTERN => X"000000000000",        -- 48-bit pattern match for pattern detect
   SEL_MASK => "MASK",                -- C, MASK, ROUNDING_MODE1, ROUNDING_MODE2
   SEL_PATTERN => "PATTERN",          -- Select pattern value (C, PATTERN)
   USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect (NO_PATDET, PATDET)
   -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
   IS_ALUMODE_INVERTED => "0000",     -- Optional inversion for ALUMODE
   IS_CARRYIN_INVERTED => '0',        -- Optional inversion for CARRYIN
   IS_CLK_INVERTED => '0',            -- Optional inversion for CLK
   IS_INMODE_INVERTED => "00000",     -- Optional inversion for INMODE
   IS_OPMODE_INVERTED => "000000000", -- Optional inversion for OPMODE
   IS_RSTALLCARRYIN_INVERTED => '0',  -- Optional inversion for RSTALLCARRYIN
   IS_RSTALUMODE_INVERTED => '0',     -- Optional inversion for RSTALUMODE
   IS_RSTA_INVERTED => '0',           -- Optional inversion for RSTA
   IS_RSTB_INVERTED => '0',           -- Optional inversion for RSTB
   IS_RSTCTRL_INVERTED => '0',        -- Optional inversion for RSTCTRL
   IS_RSTC_INVERTED => '0',           -- Optional inversion for RSTC
   IS_RSTD_INVERTED => '0',           -- Optional inversion for RSTD
   IS_RSTINMODE_INVERTED => '0',      -- Optional inversion for RSTINMODE
   IS_RSTM_INVERTED => '0',           -- Optional inversion for RSTM
   IS_RSTP_INVERTED => '0',           -- Optional inversion for RSTP
   -- Register Control Attributes: Pipeline Register Configuration
   ACASCREG => 1,                     -- Number of pipeline stages between A/ACIN and ACOUT (0-2)
   ADREG => 1,                        -- Pipeline stages for pre-adder (0-1)
   ALUMODEREG => 0,                   -- Pipeline stages for ALUMODE (0-1)
   AREG => 1,                         -- Pipeline stages for A (0-2)
   BCASCREG => 1,                     -- Number of pipeline stages between B/BCIN and BCOUT (0-2)
   BREG => 1,                         -- Pipeline stages for B (0-2)
   CARRYINREG => 0,                   -- Pipeline stages for CARRYIN (0-1)
   CARRYINSELREG => 0,                -- Pipeline stages for CARRYINSEL (0-1)
   CREG => 1,                         -- Pipeline stages for C (0-1)
   DREG => 1,                         -- Pipeline stages for D (0-1)
   INMODEREG => 0,                    -- Pipeline stages for INMODE (0-1)
   MREG => 1,                         -- Multiplier pipeline stages (0-1)
   OPMODEREG => 0,                    -- Pipeline stages for OPMODE (0-1)
   PREG => 1                          -- Number of pipeline stages for P (0-1)
)
port map (
   -- Cascade outputs: Cascade Ports
   ACOUT => open,                   -- 30-bit output: A port cascade
   BCOUT => open,                   -- 18-bit output: B cascade
   CARRYCASCOUT => open,     -- 1-bit output: Cascade carry
   MULTSIGNOUT => open,       -- 1-bit output: Multiplier sign cascade
   PCOUT => open,                   -- 48-bit output: Cascade output
   -- Control outputs: Control Inputs/Status Bits
   OVERFLOW => open,             -- 1-bit output: Overflow in add/acc
   PATTERNBDETECT => open, -- 1-bit output: Pattern bar detect
   PATTERNDETECT => open,   -- 1-bit output: Pattern detect
   UNDERFLOW => open,           -- 1-bit output: Underflow in add/acc
   -- Data outputs: Data Ports
   CARRYOUT => open,             -- 4-bit output: Carry
   P => P,                           -- 48-bit output: Primary data
   XOROUT => open,                 -- 8-bit output: XOR data
   -- Cascade inputs: Cascade Ports
   ACIN => AC,                     -- 30-bit input: A cascade data
   BCIN => (others => '0'),                     -- 18-bit input: B cascade
   CARRYCASCIN => '0',       -- 1-bit input: Cascade carry
   MULTSIGNIN => '0',         -- 1-bit input: Multiplier sign cascade
   PCIN => PC,                     -- 48-bit input: P cascade
   -- Control inputs: Control Inputs/Status Bits
   ALUMODE => "0011",               -- 4-bit input: ALU control Z-(W+X+Y+CIN)
   CARRYINSEL => "000",         -- 3-bit input: Carry select
   CLK => clk,                       -- 1-bit input: Clock
   INMODE => "10101",                 -- 5-bit input: INMODE control
   OPMODE => "111010101",             -- 9-bit input: Operation W = C, Z = PC shift, X+Y = M
   -- Data inputs: Data Ports
   A =>  (others => '1'),                           -- 30-bit input: A data
   B => B_up,                           -- 18-bit input: B data
   C => C,                           -- 48-bit input: C data
   CARRYIN => '0',               -- 1-bit input: Carry-in
   D => D_up,                           -- 27-bit input: D data
   -- Reset/Clock Enable inputs: Reset/Clock Enable Inputs
   CEA1 => '1',                     -- 1-bit input: Clock enable for 1st stage AREG
   CEA2 => '1',                     -- 1-bit input: Clock enable for 2nd stage AREG
   CEAD => '1',                     -- 1-bit input: Clock enable for ADREG
   CEALUMODE => '1',           -- 1-bit input: Clock enable for ALUMODE
   CEB1 => '1',                     -- 1-bit input: Clock enable for 1st stage BREG
   CEB2 => '1',                     -- 1-bit input: Clock enable for 2nd stage BREG
   CEC => '1',                       -- 1-bit input: Clock enable for CREG
   CECARRYIN => '1',           -- 1-bit input: Clock enable for CARRYINREG
   CECTRL => '1',                 -- 1-bit input: Clock enable for OPMODEREG and CARRYINSELREG
   CED => '1',                       -- 1-bit input: Clock enable for DREG
   CEINMODE => '1',             -- 1-bit input: Clock enable for INMODEREG
   CEM => '1',                       -- 1-bit input: Clock enable for MREG
   CEP => '1',                       -- 1-bit input: Clock enable for PREG
   RSTA => '0',                     -- 1-bit input: Reset for AREG
   RSTALLCARRYIN => '0',   -- 1-bit input: Reset for CARRYINREG
   RSTALUMODE => '0',         -- 1-bit input: Reset for ALUMODEREG
   RSTB => '0',                     -- 1-bit input: Reset for BREG
   RSTC => '0',                     -- 1-bit input: Reset for CREG
   RSTCTRL => '0',               -- 1-bit input: Reset for OPMODEREG and CARRYINSELREG
   RSTD => '0',                     -- 1-bit input: Reset for DREG and ADREG
   RSTINMODE => '0',           -- 1-bit input: Reset for INMODEREG
   RSTM => '0',                     -- 1-bit input: Reset for MREG
   RSTP => '0'                      -- 1-bit input: Reset for PREG
);
process(clk)
begin
  if(clk'event and clk = '1')then
		A(20 downto 0) <= cosine_h;
    B(16 downto 11) <= r0(5 downto 0);
		D <= sign_extend(cosine_l,27);
    B_up <=  '0' & r0(22 downto 6);
    C(43 downto 21) <= r_offset;
		D_up <= D;
		R_drift_q <= R_drift & R_drift_q(0);
		new_hit_q <= new_hit_q(1 downto 0) & new_hit;
		reset_q <= reset;
		if(reset_q = '1' or new_hit_q(0) = '1')then
			r_low <= P(36 downto 21); -- MSB is sign bit
		end if;
		if(new_hit_q(0) = '1')then
      if(or_reduce(P(47 downto 36)) = '1' and and_reduce(P(47 downto 36)) = '0')then
        OOR(0) <= '1';
      elsif(P(36 downto 34) = "011" or P(36 downto 34) = "100")then
        OOR(0) <= '1';
      else
        OOR(0) <= '0';
      end if;
    end if;
		if(reset_q = '1' or new_hit_q(1) = '1')then
			r_high <= P(36 downto 21);
		end if;
		if(new_hit_q(1) = '1')then
      if(or_reduce(P(47 downto 36)) = '1' and and_reduce(P(47 downto 36)) = '0')then
        OOR(1) <= '1';
      elsif(P(36 downto 34) = "011" or P(36 downto 34) = "100")then
        OOR(1) <= '1';
      else
        OOR(1) <= '0';
      end if;
    end if;
		if(new_hit_q(1) = '1')then
			r_low_i <= r_low - ("00000" & R_drift_q(0) & '0');
		else
			r_low_i <= r_low  + ("00000" & R_drift_q(0) & '0');
		end if;
		if(new_hit_q(2) = '1')then
			r_high_i <= r_high - ("00000" & R_drift_q(1) & '0');
		else
			r_high_i <= r_high + ("00000" & R_drift_q(1) & '0');
		end if;
    r_low_out(16) <= OOR(0);
    r_high_out(16) <= OOR(1);		
  end if;
end process;
r_low_out(15 downto 0) <= r_low_i;
r_high_out(15 downto 0) <= r_high_i;
end Behavioral;
