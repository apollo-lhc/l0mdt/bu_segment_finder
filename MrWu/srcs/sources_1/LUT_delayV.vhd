----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/18/2018 11:53:29 AM
-- Design Name: 
-- Module Name: LUT_delayV - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity LUT_delayV is
  generic ( N: positive := 2; V: positive := 16);
  port (
      clk: in STD_LOGIC;
      d: in STD_LOGIC_vector(V-1 downto 0);
      ce: in STD_LOGIC;
      q: out STD_LOGIC_vector(V-1 downto 0)
       );
end LUT_delayV;

architecture Behavioral of LUT_delayV is
signal a: std_logic_vector(4 downto 0);
signal q_i: std_logic_vector(V-1 downto 0);
begin
a <= conv_std_logic_vector(N-2,5);
gt: for i in d'range generate
delay: srl16e
    port map(
           d => d(i),
           ce => ce,
           clk => clk,
           a0 => a(0),
           a1 => a(1),
           a2 => a(2),
           a3 => a(3),
           q => q_i(i));
end generate;      
process(clk)
begin
  if(clk'event and clk = '1')then
    if(ce = '1')then
      q <= q_i;
    end if;
  end if;
end process;
end Behavioral;
