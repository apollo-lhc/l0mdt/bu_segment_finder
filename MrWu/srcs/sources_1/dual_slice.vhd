----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/05/2018 12:33:15 PM
-- Design Name: 
-- Module Name: dual_theta_slice - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity dual_slice is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           new_hit : in STD_LOGIC;
           r : in STD_LOGIC_VECTOR (16 downto 0);
           r_h : in STD_LOGIC_VECTOR (16 downto 0);
					 r_e0 : out STD_LOGIC_VECTOR (7 downto 0);
					 r_e1 : out STD_LOGIC_VECTOR (7 downto 0);
					 r_o0 : out STD_LOGIC_VECTOR (7 downto 0);
					 r_o1 : out STD_LOGIC_VECTOR (7 downto 0);
           max : out STD_LOGIC_VECTOR (5 downto 0)
           );
end dual_slice;

architecture Behavioral of dual_slice is
component slice_RAM
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC; -- synchronous to clk
           we : in STD_LOGIC_VECTOR (3 downto 0);
           ae : in STD_LOGIC_VECTOR (7 downto 0);
           ao : in STD_LOGIC_VECTOR (7 downto 0);
           r_e : out STD_LOGIC_VECTOR (7 downto 0);
           r_o : out STD_LOGIC_VECTOR (7 downto 0);
           max : out STD_LOGIC_VECTOR (4 downto 0));
end component;
signal we : std_logic_vector(1 downto 0) := (others => '0');
signal slice_max : array2X5;
signal rao : array2X9;
signal rae : array2X9;
signal new_hit_q : std_logic_vector(2 downto 0) := (others => '0');
signal we_RAM : array2X4;
signal ae : array2X8;
signal ao : array2X8;
signal reset_q : std_logic_vector(1 downto 0) := (others => '0');
signal r_h_OOR : std_logic := '0';

begin
process(clk)
variable r_int : std_logic_vector(16 downto 0);
begin
r_int := (r(15) & r(15 downto 0)) + (r_h(15) & r_h(15 downto 0));
  if(clk'event and clk = '1')then
		reset_q <= reset_q(0) & reset;
		r_h_OOR <= r_h(16);
 		rao(0) <= r(13 downto 5);
		rao(1) <= r_int(14 downto 6);
		rae(0) <= r(13 downto 5) + 1;
		rae(1) <= r_int(14 downto 6) + 1;
		new_hit_q <= new_hit_q(1 downto 0) & new_hit;
		if(r(16) = '1')then
			we <= "00";
		else
			we(0) <= not r(15) and not r(14) and (new_hit_q(1) or new_hit_q(0));
			we(1) <= not r_h_OOR and not r_int(16) and not r_int(15) and (new_hit_q(1) or new_hit_q(0));
		end if;
		for i in 0 to 1 loop
			ao(i) <= rao(i)(8 downto 1);
			ae(i) <= rae(i)(8 downto 1);
			if(reset_q(0) = '1')then
				we_RAM(i)(3 downto 2) <= "11";
			elsif(new_hit_q(2) = '1' and ao(i) = rao(i)(8 downto 1))then
				we_RAM(i)(3 downto 2) <= "00";
			else
				we_RAM(i)(3) <= we(i) and rao(i)(8);
				we_RAM(i)(2) <= we(i) and not rao(i)(8);
			end if;
			if(reset_q(0) = '1')then
				we_RAM(i)(1 downto 0) <= "11";
			elsif(new_hit_q(2) = '1' and ae(i) = rae(i)(8 downto 1))then
				we_RAM(i)(1 downto 0) <= "00";
			else
				we_RAM(i)(1) <= we(i) and rae(i)(8);
				we_RAM(i)(0) <= we(i) and not rao(i)(8) and not rae(i)(8);
			end if;
		end loop;
		if(slice_max(0)(3 downto 0) > slice_max(1)(3 downto 0))then
			max <= '0' & slice_max(0);
		else
			max <= '1' & slice_max(1);
		end if;
  end if;
end process;
i_slice_ram0: slice_RAM
	port map(clk => clk,
		reset => reset_q(1),
		we => we_RAM(0),
		ae => ae(0),
		ao => ao(0),
		r_e => r_e0,
		r_o => r_o0,
		max => slice_max(0));
i_slice_ram1: slice_RAM
	port map(clk => clk,
		reset => reset_q(1),
		we => we_RAM(1),
		ae => ae(1),
		ao => ao(1),
		r_e => r_e1,
		r_o => r_o1,
		max => slice_max(1));
end Behavioral;
