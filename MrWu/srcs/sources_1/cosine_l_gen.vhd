----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/15/2018 10:34:53 AM
-- Design Name: 
-- Module Name: cosine_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity cosine_l_gen is
    Port ( clk : in STD_LOGIC;
           new_hit : in STD_LOGIC;
           theta0 : in STD_LOGIC_VECTOR (22 downto 0);-- this is actually theta - phi, the first theta bin. bit 23 is a sign bit
           we_uRAM : in STD_LOGIC;
					 din : in STD_LOGIC_VECTOR (71 downto 0);
           addr : in array2X12;
           cosine_l : out array33X11);
end cosine_l_gen;

architecture Behavioral of cosine_l_gen is
COMPONENT multiplier
    Port ( clk : in STD_LOGIC;
           A_in : in STD_LOGIC_VECTOR (12 downto 0);
           B_in : in STD_LOGIC_VECTOR (10 downto 0);
           P_out : out STD_LOGIC_VECTOR (12 downto 0));
END COMPONENT;
COMPONENT theta_0cross
  PORT (
    clka : IN STD_LOGIC;
    addra : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(20 DOWNTO 0)
  );
END COMPONENT;
signal sine_do : std_logic_vector(143 downto 0) := (others => '0');
signal sine_theta : std_logic_vector(116 downto 0) := (others => '0');
signal sine_addr : array2X23 := (others => (others => '0'));
signal delta_cos: array9X13;
signal delta_cos_q : array9X13;
signal cosine_l_i : array33X13;
signal theta0_LSB : std_logic_vector(10 downto 0) := (others => '0');
signal theta0_dl : std_logic_vector(18 downto 6) := (others => '0');
signal delta_cosine : std_logic_vector(20 downto 0) := (others => '0');
signal delta : array8X13;
signal theta_near0 : std_logic_vector(7 downto 0) := (others => '0');
signal sel_theta_near0 : std_logic_vector(3 downto 0) := (others => '0');
signal no_0cross : std_logic := '0';
signal no_0cross_dl : std_logic := '0';
signal new_hit_dl : std_logic := '0';
signal we_uRAM_q : std_logic := '0';
begin
g_cosine_l : for i in 0 to 32 generate
	cosine_l(i) <= cosine_l_i(i)(12 downto 2);
end generate;
process(clk)
begin
  if(clk'event and clk = '1')then
    theta0_LSB <= theta0(10 downto 0);
  end if;
end process;
i_sine : URAM288_BASE
generic map (
   AUTO_SLEEP_LATENCY => 8,            -- Latency requirement to enter sleep mode
   AVG_CONS_INACTIVE_CYCLES => 10,     -- Average concecutive inactive cycles when is SLEEP mode for power
                                       -- estimation
   BWE_MODE_A => "PARITY_INTERLEAVED", -- Port A Byte write control
   BWE_MODE_B => "PARITY_INTERLEAVED", -- Port B Byte write control
   EN_AUTO_SLEEP_MODE => "FALSE",      -- Enable to automatically enter sleep mode
   EN_ECC_RD_A => "FALSE",             -- Port A ECC encoder
   EN_ECC_RD_B => "FALSE",             -- Port B ECC encoder
   EN_ECC_WR_A => "FALSE",             -- Port A ECC decoder
   EN_ECC_WR_B => "FALSE",             -- Port B ECC decoder
   IREG_PRE_A => "FALSE",              -- Optional Port A input pipeline registers
   IREG_PRE_B => "FALSE",              -- Optional Port B input pipeline registers
   IS_CLK_INVERTED => '0',             -- Optional inverter for CLK
   IS_EN_A_INVERTED => '0',            -- Optional inverter for Port A enable
   IS_EN_B_INVERTED => '0',            -- Optional inverter for Port B enable
   IS_RDB_WR_A_INVERTED => '0',        -- Optional inverter for Port A read/write select
   IS_RDB_WR_B_INVERTED => '0',        -- Optional inverter for Port B read/write select
   IS_RST_A_INVERTED => '0',           -- Optional inverter for Port A reset
   IS_RST_B_INVERTED => '0',           -- Optional inverter for Port B reset
   OREG_A => "TRUE",                  -- Optional Port A output pipeline registers
   OREG_B => "TRUE",                  -- Optional Port B output pipeline registers
   OREG_ECC_A => "FALSE",              -- Port A ECC decoder output
   OREG_ECC_B => "FALSE",              -- Port B output ECC decoder
   RST_MODE_A => "SYNC",               -- Port A reset mode
   RST_MODE_B => "SYNC",               -- Port B reset mode
   USE_EXT_CE_A => "FALSE",            -- Enable Port A external CE inputs for output registers
   USE_EXT_CE_B => "FALSE"             -- Enable Port B external CE inputs for output registers
)
port map (
   DBITERR_A => open,               -- 1-bit output: Port A double-bit error flag status
   DBITERR_B => open,               -- 1-bit output: Port B double-bit error flag status
   DOUT_A => sine_do(71 downto 0),                     -- 72-bit output: Port A read data output
   DOUT_B => sine_do(143 downto 72),                     -- 72-bit output: Port B read data output
   SBITERR_A => open,               -- 1-bit output: Port A single-bit error flag status
   SBITERR_B => open,               -- 1-bit output: Port B single-bit error flag status
   ADDR_A => sine_addr(0),                     -- 23-bit input: Port A address
   ADDR_B => sine_addr(1),                     -- 23-bit input: Port B address
   BWE_A => "111111111",                       -- 9-bit input: Port A Byte-write enable
   BWE_B => "000000000",                       -- 9-bit input: Port B Byte-write enable
   CLK => clk,                           -- 1-bit input: Clock source
   DIN_A => din,                       -- 72-bit input: Port A write data input
   DIN_B => (others => '0'),                       -- 72-bit input: Port B write data input
   EN_A => '1',                         -- 1-bit input: Port A enable
   EN_B => '1',                         -- 1-bit input: Port B enable
   INJECT_DBITERR_A => '0', -- 1-bit input: Port A double-bit error injection
   INJECT_DBITERR_B => '0', -- 1-bit input: Port B double-bit error injection
   INJECT_SBITERR_A => '0', -- 1-bit input: Port A single-bit error injection
   INJECT_SBITERR_B => '0', -- 1-bit input: Port B single-bit error injection
   OREG_CE_A => '1',               -- 1-bit input: Port A output register clock enable
   OREG_CE_B => '1',               -- 1-bit input: Port B output register clock enable
   OREG_ECC_CE_A => '1',       -- 1-bit input: Port A ECC decoder output register clock enable
   OREG_ECC_CE_B => '1',       -- 1-bit input: Port B ECC decoder output register clock enable
   RDB_WR_A => we_uRAM_q,                 -- 1-bit input: Port A read/write select
   RDB_WR_B => '0',                 -- 1-bit input: Port B read/write select
   RST_A => '0',                       -- 1-bit input: Port A asynchronous or synchronous reset for output
                                         -- registers

   RST_B => '0',                       -- 1-bit input: Port B asynchronous or synchronous reset for output
                                         -- registers

   SLEEP => '0'                        -- 1-bit input: Dynamic power gating control
);
sine_addr(0)(11 downto 0) <= addr(0);
sine_addr(1)(11 downto 0) <= addr(1);
sine_theta <= sine_do(136 downto 72) & sine_do(51 downto 0);
g_multi : for i in 0 to 8 generate
  i_multi : multiplier
  port map(
    clk => clk,
    A_IN => sine_theta(i*13+12 downto i*13),
    B_IN => theta0_LSB,
    P_out => delta_cos(i) -- sign bit cosine(i)(12) is determined by sine_theta(i)*13+12)
  );
end generate g_multi;
process(clk)
begin
  if(clk'event and clk = '1')then
		we_uRAM_q <= we_uRAM;
    for i in 0 to 7 loop
      delta(i) <= delta_cos(i+1) - delta_cos(i);
    end loop;
    delta_cos_q <= delta_cos;
    for i in 0 to 7 loop
      cosine_l_i(i*4) <= delta_cos_q(i);
      if(theta_near0(i) = '1')then
        cosine_l_i(i*4+1)(12 downto 2) <= sign_extend(delta_cosine(6 downto 0),11);
        cosine_l_i(i*4+2)(12 downto 2) <= sign_extend(delta_cosine(13 downto 7),11);
        cosine_l_i(i*4+3)(12 downto 2) <= sign_extend(delta_cosine(20 downto 14),11);
      else
        cosine_l_i(i*4+1) <= delta_cos_q(i) + sign_extend(delta(i)(12 downto 2),13);
        cosine_l_i(i*4+2) <= delta_cos_q(i+1) - sign_extend(delta(i)(12 downto 1),13);
        cosine_l_i(i*4+3) <= delta_cos_q(i+1) - sign_extend(delta(i)(12 downto 2),13);
      end if;
    end loop;
    cosine_l_i(32) <= delta_cos_q(8);
  end if;
end process;
i_theta_0cross : theta_0cross
  PORT MAP (
    clka => clk,
    addra => theta0_dl(14 downto 6),
    douta => delta_cosine
  );
i_theta0_dl : LUT_delayV generic map(N => 6, V => 13)
  port map (
     Q => theta0_dl(18 downto 6),     -- 1-bit output: SRL Data
     CE => '1',   -- 1-bit input: Clock enable
     CLK => clk, -- 1-bit input: Clock
     D => theta0(18 downto 6)     -- 1-bit input: SRL Data
  );
i_new_hit_dl : LUT_delay generic map(N => 7)
port map (
   Q => new_hit_dl,     -- 1-bit output: SRL Data
   CE => '1',   -- 1-bit input: Clock enable
   CLK => clk, -- 1-bit input: Clock
   D => new_hit     -- 1-bit input: SRL Data
);
i_no_0cross_dl : LUT_delay generic map(N => 6)
port map (
   Q => no_0cross_dl,     -- 1-bit output: SRL Data
   CE => '1',   -- 1-bit input: Clock enable
   CLK => clk, -- 1-bit input: Clock
   D => no_0cross     -- 1-bit input: SRL Data
);
process(clk)
begin
  if(clk'event and clk = '1')then
    if(theta0(22 downto 19) = "1111")then
      no_0cross <= '0';
    else
      no_0cross <= '1';
    end if;
    sel_theta_near0 <= theta0_dl(18 downto 15);
    if(no_0cross_dl = '1' or sel_theta_near0(3) /= new_hit_dl)then--0cross is there only when theta0 is negative, so theta0 is 2's complement
      theta_near0 <= (others => '0');
    else
      case sel_theta_near0(2 downto 0) is
        when "000" => theta_near0 <= x"80";
        when "001" => theta_near0 <= x"40";
        when "010" => theta_near0 <= x"20";
        when "011" => theta_near0 <= x"10";
        when "100" => theta_near0 <= x"08";
        when "101" => theta_near0 <= x"04";
        when "110" => theta_near0 <= x"02";
        when others => theta_near0 <= x"01";
      end case;
    end if;
  end if;
end process;
end Behavioral;
