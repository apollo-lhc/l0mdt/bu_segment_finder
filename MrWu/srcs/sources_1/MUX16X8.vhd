----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/05/2018 12:33:15 PM
-- Design Name: 
-- Module Name: dual_theta_slice - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity MUX16X8 is
    Port ( clk : in STD_LOGIC;
           r_e : in array8X8;
           r_o : in array8X8;
           s : in STD_LOGIC_VECTOR (3 downto 0);
           dout : out STD_LOGIC_VECTOR (11 downto 0)
           );
end MUX16X8;

architecture Behavioral of MUX16X8 is
component MUX16
    Port ( clk : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (15 downto 0);
           s : in STD_LOGIC_VECTOR (3 downto 0);
           dout : out STD_LOGIC);
end component;
signal mux16_din : array8X16;
begin
g_mux16 : for i in 0 to 7 generate
  i_mux16 : MUX16
    port map(clk => clk, din => mux16_din(i), s => s, dout => dout(i));
	g_mux16_din : for j in 0 to 7 generate
		mux16_din(i)(2*j) <= r_e(j)(i);
		mux16_din(i)(2*j+1) <= r_o(j)(i);
	end generate g_mux16_din;
end generate;
process(clk)
begin
  if(clk'event and clk = '1')then
    dout(11 downto 8) <= s;
  end if;
end process;

end Behavioral;
