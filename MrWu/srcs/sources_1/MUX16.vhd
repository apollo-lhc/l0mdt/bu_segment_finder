----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/01/2018 11:02:51 AM
-- Design Name: 
-- Module Name: slice_RAM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity MUX16 is
    Port ( clk : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (15 downto 0);
           s : in STD_LOGIC_VECTOR (3 downto 0);
           dout : out STD_LOGIC);
end MUX16;

architecture Behavioral of MUX16 is
signal MUXF7_O : std_logic_vector(1 downto 0) := (others => '0');
signal MUXF8_O : std_logic := '0';
signal do : std_logic_vector(3 downto 0) := (others => '0');
begin
process(clk)
begin
  if(clk'event and clk = '1')then
    dout <= MUXF8_O;
  end if;
end process;
i_MUXF8 : MUXF8
  port map (
     O => MUXF8_O,   -- 1-bit output: Output of MUX
     I0 => MUXF7_O(0), -- 1-bit input: Connect to LUT6 output
     I1 => MUXF7_O(1), -- 1-bit input: Connect to LUT6 output
     S => s(3)    -- 1-bit input: Input select to MUX
  );
g_MUXF7: for i in 0 to 1 generate
  i_MUXF7 : MUXF7
  port map (
     O => MUXF7_O(i),   -- 1-bit output: Output of MUX
     I0 => do(i*2), -- 1-bit input: Connect to LUT6 output
     I1 => do(i*2+1), -- 1-bit input: Connect to LUT6 output
     S => s(2)    -- 1-bit input: Input select to MUX
  );
end generate;
process(s, din)
begin
	for i in 0 to 3 loop
		case s(1 downto 0) is
			when "00" => do(i) <= din(i*4);
			when "01" => do(i) <= din(i*4+1);
			when "10" => do(i) <= din(i*4+2);
			when others => do(i) <= din(i*4+3);
		end case;
	end loop;
end process;
end Behavioral;
