----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:16:16 07/23/2018 
-- Design Name: 
-- Module Name:    quad_adder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity quad_adder is
    Port ( reset : in  STD_LOGIC;
           di : in  STD_LOGIC_VECTOR (15 downto 0);
           ci : in  STD_LOGIC_VECTOR (3 downto 0);
           do : out  STD_LOGIC_VECTOR (15 downto 0));
end quad_adder;

architecture Behavioral of quad_adder is

begin
process(reset,di,ci)
begin
	for i in 0 to 3 loop
		do(i) <= not reset and (di(i) xor ci(i));
		do(i+4) <= not reset and (di(i+4) xor (di(i) and ci(i)));
		do(i+8) <= not reset and (di(i+8) xor (di(i+4) and di(i) and ci(i)));
		do(i+12) <= not reset and (di(i+12) xor (di(i+8) and di(i+4) and di(i) and ci(i)));
	end loop;
end process;

end Behavioral;

