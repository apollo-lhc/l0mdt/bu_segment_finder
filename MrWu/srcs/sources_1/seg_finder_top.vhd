----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/29/2018 01:25:36 PM
-- Design Name: 
-- Module Name: cosine_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;
Library xpm;
use xpm.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity seg_finder_top is
  Port (
-- GTREFCLK_IN
				gtrefclkn_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
				gtrefclkp_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
-- GTH
				gthrxn_in : IN STD_LOGIC_VECTOR(43 DOWNTO 0);
				gthrxp_in : IN STD_LOGIC_VECTOR(43 DOWNTO 0);
				gthtxn_out : OUT STD_LOGIC_VECTOR(43 DOWNTO 0);
				gthtxp_out : OUT STD_LOGIC_VECTOR(43 DOWNTO 0);
-- GTY
				gtyrxn_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
				gtyrxp_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
				gtytxn_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				gtytxp_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
-- system
				sysclkp_in : in std_logic;
				sysclkn_in : in std_logic
				);
end seg_finder_top;

architecture Behavioral of seg_finder_top is
component sine_rom_init
    Port ( clk : in STD_LOGIC;
           MMCM_locked : in STD_LOGIC;
           fifo_busy : in STD_LOGIC;
					 dout : out STD_LOGIC_VECTOR (12 downto 0);
					 addr : out STD_LOGIC_VECTOR (11 downto 0);
           dout_valid : out STD_LOGIC;
           load_uRAM : out STD_LOGIC;
           we_uRAM : out STD_LOGIC;
           init_done : out STD_LOGIC
           );
end component;
component seg_finder
    Port ( clk : in STD_LOGIC;
			sysclk : in STD_LOGIC;
			MMCM_locked : in STD_LOGIC;
			reset_RAM_in : in STD_LOGIC;
			fifo_reset : in STD_LOGIC;
			fifo_busy : out STD_LOGIC;
			load_uRAM_in : in STD_LOGIC;
			we_uRAM_in : in STD_LOGIC;
--			sine_data_valid : in STD_LOGIC;
			sine_init_data: in STD_LOGIC_VECTOR (12 downto 0);
			theta_in: in STD_LOGIC_VECTOR (22 downto 0);-- this is in fact theta - phi0, it must be >= -0.96 radian  and < 0.96 radian. bit 22 is sign bit, bit 21 = 0.512 radian
			data_valid_in : in STD_LOGIC;-- valid hit present
			r_in : in STD_LOGIC_VECTOR (22 downto 0);-- positive number r0(5) = 0.25mm
			R_drift_in : in STD_LOGIC_VECTOR (9 downto 0);-- R_drift(4) = 0.25mm
			seg_pm : out STD_LOGIC_VECTOR (48 downto 0);-- 22 bit theta & 23 bit r & 4 bit count
			ready : out STD_LOGIC
     );
end component;
COMPONENT input_FIFO
  PORT (
    srst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;
COMPONENT output_FIFO
  PORT (
    srst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    prog_full : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;
COMPONENT GTY_FIFO
  PORT (
    srst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(79 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(79 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;
constant zero : std_logic_vector(0 downto 0) := (others => '0');
constant one : std_logic_vector(43 downto 0) := (others => '1');
signal sysclk_in : std_logic := '0';
signal sysclk : std_logic := '0';
signal clk : std_logic := '0';
signal clk_freerun_in : std_logic := '0';
signal CLKFB_MMCM : std_logic := '0';
signal reset_MMCM : std_logic := '0';
signal clk_MMCM : std_logic := '0';
signal MMCM_not_locked : std_logic;
signal sysclk_MMCM : std_logic := '0';
signal freerun_clk_MMCM : std_logic := '0';
signal MMCM_locked : std_logic := '0';
signal MMCM_locked_sync : std_logic := '0';
signal MMCM_locked_n : std_logic := '0';
signal startup : std_logic := '0';
signal sine_init_done : std_logic;
signal fifo_reset : std_logic := '0';
signal fifo_busy : std_logic_vector(15 downto 0);
signal clk_cep : std_logic_vector(7 downto 0);
signal clk_ce : std_logic_vector(7 downto 0);
signal clk_gated : std_logic_vector(7 downto 0);
signal reset_RAM : std_logic_vector(15 downto 0);
signal ready : std_logic_vector(15 downto 0);
signal sine_init_dout : std_logic_vector(12 downto 0);
signal sine_init_dout_valid : std_logic;
signal sine_init_addr : std_logic_vector(11 downto 0);
signal we_uRAM : std_logic;
signal load_uRAM : std_logic;
signal R_drift : array16X10;
signal seg_pm : array16X49;
signal r_in : array16X23;
signal theta_in : array16X23;
signal data_valid_in : std_logic_vector(15 downto 0);
signal gtrefclk : std_logic_vector(8 downto 0);
signal gtrefclk00_in : std_logic_vector(18 downto 0);
signal retry_ctr_out : array3X4;
signal gtwiz_userclk_tx_reset_in : std_logic_vector(2 downto 0);
signal gtwiz_userclk_rx_reset_in : std_logic_vector(2 downto 0);
signal txpmaresetdone_out : std_logic_vector(75 downto 0);
signal rxpmaresetdone_out : std_logic_vector(75 downto 0);
signal txprgdivresetdone_out : std_logic_vector(19 downto 0);
signal gtwiz_userclk_tx_usrclk2_out : std_logic_vector(2 downto 0); -- driving tx_data source
signal gtwiz_userclk_tx_active_out : std_logic_vector(2 downto 0); -- driving tx_data source
signal gtwiz_userclk_rx_usrclk2_out : std_logic_vector(2 downto 0); -- driving tx_data source
signal gtwiz_userclk_rx_active_out : std_logic_vector(2 downto 0); -- driving tx_data source
signal gtwiz_reset_clk_freerun_in : std_logic_vector(0 downto 0); -- driving tx_data source
signal init_done_out : std_logic_vector(2 downto 0); -- driving tx_data source
signal gtwiz_reset_rx_datapath_init : std_logic_vector(2 downto 0); -- driving tx_data source
signal gtwiz_reset_all_init_out : std_logic_vector(2 downto 0); -- driving tx_data source
signal gtwiz_reset_all_in : std_logic_vector(2 downto 0); -- driving tx_data source
signal rx_data_bad : std_logic_vector(55 downto 0); -- driving tx_data source
signal rx_data_good : std_logic_vector(2 downto 0); -- driving tx_data source
signal rx_data_good_in : std_logic_vector(2 downto 0); -- driving tx_data source
signal rx_data_good_sync0 : std_logic_vector(3 downto 0); -- driving tx_data source
signal rx_data_good_sync1 : std_logic_vector(3 downto 0); -- driving tx_data source
signal gtwiz_reset_tx_done : std_logic_vector(2 downto 0); -- driving tx_data source
signal gtwiz_reset_rx_done : std_logic_vector(2 downto 0); -- driving tx_data source
signal txctrl0_in : std_logic_vector(895 downto 0) := (others => '0');
signal txctrl1_in : std_logic_vector(895 downto 0) := (others => '0');
signal txctrl2_in : std_logic_vector(447 downto 0) := (others => '0');
signal rxctrl0_out : std_logic_vector(895 downto 0);
signal rxctrl1_out : std_logic_vector(895 downto 0);
signal rxctrl2_out : std_logic_vector(447 downto 0);
signal rxctrl3_out : std_logic_vector(447 downto 0);
signal gtwiz_userdata_tx_in : std_logic_vector(3391 downto 0);
signal gtwiz_userdata_rx_out : std_logic_vector(3391 downto 0);
signal input_FIFO_din : std_logic_vector(1791 downto 0);
signal input_FIFO_dout : std_logic_vector(1791 downto 0);
signal output_FIFO_din : std_logic_vector(1791 downto 0);
signal output_FIFO_dout : std_logic_vector(1791 downto 0);
signal input_FIFO_wr_en : std_logic_vector(27 downto 0);
signal input_FIFO_rd_en : std_logic_vector(27 downto 0);
signal output_FIFO_wr_en : std_logic_vector(27 downto 0);
signal output_FIFO_rd_en : std_logic_vector(27 downto 0);
signal input_FIFO_full : std_logic_vector(27 downto 0);
signal input_FIFO_empty : std_logic_vector(27 downto 0);
signal input_FIFO_valid : std_logic_vector(27 downto 0);
signal output_FIFO_full : std_logic_vector(27 downto 0);
signal output_FIFO_empty : std_logic_vector(27 downto 0);
signal output_FIFO_valid : std_logic_vector(27 downto 0);
signal output_FIFO_prog_full : std_logic_vector(27 downto 0);
signal GTY_FIFO_wr_en : std_logic_vector(19 downto 0);
signal GTY_FIFO_rd_en : std_logic_vector(19 downto 0);
signal GTY_FIFO_full : std_logic_vector(19 downto 0);
signal GTY_FIFO_empty : std_logic_vector(19 downto 0);
begin
i_reset_MMCM : SRL16E
   generic map (
      INIT => X"ffff")
   port map (
      Q => reset_MMCM,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => sysclk_in,   -- Clock input
      D => '0'        -- SRL data input
   );
i_sysclk : IBUFDS
   port map (
      O => sysclk_in,   -- 1-bit output: Buffer output
      I => sysclkp_in,   -- 1-bit input: Diff_p buffer input (connect directly to top-level port)
      IB => sysclkn_in  -- 1-bit input: Diff_n buffer input (connect directly to top-level port)
   );
i_sysMMCM : MMCME4_BASE
   generic map (
      CLKFBOUT_MULT_F => 9.0,     -- Multiply value for all CLKOUT
      CLKIN1_PERIOD => 10.0,       -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      CLKOUT0_DIVIDE_F => 18.0,    -- Divide amount for CLKOUT0
      CLKOUT1_DIVIDE => 2,        -- Divide amount for CLKOUT (1-128)
      CLKOUT2_DIVIDE => 4        -- Divide amount for CLKOUT (1-128)
   )
   port map (
      CLKFBOUT => CLKFB_MMCM,   -- 1-bit output: Feedback clock pin to the MMCM
      CLKOUT0 => freerun_clk_MMCM,     -- 1-bit output: CLKOUT0
      CLKOUT1 => clk_MMCM,     -- 1-bit output: CLKOUT1
      CLKOUT2 => sysclk_MMCM,     -- 1-bit output: CLKOUT1
      LOCKED => MMCM_locked,       -- 1-bit output: LOCK
      CLKFBIN => CLKFB_MMCM,     -- 1-bit input: Feedback clock pin to the MMCM
      CLKIN1 => sysclk_in,       -- 1-bit input: Primary clock
      PWRDWN => '0',       -- 1-bit input: Power-down
      RST => reset_MMCM              -- 1-bit input: Reset
   );
i_buf_clk : BUFG port map(i => clk_MMCM, o => clk);
i_buf_sysclk : BUFG port map(i => sysclk_MMCM, o => sysclk);
i_buf_free_run_clk : BUFG port map(i => freerun_clk_MMCM, o => clk_freerun_in);
g_clk_gated : for i in 0 to 7 generate
   i_BUFGCE : BUFGCE
			port map (
				 O => clk_gated(i),   -- 1-bit output: Buffer
				 CE => clk_ce(i), -- 1-bit input: Buffer enable
				 I => clk    -- 1-bit input: Buffer
			);
end generate g_clk_gated;
g_sf : for i in 0 to 15 generate
	i_sf: seg_finder
		port map(
			clk => clk_gated(i/2),
			sysclk => sysclk,
			MMCM_locked => MMCM_locked,
			reset_RAM_in => reset_RAM(i),
			fifo_reset => fifo_reset,
			fifo_busy => fifo_busy(i),
			load_uRAM_in => load_uRAM,
			we_uRAM_in => we_uRAM,
--			sine_data_valid => sine_init_dout_valid,
			sine_init_data => sine_init_dout,
			theta_in => theta_in(i),
			data_valid_in => data_valid_in(i),
			r_in => r_in(i),
			R_drift_in => R_drift(i),
			seg_pm => seg_pm(i),
			ready => ready(i)
			);
end generate g_sf;
i_MMCM_not_locked : xpm_cdc_async_rst
   generic map (
      DEST_SYNC_FF => 4,    -- DECIMAL; range: 2-10
      INIT_SYNC_FF => 0,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      RST_ACTIVE_HIGH => 0  -- DECIMAL; 0=active low reset, 1=active high reset
   )
   port map (
      dest_arst => MMCM_locked_sync, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination
                              -- clock domain. This output is registered. NOTE: Signal asserts asynchronously
                              -- but deasserts synchronously to dest_clk. Width of the reset signal is at least
                              -- (DEST_SYNC_FF*dest_clk) period.

      dest_clk => sysclk,   -- 1-bit input: Destination clock.
      src_arst => MMCM_locked    -- 1-bit input: Source asynchronous reset signal.
   );
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		MMCM_locked_n <= not MMCM_locked_sync;
		fifo_reset <= MMCM_locked_n and MMCM_locked_sync;
		startup <= MMCM_locked_sync and (startup or MMCM_locked_n) and not sine_init_done;
	end if;
end process;
i_sine_rom_init : sine_rom_init
	port map(
		clk => sysclk,
		MMCM_locked => MMCM_locked,
		fifo_busy => fifo_busy(0),
		dout => sine_init_dout,
		dout_valid => sine_init_dout_valid,
		addr => sine_init_addr,
		load_uRAM => load_uRAM,
		we_uRAM => we_uRAM,
		init_done => sine_init_done
		);
g_IBUFDS_GT : for i in 0 to 8 generate
	i_IBUFDS_GT : IBUFDS_GTE4
   generic map (
      REFCLK_EN_TX_PATH => '0',   -- Refer to Transceiver User Guide
      REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
      REFCLK_ICNTL_RX => "00"     -- Refer to Transceiver User Guide
   )
   port map (
      O => gtrefclk(i),         -- 1-bit output: Refer to Transceiver User Guide
      ODIV2 => open, -- 1-bit output: Refer to Transceiver User Guide
      CEB => '0',     -- 1-bit input: Refer to Transceiver User Guide
      I => gtrefclkp_in(i),         -- 1-bit input: Refer to Transceiver User Guide
      IB => gtrefclkn_in(i)        -- 1-bit input: Refer to Transceiver User Guide
   );
end generate g_IBUFDS_GT;
gtrefclk00_in(3 downto 0) <= gtrefclk(0) & gtrefclk(0) & gtrefclk(0) & gtrefclk(0);
gtrefclk00_in(7 downto 4) <= gtrefclk(1) & gtrefclk(1) & gtrefclk(1) & gtrefclk(1);
gtrefclk00_in(10 downto 8) <= gtrefclk(2) & gtrefclk(2) & gtrefclk(2);
gtrefclk00_in(13 downto 11) <= gtrefclk(3) & gtrefclk(3) & gtrefclk(3);
gtrefclk00_in(18 downto 14) <= gtrefclk(8 downto 4);
gtwiz_reset_clk_freerun_in(0) <= MMCM_locked_n;
gtwiz_userclk_tx_reset_in(0) <= not and_reduce(txpmaresetdone_out(43 downto 0));
gtwiz_userclk_rx_reset_in(0) <= not and_reduce(rxpmaresetdone_out(43 downto 0));
i_GTH_wrapper : entity work.GTH_example_wrapper
	port map(
		gthrxn_in => gthrxn_in,
		gthrxp_in => gthrxp_in,
		gthtxn_out => gthtxn_out,
		gthtxp_out => gthtxp_out,
		gtwiz_userclk_tx_reset_in => gtwiz_userclk_tx_reset_in(0 downto 0),
		gtwiz_userclk_tx_srcclk_out => open,
		gtwiz_userclk_tx_usrclk_out => open,
		gtwiz_userclk_tx_usrclk2_out => gtwiz_userclk_tx_usrclk2_out(0 downto 0),
		gtwiz_userclk_tx_active_out => gtwiz_userclk_tx_active_out(0 downto 0),
		gtwiz_userclk_rx_reset_in => gtwiz_userclk_rx_reset_in(0 downto 0),
		gtwiz_userclk_rx_srcclk_out => open,
		gtwiz_userclk_rx_usrclk_out => open,
		gtwiz_userclk_rx_usrclk2_out => gtwiz_userclk_rx_usrclk2_out(0 downto 0),
		gtwiz_userclk_rx_active_out => gtwiz_userclk_rx_active_out(0 downto 0),
		gtwiz_reset_clk_freerun_in => gtwiz_reset_clk_freerun_in(0 downto 0),
		gtwiz_reset_all_in => gtwiz_reset_all_in(0 downto 0),
		gtwiz_reset_tx_pll_and_datapath_in => zero,
		gtwiz_reset_tx_datapath_in => zero,
		gtwiz_reset_rx_pll_and_datapath_in => zero,
		gtwiz_reset_rx_datapath_in => gtwiz_reset_rx_datapath_init(0 downto 0),
		gtwiz_reset_rx_cdr_stable_out => open,
		gtwiz_reset_tx_done_out => gtwiz_reset_tx_done(0 downto 0),
		gtwiz_reset_rx_done_out => gtwiz_reset_rx_done(0 downto 0),
		gtwiz_userdata_tx_in => gtwiz_userdata_tx_in(1407 downto 0),
		gtwiz_userdata_rx_out => gtwiz_userdata_rx_out(1407 downto 0),
		gtrefclk00_in => gtrefclk00_in(10 downto 0),
		qpll0outclk_out => open,
		qpll0outrefclk_out => open,
		rx8b10ben_in => one,
		tx8b10ben_in => one,
		txctrl0_in => txctrl0_in(703 downto 0),
		txctrl1_in => txctrl1_in(703 downto 0),
		txctrl2_in => txctrl2_in(351 downto 0),
		gtpowergood_out => open,
		rxctrl0_out => rxctrl0_out(703 downto 0),
		rxctrl1_out => rxctrl1_out(703 downto 0),
		rxctrl2_out => rxctrl2_out(351 downto 0),
		rxctrl3_out => rxctrl3_out(351 downto 0),
		rxpmaresetdone_out => rxpmaresetdone_out(43 downto 0),
		txpmaresetdone_out => txpmaresetdone_out(43 downto 0)
		);
gtwiz_userclk_tx_reset_in(1) <= not and_reduce(txpmaresetdone_out(55 downto 44));
gtwiz_userclk_rx_reset_in(1) <= not and_reduce(rxpmaresetdone_out(55 downto 44));
i_GTY_slow_wrapper : entity work.GTY_slow_example_wrapper
	port map(
		gtyrxn_in => gtyrxn_in(11 DOWNTO 0),
		gtyrxp_in => gtyrxp_in(11 DOWNTO 0),
		gtytxn_out => gtytxn_out(11 DOWNTO 0),
		gtytxp_out => gtytxp_out(11 DOWNTO 0),
		gtwiz_userclk_tx_reset_in => gtwiz_userclk_tx_reset_in(1 downto 1),
		gtwiz_userclk_tx_srcclk_out => open,
		gtwiz_userclk_tx_usrclk_out => open,
		gtwiz_userclk_tx_usrclk2_out => gtwiz_userclk_tx_usrclk2_out(1 downto 1),
		gtwiz_userclk_tx_active_out => gtwiz_userclk_tx_active_out(1 downto 1),
		gtwiz_userclk_rx_reset_in => gtwiz_userclk_rx_reset_in(1 downto 1),
		gtwiz_userclk_rx_srcclk_out => open,
		gtwiz_userclk_rx_usrclk_out => open,
		gtwiz_userclk_rx_usrclk2_out => gtwiz_userclk_rx_usrclk2_out(1 downto 1),
		gtwiz_userclk_rx_active_out => gtwiz_userclk_rx_active_out(1 downto 1),
		gtwiz_reset_clk_freerun_in => gtwiz_reset_clk_freerun_in(0 downto 0),
		gtwiz_reset_all_in => gtwiz_reset_all_in(1 downto 1),
		gtwiz_reset_tx_pll_and_datapath_in => zero,
		gtwiz_reset_tx_datapath_in => zero,
		gtwiz_reset_rx_pll_and_datapath_in => zero,
		gtwiz_reset_rx_datapath_in => gtwiz_reset_rx_datapath_init(1 downto 1),
		gtwiz_reset_rx_cdr_stable_out => open,
		gtwiz_reset_tx_done_out => gtwiz_reset_tx_done(1 downto 1),
		gtwiz_reset_rx_done_out => gtwiz_reset_rx_done(1 downto 1),
		gtwiz_userdata_tx_in => gtwiz_userdata_tx_in(1791 downto 1408),
		gtwiz_userdata_rx_out => gtwiz_userdata_rx_out(1791 downto 1408),
		gtrefclk00_in => gtrefclk00_in(13 downto 11),
		qpll0outclk_out => open,
		qpll0outrefclk_out => open,
		rx8b10ben_in => one(11 downto 0),
		rxcommadeten_in => one(11 downto 0),
		rxmcommaalignen_in => one(11 downto 0),
		rxpcommaalignen_in => one(11 downto 0),
		tx8b10ben_in => one(11 downto 0),
		txctrl0_in => txctrl0_in(895 downto 704),
		txctrl1_in => txctrl1_in(895 downto 704),
		txctrl2_in => txctrl2_in(447 downto 352),
		gtpowergood_out => open,
		rxbyteisaligned_out => open,
		rxbyterealign_out => open,
		rxcommadet_out => open,
		rxctrl0_out => rxctrl0_out(895 downto 704),
		rxctrl1_out => rxctrl1_out(895 downto 704),
		rxctrl2_out => rxctrl2_out(447 downto 352),
		rxctrl3_out => rxctrl3_out(447 downto 352),
		rxpmaresetdone_out => rxpmaresetdone_out(55 downto 44),
		txpmaresetdone_out => txpmaresetdone_out(55 downto 44)
		);
gtwiz_userclk_tx_reset_in(2) <= not (and_reduce(txprgdivresetdone_out) and and_reduce(txpmaresetdone_out(75 downto 56)));
gtwiz_userclk_rx_reset_in(2) <= not and_reduce(rxpmaresetdone_out(75 downto 56));
i_GTY_wrapper : entity work.GTY_example_wrapper
      port map(
        gtyrxn_in => gtyrxn_in(31 DOWNTO 12),
        gtyrxp_in => gtyrxp_in(31 DOWNTO 12),
        gtytxn_out => gtytxn_out(31 DOWNTO 12),
        gtytxp_out => gtytxp_out(31 DOWNTO 12),
        gtwiz_userclk_tx_reset_in => gtwiz_userclk_tx_reset_in(2 downto 2),
        gtwiz_userclk_tx_srcclk_out => open,
        gtwiz_userclk_tx_usrclk_out => open,
        gtwiz_userclk_tx_usrclk2_out => gtwiz_userclk_tx_usrclk2_out(2 downto 2),
        gtwiz_userclk_tx_active_out => gtwiz_userclk_tx_active_out(2 downto 2),
        gtwiz_userclk_rx_reset_in => gtwiz_userclk_rx_reset_in(2 downto 2),
        gtwiz_userclk_rx_srcclk_out => open,
        gtwiz_userclk_rx_usrclk_out => open,
        gtwiz_userclk_rx_usrclk2_out => gtwiz_userclk_rx_usrclk2_out(2 downto 2),
        gtwiz_userclk_rx_active_out => gtwiz_userclk_rx_active_out(2 downto 2),
        gtwiz_reset_clk_freerun_in => gtwiz_reset_clk_freerun_in(0 downto 0),
        gtwiz_reset_all_in => gtwiz_reset_all_in(2 downto 2),
        gtwiz_reset_tx_pll_and_datapath_in => zero,
        gtwiz_reset_tx_datapath_in => zero,
        gtwiz_reset_rx_pll_and_datapath_in => zero,
        gtwiz_reset_rx_datapath_in => gtwiz_reset_rx_datapath_init(2 downto 2),
        gtwiz_reset_rx_cdr_stable_out => open,
        gtwiz_reset_tx_done_out => gtwiz_reset_tx_done(2 downto 2),
        gtwiz_reset_rx_done_out => gtwiz_reset_rx_done(2 downto 2),
        gtwiz_userdata_tx_in => gtwiz_userdata_tx_in(3391 downto 1792),
        gtwiz_userdata_rx_out => gtwiz_userdata_rx_out(3391 downto 1792),
        gtrefclk00_in => gtrefclk00_in(18 downto 14),
        qpll0outclk_out => open,
        qpll0outrefclk_out => open,
        gtpowergood_out => open,
        rxpmaresetdone_out => rxpmaresetdone_out(75 downto 56),
        txpmaresetdone_out => txpmaresetdone_out(75 downto 56),
        txprgdivresetdone_out => txprgdivresetdone_out(19 downto 0)
        );
g_init : for i in 0 to 2 generate
i_GT_init : entity work.GTH_example_init
	port map(
		clk_freerun_in => clk_freerun_in,
		reset_all_in	=> gtwiz_reset_all_in(i),
		tx_init_done_in => gtwiz_reset_tx_done(i),
		rx_init_done_in => gtwiz_reset_rx_done(i),
		rx_data_good_in => rx_data_good_in(i),
		reset_all_out => gtwiz_reset_all_init_out(i),
		reset_rx_out => gtwiz_reset_rx_datapath_init(i),
		init_done_out => init_done_out(i),
		retry_ctr_out => retry_ctr_out(i)
		);
end generate g_init;
process(gtwiz_userclk_rx_usrclk2_out)
begin
	if(gtwiz_userclk_rx_usrclk2_out(0)'event and gtwiz_userclk_rx_usrclk2_out(0) = '1')then
		rx_data_good(0) <= not or_reduce(rx_data_bad(43 downto 0));
		for i in 0 to 43 loop
				rx_data_bad(i) <= or_reduce(rxctrl3_out(i*8+3 downto i*8));
		end loop;
	end if;
end process;
process(gtwiz_userclk_rx_usrclk2_out)
begin
	if(gtwiz_userclk_rx_usrclk2_out(1)'event and gtwiz_userclk_rx_usrclk2_out(1) = '1')then
		rx_data_good(1) <= not or_reduce(rx_data_bad(55 downto 44));
    for i in 44 to 55 loop
        rx_data_bad(i) <= or_reduce(rxctrl3_out(i*8+3 downto i*8));
    end loop;
	end if;
end process;
process(clk_freerun_in,rx_data_good)
begin
	if(rx_data_good(0) = '0')then
		rx_data_good_sync0 <= x"0";
	elsif(clk_freerun_in'event and clk_freerun_in = '1')then
		rx_data_good_sync0 <= rx_data_good_sync0(2 downto 0) & '1';
	end if;
end process;
rx_data_good_in(0) <= rx_data_good_sync0(3);
process(clk_freerun_in,rx_data_good)
begin
	if(rx_data_good(1) = '0')then
		rx_data_good_sync1 <= x"0";
	elsif(clk_freerun_in'event and clk_freerun_in = '1')then
		rx_data_good_sync1 <= rx_data_good_sync1(2 downto 0) & '1';
	end if;
end process;
rx_data_good_in(1) <= rx_data_good_sync1(3);
gtwiz_reset_all_in <= "111" when MMCM_locked = '0' else gtwiz_reset_all_init_out;
g_GTH_FIFO : for i in 0 to 21 generate
	i_input_FIFO : input_FIFO
		PORT MAP (
			srst => gtwiz_reset_all_in(0),
			wr_clk => gtwiz_userclk_rx_usrclk2_out(0),
			rd_clk => sysclk,
			din => input_FIFO_din(i*64+63 downto i*64),
			wr_en => input_FIFO_wr_en(i),
			rd_en => input_FIFO_rd_en(i),
			dout => input_FIFO_dout(i*64+63 downto i*64),
			full => input_FIFO_full(i),
			empty => input_FIFO_empty(i),
			valid => input_FIFO_valid(i),
			wr_rst_busy => open,
			rd_rst_busy => open
		);
	i_output_FIFO : output_FIFO
		PORT MAP (
			srst => gtwiz_reset_all_in(0),
			wr_clk => sysclk,
			rd_clk => gtwiz_userclk_tx_usrclk2_out(0),
			din => output_FIFO_din(i*64+63 downto i*64),
			wr_en => output_FIFO_wr_en(i),
			rd_en => output_FIFO_rd_en(i),
			dout => output_FIFO_dout(i*64+63 downto i*64),
			full => output_FIFO_full(i),
			empty => output_FIFO_empty(i),
			valid => output_FIFO_valid(i),
			prog_full => output_FIFO_prog_full(i),
			wr_rst_busy => open,
			rd_rst_busy => open
		);
	input_FIFO_wr_en(i) <= not input_FIFO_full(i);	
	input_FIFO_rd_en(i) <= input_FIFO_valid(i);	
	output_FIFO_wr_en(i) <= not output_FIFO_full(i);	
	output_FIFO_rd_en(i) <= output_FIFO_valid(i);
end generate g_GTH_FIFO;
g_GTY_slow_FIFO : for i in 22 to 27 generate
	i_input_FIFO : input_FIFO
		PORT MAP (
			srst => gtwiz_reset_all_in(1),
			wr_clk => gtwiz_userclk_rx_usrclk2_out(1),
			rd_clk => sysclk,
			din => input_FIFO_din(i*64+63 downto i*64),
			wr_en => input_FIFO_wr_en(i),
			rd_en => input_FIFO_rd_en(i),
			dout => input_FIFO_dout(i*64+63 downto i*64),
			full => input_FIFO_full(i),
			empty => input_FIFO_empty(i),
			valid => input_FIFO_valid(i),
			wr_rst_busy => open,
			rd_rst_busy => open
		);
	i_output_FIFO : output_FIFO
		PORT MAP (
			srst => gtwiz_reset_all_in(1),
			wr_clk => sysclk,
			rd_clk => gtwiz_userclk_tx_usrclk2_out(1),
			din => output_FIFO_din(i*64+63 downto i*64),
			wr_en => output_FIFO_wr_en(i),
			rd_en => output_FIFO_rd_en(i),
			dout => output_FIFO_dout(i*64+63 downto i*64),
			full => output_FIFO_full(i),
			empty => output_FIFO_empty(i),
			valid => output_FIFO_valid(i),
			prog_full => output_FIFO_prog_full(i),
			wr_rst_busy => open,
			rd_rst_busy => open
		);
	input_FIFO_wr_en(i) <= not input_FIFO_full(i);	
	input_FIFO_rd_en(i) <= input_FIFO_valid(i);	
	output_FIFO_wr_en(i) <= not output_FIFO_full(i);	
	output_FIFO_rd_en(i) <= output_FIFO_valid(i);
end generate g_GTY_slow_FIFO;
input_FIFO_din <= gtwiz_userdata_rx_out(1791 downto 0);
gtwiz_userdata_tx_in(1791 downto 0) <= output_FIFO_dout;
rx_data_good_in(2) <= gtwiz_reset_rx_done(2);
g_GTY_FIFO : for i in 0 to 19 generate
	i_GTY_FIFO : GTY_FIFO
		PORT MAP (
			srst => gtwiz_reset_all_in(2),
			wr_clk => gtwiz_userclk_rx_usrclk2_out(2),
			rd_clk => gtwiz_userclk_tx_usrclk2_out(2),
			din => gtwiz_userdata_rx_out(i*80+1871 downto i*80+1792),
			wr_en => GTY_FIFO_wr_en(i),
			rd_en => GTY_FIFO_rd_en(i),
			dout => gtwiz_userdata_tx_in(i*80+1871 downto i*80+1792),
			full => GTY_FIFO_full(i),
			empty => GTY_FIFO_empty(i),
			wr_rst_busy => open,
			rd_rst_busy => open
		);
	GTY_FIFO_wr_en(i) <= not GTY_FIFO_full(i);	
	GTY_FIFO_rd_en(i) <= not GTY_FIFO_empty(i);	
end generate g_GTY_FIFO;
process(clk)
begin
	if(clk'event and clk = '1')then
		clk_ce <= clk_cep;
	end if;
end process;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		for i in 0 to 15 loop
			if(i < 8)then
				clk_cep(i) <= input_FIFO_dout(i*64+61) or startup;
			end if;
			reset_RAM(i) <= input_FIFO_dout(i*64+63);
			if(load_uRAM = '1')then
				data_valid_in(i) <= sine_init_dout_valid;
				theta_in(i)(22 downto 11) <= sine_init_addr;
			else
				data_valid_in(i) <= input_FIFO_dout(i*64+62);
				theta_in(i)(22 downto 11) <= input_FIFO_dout(i*64+46 downto i*64+35);
			end if;
			r_in(i) <= input_FIFO_dout(i*64+22 downto i*64);
			theta_in(i)(10 downto 0) <= input_FIFO_dout(i*64+34 downto i*64+24);
			R_drift(i) <= input_FIFO_dout(i*64+57 downto i*64+48);
			output_FIFO_din(i*64+48 downto i*64) <= seg_pm(i);
			output_FIFO_din(i*64+49) <= ready(i);
		end loop;
		for i in 16 to 27 loop
			output_FIFO_din(i*64+63 downto i*64) <= input_FIFO_dout(i*64+63 downto i*64);
		end loop;
	end if;
end process;
end Behavioral;
