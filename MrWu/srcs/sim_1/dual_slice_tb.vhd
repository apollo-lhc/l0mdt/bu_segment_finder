----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/29/2018 01:25:36 PM
-- Design Name: 
-- Module Name: cosine_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dual_slice_tb is
--  Port ( );
end dual_slice_tb;

architecture Behavioral of dual_slice_tb is
component dual_slice
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           new_hit : in STD_LOGIC;
           r : in STD_LOGIC_VECTOR (16 downto 0);
           r_h : in STD_LOGIC_VECTOR (16 downto 0);
           r_e0 : out STD_LOGIC_VECTOR (7 downto 0);
           r_e1 : out STD_LOGIC_VECTOR (7 downto 0);
           r_o0 : out STD_LOGIC_VECTOR (7 downto 0);
           r_o1 : out STD_LOGIC_VECTOR (7 downto 0);
					 max : out STD_LOGIC_VECTOR (5 downto 0));
end component;
signal r_l : std_logic_vector(16 downto 0) := (others => '0');
signal r_h : std_logic_vector(16 downto 0) := (others => '0');
signal r_e0 : std_logic_vector(7 downto 0) := (others => '0');
signal r_e1 : std_logic_vector(7 downto 0) := (others => '0');
signal r_o0 : std_logic_vector(7 downto 0) := (others => '0');
signal r_o1 : std_logic_vector(7 downto 0) := (others => '0');
signal max : std_logic_vector(5 downto 0) := (others => '0');
signal reset : std_logic := '1';
signal clk : std_logic := '0';
signal new_hit : std_logic := '0';
signal cntr : std_logic_vector(15 downto 0) := (others => '0');

begin
utt: dual_slice
		port map(
			clk => clk,
			reset => reset,
			new_hit => new_hit,
			r => r_l,
			r_h => r_h,
			r_e0 => r_e0,
			r_e1 => r_e1,
			r_o0 => r_o0,
			r_o1 => r_o1,
			max => max
		);
clk <= not clk after 1 ns;
process(clk)
begin
  if(clk'event and clk = '1')then
		if(reset = '1')then
			cntr <= (others => '0');
		else
			cntr <= cntr + 1;
		end if;
		if(cntr = x"0080")then
			new_hit <= '1';
			r_l(15 downto 0) <= x"1234";
		else
			new_hit <= '0';
			r_l(15 downto 0) <= x"1254";
		end if;
  end if;
end process;
stim_proc: process
begin    
   -- hold reset state for 100 ns.
   wait for 200 ns;  
   reset <= '0';
   -- insert stimulus here 

   wait;
end process;

end Behavioral;
