----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/29/2018 01:25:36 PM
-- Design Name: 
-- Module Name: cosine_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library xpm;
use xpm.vcomponents.all;

entity FIFO_tb is
--  Port ( );
end FIFO_tb;

architecture Behavioral of FIFO_tb is
COMPONENT FIFO72X512
  PORT (
    srst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;
signal fifo_wr_en : std_logic := '0';
signal fifo_rd_en : std_logic := '0';
signal rd_en : std_logic := '0';
signal fifo_empty : std_logic := '0';
signal reset : std_logic := '1';
signal reset_n : std_logic := '1';
signal wbusy : std_logic;
signal rbusy : std_logic;
signal fifo_din : std_logic_vector(71 downto 0) := (others => '0');
signal fifo_dout : std_logic_vector(71 downto 0) := (others => '0');
signal sysclk : std_logic := '0';
signal clk : std_logic := '0';
signal clk_gated : std_logic := '0';
signal clk_cep : std_logic := '0';
signal clk_ce : std_logic := '0';
signal dest_arst : std_logic := '0';

begin
   xpm_cdc_async_rst_inst : xpm_cdc_sync_rst
   generic map (
      DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
      INIT => 0,           -- DECIMAL; 0=initialize synchronization registers to 0, 1=initialize
                           -- synchronization registers to 1
      INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
   )
   port map (
      dest_rst => dest_arst, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination
                              -- clock domain. This output is registered. NOTE: Signal asserts asynchronously
                              -- but deasserts synchronously to dest_clk. Width of the reset signal is at least
                              -- (DEST_SYNC_FF*dest_clk) period.

      dest_clk => clk_gated,   -- 1-bit input: Destination clock.
      src_rst => reset    -- 1-bit input: Source asynchronous reset signal.
   );
reset_n <= not reset;
i_fifo_in : FIFO72X512
  PORT MAP (
    srst => reset,
    wr_clk => sysclk,
    rd_clk => clk_gated,
    din => fifo_din,
    wr_en => fifo_wr_en,
    rd_en => fifo_rd_en,
    dout => fifo_dout,
    full => open,
    empty => fifo_empty,
    wr_rst_busy => wbusy,
    rd_rst_busy => rbusy
  );
clk <= not clk after 1 ns;
sysclk <= not sysclk after 2.5ns;
i_BUFGCE : BUFGCE
			port map (
				 O => clk_gated,   -- 1-bit output: Buffer
				 CE => clk_ce, -- 1-bit input: Buffer enable
				 I => clk    -- 1-bit input: Buffer
			);
process(sysclk)
begin
  if(sysclk'event and sysclk = '1')then
  end if;
end process;
process(clk)
begin
  if(clk'event and clk = '1')then
		clk_ce <= clk_cep;
  end if;
end process;
process(clk_gated)
begin
  if(clk_gated'event and clk_gated = '1')then
		fifo_rd_en <= not fifo_empty and rd_en and not fifo_rd_en;
  end if;
end process;
stim_proc: process
begin    
   -- hold reset state for 100 ns.
   wait for 200 ns;  
	 clk_cep <= '1';	 
   wait for 10 ns;
   reset <= '1';
	 wait for 14 ns;
	 reset <= '0';
	 wait for 40 ns;
	 fifo_wr_en <= '1';
	 fifo_din(15 downto 0) <= x"abcd";
	 wait for 5 ns;
	 fifo_wr_en <= '0';
	 clk_cep <= '0';
	 wait for 40 ns;
	 fifo_wr_en <= '1';
	 fifo_din(15 downto 0) <= x"1234";
	 wait for 5 ns;
	 fifo_wr_en <= '0';
	 wait for 20 ns;
	 clk_cep <= '1';
	 wait for 51 ns;
	 rd_en <= '1';
   -- insert stimulus here 

   wait;
end process;

end Behavioral;
