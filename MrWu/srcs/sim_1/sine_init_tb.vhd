----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/29/2018 01:25:36 PM
-- Design Name: 
-- Module Name: cosine_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sine_init_tb is
--  Port ( );
end sine_init_tb;

architecture Behavioral of sine_init_tb is
component sine_rom_init
    Port ( clk : in STD_LOGIC;
           MMCM_locked : in STD_LOGIC;
           fifo_busy : in STD_LOGIC;
					 dout : out STD_LOGIC_VECTOR (12 downto 0);
					 addr : out STD_LOGIC_VECTOR (11 downto 0);
           dout_valid : out STD_LOGIC;
           load_uRAM : out STD_LOGIC;
           we_uRAM : out STD_LOGIC;
           init_done : out STD_LOGIC
           );
end component;
signal load_uRAM : std_logic := '0';
signal we_uRAM : std_logic := '0';
signal MMCM_locked : std_logic := '0';
signal reset : std_logic := '0';
signal init_done : std_logic := '0';
signal sine_init_dout_valid : std_logic := '0';
signal dout : std_logic_vector(12 downto 0) := (others => '0');
signal sine_init_addr : std_logic_vector(11 downto 0) := (others => '0');
signal clk : std_logic := '0';

begin
utt: sine_rom_init
  port map(
    clk => clk,
		MMCM_locked => MMCM_locked,
		fifo_busy => reset,
		dout => dout,
		dout_valid => sine_init_dout_valid,
		addr => sine_init_addr,
		load_uRAM => load_uRAM,
		we_uRAM => we_uRAM,
		init_done => init_done
    );
clk <= not clk after 2 ns;
process(clk)
begin
  if(clk'event and clk = '1')then
  end if;
end process;
stim_proc: process
begin    
   -- hold reset state for 100 ns.
   wait for 200 ns; 
MMCM_locked <= '1';
wait for 10 ns;	 
   reset <= '1';
wait for 100 ns;	 
   reset <= '0';
   -- insert stimulus here 

   wait;
end process;

end Behavioral;
