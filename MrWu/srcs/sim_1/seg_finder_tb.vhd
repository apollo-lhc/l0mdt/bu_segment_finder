----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/29/2018 01:25:36 PM
-- Design Name: 
-- Module Name: cosine_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.l0mdt_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library xpm;
use xpm.vcomponents.all;

entity seg_finder_tb is
--  Port ( );
end seg_finder_tb;

architecture Behavioral of seg_finder_tb is
component seg_finder
    Port ( clk : in STD_LOGIC;
			sysclk : in STD_LOGIC;
			MMCM_locked : in STD_LOGIC;
			reset_RAM_in : in STD_LOGIC;
			fifo_reset : in STD_LOGIC;
			fifo_busy : out STD_LOGIC;
			load_uRAM_in : in STD_LOGIC;
			we_uRAM_in : in STD_LOGIC;
--			sine_data_valid : in STD_LOGIC;
			sine_init_data: in STD_LOGIC_VECTOR (12 downto 0);
			theta_in: in STD_LOGIC_VECTOR (22 downto 0);-- this is in fact theta - phi0, it must be >= -0.96 radian  and < 0.96 radian. bit 22 is sign bit, bit 21 = 0.512 radian
			data_valid_in : in STD_LOGIC;-- valid hit present
			r_in : in STD_LOGIC_VECTOR (22 downto 0);-- positive number r0(5) = 0.25mm
			R_drift_in : in STD_LOGIC_VECTOR (9 downto 0);-- R_drift(4) = 0.25mm
			seg_pm : out STD_LOGIC_VECTOR (48 downto 0);-- 22 bit theta & 23 bit r & 4 bit count
			ready : out STD_LOGIC
     );
end component;
component sine_rom_init
    Port ( clk : in STD_LOGIC;
           MMCM_locked : in STD_LOGIC;
           fifo_busy : in STD_LOGIC;
					 dout : out STD_LOGIC_VECTOR (12 downto 0);
					 addr : out STD_LOGIC_VECTOR (11 downto 0);
           dout_valid : out STD_LOGIC;
           load_uRAM : out STD_LOGIC;
           we_uRAM : out STD_LOGIC
           );
end component;
signal theta_in : std_logic_vector(22 downto 0) := (others => '0');
signal r_in : std_logic_vector(22 downto 0) := (others => '0');
signal r_offset : std_logic_vector(22 downto 0) := (others => '0');
signal seg_pm : std_logic_vector(48 downto 0) := (others => '0');
signal R_drift : std_logic_vector(9 downto 0) := (others => '0');
signal CLKFB_MMCM : std_logic := '0';
signal reset_MMCM : std_logic := '0';
signal clk_MMCM : std_logic := '0';
signal MMCM_not_locked : std_logic;
signal sysclk_MMCM : std_logic := '0';
signal freerun_clk_MMCM : std_logic := '0';
signal MMCM_locked : std_logic := '0';
signal MMCM_locked_sync : std_logic := '0';
signal MMCM_locked_n : std_logic := '0';
signal clk : std_logic := '0';
signal clk_gated : std_logic := '0';
signal clk_cep: std_logic := '0';
signal clk_ce : std_logic := '0';
signal sysclk_in : std_logic := '0';
signal sysclk : std_logic := '0';
signal data_valid_in : std_logic := '0';
signal reset : std_logic := '0';
signal we_uRAM : std_logic := '0';
signal load_uRAM : std_logic := '0';
signal load_uRAM_q : std_logic := '0';
signal load_uRAM_done : std_logic := '0';
signal load_uRAM_done_dl : std_logic := '0';
signal fifo_reset : std_logic := '0';
signal fifo_busy : std_logic := '0';
signal addr : std_logic_vector(11 downto 0) := (others => '0');
signal dout : std_logic_vector(12 downto 0) := (others => '0');
signal dout_valid : std_logic := '0';
signal ready : std_logic := '0';
signal en_cntr : std_logic := '0';
signal cntr : std_logic_vector(8 downto 0) := (others => '0');

begin
sysclk_in <= not sysclk_in after 5 ns;
i_sysMMCM : MMCME4_BASE
   generic map (
      CLKFBOUT_MULT_F => 9.0,     -- Multiply value for all CLKOUT
      CLKIN1_PERIOD => 10.0,       -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      CLKOUT0_DIVIDE_F => 18.0,    -- Divide amount for CLKOUT0
      CLKOUT1_DIVIDE => 2,        -- Divide amount for CLKOUT (1-128)
      CLKOUT2_DIVIDE => 4        -- Divide amount for CLKOUT (1-128)
   )
   port map (
      CLKFBOUT => CLKFB_MMCM,   -- 1-bit output: Feedback clock pin to the MMCM
      CLKOUT1 => clk_MMCM,     -- 1-bit output: CLKOUT1
      CLKOUT2 => sysclk_MMCM,     -- 1-bit output: CLKOUT1
      LOCKED => MMCM_locked,       -- 1-bit output: LOCK
      CLKFBIN => CLKFB_MMCM,     -- 1-bit input: Feedback clock pin to the MMCM
      CLKIN1 => sysclk_in,       -- 1-bit input: Primary clock
      PWRDWN => '0',       -- 1-bit input: Power-down
      RST => reset_MMCM              -- 1-bit input: Reset
   );
i_buf_clk : BUFG port map(i => clk_MMCM, o => clk);
i_buf_sysclk : BUFG port map(i => sysclk_MMCM, o => sysclk);
i_BUFGCE : BUFGCE
			port map (
				 O => clk_gated,   -- 1-bit output: Buffer
				 CE => clk_ce, -- 1-bit input: Buffer enable
				 I => clk    -- 1-bit input: Buffer
			);
process(clk)
begin
  if(clk'event and clk = '1')then
    clk_ce <= MMCM_locked_sync;
  end if;
end process;
i_MMCM_not_locked : xpm_cdc_async_rst
   generic map (
      DEST_SYNC_FF => 4,    -- DECIMAL; range: 2-10
      INIT_SYNC_FF => 0,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      RST_ACTIVE_HIGH => 0  -- DECIMAL; 0=active low reset, 1=active high reset
   )
   port map (
      dest_arst => MMCM_locked_sync, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination
                              -- clock domain. This output is registered. NOTE: Signal asserts asynchronously
                              -- but deasserts synchronously to dest_clk. Width of the reset signal is at least
                              -- (DEST_SYNC_FF*dest_clk) period.

      dest_clk => sysclk,   -- 1-bit input: Destination clock.
      src_arst => MMCM_locked    -- 1-bit input: Source asynchronous reset signal.
   );
i_sine_rom_init : sine_rom_init
	port map(
		clk => sysclk,
		MMCM_locked => MMCM_locked,
		fifo_busy => fifo_busy,
		dout => dout,
		dout_valid => dout_valid,
		addr => addr,
		load_uRAM => load_uRAM,
		we_uRAM => we_uRAM
		);
utt: seg_finder
  port map(
    clk => clk_gated,
    sysclk => sysclk,
		MMCM_locked => MMCM_locked,
		reset_RAM_in => reset,
    fifo_reset => fifo_reset,
    fifo_busy => fifo_busy,
    load_uRAM_in => load_uRAM,
    we_uRAM_in => we_uRAM,
--    sine_data_valid => dout_valid,
    sine_init_data => dout,
    theta_in => theta_in,
    data_valid_in => data_valid_in,
    r_in => r_in,
    R_drift_in => R_drift,
    seg_pm => seg_pm,
    ready => ready
    );
process(sysclk)
    begin
      if(sysclk'event and sysclk = '1')then
        MMCM_locked_n <= not MMCM_locked_sync;
        fifo_reset <= MMCM_locked_n and MMCM_locked_sync;
      end if;
    end process;
process(sysclk)
begin
  if(sysclk'event and sysclk = '1')then
		if(load_uRAM = '1')then
			theta_in(22 downto 11) <= addr;
    elsif(reset = '1' or theta_in = x"76ffff")then
      theta_in <= "000" & x"7a080";
      r_in <= "001" & x"00000";
      R_drift <= "10" & x"00";
    elsif(data_valid_in = '1')then
      theta_in <= theta_in + x"1800";
      r_in <= r_in + x"01004";
      R_drift <= R_drift + x"04";
    end if;
		if(load_uRAM = '1')then
			data_valid_in <= dout_valid;
		elsif(cntr(8) = '1' or cntr(7 downto 0) = x"10" or cntr(7 downto 0) = x"c3")then
			data_valid_in <= '1';
		else
			data_valid_in <= '0';
		end if;
		if(ready = '1' and cntr(8) = '0')then
			cntr <= cntr + 1;
		end if;
		load_uRAM_q <= load_uRAM;
		load_uRAM_done <= load_uRAM_q and not load_uRAM;
  end if;
end process;
process(sysclk, MMCM_locked)
begin
	if(MMCM_locked = '0')then
		reset <= '0';
  elsif(sysclk'event and sysclk = '1')then
		reset <= load_uRAM_done_dl;
  end if;
end process;
i_load_uRAM_done_dl : LUT_delay generic map(N => 16)
port map (
   Q => load_uRAM_done_dl,     -- 1-bit output: SRL Data
   CE => '1',   -- 1-bit input: Clock enable
   CLK => sysclk, -- 1-bit input: Clock
   D => load_uRAM_done     -- 1-bit input: SRL Data
);
stim_proc: process
begin    
   -- hold reset state for 100 ns.
   wait for 150 ns;  
--   MMCM_locked <= '1';
--   wait for 40 ns;  
--   reset <= '1';
--   wait for 4 ns;  
--   reset <= '0';
--   wait for 200 ns;  
--   data_valid_in <= '1';
--   wait for 4 ns;
--   data_valid_in <= '0';
--   wait for 60 ns;  
--   data_valid_in <= '1';
--   wait for 4 ns;
--   data_valid_in <= '0';
--   wait for 60 ns;  
--   data_valid_in <= '1';
--   -- insert stimulus here 

   wait;
end process;

end Behavioral;
