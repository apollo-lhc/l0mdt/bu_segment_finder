// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Tue Jul 31 15:25:28 2018
// Host        : baby running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub d:/vproject/l0mdt2/l0mdt2.srcs/sources_1/ip/GTY_slow/GTY_slow_stub.v
// Design      : GTY_slow
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku15p-ffva1760-1-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "GTY_slow_gtwizard_top,Vivado 2018.2" *)
module GTY_slow(gtwiz_userclk_tx_active_in, 
  gtwiz_userclk_rx_active_in, gtwiz_reset_clk_freerun_in, gtwiz_reset_all_in, 
  gtwiz_reset_tx_pll_and_datapath_in, gtwiz_reset_tx_datapath_in, 
  gtwiz_reset_rx_pll_and_datapath_in, gtwiz_reset_rx_datapath_in, 
  gtwiz_reset_rx_cdr_stable_out, gtwiz_reset_tx_done_out, gtwiz_reset_rx_done_out, 
  gtwiz_userdata_tx_in, gtwiz_userdata_rx_out, gtrefclk00_in, qpll0outclk_out, 
  qpll0outrefclk_out, gtyrxn_in, gtyrxp_in, rx8b10ben_in, rxcommadeten_in, 
  rxmcommaalignen_in, rxpcommaalignen_in, rxusrclk_in, rxusrclk2_in, tx8b10ben_in, 
  txctrl0_in, txctrl1_in, txctrl2_in, txusrclk_in, txusrclk2_in, gtpowergood_out, gtytxn_out, 
  gtytxp_out, rxbyteisaligned_out, rxbyterealign_out, rxcommadet_out, rxctrl0_out, 
  rxctrl1_out, rxctrl2_out, rxctrl3_out, rxoutclk_out, rxpmaresetdone_out, txoutclk_out, 
  txpmaresetdone_out)
/* synthesis syn_black_box black_box_pad_pin="gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[383:0],gtwiz_userdata_rx_out[383:0],gtrefclk00_in[2:0],qpll0outclk_out[2:0],qpll0outrefclk_out[2:0],gtyrxn_in[11:0],gtyrxp_in[11:0],rx8b10ben_in[11:0],rxcommadeten_in[11:0],rxmcommaalignen_in[11:0],rxpcommaalignen_in[11:0],rxusrclk_in[11:0],rxusrclk2_in[11:0],tx8b10ben_in[11:0],txctrl0_in[191:0],txctrl1_in[191:0],txctrl2_in[95:0],txusrclk_in[11:0],txusrclk2_in[11:0],gtpowergood_out[11:0],gtytxn_out[11:0],gtytxp_out[11:0],rxbyteisaligned_out[11:0],rxbyterealign_out[11:0],rxcommadet_out[11:0],rxctrl0_out[191:0],rxctrl1_out[191:0],rxctrl2_out[95:0],rxctrl3_out[95:0],rxoutclk_out[11:0],rxpmaresetdone_out[11:0],txoutclk_out[11:0],txpmaresetdone_out[11:0]" */;
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [383:0]gtwiz_userdata_tx_in;
  output [383:0]gtwiz_userdata_rx_out;
  input [2:0]gtrefclk00_in;
  output [2:0]qpll0outclk_out;
  output [2:0]qpll0outrefclk_out;
  input [11:0]gtyrxn_in;
  input [11:0]gtyrxp_in;
  input [11:0]rx8b10ben_in;
  input [11:0]rxcommadeten_in;
  input [11:0]rxmcommaalignen_in;
  input [11:0]rxpcommaalignen_in;
  input [11:0]rxusrclk_in;
  input [11:0]rxusrclk2_in;
  input [11:0]tx8b10ben_in;
  input [191:0]txctrl0_in;
  input [191:0]txctrl1_in;
  input [95:0]txctrl2_in;
  input [11:0]txusrclk_in;
  input [11:0]txusrclk2_in;
  output [11:0]gtpowergood_out;
  output [11:0]gtytxn_out;
  output [11:0]gtytxp_out;
  output [11:0]rxbyteisaligned_out;
  output [11:0]rxbyterealign_out;
  output [11:0]rxcommadet_out;
  output [191:0]rxctrl0_out;
  output [191:0]rxctrl1_out;
  output [95:0]rxctrl2_out;
  output [95:0]rxctrl3_out;
  output [11:0]rxoutclk_out;
  output [11:0]rxpmaresetdone_out;
  output [11:0]txoutclk_out;
  output [11:0]txpmaresetdone_out;
endmodule
