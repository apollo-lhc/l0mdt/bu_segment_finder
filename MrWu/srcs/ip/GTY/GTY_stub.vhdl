-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
-- Date        : Tue Aug  7 11:43:43 2018
-- Host        : baby running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub D:/vproject/l0mdt_GT/l0mdt_GT.srcs/sources_1/ip/GTY/GTY_stub.vhdl
-- Design      : GTY
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku15p-ffva1760-1-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GTY is
  Port ( 
    gtwiz_userclk_tx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_userclk_rx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_clk_freerun_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_all_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_cdr_stable_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_userdata_tx_in : in STD_LOGIC_VECTOR ( 1599 downto 0 );
    gtwiz_userdata_rx_out : out STD_LOGIC_VECTOR ( 1599 downto 0 );
    gtrefclk00_in : in STD_LOGIC_VECTOR ( 4 downto 0 );
    qpll0outclk_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
    qpll0outrefclk_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
    gtyrxn_in : in STD_LOGIC_VECTOR ( 19 downto 0 );
    gtyrxp_in : in STD_LOGIC_VECTOR ( 19 downto 0 );
    rxusrclk_in : in STD_LOGIC_VECTOR ( 19 downto 0 );
    rxusrclk2_in : in STD_LOGIC_VECTOR ( 19 downto 0 );
    txusrclk_in : in STD_LOGIC_VECTOR ( 19 downto 0 );
    txusrclk2_in : in STD_LOGIC_VECTOR ( 19 downto 0 );
    gtpowergood_out : out STD_LOGIC_VECTOR ( 19 downto 0 );
    gtytxn_out : out STD_LOGIC_VECTOR ( 19 downto 0 );
    gtytxp_out : out STD_LOGIC_VECTOR ( 19 downto 0 );
    rxoutclk_out : out STD_LOGIC_VECTOR ( 19 downto 0 );
    rxpmaresetdone_out : out STD_LOGIC_VECTOR ( 19 downto 0 );
    txoutclk_out : out STD_LOGIC_VECTOR ( 19 downto 0 );
    txpmaresetdone_out : out STD_LOGIC_VECTOR ( 19 downto 0 );
    txprgdivresetdone_out : out STD_LOGIC_VECTOR ( 19 downto 0 )
  );

end GTY;

architecture stub of GTY is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[1599:0],gtwiz_userdata_rx_out[1599:0],gtrefclk00_in[4:0],qpll0outclk_out[4:0],qpll0outrefclk_out[4:0],gtyrxn_in[19:0],gtyrxp_in[19:0],rxusrclk_in[19:0],rxusrclk2_in[19:0],txusrclk_in[19:0],txusrclk2_in[19:0],gtpowergood_out[19:0],gtytxn_out[19:0],gtytxp_out[19:0],rxoutclk_out[19:0],rxpmaresetdone_out[19:0],txoutclk_out[19:0],txpmaresetdone_out[19:0],txprgdivresetdone_out[19:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "GTY_gtwizard_top,Vivado 2018.2";
begin
end;
