// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Tue Aug  7 11:43:43 2018
// Host        : baby running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub D:/vproject/l0mdt_GT/l0mdt_GT.srcs/sources_1/ip/GTY/GTY_stub.v
// Design      : GTY
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku15p-ffva1760-1-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "GTY_gtwizard_top,Vivado 2018.2" *)
module GTY(gtwiz_userclk_tx_active_in, 
  gtwiz_userclk_rx_active_in, gtwiz_reset_clk_freerun_in, gtwiz_reset_all_in, 
  gtwiz_reset_tx_pll_and_datapath_in, gtwiz_reset_tx_datapath_in, 
  gtwiz_reset_rx_pll_and_datapath_in, gtwiz_reset_rx_datapath_in, 
  gtwiz_reset_rx_cdr_stable_out, gtwiz_reset_tx_done_out, gtwiz_reset_rx_done_out, 
  gtwiz_userdata_tx_in, gtwiz_userdata_rx_out, gtrefclk00_in, qpll0outclk_out, 
  qpll0outrefclk_out, gtyrxn_in, gtyrxp_in, rxusrclk_in, rxusrclk2_in, txusrclk_in, 
  txusrclk2_in, gtpowergood_out, gtytxn_out, gtytxp_out, rxoutclk_out, rxpmaresetdone_out, 
  txoutclk_out, txpmaresetdone_out, txprgdivresetdone_out)
/* synthesis syn_black_box black_box_pad_pin="gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[1599:0],gtwiz_userdata_rx_out[1599:0],gtrefclk00_in[4:0],qpll0outclk_out[4:0],qpll0outrefclk_out[4:0],gtyrxn_in[19:0],gtyrxp_in[19:0],rxusrclk_in[19:0],rxusrclk2_in[19:0],txusrclk_in[19:0],txusrclk2_in[19:0],gtpowergood_out[19:0],gtytxn_out[19:0],gtytxp_out[19:0],rxoutclk_out[19:0],rxpmaresetdone_out[19:0],txoutclk_out[19:0],txpmaresetdone_out[19:0],txprgdivresetdone_out[19:0]" */;
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [1599:0]gtwiz_userdata_tx_in;
  output [1599:0]gtwiz_userdata_rx_out;
  input [4:0]gtrefclk00_in;
  output [4:0]qpll0outclk_out;
  output [4:0]qpll0outrefclk_out;
  input [19:0]gtyrxn_in;
  input [19:0]gtyrxp_in;
  input [19:0]rxusrclk_in;
  input [19:0]rxusrclk2_in;
  input [19:0]txusrclk_in;
  input [19:0]txusrclk2_in;
  output [19:0]gtpowergood_out;
  output [19:0]gtytxn_out;
  output [19:0]gtytxp_out;
  output [19:0]rxoutclk_out;
  output [19:0]rxpmaresetdone_out;
  output [19:0]txoutclk_out;
  output [19:0]txpmaresetdone_out;
  output [19:0]txprgdivresetdone_out;
endmodule
