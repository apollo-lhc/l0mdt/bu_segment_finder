// (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:ip:gtwizard_ultrascale:1.7
// IP Revision: 4

`timescale 1ns/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module GTY (
  gtwiz_userclk_tx_active_in,
  gtwiz_userclk_rx_active_in,
  gtwiz_reset_clk_freerun_in,
  gtwiz_reset_all_in,
  gtwiz_reset_tx_pll_and_datapath_in,
  gtwiz_reset_tx_datapath_in,
  gtwiz_reset_rx_pll_and_datapath_in,
  gtwiz_reset_rx_datapath_in,
  gtwiz_reset_rx_cdr_stable_out,
  gtwiz_reset_tx_done_out,
  gtwiz_reset_rx_done_out,
  gtwiz_userdata_tx_in,
  gtwiz_userdata_rx_out,
  gtrefclk00_in,
  qpll0outclk_out,
  qpll0outrefclk_out,
  gtyrxn_in,
  gtyrxp_in,
  rxusrclk_in,
  rxusrclk2_in,
  txusrclk_in,
  txusrclk2_in,
  gtpowergood_out,
  gtytxn_out,
  gtytxp_out,
  rxoutclk_out,
  rxpmaresetdone_out,
  txoutclk_out,
  txpmaresetdone_out,
  txprgdivresetdone_out
);

input wire [0 : 0] gtwiz_userclk_tx_active_in;
input wire [0 : 0] gtwiz_userclk_rx_active_in;
input wire [0 : 0] gtwiz_reset_clk_freerun_in;
input wire [0 : 0] gtwiz_reset_all_in;
input wire [0 : 0] gtwiz_reset_tx_pll_and_datapath_in;
input wire [0 : 0] gtwiz_reset_tx_datapath_in;
input wire [0 : 0] gtwiz_reset_rx_pll_and_datapath_in;
input wire [0 : 0] gtwiz_reset_rx_datapath_in;
output wire [0 : 0] gtwiz_reset_rx_cdr_stable_out;
output wire [0 : 0] gtwiz_reset_tx_done_out;
output wire [0 : 0] gtwiz_reset_rx_done_out;
input wire [1599 : 0] gtwiz_userdata_tx_in;
output wire [1599 : 0] gtwiz_userdata_rx_out;
input wire [4 : 0] gtrefclk00_in;
output wire [4 : 0] qpll0outclk_out;
output wire [4 : 0] qpll0outrefclk_out;
input wire [19 : 0] gtyrxn_in;
input wire [19 : 0] gtyrxp_in;
input wire [19 : 0] rxusrclk_in;
input wire [19 : 0] rxusrclk2_in;
input wire [19 : 0] txusrclk_in;
input wire [19 : 0] txusrclk2_in;
output wire [19 : 0] gtpowergood_out;
output wire [19 : 0] gtytxn_out;
output wire [19 : 0] gtytxp_out;
output wire [19 : 0] rxoutclk_out;
output wire [19 : 0] rxpmaresetdone_out;
output wire [19 : 0] txoutclk_out;
output wire [19 : 0] txpmaresetdone_out;
output wire [19 : 0] txprgdivresetdone_out;

  GTY_gtwizard_top #(
    .C_CHANNEL_ENABLE(192'B000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011111111111111111111),
    .C_PCIE_ENABLE(0),
    .C_PCIE_CORECLK_FREQ(250),
    .C_COMMON_SCALING_FACTOR(5),
    .C_CPLL_VCO_FREQUENCY(2578.125),
    .C_FORCE_COMMONS(0),
    .C_FREERUN_FREQUENCY(50),
    .C_GT_TYPE(3),
    .C_GT_REV(67),
    .C_INCLUDE_CPLL_CAL(2),
    .C_ENABLE_COMMON_USRCLK(0),
    .C_USER_GTPOWERGOOD_DELAY_EN(1),
    .C_SIM_CPLL_CAL_BYPASS(1),
    .C_LOCATE_COMMON(0),
    .C_LOCATE_RESET_CONTROLLER(0),
    .C_LOCATE_USER_DATA_WIDTH_SIZING(0),
    .C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER(0),
    .C_LOCATE_IN_SYSTEM_IBERT_CORE(2),
    .C_LOCATE_RX_USER_CLOCKING(1),
    .C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER(0),
    .C_LOCATE_TX_USER_CLOCKING(1),
    .C_RESET_CONTROLLER_INSTANCE_CTRL(0),
    .C_RX_BUFFBYPASS_MODE(0),
    .C_RX_BUFFER_BYPASS_INSTANCE_CTRL(0),
    .C_RX_BUFFER_MODE(1),
    .C_RX_CB_DISP(8'B00000000),
    .C_RX_CB_K(8'B00000000),
    .C_RX_CB_MAX_LEVEL(6),
    .C_RX_CB_LEN_SEQ(1),
    .C_RX_CB_NUM_SEQ(0),
    .C_RX_CB_VAL(80'B00000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .C_RX_CC_DISP(8'B00000000),
    .C_RX_CC_ENABLE(0),
    .C_RESET_SEQUENCE_INTERVAL(0),
    .C_RX_CC_K(8'B00000000),
    .C_RX_CC_LEN_SEQ(1),
    .C_RX_CC_NUM_SEQ(0),
    .C_RX_CC_PERIODICITY(5000),
    .C_RX_CC_VAL(80'B00000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .C_RX_COMMA_M_ENABLE(0),
    .C_RX_COMMA_M_VAL(10'B1010000011),
    .C_RX_COMMA_P_ENABLE(0),
    .C_RX_COMMA_P_VAL(10'B0101111100),
    .C_RX_DATA_DECODING(0),
    .C_RX_ENABLE(1),
    .C_RX_INT_DATA_WIDTH(80),
    .C_RX_LINE_RATE(25.78125),
    .C_RX_MASTER_CHANNEL_IDX(0),
    .C_RX_OUTCLK_BUFG_GT_DIV(1),
    .C_RX_OUTCLK_FREQUENCY(322.2656250),
    .C_RX_OUTCLK_SOURCE(1),
    .C_RX_PLL_TYPE(0),
    .C_RX_RECCLK_OUTPUT(192'H000000000000000000000000000000000000000000000000),
    .C_RX_REFCLK_FREQUENCY(161.1328125),
    .C_RX_SLIDE_MODE(0),
    .C_RX_USER_CLOCKING_CONTENTS(0),
    .C_RX_USER_CLOCKING_INSTANCE_CTRL(0),
    .C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK(1),
    .C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2(1),
    .C_RX_USER_CLOCKING_SOURCE(0),
    .C_RX_USER_DATA_WIDTH(80),
    .C_RX_USRCLK_FREQUENCY(322.2656250),
    .C_RX_USRCLK2_FREQUENCY(322.2656250),
    .C_SECONDARY_QPLL_ENABLE(0),
    .C_SECONDARY_QPLL_REFCLK_FREQUENCY(257.8125),
    .C_TOTAL_NUM_CHANNELS(20),
    .C_TOTAL_NUM_COMMONS(5),
    .C_TOTAL_NUM_COMMONS_EXAMPLE(0),
    .C_TXPROGDIV_FREQ_ENABLE(0),
    .C_TXPROGDIV_FREQ_SOURCE(0),
    .C_TXPROGDIV_FREQ_VAL(322.265625),
    .C_TX_BUFFBYPASS_MODE(0),
    .C_TX_BUFFER_BYPASS_INSTANCE_CTRL(0),
    .C_TX_BUFFER_MODE(1),
    .C_TX_DATA_ENCODING(0),
    .C_TX_ENABLE(1),
    .C_TX_INT_DATA_WIDTH(80),
    .C_TX_LINE_RATE(25.78125),
    .C_TX_MASTER_CHANNEL_IDX(0),
    .C_TX_OUTCLK_BUFG_GT_DIV(1),
    .C_TX_OUTCLK_FREQUENCY(322.2656250),
    .C_TX_OUTCLK_SOURCE(4),
    .C_TX_PLL_TYPE(0),
    .C_TX_REFCLK_FREQUENCY(161.1328125),
    .C_TX_USER_CLOCKING_CONTENTS(0),
    .C_TX_USER_CLOCKING_INSTANCE_CTRL(0),
    .C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK(1),
    .C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2(1),
    .C_TX_USER_CLOCKING_SOURCE(0),
    .C_TX_USER_DATA_WIDTH(80),
    .C_TX_USRCLK_FREQUENCY(322.2656250),
    .C_TX_USRCLK2_FREQUENCY(322.2656250)
  ) inst (
    .gtwiz_userclk_tx_reset_in(1'B0),
    .gtwiz_userclk_tx_active_in(gtwiz_userclk_tx_active_in),
    .gtwiz_userclk_tx_srcclk_out(),
    .gtwiz_userclk_tx_usrclk_out(),
    .gtwiz_userclk_tx_usrclk2_out(),
    .gtwiz_userclk_tx_active_out(),
    .gtwiz_userclk_rx_reset_in(1'B0),
    .gtwiz_userclk_rx_active_in(gtwiz_userclk_rx_active_in),
    .gtwiz_userclk_rx_srcclk_out(),
    .gtwiz_userclk_rx_usrclk_out(),
    .gtwiz_userclk_rx_usrclk2_out(),
    .gtwiz_userclk_rx_active_out(),
    .gtwiz_buffbypass_tx_reset_in(1'B0),
    .gtwiz_buffbypass_tx_start_user_in(1'B0),
    .gtwiz_buffbypass_tx_done_out(),
    .gtwiz_buffbypass_tx_error_out(),
    .gtwiz_buffbypass_rx_reset_in(1'B0),
    .gtwiz_buffbypass_rx_start_user_in(1'B0),
    .gtwiz_buffbypass_rx_done_out(),
    .gtwiz_buffbypass_rx_error_out(),
    .gtwiz_reset_clk_freerun_in(gtwiz_reset_clk_freerun_in),
    .gtwiz_reset_all_in(gtwiz_reset_all_in),
    .gtwiz_reset_tx_pll_and_datapath_in(gtwiz_reset_tx_pll_and_datapath_in),
    .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
    .gtwiz_reset_rx_pll_and_datapath_in(gtwiz_reset_rx_pll_and_datapath_in),
    .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
    .gtwiz_reset_tx_done_in(1'B0),
    .gtwiz_reset_rx_done_in(1'B0),
    .gtwiz_reset_qpll0lock_in(5'B0),
    .gtwiz_reset_qpll1lock_in(5'B0),
    .gtwiz_reset_rx_cdr_stable_out(gtwiz_reset_rx_cdr_stable_out),
    .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
    .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
    .gtwiz_reset_qpll0reset_out(),
    .gtwiz_reset_qpll1reset_out(),
    .gtwiz_gthe3_cpll_cal_txoutclk_period_in(360'B0),
    .gtwiz_gthe3_cpll_cal_cnt_tol_in(360'B0),
    .gtwiz_gthe3_cpll_cal_bufg_ce_in(20'B0),
    .gtwiz_gthe4_cpll_cal_txoutclk_period_in(360'B0),
    .gtwiz_gthe4_cpll_cal_cnt_tol_in(360'B0),
    .gtwiz_gthe4_cpll_cal_bufg_ce_in(20'B0),
    .gtwiz_gtye4_cpll_cal_txoutclk_period_in(360'B0),
    .gtwiz_gtye4_cpll_cal_cnt_tol_in(360'B0),
    .gtwiz_gtye4_cpll_cal_bufg_ce_in(20'B0),
    .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
    .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
    .bgbypassb_in(5'H1F),
    .bgmonitorenb_in(5'H1F),
    .bgpdb_in(5'H1F),
    .bgrcalovrd_in(25'H1084210),
    .bgrcalovrdenb_in(5'H1F),
    .drpaddr_common_in(80'H00000000000000000000),
    .drpclk_common_in(5'H00),
    .drpdi_common_in(80'H00000000000000000000),
    .drpen_common_in(5'H00),
    .drpwe_common_in(5'H00),
    .gtgrefclk0_in(5'H00),
    .gtgrefclk1_in(5'H00),
    .gtnorthrefclk00_in(5'H00),
    .gtnorthrefclk01_in(5'H00),
    .gtnorthrefclk10_in(5'H00),
    .gtnorthrefclk11_in(5'H00),
    .gtrefclk00_in(gtrefclk00_in),
    .gtrefclk01_in(5'H00),
    .gtrefclk10_in(5'H00),
    .gtrefclk11_in(5'H00),
    .gtsouthrefclk00_in(5'H00),
    .gtsouthrefclk01_in(5'H00),
    .gtsouthrefclk10_in(5'H00),
    .gtsouthrefclk11_in(5'H00),
    .pcierateqpll0_in(15'H0000),
    .pcierateqpll1_in(15'H0000),
    .pmarsvd0_in(40'H0000000000),
    .pmarsvd1_in(40'H0000000000),
    .qpll0clkrsvd0_in(5'H00),
    .qpll0clkrsvd1_in(5'H00),
    .qpll0fbdiv_in(40'H0000000000),
    .qpll0lockdetclk_in(5'H00),
    .qpll0locken_in(5'H1F),
    .qpll0pd_in(5'H00),
    .qpll0refclksel_in(15'H1249),
    .qpll0reset_in(5'H00),
    .qpll1clkrsvd0_in(5'H00),
    .qpll1clkrsvd1_in(5'H00),
    .qpll1fbdiv_in(40'H0000000000),
    .qpll1lockdetclk_in(5'H00),
    .qpll1locken_in(5'H00),
    .qpll1pd_in(5'H1F),
    .qpll1refclksel_in(15'H1249),
    .qpll1reset_in(5'H1F),
    .qpllrsvd1_in(40'H0000000000),
    .qpllrsvd2_in(25'H0000000),
    .qpllrsvd3_in(25'H0000000),
    .qpllrsvd4_in(40'H0000000000),
    .rcalenb_in(5'H1F),
    .sdm0data_in(125'H00000000000000000000000000000000),
    .sdm0reset_in(5'H00),
    .sdm0toggle_in(5'H00),
    .sdm0width_in(10'H000),
    .sdm1data_in(125'H00000000000000000000000000000000),
    .sdm1reset_in(5'H00),
    .sdm1toggle_in(5'H00),
    .sdm1width_in(10'H000),
    .tcongpi_in(1'B0),
    .tconpowerup_in(1'B0),
    .tconreset_in(1'B0),
    .tconrsvdin1_in(1'B0),
    .ubcfgstreamen_in(5'H00),
    .ubdo_in(80'H00000000000000000000),
    .ubdrdy_in(5'H00),
    .ubenable_in(5'H00),
    .ubgpi_in(10'H000),
    .ubintr_in(10'H000),
    .ubiolmbrst_in(5'H00),
    .ubmbrst_in(5'H00),
    .ubmdmcapture_in(5'H00),
    .ubmdmdbgrst_in(5'H00),
    .ubmdmdbgupdate_in(5'H00),
    .ubmdmregen_in(20'H00000),
    .ubmdmshift_in(5'H00),
    .ubmdmsysrst_in(5'H00),
    .ubmdmtck_in(5'H00),
    .ubmdmtdi_in(5'H00),
    .drpdo_common_out(),
    .drprdy_common_out(),
    .pmarsvdout0_out(),
    .pmarsvdout1_out(),
    .qpll0fbclklost_out(),
    .qpll0lock_out(),
    .qpll0outclk_out(qpll0outclk_out),
    .qpll0outrefclk_out(qpll0outrefclk_out),
    .qpll0refclklost_out(),
    .qpll1fbclklost_out(),
    .qpll1lock_out(),
    .qpll1outclk_out(),
    .qpll1outrefclk_out(),
    .qpll1refclklost_out(),
    .qplldmonitor0_out(),
    .qplldmonitor1_out(),
    .refclkoutmonitor0_out(),
    .refclkoutmonitor1_out(),
    .rxrecclk0_sel_out(),
    .rxrecclk1_sel_out(),
    .rxrecclk0sel_out(),
    .rxrecclk1sel_out(),
    .sdm0finalout_out(),
    .sdm0testdata_out(),
    .sdm1finalout_out(),
    .sdm1testdata_out(),
    .tcongpo_out(),
    .tconrsvdout0_out(),
    .ubdaddr_out(),
    .ubden_out(),
    .ubdi_out(),
    .ubdwe_out(),
    .ubmdmtdo_out(),
    .ubrsvdout_out(),
    .ubtxuart_out(),
    .cdrstepdir_in(20'H00000),
    .cdrstepsq_in(20'H00000),
    .cdrstepsx_in(20'H00000),
    .cfgreset_in(20'H00000),
    .clkrsvd0_in(20'H00000),
    .clkrsvd1_in(20'H00000),
    .cpllfreqlock_in(20'H00000),
    .cplllockdetclk_in(20'H00000),
    .cplllocken_in(20'H00000),
    .cpllpd_in(20'HFFFFF),
    .cpllrefclksel_in(60'H249249249249249),
    .cpllreset_in(20'HFFFFF),
    .dmonfiforeset_in(20'H00000),
    .dmonitorclk_in(20'H00000),
    .drpaddr_in(200'H00000000000000000000000000000000000000000000000000),
    .drpclk_in(20'H00000),
    .drpdi_in(320'H00000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .drpen_in(20'H00000),
    .drprst_in(20'H00000),
    .drpwe_in(20'H00000),
    .elpcaldvorwren_in(1'B0),
    .elpcalpaorwren_in(1'B0),
    .evoddphicaldone_in(1'B0),
    .evoddphicalstart_in(1'B0),
    .evoddphidrden_in(1'B0),
    .evoddphidwren_in(1'B0),
    .evoddphixrden_in(1'B0),
    .evoddphixwren_in(1'B0),
    .eyescanmode_in(1'B0),
    .eyescanreset_in(20'H00000),
    .eyescantrigger_in(20'H00000),
    .freqos_in(20'H00000),
    .gtgrefclk_in(20'H00000),
    .gthrxn_in(1'B0),
    .gthrxp_in(1'B0),
    .gtnorthrefclk0_in(20'H00000),
    .gtnorthrefclk1_in(20'H00000),
    .gtrefclk0_in(20'H00000),
    .gtrefclk1_in(20'H00000),
    .gtresetsel_in(1'B0),
    .gtrsvd_in(320'H00000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .gtrxreset_in(20'H00000),
    .gtrxresetsel_in(20'H00000),
    .gtsouthrefclk0_in(20'H00000),
    .gtsouthrefclk1_in(20'H00000),
    .gttxreset_in(20'H00000),
    .gttxresetsel_in(20'H00000),
    .incpctrl_in(20'H00000),
    .gtyrxn_in(gtyrxn_in),
    .gtyrxp_in(gtyrxp_in),
    .loopback_in(60'H000000000000000),
    .looprsvd_in(1'B0),
    .lpbkrxtxseren_in(1'B0),
    .lpbktxrxseren_in(1'B0),
    .pcieeqrxeqadaptdone_in(20'H00000),
    .pcierstidle_in(20'H00000),
    .pciersttxsyncstart_in(20'H00000),
    .pcieuserratedone_in(20'H00000),
    .pcsrsvdin_in(320'H00000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .pcsrsvdin2_in(1'B0),
    .pmarsvdin_in(1'B0),
    .qpll0clk_in(20'H00000),
    .qpll0freqlock_in(20'H00000),
    .qpll0refclk_in(20'H00000),
    .qpll1clk_in(20'H00000),
    .qpll1freqlock_in(20'H00000),
    .qpll1refclk_in(20'H00000),
    .resetovrd_in(20'H00000),
    .rstclkentx_in(1'B0),
    .rx8b10ben_in(20'H00000),
    .rxafecfoken_in(20'HFFFFF),
    .rxbufreset_in(20'H00000),
    .rxcdrfreqreset_in(20'H00000),
    .rxcdrhold_in(20'H00000),
    .rxcdrovrden_in(20'H00000),
    .rxcdrreset_in(20'H00000),
    .rxcdrresetrsv_in(1'B0),
    .rxchbonden_in(20'H00000),
    .rxchbondi_in(100'H0000000000000000000000000),
    .rxchbondlevel_in(60'H000000000000000),
    .rxchbondmaster_in(20'H00000),
    .rxchbondslave_in(20'H00000),
    .rxckcalreset_in(20'H00000),
    .rxckcalstart_in(140'H00000000000000000000000000000000000),
    .rxcommadeten_in(20'H00000),
    .rxdfeagcctrl_in(1'B0),
    .rxdccforcestart_in(1'B0),
    .rxdfeagchold_in(20'H00000),
    .rxdfeagcovrden_in(20'H00000),
    .rxdfecfokfcnum_in(80'HDDDDDDDDDDDDDDDDDDDD),
    .rxdfecfokfen_in(20'H00000),
    .rxdfecfokfpulse_in(20'H00000),
    .rxdfecfokhold_in(20'H00000),
    .rxdfecfokovren_in(20'H00000),
    .rxdfekhhold_in(20'H00000),
    .rxdfekhovrden_in(20'H00000),
    .rxdfelfhold_in(20'H00000),
    .rxdfelfovrden_in(20'H00000),
    .rxdfelpmreset_in(20'H00000),
    .rxdfetap10hold_in(20'H00000),
    .rxdfetap10ovrden_in(20'H00000),
    .rxdfetap11hold_in(20'H00000),
    .rxdfetap11ovrden_in(20'H00000),
    .rxdfetap12hold_in(20'H00000),
    .rxdfetap12ovrden_in(20'H00000),
    .rxdfetap13hold_in(20'H00000),
    .rxdfetap13ovrden_in(20'H00000),
    .rxdfetap14hold_in(20'H00000),
    .rxdfetap14ovrden_in(20'H00000),
    .rxdfetap15hold_in(20'H00000),
    .rxdfetap15ovrden_in(20'H00000),
    .rxdfetap2hold_in(20'H00000),
    .rxdfetap2ovrden_in(20'H00000),
    .rxdfetap3hold_in(20'H00000),
    .rxdfetap3ovrden_in(20'H00000),
    .rxdfetap4hold_in(20'H00000),
    .rxdfetap4ovrden_in(20'H00000),
    .rxdfetap5hold_in(20'H00000),
    .rxdfetap5ovrden_in(20'H00000),
    .rxdfetap6hold_in(20'H00000),
    .rxdfetap6ovrden_in(20'H00000),
    .rxdfetap7hold_in(20'H00000),
    .rxdfetap7ovrden_in(20'H00000),
    .rxdfetap8hold_in(20'H00000),
    .rxdfetap8ovrden_in(20'H00000),
    .rxdfetap9hold_in(20'H00000),
    .rxdfetap9ovrden_in(20'H00000),
    .rxdfeuthold_in(20'H00000),
    .rxdfeutovrden_in(20'H00000),
    .rxdfevphold_in(20'H00000),
    .rxdfevpovrden_in(20'H00000),
    .rxdfevsen_in(1'B0),
    .rxdfexyden_in(20'HFFFFF),
    .rxdlybypass_in(20'HFFFFF),
    .rxdlyen_in(20'H00000),
    .rxdlyovrden_in(20'H00000),
    .rxdlysreset_in(20'H00000),
    .rxelecidlemode_in(40'HFFFFFFFFFF),
    .rxeqtraining_in(20'H00000),
    .rxgearboxslip_in(20'H00000),
    .rxlatclk_in(20'H00000),
    .rxlpmen_in(20'HFFFFF),
    .rxlpmgchold_in(20'H00000),
    .rxlpmgcovrden_in(20'H00000),
    .rxlpmhfhold_in(20'H00000),
    .rxlpmhfovrden_in(20'H00000),
    .rxlpmlfhold_in(20'H00000),
    .rxlpmlfklovrden_in(20'H00000),
    .rxlpmoshold_in(20'H00000),
    .rxlpmosovrden_in(20'H00000),
    .rxmcommaalignen_in(20'H00000),
    .rxmonitorsel_in(40'H0000000000),
    .rxoobreset_in(20'H00000),
    .rxoscalreset_in(20'H00000),
    .rxoshold_in(20'H00000),
    .rxosintcfg_in(1'B0),
    .rxosinten_in(1'B0),
    .rxosinthold_in(1'B0),
    .rxosintovrden_in(1'B0),
    .rxosintstrobe_in(1'B0),
    .rxosinttestovrden_in(1'B0),
    .rxosovrden_in(20'H00000),
    .rxoutclksel_in(60'H492492492492492),
    .rxpcommaalignen_in(20'H00000),
    .rxpcsreset_in(20'H00000),
    .rxpd_in(40'H0000000000),
    .rxphalign_in(20'H00000),
    .rxphalignen_in(20'H00000),
    .rxphdlypd_in(20'HFFFFF),
    .rxphdlyreset_in(20'H00000),
    .rxphovrden_in(1'B0),
    .rxpllclksel_in(40'HFFFFFFFFFF),
    .rxpmareset_in(20'H00000),
    .rxpolarity_in(20'H00000),
    .rxprbscntreset_in(20'H00000),
    .rxprbssel_in(80'H00000000000000000000),
    .rxprogdivreset_in(20'H00000),
    .rxqpien_in(1'B0),
    .rxrate_in(60'H000000000000000),
    .rxratemode_in(20'H00000),
    .rxslide_in(20'H00000),
    .rxslipoutclk_in(20'H00000),
    .rxslippma_in(20'H00000),
    .rxsyncallin_in(20'H00000),
    .rxsyncin_in(20'H00000),
    .rxsyncmode_in(20'H00000),
    .rxsysclksel_in(40'HAAAAAAAAAA),
    .rxtermination_in(20'H00000),
    .rxuserrdy_in(20'HFFFFF),
    .rxusrclk_in(rxusrclk_in),
    .rxusrclk2_in(rxusrclk2_in),
    .sigvalidclk_in(20'H00000),
    .tstin_in(400'H0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .tx8b10bbypass_in(160'H0000000000000000000000000000000000000000),
    .tx8b10ben_in(20'H00000),
    .txbufdiffctrl_in(1'B0),
    .txcominit_in(20'H00000),
    .txcomsas_in(20'H00000),
    .txcomwake_in(20'H00000),
    .txctrl0_in(320'H00000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .txctrl1_in(320'H00000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .txctrl2_in(160'H0000000000000000000000000000000000000000),
    .txdata_in(2560'H0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000),
    .txdataextendrsvd_in(160'H0000000000000000000000000000000000000000),
    .txdccforcestart_in(20'H00000),
    .txdccreset_in(20'H00000),
    .txdeemph_in(40'H00000),
    .txdetectrx_in(20'H00000),
    .txdiffctrl_in(100'HC6318C6318C6318C6318C6318),
    .txdiffpd_in(1'B0),
    .txdlybypass_in(20'HFFFFF),
    .txdlyen_in(20'H00000),
    .txdlyhold_in(20'H00000),
    .txdlyovrden_in(20'H00000),
    .txdlysreset_in(20'H00000),
    .txdlyupdown_in(20'H00000),
    .txelecidle_in(20'H00000),
    .txelforcestart_in(1'B0),
    .txheader_in(120'H000000000000000000000000000000),
    .txinhibit_in(20'H00000),
    .txlatclk_in(20'H00000),
    .txlfpstreset_in(20'H00000),
    .txlfpsu2lpexit_in(20'H00000),
    .txlfpsu3wake_in(20'H00000),
    .txmaincursor_in(140'HA142850A142850A142850A142850A142850),
    .txmargin_in(60'H000000000000000),
    .txmuxdcdexhold_in(20'H00000),
    .txmuxdcdorwren_in(20'H00000),
    .txoneszeros_in(20'H00000),
    .txoutclksel_in(60'HB6DB6DB6DB6DB6D),
    .txpcsreset_in(20'H00000),
    .txpd_in(40'H0000000000),
    .txpdelecidlemode_in(20'H00000),
    .txphalign_in(20'H00000),
    .txphalignen_in(20'H00000),
    .txphdlypd_in(20'HFFFFF),
    .txphdlyreset_in(20'H00000),
    .txphdlytstclk_in(20'H00000),
    .txphinit_in(20'H00000),
    .txphovrden_in(20'H00000),
    .txpippmen_in(20'H00000),
    .txpippmovrden_in(20'H00000),
    .txpippmpd_in(20'H00000),
    .txpippmsel_in(20'HFFFFF),
    .txpippmstepsize_in(100'H0000000000000000000000000),
    .txpisopd_in(20'H00000),
    .txpllclksel_in(40'HFFFFFFFFFF),
    .txpmareset_in(20'H00000),
    .txpolarity_in(20'H00000),
    .txpostcursor_in(100'H0000000000000000000000000),
    .txpostcursorinv_in(1'B0),
    .txprbsforceerr_in(20'H00000),
    .txprbssel_in(80'H00000000000000000000),
    .txprecursor_in(100'H0000000000000000000000000),
    .txprecursorinv_in(1'B0),
    .txprogdivreset_in(20'H00000),
    .txqpibiasen_in(1'B0),
    .txqpistrongpdown_in(1'B0),
    .txqpiweakpup_in(1'B0),
    .txrate_in(60'H000000000000000),
    .txratemode_in(20'H00000),
    .txsequence_in(140'H00000000000000000000000000000000000),
    .txswing_in(20'H00000),
    .txsyncallin_in(20'H00000),
    .txsyncin_in(20'H00000),
    .txsyncmode_in(20'H00000),
    .txsysclksel_in(40'HAAAAAAAAAA),
    .txuserrdy_in(20'HFFFFF),
    .txusrclk_in(txusrclk_in),
    .txusrclk2_in(txusrclk2_in),
    .bufgtce_out(),
    .bufgtcemask_out(),
    .bufgtdiv_out(),
    .bufgtreset_out(),
    .bufgtrstmask_out(),
    .cpllfbclklost_out(),
    .cplllock_out(),
    .cpllrefclklost_out(),
    .dmonitorout_out(),
    .dmonitoroutclk_out(),
    .drpdo_out(),
    .drprdy_out(),
    .eyescandataerror_out(),
    .gthtxn_out(),
    .gthtxp_out(),
    .gtpowergood_out(gtpowergood_out),
    .gtrefclkmonitor_out(),
    .gtytxn_out(gtytxn_out),
    .gtytxp_out(gtytxp_out),
    .pcierategen3_out(),
    .pcierateidle_out(),
    .pcierateqpllpd_out(),
    .pcierateqpllreset_out(),
    .pciesynctxsyncdone_out(),
    .pcieusergen3rdy_out(),
    .pcieuserphystatusrst_out(),
    .pcieuserratestart_out(),
    .pcsrsvdout_out(),
    .phystatus_out(),
    .pinrsrvdas_out(),
    .powerpresent_out(),
    .resetexception_out(),
    .rxbufstatus_out(),
    .rxbyteisaligned_out(),
    .rxbyterealign_out(),
    .rxcdrlock_out(),
    .rxcdrphdone_out(),
    .rxchanbondseq_out(),
    .rxchanisaligned_out(),
    .rxchanrealign_out(),
    .rxchbondo_out(),
    .rxckcaldone_out(),
    .rxclkcorcnt_out(),
    .rxcominitdet_out(),
    .rxcommadet_out(),
    .rxcomsasdet_out(),
    .rxcomwakedet_out(),
    .rxctrl0_out(),
    .rxctrl1_out(),
    .rxctrl2_out(),
    .rxctrl3_out(),
    .rxdata_out(),
    .rxdataextendrsvd_out(),
    .rxdatavalid_out(),
    .rxdlysresetdone_out(),
    .rxelecidle_out(),
    .rxheader_out(),
    .rxheadervalid_out(),
    .rxlfpstresetdet_out(),
    .rxlfpsu2lpexitdet_out(),
    .rxlfpsu3wakedet_out(),
    .rxmonitorout_out(),
    .rxosintdone_out(),
    .rxosintstarted_out(),
    .rxosintstrobedone_out(),
    .rxosintstrobestarted_out(),
    .rxoutclk_out(rxoutclk_out),
    .rxoutclkfabric_out(),
    .rxoutclkpcs_out(),
    .rxphaligndone_out(),
    .rxphalignerr_out(),
    .rxpmaresetdone_out(rxpmaresetdone_out),
    .rxprbserr_out(),
    .rxprbslocked_out(),
    .rxprgdivresetdone_out(),
    .rxqpisenn_out(),
    .rxqpisenp_out(),
    .rxratedone_out(),
    .rxrecclkout_out(),
    .rxresetdone_out(),
    .rxsliderdy_out(),
    .rxslipdone_out(),
    .rxslipoutclkrdy_out(),
    .rxslippmardy_out(),
    .rxstartofseq_out(),
    .rxstatus_out(),
    .rxsyncdone_out(),
    .rxsyncout_out(),
    .rxvalid_out(),
    .txbufstatus_out(),
    .txcomfinish_out(),
    .txdccdone_out(),
    .txdlysresetdone_out(),
    .txoutclk_out(txoutclk_out),
    .txoutclkfabric_out(),
    .txoutclkpcs_out(),
    .txphaligndone_out(),
    .txphinitdone_out(),
    .txpmaresetdone_out(txpmaresetdone_out),
    .txprgdivresetdone_out(txprgdivresetdone_out),
    .txqpisenn_out(),
    .txqpisenp_out(),
    .txratedone_out(),
    .txresetdone_out(),
    .txsyncdone_out(),
    .txsyncout_out()
  );
endmodule
