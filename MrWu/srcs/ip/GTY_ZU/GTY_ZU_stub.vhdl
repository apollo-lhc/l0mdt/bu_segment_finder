-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
-- Date        : Wed Sep  5 14:45:47 2018
-- Host        : baby running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub d:/vproject/l0mdt_ZU/l0mdt_ZU.srcs/sources_1/ip/GTY_ZU/GTY_ZU_stub.vhdl
-- Design      : GTY_ZU
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xczu11eg-ffvc1760-1-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GTY_ZU is
  Port ( 
    gtwiz_userclk_tx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_userclk_rx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_clk_freerun_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_all_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_cdr_stable_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_userdata_tx_in : in STD_LOGIC_VECTOR ( 1279 downto 0 );
    gtwiz_userdata_rx_out : out STD_LOGIC_VECTOR ( 1279 downto 0 );
    gtrefclk00_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    qpll0outclk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    qpll0outrefclk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gtyrxn_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtyrxp_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    rxusrclk_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    rxusrclk2_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    txusrclk_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    txusrclk2_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtpowergood_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtytxn_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtytxp_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rxoutclk_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rxpmaresetdone_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    txoutclk_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    txpmaresetdone_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    txprgdivresetdone_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end GTY_ZU;

architecture stub of GTY_ZU is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[1279:0],gtwiz_userdata_rx_out[1279:0],gtrefclk00_in[3:0],qpll0outclk_out[3:0],qpll0outrefclk_out[3:0],gtyrxn_in[15:0],gtyrxp_in[15:0],rxusrclk_in[15:0],rxusrclk2_in[15:0],txusrclk_in[15:0],txusrclk2_in[15:0],gtpowergood_out[15:0],gtytxn_out[15:0],gtytxp_out[15:0],rxoutclk_out[15:0],rxpmaresetdone_out[15:0],txoutclk_out[15:0],txpmaresetdone_out[15:0],txprgdivresetdone_out[15:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "GTY_ZU_gtwizard_top,Vivado 2018.2";
begin
end;
