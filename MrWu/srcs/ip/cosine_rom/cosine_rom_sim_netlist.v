// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Tue Sep  4 15:25:36 2018
// Host        : baby running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim D:/L0MDT/srcs/ip/cosine_rom/cosine_rom_sim_netlist.v
// Design      : cosine_rom
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku15p-ffva1760-1-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "cosine_rom,blk_mem_gen_v8_4_1,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_1,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module cosine_rom
   (clka,
    addra,
    douta,
    clkb,
    addrb,
    doutb);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [11:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [335:0]douta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [11:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [335:0]doutb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [335:0]douta;
  wire [335:0]doutb;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [335:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "12" *) 
  (* C_ADDRB_WIDTH = "12" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "37" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     122.949432 mW" *) 
  (* C_FAMILY = "kintexuplus" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "1" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "cosine_rom.mem" *) 
  (* C_INIT_FILE_NAME = "cosine_rom.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "4" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "4096" *) 
  (* C_READ_DEPTH_B = "4096" *) 
  (* C_READ_WIDTH_A = "336" *) 
  (* C_READ_WIDTH_B = "336" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "4096" *) 
  (* C_WRITE_DEPTH_B = "4096" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "336" *) 
  (* C_WRITE_WIDTH_B = "336" *) 
  (* C_XDEVICEFAMILY = "kintexuplus" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  cosine_rom_blk_mem_gen_v8_4_1 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[11:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[11:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[335:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_generic_cstr" *) 
module cosine_rom_blk_mem_gen_generic_cstr
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [335:0]douta;
  output [335:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [335:0]douta;
  wire [335:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_width \ramloop[0].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[3:0]),
        .doutb(doutb[3:0]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized9 \ramloop[10].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[93:85]),
        .doutb(doutb[93:85]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized10 \ramloop[11].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[102:94]),
        .doutb(doutb[102:94]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized11 \ramloop[12].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[111:103]),
        .doutb(doutb[111:103]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized12 \ramloop[13].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[120:112]),
        .doutb(doutb[120:112]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized13 \ramloop[14].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[129:121]),
        .doutb(doutb[129:121]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized14 \ramloop[15].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[138:130]),
        .doutb(doutb[138:130]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized15 \ramloop[16].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[147:139]),
        .doutb(doutb[147:139]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized16 \ramloop[17].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[156:148]),
        .doutb(doutb[156:148]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized17 \ramloop[18].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[165:157]),
        .doutb(doutb[165:157]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized18 \ramloop[19].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[174:166]),
        .doutb(doutb[174:166]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized0 \ramloop[1].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[12:4]),
        .doutb(doutb[12:4]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized19 \ramloop[20].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[183:175]),
        .doutb(doutb[183:175]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized20 \ramloop[21].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[192:184]),
        .doutb(doutb[192:184]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized21 \ramloop[22].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[201:193]),
        .doutb(doutb[201:193]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized22 \ramloop[23].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[210:202]),
        .doutb(doutb[210:202]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized23 \ramloop[24].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[219:211]),
        .doutb(doutb[219:211]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized24 \ramloop[25].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[228:220]),
        .doutb(doutb[228:220]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized25 \ramloop[26].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[237:229]),
        .doutb(doutb[237:229]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized26 \ramloop[27].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[246:238]),
        .doutb(doutb[246:238]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized27 \ramloop[28].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[255:247]),
        .doutb(doutb[255:247]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized28 \ramloop[29].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[264:256]),
        .doutb(doutb[264:256]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized1 \ramloop[2].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[21:13]),
        .doutb(doutb[21:13]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized29 \ramloop[30].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[273:265]),
        .doutb(doutb[273:265]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized30 \ramloop[31].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[282:274]),
        .doutb(doutb[282:274]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized31 \ramloop[32].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[291:283]),
        .doutb(doutb[291:283]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized32 \ramloop[33].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[300:292]),
        .doutb(doutb[300:292]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized33 \ramloop[34].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[309:301]),
        .doutb(doutb[309:301]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized34 \ramloop[35].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[318:310]),
        .doutb(doutb[318:310]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized35 \ramloop[36].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[327:319]),
        .doutb(doutb[327:319]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized36 \ramloop[37].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[335:328]),
        .doutb(doutb[335:328]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized2 \ramloop[3].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[30:22]),
        .doutb(doutb[30:22]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized3 \ramloop[4].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[39:31]),
        .doutb(doutb[39:31]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized4 \ramloop[5].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[48:40]),
        .doutb(doutb[48:40]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized5 \ramloop[6].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[57:49]),
        .doutb(doutb[57:49]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized6 \ramloop[7].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[66:58]),
        .doutb(doutb[66:58]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized7 \ramloop[8].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[75:67]),
        .doutb(doutb[75:67]),
        .sleep(sleep));
  cosine_rom_blk_mem_gen_prim_width__parameterized8 \ramloop[9].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta[84:76]),
        .doutb(doutb[84:76]),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [3:0]douta;
  output [3:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [3:0]douta;
  wire [3:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized0
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized0 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized1
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized1 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized10
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized10 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized11
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized11 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized12
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized12 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized13
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized13 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized14
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized14 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized15
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized15 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized16
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized16 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized17
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized17 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized18
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized18 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized19
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized19 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized2
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized2 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized20
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized20 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized21
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized21 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized22
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized22 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized23
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized23 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized24
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized24 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized25
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized25 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized26
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized26 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized27
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized27 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized28
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized28 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized29
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized29 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized3
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized3 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized30
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized30 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized31
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized31 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized32
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized32 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized33
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized33 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized34
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized34 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized35
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized35 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized36
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [7:0]douta;
  output [7:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [7:0]douta;
  wire [7:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized36 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized4
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized4 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized5
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized5 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized6
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized6 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized7
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized7 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized8
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized8 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module cosine_rom_blk_mem_gen_prim_width__parameterized9
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized9 \prim_init.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [3:0]douta;
  output [3:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [3:0]douta;
  wire [3:0]doutb;
  wire sleep;
  wire [15:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_CASDOUTA_UNCONNECTED ;
  wire [15:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_CASDOUTB_UNCONNECTED ;
  wire [1:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_CASDOUTPA_UNCONNECTED ;
  wire [1:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_CASDOUTPB_UNCONNECTED ;
  wire [15:4]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOUTADOUT_UNCONNECTED ;
  wire [15:4]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOUTBDOUT_UNCONNECTED ;
  wire [1:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [1:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOUTPBDOUTP_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB18E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h00F002469C05AF5C3B3B5E93FB741FDCCCCEF147BF49F5C3B3C60A51D9742100),
    .INIT_01(256'h30FDDDDEF136AE27D3A19193C72D952FDCBAABCE0369E28D4B2A2B5F940C9632),
    .INIT_02(256'h40DA87555568ACF37C16D3B2B3D71C730DA87666679BE159E4A07E6F81B61D96),
    .INIT_03(256'hFA51EB8654444579CF37C27E5C4D6F94FA630ECBAAABCE037BF49F6D5D5E82D8),
    .INIT_04(256'hC5FA61EA8643223457AD049E4A18F80A4E940C974321123579C049E4A18081B4),
    .INIT_05(256'h092C61C841ECA9999ABD036AF49F6D5D6F83D941DB97655568ACF37B06C3A1A2),
    .INIT_06(256'h191A4D83EA731FDCCCDEF147BF39E4B2A2B4E83EA731FDCBBCDE0369D27D3908),
    .INIT_07(256'h07F7093D83FB863210001347AD16B06D4B3C5E83EA62FDBA999ABCF158D17D3A),
    .INIT_08(256'hC3A2B4D71C840DB97666679BD048C17D3A2A3C5FA51D964210F001358BF38D39),
    .INIT_09(256'h3A2A2B4E93FB741FEDCCCDE0359D16B18E6E6F82D73FB853200001247AD15A06),
    .INIT_0A(256'h29091B4E940C96310FFFF1247AD16B06D4C4C5F93E952FCA987778ACE148D27D),
    .INIT_0B(256'h07092C61C841ECA98889ACE047B05A07E6E6F82D83FC864210001347AD15AF5B),
    .INIT_0C(256'h5E71C72EA7421FEEEF0247AD16B06D4B3B4E72D840DA8644334579BE26AF5B18),
    .INIT_0D(256'h71C840DA875544568AC037B06C2908092C61C730DB87655668ACF26AE49F6D4C),
    .INIT_0E(256'h9520ECBAAABCD0259C16B17E5D5E70A50B740EBA887789BDF269E38E4B2A2A3D),
    .INIT_0F(256'hCBAAABCE036AE27C29F7E6F81B61C841ECA987788ACE148C05B17E6D6E81C61D),
    .INIT_10(256'hF147AE27C17E5C4C5E82C73EB8521FEEEEF0247AE26B06D3A2A3C5FA50C852FD),
    .INIT_11(256'h16C3918192C60B62EB85310FFF012469C049E3906E5D6F82D83EB7420EDCCCDE),
    .INIT_12(256'hB5FA50C8520ECBAAAACDF147BF38D3907F7F81B50B62EB8642111123469CF37C),
    .INIT_13(256'h987666678ADF26AE38D3A18081A3D72D841EB97544344579BE159D28E4A29192),
    .INIT_14(256'h26AF49F6C3B3B4D71B61D9520DBA98889ABD0259D16B17D4C3B4D60A50C741EB),
    .INIT_15(256'hC60B62DA631FDCBAABBCE0258C049E3906E5D6E81B60C730DA864322234579CF),
    .INIT_16(256'hCCDF0358CF38D28E5B3A2B4D71B61D952FDB98766779ACE148C05AF5B2919193),
    .INIT_17(256'h3B4D60A4FA51EA7531FEEDDEF01369CF37C17C3908F8092C60B62EA7420EDCBB),
    .INIT_18(256'h445689CE148C05A06C2918092B4E93EA52EB8642100F0013469CF37B05A06D4B),
    .INIT_19(256'hE71B50B72FB8531FEDCCCCDE0247AD159E38E4B29191A3C60B61C851ECA86554),
    .INIT_1A(256'hE259D16B17D4A29192B4E82D83FB741FCB987767789BDF258C059E4A06D4C4C5),
    .INIT_1B(256'h20ECBAA99AABDE0358CF38C17C29F6E5D6E70A4E94FB740DB86532222234579C),
    .INIT_1C(256'h4E71B50B72EB741FDBA9877788ABDF147AE26B05B17D4B3B3C4E71B50B73FB85),
    .INIT_1D(256'hF4A06D4B3B3B4D70A5FA51D952FCA8654332334568ACF258C05AF4A06D4B3B3C),
    .INIT_1E(256'h9CF259D26B16C28F6D5D5D6F82C61B62D962FC97532100F00123468BE148C059),
    .INIT_1F(256'hBCDE02469CF26AE38D28E4B1807F7092B4E83E94FB740DA864210FEEEFF01357),
    .INIT_20(256'h5310FFEEEF012468AD047BF49E38E4B2907F7081B4E82D83EA62FC96420EDCBB),
    .INIT_21(256'h50C841EB86432100F00123579CF269D26B16C28F6D5D5D6F82C61B62D952FC97),
    .INIT_22(256'h3B3B4D60A4FA50C852FCA8654332334568ACF259D15AF5A07D4B3B3B4D60A4F9),
    .INIT_23(256'h8BF37B05B17E4C3B3B4D71B50B62EA741FDBA8877789ABDF147BE27B05B17E4C),
    .INIT_24(256'h97543222223568BD047BF49E4A07E6D5E6F92C71C83FC8530EDBAA99AABCE025),
    .INIT_25(256'hC4C4D60A4E950C852FDB987767789BCF147BF38D28E4B29192A4D71B61D952EC),
    .INIT_26(256'h5568ACE158C16B06C3A19192B4E83E951DA7420EDCCCCDEF1358BF27B05B17E5),
    .INIT_27(256'h4D60A50B73FC9643100F0012468BE25AE39E4B2908192C60A50C841EC9865444),
    .INIT_28(256'hBCDE0247AE26B06C2908F8093C71C73FC96310FEDDEEF1357AE15AF4A06D4B3B),
    .INIT_29(256'h919192B5FA50C841ECA97766789BDF259D16B17D4B2A3B5E82D83FC8530FDCCB),
    .INIT_2A(256'hC975432223468AD037C06B18E6D5E6093E940C8520ECBBAABCDF136AD26B06C3),
    .INIT_2B(256'hE147C05A06D4B3C4D71B61D9520DBA98889ABD0259D16B17D4B3B3C6F94FA62F),
    .INIT_2C(256'h9192A4E82D951EB97544344579BE148D27D3A18081A3D83EA62FDA876666789B),
    .INIT_2D(256'h73FC9643211112468BE26B05B18F7F7093D83FB741FDCAAAABCE0258C05AF5B2),
    .INIT_2E(256'hDCCCDE0247BE38D28F6D5E6093E940C964210FFF01358BE26B06C2918193C61C),
    .INIT_2F(256'hF258C05AF5C3A2A3D60B62EA7420FEEEEF1258BE37C28E5C4C5E71C72EA741FE),
    .INIT_30(256'h16C18E6D6E71B50C841ECA887789ACE148C16B18F6E7F92C72EA630ECBAAABCD),
    .INIT_31(256'h3A2A2B4E83E962FDB987788ABE047B05A07E5D5E71B61C9520DCBAAABCE0259D),
    .INIT_32(256'h4D6F94EA62FCA86655678BD037C16C2908092C60B730CA865445578AD048C17D),
    .INIT_33(256'h1B5FA62EB9754334468AD048D27E4B3B4D60B61DA7420FEEEF1247AE27C17E5C),
    .INIT_34(256'h5FA51DA743100012468CF38D28F6E6E70A50B740ECA98889ACE148C16C290708),
    .INIT_35(256'h72D841ECA877789ACF259E39F5C4C4D60B61DA7421FFFF01369C049E4B19092B),
    .INIT_36(256'h0A51DA742100002358BF37D28F6E6E81B61D9530EDCCCDEF147BF39E4B2A2A3D),
    .INIT_37(256'h3D83FB853100F012469D15AF5C3A2A3D71C840DB97666679BD048C17D4B2A3C6),
    .INIT_38(256'h3D71D851FCBA999ABDF26AE38E5C3B4D60B61DA743100012368BF38D3907F709),
    .INIT_39(256'h093D72D9630EDCBBCDF137AE38E4B2A2B4E93FB741FEDCCCDF137AE38D4A191A),
    .INIT_3A(256'hA1A3C60B73FCA86555679BD149D38F6D5D6F94FA630DBA9999ACE148C16C2908),
    .INIT_3B(256'hB18081A4E940C975321123479C049E4A08F81A4E940DA7543223468AE16AF5C2),
    .INIT_3C(256'hD28E5D5D6F94FB730ECBAAABCE036AF49F6D4C5E72C73FC9754444568BE15AF4),
    .INIT_3D(256'h9D16B18F6E70A4E951EB97666678AD037C17D3B2B3D61C73FCA86555578AD048),
    .INIT_3E(256'h369C049F5B2A2B4D82E9630ECBAABCDF259D27C39191A3D72EA631FEDDDDF036),
    .INIT_3F(256'h012479D15A06C3B3C5F94FB741FECCCCDF147BF39E5B3B3C5FA50C964200F002),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_CASDOUTA_UNCONNECTED [15:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_CASDOUTB_UNCONNECTED [15:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_CASDOUTPA_UNCONNECTED [1:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_CASDOUTPB_UNCONNECTED [1:0]),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOUTADOUT_UNCONNECTED [15:4],douta}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOUTBDOUT_UNCONNECTED [15:4],doutb}),
        .DOUTPADOUTP(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOUTPADOUTP_UNCONNECTED [1:0]),
        .DOUTPBDOUTP(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOUTPBDOUTP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized0
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h07FFFFFFFE0000000001FFFFFFFFFFFFC0000000000000000000000000000000),
    .INITP_01(256'h003FFFC0001FFFF00003FFFF00000FFFFF000007FFFFF0000007FFFFFF000000),
    .INITP_02(256'hE007FF001FFC007FF001FFE001FFE001FFF0007FFC001FFF8001FFF8000FFFE0),
    .INITP_03(256'hF807F807F803FC03FE01FF007F803FE00FF801FF007FE00FFC00FFC00FFC00FF),
    .INITP_04(256'hC0FE07F01F80FE03F80FC07F01FC03F80FE03FC07F00FE01FC03FC07F807F807),
    .INITP_05(256'hF07E07C0FC1F81F81F03F03F03F03F03F03F03F03F81F81FC0FC0FE07F03F81F),
    .INITP_06(256'hF83E0F83E07C1F07C1F03E0F83F07C1F83E07C1F83E07C0F81F07E0FC0F81F03),
    .INITP_07(256'hE0F0783E1F0783C1F0783E1F0783E0F07C1F0F83E0F07C1F07C1F0F83E0F83E0),
    .INITP_08(256'h0F83E0F83E1F07C1F07C1E0F83E1F07C1E0F83C1F0F83C1F0783C1F0F83C1E0F),
    .INITP_09(256'h81F03E07E0FC1F03E07C0F83F07C0F83F07C1F83E0F81F07C1F07C0F83E0F83E),
    .INITP_0A(256'hF03F81FC0FE07E07F03F03F81F81F81F81F81F81F81F81F03F03F07E07C0FC1F),
    .INITP_0B(256'hC03FC03FC07F807F00FE01FC07F80FE03F807F01FC07E03F80FE03F01FC0FE07),
    .INITP_0C(256'hFE007FE007FE007FE00FFC01FF003FE00FF803FC01FF00FF807F803FC03FC03F),
    .INITP_0D(256'h0FFFE0003FFF0003FFF0007FFC001FFF000FFF000FFF001FFC007FF001FFC00F),
    .INITP_0E(256'h000001FFFFFFC000001FFFFFC00001FFFFE00001FFFF80001FFFF00007FFF800),
    .INITP_0F(256'h00000000000000000000000000000007FFFFFFFFFFFF0000000000FFFFFFFFC0),
    .INIT_00(256'h0F0E0D0C0B0B0A09080707060505040403030202020101010000000000000000),
    .INIT_01(256'h413F3C3B39373533312F2E2C2A2827252422211F1E1C1B1A1817161514121110),
    .INIT_02(256'h93908D8A8784817E7C797673706E6B686663615E5C59575452504E4B49474543),
    .INIT_03(256'h0804FFFBF7F3EFEBE7E4E0DCD8D4D1CDC9C6C2BFBBB8B4B1ADAAA7A3A09D9A96),
    .INIT_04(256'h9E98938E89847F7A75706B66615D58534E4A45413C37332E2A26211D1914100C),
    .INIT_05(256'h554F48423C36302A241E18120C0600FBF5EFEAE4DED9D3CEC8C3BDB8B3ADA8A3),
    .INIT_06(256'h2D261F18110902FBF4EDE6DFD8D1CBC4BDB6AFA9A29B958E88817B746E68615B),
    .INIT_07(256'h271F170F06FEF6EEE6DED6CEC6BEB6AEA69E978F8780787069615A524B433C35),
    .INIT_08(256'h433930271E140B02F9F0E7DED5CCC3BAB1A89F978E857C746B635A5249413830),
    .INIT_09(256'h7F756A60564C41372D23190F05FBF1E7DDD3C9BFB6ACA2988F857C72685F564C),
    .INIT_0A(256'hDDD1C6BBAFA4998E82776C61564B40352A1F1409FEF4E9DED4C9BEB4A99F948A),
    .INIT_0B(256'h5C4F43362A1E1105F9EDE0D4C8BCB0A4988C8074695D5145392E22160BFFF4E8),
    .INIT_0C(256'hFBEEE0D3C5B8AB9D908376695B4E4134271A0D00F4E7DACDC0B4A79A8E817568),
    .INIT_0D(256'hBCAD9F9082736557483A2C1E1001F3E5D7C9BBAD9F928476685A4D3F31241609),
    .INIT_0E(256'h9D8D7E6E5F4F4031211203F4E4D5C6B7A8998A7B6C5D4E4031221305F6E7D9CA),
    .INIT_0F(256'h9F8E7D6D5D4C3C2B1B0BFAEADACABAAA9A8A7A6A5A4A3A2A1A0AFBEBDBCCBCAC),
    .INIT_10(256'hC1AF9E8C7B69584635241301F0DFCEBDAC9B897968574635241302F2E1D0C0AF),
    .INIT_11(256'h03F1DECCB9A79482705D4B39271402F0DECCBAA8968472604E3D2B1907F6E4D2),
    .INIT_12(256'h66523F2B1804F1DECAB7A4907D6A5744311E0BF8E5D2BFAC998673614E3B2916),
    .INIT_13(256'hE9D4C0AB97826E5945311C08F4E0CCB7A38F7B67533F2B1804F0DCC8B5A18D7A),
    .INIT_14(256'h8B76604B35200AF5E0CAB5A08B75604B36210CF7E2CDB8A38E7A65503B2712FD),
    .INIT_15(256'h4E37210AF4DDC7B09A846E57412B15FFE9D2BCA6907B654F39230DF8E2CCB7A1),
    .INIT_16(256'h2F1800E9D1BAA38B745D452E1700E9D2BBA48D765F48311A03ECD6BFA8917B64),
    .INIT_17(256'h311800E7CFB69E866D553D250CF4DCC4AC947C644C341C05EDD5BDA68E765F47),
    .INIT_18(256'h51371E04EBD2B89F866D533A2108EFD6BDA48B725940270EF6DDC4AB937A6249),
    .INIT_19(256'h90755B41260CF2D7BDA3896F553A2006ECD2B99F856B51371E04EAD1B79D846A),
    .INIT_1A(256'hEED2B79C80654A2F13F8DDC2A78C71563B2005EACFB59A7F644A2F14FADFC5AA),
    .INIT_1B(256'h6A4E3115F9DDC0A4886C503418FCE0C4A88C7055391D01E6CAAE93775C402509),
    .INIT_1C(256'h05E7CAAD907255381BFEE1C4A78A6D503317FADDC0A4876A4E3115F8DCBFA386),
    .INIT_1D(256'hBD9F8163442608EACCAE9072543719FBDDBFA28466492B0DF0D2B5977A5D3F22),
    .INIT_1E(256'h9374553617F8D9BA9B7C5D3E2001E2C3A58667492A0BEDCEB09173553618FADB),
    .INIT_1F(256'h8767472707E7C7A78868482808E9C9A98A6A4A2B0BECCCAD8E6E4F3010F1D2B3),
    .INIT_20(256'h9877563615F4D3B29171502F0FEECDAD8C6C4B2B0AEAC9A98969482808E8C7A7),
    .INIT_21(256'hC6A583613F1DFCDAB89775533210EFCDAC8A69482605E4C2A1805F3E1DFBDAB9),
    .INIT_22(256'h11EFCCA98664411FFCD9B794724F2D0BE8C6A4815F3D1BF9D7B492704E2C0AE8),
    .INIT_23(256'h7955310EEAC7A3805C3915F2CFAB8865411EFBD8B5926F4B2805E3C09D7A5734),
    .INIT_24(256'hFCD7B38E6A4621FDD9B4906C4723FFDBB7936F4B2703DFBB97734F2B07E4C09C),
    .INIT_25(256'h9B76502B06E1BB96714C2701DCB7926D4823FED9B4906B4621FCD8B38E6A4521),
    .INIT_26(256'h56300AE3BD97714B25FFD9B38D67411BF5CFAA845E3813EDC7A27C57310CE6C1),
    .INIT_27(256'h2C05DEB79069421BF4CDA67F59320BE4BE97704A23FDD6B089633C16EFC9A37C),
    .INIT_28(256'h1DF5CDA57E562E06DEB78F674018F0C9A17A522B03DCB58D663F17F0C9A27A53),
    .INIT_29(256'h2900D7AE865D340CE3BB926A4119F0C8A0774F27FED6AE865E350DE5BD956D45),
    .INIT_2A(256'h4F25FBD2A87F552C03D9B0875D340BE2B88F663D14EBC29970471EF5CCA37A51),
    .INIT_2B(256'h8E643A0FE5BB90663C12E8BD93693F15EBC1976D431AF0C69C72491FF5CBA278),
    .INIT_2C(256'hE8BC91663B10E5BA8F64390EE3B88D63380DE2B88D62370DE2B88D63380EE3B9),
    .INIT_2D(256'h5A2E02D6AB7F5327FBCFA4784C21F5C99E72471BF0C4996D4217EBC095693E13),
    .INIT_2E(256'hE6B98C603306DAAD815427FBCFA276491DF1C4986C3F13E7BB8F63360ADEB286),
    .INIT_2F(256'h8A5C2F01D4A7794C1EF1C497693C0FE2B5885A2D00D3A6794C20F3C6996C3F12),
    .INIT_30(256'h4618E9BB8D5F3103D5A7794A1DEFC193653709DBAD805224F7C99B6E4012E5B7),
    .INIT_31(256'h1AEBBC8D5E2F00D1A3744516E8B98A5C2DFED0A1734416E7B98A5C2DFFD1A274),
    .INIT_32(256'h05D5A6764617E7B8885929FACA9B6B3C0DDDAE7F4F20F1C292633405D6A77849),
    .INIT_33(256'h07D7A7764616E5B5855424F4C494643303D3A3734313E3B3845424F4C4946535),
    .INIT_34(256'h20EFBE8D5C2BFAC998673605D4A3734211E0B07F4E1DEDBC8C5B2AFAC9996838),
    .INIT_35(256'h4F1EECBA885725F3C2905E2DFBCA98673504D2A16F3E0DDBAA794716E5B48351),
    .INIT_36(256'h94622FFDCB98663301CF9C6A3806D4A16F3D0BD9A7754311DFAD7B4917E5B381),
    .INIT_37(256'hEFBC885522EFBC895623F0BD8A5725F2BF8C5926F4C18E5C29F6C4915E2CF9C7),
    .INIT_38(256'h5E2AF6C38F5B28F4C08D5925F2BE8B5724F0BD895623EFBC895522EFBB885522),
    .INIT_39(256'hE2AD794510DCA8733F0BD6A26E3A06D19D693501CD996531FDC995612DF9C692),
    .INIT_3A(256'h7A4510DBA6713C07D29D6833FECA95602BF7C28D5824EFBB86511DE8B47F4B16),
    .INIT_3B(256'h25F0BA854F19E4AE79430ED8A36D3803CD98622DF8C38D5823EEB8834E19E4AF),
    .INIT_3C(256'hE4AE78420BD59F6933FDC7915B25EEB9834D17E1AB753F09D39E6832FCC7915B),
    .INIT_3D(256'hB67F4812DBA46D3700C9935C25EFB8814B14DEA7713A04CD97612AF4BE87511B),
    .INIT_3E(256'h9A632BF4BD854E17DFA8713A03CB945D26EFB8814A12DBA46D36FFC9925B24ED),
    .INIT_3F(256'h905820E8B1794109D199612AF2BA824B13DBA46C34FDC58E561FE7AF784109D2),
    .INIT_40(256'h094178AFE71F568EC5FD346CA4DB134B82BAF22A6199D1094179B1E8205890C8),
    .INIT_41(256'h245B92C9FF366DA4DB124A81B8EF265D94CB033A71A8DF174E85BDF42B639AD2),
    .INIT_42(256'h5187BEF42A6197CD043A71A7DE144B81B8EF255C93C900376DA4DB12487FB6ED),
    .INIT_43(256'h91C7FC32689ED3093F75ABE1174D83B9EE255B91C7FD33699FD50B4278AEE41B),
    .INIT_44(256'hE4194E83B8EE23588DC3F82D6298CD03386DA3D80E4379AEE4194F85BAF0255B),
    .INIT_45(256'h4B7FB4E81D5186BBEF24588DC2F72B6095CAFE33689DD2073C71A6DB10457AAF),
    .INIT_46(256'hC6F92D6195C9FD316599CD0135699DD1063A6EA2D60B3F73A8DC104579ADE216),
    .INIT_47(256'h5588BBEF225589BCEF235689BDF024578BBEF225598DC0F4285B8FC3F62A5E92),
    .INIT_48(256'hF92C5E91C4F6295C8EC1F426598CBFF225578ABDF0235689BCEF225588BCEF22),
    .INIT_49(256'hB3E517497BADDF114375A7D90B3D6FA1D406386A9CCF01336698CBFD2F6294C7),
    .INIT_4A(256'h83B4E5164779AADB0D3E6FA1D204356798CAFB2D5E90C2F3255788BAEC1E4F81),
    .INIT_4B(256'h6899C9FA2A5B8CBCED1D4E7FB0E0114273A3D405366798C9FA2B5C8DBEEF2051),
    .INIT_4C(256'h6594C4F4245484B3E3134373A3D303336494C4F4245485B5E5164676A7D70738),
    .INIT_4D(256'h78A7D605346392C2F1204F7FAEDD0D3C6B9BCAFA295988B8E7174676A6D50535),
    .INIT_4E(256'hA2D1FF2D5C8AB9E7164473A1D0FE2D5C8AB9E8164574A3D1002F5E8DBCEB1A49),
    .INIT_4F(256'hE512406E9BC9F7245280ADDB09376593C1EF1D4A79A7D503315F8DBBE9184674),
    .INIT_50(256'h3F6C99C6F3204C79A6D3002D5A88B5E20F3C6997C4F11E4C79A7D4012F5C8AB7),
    .INIT_51(256'hB2DE0A36638FBBE7133F6C98C4F11D4976A2CFFB275481ADDA0633608CB9E612),
    .INIT_52(256'h3E6995C0EB17426D99C4F01B47729EC9F5214C78A4CFFB27537FABD6022E5A86),
    .INIT_53(256'hE30E38638DB8E20D37628DB8E20D38638DB8E30E39648FBAE5103B6691BCE813),
    .INIT_54(256'hA2CBF51F49729CC6F01A436D97C1EB153F6993BDE8123C6690BBE50F3A648EB9),
    .INIT_55(256'h7AA3CCF51E477099C2EB143D668FB8E20B345D87B0D9032C557FA8D2FB254F78),
    .INIT_56(256'h6D95BDE50D355E86AED6FE274F77A0C8F019416A92BBE30C345D86AED7002951),
    .INIT_57(256'h7AA2C9F0173F668DB5DC032B527AA1C9F01840678FB7DE062E567EA5CDF51D45),
    .INIT_58(256'hA3C9EF163C6389B0D6FD234A7097BEE40B32597FA6CDF41B426990B7DE052C53),
    .INIT_59(256'hE60C31577CA2C7ED13385E84AACFF51B41678DB3D9FF254B7197BDE30A30567C),
    .INIT_5A(256'h456A8EB3D8FC21466B90B4D9FE23486D92B7DC01274C7196BBE1062B50769BC1),
    .INIT_5B(256'hC0E4072B4F7397BBDF03274B6F93B7DBFF23476C90B4D9FD21466A8EB3D7FC21),
    .INIT_5C(256'h577A9DC0E305284B6F92B5D8FB1E416588ABCFF215395C80A3C7EA0E3155799C),
    .INIT_5D(256'h0A2C4E7092B4D7F91B3D5F81A4C6E80B2D4F7294B7D9FC1F416486A9CCEF1134),
    .INIT_5E(256'hDAFB1D3E5F80A1C2E4052648698AACCDEF1032537597B8DAFC1D3F6183A5C6E8),
    .INIT_5F(256'hC7E80828486989A9C9EA0A2B4B6C8CADCDEE0F2F507191B2D3F41536567798B9),
    .INIT_60(256'hD2F110304F6E8EADCCEC0B2B4A6A8AA9C9E90828486888A7C7E70727476787A7),
    .INIT_61(256'hFA1836557391B0CEED0B2A496786A5C3E201203E5D7C9BBAD9F81736557493B3),
    .INIT_62(256'h3F5D7A97B5D2F00D2B496684A2BFDDFB1937547290AECCEA08264463819FBDDB),
    .INIT_63(256'hA3BFDCF815314E6A87A4C0DDFA1733506D8AA7C4E1FE1B38557290ADCAE70522),
    .INIT_64(256'h25405C7793AECAE6011D3955708CA8C4E0FC1834506C88A4C0DDF915314E6A86),
    .INIT_65(256'hC5DFFA142F4A647F9AB5CFEA05203B56718CA7C2DDF8132F4A65809CB7D2EE09),
    .INIT_66(256'h849DB7D1EA041E37516B859FB9D2EC06203A556F89A3BDD7F20C26415B7590AA),
    .INIT_67(256'h627A93ABC4DDF60E274059728BA4BDD6EF08213A536D869FB8D2EB041E37516A),
    .INIT_68(256'h5F768EA6BDD5ED051C344C647C94ACC4DCF40C253D556D869EB6CFE700183149),
    .INIT_69(256'h7B91A8BFD6EC031A31485F768DA4BBD2E900172E455D748BA3BAD1E900182F47),
    .INIT_6A(256'hB7CCE2F80D23394F657B90A6BCD2E9FF152B41576E849AB0C7DDF40A21374E64),
    .INIT_6B(256'h12273B50657A8EA3B8CDE2F70C21364B60758BA0B5CAE0F50A20354B60768BA1),
    .INIT_6C(256'h8DA1B5C8DCF004182B3F53677B8FA3B7CCE0F4081C3145596E8297ABC0D4E9FD),
    .INIT_6D(256'h293B4E61738699ACBFD2E5F80B1E3144576A7D90A4B7CADEF104182B3F52667A),
    .INIT_6E(256'hE4F607192B3D4E60728496A8BACCDEF0021427394B5D708294A7B9CCDEF10316),
    .INIT_6F(256'hC0D0E1F20213243546576879899BACBDCEDFF0011324354658697B8C9EAFC1D2),
    .INIT_70(256'hBCCCDBEBFB0A1A2A3A4A5A6A7A8A9AAABACADAEAFA0B1B2B3C4C5D6D7D8E9FAF),
    .INIT_71(256'hD9E7F605132231404E5D6C7B8A99A8B7C6D5E4F403122131404F5F6E7E8D9DAC),
    .INIT_72(256'h1624313F4D5A687684929FADBBC9D7E5F301101E2C3A4857657382909FADBCCA),
    .INIT_73(256'h75818E9AA7B4C0CDDAE7F4000D1A2734414E5B697683909DABB8C5D3E0EEFB09),
    .INIT_74(256'hF4FF0B16222E3945515D6974808C98A4B0BCC8D4E0EDF905111E2A36434F5C68),
    .INIT_75(256'h949FA9B4BEC9D4DEE9F4FE09141F2A35404B56616C77828E99A4AFBBC6D1DDE8),
    .INIT_76(256'h565F68727C858F98A2ACB6BFC9D3DDE7F1FB050F19232D37414C56606A757F8A),
    .INIT_77(256'h384149525A636B747C858E979FA8B1BAC3CCD5DEE7F0F9020B141E273039434C),
    .INIT_78(256'h3C434B525A6169707880878F979EA6AEB6BEC6CED6DEE6EEF6FE060F171F2730),
    .INIT_79(256'h61686E747B81888E959BA2A9AFB6BDC4CBD1D8DFE6EDF4FB020911181F262D35),
    .INIT_7A(256'hA8ADB3B8BDC3C8CED3D9DEE4EAEFF5FB00060C12181E242A30363C42484F555B),
    .INIT_7B(256'h1014191D21262A2E33373C41454A4E53585D61666B70757A7F84898E93989EA3),
    .INIT_7C(256'h9A9DA0A3A7AAADB1B4B8BBBFC2C6C9CDD1D4D8DCE0E4E7EBEFF3F7FBFF04080C),
    .INIT_7D(256'h4547494B4E50525457595C5E616366686B6E707376797C7E8184878A8D909396),
    .INIT_7E(256'h111214151617181A1B1C1E1F2122242527282A2C2E2F31333537393B3C3F4143),
    .INIT_7F(256'h0000000000000001010102020203030404050506070708090A0B0B0C0D0E0F10),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized1
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h6B48772969111EDE227C3AEFB4877096EBEE1DF5DA451478A2096FBEE0DDDC3E),
    .INITP_01(256'h777728D69690EEBBEE53E2DF2227DA5BC4447B4B75DF70F146412D228235BC44),
    .INITP_02(256'hE5111106909608D775C8B7CB487B4B84B84FB44FB04FB04B84B8787878B78A77),
    .INITP_03(256'hB444272D116087913C229AEE962379443C2720E1116D60834B44C45A5CA20DE1),
    .INITP_04(256'h5A130B45D2F27BC0EE8841DEEF7443D2E9D845DD11634AC22D16886AC2213168),
    .INITP_05(256'h5D0B3E6B5DB24210B3668E5EF7A710BBF127A610B18C6845D167BDE68B3DE689),
    .INITP_06(256'hD0999999990B9982E3DE3A17B6DBD18217B6F4217A2F453062484194C90932F4),
    .INITP_07(256'hFA0A1433A8AE4146FAE617D0CCBD2514385F66FA16D0A285E3D9BD71E85F78F7),
    .INITP_08(256'h17DE3DF42F1D7B378F428A16D0BECDF43851497A6617D0CEBEC504EA2B9850A0),
    .INITP_09(256'h745E9921265304248C1945E8BD085EDBD08317B6DBD0B8F78E8333A133333332),
    .INITP_0A(256'hB522CF79A2CF7BCD17442C631A10CBC91FBA11CBDEF4E2CD9A10849B75ACF9A1),
    .INITP_0B(256'h5A2D190886AC22D16886A58D117744372E97845DEEF70422EE07BC9E9745A190),
    .INITP_0C(256'h4F0F608A74B44645A5820D6D110E09C878453D88D2EEB2887913C20D1169C844),
    .INITP_0D(256'hDDDCA3DA3C3C3C3A43A41BE41BE45BE43A43A5BC25A7DA275DD620D212C11111),
    .INITP_0E(256'hAC447B5882896904C51E1DF75DA5BC4447B4B7C889F68F94EFBAEE12D2D629DD),
    .INITP_0F(256'h44F877760EFBED208A3C5144B75F70EFAED21DC25BEEB87C88F6F1112D29DC25),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0101010101010101010101010101010000000000000000000000000000000000),
    .INIT_06(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_07(256'h0202020202010101010101010101010101010101010101010101010101010101),
    .INIT_08(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_09(256'h0303030303030303030303030302020202020202020202020202020202020202),
    .INIT_0A(256'h0303030303030303030303030303030303030303030303030303030303030303),
    .INIT_0B(256'h0404040404040404040404040404040404040404040404040404040404030303),
    .INIT_0C(256'h0505050505050505050505050505050505050505040404040404040404040404),
    .INIT_0D(256'h0606060606060606060606060606050505050505050505050505050505050505),
    .INIT_0E(256'h0707070707070707070707060606060606060606060606060606060606060606),
    .INIT_0F(256'h0808080808080808080807070707070707070707070707070707070707070707),
    .INIT_10(256'h0909090909090909090909090808080808080808080808080808080808080808),
    .INIT_11(256'h0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0909090909090909090909090909090909),
    .INIT_12(256'h0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0A0A0A0A0A0A0A0A0A0A0A0A0A),
    .INIT_13(256'h0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0B0B0B0B0B0B0B),
    .INIT_14(256'h0E0E0E0E0E0E0E0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0C),
    .INIT_15(256'h0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E),
    .INIT_16(256'h111111101010101010101010101010101010101010101010100F0F0F0F0F0F0F),
    .INIT_17(256'h1212121212121212121212121211111111111111111111111111111111111111),
    .INIT_18(256'h1414141413131313131313131313131313131313131313131212121212121212),
    .INIT_19(256'h1515151515151515151515151515151514141414141414141414141414141414),
    .INIT_1A(256'h1717171717171717171616161616161616161616161616161616161615151515),
    .INIT_1B(256'h1919191918181818181818181818181818181818181818171717171717171717),
    .INIT_1C(256'h1B1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1919191919191919191919191919),
    .INIT_1D(256'h1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B),
    .INIT_1E(256'h1E1E1E1E1E1E1E1E1E1E1E1E1E1E1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1C1C),
    .INIT_1F(256'h202020202020202020202020201F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1E1E1E),
    .INIT_20(256'h2222222222222222222222222221212121212121212121212121212121202020),
    .INIT_21(256'h2424242424242424242424242424232323232323232323232323232323222222),
    .INIT_22(256'h2726262626262626262626262626262625252525252525252525252525252524),
    .INIT_23(256'h2929292928282828282828282828282828282727272727272727272727272727),
    .INIT_24(256'h2B2B2B2B2B2B2B2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A29292929292929292929),
    .INIT_25(256'h2D2D2D2D2D2D2D2D2D2D2D2D2C2C2C2C2C2C2C2C2C2C2C2C2C2B2B2B2B2B2B2B),
    .INIT_26(256'h3030302F2F2F2F2F2F2F2F2F2F2F2F2F2E2E2E2E2E2E2E2E2E2E2E2E2E2E2D2D),
    .INIT_27(256'h3232323232323232313131313131313131313131313030303030303030303030),
    .INIT_28(256'h3534343434343434343434343434333333333333333333333333333232323232),
    .INIT_29(256'h3737373737373737363636363636363636363636353535353535353535353535),
    .INIT_2A(256'h3A3A393939393939393939393939393838383838383838383838383737373737),
    .INIT_2B(256'h3C3C3C3C3C3C3C3C3C3C3B3B3B3B3B3B3B3B3B3B3B3B3A3A3A3A3A3A3A3A3A3A),
    .INIT_2C(256'h3F3F3F3F3F3F3E3E3E3E3E3E3E3E3E3E3E3E3D3D3D3D3D3D3D3D3D3D3D3D3C3C),
    .INIT_2D(256'h42424241414141414141414141414040404040404040404040403F3F3F3F3F3F),
    .INIT_2E(256'h4444444444444444444444434343434343434343434343424242424242424242),
    .INIT_2F(256'h4747474747474747474646464646464646464646464545454545454545454545),
    .INIT_30(256'h4A4A4A4A4A4A4A4A494949494949494949494948484848484848484848484747),
    .INIT_31(256'h4D4D4D4D4D4D4D4C4C4C4C4C4C4C4C4C4C4B4B4B4B4B4B4B4B4B4B4B4A4A4A4A),
    .INIT_32(256'h5050505050504F4F4F4F4F4F4F4F4F4F4F4E4E4E4E4E4E4E4E4E4E4E4D4D4D4D),
    .INIT_33(256'h5353535353535252525252525252525252515151515151515151515050505050),
    .INIT_34(256'h5656565656565555555555555555555555545454545454545454545353535353),
    .INIT_35(256'h5959595959595958585858585858585858585757575757575757575756565656),
    .INIT_36(256'h5C5C5C5C5C5C5C5C5C5B5B5B5B5B5B5B5B5B5B5A5A5A5A5A5A5A5A5A5A595959),
    .INIT_37(256'h5F5F5F5F5F5F5F5F5F5F5E5E5E5E5E5E5E5E5E5E5D5D5D5D5D5D5D5D5D5D5C5C),
    .INIT_38(256'h6363626262626262626262626161616161616161616160606060606060606060),
    .INIT_39(256'h6666666666656565656565656565656464646464646464646363636363636363),
    .INIT_3A(256'h6969696969696969686868686868686868676767676767676767676666666666),
    .INIT_3B(256'h6D6C6C6C6C6C6C6C6C6C6C6B6B6B6B6B6B6B6B6B6A6A6A6A6A6A6A6A6A6A6969),
    .INIT_3C(256'h70707070706F6F6F6F6F6F6F6F6F6E6E6E6E6E6E6E6E6E6E6D6D6D6D6D6D6D6D),
    .INIT_3D(256'h7373737373737373737272727272727272727171717171717171717070707070),
    .INIT_3E(256'h7777777676767676767676767675757575757575757574747474747474747473),
    .INIT_3F(256'h7A7A7A7A7A7A7A7A797979797979797979787878787878787878777777777777),
    .INIT_40(256'h77777777777878787878787878787979797979797979797A7A7A7A7A7A7A7A7A),
    .INIT_41(256'h7474747474747474747575757575757575757676767676767676767677777777),
    .INIT_42(256'h7070707071717171717171717172727272727272727273737373737373737373),
    .INIT_43(256'h6D6D6D6D6D6D6D6E6E6E6E6E6E6E6E6E6E6F6F6F6F6F6F6F6F6F707070707070),
    .INIT_44(256'h696A6A6A6A6A6A6A6A6A6A6B6B6B6B6B6B6B6B6B6C6C6C6C6C6C6C6C6C6C6D6D),
    .INIT_45(256'h6666666667676767676767676767686868686868686868696969696969696969),
    .INIT_46(256'h6363636363636364646464646464646465656565656565656565666666666666),
    .INIT_47(256'h6060606060606060606161616161616161616162626262626262626262636363),
    .INIT_48(256'h5C5D5D5D5D5D5D5D5D5D5D5E5E5E5E5E5E5E5E5E5E5F5F5F5F5F5F5F5F5F5F60),
    .INIT_49(256'h59595A5A5A5A5A5A5A5A5A5A5B5B5B5B5B5B5B5B5B5B5C5C5C5C5C5C5C5C5C5C),
    .INIT_4A(256'h5656565757575757575757575758585858585858585858585959595959595959),
    .INIT_4B(256'h5353535354545454545454545454555555555555555555555556565656565656),
    .INIT_4C(256'h5050505051515151515151515151525252525252525252525253535353535353),
    .INIT_4D(256'h4D4D4D4E4E4E4E4E4E4E4E4E4E4E4F4F4F4F4F4F4F4F4F4F4F50505050505050),
    .INIT_4E(256'h4A4A4A4B4B4B4B4B4B4B4B4B4B4B4C4C4C4C4C4C4C4C4C4C4D4D4D4D4D4D4D4D),
    .INIT_4F(256'h47484848484848484848484849494949494949494949494A4A4A4A4A4A4A4A4A),
    .INIT_50(256'h4545454545454545454546464646464646464646464647474747474747474747),
    .INIT_51(256'h4242424242424242434343434343434343434343444444444444444444444445),
    .INIT_52(256'h3F3F3F3F3F404040404040404040404040414141414141414141414142424242),
    .INIT_53(256'h3C3D3D3D3D3D3D3D3D3D3D3D3D3E3E3E3E3E3E3E3E3E3E3E3E3F3F3F3F3F3F3F),
    .INIT_54(256'h3A3A3A3A3A3A3A3A3A3B3B3B3B3B3B3B3B3B3B3B3B3C3C3C3C3C3C3C3C3C3C3C),
    .INIT_55(256'h37373737383838383838383838383838393939393939393939393939393A3A3A),
    .INIT_56(256'h3535353535353535353535363636363636363636363636373737373737373737),
    .INIT_57(256'h3232323233333333333333333333333333343434343434343434343434343535),
    .INIT_58(256'h3030303030303030303031313131313131313131313131323232323232323232),
    .INIT_59(256'h2D2E2E2E2E2E2E2E2E2E2E2E2E2E2E2F2F2F2F2F2F2F2F2F2F2F2F2F30303030),
    .INIT_5A(256'h2B2B2B2B2B2B2C2C2C2C2C2C2C2C2C2C2C2C2C2D2D2D2D2D2D2D2D2D2D2D2D2D),
    .INIT_5B(256'h2929292929292929292A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2B2B2B2B2B2B2B2B),
    .INIT_5C(256'h2727272727272727272727272728282828282828282828282828282929292929),
    .INIT_5D(256'h2525252525252525252525252525252626262626262626262626262626262727),
    .INIT_5E(256'h2222232323232323232323232323232323242424242424242424242424242424),
    .INIT_5F(256'h2020212121212121212121212121212121212222222222222222222222222222),
    .INIT_60(256'h1E1E1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F2020202020202020202020202020),
    .INIT_61(256'h1C1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E),
    .INIT_62(256'h1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C),
    .INIT_63(256'h191919191919191919191919191A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1B1B),
    .INIT_64(256'h1717171717171717181818181818181818181818181818181818181919191919),
    .INIT_65(256'h1515151616161616161616161616161616161616161617171717171717171717),
    .INIT_66(256'h1414141414141414141414141414141515151515151515151515151515151515),
    .INIT_67(256'h1212121212121213131313131313131313131313131313131313131414141414),
    .INIT_68(256'h1111111111111111111111111111111111111212121212121212121212121212),
    .INIT_69(256'h0F0F0F0F0F0F1010101010101010101010101010101010101010101011111111),
    .INIT_6A(256'h0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F),
    .INIT_6B(256'h0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0E0E0E0E0E0E0E0E),
    .INIT_6C(256'h0B0B0B0B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C),
    .INIT_6D(256'h0A0A0A0A0A0A0A0A0A0A0A0A0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B),
    .INIT_6E(256'h090909090909090909090909090909090A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A),
    .INIT_6F(256'h0808080808080808080808080808080808080809090909090909090909090909),
    .INIT_70(256'h0707070707070707070707070707070707070707070808080808080808080808),
    .INIT_71(256'h0606060606060606060606060606060606060606070707070707070707070707),
    .INIT_72(256'h0505050505050505050505050505050505060606060606060606060606060606),
    .INIT_73(256'h0404040404040404040404050505050505050505050505050505050505050505),
    .INIT_74(256'h0303040404040404040404040404040404040404040404040404040404040404),
    .INIT_75(256'h0303030303030303030303030303030303030303030303030303030303030303),
    .INIT_76(256'h0202020202020202020202020202020202020303030303030303030303030303),
    .INIT_77(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_78(256'h0101010101010101010101010101010101010101010101010101020202020202),
    .INIT_79(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_7A(256'h0000000000000000000000000000000001010101010101010101010101010101),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized10
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'hFFFFFFF800000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_05(256'h000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_06(256'hFFFF000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_08(256'h0001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000),
    .INITP_0B(256'h3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0101010101010101010101010101010101000000000000000000000000000000),
    .INIT_02(256'h0303030202020202020202020202020202020202020202010101010101010101),
    .INIT_03(256'h0505050504040404040404040404040404040303030303030303030303030303),
    .INIT_04(256'h0707070707070707070706060606060606060606060605050505050505050505),
    .INIT_05(256'h0A0A0A0A0A0A0A0A0A0A09090909090909090909080808080808080808080807),
    .INIT_06(256'h0E0E0E0E0E0E0D0D0D0D0D0D0D0D0C0C0C0C0C0C0C0C0C0B0B0B0B0B0B0B0B0B),
    .INIT_07(256'h1212121212121111111111111111101010101010100F0F0F0F0F0F0F0F0E0E0E),
    .INIT_08(256'h1717171616161616161615151515151515141414141414141313131313131312),
    .INIT_09(256'h1C1C1C1C1B1B1B1B1B1B1A1A1A1A1A1A19191919191919181818181818171717),
    .INIT_0A(256'h222222212121212120202020201F1F1F1F1F1F1E1E1E1E1E1E1D1D1D1D1D1D1C),
    .INIT_0B(256'h2828282827272727272626262626252525252524242424242323232323222222),
    .INIT_0C(256'h2F2F2E2E2E2E2E2D2D2D2D2C2C2C2C2C2B2B2B2B2B2A2A2A2A2A292929292928),
    .INIT_0D(256'h3636363535353535343434343333333332323232323131313130303030302F2F),
    .INIT_0E(256'h3E3E3D3D3D3D3C3C3C3C3B3B3B3B3A3A3A3A3939393939383838383737373736),
    .INIT_0F(256'h46464645454545444444444343434342424241414141404040403F3F3F3F3E3E),
    .INIT_10(256'h4F4F4E4E4E4E4D4D4D4C4C4C4C4B4B4B4B4A4A4A494949494848484847474746),
    .INIT_11(256'h585858575757565656565555555454545453535352525252515151505050504F),
    .INIT_12(256'h6262616161606060605F5F5F5E5E5E5D5D5D5C5C5C5C5B5B5B5A5A5A59595959),
    .INIT_13(256'h6C6C6C6B6B6B6A6A6A6969696868686767676766666665656564646463636362),
    .INIT_14(256'h77777676767575757474747373737272727171717070706F6F6F6E6E6E6D6D6D),
    .INIT_15(256'h8282828181818080807F7F7E7E7E7D7D7D7C7C7C7B7B7B7A7A7A797979787877),
    .INIT_16(256'h8E8E8D8D8D8C8C8C8B8B8A8A8A89898988888887878686868585858484848383),
    .INIT_17(256'h9A9A9A99999998989797979696959595949494939392929291919090908F8F8F),
    .INIT_18(256'hA7A7A6A6A6A5A5A4A4A4A3A3A2A2A2A1A1A0A0A09F9F9E9E9E9D9D9C9C9C9B9B),
    .INIT_19(256'hB4B4B4B3B3B2B2B1B1B1B0B0AFAFAFAEAEADADADACACABABAAAAAAA9A9A8A8A8),
    .INIT_1A(256'hC2C2C1C1C0C0C0BFBFBEBEBDBDBDBCBCBBBBBABABAB9B9B8B8B7B7B7B6B6B5B5),
    .INIT_1B(256'hD0D0CFCFCFCECECDCDCCCCCBCBCACACAC9C9C8C8C7C7C7C6C6C5C5C4C4C3C3C3),
    .INIT_1C(256'hDFDEDEDEDDDDDCDCDBDBDADAD9D9D8D8D8D7D7D6D6D5D5D4D4D3D3D3D2D2D1D1),
    .INIT_1D(256'hEEEEEDEDECECEBEBEAEAE9E9E8E8E7E7E6E6E6E5E5E4E4E3E3E2E2E1E1E0E0DF),
    .INIT_1E(256'hFEFDFDFCFCFBFBFAFAF9F9F8F8F7F7F6F6F5F5F4F4F3F3F2F2F1F1F0F0F0EFEF),
    .INIT_1F(256'h0E0D0D0C0C0B0B0A0A0909080807070606050504040303020201010000FFFFFE),
    .INIT_20(256'h1E1E1D1D1C1C1B1B1A1A191818171716161515141413131212111110100F0F0E),
    .INIT_21(256'h2F2F2E2E2D2C2C2B2B2A2A292928282727262625242423232222212120201F1F),
    .INIT_22(256'h41403F3F3E3E3D3D3C3C3B3B3A39393838373736363535343333323231313030),
    .INIT_23(256'h5252515150504F4E4E4D4D4C4C4B4B4A49494848474746464544444343424241),
    .INIT_24(256'h6564636362626161605F5F5E5E5D5D5C5B5B5A5A595958575756565555545353),
    .INIT_25(256'h77777676757474737372717170706F6F6E6D6D6C6C6B6A6A6969686867666665),
    .INIT_26(256'h8A8A89898887878686858484838382818180807F7E7E7D7D7C7B7B7A7A797978),
    .INIT_27(256'h9E9D9D9C9C9B9A9A999898979796959594949392929191908F8F8E8D8D8C8C8B),
    .INIT_28(256'hB2B1B1B0AFAFAEAEADACACABAAAAA9A9A8A7A7A6A5A5A4A4A3A2A2A1A1A09F9F),
    .INIT_29(256'hC6C6C5C4C4C3C3C2C1C1C0BFBFBEBDBDBCBCBBBABAB9B8B8B7B6B6B5B5B4B3B3),
    .INIT_2A(256'hDBDBDAD9D9D8D7D7D6D5D5D4D3D3D2D1D1D0CFCFCECDCDCCCCCBCACAC9C8C8C7),
    .INIT_2B(256'hF0F0EFEEEEEDECECEBEAEAE9E8E8E7E6E6E5E4E4E3E2E2E1E0E0DFDEDEDDDCDC),
    .INIT_2C(256'h06050504030302010100FFFFFEFDFCFCFBFAFAF9F8F8F7F6F6F5F4F4F3F2F2F1),
    .INIT_2D(256'h1C1B1B1A1918181716161514141312121110100F0E0D0D0C0B0B0A0909080707),
    .INIT_2E(256'h32323130302F2E2D2D2C2B2B2A29282827262625242423222221201F1F1E1D1D),
    .INIT_2F(256'h49484847464645444343424141403F3E3E3D3C3C3B3A39393837373635343433),
    .INIT_30(256'h605F5F5E5D5D5C5B5A5A59585757565555545352525150504F4E4D4D4C4B4B4A),
    .INIT_31(256'h7877767575747373727170706F6E6D6D6C6B6A6A696868676665656463626261),
    .INIT_32(256'h908F8E8D8D8C8B8A8A898887878685848483828181807F7E7E7D7C7B7B7A7978),
    .INIT_33(256'hA8A7A6A5A5A4A3A2A2A1A09F9F9E9D9C9C9B9A99999897969695949393929190),
    .INIT_34(256'hC0C0BFBEBDBCBCBBBAB9B9B8B7B6B6B5B4B3B2B2B1B0AFAFAEADACACABAAA9A9),
    .INIT_35(256'hD9D8D8D7D6D5D5D4D3D2D1D1D0CFCECECDCCCBCACAC9C8C7C7C6C5C4C3C3C2C1),
    .INIT_36(256'hF2F2F1F0EFEFEEEDECEBEBEAE9E8E7E7E6E5E4E3E3E2E1E0E0DFDEDDDCDCDBDA),
    .INIT_37(256'h0C0B0A0A090807060605040302020100FFFEFEFDFCFBFAFAF9F8F7F6F6F5F4F3),
    .INIT_38(256'h26252424232221201F1F1E1D1C1B1B1A1918171716151413131211100F0E0E0D),
    .INIT_39(256'h403F3F3E3D3C3B3A3A3938373635353433323131302F2E2D2C2C2B2A29282827),
    .INIT_3A(256'h5B5A5958575756555453525251504F4E4D4D4C4B4A4948484746454443434241),
    .INIT_3B(256'h76757473727170706F6E6D6C6B6B6A6968676666656463626161605F5E5D5C5C),
    .INIT_3C(256'h91908F8E8D8C8C8B8A8988878686858483828181807F7E7D7C7B7B7A79787776),
    .INIT_3D(256'hACABAAAAA9A8A7A6A5A4A3A3A2A1A09F9E9D9D9C9B9A99989897969594939292),
    .INIT_3E(256'hC8C7C6C5C4C3C3C2C1C0BFBEBDBDBCBBBAB9B8B7B6B6B5B4B3B2B1B0B0AFAEAD),
    .INIT_3F(256'hE4E3E2E1E0DFDFDEDDDCDBDAD9D8D8D7D6D5D4D3D2D1D1D0CFCECDCCCBCACAC9),
    .INIT_40(256'hAEAFB0B0B1B2B3B4B5B6B6B7B8B9BABBBCBDBDBEBFC0C1C2C3C3C4C5C6C7C8C9),
    .INIT_41(256'h9293949596979898999A9B9C9D9D9E9FA0A1A2A3A3A4A5A6A7A8A9AAAAABACAD),
    .INIT_42(256'h7778797A7B7B7C7D7E7F8081818283848586868788898A8B8C8C8D8E8F909192),
    .INIT_43(256'h5C5D5E5F6061616263646566666768696A6B6B6C6D6E6F707071727374757676),
    .INIT_44(256'h424343444546474848494A4B4C4D4D4E4F5051525253545556575758595A5B5C),
    .INIT_45(256'h2828292A2B2C2C2D2E2F3031313233343535363738393A3A3B3C3D3E3F3F4041),
    .INIT_46(256'h0E0E0F1011121313141516171718191A1B1B1C1D1E1F1F202122232424252627),
    .INIT_47(256'hF4F5F6F6F7F8F9FAFAFBFCFDFEFEFF0001020203040506060708090A0A0B0C0D),
    .INIT_48(256'hDBDCDCDDDEDFE0E0E1E2E3E3E4E5E6E7E7E8E9EAEBEBECEDEEEFEFF0F1F2F2F3),
    .INIT_49(256'hC2C3C3C4C5C6C7C7C8C9CACACBCCCDCECECFD0D1D1D2D3D4D5D5D6D7D8D8D9DA),
    .INIT_4A(256'hA9AAABACACADAEAFAFB0B1B2B2B3B4B5B6B6B7B8B9B9BABBBCBCBDBEBFC0C0C1),
    .INIT_4B(256'h9192939394959696979899999A9B9C9C9D9E9F9FA0A1A2A2A3A4A5A5A6A7A8A9),
    .INIT_4C(256'h797A7B7B7C7D7E7E7F808181828384848586878788898A8A8B8C8D8D8E8F9090),
    .INIT_4D(256'h62626364656566676868696A6A6B6C6D6D6E6F70707172737374757576777878),
    .INIT_4E(256'h4B4B4C4D4D4E4F50505152525354555556575758595A5A5B5C5D5D5E5F5F6061),
    .INIT_4F(256'h3434353637373839393A3B3C3C3D3E3E3F40414142434344454646474848494A),
    .INIT_50(256'h1D1E1F1F20212222232424252626272828292A2B2B2C2D2D2E2F303031323233),
    .INIT_51(256'h070809090A0B0B0C0D0D0E0F1010111212131414151616171818191A1B1B1C1D),
    .INIT_52(256'hF2F2F3F4F4F5F6F6F7F8F8F9FAFAFBFCFCFDFEFFFF0001010203030405050607),
    .INIT_53(256'hDCDDDEDEDFE0E0E1E2E2E3E4E4E5E6E6E7E8E8E9EAEAEBECECEDEEEEEFF0F0F1),
    .INIT_54(256'hC8C8C9CACACBCCCCCDCDCECFCFD0D1D1D2D3D3D4D5D5D6D7D7D8D9D9DADBDBDC),
    .INIT_55(256'hB3B4B5B5B6B6B7B8B8B9BABABBBCBCBDBDBEBFBFC0C1C1C2C3C3C4C4C5C6C6C7),
    .INIT_56(256'h9FA0A1A1A2A2A3A4A4A5A5A6A7A7A8A9A9AAAAABACACADAEAEAFAFB0B1B1B2B3),
    .INIT_57(256'h8C8C8D8D8E8F8F909191929293949495959697979898999A9A9B9C9C9D9D9E9F),
    .INIT_58(256'h79797A7A7B7B7C7D7D7E7E7F80808181828383848485868687878889898A8A8B),
    .INIT_59(256'h666667686869696A6A6B6C6C6D6D6E6F6F707071717273737474757676777778),
    .INIT_5A(256'h53545555565657575859595A5A5B5B5C5D5D5E5E5F5F60616162626363646565),
    .INIT_5B(256'h4242434344444546464747484849494A4B4B4C4C4D4D4E4E4F50505151525253),
    .INIT_5C(256'h3031313232333334353536363737383839393A3B3B3C3C3D3D3E3E3F3F404141),
    .INIT_5D(256'h1F202021212222232324242526262727282829292A2A2B2B2C2C2D2E2E2F2F30),
    .INIT_5E(256'h0F0F101011111212131314141515161617171818191A1A1B1B1C1C1D1D1E1E1F),
    .INIT_5F(256'hFFFF00000101020203030404050506060707080809090A0A0B0B0C0C0D0D0E0E),
    .INIT_60(256'hEFF0F0F0F1F1F2F2F3F3F4F4F5F5F6F6F7F7F8F8F9F9FAFAFBFBFCFCFDFDFEFE),
    .INIT_61(256'hE0E0E1E1E2E2E3E3E4E4E5E5E6E6E6E7E7E8E8E9E9EAEAEBEBECECEDEDEEEEEF),
    .INIT_62(256'hD1D2D2D3D3D3D4D4D5D5D6D6D7D7D8D8D8D9D9DADADBDBDCDCDDDDDEDEDEDFDF),
    .INIT_63(256'hC3C3C4C4C5C5C6C6C7C7C7C8C8C9C9CACACACBCBCCCCCDCDCECECFCFCFD0D0D1),
    .INIT_64(256'hB5B6B6B7B7B7B8B8B9B9BABABABBBBBCBCBDBDBDBEBEBFBFC0C0C0C1C1C2C2C3),
    .INIT_65(256'hA8A8A9A9AAAAAAABABACACADADADAEAEAFAFAFB0B0B1B1B1B2B2B3B3B4B4B4B5),
    .INIT_66(256'h9B9C9C9C9D9D9E9E9E9F9FA0A0A0A1A1A2A2A2A3A3A4A4A4A5A5A6A6A6A7A7A8),
    .INIT_67(256'h8F8F90909091919292929393949494959595969697979798989999999A9A9A9B),
    .INIT_68(256'h8384848485858586868687878888888989898A8A8A8B8B8C8C8C8D8D8D8E8E8F),
    .INIT_69(256'h78787979797A7A7A7B7B7B7C7C7C7D7D7D7E7E7E7F7F80808081818182828283),
    .INIT_6A(256'h6D6D6E6E6E6F6F6F707070717171727272737373747474757575767676777777),
    .INIT_6B(256'h636363646464656565666666676767676868686969696A6A6A6B6B6B6C6C6C6D),
    .INIT_6C(256'h5959595A5A5A5B5B5B5C5C5C5C5D5D5D5E5E5E5F5F5F60606060616161626262),
    .INIT_6D(256'h5050505051515152525252535353545454545555555656565657575758585859),
    .INIT_6E(256'h47474748484848494949494A4A4A4B4B4B4B4C4C4C4C4D4D4D4E4E4E4E4F4F4F),
    .INIT_6F(256'h3E3F3F3F3F404040404141414142424243434343444444444545454546464646),
    .INIT_70(256'h373737373838383839393939393A3A3A3A3B3B3B3B3C3C3C3C3D3D3D3D3E3E3E),
    .INIT_71(256'h2F30303030303131313132323232323333333334343434353535353536363636),
    .INIT_72(256'h29292929292A2A2A2A2A2B2B2B2B2B2C2C2C2C2C2D2D2D2D2E2E2E2E2E2F2F2F),
    .INIT_73(256'h2222232323232324242424242525252525262626262627272727272828282828),
    .INIT_74(256'h1D1D1D1D1D1D1E1E1E1E1E1E1F1F1F1F1F1F2020202020212121212122222222),
    .INIT_75(256'h1717181818181818191919191919191A1A1A1A1A1A1B1B1B1B1B1B1C1C1C1C1C),
    .INIT_76(256'h1313131313131314141414141414151515151515151616161616161617171717),
    .INIT_77(256'h0E0E0F0F0F0F0F0F0F0F10101010101010111111111111111112121212121212),
    .INIT_78(256'h0B0B0B0B0B0B0B0B0C0C0C0C0C0C0C0C0C0D0D0D0D0D0D0D0D0E0E0E0E0E0E0E),
    .INIT_79(256'h0808080808080808080808090909090909090909090A0A0A0A0A0A0A0A0A0A0B),
    .INIT_7A(256'h0505050505050505050606060606060606060606060707070707070707070707),
    .INIT_7B(256'h0303030303030303030303030304040404040404040404040404040505050505),
    .INIT_7C(256'h0101010101010101020202020202020202020202020202020202020203030303),
    .INIT_7D(256'h0000000000000000000000000000010101010101010101010101010101010101),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized11
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hC3F00FFFFF803E0F1E318CCCCD9249694AD556AD552A52DA49B2666739C78F07),
    .INITP_01(256'h78781FE000007FC1F0E1CE3319B364925AD6AD5555556AD696DB6CD999CE71C3),
    .INITP_02(256'h3C3E01FFFFFFF80F87871CE7333264924B4A54AAB54AAA56B4B49364CCCCE71C),
    .INITP_03(256'hFC003FF0007E07870E318CCCCD936D25A56AD5555555AB52D2DB64D9999CE71C),
    .INITP_04(256'h007E0F0F1C6398CCCD9B6496D294AD55AAAAD54A94A4B6DB66CCCCE638E387C1),
    .INITP_05(256'hCC9936DB69296A56AAD55556AA54A52D24B24D9933198C71C78783F007FFFFFF),
    .INITP_06(256'h5AB52D696DB6DB266CCCCE631CE3C78781F80FFFE00FFFE01F83C3C71C7398CC),
    .INITP_07(256'h0000000003FF01F83C1E3C71C739CC666664C9B24924B4B4A56AD54AAAAAAA95),
    .INITP_08(256'h6D2D695AB552AAAAAAA556AD4A5A5A49249B264CCCCC6739C71C78F0783F01FF),
    .INITP_09(256'h2DB6D9326666339C71C78783F00FFFE00FFFE03F03C3C78E718CE6666CC9B6DB),
    .INITP_0A(256'h71E1E0FC01FFFFFFC01F83C3C71C63319933649A49694A54AAD55556AAD4AD29),
    .INITP_0B(256'h001FF8007F07C38E38CE6666CDB6DA4A52A556AAAB556A5296D24DB36666338C),
    .INITP_0C(256'hFFFF00F87871CE7333364DB69695AB55555556AD4B496D9366666318E1C3C0FC),
    .INITP_0D(256'h000FF03C3C71CE66664D925A5AD4AAA55AAA54A5A4924C9999CE71C3C3E03FFF),
    .INITP_0E(256'hFFFFE01F87871CE733366DB6D2D6AD5555556AD6B4924D9B3198E70E1F07FC00),
    .INITP_0F(256'hF00000001FC1E3C739CCCC9B24B694A9556AD556A52D249366666318F1E0F803),
    .INIT_00(256'hA83CD4700CAC4CEC9438E48C3CEC9C5004BC7430F0B07038FCC4905C2CFCD0A4),
    .INIT_01(256'h3888E03490EC48A8086CD43CA41080F064D84CC840C03CC040C850D864F08014),
    .INIT_02(256'h2838485C748CA8C4E404244C709CC8F4245488BCF4306CA8E82C70B8004C98E4),
    .INIT_03(256'h784414E4B89068401CF8D8BCA0846C58443424180C00FCF4F4F4F4F8FC040C18),
    .INIT_04(256'h28B03CCC5CF08418B44CEC882CCC741CC4701CCC8034E8A05C18D898581CE4AC),
    .INIT_05(256'h3078C00C5CACFC50A4FC58B41474D438A00874E050C034A8209C1494149418A0),
    .INIT_06(256'h90949CA4B0BCCCDCF0081C38547090B4D8FC24507CA8D80C4078B0EC2868A8EC),
    .INIT_07(256'h4408CC945C24F0C090643810E8C4A08060442C10FCE8D4C4B8ACA09894909090),
    .INIT_08(256'h48C84CD054DC68F48010A438D06804A03CE08028D07824D08030E49C500CC884),
    .INIT_09(256'h98D4145498E02870BC0C5CAC0058B00C68C42888F054BC28980478E860D850CC),
    .INIT_0A(256'h2824242424282C34404C58687C90A4BCD8F41434587CA0CCF4245084B4EC245C),
    .INIT_0B(256'hFCB4702CECB0743800CC98643408DCB48C64402000E4C8B09884706050443830),
    .INIT_0C(256'h047CF470F070F478FC84109C2CBC4CE47810AC48E8882CD07820CC7828DC8C44),
    .INIT_0D(256'h3C74ACE82464A4E82C70BC0454A0F4449CF44CA80464C42890F460CC38A81C8C),
    .INIT_0E(256'hA0988C8884808080848C909CA8B4C4D8EC001834506C8CB0D4F8204C78A8D808),
    .INIT_0F(256'h24D8904804C07C4000C48C5420ECBC8C60340CE4C09C7C5C40240CF4E0CCBCAC),
    .INIT_10(256'hC034AC249C189818981CA42CB440D060F0841CB44CE88828CC7014BC6814C474),
    .INIT_11(256'h6CA0D40C4880C0004084C8105CA8F44494E84098F04CA8086CD0349C0470E050),
    .INIT_12(256'h1C1004FCF8F4F0F0F0F4F8000C14243444586C84A0B8D8F8183C6088B4DC0C3C),
    .INIT_13(256'hC87C30E8A4601CDC9C6028F0B8845020F4C89C744C2808E8C8AC9078644C3C2C),
    .INIT_14(256'h64D84CC440BC3CBC3CC448D05CE87404982CC058F49030D07014BC640CB86414),
    .INIT_15(256'hE0185088C4004080C40C509CE83484D4287CD42C88E440A4046CD038A41080F0),
    .INIT_16(256'h383028242020202428303840505C6C8094A8C0DCF81838587CA0C8F4204C7CB0),
    .INIT_17(256'h6018D0904C0CD0945C24ECB8885828FCD4AC8460401C00E4C8B0988470605044),
    .INIT_18(256'h44BC38B838BC40C850DC68F48418AC40D87410AC4CEC9038DC8834E09040F4A8),
    .INIT_19(256'hD8145498D82068B0FC4898E83890E43C98F450B01478DC44B01C88F868DC50C8),
    .INIT_1A(256'h10101418202830404C5C6C8098B0C8E40020406488B0D804305C8CC0F42C609C),
    .INIT_1B(256'hE0A46C34FCC89864380CE0B8906C482808E8CCB49C84706050403428201C1414),
    .INIT_1C(256'h34C048D464F08418AC44DC7814B454F89C40E89440EC9C5000B86C28E0A05C1C),
    .INIT_1D(256'h0450A0F04094EC449CF858B41878E044B01884F464D448C038B02CA828A82CB0),
    .INIT_1E(256'h38485C7088A4BCDCF8183C6084ACD804306090C4F83068A4E01C5CA0E02870B8),
    .INIT_1F(256'hC098744C2808E8CCB0947C68544030201408FCF4F0ECE8E8E8ECF0F8000C1828),
    .INIT_20(256'h9430D07014B86008B05C0CB86C20D48844FCB87838F8BC804810DCA8784818EC),
    .INIT_21(256'h98FC60C834A00C78EC5CD048C038B430B030B438BC44D05CE878089C30C45CF8),
    .INIT_22(256'hC0EC184878ACE0144C84C0FC3C7CC0044890D82470C01064B80C64C01878D434),
    .INIT_23(256'hF8ECE0D8D0C8C4C4C4C4C8CCD0D8E4F0FC0C1C304458708CA4C4E00424487098),
    .INIT_24(256'h30ECA86828E8B0743C04D09C6C3C0CE0B48C64401CFCDCBCA0846C54402C1808),
    .INIT_25(256'h50D45CE46CF88414A434C860F89028C86404A448EC943CE89440F0A05408C078),
    .INIT_26(256'h4898E8388CE0348CE844A00060C4288CF45CC834A01084F86CE45CD450D04CD0),
    .INIT_27(256'h041C3450708CACD0F418406894C0F01C5084B8EC24609CD8185898DC2068B4FC),
    .INIT_28(256'h6C50341C04ECD8C4B4A4948880746C686460606064686C74808894A4B4C4D8EC),
    .INIT_29(256'h701CCC8034E8A05810CC884808CC90541CE4B07C4818E8BC90643C18F0CCAC8C),
    .INIT_2A(256'hF470EC68E86CEC70F880089420B040D064F88C24C058F89434D87820C46C18C0),
    .INIT_2B(256'hE83078C41060B00054A8FC54AC0864C42484E44CB01880EC58C834A8188C047C),
    .INIT_2C(256'h3549617995ADCDE9092D50749CC4EC184474A4D4083C74ACE4205C98D81C5CA4),
    .INIT_2D(256'hC1A5897159412D1909F9E9DDD1C9C1B9B5B1ADADB1B1B5BDC1CDD5E1F1FD0D21),
    .INIT_2E(256'h7929DD954D05BD7939F5B5793D01C58D5925F1BD8D5D3105DDB18D654121FDE1),
    .INIT_2F(256'h41C145CD51D965F17D0D9D2DC155E98119B551ED8D2DD17519C16911BD6915C5),
    .INIT_30(256'h0155A90159B10965C52181E549AD1179E54DB929950979ED61D951C945C141BD),
    .INIT_31(256'hA9CDF119416D99C5F1215585B9F1296199D5155191D11559A1E53179C51161B1),
    .INIT_32(256'h150D05FDF9F5F1F1F1F1F5F90109111D2935455565798DA1B9D1ED0925456585),
    .INIT_33(256'h35FDC995613101D5A579512901D9B59171513115F9DDC5AD95816D5D493D2D21),
    .INIT_34(256'hE98925C56509AD51F9A149F5A14DFDAD5D11C57D31EDA5611DDD995D1DE1A56D),
    .INIT_35(256'h1D910175ED61D955CD49C949C949CD51D55DE571F98915A535C559ED851DB551),
    .INIT_36(256'hB5F9418DD52171BD0D61B1055DB10965BD1979D53599F95DC12991F965D13DAD),
    .INIT_37(256'h91ADCDED0D3151799DC5ED15416D9DCDFD2D6195C9013971ADE92965A5E9296D),
    .INIT_38(256'h9D91857D756D6965615D5D5D6161696D757D85919DA9B9C9D9ED01152D455D75),
    .INIT_39(256'hBD895521F1C195693D11E9C19975512D0DEDCDB191795D452D1501EDDDC9B9AD),
    .INIT_3A(256'hD1751DC16915BD6915C57525D5893DF1A96119D5914D0DC98D4D11D5996129F1),
    .INIT_3B(256'hC141BD41C145C94DD159E16DF98511A131C155E57D11A941D97511AD4DED8D2D),
    .INIT_3C(256'h71C9217DD93595F555B5197DE149B11985ED59C935A51989FD71E55DD54DC945),
    .INIT_3D(256'hC1F5296199D1094581BDF93979BDFD4185CD155DA5F13D89D52575C9196DC519),
    .INIT_3E(256'h99A9B9CDE1F5092139516D85A1C1DDFD1D416589ADD1F9214D79A5D1FD2D5D91),
    .INIT_3F(256'hD9C5B5A59585796D61554D453D3935312D2D2D2D2D3135394149515965717D89),
    .INIT_40(256'h09D1996129F5C1915D2DFDD1A5794D21F9D1AD8965411DFDDDC1A1856D513921),
    .INIT_41(256'h9535D97D21C97119C56D19C97525D5893DF1A55D15CD8541FDBD7939F9BD8145),
    .INIT_42(256'hC945C141BD41C145C94DD55DE571FD8919A535C959ED8519B149E17D19B555F5),
    .INIT_43(256'hBD1569C11D75D12D8DED4DAD1175D941A9117DE555C131A11185F96DE159D14D),
    .INIT_44(256'h95C1F1215589BDF1296199D5114D8DC90D4D91D51961A9F13D89D52575C51569),
    .INIT_45(256'h696D757D85919DADB9C9DDED01152D455D7991B1CDED0D2D517599C1E9113D69),
    .INIT_46(256'h51310DEDCDAD91755D452D1501EDD9C9B9A99D91857D756D6961615D5D5D6165),
    .INIT_47(256'h7121D58D41F9B56D29E9A56529E9AD713901C995612DFDCD9D6D4115EDC59D79),
    .INIT_48(256'hD961ED7501911DAD3DD165F99129C15DF99935D57919BD6509B15D05B1610DBD),
    .INIT_49(256'hAD0965C52589E951B51D85ED59C535A51589F971E55DD551CD49C949C949CD55),
    .INIT_4A(256'h01316195C9FD356DA5E11D5D99DD1D61A5ED317DC5115DADFD4DA1F549A1F951),
    .INIT_4B(256'hF1F5F9FD050D15212D3D495D6D8195ADC5DDF91531517191B5D901295179A5D5),
    .INIT_4C(256'h996D4119F1CDA98565452509EDD1B9A18D7965554535291D110901F9F5F1F1F1),
    .INIT_4D(256'h09B15901A95501B16111C57931E5A15915D1915115D5996129F1B9855521F1C5),
    .INIT_4E(256'h65D951CD45C141BD41C145C951D961ED79099529B94DE57911AD49E58121C565),
    .INIT_4F(256'hBD054D95DD2979C51569BD1169C11975D12D8DED51B51981E955C12D9D0D7DF1),
    .INIT_50(256'h2D41597189A5C1E1FD2141658DB1DD05315D8DBDF125598DC5013D79B5F53979),
    .INIT_51(256'hCDAD9579614935210DFDF1E1D5CDC1BDB5B1B1ADADB1B5B9C1C9D1DDE9F90919),
    .INIT_52(256'hB06010C47830E8A45C1CD8985C20E4AC743C08D4A4754519EDC59D75512D09E9),
    .INIT_53(256'hEC6CE868EC70F47C048C18A834C858EC8018B04CE48424C46408AC54FCA85400),
    .INIT_54(256'hA0E83480CC1C70C0186CC42078D83494F858C0248CF864D040B020940880F870),
    .INIT_55(256'hD8EC041C34506C8CACCCF0183C6490BCE818487CB0E41C5490CC084888CC1058),
    .INIT_56(256'hAC8C7050341C04ECD8C4B4A4948880746C686460606064686C74808894A4B4C4),
    .INIT_57(256'h34E08C38E89848FCB46820DC985818D89C6024ECB884501CF0C094684018F4D0),
    .INIT_58(256'h84F86CE45CD450D04CD050D45CE46CF88410A034C85CF48C28C46000A044E88C),
    .INIT_59(256'hB0E82868A8EC3078C00854A0F04094E83C94EC48A40464C82890F860C834A414),
    .INIT_5A(256'hC4C8D0D8E0ECF808182C40546C84A0BCDCFC1C40648CB4E00C3C6C9CD0043C74),
    .INIT_5B(256'hE0AC784818ECC09870482404E0C4A48C705844301C0CFCF0E4D8D0CCC8C4C4C4),
    .INIT_5C(256'h0CA034C860FC9834D47818C0640CB86410C07024D8904804C07C3CFCC0844C14),
    .INIT_5D(256'h60B81470D03094F85CC4309C0878E85CD044BC38B430B030B438C048D05CEC78),
    .INIT_5E(256'hE808284C7498C0EC184878A8DC104880BCF83878B8FC4488D4206CB80C5CB008),
    .INIT_5F(256'hBCA488705C483828180C00F8F0ECE8E8E8ECF0F4FC081420304054687C94B0CC),
    .INIT_60(256'hEC9440F0A05004B87028E0A05C1CE0A46830F8C490603004D8AC84603C18F8DC),
    .INIT_61(256'h84F064D448C034B02CA828A82CB038C048D464F48418B044E07818B458F89C44),
    .INIT_62(256'h98C8FC346CA4E01C5CA0E0286CB800509CEC4094E8409CF854B41478DC44AC18),
    .INIT_63(256'h3028201814101014141C20283440506070849CB4CCE80828486C90B8E00C3864),
    .INIT_64(256'h6820D8985414D89C602CF4C08C5C3004D8B08864402000E4C8B098806C5C4C40),
    .INIT_65(256'h40BC38B838BC44C850DC68F8881CB044DC7814B050F4983CE49038E89848FCB0),
    .INIT_66(256'hD00C4C90D01860A8F44090E03488DC3890EC4CAC1074D840AC1884F468DC50C8),
    .INIT_67(256'h20202024283038445060708498B0C8E4001C406084ACD4FC285888B8EC245C94),
    .INIT_68(256'h4000C4885018E0B07C4C20F4C8A07C583818F8DCC0A894806C5C504038302824),
    .INIT_69(256'h3CBC40C44CD864F08010A438D06C04A440E4882CD47C28D48434E89C500CC480),
    .INIT_6A(256'h1C60A4E8307CC81464B80C64BC1470D03090F458C02C980474E85CD048C43CBC),
    .INIT_6B(256'hF0F4F8FC04101C2C3C4C647890ACC8E808284C749CC8F4205084B8F028609CDC),
    .INIT_6C(256'hC080480CD4A06C3C0CDCB488603C18F8D8B8A0846C58443424140C00F8F4F0F0),
    .INIT_6D(256'h98189C24AC34C050E070049C34D06C08A84CF09840E89444F4A85C10C8844000),
    .INIT_6E(256'h7CC0044890D82474C41468BC1470CC2888E84CB41C84F060D040B42CA41C9818),
    .INIT_6F(256'h808084888C98A0ACBCCCE0F40C24405C7C9CC0E40C34608CBCEC20548CC40040),
    .INIT_70(256'hA46424E8AC743C08D8A8784C20F8D4B08C6C50341800ECD8C4B4A89C908C8480),
    .INIT_71(256'hF470F070F47C048C1CA838CC60F49028C46404A84CF49C44F4A05404BC702CE8),
    .INIT_72(256'h74B0EC2C70B4FC448CDC2878CC2078D02C88E848AC1078E44CBC2C9C1084FC78),
    .INIT_73(256'h2C2824242424283038445060708498B0C8E4002040648CB4DC08346498CC0038),
    .INIT_74(256'h28E0985414D4985C24ECB4845024F4CCA07C583414F4D8BCA4907C68584C4034),
    .INIT_75(256'h68DC54D04CC848CC50D860E878049828BC54F08828C4680CB05800AC5C0CBC70),
    .INIT_76(256'hF0245C94CC084484C80C509CE43080D02478D02880E03CA00468D038A41080F4),
    .INIT_77(256'hCCBCB0A49C94909090909498A0ACB8C4D4E8FC102C446080A0C4E810386490C0),
    .INIT_78(256'hFCAC5C0CC07830ECA86828ECB078400CD8A87C5024FCD8B4907054381C08F0DC),
    .INIT_79(256'h84F05CCC3CB028A018941494149C20A834C050E07408A038D47414B458FCA450),
    .INIT_7A(256'h6890B8E4144478ACE41C5898D8185CA0E83480CC1C70C41C74CC2C88EC4CB418),
    .INIT_7B(256'hA88C745C483828180C04FCF8F4F4F4F4FC000C18243444586C84A0BCD8F81C40),
    .INIT_7C(256'h48EC9034E08838E4984C00B8702CE8A86C30F4BC885424F4C89C704C2404E4C4),
    .INIT_7D(256'h4CAC0C70D43CA81480F064D850C840C03CC040C84CD864F08010A43CD46C08A8),
    .INIT_7E(256'hB0CCEC0C30547CA4D0FC2C5C90C4FC3870B0F03074BC04509CEC3C8CE43894EC),
    .INIT_7F(256'h7C54300CECCCB0988068544434241C1008040000000408101C24344454688098),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized12
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h00000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000),
    .INITP_02(256'h00000000000000000007FFFFFFFFFFFFFFFFFFFFFFF800000000000000000000),
    .INITP_03(256'h000000007FFFFFFFFFFFFFFFE000000000000000001FFFFFFFFFFFFFFFFFFF00),
    .INITP_04(256'hC0000000000001FFFFFFFFFFFFF800000000000003FFFFFFFFFFFFFFC0000000),
    .INITP_05(256'h00003FFFFFFFFFFE000000000003FFFFFFFFFFFC000000000001FFFFFFFFFFFF),
    .INITP_06(256'h00000FFFFFFFFFF80000000003FFFFFFFFFE00000000003FFFFFFFFFF8000000),
    .INITP_07(256'hFF8000000003FFFFFFFFE0000000007FFFFFFFFE0000000007FFFFFFFFF00000),
    .INITP_08(256'hFFFFE0000000001FFFFFFFFFC000000000FFFFFFFFFC000000000FFFFFFFFF80),
    .INITP_09(256'hFFFFF800000000003FFFFFFFFFF80000000000FFFFFFFFFF80000000003FFFFF),
    .INITP_0A(256'h0000000007FFFFFFFFFFFF0000000000007FFFFFFFFFFF800000000000FFFFFF),
    .INITP_0B(256'hFC0000000000000007FFFFFFFFFFFFFF800000000000003FFFFFFFFFFFFF0000),
    .INITP_0C(256'h000000000001FFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFF),
    .INITP_0D(256'hFF80000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFC000000000),
    .INITP_0E(256'h000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0505040404040403030303030302020202020202010101010101010101000000),
    .INIT_01(256'h0E0D0D0D0C0C0C0B0B0B0A0A0A0A090909080808080707070706060606050505),
    .INIT_02(256'h1B1A1A1919181817171716161515141414131312121211111010100F0F0F0E0E),
    .INIT_03(256'h2C2B2B2A2929282827262625252424232322222121201F1F1E1E1D1D1C1C1C1B),
    .INIT_04(256'h4140403F3E3D3D3C3B3B3A3939383737363535343333323131302F2F2E2E2D2C),
    .INIT_05(256'h5B5A5958575655555453525151504F4E4D4D4C4B4A4949484746464544434342),
    .INIT_06(256'h787776757473727170706F6E6D6C6B6A69686766656463636261605F5E5D5C5B),
    .INIT_07(256'h9A9998979695939291908F8E8D8C8B8A8988878684838281807F7E7D7C7B7A79),
    .INIT_08(256'hC0BFBEBCBBBAB9B7B6B5B4B3B1B0AFAEADABAAA9A8A7A6A4A3A2A1A09F9E9C9B),
    .INIT_09(256'hEAE9E8E6E5E3E2E1DFDEDDDBDAD9D7D6D5D3D2D1CFCECDCCCAC9C8C6C5C4C3C1),
    .INIT_0A(256'h191716141311100E0D0B0A080705040201FFFEFDFBFAF8F7F5F4F3F1F0EEEDEC),
    .INIT_0B(256'h4B494846444341403E3C3B393836343331302E2D2B292826252322201F1D1C1A),
    .INIT_0C(256'h82807E7C7A7977757372706E6D6B6967666462615F5D5C5A5857555352504E4D),
    .INIT_0D(256'hBCBAB8B6B5B3B1AFADABA9A8A6A4A2A09E9C9B9997959392908E8C8A89878583),
    .INIT_0E(256'hFBF9F7F5F3F1EFEDEBE9E7E5E3E1DFDDDBD9D7D5D3D1CFCDCBC9C8C6C4C2C0BE),
    .INIT_0F(256'h3E3B39373533312F2D2A28262422201E1C1A181513110F0D0B0907050301FFFD),
    .INIT_10(256'h8482807E7B79777572706E6C69676563605E5C5A585553514F4D4B4846444240),
    .INIT_11(256'hCFCDCAC8C6C3C1BFBCBAB7B5B3B0AEACA9A7A5A2A09E9B99979492908E8B8987),
    .INIT_12(256'h1E1C191614110F0C0A07050300FEFBF9F6F4F1EFECEAE7E5E3E0DEDBD9D6D4D2),
    .INIT_13(256'h716E6C696664615E5C595754514F4C4A4744423F3D3A383532302D2B28262321),
    .INIT_14(256'hC8C5C2BFBDBAB7B4B2AFACA9A7A4A19F9C999694918E8C898684817E7C797674),
    .INIT_15(256'h22201D1A1714110E0B09060300FDFAF7F5F2EFECE9E6E4E1DEDBD8D6D3D0CDCA),
    .INIT_16(256'h817E7B7875726F6C696663605D5A5754514E4B484543403D3A3734312E2B2825),
    .INIT_17(256'hE4E1DDDAD7D4D1CECBC8C4C1BEBBB8B5B2AFACA9A6A3A09C999693908D8A8784),
    .INIT_18(256'h4A4744403D3A3733302D2A2623201D1A1613100D0A060300FDFAF7F3F0EDEAE7),
    .INIT_19(256'hB4B1AEAAA7A4A09D9996938F8C8985827F7B7875726E6B6864615E5A5754514D),
    .INIT_1A(256'h231F1C1815110E0A070300FCF9F5F2EEEBE8E4E1DDDAD6D3D0CCC9C5C2BFBBB8),
    .INIT_1B(256'h94918D8A86827F7B7874706D6966625F5B5754504D4946423F3B3834312D2A26),
    .INIT_1C(256'h0A0603FFFBF7F4F0ECE9E5E1DEDAD6D2CFCBC7C4C0BCB9B5B2AEAAA7A39F9C98),
    .INIT_1D(256'h84807C7874706C6965615D5956524E4A46433F3B3733302C2824211D1915120E),
    .INIT_1E(256'h01FDF9F5F1EDE9E5E1DDD9D5D1CDC9C6C2BEBAB6B2AEAAA6A29F9B97938F8B87),
    .INIT_1F(256'h817D7975716D6965615D5955514D4945413D3834302C2824201C1814110D0905),
    .INIT_20(256'h0602FDF9F5F1EDE9E4E0DCD8D4D0CBC7C3BFBBB7B3AEAAA6A29E9A96928E8A85),
    .INIT_21(256'h8E8985817D7874706B67635F5A56524E4945413D3834302C27231F1B17120E0A),
    .INIT_22(256'h1915110C0803FFFBF6F2EDE9E5E0DCD8D3CFCAC6C2BDB9B5B0ACA8A39F9B9692),
    .INIT_23(256'hA8A49F9B96928D8984807B77726E6965605C58534F4A46413D3834302B27221E),
    .INIT_24(256'h3B36322D29241F1B16120D0804FFFBF6F1EDE8E4DFDAD6D1CDC8C4BFBBB6B2AD),
    .INIT_25(256'hD1CCC8C3BEB9B5B0ABA7A29D98948F8A86817C78736E6A65605C57524E494440),
    .INIT_26(256'h6B66615C57524E49443F3A36312C27221D19140F0A0601FCF7F2EEE9E4DFDBD6),
    .INIT_27(256'h0803FEF9F4EFEAE5E0DBD6D1CCC7C2BEB9B4AFAAA5A09B96928D88837E79746F),
    .INIT_28(256'hA8A39E99948E89847F7A75706B66615C57524D48433E39342F2A25201B16110C),
    .INIT_29(256'h4B46413C37312C27221D18130E0803FEF9F4EFEAE5E0DAD5D0CBC6C1BCB7B2AD),
    .INIT_2A(256'hF2EDE7E2DDD8D2CDC8C3BEB8B3AEA9A39E99948F89847F7A756F6A65605B5650),
    .INIT_2B(256'h9C97918C87817C77716C66615C57514C47413C37312C27211C17120C0702FDF7),
    .INIT_2C(256'h49443E39332E28231E18130D0802FDF8F2EDE7E2DDD7D2CCC7C2BCB7B1ACA7A1),
    .INIT_2D(256'hF9F4EEE9E3DED8D3CDC7C2BCB7B1ACA6A19B96908B85807A756F6A645F59544F),
    .INIT_2E(256'hADA7A19C96918B85807A746F69645E58534D47423C37312C26201B15100A04FF),
    .INIT_2F(256'h635D58524C46413B35302A241E19130D0802FCF6F1EBE5E0DAD4CFC9C3BEB8B2),
    .INIT_30(256'h1D17110B05FFFAF4EEE8E2DCD7D1CBC5BFBAB4AEA8A39D97918B86807A746F69),
    .INIT_31(256'hD9D3CDC7C1BBB5AFA9A49E98928C86807A746F69635D57514B45403A342E2822),
    .INIT_32(256'h98928C86807A746E68625C56504A443E38322C26201A140E0802FCF7F1EBE5DF),
    .INIT_33(256'h5A544E48423C362F29231D17110B05FFF9F3EDE7E0DAD4CEC8C2BCB6B0AAA49E),
    .INIT_34(256'h1F19130C0600FAF4EDE7E1DBD5CFC8C2BCB6B0AAA49D97918B857F79736C6660),
    .INIT_35(256'hE7E0DAD4CDC7C1BBB4AEA8A29B958F89827C767069635D57514A443E38322B25),
    .INIT_36(256'hB1AAA49E97918B847E78716B655E58524B453F38322C251F19130C0600F9F3ED),
    .INIT_37(256'h7E77716A645E57514A443D37312A241D17110A04FDF7F1EAE4DDD7D1CAC4BEB7),
    .INIT_38(256'h4D47403A332D262019130C06FFF9F2ECE5DFD8D2CBC5BEB8B1ABA59E98918B84),
    .INIT_39(256'h1F19120C05FEF8F1EBE4DDD7D0CAC3BDB6AFA9A29C958F88827B756E67615A54),
    .INIT_3A(256'hF4EDE7E0D9D3CCC5BFB8B1ABA49D979089837C756F68625B544E47403A332D26),
    .INIT_3B(256'hCBC4BDB7B0A9A29C958E87817A736D665F58524B443E373029231C150F0801FB),
    .INIT_3C(256'hA49D979089827B746E676059524C453E37302A231C150F0801FAF3EDE6DFD8D2),
    .INIT_3D(256'h8079726B645D575049423B342D261F19120B04FDF6EFE9E2DBD4CDC6C0B9B2AB),
    .INIT_3E(256'h5E575049423B342D261F18110A03FCF5EFE8E1DAD3CCC5BEB7B0A9A29B958E87),
    .INIT_3F(256'h3E373029221B140D06FFF8F1EAE3DCD5CEC7C0B9B2ABA49D968F88817A736C65),
    .INIT_40(256'h575D646B727980878E959BA2A9B0B7BEC5CCD3DAE1E8EFF5FC030A11181F262D),
    .INIT_41(256'h7B828990979DA4ABB2B9C0C6CDD4DBE2E9EFF6FD040B12191F262D343B424950),
    .INIT_42(256'hA2A9B0B7BDC4CBD2D8DFE6EDF3FA01080F151C232A30373E454C525960676E74),
    .INIT_43(256'hCCD3D9E0E7EDF4FB01080F151C232930373E444B52585F666D737A81878E959C),
    .INIT_44(256'hF8FE050C12191F262D333A40474E545B62686F757C838990979DA4ABB1B8BFC5),
    .INIT_45(256'h262D333A40474D545A61676E757B82888F959CA2A9AFB6BDC3CAD0D7DDE4EBF1),
    .INIT_46(256'h575E646A71777E848B91989EA5ABB1B8BEC5CBD2D8DFE5ECF2F9FF060C131920),
    .INIT_47(256'h8B91979EA4AAB1B7BEC4CAD1D7DDE4EAF1F7FD040A11171D242A31373D444A51),
    .INIT_48(256'hC1C7CDD4DAE0E7EDF3F900060C13191F252C32383F454B52585E656B71787E84),
    .INIT_49(256'hFA00060C13191F252B32383E444A51575D636970767C82898F959BA2A8AEB4BB),
    .INIT_4A(256'h363C42484E545A60666C73797F858B91979DA4AAB0B6BCC2C8CFD5DBE1E7EDF4),
    .INIT_4B(256'h747A80868C92989EA4AAB0B6BCC2C8CED4DAE0E7EDF3F9FF050B11171D23292F),
    .INIT_4C(256'hB5BBC1C7CDD3D9DFE5EBF1F7FC02080E141A20262C32383E444A50565C62686E),
    .INIT_4D(256'hFAFF050B11171D22282E343A40454B51575D63696F747A80868C92989EA4A9AF),
    .INIT_4E(256'h41464C52585D63696F747A80868B91979DA3A8AEB4BABFC5CBD1D7DCE2E8EEF4),
    .INIT_4F(256'h8B91969CA1A7ADB2B8BEC3C9CFD4DAE0E5EBF1F6FC02080D13191E242A30353B),
    .INIT_50(256'hD8DEE3E9EEF4F9FF040A10151B20262C31373C42474D53585E64696F747A8085),
    .INIT_51(256'h282E33393E44494F54595F646A6F757A80858B90969BA1A6ACB1B7BCC2C7CDD3),
    .INIT_52(256'h7C81878C91979CA1A7ACB1B7BCC2C7CCD2D7DDE2E7EDF2F8FD02080D13181E23),
    .INIT_53(256'hD2D8DDE2E7EDF2F7FD02070C12171C21272C31373C41474C51575C61666C7177),
    .INIT_54(256'h2C31373C41464B50565B60656A6F757A7F84898F94999EA3A9AEB3B8BEC3C8CD),
    .INIT_55(256'h898E94999EA3A8ADB2B7BCC1C6CBD0D5DAE0E5EAEFF4F9FE03080E13181D2227),
    .INIT_56(256'hEAEFF4F9FE03080C11161B20252A2F34393E43484D52575C61666B70757A7F84),
    .INIT_57(256'h4E52575C61666B6F74797E83888D92969BA0A5AAAFB4B9BEC2C7CCD1D6DBE0E5),
    .INIT_58(256'hB5B9BEC3C8CCD1D6DBDFE4E9EEF2F7FC01060A0F14191D22272C31363A3F4449),
    .INIT_59(256'h1F24292D32363B4044494E52575C60656A6E73787C81868A8F94989DA2A7ABB0),
    .INIT_5A(256'h8D92969B9FA4A8ADB2B6BBBFC4C8CDD1D6DADFE4E8EDF1F6FBFF04080D12161B),
    .INIT_5B(256'hFF03080C1115191E22272B3034383D41464A4F53585C6065696E72777B808489),
    .INIT_5C(256'h74787D8185898E92969B9FA3A8ACB0B5B9BDC2C6CACFD3D8DCE0E5E9EDF2F6FB),
    .INIT_5D(256'hEDF1F5F9FD02060A0E12171B1F23272C3034383D4145494E52565A5F63676B70),
    .INIT_5E(256'h696D7175797D81858A8E92969A9EA2A6AAAEB3B7BBBFC3C7CBD0D4D8DCE0E4E9),
    .INIT_5F(256'hE9EDF1F5F9FD0105090D1114181C2024282C3034383D4145494D5155595D6165),
    .INIT_60(256'h6C7074787C8084878B8F93979B9FA2A6AAAEB2B6BABEC2C6C9CDD1D5D9DDE1E5),
    .INIT_61(256'hF4F7FBFF03060A0E1215191D2124282C3033373B3F43464A4E5256595D616569),
    .INIT_62(256'h7F82868A8D9194989C9FA3A7AAAEB2B5B9BCC0C4C7CBCFD2D6DADEE1E5E9ECF0),
    .INIT_63(256'h0E1115181C1F23262A2D3134383B3F4246494D5054575B5F6266696D7074787B),
    .INIT_64(256'hA0A4A7AAAEB1B4B8BBBFC2C5C9CCD0D3D6DADDE1E4E8EBEEF2F5F9FC0003070A),
    .INIT_65(256'h373A3D4044474A4D5154575A5E6164686B6E7275787B7F8285898C8F9396999D),
    .INIT_66(256'hD1D4D7DADDE1E4E7EAEDF0F3F7FAFD0003060A0D1013161A1D2023262A2D3033),
    .INIT_67(256'h6F7275787B7E8184878A8D909396999CA0A3A6A9ACAFB2B5B8BBBEC1C4C8CBCE),
    .INIT_68(256'h1114171A1D202225282B2E3134373A3D404345484B4E5154575A5D606366696C),
    .INIT_69(256'hB7BABDBFC2C5C8CACDD0D3D6D8DBDEE1E4E6E9ECEFF2F5F7FAFD000306090B0E),
    .INIT_6A(256'h616466696C6E717476797C7E818486898C8E919496999C9FA1A4A7A9ACAFB2B4),
    .INIT_6B(256'h0F111416191C1E212326282B2D303235383A3D3F4244474A4C4F515457595C5E),
    .INIT_6C(256'hC1C3C6C8CACDCFD2D4D6D9DBDEE0E3E5E7EAECEFF1F4F6F9FBFE000305070A0C),
    .INIT_6D(256'h77797B7E80828487898B8E90929497999B9EA0A2A5A7A9ACAEB0B3B5B7BABCBF),
    .INIT_6E(256'h31333537393B3E40424446484B4D4F515355585A5C5E60636567696C6E707275),
    .INIT_6F(256'hEFF1F3F5F7F9FBFDFF01030507090B0D0F111315181A1C1E20222426282A2D2F),
    .INIT_70(256'hB1B3B5B6B8BABCBEC0C2C4C6C8C9CBCDCFD1D3D5D7D9DBDDDFE1E3E5E7E9EBED),
    .INIT_71(256'h77797A7C7E8082838587898A8C8E9092939597999B9C9EA0A2A4A6A8A9ABADAF),
    .INIT_72(256'h4143444648494B4D4E5052535557585A5C5D5F6162646667696B6D6E70727375),
    .INIT_73(256'h101113141617191A1C1D1F202223252628292B2D2E303133343638393B3C3E40),
    .INIT_74(256'hE2E3E5E6E8E9EAECEDEEF0F1F3F4F5F7F8FAFBFDFEFF0102040507080A0B0D0E),
    .INIT_75(256'hB9BABBBCBEBFC0C1C3C4C5C6C8C9CACCCDCECFD1D2D3D5D6D7D9DADBDDDEDFE1),
    .INIT_76(256'h9395969798999A9B9C9E9FA0A1A2A3A4A6A7A8A9AAABADAEAFB0B1B3B4B5B6B7),
    .INIT_77(256'h72737475767778797A7B7C7D7E7F8081828384868788898A8B8C8D8E8F909192),
    .INIT_78(256'h55565758595A5B5B5C5D5E5F60616263636465666768696A6B6C6D6E6F707071),
    .INIT_79(256'h3D3D3E3F40404142434344454646474849494A4B4C4D4D4E4F50515152535455),
    .INIT_7A(256'h2829292A2B2B2C2C2D2E2E2F2F3031313233333435353637373839393A3B3B3C),
    .INIT_7B(256'h181819191A1A1B1B1C1C1C1D1D1E1E1F1F202121222223232424252526262728),
    .INIT_7C(256'h0C0C0C0D0D0D0E0E0E0F0F0F1010101111121212131314141415151616171717),
    .INIT_7D(256'h040404040405050505050606060607070707080808080909090A0A0A0A0B0B0B),
    .INIT_7E(256'h0000000000000000000001010101010101010102020202020202030303030303),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized13
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0066AACE01C956C7FC6D5B381E4AA47039A5A63F8CAA99FF1B559820CD56C7F8),
    .INITP_01(256'h9820ED25B800C956CFFE6556C7F8DAAD8FF195533FF1B56CE0792AB380335D66),
    .INITP_02(256'h38073514CFFE24ADB800CD55987864AB67C79955B1FE32D69C0064AA67039955),
    .INITP_03(256'h92AB33FF196B4CF9F325698FF8C95247CF9B553380E25548E079B54987C32553),
    .INITP_04(256'h92A923C0F36AA99C01CDAAB31FF8DA5A4E00392AA4E1C19A56D87F8C9569C7E1),
    .INITP_05(256'h10392552CC3F872575261FF1DB55267C1E66AA931FF8CD2B48E00736AA4C7FF1),
    .INITP_06(256'h95524E3FFC765AA5B1C003992AADB1F07CCD2A9271FF0CDAAA4CF00F32555B38),
    .INITP_07(256'h9338FFF0E6DA92B6C707E0E64B55A4CE020399A554B33C00799A554931F8FC64),
    .INITP_08(256'hDC7FF8E495524C7E3F192554B33C00799A554B338080E64B55A4CE0FC1C6DA92),
    .INITP_09(256'hF8669549381039B55499E01E64AAB661FF1C92A9667C1F1B6AA93380071B4AB4),
    .INITP_0A(256'hAD9E07892A931FFC64AAD9C00E25A9663FF192AACCF07CC955B71FF0C95D49C3),
    .INITP_0B(256'hAD31FF99AA930FC72D5263FC36D4B3070E4AA93800E4B4B63FF19AAB6700732A),
    .INITP_0C(256'hFFE65159C039954987C3255B3C0E25548E039955B3E7C495263FE32D499F3E65),
    .INITP_0D(256'h003B496E0833553381CCAA4C0072D698FF1B5533C7CDAA4C3C335566003B6A48),
    .INITP_0E(256'h2700E6AACC00CD7598039AA93C0E6D5B1FF995531FE36AB63FC6D54CFFE6D526),
    .INITP_0F(256'hDAA4C0064AB63FC6D566083355B1FF32AA63F8CB4B381C4AA4F039B56C7FC6D5),
    .INIT_00(256'h208000A040E0A08060606060A0C02060E060E08020E0A080808080C0E02080E0),
    .INIT_01(256'h0060C020C04000A0806040404060A0E02080008020C060400000E000004080C0),
    .INIT_02(256'hE02060C040C040E0A06040202020206080E040A020A040E0A0806040406080C0),
    .INIT_03(256'hC0E02060C020A020C0804000E0C0C0E0002060C020A020C06000E0A0A0A0A0C0),
    .INIT_04(256'hA0A0C0004080E060E08020C0A06060406060A0E02080E06000A04000E0C0C0C0),
    .INIT_05(256'h4040406080C00060E060E08020E0C0A0A0A0A0C00040A0008000A04000E0A0A0),
    .INIT_06(256'hA080808080A0E02080E060E08040E0C0A08080A0C0E02080E040C06000C08060),
    .INIT_07(256'h80604020204060A0E02080008020C0804020000000206080E040A020C06000C0),
    .INIT_08(256'h00C0806040406080A0E040A0008020C080402000E000004080C02080008020E0),
    .INIT_09(256'hA06020E0C0A0A0A0C0004080E060E06000C080604020404080A0E040C020C040),
    .INIT_0A(256'h8020C08040202020204060A00060C040E08020E0C0A0A0A0C0E00060A0208020),
    .INIT_0B(256'h40E06020E0A080606080A0C00060C020A040E0A06020000000204080C0208000),
    .INIT_0C(256'hC040E06020E0A0808080A0C0E02080E060E06020C080604040406080C00060C0),
    .INIT_0D(256'hC040C040E0A06040202020406080E020A000A020E0A060402020204080A00060),
    .INIT_0E(256'h0060E06000C0604020000000206080E040A020C06000C0A080606080A0C00060),
    .INIT_0F(256'h41A121A121C181412101E101012161A10161E16101A1612101E1E1E1012160A0),
    .INIT_10(256'h2181018101A16121E1C1C1C1C1E12161A10181018121E1A1614141416181A1E1),
    .INIT_11(256'hA1E161E16101A1614101010101214181E141A121A14101C1816141416181A1E1),
    .INIT_12(256'h2161E161E18121E1C1A1818181A1C10161A121A121C1612101C1C1C1C1E10141),
    .INIT_13(256'h61C121A141E181412101E1E1E1014181C1218101A141E1A161414141416181C1),
    .INIT_14(256'h2181018121C1612101E1E1E1E1214181E141A121C16101C1A181818181A1E121),
    .INIT_15(256'h0262E28202C2824222020202226181E141A121A141E1A16141210121216181C1),
    .INIT_16(256'h82028222C2824222020202224282C20282E26202A24202E2C2C2C2C2E20242A2),
    .INIT_17(256'h8202A24202C28282626282A2E22262C242C242E2A262220202E202224282C222),
    .INIT_18(256'h42E2824222E2C2C2C2E2024282E242A222C26202C2A28262626282C2E242A202),
    .INIT_19(256'hA26202E2C2A2A2C2C2024282E242C242C28222E2C2A28282A2C2E22282E242C2),
    .INIT_1A(256'h03E3A3A38383A3C30343830262E26202C2824222020202224282C22282E26202),
    .INIT_1B(256'h0303E3E3032363A3E343C323C36303C383634343434363A3E32383038303A343),
    .INIT_1C(256'h43436383A30343A3238323C36323E3C3A3A3A3C3E30343A30363E38303C36343),
    .INIT_1D(256'h034383C32383038303A36323E3C3C3A3C3C3032383C323A323A34303C3836343),
    .INIT_1E(256'h0463C343C343E3834323E3E3C3C3E3032363C32383038323C383432303E3E303),
    .INIT_1F(256'hC444C46404C4A4644444446484A4E444A4048404A444E4A484644444446484C4),
    .INIT_20(256'h8424E4C4A4848484A4C4044484E464C46404A44424E4C4C4C4C4E4044484E444),
    .INIT_21(256'hC4C4A4A4C4E40444A40464C464E4844404C4A4848484A4C4E42484E444C444E4),
    .INIT_22(256'h254585C50565C444C46404A4642404E4E4E404244484C42484048424C4642404),
    .INIT_23(256'h0565C545C56505C5854525252525456585C52585E565E58525C5856525250525),
    .INIT_24(256'hA54505C585654545454585A5E52585E565E56505A5652505E5E5E5E5052565A5),
    .INIT_25(256'h8585658585A5E52565C525A525A54505C58545452525254565A5E545A5058505),
    .INIT_26(256'h0646A6E646C646C66606A6664626060606264666A6E646A5058525C56525E5A5),
    .INIT_27(256'hA626C66626E6A68686666686A6C60646A60686E68626C6662606E6C6C6C6C6E6),
    .INIT_28(256'h866666666686A6E62666C626A626A64606A6664626060606264666A60646A626),
    .INIT_29(256'h2767C70787E76707A74707C787674746466686A6E62686E646C646E68626E6C6),
    .INIT_2A(256'hC76727E7A787674747676787C70747A70787078727C7672707C7C7A7A7C7C707),
    .INIT_2B(256'hC7E7E7274787C72787078707A747E7A7674727272727276787C70767C747A747),
    .INIT_2C(256'h88E86808A84808C8A8686848486887A7E72767C727A727A747E7A7672707E7C7),
    .INIT_2D(256'h0808E80808284888C80868C848C848E8884808C8A88868688888A8E80868A808),
    .INIT_2E(256'hE848A828C848E8A86828E8C8C8A8A8C8E8082868C82888E868E88828E8886828),
    .INIT_2F(256'h290909E90909296989C92989E969E96909A94808C8A88868686888A8C8084888),
    .INIT_30(256'h2989098909A94909C989492929090929496989C92969C949A949C96909C98949),
    .INIT_31(256'h29292929496989C90969C9298909A929C9894909C9A9A9898989A9C9092989C9),
    .INIT_32(256'h4AEA6A0AAA6A2AEACAAA8A8A8A8AAACA0A4A8AEA4AAA2AAA2ACA6909C9896949),
    .INIT_33(256'hEA0A2A6AAAEA4AAA0A8A0AAA2ACA8A4A0ACAAA8A8A8A8AAACAEA2A6AAA0A6AEA),
    .INIT_34(256'h2BEBAB8B4B4B2A2A2A4A6A8ACA0A4AAA0A6AEA6AEA8A2AEA8A6A2A0AEAEACAEA),
    .INIT_35(256'h2B8BEB6BEB8B0BAB6B0BCBAB6B4B4B4B4B4B6B8BABEB2B8BCB4BAB2BAB2BCB6B),
    .INIT_36(256'h2B4B4B6BABCB0B6BAB0B8BEB6B0B8B2BEB8B4B2BEBCBCBABABCBCBEB0B4B8BCB),
    .INIT_37(256'h6C2CECAC8C6C6C6C6C8C8CCCEC2C6CAC0C6CEC6CEB6B0BAB4B0BCB8B6B4B4B2B),
    .INIT_38(256'hEC4CCC6CEC8C2CECAC6C4C2C0CECECEC0C0C4C6CACEC2C8CEC4CCC4CCC6C0CAC),
    .INIT_39(256'hCD0D4D8DED4CAC0C8C0CAC4CEC8C4C0CCCAC8C6C6C6C6C8C8CCCEC2C6CAC0C6C),
    .INIT_3A(256'h6D6D6D6D8DADED0D4DADED4DAD2DAD2DAD4DED8D4D0DCDAD8D6D6D4D6D6D8DAD),
    .INIT_3B(256'hAD6D4D2D0D0DED0D0D2D4D6D8DCD0D6DCD2D8D0D8D0DAD2DED8D4D0DCDAD8D6D),
    .INIT_3C(256'hCE6E2ECE8E4E0EEECEAEAEAEAEAECEEE0E4E8ECE2E6ECE4EAE2DCD4DED8D2DED),
    .INIT_3D(256'hEE6EEE8E0EAE4E0ECE8E4E2E0EEECECECEEEEE0E2E6EAEEE2E8EEE4EAE2EAE4E),
    .INIT_3E(256'h2F8FEF4FCF4FCF6F0FAF4F0FCF8F6E2E0E0EEEEEEE0E2E4E6E8ECE0E6ECE2E8E),
    .INIT_3F(256'h6FCF0F6FCF2FAF2FAF2FCF6F0FAF6F2FEFCFAF8F6F6F6F6F6F8FAFCF0F4F8FCF),
    .INIT_40(256'hCEEE0E2E4E8ECE0E4EAE0E8EEE6EEE8E2ECE6E0ECE8E6E4E2E0FEFEFEF0F0F2F),
    .INIT_41(256'hCEAEAEAEAEAECEEE0E4E8ECE2E6ECE4EAE2EAE4EEE8E2EEEAE6E2E0EEEEECECE),
    .INIT_42(256'h0DCD8D6D4D2D0D0DED0D0D2D4D6DAEEE2E8EEE4ECE2EAE4ECE6E2ECE8E4E0EEE),
    .INIT_43(256'hAD2DAD4DEDAD4D0DEDAD8D6D6D6D6D6D8DADCD0D4D8DED2DAD0D8D0D8D2DCD6D),
    .INIT_44(256'h4C8CED4DAD0D8D0DAD4DED8D4D0DCDAD8D6D6D4D6D6D8DADCD0D4D8DED4DAD2D),
    .INIT_45(256'hECEC0C2C4C6CACEC2C8CEC6CCC4CEC6C0CAC6C2CECCC8C8C6C6C6C6C8CACCC0C),
    .INIT_46(256'h6C2CECCC8C8C6C6C6C6C8CACEC2C6CAC0C6CCC4CCC4CEC8C2CECAC6C4C0C0CEC),
    .INIT_47(256'h8B0B6BEB8B0BAB6B0BCBAB6B4B4B2B2B4B4B6B8CCC0C4CAC0C6CEC6CEC6C0CAC),
    .INIT_48(256'h4B4B6BABCB0B6BAB0B8BEB6BEB8B2BCB8B4B0BEBCBCBABABCBCBEB2B4B8BEB2B),
    .INIT_49(256'h4A0BCB8B6B4B2B2B2B4B4B8BABEB2B6BCB2BAB2BAB4BCB8B2BEBAB8B6B4B4B4B),
    .INIT_4A(256'h8ACA2AAA0A8A0AAA4AEAAA6A2A0AEAEACAEAEA0A2A6A8AEA2A8AEA6AEA6A0AAA),
    .INIT_4B(256'hAA8A8A8A8AAACAEA2A6AAA0A6AEA4AEA6A0AAA6A2AEACAAA8A8A8A8AAACA0A4A),
    .INIT_4C(256'hA9098929C96909C989694929292A2A4A6A8ACA0A6ACA2AAA2AAA4AEA8A4A0ACA),
    .INIT_4D(256'h090929294989C90949A90989098929C9892909C9A9898989A9A9C9094989C929),
    .INIT_4E(256'hE969E98929C98969290909E90909294989C90969C949A949C96929C989694929),
    .INIT_4F(256'hA8A8C8C8E82868A8E848C828A848E8884808C8A88869696989A9C90949A90969),
    .INIT_50(256'h48C848C86808C88848280808E80808286888E82888E868E88828C8682808E8C8),
    .INIT_51(256'h886848486868A8C80848A80868E88808A86808E8A88888686888A8C8084888E8),
    .INIT_52(256'hE747A70787078727C7874727E7E7C7C7E7072767A7E747A727A828C86828E8A8),
    .INIT_53(256'h4707C787676747476787A7E72767C747A747C76707C7876727272727274767A7),
    .INIT_54(256'h476787C70747A70767E78707C7672707C7C7A7A7C7C7072767C72787078707A7),
    .INIT_55(256'hA626A626C66626E6A6866666666686C6E62686E646C646E68727E7A787674747),
    .INIT_56(256'h06C6A68666668686A6E62666C626A626A64606A6664626060606264666A60646),
    .INIT_57(256'h06264666A60666C646C646E6A64606E6C6C6C6C6E6062666C62686E68606A646),
    .INIT_58(256'h45A525A525C56525E5A58585658585A5E62666C6268606A646E6A66646260606),
    .INIT_59(256'h8525E5A585454545456585C50545A5058505A545E5A56545252525454585C505),
    .INIT_5A(256'h45252525254585C50565C545C56505A5652505E5E5E5E5052565A50565E565E5),
    .INIT_5B(256'h042565A50565C545C56505C5854525250525256585C52585E565E58525C58565),
    .INIT_5C(256'h84E464C46404A44404E4C4A4A4C4C4042464C42484048424C484442404E4E4E4),
    .INIT_5D(256'h64E4844404C4A4848484A4C4E42484E444C444E48424E4C4A4848484A4C40444),
    .INIT_5E(256'hE4A4846444444464A4C40464C444C444E4844404E4C4C4C4C4E42444A40464C4),
    .INIT_5F(256'hE3C3C3E3E3234484E444C444C46404C484644444446484A4E444A4048404A444),
    .INIT_60(256'hC3C3E32363A30383038323C383430303E3E303234383C32383038323C3632303),
    .INIT_61(256'hE32363C3238323A34303A383634343436383C30343A323A323C3832303C3C3A3),
    .INIT_62(256'h0363C323C343E3A3632303E3E303034363C30383E36303A34303E3C3A3A3A3C3),
    .INIT_63(256'h63E36303834303C3A38383A3A3E30343A30383038323E3A363434343436383C3),
    .INIT_64(256'hC242E2824202C2C2A2A2C2E20262A20262E28222C2824222020202224383C303),
    .INIT_65(256'h42E2824202E2C2C2C2E2224282E242C242E28222E2C2A28282A2C2E22282C242),
    .INIT_66(256'h6222E2A28262628282C20242A2028202A242E2C28262626282A2C20262C222A2),
    .INIT_67(256'hC2824222020202224282C22282028222C282422202E202022262A2E242C242C2),
    .INIT_68(256'h826222020202224282C20282E26202A24202E2C2C2C2C2E20242A20262E28202),
    .INIT_69(256'h4121E1E1E1E1012161C12181018121C18161212101214161A1E142A222A242E2),
    .INIT_6A(256'h4101E1E1E101214181E141A121C16121E1A181818181A1C10161C121A141E181),
    .INIT_6B(256'hC1A1818181A1C1E12181E161E16121C181614141414161A1E141A1018121C181),
    .INIT_6C(256'h4121010101014161A10161E161E1A14101E1C1C1C1C1012161C121A121A16101),
    .INIT_6D(256'h21E1C1C1C1C1E12161A10181018121E1A1816141416181C10141A121A141E181),
    .INIT_6E(256'h61210101E101214181C121A121A141E1A1816141414161A1E12181018101A161),
    .INIT_6F(256'h806020000000204060C10161E16101A1612101E1E1E1012161A10161E16101A1),
    .INIT_70(256'hE08060402020204060A0E040C040C06000C0A080606080A0C00060C020A040E0),
    .INIT_71(256'h8020E0C0A0808080A0E02060E040C06000A080402020204060A0E020A000A020),
    .INIT_72(256'hC06000C0A080606080A0E02060E040C06000C080604040406080C02060E060E0),
    .INIT_73(256'hC06000A06040202020204080C02080008020C08040200000002060A0E040A020),
    .INIT_74(256'hE060E0804000C0A0A0A0C0E02060A0208020A06000E0C0A0A0A0C0E02080E040),
    .INIT_75(256'h208000A040E0A0806040406080C00040C020C040E0A080404020406080C00060),
    .INIT_76(256'hC02080008020E0A060402020406080E02080008020C080400000E000204080C0),
    .INIT_77(256'hE04080E060E08020E0A080808080A0C00060C020A040E0806020000000204080),
    .INIT_78(256'hC0E02080E060E06000C080604040406080C00060C040E08020E0C0A08080A0C0),
    .INIT_79(256'h6060A0C02080E060E0804000C0A0A0A0A0E00040A0008000A04000C0A0A0A0A0),
    .INIT_7A(256'hC0C0E0004080C020A020C06020E0C0C0C0C0E00040A00060E08020E0A0606040),
    .INIT_7B(256'h202020204060A0E040C040C06020E0C0A0A0A0A0E00060C020A020C0602000E0),
    .INIT_7C(256'hA0604040406080A00040C020C06000C0806040406080A0E040A020A040E08060),
    .INIT_7D(256'h20C0A06060606080A0E040A0008020C080400000E000004060C02080008020E0),
    .INIT_7E(256'hA04000C08060606080A0E02080E060E08020E0C080808080A0E02080E060E060),
    .INIT_7F(256'h60E08020E0A08060606080C00040A020A020E0804020000000204080E020A020),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized14
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h00000007FFFFFFFE0000000001FFFFFFFFFFFFC0000000000000000000000000),
    .INITP_01(256'h0FFFE0003FFFC0001FFFF00003FFFF00000FFFFF000007FFFFF0000007FFFFFF),
    .INITP_02(256'hFC00FFE007FF001FFC007FF001FFE001FFE001FFF0007FFC001FFF8001FFF800),
    .INITP_03(256'h07F807F807F807F803FC03FE01FF007F803FE00FF801FF007FE00FFC00FFC00F),
    .INITP_04(256'h03F81FC0FE07F01F80FE03F80FC07F01FC03F80FE03FC07F00FE01FC03FC07F8),
    .INITP_05(256'hF81F03F07E07C0FC1F81F81F03F03F03F03F03F03F03F03F81F81FC0FC0FE07F),
    .INITP_06(256'h0F83E0F83E0F83E07C1F07C1F03E0F83F07C1F83E07C1F83E07C0F81F07E0FC0),
    .INITP_07(256'h0F87C1E0F0783E1F0783C1F0783E1F0783E0F07C1F0F83E0F07C1F07C1F0F83E),
    .INITP_08(256'hE0F83E0F83E0F83E1F07C1F07C1E0F83E1F07C1E0F83C1F0F83C1F0783C1F0F8),
    .INITP_09(256'hC0FC1F81F03E07E0FC1F03E07C0F83F07C0F83F07C1F83E0F81F07C1F07C0F83),
    .INITP_0A(256'hC0FE07F03F81FC0FE07E07F03F03F81F81F81F81F81F81F81F81F03F03F07E07),
    .INITP_0B(256'h3FC03FC03FC03FC07F807F00FE01FC07F80FE03F807F01FC07E03F80FE03F01F),
    .INITP_0C(256'hFFC00FFE007FE007FE007FE00FFC01FF003FE00FF803FC01FF00FF807F803FC0),
    .INITP_0D(256'hFFF8000FFFE0003FFF0003FFF0007FFC001FFF000FFF000FFF001FFC007FF001),
    .INITP_0E(256'hFFFFC0000001FFFFFFC000001FFFFFC00001FFFFE00001FFFF80001FFFF00007),
    .INITP_0F(256'h00000000000000000000000000000000000007FFFFFFFFFFFF0000000000FFFF),
    .INIT_00(256'h312F2E2C2A2827252422211F1E1C1B1A18171615141211100F0E0D0C0B0B0A09),
    .INIT_01(256'h7C797673706E6B686663615E5C59575452504E4B49474543413F3C3B39373533),
    .INIT_02(256'hE7E4E0DCD8D4D1CDC9C6C2BFBBB8B4B1ADAAA7A3A09D9A9693908D8A8784817E),
    .INIT_03(256'h75706B66615D58534E4A45413C37332E2A26211D1914100C0804FFFBF7F3EFEB),
    .INIT_04(256'h241E18120C0600FBF5EFEAE4DED9D3CEC8C3BDB8B3ADA8A39E98938E89847F7A),
    .INIT_05(256'hF4EDE6DFD8D1CBC4BDB6AFA9A29B958E88817B746E68615B554F48423C36302A),
    .INIT_06(256'hE6DED6CEC6BEB6AEA69E978F8780787069615A524B433C352D261F18110902FB),
    .INIT_07(256'hF9F0E7DED5CCC3BAB1A89F978E857C746B635A5249413830271F170F06FEF6EE),
    .INIT_08(256'h2D23190F05FBF1E7DDD3C9BFB6ACA2988F857C72685F564C433930271E140B02),
    .INIT_09(256'h82776C61564B40352A1F1409FEF4E9DED4C9BEB4A99F948A7F756A60564C4137),
    .INIT_0A(256'hF9EDE0D4C8BCB0A4988C8074695D5145392E22160BFFF4E8DDD1C6BBAFA4998E),
    .INIT_0B(256'h908376695B4E4134271A0D00F4E7DACDC0B4A79A8E8175685C4F43362A1E1105),
    .INIT_0C(256'h483A2C1E1001F3E5D7C9BBAD9F928476685A4D3F31241609FBEEE0D3C5B8AB9D),
    .INIT_0D(256'h211203F4E4D5C6B7A8998A7B6C5D4E4031221305F6E7D9CABCAD9F9082736557),
    .INIT_0E(256'h1B0BFAEADACABAAA9A8A7A6A5A4A3A2A1A0AFBEBDBCCBCAC9D8D7E6E5F4F4031),
    .INIT_0F(256'h35241301F0DFCEBDAC9B897968574635241302F2E1D0C0AF9F8E7D6D5D4C3C2B),
    .INIT_10(256'h705D4B39271402F0DECCBAA8968472604E3D2B1907F6E4D2C1AF9E8C7B695846),
    .INIT_11(256'hCAB7A4907D6A5744311E0BF8E5D2BFAC998673614E3B291603F1DECCB9A79482),
    .INIT_12(256'h45311C08F4E0CCB7A38F7B67533F2B1804F0DCC8B5A18D7A66523F2B1804F1DE),
    .INIT_13(256'hE0CAB5A08B75604B36210CF7E2CDB8A38E7A65503B2712FDE9D4C0AB97826E59),
    .INIT_14(256'h9A846E57412B15FFE9D2BCA6907B654F39230DF8E2CCB7A18B76604B35200AF5),
    .INIT_15(256'h745D452E1700E9D2BBA48D765F48311A03ECD6BFA8917B644E37210AF4DDC7B0),
    .INIT_16(256'h6D553D250CF4DCC4AC947C644C341C05EDD5BDA68E765F472F1800E9D1BAA38B),
    .INIT_17(256'h866D533A2108EFD6BDA48B725940270EF6DDC4AB937A6249311800E7CFB69E86),
    .INIT_18(256'hBDA3896F553A2006ECD2B99F856B51371E04EAD1B79D846A51371E04EBD2B89F),
    .INIT_19(256'h13F8DDC2A78C71563B2005EACFB59A7F644A2F14FADFC5AA90755B41260CF2D7),
    .INIT_1A(256'h886C503418FCE0C4A88C7055391D01E6CAAE93775C402509EED2B79C80654A2F),
    .INIT_1B(256'h1BFEE1C4A78A6D503317FADDC0A4876A4E3115F8DCBFA3866A4E3115F9DDC0A4),
    .INIT_1C(256'hCCAE9072543719FBDDBFA28466492B0DF0D2B5977A5D3F2205E7CAAD90725538),
    .INIT_1D(256'h9B7C5D3E2001E2C3A58667492A0BEDCEB09173553618FADBBD9F8163442608EA),
    .INIT_1E(256'h8868482808E9C9A98A6A4A2B0BECCCAD8E6E4F3010F1D2B39374553617F8D9BA),
    .INIT_1F(256'h9171502F0FEECDAD8C6C4B2B0AEAC9A98969482808E8C7A78767472707E7C7A7),
    .INIT_20(256'hB89775533210EFCDAC8A69482605E4C2A1805F3E1DFBDAB99877563615F4D3B2),
    .INIT_21(256'hFCD9B794724F2D0BE8C6A4815F3D1BF9D7B492704E2C0AE8C6A583613F1DFCDA),
    .INIT_22(256'h5C3915F2CFAB8865411EFBD8B5926F4B2805E3C09D7A573411EFCCA98664411F),
    .INIT_23(256'hD9B4906C4723FFDBB7936F4B2703DFBB97734F2B07E4C09C7955310EEAC7A380),
    .INIT_24(256'h714C2701DCB7926D4823FED9B4906B4621FCD8B38E6A4521FCD7B38E6A4621FD),
    .INIT_25(256'h25FFD9B38D67411BF5CFAA845E3813EDC7A27C57310CE6C19B76502B06E1BB96),
    .INIT_26(256'hF4CDA67F59320BE4BE97704A23FDD6B089633C16EFC9A37C56300AE3BD97714B),
    .INIT_27(256'hDEB78F674018F0C9A17A522B03DCB58D663F17F0C9A27A532C05DEB79069421B),
    .INIT_28(256'hE3BB926A4119F0C8A0774F27FED6AE865E350DE5BD956D451DF5CDA57E562E06),
    .INIT_29(256'h03D9B0875D340BE2B88F663D14EBC29970471EF5CCA37A512900D7AE865D340C),
    .INIT_2A(256'h3C12E8BD93693F15EBC1976D431AF0C69C72491FF5CBA2784F25FBD2A87F552C),
    .INIT_2B(256'h8F64390EE3B88D63380DE2B88D62370DE2B88D63380EE3B98E643A0FE5BB9066),
    .INIT_2C(256'hFBCFA4784C21F5C99E72471BF0C4996D4217EBC095693E13E8BC91663B10E5BA),
    .INIT_2D(256'h815427FBCFA276491DF1C4986C3F13E7BB8F63360ADEB2865A2E02D6AB7F5327),
    .INIT_2E(256'h1EF1C497693C0FE2B5885A2D00D3A6794C20F3C6996C3F12E6B98C603306DAAD),
    .INIT_2F(256'hD5A7794A1DEFC193653709DBAD805224F7C99B6E4012E5B78A5C2F01D4A7794C),
    .INIT_30(256'hA3744516E8B98A5C2DFED0A1734416E7B98A5C2DFFD1A2744618E9BB8D5F3103),
    .INIT_31(256'h885929FACA9B6B3C0DDDAE7F4F20F1C292633405D6A778491AEBBC8D5E2F00D1),
    .INIT_32(256'h855424F4C494643303D3A3734313E3B3845424F4C494653505D5A6764617E7B8),
    .INIT_33(256'h98673605D4A3734211E0B07F4E1DEDBC8C5B2AFAC999683807D7A7764616E5B5),
    .INIT_34(256'hC2905E2DFBCA98673504D2A16F3E0DDBAA794716E5B4835120EFBE8D5C2BFAC9),
    .INIT_35(256'h01CF9C6A3806D4A16F3D0BD9A7754311DFAD7B4917E5B3814F1EECBA885725F3),
    .INIT_36(256'h5623F0BD8A5725F2BF8C5926F4C18E5C29F6C4915E2CF9C794622FFDCB986633),
    .INIT_37(256'hC08D5925F2BE8B5724F0BD895623EFBC895522EFBB885522EFBC885522EFBC89),
    .INIT_38(256'h3F0BD6A26E3A06D19D693501CD996531FDC995612DF9C6925E2AF6C38F5B28F4),
    .INIT_39(256'hD29D6833FECA95602BF7C28D5824EFBB86511DE8B47F4B16E2AD794510DCA873),
    .INIT_3A(256'h79430ED8A36D3803CD98622DF8C38D5823EEB8834E19E4AF7A4510DBA6713C07),
    .INIT_3B(256'h33FDC7915B25EEB9834D17E1AB753F09D39E6832FCC7915B25F0BA854F19E4AE),
    .INIT_3C(256'h00C9935C25EFB8814B14DEA7713A04CD97612AF4BE87511BE4AE78420BD59F69),
    .INIT_3D(256'hDFA8713A03CB945D26EFB8814A12DBA46D36FFC9925B24EDB67F4812DBA46D37),
    .INIT_3E(256'hD199612AF2BA824B13DBA46C34FDC58E561FE7AF784109D29A632BF4BD854E17),
    .INIT_3F(256'hD49C642BF3BB824A12DAA16931F9C1895018E0A8703800C8905820E8B1794109),
    .INIT_40(256'hDB124A81B8EF265D94CB033A71A8DF174E85BDF42B639AD2094178AFE71F568E),
    .INIT_41(256'h043A71A7DE144B81B8EF255C93C900376DA4DB12487FB6ED245B92C9FF366DA4),
    .INIT_42(256'h3F75ABE1174D83B9EE255B91C7FD33699FD50B4278AEE41B5187BEF42A6197CD),
    .INIT_43(256'h8DC3F82D6298CD03386DA3D80E4379AEE4194F85BAF0255B91C7FC32689ED309),
    .INIT_44(256'hEF24588DC2F72B6095CAFE33689DD2073C71A6DB10457AAFE4194E83B8EE2358),
    .INIT_45(256'h6599CD0135699DD1063A6EA2D60B3F73A8DC104579ADE2164B7FB4E81D5186BB),
    .INIT_46(256'hEF235689BDF024578BBEF225598DC0F4285B8FC3F62A5E92C6F92D6195C9FD31),
    .INIT_47(256'h8EC1F426598CBFF225578ABDF0235689BCEF225588BCEF225588BBEF225589BC),
    .INIT_48(256'h4375A7D90B3D6FA1D406386A9CCF01336698CBFD2F6294C7F92C5E91C4F6295C),
    .INIT_49(256'h0D3E6FA1D204356798CAFB2D5E90C2F3255788BAEC1E4F81B3E517497BADDF11),
    .INIT_4A(256'hED1D4E7FB0E0114273A3D405366798C9FA2B5C8DBEEF205183B4E5164779AADB),
    .INIT_4B(256'hE3134373A3D303336494C4F4245485B5E5164676A7D707386899C9FA2A5B8CBC),
    .INIT_4C(256'hF1204F7FAEDD0D3C6B9BCAFA295988B8E7174676A6D505356594C4F4245484B3),
    .INIT_4D(256'h164473A1D0FE2D5C8AB9E8164574A3D1002F5E8DBCEB1A4978A7D605346392C2),
    .INIT_4E(256'h5280ADDB09376593C1EF1D4A79A7D503315F8DBBE9184674A2D1FF2D5C8AB9E7),
    .INIT_4F(256'hA6D3002D5A88B5E20F3C6997C4F11E4C79A7D4012F5C8AB7E512406E9BC9F724),
    .INIT_50(256'h133F6C98C4F11D4976A2CFFB275481ADDA0633608CB9E6123F6C99C6F3204C79),
    .INIT_51(256'h99C4F01B47729EC9F5214C78A4CFFB27537FABD6022E5A86B2DE0A36638FBBE7),
    .INIT_52(256'h37628DB8E20D38638DB8E30E39648FBAE5103B6691BCE8133E6995C0EB17426D),
    .INIT_53(256'hF01A436D97C1EB153F6993BDE8123C6690BBE50F3A648EB9E30E38638DB8E20D),
    .INIT_54(256'hC2EB143D668FB8E20B345D87B0D9032C557FA8D2FB254F78A2CBF51F49729CC6),
    .INIT_55(256'hAED6FE274F77A0C8F019416A92BBE30C345D86AED70029517AA3CCF51E477099),
    .INIT_56(256'hB5DC032B527AA1C9F01840678FB7DE062E567EA5CDF51D456D95BDE50D355E86),
    .INIT_57(256'hD6FD234A7097BEE40B32597FA6CDF41B426990B7DE052C537AA2C9F0173F668D),
    .INIT_58(256'h13385E84AACFF51B41678DB3D9FF254B7197BDE30A30567CA3C9EF163C6389B0),
    .INIT_59(256'h6B90B4D9FE23486D92B7DC01274C7196BBE1062B50769BC1E60C31577CA2C7ED),
    .INIT_5A(256'hDF03274B6F93B7DBFF23476C90B4D9FD21466A8EB3D7FC21456A8EB3D8FC2146),
    .INIT_5B(256'h6F92B5D8FB1E416588ABCFF215395C80A3C7EA0E3155799CC0E4072B4F7397BB),
    .INIT_5C(256'h1B3D5F81A4C6E80B2D4F7294B7D9FC1F416486A9CCEF1134577A9DC0E305284B),
    .INIT_5D(256'hE4052648698AACCDEF1032537597B8DAFC1D3F6183A5C6E80A2C4E7092B4D7F9),
    .INIT_5E(256'hC9EA0A2B4B6C8CADCDEE0F2F507191B2D3F41536567798B9DAFB1D3E5F80A1C2),
    .INIT_5F(256'hCCEC0B2B4A6A8AA9C9E90828486888A7C7E70727476787A7C7E80828486989A9),
    .INIT_60(256'hED0B2A496786A5C3E201203E5D7C9BBAD9F81736557493B3D2F110304F6E8EAD),
    .INIT_61(256'h2B496684A2BFDDFB1937547290AECCEA08264463819FBDDBFA1836557391B0CE),
    .INIT_62(256'h87A4C0DDFA1733506D8AA7C4E1FE1B38557290ADCAE705223F5D7A97B5D2F00D),
    .INIT_63(256'h011D3955708CA8C4E0FC1834506C88A4C0DDF915314E6A86A3BFDCF815314E6A),
    .INIT_64(256'h9AB5CFEA05203B56718CA7C2DDF8132F4A65809CB7D2EE0925405C7793AECAE6),
    .INIT_65(256'h516B859FB9D2EC06203A556F89A3BDD7F20C26415B7590AAC5DFFA142F4A647F),
    .INIT_66(256'h274059728BA4BDD6EF08213A536D869FB8D2EB041E37516A849DB7D1EA041E37),
    .INIT_67(256'h1C344C647C94ACC4DCF40C253D556D869EB6CFE700183149627A93ABC4DDF60E),
    .INIT_68(256'h31485F768DA4BBD2E900172E455D748BA3BAD1E900182F475F768EA6BDD5ED05),
    .INIT_69(256'h657B90A6BCD2E9FF152B41576E849AB0C7DDF40A21374E647B91A8BFD6EC031A),
    .INIT_6A(256'hB8CDE2F70C21364B60758BA0B5CAE0F50A20354B60768BA1B7CCE2F80D23394F),
    .INIT_6B(256'h2B3F53677B8FA3B7CCE0F4081C3145596E8297ABC0D4E9FD12273B50657A8EA3),
    .INIT_6C(256'hBFD2E5F80B1E3144576A7D90A4B7CADEF104182B3F52667A8DA1B5C8DCF00418),
    .INIT_6D(256'h728496A8BACCDEF0021427394B5D708294A7B9CCDEF10316293B4E61738699AC),
    .INIT_6E(256'h46576879899BACBDCEDFF0011324354658697B8C9EAFC1D2E4F607192B3D4E60),
    .INIT_6F(256'h3A4A5A6A7A8A9AAABACADAEAFA0B1B2B3C4C5D6D7D8E9FAFC0D0E1F202132435),
    .INIT_70(256'h4E5D6C7B8A99A8B7C6D5E4F403122131404F5F6E7E8D9DACBCCCDBEBFB0A1A2A),
    .INIT_71(256'h84929FADBBC9D7E5F301101E2C3A4857657382909FADBCCAD9E7F60513223140),
    .INIT_72(256'hDAE7F4000D1A2734414E5B697683909DABB8C5D3E0EEFB091624313F4D5A6876),
    .INIT_73(256'h515D6974808C98A4B0BCC8D4E0EDF905111E2A36434F5C6875818E9AA7B4C0CD),
    .INIT_74(256'hE9F4FE09141F2A35404B56616C77828E99A4AFBBC6D1DDE8F4FF0B16222E3945),
    .INIT_75(256'hA2ACB6BFC9D3DDE7F1FB050F19232D37414C56606A757F8A949FA9B4BEC9D4DE),
    .INIT_76(256'h7C858E979FA8B1BAC3CCD5DEE7F0F9020B141E273039434C565F68727C858F98),
    .INIT_77(256'h7880878F979EA6AEB6BEC6CED6DEE6EEF6FE060F171F2730384149525A636B74),
    .INIT_78(256'h959BA2A9AFB6BDC4CBD1D8DFE6EDF4FB020911181F262D353C434B525A616970),
    .INIT_79(256'hD3D9DEE4EAEFF5FB00060C12181E242A30363C42484F555B61686E747B81888E),
    .INIT_7A(256'h33373C41454A4E53585D61666B70757A7F84898E93989EA3A8ADB3B8BDC3C8CE),
    .INIT_7B(256'hB4B8BBBFC2C6C9CDD1D4D8DCE0E4E7EBEFF3F7FBFF04080C1014191D21262A2E),
    .INIT_7C(256'h57595C5E616366686B6E707376797C7E8184878A8D9093969A9DA0A3A7AAADB1),
    .INIT_7D(256'h1B1C1E1F2122242527282A2C2E2F31333537393B3C3F41434547494B4E505254),
    .INIT_7E(256'h010102020203030404050506070708090A0B0B0C0D0E0F10111214151617181A),
    .INIT_7F(256'h0807070605050404030302020201010100000000000000000000000000000001),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized15
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h35BC446B48772969111EDE227C3AEFB4877096EBEE1DF5DA451478A2096FBEE0),
    .INITP_01(256'hB78A77777728D69690EEBBEE53E2DF2227DA5BC4447B4B75DF70F146412D2282),
    .INITP_02(256'hA20DE1E5111106909608D775C8B7CB487B4B84B84FB44FB04FB04B84B8787878),
    .INITP_03(256'h213168B444272D116087913C229AEE962379443C2720E1116D60834B44C45A5C),
    .INITP_04(256'h3DE6895A130B45D2F27BC0EE8841DEEF7443D2E9D845DD11634AC22D16886AC2),
    .INITP_05(256'h0932F45D0B3E6B5DB24210B3668E5EF7A710BBF127A610B18C6845D167BDE68B),
    .INITP_06(256'h5F78F7D0999999990B9982E3DE3A17B6DBD18217B6F4217A2F453062484194C9),
    .INITP_07(256'h997D7AFA0A1433A8AE4146FAE617D0CCBD2514385F66FA16D0A285E3D9BD71E8),
    .INITP_08(256'h33333217DE3DF42F1D7B378F428A16D0BECDF43851497A6617D0CEBEC504EA2B),
    .INITP_09(256'hACF9A1745E9921265304248C1945E8BD085EDBD08317B6DBD0B8F78E8333A133),
    .INITP_0A(256'h45A190B522CF79A2CF7BCD17442C631A10CBC91FBA11CBDEF4E2CD9A10849B75),
    .INITP_0B(256'h69C8445A2D190886AC22D16886A58D117744372E97845DEEF70422EE07BC9E97),
    .INITP_0C(256'hC111114F0F608A74B44645A5820D6D110E09C878453D88D2EEB2887913C20D11),
    .INITP_0D(256'hD629DDDDDCA3DA3C3C3C3A43A41BE41BE45BE43A43A5BC25A7DA275DD620D212),
    .INITP_0E(256'h29DC25AC447B5882896904C51E1DF75DA5BC4447B4B7C889F68F94EFBAEE12D2),
    .INITP_0F(256'hDDDC3E44F877760EFBED208A3C5144B75F70EFAED21DC25BEEB87C88F6F1112D),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0101010101010100000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_06(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_07(256'h0202020202020202020202020202020202020202020202020202020202010101),
    .INIT_08(256'h0303030303020202020202020202020202020202020202020202020202020202),
    .INIT_09(256'h0303030303030303030303030303030303030303030303030303030303030303),
    .INIT_0A(256'h0404040404040404040404040404040404040404040303030303030303030303),
    .INIT_0B(256'h0505050505050505050505050404040404040404040404040404040404040404),
    .INIT_0C(256'h0606060606060505050505050505050505050505050505050505050505050505),
    .INIT_0D(256'h0707070606060606060606060606060606060606060606060606060606060606),
    .INIT_0E(256'h0808070707070707070707070707070707070707070707070707070707070707),
    .INIT_0F(256'h0909090908080808080808080808080808080808080808080808080808080808),
    .INIT_10(256'h0A0A0A0A0A0A0A09090909090909090909090909090909090909090909090909),
    .INIT_11(256'h0B0B0B0B0B0B0B0B0B0B0B0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A),
    .INIT_12(256'h0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B),
    .INIT_13(256'h0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0C0C0C0C0C0C0C0C0C),
    .INIT_14(256'h0F0F0F0F0F0F0F0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0D),
    .INIT_15(256'h10101010101010101010101010101010100F0F0F0F0F0F0F0F0F0F0F0F0F0F0F),
    .INIT_16(256'h1212121212111111111111111111111111111111111111111111111010101010),
    .INIT_17(256'h1313131313131313131313131313131312121212121212121212121212121212),
    .INIT_18(256'h1515151515151515141414141414141414141414141414141414141413131313),
    .INIT_19(256'h1716161616161616161616161616161616161616151515151515151515151515),
    .INIT_1A(256'h1818181818181818181818181818181717171717171717171717171717171717),
    .INIT_1B(256'h1A1A1A1A1A1A1A1A1A1A19191919191919191919191919191919191918181818),
    .INIT_1C(256'h1C1C1C1C1C1C1C1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1A1A1A1A1A1A1A),
    .INIT_1D(256'h1E1E1E1E1E1E1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1C1C1C1C1C1C1C1C1C1C),
    .INIT_1E(256'h20202020201F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1E1E1E1E1E1E1E1E1E1E1E),
    .INIT_1F(256'h2222222222212121212121212121212121212121212020202020202020202020),
    .INIT_20(256'h2424242424242323232323232323232323232323232222222222222222222222),
    .INIT_21(256'h2626262626262626252525252525252525252525252525242424242424242424),
    .INIT_22(256'h2828282828282828282827272727272727272727272727272726262626262626),
    .INIT_23(256'h2A2A2A2A2A2A2A2A2A2A2A2A2A2A292929292929292929292929292928282828),
    .INIT_24(256'h2D2D2D2D2C2C2C2C2C2C2C2C2C2C2C2C2C2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A),
    .INIT_25(256'h2F2F2F2F2F2F2F2F2E2E2E2E2E2E2E2E2E2E2E2E2E2E2D2D2D2D2D2D2D2D2D2D),
    .INIT_26(256'h3131313131313131313131313130303030303030303030303030302F2F2F2F2F),
    .INIT_27(256'h3434343434343333333333333333333333333332323232323232323232323232),
    .INIT_28(256'h3636363636363636363636363535353535353535353535353534343434343434),
    .INIT_29(256'h3939393939393938383838383838383838383837373737373737373737373737),
    .INIT_2A(256'h3C3C3B3B3B3B3B3B3B3B3B3B3B3B3A3A3A3A3A3A3A3A3A3A3A3A393939393939),
    .INIT_2B(256'h3E3E3E3E3E3E3E3E3E3E3D3D3D3D3D3D3D3D3D3D3D3D3C3C3C3C3C3C3C3C3C3C),
    .INIT_2C(256'h4141414141414040404040404040404040403F3F3F3F3F3F3F3F3F3F3F3F3E3E),
    .INIT_2D(256'h4444444343434343434343434343434242424242424242424242424141414141),
    .INIT_2E(256'h4746464646464646464646464645454545454545454545454444444444444444),
    .INIT_2F(256'h4949494949494949494949484848484848484848484847474747474747474747),
    .INIT_30(256'h4C4C4C4C4C4C4C4C4C4B4B4B4B4B4B4B4B4B4B4B4A4A4A4A4A4A4A4A4A4A4A4A),
    .INIT_31(256'h4F4F4F4F4F4F4F4F4F4E4E4E4E4E4E4E4E4E4E4E4D4D4D4D4D4D4D4D4D4D4D4C),
    .INIT_32(256'h5252525252525252525151515151515151515150505050505050505050504F4F),
    .INIT_33(256'h5555555555555555555454545454545454545453535353535353535353535252),
    .INIT_34(256'h5858585858585858585857575757575757575757565656565656565656565555),
    .INIT_35(256'h5C5B5B5B5B5B5B5B5B5B5B5A5A5A5A5A5A5A5A5A5A5959595959595959595958),
    .INIT_36(256'h5F5F5E5E5E5E5E5E5E5E5E5E5D5D5D5D5D5D5D5D5D5D5C5C5C5C5C5C5C5C5C5C),
    .INIT_37(256'h6262626261616161616161616161606060606060606060605F5F5F5F5F5F5F5F),
    .INIT_38(256'h6565656565656564646464646464646463636363636363636363626262626262),
    .INIT_39(256'h6868686868686868686767676767676767676766666666666666666666656565),
    .INIT_3A(256'h6C6C6C6B6B6B6B6B6B6B6B6B6A6A6A6A6A6A6A6A6A6A69696969696969696969),
    .INIT_3B(256'h6F6F6F6F6F6F6E6E6E6E6E6E6E6E6E6E6D6D6D6D6D6D6D6D6D6C6C6C6C6C6C6C),
    .INIT_3C(256'h73727272727272727272717171717171717171707070707070707070706F6F6F),
    .INIT_3D(256'h7676767676757575757575757575747474747474747474737373737373737373),
    .INIT_3E(256'h7979797979797979797878787878787878787777777777777777777676767676),
    .INIT_3F(256'h7D7D7D7D7C7C7C7C7C7C7C7C7C7B7B7B7B7B7B7B7B7B7B7A7A7A7A7A7A7A7A7A),
    .INIT_40(256'h7475757575757575757576767676767676767676777777777777777777787878),
    .INIT_41(256'h7171717171727272727272727272737373737373737373737474747474747474),
    .INIT_42(256'h6E6E6E6E6E6E6E6E6E6F6F6F6F6F6F6F6F6F7070707070707070707071717171),
    .INIT_43(256'h6A6A6A6B6B6B6B6B6B6B6B6B6C6C6C6C6C6C6C6C6C6C6D6D6D6D6D6D6D6D6D6E),
    .INIT_44(256'h676767676767686868686868686868696969696969696969696A6A6A6A6A6A6A),
    .INIT_45(256'h6464646464646464656565656565656565656666666666666666666667676767),
    .INIT_46(256'h6061616161616161616161626262626262626262626363636363636363636364),
    .INIT_47(256'h5D5D5D5E5E5E5E5E5E5E5E5E5E5F5F5F5F5F5F5F5F5F5F606060606060606060),
    .INIT_48(256'h5A5A5A5A5B5B5B5B5B5B5B5B5B5B5C5C5C5C5C5C5C5C5C5C5C5D5D5D5D5D5D5D),
    .INIT_49(256'h57575757575858585858585858585858595959595959595959595A5A5A5A5A5A),
    .INIT_4A(256'h5454545454545555555555555555555555565656565656565656565757575757),
    .INIT_4B(256'h5151515151515252525252525252525252535353535353535353535354545454),
    .INIT_4C(256'h4E4E4E4E4E4E4F4F4F4F4F4F4F4F4F4F4F505050505050505050505051515151),
    .INIT_4D(256'h4B4B4B4B4B4B4C4C4C4C4C4C4C4C4C4C4D4D4D4D4D4D4D4D4D4D4D4E4E4E4E4E),
    .INIT_4E(256'h4848484849494949494949494949494A4A4A4A4A4A4A4A4A4A4A4A4B4B4B4B4B),
    .INIT_4F(256'h4545464646464646464646464646474747474747474747474748484848484848),
    .INIT_50(256'h4343434343434343434343434444444444444444444444454545454545454545),
    .INIT_51(256'h4040404040404040404141414141414141414141424242424242424242424242),
    .INIT_52(256'h3D3D3D3D3D3E3E3E3E3E3E3E3E3E3E3E3E3F3F3F3F3F3F3F3F3F3F3F3F404040),
    .INIT_53(256'h3A3B3B3B3B3B3B3B3B3B3B3B3B3C3C3C3C3C3C3C3C3C3C3C3C3D3D3D3D3D3D3D),
    .INIT_54(256'h3838383838383838393939393939393939393939393A3A3A3A3A3A3A3A3A3A3A),
    .INIT_55(256'h3535353636363636363636363636363737373737373737373737373738383838),
    .INIT_56(256'h3333333333333333333434343434343434343434343435353535353535353535),
    .INIT_57(256'h3030313131313131313131313131313232323232323232323232323233333333),
    .INIT_58(256'h2E2E2E2E2E2E2E2F2F2F2F2F2F2F2F2F2F2F2F2F303030303030303030303030),
    .INIT_59(256'h2C2C2C2C2C2C2C2C2C2C2C2D2D2D2D2D2D2D2D2D2D2D2D2D2D2E2E2E2E2E2E2E),
    .INIT_5A(256'h292A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C2C),
    .INIT_5B(256'h2727272727282828282828282828282828282829292929292929292929292929),
    .INIT_5C(256'h2525252525252526262626262626262626262626262627272727272727272727),
    .INIT_5D(256'h2323232323232323232424242424242424242424242424242525252525252525),
    .INIT_5E(256'h2121212121212121212122222222222222222222222222222222232323232323),
    .INIT_5F(256'h1F1F1F1F1F1F1F1F1F1F20202020202020202020202020202020212121212121),
    .INIT_60(256'h1D1D1D1D1D1D1D1D1D1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1F1F1F1F1F1F),
    .INIT_61(256'h1B1B1B1B1B1B1B1B1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D1D1D1D1D),
    .INIT_62(256'h19191919191A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1B1B1B1B1B1B1B1B1B1B),
    .INIT_63(256'h1818181818181818181818181818181818181819191919191919191919191919),
    .INIT_64(256'h1616161616161616161616161616171717171717171717171717171717171717),
    .INIT_65(256'h1414141414141415151515151515151515151515151515151515151616161616),
    .INIT_66(256'h1313131313131313131313131313131313131314141414141414141414141414),
    .INIT_67(256'h1111111111111111111112121212121212121212121212121212121212121213),
    .INIT_68(256'h1010101010101010101010101010101010101010111111111111111111111111),
    .INIT_69(256'h0E0E0E0E0E0E0E0E0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F1010),
    .INIT_6A(256'h0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E),
    .INIT_6B(256'h0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0D0D0D0D0D0D0D0D),
    .INIT_6C(256'h0A0A0A0A0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0C0C),
    .INIT_6D(256'h09090909090909090A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A),
    .INIT_6E(256'h0808080808080808080808090909090909090909090909090909090909090909),
    .INIT_6F(256'h0707070707070707070707070708080808080808080808080808080808080808),
    .INIT_70(256'h0606060606060606060606060707070707070707070707070707070707070707),
    .INIT_71(256'h0505050505050505050606060606060606060606060606060606060606060606),
    .INIT_72(256'h0404040505050505050505050505050505050505050505050505050505050505),
    .INIT_73(256'h0404040404040404040404040404040404040404040404040404040404040404),
    .INIT_74(256'h0303030303030303030303030303030303030303030303030303040404040404),
    .INIT_75(256'h0202020202020202020203030303030303030303030303030303030303030303),
    .INIT_76(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_77(256'h0101010101010101010101010101010101010202020202020202020202020202),
    .INIT_78(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_79(256'h0000000000000000010101010101010101010101010101010101010101010101),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized16
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0E1C3C3878783C3C1E0F83E0F81F03F03F80FE03FE01FF801FFE0007FFFE0000),
    .INITP_01(256'hCC9999933333333319998CCC663319CE6318C6318E718E71C718E38F1C71E387),
    .INITP_02(256'hA5AD29694B4B4B49692D25B6925B6DB6DB6DB6D924DB64DB26C9B364C9933664),
    .INITP_03(256'h555555555555555555AAAAA95556AAB556AA556A956A952A54AD5A94AD6A5294),
    .INITP_04(256'hA52D2D694B5AD6B5AD4A56A56A56AD5AB56A954AA552AA554AAA5555AAAAA955),
    .INITP_05(256'h264C993264D9366D9364DB24DB64926DB6DB6DB6DB6924B6D25B49692D2DA5A5),
    .INITP_06(256'h31CE739CE7398C67319CC6633199CCCE6667333333333333333366664CCD99B3),
    .INITP_07(256'hC3C3C3C3C3C3C387870E1E3C70E1C78E1C71E38E38E38E38E39C71CE39C639C6),
    .INITP_08(256'h6339CE739CE718C738C738E71C738E38E38E38E38F1C70E3C70E1C78F0E1C3C3),
    .INITP_09(256'hD9364C993264C99B336664CCCD9999999999999999CCCCE66733198CC67319CC),
    .INITP_0A(256'hD6B5A52D69694B4B4B69692D25B496DA492DB6DB6DB6DB6C924DB649B64D936C),
    .INITP_0B(256'h55555555555555552AAAAB5554AAA554AA954AA552AD5AB56AD4AD4AD4A56B5A),
    .INITP_0C(256'hA5A5A52D296B4A5294AD6A52B56A54A952AD52AD54AAD55AAAD5552AAAAB5555),
    .INITP_0D(256'h999999933332664CD993264D9B26C9B64DB64936DB6DB6DB6DB492DB49692D25),
    .INITP_0E(256'h783C3C387870E1C38F1C71E38E31C71CE31CE318C6318CE73198CC6663333199),
    .INITP_0F(256'h000000000000000000FFFFC000FFF003FF00FF80FE03F81F81F03E0F83E0F078),
    .INIT_00(256'hC8B9AA9B8C7E706255473A2E211509FDF2E7DCD1C7BDB3AAA0978E867E766E67),
    .INIT_01(256'h3D250EF7E0C9B39C87715C46321D09F5E1CDBAA79482705E4C3B291908F8E7D8),
    .INIT_02(256'hBE9E7E5F3F2001E3C5A7896B4E3115F8DCC0A4896E53391E04EAD1B79E866D55),
    .INIT_03(256'h4B23FBD3AB835C350FE8C29C77522D08E3BF9B7754300DEBC8A684634120FFDE),
    .INIT_04(256'hE4B3825222F2C394653607D9AB7E5023F6C99D714519EEC3986E4319F0C69D74),
    .INIT_05(256'h884F16DDA56D35FDC68F5821EBB57F4A14DFAB76420EDAA774410EDCAA784615),
    .INIT_06(256'h37F5B47332F2B27232F2B37436F7B97B3E01C3874A0ED2965B1FE4AA6F35FBC1),
    .INIT_07(256'hF0A65C13CA8139F0A86119D28B44FEB8722CE7A25D18D4904C08C5823FFDBA78),
    .INIT_08(256'hB3600FBD6C1BCA7929D9893AEA9B4DFEB06214C77A2DE09447FCB0651ACF843A),
    .INIT_09(256'h7E24CA7016BD640BB35A02AA53FCA54EF7A14BF5A04BF6A14DF8A551FEAA5705),
    .INIT_0A(256'h53F08E2CCA6807A645E48424C46505A648E98B2DCF7214B75BFEA246EB8F34D9),
    .INIT_0B(256'h2FC459EF851BB148DF760EA53DD66E07A039D36C06A13BD6710CA844E07C19B6),
    .INIT_0C(256'h129E2CB947D563F1800F9E2EBE4EDE6FFF9022B345D769FC8F22B549DD71059A),
    .INIT_0D(256'hFA7F04890F951BA127AE35BD44CC54DD65EE77008A149E28B33EC955E06CF885),
    .INIT_0E(256'hE864E15EDC59D755D453D151D050D050D051D253D556D85BDD60E366EA6DF176),
    .INIT_0F(256'hD94EC237AD22980E84FB72E960D84FC840B831AA249D17910C86017CF873EF6B),
    .INIT_10(256'hCD3AA61380EE5CCA38A61584F363D343B323940576E85ACC3EB02396097DF165),
    .INIT_11(256'hC3278CF156BB2187ED54BA2189F058C02890F962CB359E0872DD48B31E89F561),
    .INIT_12(256'hB91572CF2C89E745A30260BF1F7EDE3E9EFE5FC02182E446A80B6DD03497FB5F),
    .INIT_13(256'hAE0257AC0157AC0259AF065DB40C63BB136CC51E77D02A84DE3993EE49A5015D),
    .INIT_14(256'hA0ED3A87D42270BE0C5BA9F84897E73787D8297ACB1D6EC01365B80B5EB2065A),
    .INIT_15(256'h8FD4195EA4E92F76BC034A91D82068B0F8418AD31D66B0FA458FDA2571BC0854),
    .INIT_16(256'h79B6F3306EACEA2867A6E52464A4E42465A6E7286AABED3072B5F83B7FC3074B),
    .INIT_17(256'h5B91C6FC32689ED50C437AB2E9215A92CB043D77B1EB255F9AD5104C88C3003C),
    .INIT_18(256'h366391BFED1B4A79A8D707376797C8F92A5B8DBEF0235588BBEE225589BDF227),
    .INIT_19(256'h062B51789EC5EC133B628AB3DB042D567FA9D3FD27527CA7D3FE2A5682AFDB08),
    .INIT_1A(256'hCAE80625446382A2C2E20223446586A8C9EB0D30537699BCE004284C7196BBE0),
    .INIT_1B(256'h8097ADC5DCF40B233C546D869FB9D3EC07213C57728DA9C5E1FD193653708EAC),
    .INIT_1C(256'h2736455565758596A6B8C9DAECFE102335485B6F8296AABFD3E8FD12283D536A),
    .INIT_1D(256'hBCC3CBD4DCE5EEF7000A141E28323D48535F6B76838F9CA8B6C3D0DEECFA0918),
    .INIT_1E(256'h3D3D3E3F4041434547494B4E5154575B5F63676C70757B80868C92989FA6ADB4),
    .INIT_1F(256'hA9A29B958F89837D78736E6965615D5956524F4D4A48464442413F3E3E3D3D3D),
    .INIT_20(256'hFDEFE1D3C6B8AB9F92867A6E62574C41362B21170D04FAF1E8DFD7CFC7BFB7B0),
    .INIT_21(256'h37210CF8E3CFBAA6937F6C594634210FFDEBDAC9B8A796867666564737281A0B),
    .INIT_22(256'h54381C00E4C9AE93785D43290FF5DCC2A990785F472F1800E9D2BBA48E78624C),
    .INIT_23(256'h54300DEAC8A583613F1DFCDBBA9978583818F9D9BA9B7C5E3F2103E6C8AB8E71),
    .INIT_24(256'h3208DEB48A61380FE6BD956D451DF6CEA7805A330DE7C19C76512C08E3BF9B77),
    .INIT_25(256'hEDBC8B5A2AFAC99A6A3A0BDCAD7F5022F4C6996C3F12E5B98C603409DDB2875D),
    .INIT_26(256'h834B13DBA46D36FFC9935D27F1BC86511DE8B4804C18E5B17E4B19E6B482501F),
    .INIT_27(256'hF0B17234F6B87B3D00C3864A0ED1955A1EE3A86D32F8BE834A10D79D642CF3BB),
    .INIT_28(256'h32ECA7631ED995510DCA864300BD7B38F6B47231F0AE6D2DECAC6C2CECAD6D2E),
    .INIT_29(256'h46FBAF6418CD8338EEA35910C67D34EBA25911C98139F2AA631CD68F4903BD77),
    .INIT_2A(256'h2BD98635E39140EF9E4EFDAD5D0DBD6E1FD08132E49648FAAC5F11C4782BDF92),
    .INIT_2B(256'hDC842BD37B23CB741DC66F18C26C16C06A15BF6A15C16C18C4701CC97623D07D),
    .INIT_2C(256'h58F99A3CDE7F21C46609AC4FF29539DD8125CA6E13B85D03A84EF49A41E78E35),
    .INIT_2D(256'h9B36D16C08A43FDC7814B14EEB8825C361FF9D3BDA7918B756F69535D67616B7),
    .INIT_2E(256'hA338CD62F78D22B84FE57B12A940D76F079E36CF67009831CB64FE9731CB6600),
    .INIT_2F(256'h6CFB8A19A838C857E87808992ABB4CDE6F019325B84ADD7003962ABD51E57A0E),
    .INIT_30(256'hF47C068F18A22CB640CA55E06BF6810C9824B03CC955E26FFC8917A532C14FDD),
    .INIT_31(256'h37BA3DC144C84CD055D95EE368ED73F87E048A10971EA52CB33AC24AD25AE26B),
    .INIT_32(256'h32B02DAB29A725A422A1209F1F9E1E9E1E9E1E9F20A122A325A628AA2CAF31B4),
    .INIT_33(256'hE35BD34BC33CB42DA61F99128C0680FA74EF6AE560DB56D24ECA46C23FBB38B5),
    .INIT_34(256'h46B82B9D1083F66ADD51C438AC21950A7FF469DE54C93FB52BA2188F067DF46C),
    .INIT_35(256'h58C5329F0C7AE755C332A00E7DEC5BCA39A91989F969D94ABA2B9C0E7FF162D4),
    .INIT_36(256'h157DE54CB41D85ED56BF2891FA64CE37A10C76E04BB6218CF763CF3AA6137FEB),
    .INIT_37(256'h7BDD40A20568CB2E92F559BD2186EA4FB3187DE248AD1379DF45AC1279E047AE),
    .INIT_38(256'h85E2409DFB59B71573D2308FEE4DAD0C6CCB2B8BEC4CAD0D6ECF3092F355B719),
    .INIT_39(256'h3189E23A93EC459EF751AA045EB8126DC7227DD8338EEA45A1FD59B6126FCB28),
    .INIT_3A(256'h7BCE2275C91D71C51A6EC3186DC2176DC2186EC41A71C71E75CC237AD22981D9),
    .INIT_3B(256'h5FAEFC4B9AE93988D82877C81868B9095AABFC4D9FF04294E6388ADC2F82D528),
    .INIT_3C(256'hDA246EB9034D98E32E79C40F5BA6F23E8AD6236FBC0956A3F03D8BD92674C311),
    .INIT_3D(256'hE92F74BAFF458BD2185EA5EC337AC1085097DF276FB7FF4890D9226BB4FE4791),
    .INIT_3E(256'h88C90A4B8CCE0F5193D517599CDE2164A7EA2D70B4F83B7FC3084C91D51A5FA4),
    .INIT_3F(256'hB2EF2C68A5E3205D9BD8165492D00F4D8CCA094887C7064685C5054585C60647),
    .INIT_40(256'hFFB76F27DF975008C17A33ECA55E18D28B45FFBA742FE9A45F1AD5914C08C37F),
    .INIT_41(256'h5609BC6F23D68A3EF2A65B0FC4792EE3984D03B96E24DA9147FEB46B22D99048),
    .INIT_42(256'h42F09F4DFCAB5A09B96818C87728D88839E99A4BFCAE5F11C37426D98B3DF0A3),
    .INIT_43(256'hC7711AC46E18C26D17C26D18C36E1AC5711DC97522CE7B28D5822FDC8A38E694),
    .INIT_44(256'hEA8E33D87D22C76D12B85E04AA51F79E45EC933AE28931D98129D27A23CC751E),
    .INIT_45(256'hAD4CEC8B2BCB6C0CAD4DEE8F30D27315B759FB9D40E28528CB6F12B659FDA145),
    .INIT_46(256'h13AD48E27D18B34FEA8621BD59F5922ECB6805A240DD7B19B755F39230CF6E0D),
    .INIT_47(256'h21B64BE0760CA137CE64FA9128BF56ED851DB44CE57D15AE47E07912AC45DF79),
    .INIT_48(256'hD969F98919A939CA5BEC7D0EA032C355E77A0C9F32C558EB7F13A63ACF63F78C),
    .INIT_49(256'h3FC954DE69F47F0A9521AC38C451DD6AF683109D2BB846D462F17F0E9C2BBA4A),
    .INIT_4A(256'h56DB60E56AEF74FA80068C12991FA62DB43CC34BD35BE36CF47D068F18A22BB5),
    .INIT_4B(256'h22A1209F1E9E1E9E1E9E1F9F20A122A425A729AB2DB032B538BB3FC246CA4ED2),
    .INIT_4C(256'hA51E97108A047EF873ED68E35ED955D04CC844C13DBA37B431AF2CAA28A625A3),
    .INIT_4D(256'hE255C93CB024980C81F66BE055CA40B62CA2188F067CF46BE25AD24AC23AB32C),
    .INIT_4E(256'hDD4AB82593016FDE4CBB2A990878E857C838A8198AFB6CDD4FC132A51789FC6F),
    .INIT_4F(256'h980067CF369E076FD740A9127BE54FB8228DF762CD38A30E7AE551BD2A960370),
    .INIT_50(256'h1879DA3B9DFF61C32588EB4EB11478DC3FA4086CD1369B0066CB3197FE64CB31),
    .INIT_51(256'h5DB8136ECA2581DD3995F24FAC0966C4217FDE3C9AF958B71676D63595F656B7),
    .INIT_52(256'h6CC1156ABF156AC0166CC2186FC61D74CB237BD32B84DC358EE7419AF44EA803),
    .INIT_53(256'h4896E43281D01F6EBD0D5DADFD4E9EEF4091E33586D92B7DD02376C91C70C418),
    .INIT_54(256'hF23981C91159A2EB347DC61059A3EE3883CD1864AFFB4692DF2B78C4115FACFA),
    .INIT_55(256'h6DAEF03172B4F6387BBD004386CA0D5195D91E63A7EC3277BD03498FD61C63AA),
    .INIT_56(256'hBEF8326DA8E31E5A95D10E4A86C3003D7BB8F63472B1F02E6DADEC2C6CACEC2D),
    .INIT_57(256'hE5184C80B4E81D5186BCF1275D93C9FF366DA4DB134B83BBF32C649DD7104A83),
    .INIT_58(256'hE5123F6C99C6F422507FADDC0B3A6A9AC9FA2A5A8BBCED1F5082B4E6194B7EB1),
    .INIT_59(256'hC1E70D335A80A7CEF61D456D95BDE60F38618AB4DE08325D87B2DD0934608CB9),
    .INIT_5A(256'h7C9BBAD9F91838587899BADBFC1D3F6183A5C8EA0D3054779BBFE3082C51769C),
    .INIT_5B(256'h182F475F7890A9C2DCF50F29435D7893AEC9E4001C3854718EABC8E603213F5E),
    .INIT_5C(256'h96A7B8C9DAEBFD0F213446596C7F93A6BACFE3F80C21374C62788EA4BBD2E900),
    .INIT_5D(256'hFA040D17212B36414C57626E7A86929FABB8C6D3E1EFFD0B1A28374756667686),
    .INIT_5E(256'h46484A4D4F5256595D6165696E73787D83898F959BA2A9B0B7BFC7CFD7DFE8F1),
    .INIT_5F(256'h7B75706C67635F5B5754514E4B4947454341403F3E3D3D3D3D3D3E3E3F414244),
    .INIT_60(256'h9C8F83766B5F53483D32281E140A00F7EEE5DCD4CBC3BCB4ADA69F98928C8680),
    .INIT_61(256'hAA96826F5B48352310FEECDAC9B8A696857565554536271809FAECDED0C3B6A8),
    .INIT_62(256'hA98D72573C2107ECD3B99F866D543C230BF4DCC5AD97806A533D2812FDE8D3BF),
    .INIT_63(256'h997653300DEBC9A88665442302E2C2A28263442506E8CAAC8E70533619FDE1C5),
    .INIT_64(256'h7C5227FDD3A97F562D04DBB38A623B13ECC59E78512B06E0BB96714C2804E0BC),
    .INIT_65(256'h5523F0BE8D5B2AF9C897673707D7A8794A1BEDBF91633608DBAF82562AFED3A7),
    .INIT_66(256'h25EBB1773D04CB925A21E9B27A430CD59E6832FCC6915B27F2BD895522EEBB88),
    .INIT_67(256'hEDAB6A28E7A66524E4A46424E5A66728EAAC6E30F3B6793C00C3884C10D59A5F),
    .INIT_68(256'hB0661DD38A41F8B06820D8914A03BC762FE9A45E19D48F4B07C37F3BF8B57230),
    .INIT_69(256'h6E1DCB7A29D88737E79748F8A95B0CBE7022D4873AEDA05408BC7125DA8F45FA),
    .INIT_6A(256'h2AD0771EC56C13BB630CB45D06AF5902AC5701AC5702AE5A06B25E0BB86513C0),
    .INIT_6B(256'hE48221C05FFE9E3EDE7E1FBF6002A345E7892CCF7215B95D01A549EE9339DE84),
    .INIT_6C(256'h9E35CB62F99028C058F08921BA54ED8721BB56F18C27C35FFB9734D06D0BA846),
    .INIT_6D(256'h5AE876059423B343D363F38415A638CA5CEE8013A63ACD61F5891EB348DD7208),
    .INIT_6E(256'h179D24AA31B840C84FD860E972FB840E9822AD37C24ED965F17D099623B03ECC),
    .INIT_6F(256'hD856D553D251D050D050D051D153D455D759DC5EE164E86BEF73F87C01860C91),
    .INIT_70(256'h9E148A0077EE65DD54CC44BD35AE27A11B950F89047FFA76F16DEA66E360DD5B),
    .INIT_71(256'h69D745B32290FF6FDE4EBE2E9E0F80F163D547B92C9E1285F86CE055C93EB328),
    .INIT_72(256'h3BA1066CD339A0076ED63DA50E76DF48B11B85EF59C42F9A0571DD49B5228FFC),
    .INIT_73(256'h1472CF2D8BE948A60565C42484E445A60768CA2C8EF053B6197CE044A80C71D6),
    .INIT_74(256'hF64BA0F54BA1F74EA5FC53AA025AB30B64BD1670CA247ED9348FEB46A2FE5BB7),
    .INIT_75(256'hE02D7AC71462B0FE4D9BEA3A89D92979CA1B6CBD0F60B30557AAFE51A5F84DA1),
    .INIT_76(256'hD4185DA2E72C72B8FE448BD21961A8F03981CA135CA6F03A84CF1A65B0FC4794),
    .INIT_77(256'hD20E4A87C3013E7BB9F73674B3F23272B2F23273B4F53778BAFD3F82C5084C90),
    .INIT_78(256'hDA0E4276ABDF144A7FB5EB21588FC6FD356DA5DD164F88C1FB356FAAE41F5B96),
    .INIT_79(256'hEE1945719DC9F623507EABD907366594C3F2225282B3E4154678AADC0E4174A7),
    .INIT_7A(256'h0D3054779BBFE3082D52779CC2E80F355C83ABD3FB234B749DC6F019436E98C3),
    .INIT_7B(256'h39536E89A4C0DCF815314E6B89A7C5E301203F5F7E9EBEDEFF20416384A6C8EB),
    .INIT_7C(256'h708294A7BACDE1F5091D32465C71879CB3C9E0F70E253D556D869EB7D1EA041E),
    .INIT_7D(256'hB3BDC7D1DCE7F2FD0915212E3A475562707E8C9BAAB9C8D8E7F80819293B4C5E),
    .INIT_7E(256'h030406080A0D101316191D21262A2F343A3F454B52585F676E767E868E97A0AA),
    .INIT_7F(256'h5F58524B453F3A342F2A26211D191613100D0A08060403020100000000000102),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized17
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'hFFFFFFFFFF800000000000000000000000000000000000000000000000000000),
    .INITP_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_05(256'h000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_06(256'hFFFFFFF000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_08(256'h0000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000),
    .INITP_0B(256'h0003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0202020101010101010101010101010101010101010101010101010101000000),
    .INIT_02(256'h0303030303030303030303030303030202020202020202020202020202020202),
    .INIT_03(256'h0606050505050505050505050505050504040404040404040404040404040303),
    .INIT_04(256'h0808080808080808080808070707070707070707070706060606060606060606),
    .INIT_05(256'h0C0C0C0B0B0B0B0B0B0B0B0B0A0A0A0A0A0A0A0A0A0A09090909090909090909),
    .INIT_06(256'h100F0F0F0F0F0F0F0F0E0E0E0E0E0E0E0E0E0D0D0D0D0D0D0D0D0C0C0C0C0C0C),
    .INIT_07(256'h1414141413131313131313121212121212121111111111111111101010101010),
    .INIT_08(256'h1919191818181818181717171717171616161616161615151515151515141414),
    .INIT_09(256'h1E1E1E1E1E1D1D1D1D1D1D1C1C1C1C1C1B1B1B1B1B1B1A1A1A1A1A1A19191919),
    .INIT_0A(256'h242424242323232323222222222222212121212120202020201F1F1F1F1F1F1E),
    .INIT_0B(256'h2B2A2A2A2A2A2929292929282828282827272727272626262626252525252524),
    .INIT_0C(256'h323131313130303030302F2F2F2F2E2E2E2E2E2D2D2D2D2C2C2C2C2C2B2B2B2B),
    .INIT_0D(256'h3939393838383837373737363636363535353535343434343333333332323232),
    .INIT_0E(256'h4141404040403F3F3F3F3E3E3E3E3D3D3D3D3C3C3C3C3B3B3B3B3A3A3A3A3939),
    .INIT_0F(256'h4949494948484848474747464646464545454544444444434343434242424141),
    .INIT_10(256'h52525252515151505050504F4F4F4E4E4E4E4D4D4D4C4C4C4C4B4B4B4B4A4A4A),
    .INIT_11(256'h5C5C5B5B5B5A5A5A595959595858585757575656565655555554545454535353),
    .INIT_12(256'h6666656565646464636363626262616161606060605F5F5F5E5E5E5D5D5D5C5C),
    .INIT_13(256'h7070706F6F6F6E6E6E6D6D6D6C6C6C6B6B6B6A6A6A6969696868686767676766),
    .INIT_14(256'h7B7B7B7A7A7A7979797878777777767676757575747474737373727272717171),
    .INIT_15(256'h8786868685858584848483838282828181818080807F7F7E7E7E7D7D7D7C7C7C),
    .INIT_16(256'h9392929291919090908F8F8F8E8E8D8D8D8C8C8C8B8B8A8A8A89898988888887),
    .INIT_17(256'h9F9F9E9E9E9D9D9C9C9C9B9B9A9A9A9999999898979797969695959594949493),
    .INIT_18(256'hACACABABAAAAAAA9A9A8A8A8A7A7A6A6A6A5A5A4A4A4A3A3A2A2A2A1A1A0A0A0),
    .INIT_19(256'hBAB9B9B8B8B7B7B7B6B6B5B5B4B4B4B3B3B2B2B1B1B1B0B0AFAFAFAEAEADADAD),
    .INIT_1A(256'hC7C7C7C6C6C5C5C4C4C3C3C3C2C2C1C1C0C0C0BFBFBEBEBDBDBDBCBCBBBBBABA),
    .INIT_1B(256'hD6D5D5D4D4D3D3D3D2D2D1D1D0D0CFCFCFCECECDCDCCCCCBCBCACACAC9C9C8C8),
    .INIT_1C(256'hE5E4E4E3E3E2E2E1E1E0E0DFDFDEDEDEDDDDDCDCDBDBDADAD9D9D8D8D8D7D7D6),
    .INIT_1D(256'hF4F3F3F2F2F1F1F0F0F0EFEFEEEEEDEDECECEBEBEAEAE9E9E8E8E7E7E6E6E6E5),
    .INIT_1E(256'h040303020201010000FFFFFEFEFDFDFCFCFBFBFAFAF9F9F8F8F7F7F6F6F5F5F4),
    .INIT_1F(256'h1413131212111110100F0F0E0E0D0D0C0C0B0B0A0A0909080807070606050504),
    .INIT_20(256'h242423232222212120201F1F1E1E1D1D1C1C1B1B1A1A19181817171616151514),
    .INIT_21(256'h3635353433333232313130302F2F2E2E2D2C2C2B2B2A2A292928282727262625),
    .INIT_22(256'h47474646454444434342424141403F3F3E3E3D3D3C3C3B3B3A39393838373736),
    .INIT_23(256'h5959585757565655555453535252515150504F4E4E4D4D4C4C4B4B4A49494848),
    .INIT_24(256'h6C6B6A6A69696868676666656564636362626161605F5F5E5E5D5D5C5B5B5A5A),
    .INIT_25(256'h7E7E7D7D7C7B7B7A7A79797877777676757474737372717170706F6F6E6D6D6C),
    .INIT_26(256'h929191908F8F8E8D8D8C8C8B8A8A89898887878686858484838382818180807F),
    .INIT_27(256'hA5A5A4A4A3A2A2A1A1A09F9F9E9D9D9C9C9B9A9A999898979796959594949392),
    .INIT_28(256'hBAB9B8B8B7B6B6B5B5B4B3B3B2B1B1B0AFAFAEAEADACACABAAAAA9A9A8A7A7A6),
    .INIT_29(256'hCECDCDCCCCCBCACAC9C8C8C7C6C6C5C4C4C3C3C2C1C1C0BFBFBEBDBDBCBCBBBA),
    .INIT_2A(256'hE3E2E2E1E0E0DFDEDEDDDCDCDBDBDAD9D9D8D7D7D6D5D5D4D3D3D2D1D1D0CFCF),
    .INIT_2B(256'hF8F8F7F6F6F5F4F4F3F2F2F1F0F0EFEEEEEDECECEBEAEAE9E8E8E7E6E6E5E4E4),
    .INIT_2C(256'h0E0D0D0C0B0B0A090908070706050504030302010100FFFFFEFDFCFCFBFAFAF9),
    .INIT_2D(256'h242423222221201F1F1E1D1D1C1B1B1A1918181716161514141312121110100F),
    .INIT_2E(256'h3B3A3939383737363534343332323130302F2E2D2D2C2B2B2A29282827262625),
    .INIT_2F(256'h525150504F4E4D4D4C4B4B4A49484847464645444343424141403F3E3E3D3C3C),
    .INIT_30(256'h696868676665656463626261605F5F5E5D5D5C5B5A5A59585757565555545352),
    .INIT_31(256'h81807F7E7E7D7C7B7B7A79787877767575747373727170706F6E6D6D6C6B6A6A),
    .INIT_32(256'h999897969695949393929190908F8E8D8D8C8B8A8A8988878786858484838281),
    .INIT_33(256'hB1B0AFAFAEADACACABAAA9A9A8A7A6A5A5A4A3A2A2A1A09F9F9E9D9C9C9B9A99),
    .INIT_34(256'hCAC9C8C7C7C6C5C4C3C3C2C1C0C0BFBEBDBCBCBBBAB9B9B8B7B6B6B5B4B3B2B2),
    .INIT_35(256'hE3E2E1E0E0DFDEDDDCDCDBDAD9D8D8D7D6D5D5D4D3D2D1D1D0CFCECECDCCCBCA),
    .INIT_36(256'hFCFBFAFAF9F8F7F6F6F5F4F3F2F2F1F0EFEFEEEDECEBEBEAE9E8E7E7E6E5E4E3),
    .INIT_37(256'h16151413131211100F0E0E0D0C0B0A0A090807060605040302020100FFFEFEFD),
    .INIT_38(256'h302F2E2D2C2C2B2A2928282726252424232221201F1F1E1D1C1B1B1A19181717),
    .INIT_39(256'h4A4948484746454443434241403F3F3E3D3C3B3A3A3938373635353433323131),
    .INIT_3A(256'h656463626161605F5E5D5C5C5B5A5958575756555453525251504F4E4D4D4C4B),
    .INIT_3B(256'h807F7E7D7C7B7B7A7978777676757473727170706F6E6D6C6B6B6A6968676666),
    .INIT_3C(256'h9B9A9998989796959493929291908F8E8D8C8C8B8A8988878686858483828181),
    .INIT_3D(256'hB6B6B5B4B3B2B1B0B0AFAEADACABAAAAA9A8A7A6A5A4A3A3A2A1A09F9E9D9D9C),
    .INIT_3E(256'hD2D1D1D0CFCECDCCCBCACAC9C8C7C6C5C4C3C3C2C1C0BFBEBDBDBCBBBAB9B8B7),
    .INIT_3F(256'hEEEDEDECEBEAE9E8E7E6E6E5E4E3E2E1E0DFDFDEDDDCDBDAD9D8D8D7D6D5D4D3),
    .INIT_40(256'hA3A4A5A6A7A8A9AAAAABACADAEAFB0B0B1B2B3B4B5B6B6B7B8B9BABBBCBDBDBE),
    .INIT_41(256'h88898A8B8C8C8D8E8F9091929293949596979898999A9B9C9D9D9E9FA0A1A2A3),
    .INIT_42(256'h6D6E6F7070717273747576767778797A7B7B7C7D7E7F80818182838485868687),
    .INIT_43(256'h5253545556575758595A5B5C5C5D5E5F6061616263646566666768696A6B6B6C),
    .INIT_44(256'h38393A3A3B3C3D3E3F3F4041424343444546474848494A4B4C4D4D4E4F505152),
    .INIT_45(256'h1E1F1F2021222324242526272828292A2B2C2C2D2E2F30313132333435353637),
    .INIT_46(256'h040506060708090A0A0B0C0D0E0E0F1011121313141516171718191A1B1B1C1D),
    .INIT_47(256'hEBEBECEDEEEFEFF0F1F2F2F3F4F5F6F6F7F8F9FAFAFBFCFDFEFEFF0001020203),
    .INIT_48(256'hD1D2D3D4D5D5D6D7D8D8D9DADBDCDCDDDEDFE0E0E1E2E3E3E4E5E6E7E7E8E9EA),
    .INIT_49(256'hB9B9BABBBCBCBDBEBFC0C0C1C2C3C3C4C5C6C7C7C8C9CACACBCCCDCECECFD0D1),
    .INIT_4A(256'hA0A1A2A2A3A4A5A5A6A7A8A9A9AAABACACADAEAFAFB0B1B2B2B3B4B5B6B6B7B8),
    .INIT_4B(256'h88898A8A8B8C8D8D8E8F90909192939394959696979899999A9B9C9C9D9E9F9F),
    .INIT_4C(256'h707172737374757576777878797A7B7B7C7D7E7E7F8081818283848485868787),
    .INIT_4D(256'h595A5A5B5C5D5D5E5F5F606162626364656566676868696A6A6B6C6D6D6E6F70),
    .INIT_4E(256'h42434344454646474848494A4B4B4C4D4D4E4F50505152525354555556575758),
    .INIT_4F(256'h2B2C2D2D2E2F3030313232333434353637373839393A3B3C3C3D3E3E3F404141),
    .INIT_50(256'h151616171818191A1B1B1C1D1D1E1F1F20212222232424252626272828292A2B),
    .INIT_51(256'hFF0001010203030405050607070809090A0B0B0C0D0D0E0F1010111212131414),
    .INIT_52(256'hEAEAEBECECEDEEEEEFF0F0F1F2F2F3F4F4F5F6F6F7F8F8F9FAFAFBFCFCFDFEFF),
    .INIT_53(256'hD5D5D6D7D7D8D9D9DADBDBDCDCDDDEDEDFE0E0E1E2E2E3E4E4E5E6E6E7E8E8E9),
    .INIT_54(256'hC0C1C1C2C3C3C4C4C5C6C6C7C8C8C9CACACBCCCCCDCDCECFCFD0D1D1D2D3D3D4),
    .INIT_55(256'hACACADAEAEAFAFB0B1B1B2B3B3B4B5B5B6B6B7B8B8B9BABABBBCBCBDBDBEBFBF),
    .INIT_56(256'h9898999A9A9B9C9C9D9D9E9F9FA0A1A1A2A2A3A4A4A5A5A6A7A7A8A9A9AAAAAB),
    .INIT_57(256'h8485868687878889898A8A8B8C8C8D8D8E8F8F90919192929394949595969797),
    .INIT_58(256'h71727373747475767677777879797A7A7B7B7C7D7D7E7E7F8080818182838384),
    .INIT_59(256'h5F5F60616162626363646565666667686869696A6A6B6C6C6D6D6E6F6F707071),
    .INIT_5A(256'h4D4D4E4E4F5050515152525353545555565657575859595A5A5B5B5C5D5D5E5E),
    .INIT_5B(256'h3B3C3C3D3D3E3E3F3F4041414242434344444546464747484849494A4B4B4C4C),
    .INIT_5C(256'h2A2A2B2B2C2C2D2E2E2F2F303031313232333334353536363737383839393A3B),
    .INIT_5D(256'h191A1A1B1B1C1C1D1D1E1E1F1F20202121222223232424252626272728282929),
    .INIT_5E(256'h09090A0A0B0B0C0C0D0D0E0E0F0F101011111212131314141515161617171818),
    .INIT_5F(256'hF9F9FAFAFBFBFCFCFDFDFEFEFFFF000001010202030304040505060607070808),
    .INIT_60(256'hE9EAEAEBEBECECEDEDEEEEEFEFF0F0F0F1F1F2F2F3F3F4F4F5F5F6F6F7F7F8F8),
    .INIT_61(256'hDADBDBDCDCDDDDDEDEDEDFDFE0E0E1E1E2E2E3E3E4E4E5E5E6E6E6E7E7E8E8E9),
    .INIT_62(256'hCCCCCDCDCECECFCFCFD0D0D1D1D2D2D3D3D3D4D4D5D5D6D6D7D7D8D8D8D9D9DA),
    .INIT_63(256'hBEBEBFBFC0C0C0C1C1C2C2C3C3C3C4C4C5C5C6C6C7C7C7C8C8C9C9CACACACBCB),
    .INIT_64(256'hB0B1B1B1B2B2B3B3B4B4B4B5B5B6B6B7B7B7B8B8B9B9BABABABBBBBCBCBDBDBD),
    .INIT_65(256'hA3A4A4A4A5A5A6A6A6A7A7A8A8A8A9A9AAAAAAABABACACADADADAEAEAFAFAFB0),
    .INIT_66(256'h97979798989999999A9A9A9B9B9C9C9C9D9D9E9E9E9F9FA0A0A0A1A1A2A2A2A3),
    .INIT_67(256'h8A8B8B8C8C8C8D8D8D8E8E8F8F8F909090919192929293939494949595959696),
    .INIT_68(256'h7F7F808080818181828282838384848485858586868687878888888989898A8A),
    .INIT_69(256'h74747475757576767677777778787979797A7A7A7B7B7B7C7C7C7D7D7D7E7E7E),
    .INIT_6A(256'h69696A6A6A6B6B6B6C6C6C6D6D6D6E6E6E6F6F6F707070717171727272737373),
    .INIT_6B(256'h5F5F606060606161616262626363636464646565656666666767676768686869),
    .INIT_6C(256'h5556565656575757585858595959595A5A5A5B5B5B5C5C5C5C5D5D5D5E5E5E5F),
    .INIT_6D(256'h4C4C4D4D4D4E4E4E4E4F4F4F5050505051515152525252535353545454545555),
    .INIT_6E(256'h44444444454545454646464647474748484848494949494A4A4A4B4B4B4B4C4C),
    .INIT_6F(256'h3B3C3C3C3C3D3D3D3D3E3E3E3E3F3F3F3F404040404141414142424243434343),
    .INIT_70(256'h343434353535353536363636373737373838383839393939393A3A3A3A3B3B3B),
    .INIT_71(256'h2D2D2D2D2E2E2E2E2E2F2F2F2F30303030303131313132323232323333333334),
    .INIT_72(256'h26262727272727282828282829292929292A2A2A2A2A2B2B2B2B2B2C2C2C2C2C),
    .INIT_73(256'h2020202121212121222222222222232323232324242424242525252525262626),
    .INIT_74(256'h1A1B1B1B1B1B1B1C1C1C1C1C1D1D1D1D1D1D1E1E1E1E1E1E1F1F1F1F1F1F2020),
    .INIT_75(256'h1516161616161616171717171717181818181818191919191919191A1A1A1A1A),
    .INIT_76(256'h1111111111121212121212121313131313131314141414141414151515151515),
    .INIT_77(256'h0D0D0D0D0D0E0E0E0E0E0E0E0E0E0F0F0F0F0F0F0F0F10101010101010111111),
    .INIT_78(256'h090A0A0A0A0A0A0A0A0A0A0B0B0B0B0B0B0B0B0B0C0C0C0C0C0C0C0C0C0D0D0D),
    .INIT_79(256'h0607070707070707070707070808080808080808080808090909090909090909),
    .INIT_7A(256'h0404040404040405050505050505050505050505050606060606060606060606),
    .INIT_7B(256'h0202020202020202030303030303030303030303030303030304040404040404),
    .INIT_7C(256'h0101010101010101010101010101010101010101020202020202020202020202),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000010101010101),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized18
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h1C3C3F00FFFFF803E0F1E318CCCCD9249694AD556AD552A52DA49B2666739C78),
    .INITP_01(256'h71C78781FE000007FC1F0E1CE3319B364925AD6AD5555556AD696DB6CD999CE7),
    .INITP_02(256'h71C3C3E01FFFFFFF80F87871CE7333264924B4A54AAB54AAA56B4B49364CCCCE),
    .INITP_03(256'h7C1FC003FF0007E07870E318CCCCD936D25A56AD5555555AB52D2DB64D9999CE),
    .INITP_04(256'hFFF007E0F0F1C6398CCCD9B6496D294AD55AAAAD54A94A4B6DB66CCCCE638E38),
    .INITP_05(256'h8CCCC9936DB69296A56AAD55556AA54A52D24B24D9933198C71C78783F007FFF),
    .INITP_06(256'hA955AB52D696DB6DB266CCCCE631CE3C78781F80FFFE00FFFE01F83C3C71C739),
    .INITP_07(256'h7FE0000000003FF01F83C1E3C71C739CC666664C9B24924B4B4A56AD54AAAAAA),
    .INITP_08(256'h6DB6D2D695AB552AAAAAAA556AD4A5A5A49249B264CCCCC6739C71C78F0783F0),
    .INITP_09(256'hD292DB6D9326666339C71C78783F00FFFE00FFFE03F03C3C78E718CE6666CC9B),
    .INITP_0A(256'h38C71E1E0FC01FFFFFFC01F83C3C71C63319933649A49694A54AAD55556AAD4A),
    .INITP_0B(256'h0FC001FF8007F07C38E38CE6666CDB6DA4A52A556AAAB556A5296D24DB366663),
    .INITP_0C(256'hFFFFFFF00F87871CE7333364DB69695AB55555556AD4B496D9366666318E1C3C),
    .INITP_0D(256'hC00000FF03C3C71CE66664D925A5AD4AAA55AAA54A5A4924C9999CE71C3C3E03),
    .INITP_0E(256'h803FFFFE01F87871CE733366DB6D2D6AD5555556AD6B4924D9B3198E70E1F07F),
    .INITP_0F(256'hF07F00000001FC1E3C739CCCC9B24B694A9556AD556A52D249366666318F1E0F),
    .INIT_00(256'h40C03CC040C850D864F08014A83CD4700CAC4CEC9438E48C3CEC9C5004BC7430),
    .INIT_01(256'hF4306CA8E82C70B8004C98E43888E03490EC48A8086CD43CA41080F064D84CC8),
    .INIT_02(256'h0C00FCF4F4F4F4F8FC040C182838485C748CA8C4E404244C709CC8F4245488BC),
    .INIT_03(256'h8034E8A05C18D898581CE4AC784414E4B89068401CF8D8BCA0846C5844342418),
    .INIT_04(256'h50C034A8209C1494149418A028B03CCC5CF08418B44CEC882CCC741CC4701CCC),
    .INIT_05(256'h7CA8D80C4078B0EC2868A8EC3078C00C5CACFC50A4FC58B41474D438A00874E0),
    .INIT_06(256'hFCE8D4C4B8ACA0989490909090949CA4B0BCCCDCF0081C38547090B4D8FC2450),
    .INIT_07(256'hD07824D08030E49C500CC8844408CC945C24F0C090643810E8C4A08060442C10),
    .INIT_08(256'hF054BC28980478E860D850CC48C84CD054DC68F48010A438D06804A03CE08028),
    .INIT_09(256'h587CA0CCF4245084B4EC245C98D4145498E02870BC0C5CAC0058B00C68C42888),
    .INIT_0A(256'h00E4C8B098847060504438302824242424282C34404C58687C90A4BCD8F41434),
    .INIT_0B(256'hE8882CD07820CC7828DC8C44FCB4702CECB0743800CC98643408DCB48C644020),
    .INIT_0C(256'h0464C42890F460CC38A81C8C047CF470F070F478FC84109C2CBC4CE47810AC48),
    .INIT_0D(256'h506C8CB0D4F8204C78A8D8083C74ACE82464A4E82C70BC0454A0F4449CF44CA8),
    .INIT_0E(256'hC09C7C5C40240CF4E0CCBCACA0988C8884808080848C909CA8B4C4D8EC001834),
    .INIT_0F(256'h4CE88828CC7014BC6814C47424D8904804C07C4000C48C5420ECBC8C60340CE4),
    .INIT_10(256'hF04CA8086CD0349C0470E050C034AC249C189818981CA42CB440D060F0841CB4),
    .INIT_11(256'hA0B8D8F8183C6088B4DC0C3C6CA0D40C4880C0004084C8105CA8F44494E84098),
    .INIT_12(256'h4C2808E8C8AC9078644C3C2C1C1004FCF8F4F0F0F0F4F8000C14243444586C84),
    .INIT_13(256'hF49030D07014BC640CB86414C87C30E8A4601CDC9C6028F0B8845020F4C89C74),
    .INIT_14(256'h88E440A4046CD038A41080F064D84CC440BC3CBC3CC448D05CE87404982CC058),
    .INIT_15(256'hF81838587CA0C8F4204C7CB0E0185088C4004080C40C509CE83484D4287CD42C),
    .INIT_16(256'h401C00E4C8B0988470605044383028242020202428303840505C6C8094A8C0DC),
    .INIT_17(256'h4CEC9038DC8834E09040F4A86018D0904C0CD0945C24ECB8885828FCD4AC8460),
    .INIT_18(256'h1478DC44B01C88F868DC50C844BC38B838BC40C850DC68F48418AC40D87410AC),
    .INIT_19(256'h88B0D804305C8CC0F42C609CD8145498D82068B0FC4898E83890E43C98F450B0),
    .INIT_1A(256'h9C84706050403428201C141410101418202830404C5C6C8098B0C8E400204064),
    .INIT_1B(256'h40EC9C5000B86C28E0A05C1CE0A46C34FCC89864380CE0B8906C482808E8CCB4),
    .INIT_1C(256'h64D448C038B02CA828A82CB034C048D464F08418AC44DC7814B454F89C40E894),
    .INIT_1D(256'hF83068A4E01C5CA0E02870B80450A0F04094EC449CF858B41878E044B01884F4),
    .INIT_1E(256'hF0ECE8E8E8ECF0F8000C182838485C7088A4BCDCF8183C6084ACD804306090C4),
    .INIT_1F(256'h38F8BC804810DCA8784818ECC098744C2808E8CCB0947C68544030201408FCF4),
    .INIT_20(256'hBC44D05CE878089C30C45CF89430D07014B86008B05C0CB86C20D48844FCB878),
    .INIT_21(256'h70C01064B80C64C01878D43498FC60C834A00C78EC5CD048C038B430B030B438),
    .INIT_22(256'h4458708CA4C4E00424487098C0EC184878ACE0144C84C0FC3C7CC0044890D824),
    .INIT_23(256'h1CFCDCBCA0846C54402C1808F8ECE0D8D0C8C4C4C4C4C8CCD0D8E4F0FC0C1C30),
    .INIT_24(256'hEC943CE89440F0A05408C07830ECA86828E8B0743C04D09C6C3C0CE0B48C6440),
    .INIT_25(256'hA01084F86CE45CD450D04CD050D45CE46CF88414A434C860F89028C86404A448),
    .INIT_26(256'h24609CD8185898DC2068B4FC4898E8388CE0348CE844A00060C4288CF45CC834),
    .INIT_27(256'h64686C74808894A4B4C4D8EC041C3450708CACD0F418406894C0F01C5084B8EC),
    .INIT_28(256'h4818E8BC90643C18F0CCAC8C6C50341C04ECD8C4B4A4948880746C6864606060),
    .INIT_29(256'hC058F89434D87820C46C18C0701CCC8034E8A05810CC884808CC90541CE4B07C),
    .INIT_2A(256'hB01880EC58C834A8188C047CF470EC68E86CEC70F880089420B040D064F88C24),
    .INIT_2B(256'h083C74ACE4205C98D81C5CA4E83078C41060B00054A8FC54AC0864C42484E44C),
    .INIT_2C(256'hB1B1B5BDC1CDD5E1F1FD0D213549617995ADCDE9092D50749CC4EC184474A4D4),
    .INIT_2D(256'h8D5D3105DDB18D654121FDE1C1A5897159412D1909F9E9DDD1C9C1B9B5B1ADAD),
    .INIT_2E(256'h8D2DD17519C16911BD6915C57929DD954D05BD7939F5B5793D01C58D5925F1BD),
    .INIT_2F(256'h950979ED61D951C945C141BD41C145CD51D965F17D0D9D2DC155E98119B551ED),
    .INIT_30(256'h91D11559A1E53179C51161B10155A90159B10965C52181E549AD1179E54DB929),
    .INIT_31(256'h65798DA1B9D1ED0925456585A9CDF119416D99C5F1215585B9F1296199D51551),
    .INIT_32(256'hF9DDC5AD95816D5D493D2D21150D05FDF9F5F1F1F1F1F5F90109111D29354555),
    .INIT_33(256'h31EDA5611DDD995D1DE1A56D35FDC995613101D5A579512901D9B59171513115),
    .INIT_34(256'hF98915A535C559ED851DB551E98925C56509AD51F9A149F5A14DFDAD5D11C57D),
    .INIT_35(256'h3599F95DC12991F965D13DAD1D910175ED61D955CD49C949C949CD51D55DE571),
    .INIT_36(256'hC9013971ADE92965A5E9296DB5F9418DD52171BD0D61B1055DB10965BD1979D5),
    .INIT_37(256'h9DA9B9C9D9ED01152D455D7591ADCDED0D3151799DC5ED15416D9DCDFD2D6195),
    .INIT_38(256'h91795D452D1501EDDDC9B9AD9D91857D756D6965615D5D5D6161696D757D8591),
    .INIT_39(256'h914D0DC98D4D11D5996129F1BD895521F1C195693D11E9C19975512D0DEDCDB1),
    .INIT_3A(256'h7D11A941D97511AD4DED8D2DD1751DC16915BD6915C57525D5893DF1A96119D5),
    .INIT_3B(256'h35A51989FD71E55DD54DC945C141BD41C145C94DD159E16DF98511A131C155E5),
    .INIT_3C(256'hA5F13D89D52575C9196DC51971C9217DD93595F555B5197DE149B11985ED59C9),
    .INIT_3D(256'hADD1F9214D79A5D1FD2D5D91C1F5296199D1094581BDF93979BDFD4185CD155D),
    .INIT_3E(256'h2D3135394149515965717D8999A9B9CDE1F5092139516D85A1C1DDFD1D416589),
    .INIT_3F(256'h0DEDCDB1957961452D1901EDD9C5B5A59585796D61554D453D3935312D2D2D2D),
    .INIT_40(256'h15CD8541FDBD7939F9BD814509D1996129F5C1915D2DFDD1A5794D21F9D1AD89),
    .INIT_41(256'h59ED8519B149E17D19B555F59535D97D21C97119C56D19C97525D5893DF1A55D),
    .INIT_42(256'h55C131A11185F96DE159D14DC945C141BD41C145C94DD55DE571FD8919A535C9),
    .INIT_43(256'h1961A9F13D89D52575C51569BD1569C11D75D12D8DED4DAD1175D941A9117DE5),
    .INIT_44(256'hCDED0D2D517599C1E9113D6995C1F1215589BDF1296199D5114D8DC90D4D91D5),
    .INIT_45(256'h857D756D6961615D5D5D6165696D757D85919DADB9C9DDED01152D455D7991B1),
    .INIT_46(256'h612DFDCD9D6D4115EDC59D7951310DEDCDAD91755D452D1501EDD9C9B9A99D91),
    .INIT_47(256'h7919BD6509B15D05B1610DBD7121D58D41F9B56D29E9A56529E9AD713901C995),
    .INIT_48(256'hE55DD551CD49C949C949CD55D961ED7501911DAD3DD165F99129C15DF99935D5),
    .INIT_49(256'hC5115DADFD4DA1F549A1F951AD0965C52589E951B51D85ED59C535A51589F971),
    .INIT_4A(256'h31517191B5D901295179A5D501316195C9FD356DA5E11D5D99DD1D61A5ED317D),
    .INIT_4B(256'h4535291D110901F9F5F1F1F1F1F5F9FD050D15212D3D495D6D8195ADC5DDF915),
    .INIT_4C(256'h15D5996129F1B9855521F1C5996D4119F1CDA98565452509EDD1B9A18D796555),
    .INIT_4D(256'hB94DE57911AD49E58121C56509B15901A95501B16111C57931E5A15915D19151),
    .INIT_4E(256'h51B51981E955C12D9D0D7DF165D951CD45C141BD41C145C951D961ED79099529),
    .INIT_4F(256'hF125598DC5013D79B5F53979BD054D95DD2979C51569BD1169C11975D12D8DED),
    .INIT_50(256'hADB1B5B9C1C9D1DDE9F909192D41597189A5C1E1FD2141658DB1DD05315D8DBD),
    .INIT_51(256'hA4754519EDC59D75512D09E9CDAD9579614935210DFDF1E1D5CDC1BDB5B1B1AD),
    .INIT_52(256'hE48424C46408AC54FCA85400B06010C47830E8A45C1CD8985C20E4AC743C08D4),
    .INIT_53(256'h8CF864D040B020940880F870EC6CE868EC70F47C048C18A834C858EC8018B04C),
    .INIT_54(256'hB0E41C5490CC084888CC1058A0E83480CC1C70C0186CC42078D83494F858C024),
    .INIT_55(256'h606064686C74808894A4B4C4D8EC041C34506C8CACCCF0183C6490BCE818487C),
    .INIT_56(256'hB884501CF0C094684018F4D0AC8C7050341C04ECD8C4B4A4948880746C686460),
    .INIT_57(256'hC85CF48C28C46000A044E88C34E08C38E89848FCB46820DC985818D89C6024EC),
    .INIT_58(256'hA40464C82890F860C834A41484F86CE45CD450D04CD050D45CE46CF88410A034),
    .INIT_59(256'h648CB4E00C3C6C9CD0043C74B0E82868A8EC3078C00854A0F04094E83C94EC48),
    .INIT_5A(256'h1C0CFCF0E4D8D0CCC8C4C4C4C4C8D0D8E0ECF808182C40546C84A0BCDCFC1C40),
    .INIT_5B(256'hD8904804C07C3CFCC0844C14E0AC784818ECC09870482404E0C4A48C70584430),
    .INIT_5C(256'hB430B030B438C048D05CEC780CA034C860FC9834D47818C0640CB86410C07024),
    .INIT_5D(256'hB8FC4488D4206CB80C5CB00860B81470D03094F85CC4309C0878E85CD044BC38),
    .INIT_5E(256'hFC081420304054687C94B0CCE808284C7498C0EC184878A8DC104880BCF83878),
    .INIT_5F(256'h90603004D8AC84603C18F8DCBCA488705C483828180C00F8F0ECE8E8E8ECF0F4),
    .INIT_60(256'h8418B044E07818B458F89C44EC9440F0A05004B87028E0A05C1CE0A46830F8C4),
    .INIT_61(256'hE8409CF854B41478DC44AC1884F064D448C034B02CA828A82CB038C048D464F4),
    .INIT_62(256'hCCE80828486C90B8E00C386498C8FC346CA4E01C5CA0E0286CB800509CEC4094),
    .INIT_63(256'h402000E4C8B098806C5C4C403028201814101014141C20283440506070849CB4),
    .INIT_64(256'h50F4983CE49038E89848FCB06820D8985414D89C602CF4C08C5C3004D8B08864),
    .INIT_65(256'h1074D840AC1884F468DC50C840BC38B838BC44C850DC68F8881CB044DC7814B0),
    .INIT_66(256'h84ACD4FC285888B8EC245C94D00C4C90D01860A8F44090E03488DC3890EC4CAC),
    .INIT_67(256'hC0A894806C5C50403830282420202024283038445060708498B0C8E4001C4060),
    .INIT_68(256'hD47C28D48434E89C500CC4804000C4885018E0B07C4C20F4C8A07C583818F8DC),
    .INIT_69(256'hC02C980474E85CD048C43CBC3CBC40C44CD864F08010A438D06C04A440E4882C),
    .INIT_6A(256'h9CC8F4205084B8F028609CDC1C60A4E8307CC81464B80C64BC1470D03090F458),
    .INIT_6B(256'h6C58443424140C00F8F4F0F0F0F4F8FC04101C2C3C4C647890ACC8E808284C74),
    .INIT_6C(256'h40E89444F4A85C10C8844000C080480CD4A06C3C0CDCB488603C18F8D8B8A084),
    .INIT_6D(256'h1C84F060D040B42CA41C981898189C24AC34C050E070049C34D06C08A84CF098),
    .INIT_6E(256'h0C34608CBCEC20548CC400407CC0044890D82474C41468BC1470CC2888E84CB4),
    .INIT_6F(256'h1800ECD8C4B4A89C908C8480808084888C98A0ACBCCCE0F40C24405C7C9CC0E4),
    .INIT_70(256'h4CF49C44F4A05404BC702CE8A46424E8AC743C08D8A8784C20F8D4B08C6C5034),
    .INIT_71(256'hAC1078E44CBC2C9C1084FC78F470F070F47C048C1CA838CC60F49028C46404A8),
    .INIT_72(256'h40648CB4DC08346498CC003874B0EC2C70B4FC448CDC2878CC2078D02C88E848),
    .INIT_73(256'h14F4D8BCA4907C68584C40342C2824242424283038445060708498B0C8E40020),
    .INIT_74(256'h28C4680CB05800AC5C0CBC7028E0985414D4985C24ECB4845024F4CCA07C5834),
    .INIT_75(256'h80E03CA00468D038A41080F468DC54D04CC848CC50D860E878049828BC54F088),
    .INIT_76(256'h2C446080A0C4E810386490C0F0245C94CC084484C80C509CE43080D02478D028),
    .INIT_77(256'h24FCD8B4907054381C08F0DCCCBCB0A49C94909090909498A0ACB8C4D4E8FC10),
    .INIT_78(256'h7408A038D47414B458FCA450FCAC5C0CC07830ECA86828ECB078400CD8A87C50),
    .INIT_79(256'h1C70C41C74CC2C88EC4CB41884F05CCC3CB028A018941494149C20A834C050E0),
    .INIT_7A(256'h243444586C84A0BCD8F81C406890B8E4144478ACE41C5898D8185CA0E83480CC),
    .INIT_7B(256'h885424F4C89C704C2404E4C4A88C745C483828180C04FCF8F4F4F4F4FC000C18),
    .INIT_7C(256'h4CD864F08010A43CD46C08A848EC9034E08838E4984C00B8702CE8A86C30F4BC),
    .INIT_7D(256'h74BC04509CEC3C8CE43894EC4CAC0C70D43CA81480F064D850C840C03CC040C8),
    .INIT_7E(256'h000408101C24344454688098B0CCEC0C30547CA4D0FC2C5C90C4FC3870B0F030),
    .INIT_7F(256'hF0B07038FCC4905C2CFCD0A47C54300CECCCB0988068544434241C1008040000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized19
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h00000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000),
    .INITP_02(256'hF0000000000000000000007FFFFFFFFFFFFFFFFFFFFFFF800000000000000000),
    .INITP_03(256'h000000000007FFFFFFFFFFFFFFFE000000000000000001FFFFFFFFFFFFFFFFFF),
    .INITP_04(256'hFFFC0000000000001FFFFFFFFFFFFF800000000000003FFFFFFFFFFFFFFC0000),
    .INITP_05(256'h00000003FFFFFFFFFFE000000000003FFFFFFFFFFFC000000000001FFFFFFFFF),
    .INITP_06(256'h00000000FFFFFFFFFF80000000003FFFFFFFFFE00000000003FFFFFFFFFF8000),
    .INITP_07(256'hFFFFF8000000003FFFFFFFFE0000000007FFFFFFFFE0000000007FFFFFFFFF00),
    .INITP_08(256'hFFFFFFFE0000000001FFFFFFFFFC000000000FFFFFFFFFC000000000FFFFFFFF),
    .INITP_09(256'hFFFFFFFF800000000003FFFFFFFFFF80000000000FFFFFFFFFF80000000003FF),
    .INITP_0A(256'h0000000000007FFFFFFFFFFFF0000000000007FFFFFFFFFFF800000000000FFF),
    .INITP_0B(256'hFFFFC0000000000000007FFFFFFFFFFFFFF800000000000003FFFFFFFFFFFFF0),
    .INITP_0C(256'h000000000000001FFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFF),
    .INITP_0D(256'hFFFFF80000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFC000000),
    .INITP_0E(256'h000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0807070707060606060505050505040404040403030303030302020202020202),
    .INIT_01(256'h121211111010100F0F0F0E0E0E0D0D0D0C0C0C0B0B0B0A0A0A0A090909080808),
    .INIT_02(256'h21201F1F1E1E1D1D1C1C1C1B1B1A1A1919181817171716161515141414131312),
    .INIT_03(256'h3333323131302F2F2E2E2D2C2C2B2B2A29292828272626252524242323222221),
    .INIT_04(256'h4A49494847464645444343424140403F3E3D3D3C3B3B3A393938373736353534),
    .INIT_05(256'h656463636261605F5E5D5C5B5B5A5958575655555453525151504F4E4D4D4C4B),
    .INIT_06(256'h84838281807F7E7D7C7B7A79787776757473727170706F6E6D6C6B6A69686766),
    .INIT_07(256'hA8A7A6A4A3A2A1A09F9E9C9B9A9998979695939291908F8E8D8C8B8A89888786),
    .INIT_08(256'hCFCECDCCCAC9C8C6C5C4C3C1C0BFBEBCBBBAB9B7B6B5B4B3B1B0AFAEADABAAA9),
    .INIT_09(256'hFBFAF8F7F5F4F3F1F0EEEDECEAE9E8E6E5E3E2E1DFDEDDDBDAD9D7D6D5D3D2D1),
    .INIT_0A(256'h2B292826252322201F1D1C1A191716141311100E0D0B0A080705040201FFFEFD),
    .INIT_0B(256'h5F5D5C5A5857555352504E4D4B494846444341403E3C3B393836343331302E2D),
    .INIT_0C(256'h97959392908E8C8A8987858382807E7C7A7977757372706E6D6B696766646261),
    .INIT_0D(256'hD3D1CFCDCBC9C8C6C4C2C0BEBCBAB8B6B5B3B1AFADABA9A8A6A4A2A09E9C9B99),
    .INIT_0E(256'h13110F0D0B0907050301FFFDFBF9F7F5F3F1EFEDEBE9E7E5E3E1DFDDDBD9D7D5),
    .INIT_0F(256'h585553514F4D4B48464442403E3B39373533312F2D2A28262422201E1C1A1815),
    .INIT_10(256'hA09E9B99979492908E8B89878482807E7B79777572706E6C69676563605E5C5A),
    .INIT_11(256'hECEAE7E5E3E0DEDBD9D6D4D2CFCDCAC8C6C3C1BFBCBAB7B5B3B0AEACA9A7A5A2),
    .INIT_12(256'h3D3A383532302D2B282623211E1C191614110F0C0A07050300FEFBF9F6F4F1EF),
    .INIT_13(256'h918E8C898684817E7C797674716E6C696664615E5C595754514F4C4A4744423F),
    .INIT_14(256'hE9E6E4E1DEDBD8D6D3D0CDCAC8C5C2BFBDBAB7B4B2AFACA9A7A4A19F9C999694),
    .INIT_15(256'h4543403D3A3734312E2B282522201D1A1714110E0B09060300FDFAF7F5F2EFEC),
    .INIT_16(256'hA6A3A09C999693908D8A8784817E7B7875726F6C696663605D5A5754514E4B48),
    .INIT_17(256'h0A060300FDFAF7F3F0EDEAE7E4E1DDDAD7D4D1CECBC8C4C1BEBBB8B5B2AFACA9),
    .INIT_18(256'h726E6B6864615E5A5754514D4A4744403D3A3733302D2A2623201D1A1613100D),
    .INIT_19(256'hDDDAD6D3D0CCC9C5C2BFBBB8B4B1AEAAA7A4A09D9996938F8C8985827F7B7875),
    .INIT_1A(256'h4D4946423F3B3834312D2A26231F1C1815110E0A070300FCF9F5F2EEEBE8E4E1),
    .INIT_1B(256'hC0BCB9B5B2AEAAA7A39F9C9894918D8A86827F7B7874706D6966625F5B575450),
    .INIT_1C(256'h3733302C2824211D1915120E0A0603FFFBF7F4F0ECE9E5E1DEDAD6D2CFCBC7C4),
    .INIT_1D(256'hB2AEAAA6A29F9B97938F8B8784807C7874706C6965615D5956524E4A46433F3B),
    .INIT_1E(256'h302C2824201C1814110D090501FDF9F5F1EDE9E5E1DDD9D5D1CDC9C6C2BEBAB6),
    .INIT_1F(256'hB3AEAAA6A29E9A96928E8A85817D7975716D6965615D5955514D4945413D3834),
    .INIT_20(256'h3834302C27231F1B17120E0A0602FDF9F5F1EDE9E4E0DCD8D4D0CBC7C3BFBBB7),
    .INIT_21(256'hC2BDB9B5B0ACA8A39F9B96928E8985817D7874706B67635F5A56524E4945413D),
    .INIT_22(256'h4F4A46413D3834302B27221E1915110C0803FFFBF6F2EDE9E5E0DCD8D3CFCAC6),
    .INIT_23(256'hDFDAD6D1CDC8C4BFBBB6B2ADA8A49F9B96928D8984807B77726E6965605C5853),
    .INIT_24(256'h736E6A65605C57524E4944403B36322D29241F1B16120D0804FFFBF6F1EDE8E4),
    .INIT_25(256'h0A0601FCF7F2EEE9E4DFDBD6D1CCC8C3BEB9B5B0ABA7A29D98948F8A86817C78),
    .INIT_26(256'hA5A09B96928D88837E79746F6B66615C57524E49443F3A36312C27221D19140F),
    .INIT_27(256'h433E39342F2A25201B16110C0803FEF9F4EFEAE5E0DBD6D1CCC7C2BEB9B4AFAA),
    .INIT_28(256'hE5E0DAD5D0CBC6C1BCB7B2ADA8A39E99948E89847F7A75706B66615C57524D48),
    .INIT_29(256'h89847F7A756F6A65605B56504B46413C37312C27221D18130E0803FEF9F4EFEA),
    .INIT_2A(256'h312C27211C17120C0702FDF7F2EDE7E2DDD8D2CDC8C3BEB8B3AEA9A39E99948F),
    .INIT_2B(256'hDDD7D2CCC7C2BCB7B1ACA7A19C97918C87817C77716C66615C57514C47413C37),
    .INIT_2C(256'h8B85807A756F6A645F59544F49443E39332E28231E18130D0802FDF8F2EDE7E2),
    .INIT_2D(256'h3C37312C26201B15100A04FFF9F4EEE9E3DED8D3CDC7C2BCB7B1ACA6A19B9690),
    .INIT_2E(256'hF1EBE5E0DAD4CFC9C3BEB8B2ADA7A19C96918B85807A746F69645E58534D4742),
    .INIT_2F(256'hA8A39D97918B86807A746F69635D58524C46413B35302A241E19130D0802FCF6),
    .INIT_30(256'h635D57514B45403A342E28221D17110B05FFFAF4EEE8E2DCD7D1CBC5BFBAB4AE),
    .INIT_31(256'h201A140E0802FCF7F1EBE5DFD9D3CDC7C1BBB5AFA9A49E98928C86807A746F69),
    .INIT_32(256'hE0DAD4CEC8C2BCB6B0AAA49E98928C86807A746E68625C56504A443E38322C26),
    .INIT_33(256'hA49D97918B857F79736C66605A544E48423C362F29231D17110B05FFF9F3EDE7),
    .INIT_34(256'h69635D57514A443E38322B251F19130C0600FAF4EDE7E1DBD5CFC8C2BCB6B0AA),
    .INIT_35(256'h322C251F19130C0600F9F3EDE7E0DAD4CDC7C1BBB4AEA8A29B958F89827C7670),
    .INIT_36(256'hFDF7F1EAE4DDD7D1CAC4BEB7B1AAA49E97918B847E78716B655E58524B453F38),
    .INIT_37(256'hCBC5BEB8B1ABA59E98918B847E77716A645E57514A443D37312A241D17110A04),
    .INIT_38(256'h9C958F88827B756E67615A544D47403A332D262019130C06FFF9F2ECE5DFD8D2),
    .INIT_39(256'h6F68625B544E47403A332D261F19120C05FEF8F1EBE4DDD7D0CAC3BDB6AFA9A2),
    .INIT_3A(256'h443E373029231C150F0801FBF4EDE7E0D9D3CCC5BFB8B1ABA49D979089837C75),
    .INIT_3B(256'h1C150F0801FAF3EDE6DFD8D2CBC4BDB7B0A9A29C958E87817A736D665F58524B),
    .INIT_3C(256'hF6EFE9E2DBD4CDC6C0B9B2ABA49D979089827B746E676059524C453E37302A23),
    .INIT_3D(256'hD3CCC5BEB7B0A9A29B958E878079726B645D575049423B342D261F19120B04FD),
    .INIT_3E(256'hB2ABA49D968F88817A736C655E575049423B342D261F18110A03FCF5EFE8E1DA),
    .INIT_3F(256'h938B847D766F68615A534C453E373029221B140D06FFF8F1EAE3DCD5CEC7C0B9),
    .INIT_40(256'h040B12191F262D343B424950575D646B727980878E959BA2A9B0B7BEC5CCD3DA),
    .INIT_41(256'h2A30373E454C525960676E747B828990979DA4ABB2B9C0C6CDD4DBE2E9EFF6FD),
    .INIT_42(256'h52585F666D737A81878E959CA2A9B0B7BDC4CBD2D8DFE6EDF3FA01080F151C23),
    .INIT_43(256'h7C838990979DA4ABB1B8BFC5CCD3D9E0E7EDF4FB01080F151C232930373E444B),
    .INIT_44(256'hA9AFB6BDC3CAD0D7DDE4EBF1F8FE050C12191F262D333A40474E545B62686F75),
    .INIT_45(256'hD8DFE5ECF2F9FF060C131920262D333A40474D545A61676E757B82888F959CA2),
    .INIT_46(256'h0A11171D242A31373D444A51575E646A71777E848B91989EA5ABB1B8BEC5CBD2),
    .INIT_47(256'h3F454B52585E656B71787E848B91979EA4AAB1B7BEC4CAD1D7DDE4EAF1F7FD04),
    .INIT_48(256'h767C82898F959BA2A8AEB4BBC1C7CDD4DAE0E7EDF3F900060C13191F252C3238),
    .INIT_49(256'hB0B6BCC2C8CFD5DBE1E7EDF4FA00060C13191F252B32383E444A51575D636970),
    .INIT_4A(256'hEDF3F9FF050B11171D23292F363C42484E545A60666C73797F858B91979DA4AA),
    .INIT_4B(256'h2C32383E444A50565C62686E747A80868C92989EA4AAB0B6BCC2C8CED4DAE0E7),
    .INIT_4C(256'h6F747A80868C92989EA4A9AFB5BBC1C7CDD3D9DFE5EBF1F7FC02080E141A2026),
    .INIT_4D(256'hB4BABFC5CBD1D7DCE2E8EEF4FAFF050B11171D22282E343A40454B51575D6369),
    .INIT_4E(256'hFC02080D13191E242A30353B41464C52585D63696F747A80868B91979DA3A8AE),
    .INIT_4F(256'h474D53585E64696F747A80858B91969CA1A7ADB2B8BEC3C9CFD4DAE0E5EBF1F6),
    .INIT_50(256'h969BA1A6ACB1B7BCC2C7CDD3D8DEE3E9EEF4F9FF040A10151B20262C31373C42),
    .INIT_51(256'hE7EDF2F8FD02080D13181E23282E33393E44494F54595F646A6F757A80858B90),
    .INIT_52(256'h3C41474C51575C61666C71777C81878C91979CA1A7ACB1B7BCC2C7CCD2D7DDE2),
    .INIT_53(256'h94999EA3A9AEB3B8BEC3C8CDD2D8DDE2E7EDF2F7FD02070C12171C21272C3137),
    .INIT_54(256'hEFF4F9FE03080E13181D22272C31373C41464B50565B60656A6F757A7F84898F),
    .INIT_55(256'h4D52575C61666B70757A7F84898E94999EA3A8ADB2B7BCC1C6CBD0D5DAE0E5EA),
    .INIT_56(256'hAFB4B9BEC2C7CCD1D6DBE0E5EAEFF4F9FE03080C11161B20252A2F34393E4348),
    .INIT_57(256'h14191D22272C31363A3F44494E52575C61666B6F74797E83888D92969BA0A5AA),
    .INIT_58(256'h7C81868A8F94989DA2A7ABB0B5B9BEC3C8CCD1D6DBDFE4E9EEF2F7FC01060A0F),
    .INIT_59(256'hE8EDF1F6FBFF04080D12161B1F24292D32363B4044494E52575C60656A6E7378),
    .INIT_5A(256'h585C6065696E72777B8084898D92969B9FA4A8ADB2B6BBBFC4C8CDD1D6DADFE4),
    .INIT_5B(256'hCACFD3D8DCE0E5E9EDF2F6FBFF03080C1115191E22272B3034383D41464A4F53),
    .INIT_5C(256'h4145494E52565A5F63676B7074787D8185898E92969B9FA3A8ACB0B5B9BDC2C6),
    .INIT_5D(256'hBBBFC3C7CBD0D4D8DCE0E4E9EDF1F5F9FD02060A0E12171B1F23272C3034383D),
    .INIT_5E(256'h383D4145494D5155595D6165696D7175797D81858A8E92969A9EA2A6AAAEB3B7),
    .INIT_5F(256'hBABEC2C6C9CDD1D5D9DDE1E5E9EDF1F5F9FD0105090D1114181C2024282C3034),
    .INIT_60(256'h3F43464A4E5256595D6165696C7074787C8084878B8F93979B9FA2A6AAAEB2B6),
    .INIT_61(256'hC7CBCFD2D6DADEE1E5E9ECF0F4F7FBFF03060A0E1215191D2124282C3033373B),
    .INIT_62(256'h54575B5F6266696D7074787B7F82868A8D9194989C9FA3A7AAAEB2B5B9BCC0C4),
    .INIT_63(256'hE4E8EBEEF2F5F9FC0003070A0E1115181C1F23262A2D3134383B3F4246494D50),
    .INIT_64(256'h787B7F8285898C8F9396999DA0A4A7AAAEB1B4B8BBBFC2C5C9CCD0D3D6DADDE1),
    .INIT_65(256'h1013161A1D2023262A2D3033373A3D4044474A4D5154575A5E6164686B6E7275),
    .INIT_66(256'hACAFB2B5B8BBBEC1C4C8CBCED1D4D7DADDE1E4E7EAEDF0F3F7FAFD0003060A0D),
    .INIT_67(256'h4B4E5154575A5D606366696C6F7275787B7E8184878A8D909396999CA0A3A6A9),
    .INIT_68(256'hEFF2F5F7FAFD000306090B0E1114171A1D202225282B2E3134373A3D40434548),
    .INIT_69(256'h96999C9FA1A4A7A9ACAFB2B4B7BABDBFC2C5C8CACDD0D3D6D8DBDEE1E4E6E9EC),
    .INIT_6A(256'h4244474A4C4F515457595C5E616466696C6E717476797C7E818486898C8E9194),
    .INIT_6B(256'hF1F4F6F9FBFE000305070A0C0F111416191C1E212326282B2D303235383A3D3F),
    .INIT_6C(256'hA5A7A9ACAEB0B3B5B7BABCBFC1C3C6C8CACDCFD2D4D6D9DBDEE0E3E5E7EAECEF),
    .INIT_6D(256'h5C5E60636567696C6E70727577797B7E80828487898B8E90929497999B9EA0A2),
    .INIT_6E(256'h181A1C1E20222426282A2D2F31333537393B3E40424446484B4D4F515355585A),
    .INIT_6F(256'hD7D9DBDDDFE1E3E5E7E9EBEDEFF1F3F5F7F9FBFDFF01030507090B0D0F111315),
    .INIT_70(256'h9B9C9EA0A2A4A6A8A9ABADAFB1B3B5B6B8BABCBEC0C2C4C6C8C9CBCDCFD1D3D5),
    .INIT_71(256'h62646667696B6D6E7072737577797A7C7E8082838587898A8C8E909293959799),
    .INIT_72(256'h2E303133343638393B3C3E404143444648494B4D4E5052535557585A5C5D5F61),
    .INIT_73(256'hFEFF0102040507080A0B0D0E101113141617191A1C1D1F202223252628292B2D),
    .INIT_74(256'hD2D3D5D6D7D9DADBDDDEDFE1E2E3E5E6E8E9EAECEDEEF0F1F3F4F5F7F8FAFBFD),
    .INIT_75(256'hAAABADAEAFB0B1B3B4B5B6B7B9BABBBCBEBFC0C1C3C4C5C6C8C9CACCCDCECFD1),
    .INIT_76(256'h8788898A8B8C8D8E8F9091929395969798999A9B9C9E9FA0A1A2A3A4A6A7A8A9),
    .INIT_77(256'h6768696A6B6C6D6E6F70707172737475767778797A7B7C7D7E7F808182838486),
    .INIT_78(256'h4C4D4D4E4F5051515253545555565758595A5B5B5C5D5E5F6061626363646566),
    .INIT_79(256'h35353637373839393A3B3B3C3D3D3E3F40404142434344454646474849494A4B),
    .INIT_7A(256'h2222232324242525262627282829292A2B2B2C2C2D2E2E2F2F30313132333334),
    .INIT_7B(256'h131314141415151616171717181819191A1A1B1B1C1C1C1D1D1E1E1F1F202121),
    .INIT_7C(256'h08080909090A0A0A0A0B0B0B0C0C0C0D0D0D0E0E0E0F0F0F1010101111121212),
    .INIT_7D(256'h0202020202020303030303030404040404050505050506060606070707070808),
    .INIT_7E(256'h0000000000000000000000000000000000000000000001010101010101010102),
    .INIT_7F(256'h0101010101010101010000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized2
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h3878783C3C1E0F83E0F81F03F03F80FE03FE01FF801FFE0007FFFE0000000000),
    .INITP_01(256'h933333333319998CCC663319CE6318C6318E718E71C718E38F1C71E3870E1C3C),
    .INITP_02(256'h694B4B4B49692D25B6925B6DB6DB6DB6D924DB64DB26C9B364C9933664CC9999),
    .INITP_03(256'h555555555555AAAAA95556AAB556AA556A956A952A54AD5A94AD6A5294A5AD29),
    .INITP_04(256'h694B5AD6B5AD4A56A56A56AD5AB56A954AA552AA554AAA5555AAAAA955555555),
    .INITP_05(256'h3264D9366D9364DB24DB64926DB6DB6DB6DB6924B6D25B49692D2DA5A5A52D2D),
    .INITP_06(256'h9CE7398C67319CC6633199CCCE6667333333333333333366664CCD99B3264C99),
    .INITP_07(256'hC3C3C3C387870E1E3C70E1C78E1C71E38E38E38E38E39C71CE39C639C631CE73),
    .INITP_08(256'h739CE718C738C738E71C738E38E38E38E38F1C70E3C70E1C78F0E1C3C3878787),
    .INITP_09(256'h993264C99B336664CCCD9999999999999999CCCCE66733198CC67319CC6339CE),
    .INITP_0A(256'h2D69694B4B4B69692D25B496DA492DB6DB6DB6DB6C924DB649B64D936CD9364C),
    .INITP_0B(256'h55555555552AAAAB5554AAA554AA954AA552AD5AB56AD4AD4AD4A56B5AD6B5A5),
    .INITP_0C(256'h2D296B4A5294AD6A52B56A54A952AD52AD54AAD55AAAD5552AAAAB5555555555),
    .INITP_0D(256'h933332664CD993264D9B26C9B64DB64936DB6DB6DB6DB492DB49692D25A5A5A5),
    .INITP_0E(256'h387870E1C38F1C71E38E31C71CE31CE318C6318CE73198CC6663333199999999),
    .INITP_0F(256'h000000000000FFFFC000FFF003FF00FF80FE03F81F81F03E0F83E0F078783C3C),
    .INIT_00(256'hA0978E867E766E675F58524B453F3A342F2A26211D191613100D0A0806040302),
    .INIT_01(256'h4C3B291908F8E7D8C8B9AA9B8C7E706255473A2E211509FDF2E7DCD1C7BDB3AA),
    .INIT_02(256'h04EAD1B79E866D553D250EF7E0C9B39C87715C46321D09F5E1CDBAA79482705E),
    .INIT_03(256'hC8A684634120FFDEBE9E7E5F3F2001E3C5A7896B4E3115F8DCC0A4896E53391E),
    .INIT_04(256'h986E4319F0C69D744B23FBD3AB835C350FE8C29C77522D08E3BF9B7754300DEB),
    .INIT_05(256'h74410EDCAA784615E4B3825222F2C394653607D9AB7E5023F6C99D714519EEC3),
    .INIT_06(256'h5B1FE4AA6F35FBC1884F16DDA56D35FDC68F5821EBB57F4A14DFAB76420EDAA7),
    .INIT_07(256'h4C08C5823FFDBA7837F5B47332F2B27232F2B37436F7B97B3E01C3874A0ED296),
    .INIT_08(256'h47FCB0651ACF843AF0A65C13CA8139F0A86119D28B44FEB8722CE7A25D18D490),
    .INIT_09(256'h4DF8A551FEAA5705B3600FBD6C1BCA7929D9893AEA9B4DFEB06214C77A2DE094),
    .INIT_0A(256'h5BFEA246EB8F34D97E24CA7016BD640BB35A02AA53FCA54EF7A14BF5A04BF6A1),
    .INIT_0B(256'h710CA844E07C19B653F08E2CCA6807A645E48424C46505A648E98B2DCF7214B7),
    .INIT_0C(256'h8F22B549DD71059A2FC459EF851BB148DF760EA53DD66E07A039D36C06A13BD6),
    .INIT_0D(256'hB33EC955E06CF885129E2CB947D563F1800F9E2EBE4EDE6FFF9022B345D769FC),
    .INIT_0E(256'hDD60E366EA6DF176FA7F04890F951BA127AE35BD44CC54DD65EE77008A149E28),
    .INIT_0F(256'h0C86017CF873EF6BE864E15EDC59D755D453D151D050D050D051D253D556D85B),
    .INIT_10(256'h3EB02396097DF165D94EC237AD22980E84FB72E960D84FC840B831AA249D1791),
    .INIT_11(256'h72DD48B31E89F561CD3AA61380EE5CCA38A61584F363D343B323940576E85ACC),
    .INIT_12(256'hA80B6DD03497FB5FC3278CF156BB2187ED54BA2189F058C02890F962CB359E08),
    .INIT_13(256'hDE3993EE49A5015DB91572CF2C89E745A30260BF1F7EDE3E9EFE5FC02182E446),
    .INIT_14(256'h1365B80B5EB2065AAE0257AC0157AC0259AF065DB40C63BB136CC51E77D02A84),
    .INIT_15(256'h458FDA2571BC0854A0ED3A87D42270BE0C5BA9F84897E73787D8297ACB1D6EC0),
    .INIT_16(256'h72B5F83B7FC3074B8FD4195EA4E92F76BC034A91D82068B0F8418AD31D66B0FA),
    .INIT_17(256'h9AD5104C88C3003C79B6F3306EACEA2867A6E52464A4E42465A6E7286AABED30),
    .INIT_18(256'hBBEE225589BDF2275B91C6FC32689ED50C437AB2E9215A92CB043D77B1EB255F),
    .INIT_19(256'hD3FE2A5682AFDB08366391BFED1B4A79A8D707376797C8F92A5B8DBEF0235588),
    .INIT_1A(256'hE004284C7196BBE0062B51789EC5EC133B628AB3DB042D567FA9D3FD27527CA7),
    .INIT_1B(256'hE1FD193653708EACCAE80625446382A2C2E20223446586A8C9EB0D30537699BC),
    .INIT_1C(256'hD3E8FD12283D536A8097ADC5DCF40B233C546D869FB9D3EC07213C57728DA9C5),
    .INIT_1D(256'hB6C3D0DEECFA09182736455565758596A6B8C9DAECFE102335485B6F8296AABF),
    .INIT_1E(256'h868C92989FA6ADB4BCC3CBD4DCE5EEF7000A141E28323D48535F6B76838F9CA8),
    .INIT_1F(256'h42413F3E3E3D3D3D3D3D3E3F4041434547494B4E5154575B5F63676C70757B80),
    .INIT_20(256'hE8DFD7CFC7BFB7B0A9A29B958F89837D78736E6965615D5956524F4D4A484644),
    .INIT_21(256'h7666564737281A0BFDEFE1D3C6B8AB9F92867A6E62574C41362B21170D04FAF1),
    .INIT_22(256'hE9D2BBA48E78624C37210CF8E3CFBAA6937F6C594634210FFDEBDAC9B8A79686),
    .INIT_23(256'h3F2103E6C8AB8E7154381C00E4C9AE93785D43290FF5DCC2A990785F472F1800),
    .INIT_24(256'h76512C08E3BF9B7754300DEAC8A583613F1DFCDBBA9978583818F9D9BA9B7C5E),
    .INIT_25(256'h8C603409DDB2875D3208DEB48A61380FE6BD956D451DF6CEA7805A330DE7C19C),
    .INIT_26(256'h7E4B19E6B482501FEDBC8B5A2AFAC99A6A3A0BDCAD7F5022F4C6996C3F12E5B9),
    .INIT_27(256'h4A10D79D642CF3BB834B13DBA46D36FFC9935D27F1BC86511DE8B4804C18E5B1),
    .INIT_28(256'hECAC6C2CECAD6D2EF0B17234F6B87B3D00C3864A0ED1955A1EE3A86D32F8BE83),
    .INIT_29(256'h631CD68F4903BD7732ECA7631ED995510DCA864300BD7B38F6B47231F0AE6D2D),
    .INIT_2A(256'hAC5F11C4782BDF9246FBAF6418CD8338EEA35910C67D34EBA25911C98139F2AA),
    .INIT_2B(256'hC4701CC97623D07D2BD98635E39140EF9E4EFDAD5D0DBD6E1FD08132E49648FA),
    .INIT_2C(256'hA84EF49A41E78E35DC842BD37B23CB741DC66F18C26C16C06A15BF6A15C16C18),
    .INIT_2D(256'h56F69535D67616B758F99A3CDE7F21C46609AC4FF29539DD8125CA6E13B85D03),
    .INIT_2E(256'hCB64FE9731CB66009B36D16C08A43FDC7814B14EEB8825C361FF9D3BDA7918B7),
    .INIT_2F(256'h03962ABD51E57A0EA338CD62F78D22B84FE57B12A940D76F079E36CF67009831),
    .INIT_30(256'hFC8917A532C14FDD6CFB8A19A838C857E87808992ABB4CDE6F019325B84ADD70),
    .INIT_31(256'hB33AC24AD25AE26BF47C068F18A22CB640CA55E06BF6810C9824B03CC955E26F),
    .INIT_32(256'h25A628AA2CAF31B437BA3DC144C84CD055D95EE368ED73F87E048A10971EA52C),
    .INIT_33(256'h4ECA46C23FBB38B532B02DAB29A725A422A1209F1F9E1E9E1E9E1E9F20A122A3),
    .INIT_34(256'h2BA2188F067DF46CE35BD34BC33CB42DA61F99128C0680FA74EF6AE560DB56D2),
    .INIT_35(256'hBA2B9C0E7FF162D446B82B9D1083F66ADD51C438AC21950A7FF469DE54C93FB5),
    .INIT_36(256'hF763CF3AA6137FEB58C5329F0C7AE755C332A00E7DEC5BCA39A91989F969D94A),
    .INIT_37(256'hDF45AC1279E047AE157DE54CB41D85ED56BF2891FA64CE37A10C76E04BB6218C),
    .INIT_38(256'h6ECF3092F355B7197BDD40A20568CB2E92F559BD2186EA4FB3187DE248AD1379),
    .INIT_39(256'hA1FD59B6126FCB2885E2409DFB59B71573D2308FEE4DAD0C6CCB2B8BEC4CAD0D),
    .INIT_3A(256'h75CC237AD22981D93189E23A93EC459EF751AA045EB8126DC7227DD8338EEA45),
    .INIT_3B(256'hE6388ADC2F82D5287BCE2275C91D71C51A6EC3186DC2176DC2186EC41A71C71E),
    .INIT_3C(256'hF03D8BD92674C3115FAEFC4B9AE93988D82877C81868B9095AABFC4D9FF04294),
    .INIT_3D(256'h90D9226BB4FE4791DA246EB9034D98E32E79C40F5BA6F23E8AD6236FBC0956A3),
    .INIT_3E(256'hC3084C91D51A5FA4E92F74BAFF458BD2185EA5EC337AC1085097DF276FB7FF48),
    .INIT_3F(256'h85C5054585C6064788C90A4B8CCE0F5193D517599CDE2164A7EA2D70B4F83B7F),
    .INIT_40(256'h5F1AD5914C08C37F3BF8B4702DEAA76421DE9C5917D593510FCE8C4B0AC98847),
    .INIT_41(256'h47FEB46B22D99048FFB76F27DF975008C17A33ECA55E18D28B45FFBA742FE9A4),
    .INIT_42(256'hC37426D98B3DF0A35609BC6F23D68A3EF2A65B0FC4792EE3984D03B96E24DA91),
    .INIT_43(256'hD5822FDC8A38E69442F09F4DFCAB5A09B96818C87728D88839E99A4BFCAE5F11),
    .INIT_44(256'h8129D27A23CC751EC7711AC46E18C26D17C26D18C36E1AC5711DC97522CE7B28),
    .INIT_45(256'hCB6F12B659FDA145EA8E33D87D22C76D12B85E04AA51F79E45EC933AE28931D9),
    .INIT_46(256'hB755F39230CF6E0DAD4CEC8B2BCB6C0CAD4DEE8F30D27315B759FB9D40E28528),
    .INIT_47(256'h47E07912AC45DF7913AD48E27D18B34FEA8621BD59F5922ECB6805A240DD7B19),
    .INIT_48(256'h7F13A63ACF63F78C21B64BE0760CA137CE64FA9128BF56ED851DB44CE57D15AE),
    .INIT_49(256'h62F17F0E9C2BBA4AD969F98919A939CA5BEC7D0EA032C355E77A0C9F32C558EB),
    .INIT_4A(256'hF47D068F18A22BB53FC954DE69F47F0A9521AC38C451DD6AF683109D2BB846D4),
    .INIT_4B(256'h38BB3FC246CA4ED256DB60E56AEF74FA80068C12991FA62DB43CC34BD35BE36C),
    .INIT_4C(256'h31AF2CAA28A625A322A1209F1E9E1E9E1E9E1F9F20A122A425A729AB2DB032B5),
    .INIT_4D(256'hE25AD24AC23AB32CA51E97108A047EF873ED68E35ED955D04CC844C13DBA37B4),
    .INIT_4E(256'h4FC132A51789FC6FE255C93CB024980C81F66BE055CA40B62CA2188F067CF46B),
    .INIT_4F(256'h7AE551BD2A960370DD4AB82593016FDE4CBB2A990878E857C838A8198AFB6CDD),
    .INIT_50(256'h66CB3197FE64CB31980067CF369E076FD740A9127BE54FB8228DF762CD38A30E),
    .INIT_51(256'h1676D63595F656B71879DA3B9DFF61C32588EB4EB11478DC3FA4086CD1369B00),
    .INIT_52(256'h8EE7419AF44EA8035DB8136ECA2581DD3995F24FAC0966C4217FDE3C9AF958B7),
    .INIT_53(256'hD02376C91C70C4186CC1156ABF156AC0166CC2186FC61D74CB237BD32B84DC35),
    .INIT_54(256'hDF2B78C4115FACFA4896E43281D01F6EBD0D5DADFD4E9EEF4091E33586D92B7D),
    .INIT_55(256'hBD03498FD61C63AAF23981C91159A2EB347DC61059A3EE3883CD1864AFFB4692),
    .INIT_56(256'h6DADEC2C6CACEC2D6DAEF03172B4F6387BBD004386CA0D5195D91E63A7EC3277),
    .INIT_57(256'hF32C649DD7104A83BEF8326DA8E31E5A95D10E4A86C3003D7BB8F63472B1F02E),
    .INIT_58(256'h5082B4E6194B7EB1E5184C80B4E81D5186BCF1275D93C9FF366DA4DB134B83BB),
    .INIT_59(256'h87B2DD0934608CB9E5123F6C99C6F422507FADDC0B3A6A9AC9FA2A5A8BBCED1F),
    .INIT_5A(256'h9BBFE3082C51769CC1E70D335A80A7CEF61D456D95BDE60F38618AB4DE08325D),
    .INIT_5B(256'h8EABC8E603213F5E7C9BBAD9F91838587899BADBFC1D3F6183A5C8EA0D305477),
    .INIT_5C(256'h62788EA4BBD2E900182F475F7890A9C2DCF50F29435D7893AEC9E4001C385471),
    .INIT_5D(256'h1A2837475666768696A7B8C9DAEBFD0F213446596C7F93A6BACFE3F80C21374C),
    .INIT_5E(256'hB7BFC7CFD7DFE8F1FA040D17212B36414C57626E7A86929FABB8C6D3E1EFFD0B),
    .INIT_5F(256'h3D3D3E3E3F41424446484A4D4F5256595D6165696E73787D83898F959BA2A9B0),
    .INIT_60(256'hADA69F98928C86807B75706C67635F5B5754514E4B4947454341403F3E3D3D3D),
    .INIT_61(256'h09FAECDED0C3B6A89C8F83766B5F53483D32281E140A00F7EEE5DCD4CBC3BCB4),
    .INIT_62(256'h533D2812FDE8D3BFAA96826F5B48352310FEECDAC9B8A6968575655545362718),
    .INIT_63(256'h8E70533619FDE1C5A98D72573C2107ECD3B99F866D543C230BF4DCC5AD97806A),
    .INIT_64(256'hBB96714C2804E0BC997653300DEBC9A88665442302E2C2A28263442506E8CAAC),
    .INIT_65(256'hDBAF82562AFED3A77C5227FDD3A97F562D04DBB38A623B13ECC59E78512B06E0),
    .INIT_66(256'hF2BD895522EEBB885523F0BE8D5B2AF9C897673707D7A8794A1BEDBF91633608),
    .INIT_67(256'h00C3884C10D59A5F25EBB1773D04CB925A21E9B27A430CD59E6832FCC6915B27),
    .INIT_68(256'h07C37F3BF8B57230EDAB6A28E7A66524E4A46424E5A66728EAAC6E30F3B6793C),
    .INIT_69(256'h08BC7125DA8F45FAB0661DD38A41F8B06820D8914A03BC762FE9A45E19D48F4B),
    .INIT_6A(256'h06B25E0BB86513C06E1DCB7A29D88737E79748F8A95B0CBE7022D4873AEDA054),
    .INIT_6B(256'h01A549EE9339DE842AD0771EC56C13BB630CB45D06AF5902AC5701AC5702AE5A),
    .INIT_6C(256'hFB9734D06D0BA846E48221C05FFE9E3EDE7E1FBF6002A345E7892CCF7215B95D),
    .INIT_6D(256'hF5891EB348DD72089E35CB62F99028C058F08921BA54ED8721BB56F18C27C35F),
    .INIT_6E(256'hF17D099623B03ECC5AE876059423B343D363F38415A638CA5CEE8013A63ACD61),
    .INIT_6F(256'hEF73F87C01860C91179D24AA31B840C84FD860E972FB840E9822AD37C24ED965),
    .INIT_70(256'hF16DEA66E360DD5BD856D553D251D050D050D051D153D455D759DC5EE164E86B),
    .INIT_71(256'hF86CE055C93EB3289E148A0077EE65DD54CC44BD35AE27A11B950F89047FFA76),
    .INIT_72(256'h0571DD49B5228FFC69D745B32290FF6FDE4EBE2E9E0F80F163D547B92C9E1285),
    .INIT_73(256'h197CE044A80C71D63BA1066CD339A0076ED63DA50E76DF48B11B85EF59C42F9A),
    .INIT_74(256'h348FEB46A2FE5BB71472CF2D8BE948A60565C42484E445A60768CA2C8EF053B6),
    .INIT_75(256'h57AAFE51A5F84DA1F64BA0F54BA1F74EA5FC53AA025AB30B64BD1670CA247ED9),
    .INIT_76(256'h84CF1A65B0FC4794E02D7AC71462B0FE4D9BEA3A89D92979CA1B6CBD0F60B305),
    .INIT_77(256'hBAFD3F82C5084C90D4185DA2E72C72B8FE448BD21961A8F03981CA135CA6F03A),
    .INIT_78(256'hFB356FAAE41F5B96D20E4A87C3013E7BB9F73674B3F23272B2F23273B4F53778),
    .INIT_79(256'h4678AADC0E4174A7DA0E4276ABDF144A7FB5EB21588FC6FD356DA5DD164F88C1),
    .INIT_7A(256'h9DC6F019436E98C3EE1945719DC9F623507EABD907366594C3F2225282B3E415),
    .INIT_7B(256'hFF20416384A6C8EB0D3054779BBFE3082D52779CC2E80F355C83ABD3FB234B74),
    .INIT_7C(256'h6D869EB7D1EA041E39536E89A4C0DCF815314E6B89A7C5E301203F5F7E9EBEDE),
    .INIT_7D(256'hE7F80819293B4C5E708294A7BACDE1F5091D32465C71879CB3C9E0F70E253D55),
    .INIT_7E(256'h6E767E868E97A0AAB3BDC7D1DCE7F2FD0915212E3A475562707E8C9BAAB9C8D8),
    .INIT_7F(256'h0100000000000102030406080A0D101316191D21262A2F343A3F454B52585F67),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized20
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hD660066AACE01C956C7FC6D5B381E4AA47039A5A63F8CAA99FF1B559820CD56C),
    .INITP_01(256'h9559820ED25B800C956CFFE6556C7F8DAAD8FF195533FF1B56CE0792AB380335),
    .INITP_02(256'h55338073514CFFE24ADB800CD55987864AB67C79955B1FE32D69C0064AA67039),
    .INITP_03(256'h7E192AB33FF196B4CF9F325698FF8C95247CF9B553380E25548E079B54987C32),
    .INITP_04(256'hFF192A923C0F36AA99C01CDAAB31FF8DA5A4E00392AA4E1C19A56D87F8C9569C),
    .INITP_05(256'hB3810392552CC3F872575261FF1DB55267C1E66AA931FF8CD2B48E00736AA4C7),
    .INITP_06(256'hC6495524E3FFC765AA5B1C003992AADB1F07CCD2A9271FF0CDAAA4CF00F32555),
    .INITP_07(256'h5569338FFF0E6DA92B6C707E0E64B55A4CE020399A554B33C00799A554931F8F),
    .INITP_08(256'hAB4DC7FF8E495524C7E3F192554B33C00799A554B338080E64B55A4CE0FC1C6D),
    .INITP_09(256'h9C3F8669549381039B55499E01E64AAB661FF1C92A9667C1F1B6AA93380071B4),
    .INITP_0A(256'h32AAD9E07892A931FFC64AAD9C00E25A9663FF192AACCF07CC955B71FF0C95D4),
    .INITP_0B(256'hE65AD31FF99AA930FC72D5263FC36D4B3070E4AA93800E4B4B63FF19AAB67007),
    .INITP_0C(256'hA48FFE65159C039954987C3255B3C0E25548E039955B3E7C495263FE32D499F3),
    .INITP_0D(256'h526003B496E0833553381CCAA4C0072D698FF1B5533C7CDAA4C3C335566003B6),
    .INITP_0E(256'h6D52700E6AACC00CD7598039AA93C0E6D5B1FF995531FE36AB63FC6D54CFFE6D),
    .INITP_0F(256'h7F8DAA4C0064AB63FC6D566083355B1FF32AA63F8CB4B381C4AA4F039B56C7FC),
    .INIT_00(256'h20C060400000E000004080C0208000A040E0A08060606060A0C02060E060E080),
    .INIT_01(256'h20A040E0A0806040406080C00060C020C04000A0806040404060A0E020800080),
    .INIT_02(256'h20A020C06000E0A0A0A0A0C0E02060C040C040E0A06040202020206080E040A0),
    .INIT_03(256'h2080E06000A04000E0C0C0C0C0E02060C020A020C0804000E0C0C0E0002060C0),
    .INIT_04(256'h0040A0008000A04000E0A0A0A0A0C0004080E060E08020C0A06060406060A0E0),
    .INIT_05(256'hC0E02080E040C06000C080604040406080C00060E060E08020E0C0A0A0A0A0C0),
    .INIT_06(256'h00206080E040A020C06000C0A080808080A0E02080E060E08040E0C0A08080A0),
    .INIT_07(256'hE000004080C02080008020E080604020204060A0E02080008020C08040200000),
    .INIT_08(256'h4020404080A0E040C020C04000C0806040406080A0E040A0008020C080402000),
    .INIT_09(256'hC0A0A0A0C0E00060A0208020A06020E0C0A0A0A0C0004080E060E06000C08060),
    .INIT_0A(256'h6020000000204080C02080008020C08040202020204060A00060C040E08020E0),
    .INIT_0B(256'hC080604040406080C00060C040E06020E0A080606080A0C00060C020A040E0A0),
    .INIT_0C(256'hE0A060402020204080A00060C040E06020E0A0808080A0C0E02080E060E06020),
    .INIT_0D(256'h6000C0A080606080A0C00060C040C040E0A06040202020406080E020A000A020),
    .INIT_0E(256'h01A1612101E1E1E1012160A00060E06000C0604020000000206080E040A020C0),
    .INIT_0F(256'h8121E1A1614141416181A1E141A121A121C181412101E101012161A10161E161),
    .INIT_10(256'hA14101C1816141416181A1E12181018101A16121E1C1C1C1C1E12161A1018101),
    .INIT_11(256'h21C1612101C1C1C1C1E10141A1E161E16101A1614101010101214181E141A121),
    .INIT_12(256'hA141E1A161414141416181C12161E161E18121E1C1A1818181A1C10161A121A1),
    .INIT_13(256'hC16101C1A181818181A1E12161C121A141E181412101E1E1E1014181C1218101),
    .INIT_14(256'h41E1A16141210121216181C12181018121C1612101E1E1E1E1214181E141A121),
    .INIT_15(256'hA24202E2C2C2C2C2E20242A20262E28202C2824222020202226181E141A121A1),
    .INIT_16(256'hA262220202E202224282C22282028222C2824222020202224282C20282E26202),
    .INIT_17(256'hC2A28262626282C2E242A2028202A24202C28282626282A2E22262C242C242E2),
    .INIT_18(256'hC2A28282A2C2E22282E242C242E2824222E2C2C2C2E2024282E242A222C26202),
    .INIT_19(256'h020202224282C22282E26202A26202E2C2A2A2C2C2024282E242C242C28222E2),
    .INIT_1A(256'h434363A3E32383038303A34303E3A3A38383A3C30343830262E26202C2824222),
    .INIT_1B(256'hE30343A30363E38303C363430303E3E3032363A3E343C323C36303C383634343),
    .INIT_1C(256'h83C323A323A34303C383634343436383A30343A3238323C36323E3C3A3A3A3C3),
    .INIT_1D(256'h83038323C383432303E3E303034383C32383038303A36323E3C3C3A3C3C30323),
    .INIT_1E(256'hA444E4A484644444446484C40463C343C343E3834323E3E3C3C3E3032363C323),
    .INIT_1F(256'h24E4C4C4C4C4E4044484E444C444C46404C4A4644444446484A4E444A4048404),
    .INIT_20(256'h8484A4C4E42484E444C444E48424E4C4A4848484A4C4044484E464C46404A444),
    .INIT_21(256'h4484C42484048424C4642404C4C4A4A4C4E40444A40464C464E4844404C4A484),
    .INIT_22(256'hE565E58525C5856525250525254585C50565C444C46404A4642404E4E4E40424),
    .INIT_23(256'hA5652505E5E5E5E5052565A50565C545C56505C5854525252525456585C52585),
    .INIT_24(256'h2525254565A5E545A5058505A54505C585654545454585A5E52585E565E56505),
    .INIT_25(256'hA6E646A5058525C56525E5A58585658585A5E52565C525A525A54505C5854545),
    .INIT_26(256'h8626C6662606E6C6C6C6C6E60646A6E646C646C66606A6664626060606264666),
    .INIT_27(256'h26060606264666A60646A626A626C66626E6A68686666686A6C60646A60686E6),
    .INIT_28(256'hE62686E646C646E68626E6C6866666666686A6E62666C626A626A64606A66646),
    .INIT_29(256'h27C7672707C7C7A7A7C7C7072767C70787E76707A74707C787674746466686A6),
    .INIT_2A(256'h2727276787C70767C747A747C76727E7A787674747676787C70747A707870787),
    .INIT_2B(256'h27A727A747E7A7672707E7C7C7E7E7274787C72787078707A747E7A767472727),
    .INIT_2C(256'hA88868688888A8E80868A80888E86808A84808C8A8686848486887A7E72767C7),
    .INIT_2D(256'hC82888E868E88828E88868280808E80808284888C80868C848C848E8884808C8),
    .INIT_2E(256'hC8A88868686888A8C8084888E848A828C848E8A86828E8C8C8A8A8C8E8082868),
    .INIT_2F(256'h2969C949A949C96909C98949290909E90909296989C92989E969E96909A94808),
    .INIT_30(256'hC9A9A9898989A9C9092989C92989098909A94909C989492929090929496989C9),
    .INIT_31(256'h4AAA2AAA2ACA6909C989694929292929496989C90969C9298909A929C9894909),
    .INIT_32(256'h8A8A8AAACAEA2A6AAA0A6AEA4AEA6A0AAA6A2AEACAAA8A8A8A8AAACA0A4A8AEA),
    .INIT_33(256'hEA8A2AEA8A6A2A0AEAEACAEAEA0A2A6AAAEA4AAA0A8A0AAA2ACA8A4A0ACAAA8A),
    .INIT_34(256'hABEB2B8BCB4BAB2BAB2BCB6B2BEBAB8B4B4B2A2A2A4A6A8ACA0A4AAA0A6AEA6A),
    .INIT_35(256'hEBCBCBABABCBCBEB0B4B8BCB2B8BEB6BEB8B0BAB6B0BCBAB6B4B4B4B4B4B6B8B),
    .INIT_36(256'hEB6B0BAB4B0BCB8B6B4B4B2B2B4B4B6BABCB0B6BAB0B8BEB6B0B8B2BEB8B4B2B),
    .INIT_37(256'hACEC2C8CEC4CCC4CCC6C0CAC6C2CECAC8C6C6C6C6C8C8CCCEC2C6CAC0C6CEC6C),
    .INIT_38(256'h6C6C6C8C8CCCEC2C6CAC0C6CEC4CCC6CEC8C2CECAC6C4C2C0CECECEC0C0C4C6C),
    .INIT_39(256'h4D0DCDAD8D6D6D4D6D6D8DADCD0D4D8DED4CAC0C8C0CAC4CEC8C4C0CCCAC8C6C),
    .INIT_3A(256'h8D0DAD2DED8D4D0DCDAD8D6D6D6D6D6D8DADED0D4DADED4DAD2DAD2DAD4DED8D),
    .INIT_3B(256'h2E6ECE4EAE2DCD4DED8D2DEDAD6D4D2D0D0DED0D0D2D4D6D8DCD0D6DCD2D8D0D),
    .INIT_3C(256'h2E6EAEEE2E8EEE4EAE2EAE4ECE6E2ECE8E4E0EEECEAEAEAEAEAECEEE0E4E8ECE),
    .INIT_3D(256'hEE0E2E4E6E8ECE0E6ECE2E8EEE6EEE8E0EAE4E0ECE8E4E2E0EEECECECEEEEE0E),
    .INIT_3E(256'h6F6F6F6F6F8FAFCF0F4F8FCF2F8FEF4FCF4FCF6F0FAF4F0FCF8F6E2E0E0EEEEE),
    .INIT_3F(256'hAF8F8F6F6F6F6F8FAFCF0F2F6FCF0F6FCF2FAF2FAF2FCF6F0FAF6F2FEFCFAF8F),
    .INIT_40(256'hEE8E2EEEAE6E2E0EEEEECECECEEE0E2E4E8ECE0E4EAE0E8EEE6EEE8E2ECE6E0E),
    .INIT_41(256'hCE2EAE4ECE6E2ECE8E4E0EEECEAEAEAEAEAECEEE0E4E8ECE2E6ECE4EAE2EAE4E),
    .INIT_42(256'h4D8DED2DAD0D8D0D8D2DCD6D0DCD8D6D4D2D0D0DED0D0D2D4D6DAEEE2E8EEE4E),
    .INIT_43(256'h6D6D8DADCD0D4D8DED4DAD2DAD2DAD4DEDAD4D0DEDAD8D6D6D6D6D6D8DADCD0D),
    .INIT_44(256'hECCC8C8C6C6C6C6C8CACCC0C4C8CED4DAD0D8D0DAD4DED8D4D0DCDAD8D6D6D4D),
    .INIT_45(256'hCC4CEC8C2CECAC6C4C0C0CECECEC0C2C4C6CACEC2C8CEC6CCC4CEC6C0CAC6C2C),
    .INIT_46(256'hCC0C4CAC0C6CEC6CEC6C0CAC6C2CECCC8C8C6C6C6C6C8CACEC2C6CAC0C6CCC4C),
    .INIT_47(256'hCBCBABABCBCBEB2B4B8BEB2B8B0B6BEB8B0BAB6B0BCBAB6B4B4B2B2B4B4B6B8C),
    .INIT_48(256'hAB4BCB8B2BEBAB8B6B4B4B4B4B4B6BABCB0B6BAB0B8BEB6BEB8B2BCB8B4B0BEB),
    .INIT_49(256'h2A6A8AEA2A8AEA6AEA6A0AAA4A0BCB8B6B4B2B2B2B4B4B8BABEB2B6BCB2BAB2B),
    .INIT_4A(256'h2AEACAAA8A8A8A8AAACA0A4A8ACA2AAA0A8A0AAA4AEAAA6A2A0AEAEACAEAEA0A),
    .INIT_4B(256'h6ACA2AAA2AAA4AEA8A4A0ACAAA8A8A8A8AAACAEA2A6AAA0A6AEA4AEA6A0AAA6A),
    .INIT_4C(256'hA9898989A9A9C9094989C929A9098929C96909C989694929292A2A4A6A8ACA0A),
    .INIT_4D(256'hC949A949C96929C989694929090929294989C90949A90989098929C9892909C9),
    .INIT_4E(256'h8869696989A9C90949A90969E969E98929C98969290909E90909294989C90969),
    .INIT_4F(256'h88E868E88828C8682808E8C8A8A8C8C8E82868A8E848C828A848E8884808C8A8),
    .INIT_50(256'hA88888686888A8C8084888E848C848C86808C88848280808E80808286888E828),
    .INIT_51(256'hA7E747A727A828C86828E8A8886848486868A8C80848A80868E88808A86808E8),
    .INIT_52(256'h07C7876727272727274767A7E747A70787078727C7874727E7E7C7C7E7072767),
    .INIT_53(256'hC7C7072767C72787078707A74707C787676747476787A7E72767C747A747C767),
    .INIT_54(256'h46C646E68727E7A787674747476787C70747A70767E78707C7672707C7C7A7A7),
    .INIT_55(256'h664626060606264666A60646A626A626C66626E6A6866666666686C6E62686E6),
    .INIT_56(256'hE6062666C62686E68606A64606C6A68666668686A6E62666C626A626A64606A6),
    .INIT_57(256'h268606A646E6A6664626060606264666A60666C646C646E6A64606E6C6C6C6C6),
    .INIT_58(256'hE5A56545252525454585C50545A525A525C56525E5A58585658585A5E62666C6),
    .INIT_59(256'hE5E5E5052565A50565E565E58525E5A585454545456585C50545A5058505A545),
    .INIT_5A(256'h85C52585E565E58525C5856545252525254585C50565C545C56505A5652505E5),
    .INIT_5B(256'h84048424C484442404E4E4E4042565A50565C545C56505C58545252505252565),
    .INIT_5C(256'h8424E4C4A4848484A4C4044484E464C46404A44404E4C4A4A4C4C4042464C424),
    .INIT_5D(256'hE4C4C4C4C4E42444A40464C464E4844404C4A4848484A4C4E42484E444C444E4),
    .INIT_5E(256'h446484A4E444A4048404A444E4A4846444444464A4C40464C444C444E4844404),
    .INIT_5F(256'h4383C32383038323C3632303E3C3C3E3E3234484E444C444C46404C484644444),
    .INIT_60(256'h43A323A323C3832303C3C3A3C3C3E32363A30383038323C383430303E3E30323),
    .INIT_61(256'hE36303A34303E3C3A3A3A3C3E32363C3238323A34303A383634343436383C303),
    .INIT_62(256'h8323E3A363434343436383C30363C323C343E3A3632303E3E303034363C30383),
    .INIT_63(256'hC2824222020202224383C30363E36303834303C3A38383A3A3E30343A3038303),
    .INIT_64(256'hE2C2A28282A2C2E22282C242C242E2824202C2C2A2A2C2E20262A20262E28222),
    .INIT_65(256'h8262626282A2C20262C222A242E2824202E2C2C2C2E2224282E242C242E28222),
    .INIT_66(256'h02E202022262A2E242C242C26222E2A28262628282C20242A2028202A242E2C2),
    .INIT_67(256'hC2C2C2E20242A20262E28202C2824222020202224282C22282028222C2824222),
    .INIT_68(256'h01214161A1E142A222A242E2826222020202224282C20282E26202A24202E2C2),
    .INIT_69(256'h8181A1C10161C121A141E1814121E1E1E1E1012161C12181018121C181612121),
    .INIT_6A(256'h414161A1E141A1018121C1814101E1E1E101214181E141A121C16121E1A18181),
    .INIT_6B(256'hC1C1012161C121A121A16101C1A1818181A1C1E12181E161E16121C181614141),
    .INIT_6C(256'h416181C10141A121A141E1814121010101014161A10161E161E1A14101E1C1C1),
    .INIT_6D(256'h414161A1E12181018101A16121E1C1C1C1C1E12161A10181018121E1A1816141),
    .INIT_6E(256'hE1E1012161A10161E16101A161210101E101214181C121A121A141E1A1816141),
    .INIT_6F(256'h606080A0C00060C020A040E0806020000000204060C10161E16101A1612101E1),
    .INIT_70(256'h2020204060A0E020A000A020E08060402020204060A0E040C040C06000C0A080),
    .INIT_71(256'h604040406080C02060E060E08020E0C0A0808080A0E02060E040C06000A08040),
    .INIT_72(256'h40200000002060A0E040A020C06000C0A080606080A0E02060E040C06000C080),
    .INIT_73(256'h00E0C0A0A0A0C0E02080E040C06000A06040202020204080C02080008020C080),
    .INIT_74(256'hE0A080404020406080C00060E060E0804000C0A0A0A0C0E02060A0208020A060),
    .INIT_75(256'h20C080400000E000204080C0208000A040E0A0806040406080C00040C020C040),
    .INIT_76(256'hA040E0806020000000204080C02080008020E0A060402020406080E020800080),
    .INIT_77(256'hC040E08020E0C0A08080A0C0E04080E060E08020E0A080808080A0C00060C020),
    .INIT_78(256'hA0008000A04000C0A0A0A0A0C0E02080E060E06000C080604040406080C00060),
    .INIT_79(256'h40A00060E08020E0A06060406060A0C02080E060E0804000C0A0A0A0A0E00040),
    .INIT_7A(256'hE00060C020A020C0602000E0C0C0E0004080C020A020C06020E0C0C0C0C0E000),
    .INIT_7B(256'h6080A0E040A020A040E08060202020204060A0E040C040C06020E0C0A0A0A0A0),
    .INIT_7C(256'hE000004060C02080008020E0A0604040406080A00040C020C06000C080604040),
    .INIT_7D(256'h80808080A0E02080E060E06020C0A06060606080A0E040A0008020C080400000),
    .INIT_7E(256'h4020000000204080E020A020A04000C08060606080A0E02080E060E08020E0C0),
    .INIT_7F(256'h20E0A080808080C0E02080E060E08020E0A08060606080C00040A020A020E080),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized21
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hFFF00000007FFFFFFFE0000000001FFFFFFFFFFFFC0000000000000000000000),
    .INITP_01(256'h8000FFFE0003FFFC0001FFFF00003FFFF00000FFFFF000007FFFFF0000007FFF),
    .INITP_02(256'h00FFC00FFE007FF001FFC007FF001FFE001FFE001FFF0007FFC001FFF8001FFF),
    .INITP_03(256'h7F807F807F807F807F803FC03FE01FF007F803FE00FF801FF007FE00FFC00FFC),
    .INITP_04(256'h07F03F81FC0FE07F01F80FE03F80FC07F01FC03F80FE03FC07F00FE01FC03FC0),
    .INITP_05(256'hFC0F81F03F07E07C0FC1F81F81F03F03F03F03F03F03F03F03F81F81FC0FC0FE),
    .INITP_06(256'h83E0F83E0F83E0F83E07C1F07C1F03E0F83F07C1F83E07C1F83E07C0F81F07E0),
    .INITP_07(256'hE1F0F87C1E0F0783E1F0783C1F0783E1F0783E0F07C1F0F83E0F07C1F07C1F0F),
    .INITP_08(256'hF83E0F83E0F83E0F83E1F07C1F07C1E0F83E1F07C1E0F83C1F0F83C1F0783C1F),
    .INITP_09(256'hE07C0FC1F81F03E07E0FC1F03E07C0F83F07C0F83F07C1F83E0F81F07C1F07C0),
    .INITP_0A(256'h01FC0FE07F03F81FC0FE07E07F03F03F81F81F81F81F81F81F81F81F03F03F07),
    .INITP_0B(256'hFC03FC03FC03FC03FC07F807F00FE01FC07F80FE03F807F01FC07E03F80FE03F),
    .INITP_0C(256'h001FFC00FFE007FE007FE007FE00FFC01FF003FE00FF803FC01FF00FF807F803),
    .INITP_0D(256'h007FFF8000FFFE0003FFF0003FFF0007FFC001FFF000FFF000FFF001FFC007FF),
    .INITP_0E(256'hFFFFFFFC0000001FFFFFFC000001FFFFFC00001FFFFE00001FFFF80001FFFF00),
    .INITP_0F(256'h00000000000000000000000000000000000000007FFFFFFFFFFFF0000000000F),
    .INIT_00(256'h49474543413F3C3B39373533312F2E2C2A2827252422211F1E1C1B1A18171615),
    .INIT_01(256'hA09D9A9693908D8A8784817E7C797673706E6B686663615E5C59575452504E4B),
    .INIT_02(256'h1914100C0804FFFBF7F3EFEBE7E4E0DCD8D4D1CDC9C6C2BFBBB8B4B1ADAAA7A3),
    .INIT_03(256'hB3ADA8A39E98938E89847F7A75706B66615D58534E4A45413C37332E2A26211D),
    .INIT_04(256'h6E68615B554F48423C36302A241E18120C0600FBF5EFEAE4DED9D3CEC8C3BDB8),
    .INIT_05(256'h4B433C352D261F18110902FBF4EDE6DFD8D1CBC4BDB6AFA9A29B958E88817B74),
    .INIT_06(256'h49413830271F170F06FEF6EEE6DED6CEC6BEB6AEA69E978F8780787069615A52),
    .INIT_07(256'h685F564C433930271E140B02F9F0E7DED5CCC3BAB1A89F978E857C746B635A52),
    .INIT_08(256'hA99F948A7F756A60564C41372D23190F05FBF1E7DDD3C9BFB6ACA2988F857C72),
    .INIT_09(256'h0BFFF4E8DDD1C6BBAFA4998E82776C61564B40352A1F1409FEF4E9DED4C9BEB4),
    .INIT_0A(256'h8E8175685C4F43362A1E1105F9EDE0D4C8BCB0A4988C8074695D5145392E2216),
    .INIT_0B(256'h31241609FBEEE0D3C5B8AB9D908376695B4E4134271A0D00F4E7DACDC0B4A79A),
    .INIT_0C(256'hF6E7D9CABCAD9F9082736557483A2C1E1001F3E5D7C9BBAD9F928476685A4D3F),
    .INIT_0D(256'hDBCCBCAC9D8D7E6E5F4F4031211203F4E4D5C6B7A8998A7B6C5D4E4031221305),
    .INIT_0E(256'hE1D0C0AF9F8E7D6D5D4C3C2B1B0BFAEADACABAAA9A8A7A6A5A4A3A2A1A0AFBEB),
    .INIT_0F(256'h07F6E4D2C1AF9E8C7B69584635241301F0DFCEBDAC9B897968574635241302F2),
    .INIT_10(256'h4E3B291603F1DECCB9A79482705D4B39271402F0DECCBAA8968472604E3D2B19),
    .INIT_11(256'hB5A18D7A66523F2B1804F1DECAB7A4907D6A5744311E0BF8E5D2BFAC99867361),
    .INIT_12(256'h3B2712FDE9D4C0AB97826E5945311C08F4E0CCB7A38F7B67533F2B1804F0DCC8),
    .INIT_13(256'hE2CCB7A18B76604B35200AF5E0CAB5A08B75604B36210CF7E2CDB8A38E7A6550),
    .INIT_14(256'hA8917B644E37210AF4DDC7B09A846E57412B15FFE9D2BCA6907B654F39230DF8),
    .INIT_15(256'h8E765F472F1800E9D1BAA38B745D452E1700E9D2BBA48D765F48311A03ECD6BF),
    .INIT_16(256'h937A6249311800E7CFB69E866D553D250CF4DCC4AC947C644C341C05EDD5BDA6),
    .INIT_17(256'hB79D846A51371E04EBD2B89F866D533A2108EFD6BDA48B725940270EF6DDC4AB),
    .INIT_18(256'hFADFC5AA90755B41260CF2D7BDA3896F553A2006ECD2B99F856B51371E04EAD1),
    .INIT_19(256'h5C402509EED2B79C80654A2F13F8DDC2A78C71563B2005EACFB59A7F644A2F14),
    .INIT_1A(256'hDCBFA3866A4E3115F9DDC0A4886C503418FCE0C4A88C7055391D01E6CAAE9377),
    .INIT_1B(256'h7A5D3F2205E7CAAD907255381BFEE1C4A78A6D503317FADDC0A4876A4E3115F8),
    .INIT_1C(256'h3618FADBBD9F8163442608EACCAE9072543719FBDDBFA28466492B0DF0D2B597),
    .INIT_1D(256'h10F1D2B39374553617F8D9BA9B7C5D3E2001E2C3A58667492A0BEDCEB0917355),
    .INIT_1E(256'h08E8C7A78767472707E7C7A78868482808E9C9A98A6A4A2B0BECCCAD8E6E4F30),
    .INIT_1F(256'h1DFBDAB99877563615F4D3B29171502F0FEECDAD8C6C4B2B0AEAC9A989694828),
    .INIT_20(256'h4E2C0AE8C6A583613F1DFCDAB89775533210EFCDAC8A69482605E4C2A1805F3E),
    .INIT_21(256'h9D7A573411EFCCA98664411FFCD9B794724F2D0BE8C6A4815F3D1BF9D7B49270),
    .INIT_22(256'h07E4C09C7955310EEAC7A3805C3915F2CFAB8865411EFBD8B5926F4B2805E3C0),
    .INIT_23(256'h8E6A4521FCD7B38E6A4621FDD9B4906C4723FFDBB7936F4B2703DFBB97734F2B),
    .INIT_24(256'h310CE6C19B76502B06E1BB96714C2701DCB7926D4823FED9B4906B4621FCD8B3),
    .INIT_25(256'hEFC9A37C56300AE3BD97714B25FFD9B38D67411BF5CFAA845E3813EDC7A27C57),
    .INIT_26(256'hC9A27A532C05DEB79069421BF4CDA67F59320BE4BE97704A23FDD6B089633C16),
    .INIT_27(256'hBD956D451DF5CDA57E562E06DEB78F674018F0C9A17A522B03DCB58D663F17F0),
    .INIT_28(256'hCCA37A512900D7AE865D340CE3BB926A4119F0C8A0774F27FED6AE865E350DE5),
    .INIT_29(256'hF5CBA2784F25FBD2A87F552C03D9B0875D340BE2B88F663D14EBC29970471EF5),
    .INIT_2A(256'h380EE3B98E643A0FE5BB90663C12E8BD93693F15EBC1976D431AF0C69C72491F),
    .INIT_2B(256'h95693E13E8BC91663B10E5BA8F64390EE3B88D63380DE2B88D62370DE2B88D63),
    .INIT_2C(256'h0ADEB2865A2E02D6AB7F5327FBCFA4784C21F5C99E72471BF0C4996D4217EBC0),
    .INIT_2D(256'h996C3F12E6B98C603306DAAD815427FBCFA276491DF1C4986C3F13E7BB8F6336),
    .INIT_2E(256'h4012E5B78A5C2F01D4A7794C1EF1C497693C0FE2B5885A2D00D3A6794C20F3C6),
    .INIT_2F(256'hFFD1A2744618E9BB8D5F3103D5A7794A1DEFC193653709DBAD805224F7C99B6E),
    .INIT_30(256'hD6A778491AEBBC8D5E2F00D1A3744516E8B98A5C2DFED0A1734416E7B98A5C2D),
    .INIT_31(256'hC494653505D5A6764617E7B8885929FACA9B6B3C0DDDAE7F4F20F1C292633405),
    .INIT_32(256'hC999683807D7A7764616E5B5855424F4C494643303D3A3734313E3B3845424F4),
    .INIT_33(256'hE5B4835120EFBE8D5C2BFAC998673605D4A3734211E0B07F4E1DEDBC8C5B2AFA),
    .INIT_34(256'h17E5B3814F1EECBA885725F3C2905E2DFBCA98673504D2A16F3E0DDBAA794716),
    .INIT_35(256'h5E2CF9C794622FFDCB98663301CF9C6A3806D4A16F3D0BD9A7754311DFAD7B49),
    .INIT_36(256'hBB885522EFBC885522EFBC895623F0BD8A5725F2BF8C5926F4C18E5C29F6C491),
    .INIT_37(256'h2DF9C6925E2AF6C38F5B28F4C08D5925F2BE8B5724F0BD895623EFBC895522EF),
    .INIT_38(256'hB47F4B16E2AD794510DCA8733F0BD6A26E3A06D19D693501CD996531FDC99561),
    .INIT_39(256'h4E19E4AF7A4510DBA6713C07D29D6833FECA95602BF7C28D5824EFBB86511DE8),
    .INIT_3A(256'hFCC7915B25F0BA854F19E4AE79430ED8A36D3803CD98622DF8C38D5823EEB883),
    .INIT_3B(256'hBE87511BE4AE78420BD59F6933FDC7915B25EEB9834D17E1AB753F09D39E6832),
    .INIT_3C(256'h925B24EDB67F4812DBA46D3700C9935C25EFB8814B14DEA7713A04CD97612AF4),
    .INIT_3D(256'h784109D29A632BF4BD854E17DFA8713A03CB945D26EFB8814A12DBA46D36FFC9),
    .INIT_3E(256'h703800C8905820E8B1794109D199612AF2BA824B13DBA46C34FDC58E561FE7AF),
    .INIT_3F(256'h7A4109D0985F27EEB67D450DD49C642BF3BB824A12DAA16931F9C1895018E0A8),
    .INIT_40(256'h487FB6ED245B92C9FF366DA4DB124A81B8EF265D94CB033A71A8DF174E85BDF4),
    .INIT_41(256'h78AEE41B5187BEF42A6197CD043A71A7DE144B81B8EF255C93C900376DA4DB12),
    .INIT_42(256'hBAF0255B91C7FC32689ED3093F75ABE1174D83B9EE255B91C7FD33699FD50B42),
    .INIT_43(256'h10457AAFE4194E83B8EE23588DC3F82D6298CD03386DA3D80E4379AEE4194F85),
    .INIT_44(256'h79ADE2164B7FB4E81D5186BBEF24588DC2F72B6095CAFE33689DD2073C71A6DB),
    .INIT_45(256'hF62A5E92C6F92D6195C9FD316599CD0135699DD1063A6EA2D60B3F73A8DC1045),
    .INIT_46(256'h88BCEF225588BBEF225589BCEF235689BDF024578BBEF225598DC0F4285B8FC3),
    .INIT_47(256'h2F6294C7F92C5E91C4F6295C8EC1F426598CBFF225578ABDF0235689BCEF2255),
    .INIT_48(256'hEC1E4F81B3E517497BADDF114375A7D90B3D6FA1D406386A9CCF01336698CBFD),
    .INIT_49(256'hBEEF205183B4E5164779AADB0D3E6FA1D204356798CAFB2D5E90C2F3255788BA),
    .INIT_4A(256'hA7D707386899C9FA2A5B8CBCED1D4E7FB0E0114273A3D405366798C9FA2B5C8D),
    .INIT_4B(256'hA6D505356594C4F4245484B3E3134373A3D303336494C4F4245485B5E5164676),
    .INIT_4C(256'hBCEB1A4978A7D605346392C2F1204F7FAEDD0D3C6B9BCAFA295988B8E7174676),
    .INIT_4D(256'hE9184674A2D1FF2D5C8AB9E7164473A1D0FE2D5C8AB9E8164574A3D1002F5E8D),
    .INIT_4E(256'h2F5C8AB7E512406E9BC9F7245280ADDB09376593C1EF1D4A79A7D503315F8DBB),
    .INIT_4F(256'h8CB9E6123F6C99C6F3204C79A6D3002D5A88B5E20F3C6997C4F11E4C79A7D401),
    .INIT_50(256'h022E5A86B2DE0A36638FBBE7133F6C98C4F11D4976A2CFFB275481ADDA063360),
    .INIT_51(256'h91BCE8133E6995C0EB17426D99C4F01B47729EC9F5214C78A4CFFB27537FABD6),
    .INIT_52(256'h3A648EB9E30E38638DB8E20D37628DB8E20D38638DB8E30E39648FBAE5103B66),
    .INIT_53(256'hFB254F78A2CBF51F49729CC6F01A436D97C1EB153F6993BDE8123C6690BBE50F),
    .INIT_54(256'hD70029517AA3CCF51E477099C2EB143D668FB8E20B345D87B0D9032C557FA8D2),
    .INIT_55(256'hCDF51D456D95BDE50D355E86AED6FE274F77A0C8F019416A92BBE30C345D86AE),
    .INIT_56(256'hDE052C537AA2C9F0173F668DB5DC032B527AA1C9F01840678FB7DE062E567EA5),
    .INIT_57(256'h0A30567CA3C9EF163C6389B0D6FD234A7097BEE40B32597FA6CDF41B426990B7),
    .INIT_58(256'h50769BC1E60C31577CA2C7ED13385E84AACFF51B41678DB3D9FF254B7197BDE3),
    .INIT_59(256'hB3D7FC21456A8EB3D8FC21466B90B4D9FE23486D92B7DC01274C7196BBE1062B),
    .INIT_5A(256'h3155799CC0E4072B4F7397BBDF03274B6F93B7DBFF23476C90B4D9FD21466A8E),
    .INIT_5B(256'hCCEF1134577A9DC0E305284B6F92B5D8FB1E416588ABCFF215395C80A3C7EA0E),
    .INIT_5C(256'h83A5C6E80A2C4E7092B4D7F91B3D5F81A4C6E80B2D4F7294B7D9FC1F416486A9),
    .INIT_5D(256'h567798B9DAFB1D3E5F80A1C2E4052648698AACCDEF1032537597B8DAFC1D3F61),
    .INIT_5E(256'h476787A7C7E80828486989A9C9EA0A2B4B6C8CADCDEE0F2F507191B2D3F41536),
    .INIT_5F(256'h557493B3D2F110304F6E8EADCCEC0B2B4A6A8AA9C9E90828486888A7C7E70727),
    .INIT_60(256'h819FBDDBFA1836557391B0CEED0B2A496786A5C3E201203E5D7C9BBAD9F81736),
    .INIT_61(256'hCAE705223F5D7A97B5D2F00D2B496684A2BFDDFB1937547290AECCEA08264463),
    .INIT_62(256'h314E6A86A3BFDCF815314E6A87A4C0DDFA1733506D8AA7C4E1FE1B38557290AD),
    .INIT_63(256'hB7D2EE0925405C7793AECAE6011D3955708CA8C4E0FC1834506C88A4C0DDF915),
    .INIT_64(256'h5B7590AAC5DFFA142F4A647F9AB5CFEA05203B56718CA7C2DDF8132F4A65809C),
    .INIT_65(256'h1E37516A849DB7D1EA041E37516B859FB9D2EC06203A556F89A3BDD7F20C2641),
    .INIT_66(256'h00183149627A93ABC4DDF60E274059728BA4BDD6EF08213A536D869FB8D2EB04),
    .INIT_67(256'h00182F475F768EA6BDD5ED051C344C647C94ACC4DCF40C253D556D869EB6CFE7),
    .INIT_68(256'h21374E647B91A8BFD6EC031A31485F768DA4BBD2E900172E455D748BA3BAD1E9),
    .INIT_69(256'h60768BA1B7CCE2F80D23394F657B90A6BCD2E9FF152B41576E849AB0C7DDF40A),
    .INIT_6A(256'hC0D4E9FD12273B50657A8EA3B8CDE2F70C21364B60758BA0B5CAE0F50A20354B),
    .INIT_6B(256'h3F52667A8DA1B5C8DCF004182B3F53677B8FA3B7CCE0F4081C3145596E8297AB),
    .INIT_6C(256'hDEF10316293B4E61738699ACBFD2E5F80B1E3144576A7D90A4B7CADEF104182B),
    .INIT_6D(256'h9EAFC1D2E4F607192B3D4E60728496A8BACCDEF0021427394B5D708294A7B9CC),
    .INIT_6E(256'h7D8E9FAFC0D0E1F20213243546576879899BACBDCEDFF0011324354658697B8C),
    .INIT_6F(256'h7E8D9DACBCCCDBEBFB0A1A2A3A4A5A6A7A8A9AAABACADAEAFA0B1B2B3C4C5D6D),
    .INIT_70(256'h9FADBCCAD9E7F605132231404E5D6C7B8A99A8B7C6D5E4F403122131404F5F6E),
    .INIT_71(256'hE0EEFB091624313F4D5A687684929FADBBC9D7E5F301101E2C3A485765738290),
    .INIT_72(256'h434F5C6875818E9AA7B4C0CDDAE7F4000D1A2734414E5B697683909DABB8C5D3),
    .INIT_73(256'hC6D1DDE8F4FF0B16222E3945515D6974808C98A4B0BCC8D4E0EDF905111E2A36),
    .INIT_74(256'h6A757F8A949FA9B4BEC9D4DEE9F4FE09141F2A35404B56616C77828E99A4AFBB),
    .INIT_75(256'h3039434C565F68727C858F98A2ACB6BFC9D3DDE7F1FB050F19232D37414C5660),
    .INIT_76(256'h171F2730384149525A636B747C858E979FA8B1BAC3CCD5DEE7F0F9020B141E27),
    .INIT_77(256'h1F262D353C434B525A6169707880878F979EA6AEB6BEC6CED6DEE6EEF6FE060F),
    .INIT_78(256'h484F555B61686E747B81888E959BA2A9AFB6BDC4CBD1D8DFE6EDF4FB02091118),
    .INIT_79(256'h93989EA3A8ADB3B8BDC3C8CED3D9DEE4EAEFF5FB00060C12181E242A30363C42),
    .INIT_7A(256'hFF04080C1014191D21262A2E33373C41454A4E53585D61666B70757A7F84898E),
    .INIT_7B(256'h8D9093969A9DA0A3A7AAADB1B4B8BBBFC2C6C9CDD1D4D8DCE0E4E7EBEFF3F7FB),
    .INIT_7C(256'h3C3F41434547494B4E50525457595C5E616366686B6E707376797C7E8184878A),
    .INIT_7D(256'h0D0E0F10111214151617181A1B1C1E1F2122242527282A2C2E2F31333537393B),
    .INIT_7E(256'h000000000000000000000001010102020203030404050506070708090A0B0B0C),
    .INIT_7F(256'h141211100F0E0D0C0B0B0A090807070605050404030302020201010100000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized22
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h28235BC446B48772969111EDE227C3AEFB4877096EBEE1DF5DA451478A2096FB),
    .INITP_01(256'h878B78A77777728D69690EEBBEE53E2DF2227DA5BC4447B4B75DF70F146412D2),
    .INITP_02(256'hA5CA20DE1E5111106909608D775C8B7CB487B4B84B84FB44FB04FB04B84B8787),
    .INITP_03(256'hAC2213168B444272D116087913C229AEE962379443C2720E1116D60834B44C45),
    .INITP_04(256'h68B3DE6895A130B45D2F27BC0EE8841DEEF7443D2E9D845DD11634AC22D16886),
    .INITP_05(256'h4C90932F45D0B3E6B5DB24210B3668E5EF7A710BBF127A610B18C6845D167BDE),
    .INITP_06(256'h1E85F78F7D0999999990B9982E3DE3A17B6DBD18217B6F4217A2F45306248419),
    .INITP_07(256'h09E997D7AFA0A1433A8AE4146FAE617D0CCBD2514385F66FA16D0A285E3D9BD7),
    .INITP_08(256'h13333333217DE3DF42F1D7B378F428A16D0BECDF43851497A6617D0CEBEC504E),
    .INITP_09(256'hB75ACF9A1745E9921265304248C1945E8BD085EDBD08317B6DBD0B8F78E8333A),
    .INITP_0A(256'hE9745A190B522CF79A2CF7BCD17442C631A10CBC91FBA11CBDEF4E2CD9A10849),
    .INITP_0B(256'hD1169C8445A2D190886AC22D16886A58D117744372E97845DEEF70422EE07BC9),
    .INITP_0C(256'h212C111114F0F608A74B44645A5820D6D110E09C878453D88D2EEB2887913C20),
    .INITP_0D(256'h2D2D629DDDDDCA3DA3C3C3C3A43A41BE41BE45BE43A43A5BC25A7DA275DD620D),
    .INITP_0E(256'h12D29DC25AC447B5882896904C51E1DF75DA5BC4447B4B7C889F68F94EFBAEE1),
    .INITP_0F(256'hEE0DDDC3E44F877760EFBED208A3C5144B75F70EFAED21DC25BEEB87C88F6F11),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0101010101010101010101010101010101010100000000000000000000000000),
    .INIT_05(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_06(256'h0202020202020202020101010101010101010101010101010101010101010101),
    .INIT_07(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_08(256'h0303030303030303030303030303030303020202020202020202020202020202),
    .INIT_09(256'h0403030303030303030303030303030303030303030303030303030303030303),
    .INIT_0A(256'h0404040404040404040404040404040404040404040404040404040404040404),
    .INIT_0B(256'h0505050505050505050505050505050505050505050505050404040404040404),
    .INIT_0C(256'h0606060606060606060606060606060606060505050505050505050505050505),
    .INIT_0D(256'h0707070707070707070707070707070606060606060606060606060606060606),
    .INIT_0E(256'h0808080808080808080808080808070707070707070707070707070707070707),
    .INIT_0F(256'h0909090909090909090909090909090908080808080808080808080808080808),
    .INIT_10(256'h0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A09090909090909090909090909),
    .INIT_11(256'h0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0A0A0A0A0A0A0A0A0A),
    .INIT_12(256'h0D0D0D0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0B0B0B),
    .INIT_13(256'h0E0E0E0E0E0E0E0E0E0E0E0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D),
    .INIT_14(256'h0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0E0E0E0E0E0E0E0E0E0E0E0E0E),
    .INIT_15(256'h11111111111111101010101010101010101010101010101010101010100F0F0F),
    .INIT_16(256'h1212121212121212121212121212121212111111111111111111111111111111),
    .INIT_17(256'h1414141414141414131313131313131313131313131313131313131312121212),
    .INIT_18(256'h1515151515151515151515151515151515151515141414141414141414141414),
    .INIT_19(256'h1717171717171717171717171716161616161616161616161616161616161616),
    .INIT_1A(256'h1919191919191919181818181818181818181818181818181818181717171717),
    .INIT_1B(256'h1B1B1B1B1B1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A19191919191919191919),
    .INIT_1C(256'h1D1D1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1B1B1B1B1B1B1B1B1B1B1B1B1B),
    .INIT_1D(256'h1F1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1D1D1D1D1D1D1D1D1D1D1D1D1D1D),
    .INIT_1E(256'h21202020202020202020202020202020201F1F1F1F1F1F1F1F1F1F1F1F1F1F1F),
    .INIT_1F(256'h2322222222222222222222222222222222212121212121212121212121212121),
    .INIT_20(256'h2525252424242424242424242424242424242323232323232323232323232323),
    .INIT_21(256'h2727272727262626262626262626262626262626252525252525252525252525),
    .INIT_22(256'h2929292929292929282828282828282828282828282827272727272727272727),
    .INIT_23(256'h2B2B2B2B2B2B2B2B2B2B2B2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A292929292929),
    .INIT_24(256'h2E2E2D2D2D2D2D2D2D2D2D2D2D2D2D2D2C2C2C2C2C2C2C2C2C2C2C2C2C2B2B2B),
    .INIT_25(256'h303030303030302F2F2F2F2F2F2F2F2F2F2F2F2F2E2E2E2E2E2E2E2E2E2E2E2E),
    .INIT_26(256'h3232323232323232323232323131313131313131313131313130303030303030),
    .INIT_27(256'h3535353535343434343434343434343434343333333333333333333333333332),
    .INIT_28(256'h3737373737373737373737373636363636363636363636363535353535353535),
    .INIT_29(256'h3A3A3A3A3A3A3939393939393939393939393938383838383838383838383837),
    .INIT_2A(256'h3D3D3C3C3C3C3C3C3C3C3C3C3C3C3B3B3B3B3B3B3B3B3B3B3B3B3A3A3A3A3A3A),
    .INIT_2B(256'h3F3F3F3F3F3F3F3F3F3F3E3E3E3E3E3E3E3E3E3E3E3E3D3D3D3D3D3D3D3D3D3D),
    .INIT_2C(256'h4242424242424241414141414141414141414040404040404040404040403F3F),
    .INIT_2D(256'h4545454544444444444444444444444343434343434343434343434242424242),
    .INIT_2E(256'h4848474747474747474747474746464646464646464646464645454545454545),
    .INIT_2F(256'h4A4A4A4A4A4A4A4A4A4A4A4A4949494949494949494949484848484848484848),
    .INIT_30(256'h4D4D4D4D4D4D4D4D4D4D4D4C4C4C4C4C4C4C4C4C4C4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_31(256'h505050505050505050504F4F4F4F4F4F4F4F4F4F4F4E4E4E4E4E4E4E4E4E4E4E),
    .INIT_32(256'h5353535353535353535352525252525252525252525151515151515151515150),
    .INIT_33(256'h5656565656565656565655555555555555555555555454545454545454545453),
    .INIT_34(256'h5A59595959595959595959585858585858585858585857575757575757575757),
    .INIT_35(256'h5D5D5C5C5C5C5C5C5C5C5C5C5C5B5B5B5B5B5B5B5B5B5B5A5A5A5A5A5A5A5A5A),
    .INIT_36(256'h606060605F5F5F5F5F5F5F5F5F5F5E5E5E5E5E5E5E5E5E5E5D5D5D5D5D5D5D5D),
    .INIT_37(256'h6363636363636262626262626262626261616161616161616161606060606060),
    .INIT_38(256'h6666666666666666666565656565656565656564646464646464646463636363),
    .INIT_39(256'h6A6A696969696969696969696868686868686868686767676767676767676766),
    .INIT_3A(256'h6D6D6D6D6D6C6C6C6C6C6C6C6C6C6C6B6B6B6B6B6B6B6B6B6A6A6A6A6A6A6A6A),
    .INIT_3B(256'h7070707070707070706F6F6F6F6F6F6F6F6F6E6E6E6E6E6E6E6E6E6E6D6D6D6D),
    .INIT_3C(256'h7474747373737373737373737372727272727272727271717171717171717170),
    .INIT_3D(256'h7777777777777776767676767676767676757575757575757575747474747474),
    .INIT_3E(256'h7B7B7B7A7A7A7A7A7A7A7A7A7979797979797979797878787878787878787777),
    .INIT_3F(256'h7E7E7E7E7E7E7E7D7D7D7D7D7D7D7D7D7C7C7C7C7C7C7C7C7C7B7B7B7B7B7B7B),
    .INIT_40(256'h7373737374747474747474747475757575757575757576767676767676767676),
    .INIT_41(256'h7070707070707070717171717171717171727272727272727272737373737373),
    .INIT_42(256'h6C6C6D6D6D6D6D6D6D6D6D6E6E6E6E6E6E6E6E6E6E6F6F6F6F6F6F6F6F6F7070),
    .INIT_43(256'h69696969696A6A6A6A6A6A6A6A6A6A6B6B6B6B6B6B6B6B6B6C6C6C6C6C6C6C6C),
    .INIT_44(256'h6666666666666666676767676767676767676868686868686868686969696969),
    .INIT_45(256'h6263636363636363636363646464646464646464656565656565656565656666),
    .INIT_46(256'h5F5F5F6060606060606060606061616161616161616161626262626262626262),
    .INIT_47(256'h5C5C5C5C5C5D5D5D5D5D5D5D5D5D5D5E5E5E5E5E5E5E5E5E5E5F5F5F5F5F5F5F),
    .INIT_48(256'h5959595959595A5A5A5A5A5A5A5A5A5A5B5B5B5B5B5B5B5B5B5B5C5C5C5C5C5C),
    .INIT_49(256'h5656565656565657575757575757575757585858585858585858585859595959),
    .INIT_4A(256'h5353535353535353545454545454545454545555555555555555555555565656),
    .INIT_4B(256'h5050505050505050515151515151515151515252525252525252525252535353),
    .INIT_4C(256'h4D4D4D4D4D4D4D4E4E4E4E4E4E4E4E4E4E4E4F4F4F4F4F4F4F4F4F4F4F505050),
    .INIT_4D(256'h4A4A4A4A4A4A4A4B4B4B4B4B4B4B4B4B4B4B4C4C4C4C4C4C4C4C4C4C4D4D4D4D),
    .INIT_4E(256'h4747474747484848484848484848484849494949494949494949494A4A4A4A4A),
    .INIT_4F(256'h4444444545454545454545454545464646464646464646464646474747474747),
    .INIT_50(256'h4242424242424242424242424343434343434343434343434444444444444444),
    .INIT_51(256'h3F3F3F3F3F3F3F3F3F4040404040404040404040404141414141414141414141),
    .INIT_52(256'h3C3C3C3C3C3D3D3D3D3D3D3D3D3D3D3D3D3E3E3E3E3E3E3E3E3E3E3E3E3F3F3F),
    .INIT_53(256'h393A3A3A3A3A3A3A3A3A3A3A3A3B3B3B3B3B3B3B3B3B3B3B3B3C3C3C3C3C3C3C),
    .INIT_54(256'h3737373737373737383838383838383838383838393939393939393939393939),
    .INIT_55(256'h3434353535353535353535353535353636363636363636363636363737373737),
    .INIT_56(256'h3232323232323232333333333333333333333333333434343434343434343434),
    .INIT_57(256'h3030303030303030303030303030313131313131313131313131313232323232),
    .INIT_58(256'h2D2D2D2D2D2E2E2E2E2E2E2E2E2E2E2E2E2E2E2F2F2F2F2F2F2F2F2F2F2F2F2F),
    .INIT_59(256'h2B2B2B2B2B2B2B2B2B2B2C2C2C2C2C2C2C2C2C2C2C2C2C2D2D2D2D2D2D2D2D2D),
    .INIT_5A(256'h292929292929292929292929292A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2B2B2B2B),
    .INIT_5B(256'h2626272727272727272727272727272727282828282828282828282828282829),
    .INIT_5C(256'h2424242425252525252525252525252525252526262626262626262626262626),
    .INIT_5D(256'h2222222222222323232323232323232323232323232424242424242424242424),
    .INIT_5E(256'h2020202020202121212121212121212121212121212122222222222222222222),
    .INIT_5F(256'h1E1E1E1E1E1E1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F20202020202020202020),
    .INIT_60(256'h1C1C1C1C1C1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1E1E1E1E1E1E1E1E1E1E1E),
    .INIT_61(256'h1A1A1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1C1C1C1C1C1C1C1C1C1C1C1C),
    .INIT_62(256'h19191919191919191919191919191919191A1A1A1A1A1A1A1A1A1A1A1A1A1A1A),
    .INIT_63(256'h1717171717171717171717171818181818181818181818181818181818181819),
    .INIT_64(256'h1515151515151516161616161616161616161616161616161616171717171717),
    .INIT_65(256'h1414141414141414141414141414141414141415151515151515151515151515),
    .INIT_66(256'h1212121212121212121212131313131313131313131313131313131313131314),
    .INIT_67(256'h1111111111111111111111111111111111111111111112121212121212121212),
    .INIT_68(256'h0F0F0F0F0F0F0F0F0F0F10101010101010101010101010101010101010101010),
    .INIT_69(256'h0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0F0F0F0F0F0F0F0F0F0F0F0F),
    .INIT_6A(256'h0C0C0C0C0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0E0E0E0E),
    .INIT_6B(256'h0B0B0B0B0B0B0B0B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C),
    .INIT_6C(256'h0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B),
    .INIT_6D(256'h09090909090909090909090909090909090909090A0A0A0A0A0A0A0A0A0A0A0A),
    .INIT_6E(256'h0808080808080808080808080808080808080808080808090909090909090909),
    .INIT_6F(256'h0707070707070707070707070707070707070707070707070708080808080808),
    .INIT_70(256'h0606060606060606060606060606060606060606060606060707070707070707),
    .INIT_71(256'h0505050505050505050505050505050505050505050606060606060606060606),
    .INIT_72(256'h0404040404040404040404040404040505050505050505050505050505050505),
    .INIT_73(256'h0303030303030404040404040404040404040404040404040404040404040404),
    .INIT_74(256'h0303030303030303030303030303030303030303030303030303030303030303),
    .INIT_75(256'h0202020202020202020202020202020202020202020203030303030303030303),
    .INIT_76(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_77(256'h0101010101010101010101010101010101010101010101010101010101010202),
    .INIT_78(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_79(256'h0000000000000000000000000000000000000000010101010101010101010101),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized23
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h3870E1C3C3878783C3C1E0F83E0F81F03F03F80FE03FE01FF801FFE0007FFFE0),
    .INITP_01(256'h664CC9999933333333319998CCC663319CE6318C6318E718E71C718E38F1C71E),
    .INITP_02(256'h294A5AD29694B4B4B49692D25B6925B6DB6DB6DB6D924DB64DB26C9B364C9933),
    .INITP_03(256'h955555555555555555555AAAAA95556AAB556AA556A956A952A54AD5A94AD6A5),
    .INITP_04(256'h5A5A52D2D694B5AD6B5AD4A56A56A56AD5AB56A954AA552AA554AAA5555AAAAA),
    .INITP_05(256'h9B3264C993264D9366D9364DB24DB64926DB6DB6DB6DB6924B6D25B49692D2DA),
    .INITP_06(256'h9C631CE739CE7398C67319CC6633199CCCE6667333333333333333366664CCD9),
    .INITP_07(256'h783C3C3C3C3C3C3C387870E1E3C70E1C78E1C71E38E38E38E38E39C71CE39C63),
    .INITP_08(256'h9CC6339CE739CE718C738C738E71C738E38E38E38E38F1C70E3C70E1C78F0E1C),
    .INITP_09(256'h36CD9364C993264C99B336664CCCD9999999999999999CCCCE66733198CC6731),
    .INITP_0A(256'hB5AD6B5A52D69694B4B4B69692D25B496DA492DB6DB6DB6DB6C924DB649B64D9),
    .INITP_0B(256'h55555555555555555552AAAAB5554AAA554AA954AA552AD5AB56AD4AD4AD4A56),
    .INITP_0C(256'hD25A5A5A52D296B4A5294AD6A52B56A54A952AD52AD54AAD55AAAD5552AAAAB5),
    .INITP_0D(256'h199999999933332664CD993264D9B26C9B64DB64936DB6DB6DB6DB492DB49692),
    .INITP_0E(256'h078783C3C387870E1C38F1C71E38E31C71CE31CE318C6318CE73198CC6663333),
    .INITP_0F(256'h000000000000000000000FFFFC000FFF003FF00FF80FE03F81F81F03E0F83E0F),
    .INIT_00(256'h9482705E4C3B291908F8E7D8C8B9AA9B8C7E706255473A2E211509FDF2E7DCD1),
    .INIT_01(256'h6E53391E04EAD1B79E866D553D250EF7E0C9B39C87715C46321D09F5E1CDBAA7),
    .INIT_02(256'h54300DEBC8A684634120FFDEBE9E7E5F3F2001E3C5A7896B4E3115F8DCC0A489),
    .INIT_03(256'h4519EEC3986E4319F0C69D744B23FBD3AB835C350FE8C29C77522D08E3BF9B77),
    .INIT_04(256'h420EDAA774410EDCAA784615E4B3825222F2C394653607D9AB7E5023F6C99D71),
    .INIT_05(256'h4A0ED2965B1FE4AA6F35FBC1884F16DDA56D35FDC68F5821EBB57F4A14DFAB76),
    .INIT_06(256'h5D18D4904C08C5823FFDBA7837F5B47332F2B27232F2B37436F7B97B3E01C387),
    .INIT_07(256'h7A2DE09447FCB0651ACF843AF0A65C13CA8139F0A86119D28B44FEB8722CE7A2),
    .INIT_08(256'hA04BF6A14DF8A551FEAA5705B3600FBD6C1BCA7929D9893AEA9B4DFEB06214C7),
    .INIT_09(256'hCF7214B75BFEA246EB8F34D97E24CA7016BD640BB35A02AA53FCA54EF7A14BF5),
    .INIT_0A(256'h06A13BD6710CA844E07C19B653F08E2CCA6807A645E48424C46505A648E98B2D),
    .INIT_0B(256'h45D769FC8F22B549DD71059A2FC459EF851BB148DF760EA53DD66E07A039D36C),
    .INIT_0C(256'h8A149E28B33EC955E06CF885129E2CB947D563F1800F9E2EBE4EDE6FFF9022B3),
    .INIT_0D(256'hD556D85BDD60E366EA6DF176FA7F04890F951BA127AE35BD44CC54DD65EE7700),
    .INIT_0E(256'h249D17910C86017CF873EF6BE864E15EDC59D755D453D151D050D050D051D253),
    .INIT_0F(256'h76E85ACC3EB02396097DF165D94EC237AD22980E84FB72E960D84FC840B831AA),
    .INIT_10(256'hCB359E0872DD48B31E89F561CD3AA61380EE5CCA38A61584F363D343B3239405),
    .INIT_11(256'h2182E446A80B6DD03497FB5FC3278CF156BB2187ED54BA2189F058C02890F962),
    .INIT_12(256'h77D02A84DE3993EE49A5015DB91572CF2C89E745A30260BF1F7EDE3E9EFE5FC0),
    .INIT_13(256'hCB1D6EC01365B80B5EB2065AAE0257AC0157AC0259AF065DB40C63BB136CC51E),
    .INIT_14(256'h1D66B0FA458FDA2571BC0854A0ED3A87D42270BE0C5BA9F84897E73787D8297A),
    .INIT_15(256'h6AABED3072B5F83B7FC3074B8FD4195EA4E92F76BC034A91D82068B0F8418AD3),
    .INIT_16(256'hB1EB255F9AD5104C88C3003C79B6F3306EACEA2867A6E52464A4E42465A6E728),
    .INIT_17(256'hF0235588BBEE225589BDF2275B91C6FC32689ED50C437AB2E9215A92CB043D77),
    .INIT_18(256'h27527CA7D3FE2A5682AFDB08366391BFED1B4A79A8D707376797C8F92A5B8DBE),
    .INIT_19(256'h537699BCE004284C7196BBE0062B51789EC5EC133B628AB3DB042D567FA9D3FD),
    .INIT_1A(256'h728DA9C5E1FD193653708EACCAE80625446382A2C2E20223446586A8C9EB0D30),
    .INIT_1B(256'h8296AABFD3E8FD12283D536A8097ADC5DCF40B233C546D869FB9D3EC07213C57),
    .INIT_1C(256'h838F9CA8B6C3D0DEECFA09182736455565758596A6B8C9DAECFE102335485B6F),
    .INIT_1D(256'h70757B80868C92989FA6ADB4BCC3CBD4DCE5EEF7000A141E28323D48535F6B76),
    .INIT_1E(256'h4A48464442413F3E3E3D3D3D3D3D3E3F4041434547494B4E5154575B5F63676C),
    .INIT_1F(256'h0D04FAF1E8DFD7CFC7BFB7B0A9A29B958F89837D78736E6965615D5956524F4D),
    .INIT_20(256'hB8A796867666564737281A0BFDEFE1D3C6B8AB9F92867A6E62574C41362B2117),
    .INIT_21(256'h472F1800E9D2BBA48E78624C37210CF8E3CFBAA6937F6C594634210FFDEBDAC9),
    .INIT_22(256'hBA9B7C5E3F2103E6C8AB8E7154381C00E4C9AE93785D43290FF5DCC2A990785F),
    .INIT_23(256'h0DE7C19C76512C08E3BF9B7754300DEAC8A583613F1DFCDBBA9978583818F9D9),
    .INIT_24(256'h3F12E5B98C603409DDB2875D3208DEB48A61380FE6BD956D451DF6CEA7805A33),
    .INIT_25(256'h4C18E5B17E4B19E6B482501FEDBC8B5A2AFAC99A6A3A0BDCAD7F5022F4C6996C),
    .INIT_26(256'h32F8BE834A10D79D642CF3BB834B13DBA46D36FFC9935D27F1BC86511DE8B480),
    .INIT_27(256'hF0AE6D2DECAC6C2CECAD6D2EF0B17234F6B87B3D00C3864A0ED1955A1EE3A86D),
    .INIT_28(256'h8139F2AA631CD68F4903BD7732ECA7631ED995510DCA864300BD7B38F6B47231),
    .INIT_29(256'hE49648FAAC5F11C4782BDF9246FBAF6418CD8338EEA35910C67D34EBA25911C9),
    .INIT_2A(256'h15C16C18C4701CC97623D07D2BD98635E39140EF9E4EFDAD5D0DBD6E1FD08132),
    .INIT_2B(256'h13B85D03A84EF49A41E78E35DC842BD37B23CB741DC66F18C26C16C06A15BF6A),
    .INIT_2C(256'hDA7918B756F69535D67616B758F99A3CDE7F21C46609AC4FF29539DD8125CA6E),
    .INIT_2D(256'h67009831CB64FE9731CB66009B36D16C08A43FDC7814B14EEB8825C361FF9D3B),
    .INIT_2E(256'hB84ADD7003962ABD51E57A0EA338CD62F78D22B84FE57B12A940D76F079E36CF),
    .INIT_2F(256'hC955E26FFC8917A532C14FDD6CFB8A19A838C857E87808992ABB4CDE6F019325),
    .INIT_30(256'h971EA52CB33AC24AD25AE26BF47C068F18A22CB640CA55E06BF6810C9824B03C),
    .INIT_31(256'h20A122A325A628AA2CAF31B437BA3DC144C84CD055D95EE368ED73F87E048A10),
    .INIT_32(256'h60DB56D24ECA46C23FBB38B532B02DAB29A725A422A1209F1F9E1E9E1E9E1E9F),
    .INIT_33(256'h54C93FB52BA2188F067DF46CE35BD34BC33CB42DA61F99128C0680FA74EF6AE5),
    .INIT_34(256'hF969D94ABA2B9C0E7FF162D446B82B9D1083F66ADD51C438AC21950A7FF469DE),
    .INIT_35(256'h4BB6218CF763CF3AA6137FEB58C5329F0C7AE755C332A00E7DEC5BCA39A91989),
    .INIT_36(256'h48AD1379DF45AC1279E047AE157DE54CB41D85ED56BF2891FA64CE37A10C76E0),
    .INIT_37(256'hEC4CAD0D6ECF3092F355B7197BDD40A20568CB2E92F559BD2186EA4FB3187DE2),
    .INIT_38(256'h338EEA45A1FD59B6126FCB2885E2409DFB59B71573D2308FEE4DAD0C6CCB2B8B),
    .INIT_39(256'h1A71C71E75CC237AD22981D93189E23A93EC459EF751AA045EB8126DC7227DD8),
    .INIT_3A(256'h9FF04294E6388ADC2F82D5287BCE2275C91D71C51A6EC3186DC2176DC2186EC4),
    .INIT_3B(256'hBC0956A3F03D8BD92674C3115FAEFC4B9AE93988D82877C81868B9095AABFC4D),
    .INIT_3C(256'h6FB7FF4890D9226BB4FE4791DA246EB9034D98E32E79C40F5BA6F23E8AD6236F),
    .INIT_3D(256'hB4F83B7FC3084C91D51A5FA4E92F74BAFF458BD2185EA5EC337AC1085097DF27),
    .INIT_3E(256'h87C7064685C5054585C6064788C90A4B8CCE0F5193D517599CDE2164A7EA2D70),
    .INIT_3F(256'hE6215C97D20E4A85C1FD3976B2EF2C68A5E3205D9BD8165492D00F4D8CCA0948),
    .INIT_40(256'h6E24DA9147FEB46B22D99048FFB76F27DF975008C17A33ECA55E18D28B45FFBA),
    .INIT_41(256'hFCAE5F11C37426D98B3DF0A35609BC6F23D68A3EF2A65B0FC4792EE3984D03B9),
    .INIT_42(256'h22CE7B28D5822FDC8A38E69442F09F4DFCAB5A09B96818C87728D88839E99A4B),
    .INIT_43(256'hE28931D98129D27A23CC751EC7711AC46E18C26D17C26D18C36E1AC5711DC975),
    .INIT_44(256'h40E28528CB6F12B659FDA145EA8E33D87D22C76D12B85E04AA51F79E45EC933A),
    .INIT_45(256'h40DD7B19B755F39230CF6E0DAD4CEC8B2BCB6C0CAD4DEE8F30D27315B759FB9D),
    .INIT_46(256'hE57D15AE47E07912AC45DF7913AD48E27D18B34FEA8621BD59F5922ECB6805A2),
    .INIT_47(256'h32C558EB7F13A63ACF63F78C21B64BE0760CA137CE64FA9128BF56ED851DB44C),
    .INIT_48(256'h2BB846D462F17F0E9C2BBA4AD969F98919A939CA5BEC7D0EA032C355E77A0C9F),
    .INIT_49(256'hD35BE36CF47D068F18A22BB53FC954DE69F47F0A9521AC38C451DD6AF683109D),
    .INIT_4A(256'h2DB032B538BB3FC246CA4ED256DB60E56AEF74FA80068C12991FA62DB43CC34B),
    .INIT_4B(256'h3DBA37B431AF2CAA28A625A322A1209F1E9E1E9E1E9E1F9F20A122A425A729AB),
    .INIT_4C(256'h067CF46BE25AD24AC23AB32CA51E97108A047EF873ED68E35ED955D04CC844C1),
    .INIT_4D(256'h8AFB6CDD4FC132A51789FC6FE255C93CB024980C81F66BE055CA40B62CA2188F),
    .INIT_4E(256'hCD38A30E7AE551BD2A960370DD4AB82593016FDE4CBB2A990878E857C838A819),
    .INIT_4F(256'hD1369B0066CB3197FE64CB31980067CF369E076FD740A9127BE54FB8228DF762),
    .INIT_50(256'h9AF958B71676D63595F656B71879DA3B9DFF61C32588EB4EB11478DC3FA4086C),
    .INIT_51(256'h2B84DC358EE7419AF44EA8035DB8136ECA2581DD3995F24FAC0966C4217FDE3C),
    .INIT_52(256'h86D92B7DD02376C91C70C4186CC1156ABF156AC0166CC2186FC61D74CB237BD3),
    .INIT_53(256'hAFFB4692DF2B78C4115FACFA4896E43281D01F6EBD0D5DADFD4E9EEF4091E335),
    .INIT_54(256'hA7EC3277BD03498FD61C63AAF23981C91159A2EB347DC61059A3EE3883CD1864),
    .INIT_55(256'h72B1F02E6DADEC2C6CACEC2D6DAEF03172B4F6387BBD004386CA0D5195D91E63),
    .INIT_56(256'h134B83BBF32C649DD7104A83BEF8326DA8E31E5A95D10E4A86C3003D7BB8F634),
    .INIT_57(256'h8BBCED1F5082B4E6194B7EB1E5184C80B4E81D5186BCF1275D93C9FF366DA4DB),
    .INIT_58(256'hDE08325D87B2DD0934608CB9E5123F6C99C6F422507FADDC0B3A6A9AC9FA2A5A),
    .INIT_59(256'h0D3054779BBFE3082C51769CC1E70D335A80A7CEF61D456D95BDE60F38618AB4),
    .INIT_5A(256'h1C3854718EABC8E603213F5E7C9BBAD9F91838587899BADBFC1D3F6183A5C8EA),
    .INIT_5B(256'h0C21374C62788EA4BBD2E900182F475F7890A9C2DCF50F29435D7893AEC9E400),
    .INIT_5C(256'hE1EFFD0B1A2837475666768696A7B8C9DAEBFD0F213446596C7F93A6BACFE3F8),
    .INIT_5D(256'h9BA2A9B0B7BFC7CFD7DFE8F1FA040D17212B36414C57626E7A86929FABB8C6D3),
    .INIT_5E(256'h3E3D3D3D3D3D3E3E3F41424446484A4D4F5256595D6165696E73787D83898F95),
    .INIT_5F(256'hCBC3BCB4ADA69F98928C86807B75706C67635F5B5754514E4B4947454341403F),
    .INIT_60(256'h4536271809FAECDED0C3B6A89C8F83766B5F53483D32281E140A00F7EEE5DCD4),
    .INIT_61(256'hAD97806A533D2812FDE8D3BFAA96826F5B48352310FEECDAC9B8A69685756555),
    .INIT_62(256'h06E8CAAC8E70533619FDE1C5A98D72573C2107ECD3B99F866D543C230BF4DCC5),
    .INIT_63(256'h512B06E0BB96714C2804E0BC997653300DEBC9A88665442302E2C2A282634425),
    .INIT_64(256'h91633608DBAF82562AFED3A77C5227FDD3A97F562D04DBB38A623B13ECC59E78),
    .INIT_65(256'hC6915B27F2BD895522EEBB885523F0BE8D5B2AF9C897673707D7A8794A1BEDBF),
    .INIT_66(256'hF3B6793C00C3884C10D59A5F25EBB1773D04CB925A21E9B27A430CD59E6832FC),
    .INIT_67(256'h19D48F4B07C37F3BF8B57230EDAB6A28E7A66524E4A46424E5A66728EAAC6E30),
    .INIT_68(256'h3AEDA05408BC7125DA8F45FAB0661DD38A41F8B06820D8914A03BC762FE9A45E),
    .INIT_69(256'h5702AE5A06B25E0BB86513C06E1DCB7A29D88737E79748F8A95B0CBE7022D487),
    .INIT_6A(256'h7215B95D01A549EE9339DE842AD0771EC56C13BB630CB45D06AF5902AC5701AC),
    .INIT_6B(256'h8C27C35FFB9734D06D0BA846E48221C05FFE9E3EDE7E1FBF6002A345E7892CCF),
    .INIT_6C(256'hA63ACD61F5891EB348DD72089E35CB62F99028C058F08921BA54ED8721BB56F1),
    .INIT_6D(256'hC24ED965F17D099623B03ECC5AE876059423B343D363F38415A638CA5CEE8013),
    .INIT_6E(256'hE164E86BEF73F87C01860C91179D24AA31B840C84FD860E972FB840E9822AD37),
    .INIT_6F(256'h047FFA76F16DEA66E360DD5BD856D553D251D050D050D051D153D455D759DC5E),
    .INIT_70(256'h2C9E1285F86CE055C93EB3289E148A0077EE65DD54CC44BD35AE27A11B950F89),
    .INIT_71(256'h59C42F9A0571DD49B5228FFC69D745B32290FF6FDE4EBE2E9E0F80F163D547B9),
    .INIT_72(256'h8EF053B6197CE044A80C71D63BA1066CD339A0076ED63DA50E76DF48B11B85EF),
    .INIT_73(256'hCA247ED9348FEB46A2FE5BB71472CF2D8BE948A60565C42484E445A60768CA2C),
    .INIT_74(256'h0F60B30557AAFE51A5F84DA1F64BA0F54BA1F74EA5FC53AA025AB30B64BD1670),
    .INIT_75(256'h5CA6F03A84CF1A65B0FC4794E02D7AC71462B0FE4D9BEA3A89D92979CA1B6CBD),
    .INIT_76(256'hB4F53778BAFD3F82C5084C90D4185DA2E72C72B8FE448BD21961A8F03981CA13),
    .INIT_77(256'h164F88C1FB356FAAE41F5B96D20E4A87C3013E7BB9F73674B3F23272B2F23273),
    .INIT_78(256'h82B3E4154678AADC0E4174A7DA0E4276ABDF144A7FB5EB21588FC6FD356DA5DD),
    .INIT_79(256'hFB234B749DC6F019436E98C3EE1945719DC9F623507EABD907366594C3F22252),
    .INIT_7A(256'h7E9EBEDEFF20416384A6C8EB0D3054779BBFE3082D52779CC2E80F355C83ABD3),
    .INIT_7B(256'h0E253D556D869EB7D1EA041E39536E89A4C0DCF815314E6B89A7C5E301203F5F),
    .INIT_7C(256'hAAB9C8D8E7F80819293B4C5E708294A7BACDE1F5091D32465C71879CB3C9E0F7),
    .INIT_7D(256'h52585F676E767E868E97A0AAB3BDC7D1DCE7F2FD0915212E3A475562707E8C9B),
    .INIT_7E(256'h060403020100000000000102030406080A0D101316191D21262A2F343A3F454B),
    .INIT_7F(256'hC7BDB3AAA0978E867E766E675F58524B453F3A342F2A26211D191613100D0A08),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized24
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'hFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000),
    .INITP_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_05(256'h000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_06(256'hFFFFFFFFFF000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_08(256'h0000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000),
    .INITP_0B(256'h0000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0101010101010101010000000000000000000000000000000000000000000000),
    .INIT_01(256'h0202020202020202020202020202020101010101010101010101010101010101),
    .INIT_02(256'h0404040404040404040403030303030303030303030303030303030202020202),
    .INIT_03(256'h0707060606060606060606060606050505050505050505050505050504040404),
    .INIT_04(256'h0A0A090909090909090909090808080808080808080808070707070707070707),
    .INIT_05(256'h0D0D0D0D0D0D0C0C0C0C0C0C0C0C0C0B0B0B0B0B0B0B0B0B0A0A0A0A0A0A0A0A),
    .INIT_06(256'h111111111111101010101010100F0F0F0F0F0F0F0F0E0E0E0E0E0E0E0E0E0D0D),
    .INIT_07(256'h1616151515151515151414141414141413131313131313121212121212121111),
    .INIT_08(256'h1B1B1A1A1A1A1A1A191919191919191818181818181717171717171616161616),
    .INIT_09(256'h20202020201F1F1F1F1F1F1E1E1E1E1E1E1D1D1D1D1D1D1C1C1C1C1C1B1B1B1B),
    .INIT_0A(256'h2726262626262525252525242424242423232323232222222222222121212121),
    .INIT_0B(256'h2D2D2D2C2C2C2C2C2B2B2B2B2B2A2A2A2A2A2929292929282828282827272727),
    .INIT_0C(256'h343434343333333332323232323131313130303030302F2F2F2F2E2E2E2E2E2D),
    .INIT_0D(256'h3C3C3B3B3B3B3A3A3A3A39393939393838383837373737363636363535353535),
    .INIT_0E(256'h4444444343434342424241414141404040403F3F3F3F3E3E3E3E3D3D3D3D3C3C),
    .INIT_0F(256'h4D4C4C4C4C4B4B4B4B4A4A4A4949494948484848474747464646464545454544),
    .INIT_10(256'h56565555555454545453535352525252515151505050504F4F4F4E4E4E4E4D4D),
    .INIT_11(256'h605F5F5F5E5E5E5D5D5D5C5C5C5C5B5B5B5A5A5A595959595858585757575656),
    .INIT_12(256'h6A69696968686867676767666666656565646464636363626262616161606060),
    .INIT_13(256'h7474747373737272727171717070706F6F6F6E6E6E6D6D6D6C6C6C6B6B6B6A6A),
    .INIT_14(256'h807F7F7E7E7E7D7D7D7C7C7C7B7B7B7A7A7A7979797878777777767676757575),
    .INIT_15(256'h8B8B8A8A8A898989888888878786868685858584848483838282828181818080),
    .INIT_16(256'h9797979696959595949494939392929291919090908F8F8F8E8E8D8D8D8C8C8C),
    .INIT_17(256'hA4A4A3A3A2A2A2A1A1A0A0A09F9F9E9E9E9D9D9C9C9C9B9B9A9A9A9999999898),
    .INIT_18(256'hB1B1B0B0AFAFAFAEAEADADADACACABABAAAAAAA9A9A8A8A8A7A7A6A6A6A5A5A4),
    .INIT_19(256'hBFBEBEBDBDBDBCBCBBBBBABABAB9B9B8B8B7B7B7B6B6B5B5B4B4B4B3B3B2B2B1),
    .INIT_1A(256'hCDCCCCCBCBCACACAC9C9C8C8C7C7C7C6C6C5C5C4C4C3C3C3C2C2C1C1C0C0C0BF),
    .INIT_1B(256'hDBDBDADAD9D9D8D8D8D7D7D6D6D5D5D4D4D3D3D3D2D2D1D1D0D0CFCFCFCECECD),
    .INIT_1C(256'hEAEAE9E9E8E8E7E7E6E6E6E5E5E4E4E3E3E2E2E1E1E0E0DFDFDEDEDEDDDDDCDC),
    .INIT_1D(256'hFAF9F9F8F8F7F7F6F6F5F5F4F4F3F3F2F2F1F1F0F0F0EFEFEEEEEDEDECECEBEB),
    .INIT_1E(256'h0A0909080807070606050504040303020201010000FFFFFEFEFDFDFCFCFBFBFA),
    .INIT_1F(256'h1A1A191818171716161515141413131212111110100F0F0E0E0D0D0C0C0B0B0A),
    .INIT_20(256'h2B2A2A292928282727262625242423232222212120201F1F1E1E1D1D1C1C1B1B),
    .INIT_21(256'h3C3C3B3B3A393938383737363635353433333232313130302F2F2E2E2D2C2C2B),
    .INIT_22(256'h4E4D4D4C4C4B4B4A4949484847474646454444434342424141403F3F3E3E3D3D),
    .INIT_23(256'h605F5F5E5E5D5D5C5B5B5A5A5959585757565655555453535252515150504F4E),
    .INIT_24(256'h7372717170706F6F6E6D6D6C6C6B6A6A69696868676666656564636362626161),
    .INIT_25(256'h86858484838382818180807F7E7E7D7D7C7B7B7A7A7979787777767675747473),
    .INIT_26(256'h999898979796959594949392929191908F8F8E8D8D8C8C8B8A8A898988878786),
    .INIT_27(256'hADACACABAAAAA9A9A8A7A7A6A5A5A4A4A3A2A2A1A1A09F9F9E9D9D9C9C9B9A9A),
    .INIT_28(256'hC1C1C0BFBFBEBDBDBCBCBBBABAB9B8B8B7B6B6B5B5B4B3B3B2B1B1B0AFAFAEAE),
    .INIT_29(256'hD6D5D5D4D3D3D2D1D1D0CFCFCECDCDCCCCCBCACAC9C8C8C7C6C6C5C4C4C3C3C2),
    .INIT_2A(256'hEBEAEAE9E8E8E7E6E6E5E4E4E3E2E2E1E0E0DFDEDEDDDCDCDBDBDAD9D9D8D7D7),
    .INIT_2B(256'h0100FFFFFEFDFCFCFBFAFAF9F8F8F7F6F6F5F4F4F3F2F2F1F0F0EFEEEEEDECEC),
    .INIT_2C(256'h16161514141312121110100F0E0D0D0C0B0B0A09090807070605050403030201),
    .INIT_2D(256'h2D2C2B2B2A29282827262625242423222221201F1F1E1D1D1C1B1B1A19181817),
    .INIT_2E(256'h4343424141403F3E3E3D3C3C3B3A3939383737363534343332323130302F2E2D),
    .INIT_2F(256'h5A5A59585757565555545352525150504F4E4D4D4C4B4B4A4948484746464544),
    .INIT_30(256'h727170706F6E6D6D6C6B6A6A696868676665656463626261605F5F5E5D5D5C5B),
    .INIT_31(256'h8A898887878685848483828181807F7E7E7D7C7B7B7A79787877767575747373),
    .INIT_32(256'hA2A1A09F9F9E9D9C9C9B9A99999897969695949393929190908F8E8D8D8C8B8A),
    .INIT_33(256'hBAB9B9B8B7B6B6B5B4B3B2B2B1B0AFAFAEADACACABAAA9A9A8A7A6A5A5A4A3A2),
    .INIT_34(256'hD3D2D1D1D0CFCECECDCCCBCACAC9C8C7C7C6C5C4C3C3C2C1C0C0BFBEBDBCBCBB),
    .INIT_35(256'hECEBEBEAE9E8E7E7E6E5E4E3E3E2E1E0E0DFDEDDDCDCDBDAD9D8D8D7D6D5D5D4),
    .INIT_36(256'h0605040302020100FFFEFEFDFCFBFAFAF9F8F7F6F6F5F4F3F2F2F1F0EFEFEEED),
    .INIT_37(256'h1F1F1E1D1C1B1B1A1918171716151413131211100F0E0E0D0C0B0A0A09080706),
    .INIT_38(256'h3A3938373635353433323131302F2E2D2C2C2B2A292828272625242423222120),
    .INIT_39(256'h5453525251504F4E4D4D4C4B4A4948484746454443434241403F3F3E3D3C3B3A),
    .INIT_3A(256'h6F6E6D6C6B6B6A6968676666656463626161605F5E5D5C5C5B5A595857575655),
    .INIT_3B(256'h8A8988878686858483828181807F7E7D7C7B7B7A797877767675747372717070),
    .INIT_3C(256'hA5A4A3A3A2A1A09F9E9D9D9C9B9A9998989796959493929291908F8E8D8C8C8B),
    .INIT_3D(256'hC1C0BFBEBDBDBCBBBAB9B8B7B6B6B5B4B3B2B1B0B0AFAEADACABAAAAA9A8A7A6),
    .INIT_3E(256'hDDDCDBDAD9D8D8D7D6D5D4D3D2D1D1D0CFCECDCCCBCACAC9C8C7C6C5C4C3C3C2),
    .INIT_3F(256'hF9F8F7F6F5F5F4F3F2F1F0EFEEEDEDECEBEAE9E8E7E6E6E5E4E3E2E1E0DFDFDE),
    .INIT_40(256'h999A9B9C9D9D9E9FA0A1A2A3A3A4A5A6A7A8A9AAAAABACADAEAFB0B0B1B2B3B4),
    .INIT_41(256'h7E7F8081818283848586868788898A8B8C8C8D8E8F9091929293949596979898),
    .INIT_42(256'h63646566666768696A6B6B6C6D6E6F7070717273747576767778797A7B7B7C7D),
    .INIT_43(256'h48494A4B4C4D4D4E4F5051525253545556575758595A5B5C5C5D5E5F60616162),
    .INIT_44(256'h2E2F3031313233343535363738393A3A3B3C3D3E3F3F40414243434445464748),
    .INIT_45(256'h141516171718191A1B1B1C1D1E1F1F2021222324242526272828292A2B2C2C2D),
    .INIT_46(256'hFAFBFCFDFEFEFF0001020203040506060708090A0A0B0C0D0E0E0F1011121313),
    .INIT_47(256'hE1E2E3E3E4E5E6E7E7E8E9EAEBEBECEDEEEFEFF0F1F2F2F3F4F5F6F6F7F8F9FA),
    .INIT_48(256'hC8C9CACACBCCCDCECECFD0D1D1D2D3D4D5D5D6D7D8D8D9DADBDCDCDDDEDFE0E0),
    .INIT_49(256'hAFB0B1B2B2B3B4B5B6B6B7B8B9B9BABBBCBCBDBEBFC0C0C1C2C3C3C4C5C6C7C7),
    .INIT_4A(256'h979899999A9B9C9C9D9E9F9FA0A1A2A2A3A4A5A5A6A7A8A9A9AAABACACADAEAF),
    .INIT_4B(256'h7F808181828384848586878788898A8A8B8C8D8D8E8F90909192939394959696),
    .INIT_4C(256'h6868696A6A6B6C6D6D6E6F70707172737374757576777878797A7B7B7C7D7E7E),
    .INIT_4D(256'h505152525354555556575758595A5A5B5C5D5D5E5F5F60616262636465656667),
    .INIT_4E(256'h393A3B3C3C3D3E3E3F40414142434344454646474848494A4B4B4C4D4D4E4F50),
    .INIT_4F(256'h232424252626272828292A2B2B2C2D2D2E2F3030313232333434353637373839),
    .INIT_50(256'h0D0D0E0F1010111212131414151616171818191A1B1B1C1D1D1E1F1F20212222),
    .INIT_51(256'hF7F8F8F9FAFAFBFCFCFDFEFFFF0001010203030405050607070809090A0B0B0C),
    .INIT_52(256'hE2E2E3E4E4E5E6E6E7E8E8E9EAEAEBECECEDEEEEEFF0F0F1F2F2F3F4F4F5F6F6),
    .INIT_53(256'hCDCDCECFCFD0D1D1D2D3D3D4D5D5D6D7D7D8D9D9DADBDBDCDCDDDEDEDFE0E0E1),
    .INIT_54(256'hB8B9BABABBBCBCBDBDBEBFBFC0C1C1C2C3C3C4C4C5C6C6C7C8C8C9CACACBCCCC),
    .INIT_55(256'hA4A5A5A6A7A7A8A9A9AAAAABACACADAEAEAFAFB0B1B1B2B3B3B4B5B5B6B6B7B8),
    .INIT_56(256'h9191929293949495959697979898999A9A9B9C9C9D9D9E9F9FA0A1A1A2A2A3A4),
    .INIT_57(256'h7D7E7E7F80808181828383848485868687878889898A8A8B8C8C8D8D8E8F8F90),
    .INIT_58(256'h6A6B6C6C6D6D6E6F6F70707171727373747475767677777879797A7A7B7B7C7D),
    .INIT_59(256'h5859595A5A5B5B5C5D5D5E5E5F5F60616162626363646565666667686869696A),
    .INIT_5A(256'h464747484849494A4B4B4C4C4D4D4E4E4F505051515252535354555556565757),
    .INIT_5B(256'h353536363737383839393A3B3B3C3C3D3D3E3E3F3F4041414242434344444546),
    .INIT_5C(256'h2324242526262727282829292A2A2B2B2C2C2D2E2E2F2F303031313232333334),
    .INIT_5D(256'h131314141515161617171818191A1A1B1B1C1C1D1D1E1E1F1F20202121222223),
    .INIT_5E(256'h03030404050506060707080809090A0A0B0B0C0C0D0D0E0E0F0F101011111212),
    .INIT_5F(256'hF3F3F4F4F5F5F6F6F7F7F8F8F9F9FAFAFBFBFCFCFDFDFEFEFFFF000001010202),
    .INIT_60(256'hE4E4E5E5E6E6E6E7E7E8E8E9E9EAEAEBEBECECEDEDEEEEEFEFF0F0F0F1F1F2F2),
    .INIT_61(256'hD5D5D6D6D7D7D8D8D8D9D9DADADBDBDCDCDDDDDEDEDEDFDFE0E0E1E1E2E2E3E3),
    .INIT_62(256'hC7C7C7C8C8C9C9CACACACBCBCCCCCDCDCECECFCFCFD0D0D1D1D2D2D3D3D3D4D4),
    .INIT_63(256'hB9B9BABABABBBBBCBCBDBDBDBEBEBFBFC0C0C0C1C1C2C2C3C3C3C4C4C5C5C6C6),
    .INIT_64(256'hABACACADADADAEAEAFAFAFB0B0B1B1B1B2B2B3B3B4B4B4B5B5B6B6B7B7B7B8B8),
    .INIT_65(256'h9E9F9FA0A0A0A1A1A2A2A2A3A3A4A4A4A5A5A6A6A6A7A7A8A8A8A9A9AAAAAAAB),
    .INIT_66(256'h92929393949494959595969697979798989999999A9A9A9B9B9C9C9C9D9D9E9E),
    .INIT_67(256'h868687878888888989898A8A8A8B8B8C8C8C8D8D8D8E8E8F8F8F909090919192),
    .INIT_68(256'h7B7B7B7C7C7C7D7D7D7E7E7E7F7F808080818181828282838384848485858586),
    .INIT_69(256'h70707071717172727273737374747475757576767677777778787979797A7A7A),
    .INIT_6A(256'h65666666676767676868686969696A6A6A6B6B6B6C6C6C6D6D6D6E6E6E6F6F6F),
    .INIT_6B(256'h5B5C5C5C5C5D5D5D5E5E5E5F5F5F606060606161616262626363636464646565),
    .INIT_6C(256'h5252525353535454545455555556565656575757585858595959595A5A5A5B5B),
    .INIT_6D(256'h4949494A4A4A4B4B4B4B4C4C4C4C4D4D4D4E4E4E4E4F4F4F5050505051515152),
    .INIT_6E(256'h4041414141424242434343434444444445454545464646464747474848484849),
    .INIT_6F(256'h39393939393A3A3A3A3B3B3B3B3C3C3C3C3D3D3D3D3E3E3E3E3F3F3F3F404040),
    .INIT_70(256'h3131323232323233333333343434343535353535363636363737373738383838),
    .INIT_71(256'h2A2A2B2B2B2B2B2C2C2C2C2C2D2D2D2D2E2E2E2E2E2F2F2F2F30303030303131),
    .INIT_72(256'h24242424252525252526262626262727272727282828282829292929292A2A2A),
    .INIT_73(256'h1E1E1E1E1F1F1F1F1F1F20202020202121212121222222222222232323232324),
    .INIT_74(256'h191919191919191A1A1A1A1A1A1B1B1B1B1B1B1C1C1C1C1C1D1D1D1D1D1D1E1E),
    .INIT_75(256'h1414141414141515151515151516161616161616171717171717181818181818),
    .INIT_76(256'h0F0F101010101010101111111111111111121212121212121313131313131314),
    .INIT_77(256'h0C0C0C0C0C0C0C0C0C0D0D0D0D0D0D0D0D0E0E0E0E0E0E0E0E0E0F0F0F0F0F0F),
    .INIT_78(256'h080808090909090909090909090A0A0A0A0A0A0A0A0A0A0B0B0B0B0B0B0B0B0B),
    .INIT_79(256'h0506060606060606060606060607070707070707070707070808080808080808),
    .INIT_7A(256'h0303030303040404040404040404040404040405050505050505050505050505),
    .INIT_7B(256'h0202020202020202020202020202020202020202030303030303030303030303),
    .INIT_7C(256'h0000000000000101010101010101010101010101010101010101010101010101),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized25
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hCE71C3C3F00FFFFF803E0F1E318CCCCD9249694AD556AD552A52DA49B2666739),
    .INITP_01(256'hCCE71C78781FE000007FC1F0E1CE3319B364925AD6AD5555556AD696DB6CD999),
    .INITP_02(256'h9CE71C3C3E01FFFFFFF80F87871CE7333264924B4A54AAB54AAA56B4B49364CC),
    .INITP_03(256'hE387C1FC003FF0007E07870E318CCCCD936D25A56AD5555555AB52D2DB64D999),
    .INITP_04(256'hFFFFFF007E0F0F1C6398CCCD9B6496D294AD55AAAAD54A94A4B6DB66CCCCE638),
    .INITP_05(256'h7398CCCC9936DB69296A56AAD55556AA54A52D24B24D9933198C71C78783F007),
    .INITP_06(256'hAAAA955AB52D696DB6DB266CCCCE631CE3C78781F80FFFE00FFFE01F83C3C71C),
    .INITP_07(256'hFC07FE0000000003FF01F83C1E3C71C739CC666664C9B24924B4B4A56AD54AAA),
    .INITP_08(256'hC9B6DB6D2D695AB552AAAAAAA556AD4A5A5A49249B264CCCCC6739C71C78F078),
    .INITP_09(256'hD4AD292DB6D9326666339C71C78783F00FFFE00FFFE03F03C3C78E718CE6666C),
    .INITP_0A(256'h66338C71E1E0FC01FFFFFFC01F83C3C71C63319933649A49694A54AAD55556AA),
    .INITP_0B(256'hC3C0FC001FF8007F07C38E38CE6666CDB6DA4A52A556AAAB556A5296D24DB366),
    .INITP_0C(256'hE03FFFFFFF00F87871CE7333364DB69695AB55555556AD4B496D9366666318E1),
    .INITP_0D(256'h07FC00000FF03C3C71CE66664D925A5AD4AAA55AAA54A5A4924C9999CE71C3C3),
    .INITP_0E(256'hE0F803FFFFE01F87871CE733366DB6D2D6AD5555556AD6B4924D9B3198E70E1F),
    .INITP_0F(256'hC78F07F00000001FC1E3C739CCCC9B24B694A9556AD556A52D249366666318F1),
    .INIT_00(256'h086CD43CA41080F064D84CC840C03CC040C850D864F08014A83CD4700CAC4CEC),
    .INIT_01(256'hE404244C709CC8F4245488BCF4306CA8E82C70B8004C98E43888E03490EC48A8),
    .INIT_02(256'h1CF8D8BCA0846C58443424180C00FCF4F4F4F4F8FC040C182838485C748CA8C4),
    .INIT_03(256'hB44CEC882CCC741CC4701CCC8034E8A05C18D898581CE4AC784414E4B8906840),
    .INIT_04(256'hA4FC58B41474D438A00874E050C034A8209C1494149418A028B03CCC5CF08418),
    .INIT_05(256'hF0081C38547090B4D8FC24507CA8D80C4078B0EC2868A8EC3078C00C5CACFC50),
    .INIT_06(256'h90643810E8C4A08060442C10FCE8D4C4B8ACA0989490909090949CA4B0BCCCDC),
    .INIT_07(256'h8010A438D06804A03CE08028D07824D08030E49C500CC8844408CC945C24F0C0),
    .INIT_08(256'hBC0C5CAC0058B00C68C42888F054BC28980478E860D850CC48C84CD054DC68F4),
    .INIT_09(256'h404C58687C90A4BCD8F41434587CA0CCF4245084B4EC245C98D4145498E02870),
    .INIT_0A(256'h00CC98643408DCB48C64402000E4C8B098847060504438302824242424282C34),
    .INIT_0B(256'hFC84109C2CBC4CE47810AC48E8882CD07820CC7828DC8C44FCB4702CECB07438),
    .INIT_0C(256'h2C70BC0454A0F4449CF44CA80464C42890F460CC38A81C8C047CF470F070F478),
    .INIT_0D(256'h848C909CA8B4C4D8EC001834506C8CB0D4F8204C78A8D8083C74ACE82464A4E8),
    .INIT_0E(256'h00C48C5420ECBC8C60340CE4C09C7C5C40240CF4E0CCBCACA0988C8884808080),
    .INIT_0F(256'h981CA42CB440D060F0841CB44CE88828CC7014BC6814C47424D8904804C07C40),
    .INIT_10(256'h4084C8105CA8F44494E84098F04CA8086CD0349C0470E050C034AC249C189818),
    .INIT_11(256'hF0F4F8000C14243444586C84A0B8D8F8183C6088B4DC0C3C6CA0D40C4880C000),
    .INIT_12(256'h9C6028F0B8845020F4C89C744C2808E8C8AC9078644C3C2C1C1004FCF8F4F0F0),
    .INIT_13(256'h3CC448D05CE87404982CC058F49030D07014BC640CB86414C87C30E8A4601CDC),
    .INIT_14(256'hC40C509CE83484D4287CD42C88E440A4046CD038A41080F064D84CC440BC3CBC),
    .INIT_15(256'h28303840505C6C8094A8C0DCF81838587CA0C8F4204C7CB0E0185088C4004080),
    .INIT_16(256'h5C24ECB8885828FCD4AC8460401C00E4C8B09884706050443830282420202024),
    .INIT_17(256'h50DC68F48418AC40D87410AC4CEC9038DC8834E09040F4A86018D0904C0CD094),
    .INIT_18(256'hFC4898E83890E43C98F450B01478DC44B01C88F868DC50C844BC38B838BC40C8),
    .INIT_19(256'h4C5C6C8098B0C8E40020406488B0D804305C8CC0F42C609CD8145498D82068B0),
    .INIT_1A(256'h380CE0B8906C482808E8CCB49C84706050403428201C14141010141820283040),
    .INIT_1B(256'hAC44DC7814B454F89C40E89440EC9C5000B86C28E0A05C1CE0A46C34FCC89864),
    .INIT_1C(256'h9CF858B41878E044B01884F464D448C038B02CA828A82CB034C048D464F08418),
    .INIT_1D(256'hF8183C6084ACD804306090C4F83068A4E01C5CA0E02870B80450A0F04094EC44),
    .INIT_1E(256'hB0947C68544030201408FCF4F0ECE8E8E8ECF0F8000C182838485C7088A4BCDC),
    .INIT_1F(256'hB05C0CB86C20D48844FCB87838F8BC804810DCA8784818ECC098744C2808E8CC),
    .INIT_20(256'hEC5CD048C038B430B030B438BC44D05CE878089C30C45CF89430D07014B86008),
    .INIT_21(256'h4C84C0FC3C7CC0044890D82470C01064B80C64C01878D43498FC60C834A00C78),
    .INIT_22(256'hC4C4C8CCD0D8E4F0FC0C1C304458708CA4C4E00424487098C0EC184878ACE014),
    .INIT_23(256'h3C04D09C6C3C0CE0B48C64401CFCDCBCA0846C54402C1808F8ECE0D8D0C8C4C4),
    .INIT_24(256'hA434C860F89028C86404A448EC943CE89440F0A05408C07830ECA86828E8B074),
    .INIT_25(256'hE844A00060C4288CF45CC834A01084F86CE45CD450D04CD050D45CE46CF88414),
    .INIT_26(256'hF418406894C0F01C5084B8EC24609CD8185898DC2068B4FC4898E8388CE0348C),
    .INIT_27(256'hB4A4948880746C686460606064686C74808894A4B4C4D8EC041C3450708CACD0),
    .INIT_28(256'h10CC884808CC90541CE4B07C4818E8BC90643C18F0CCAC8C6C50341C04ECD8C4),
    .INIT_29(256'hF880089420B040D064F88C24C058F89434D87820C46C18C0701CCC8034E8A058),
    .INIT_2A(256'h54A8FC54AC0864C42484E44CB01880EC58C834A8188C047CF470EC68E86CEC70),
    .INIT_2B(256'h092D50749CC4EC184474A4D4083C74ACE4205C98D81C5CA4E83078C41060B000),
    .INIT_2C(256'h09F9E9DDD1C9C1B9B5B1ADADB1B1B5BDC1CDD5E1F1FD0D213549617995ADCDE9),
    .INIT_2D(256'h39F5B5793D01C58D5925F1BD8D5D3105DDB18D654121FDE1C1A5897159412D19),
    .INIT_2E(256'h7D0D9D2DC155E98119B551ED8D2DD17519C16911BD6915C57929DD954D05BD79),
    .INIT_2F(256'hC52181E549AD1179E54DB929950979ED61D951C945C141BD41C145CD51D965F1),
    .INIT_30(256'hF1215585B9F1296199D5155191D11559A1E53179C51161B10155A90159B10965),
    .INIT_31(256'hF1F1F5F90109111D2935455565798DA1B9D1ED0925456585A9CDF119416D99C5),
    .INIT_32(256'hA579512901D9B59171513115F9DDC5AD95816D5D493D2D21150D05FDF9F5F1F1),
    .INIT_33(256'hF9A149F5A14DFDAD5D11C57D31EDA5611DDD995D1DE1A56D35FDC995613101D5),
    .INIT_34(256'hCD49C949C949CD51D55DE571F98915A535C559ED851DB551E98925C56509AD51),
    .INIT_35(256'h0D61B1055DB10965BD1979D53599F95DC12991F965D13DAD1D910175ED61D955),
    .INIT_36(256'h9DC5ED15416D9DCDFD2D6195C9013971ADE92965A5E9296DB5F9418DD52171BD),
    .INIT_37(256'h615D5D5D6161696D757D85919DA9B9C9D9ED01152D455D7591ADCDED0D315179),
    .INIT_38(256'h3D11E9C19975512D0DEDCDB191795D452D1501EDDDC9B9AD9D91857D756D6965),
    .INIT_39(256'h15C57525D5893DF1A96119D5914D0DC98D4D11D5996129F1BD895521F1C19569),
    .INIT_3A(256'hD159E16DF98511A131C155E57D11A941D97511AD4DED8D2DD1751DC16915BD69),
    .INIT_3B(256'h55B5197DE149B11985ED59C935A51989FD71E55DD54DC945C141BD41C145C94D),
    .INIT_3C(256'h81BDF93979BDFD4185CD155DA5F13D89D52575C9196DC51971C9217DD93595F5),
    .INIT_3D(256'h39516D85A1C1DDFD1D416589ADD1F9214D79A5D1FD2D5D91C1F5296199D10945),
    .INIT_3E(256'h61554D453D3935312D2D2D2D2D3135394149515965717D8999A9B9CDE1F50921),
    .INIT_3F(256'hD9AD85593109E1B99571512D0DEDCDB1957961452D1901EDD9C5B5A59585796D),
    .INIT_40(256'hC56D19C97525D5893DF1A55D15CD8541FDBD7939F9BD814509D1996129F5C191),
    .INIT_41(256'hC94DD55DE571FD8919A535C959ED8519B149E17D19B555F59535D97D21C97119),
    .INIT_42(256'h8DED4DAD1175D941A9117DE555C131A11185F96DE159D14DC945C141BD41C145),
    .INIT_43(256'h296199D5114D8DC90D4D91D51961A9F13D89D52575C51569BD1569C11D75D12D),
    .INIT_44(256'hB9C9DDED01152D455D7991B1CDED0D2D517599C1E9113D6995C1F1215589BDF1),
    .INIT_45(256'h5D452D1501EDD9C9B9A99D91857D756D6961615D5D5D6165696D757D85919DAD),
    .INIT_46(256'h29E9A56529E9AD713901C995612DFDCD9D6D4115EDC59D7951310DEDCDAD9175),
    .INIT_47(256'h3DD165F99129C15DF99935D57919BD6509B15D05B1610DBD7121D58D41F9B56D),
    .INIT_48(256'hB51D85ED59C535A51589F971E55DD551CD49C949C949CD55D961ED7501911DAD),
    .INIT_49(256'hA5E11D5D99DD1D61A5ED317DC5115DADFD4DA1F549A1F951AD0965C52589E951),
    .INIT_4A(256'h2D3D495D6D8195ADC5DDF91531517191B5D901295179A5D501316195C9FD356D),
    .INIT_4B(256'h65452509EDD1B9A18D7965554535291D110901F9F5F1F1F1F1F5F9FD050D1521),
    .INIT_4C(256'h6111C57931E5A15915D1915115D5996129F1B9855521F1C5996D4119F1CDA985),
    .INIT_4D(256'h41C145C951D961ED79099529B94DE57911AD49E58121C56509B15901A95501B1),
    .INIT_4E(256'h1569BD1169C11975D12D8DED51B51981E955C12D9D0D7DF165D951CD45C141BD),
    .INIT_4F(256'hFD2141658DB1DD05315D8DBDF125598DC5013D79B5F53979BD054D95DD2979C5),
    .INIT_50(256'h0DFDF1E1D5CDC1BDB5B1B1ADADB1B5B9C1C9D1DDE9F909192D41597189A5C1E1),
    .INIT_51(256'h5C1CD8985C20E4AC743C08D4A4754519EDC59D75512D09E9CDAD957961493521),
    .INIT_52(256'h048C18A834C858EC8018B04CE48424C46408AC54FCA85400B06010C47830E8A4),
    .INIT_53(256'h186CC42078D83494F858C0248CF864D040B020940880F870EC6CE868EC70F47C),
    .INIT_54(256'hACCCF0183C6490BCE818487CB0E41C5490CC084888CC1058A0E83480CC1C70C0),
    .INIT_55(256'hD8C4B4A4948880746C686460606064686C74808894A4B4C4D8EC041C34506C8C),
    .INIT_56(256'hB46820DC985818D89C6024ECB884501CF0C094684018F4D0AC8C7050341C04EC),
    .INIT_57(256'h4CD050D45CE46CF88410A034C85CF48C28C46000A044E88C34E08C38E89848FC),
    .INIT_58(256'hC00854A0F04094E83C94EC48A40464C82890F860C834A41484F86CE45CD450D0),
    .INIT_59(256'h182C40546C84A0BCDCFC1C40648CB4E00C3C6C9CD0043C74B0E82868A8EC3078),
    .INIT_5A(256'h70482404E0C4A48C705844301C0CFCF0E4D8D0CCC8C4C4C4C4C8D0D8E0ECF808),
    .INIT_5B(256'hD47818C0640CB86410C07024D8904804C07C3CFCC0844C14E0AC784818ECC098),
    .INIT_5C(256'h5CC4309C0878E85CD044BC38B430B030B438C048D05CEC780CA034C860FC9834),
    .INIT_5D(256'h184878A8DC104880BCF83878B8FC4488D4206CB80C5CB00860B81470D03094F8),
    .INIT_5E(256'h180C00F8F0ECE8E8E8ECF0F4FC081420304054687C94B0CCE808284C7498C0EC),
    .INIT_5F(256'h7028E0A05C1CE0A46830F8C490603004D8AC84603C18F8DCBCA488705C483828),
    .INIT_60(256'h2CA828A82CB038C048D464F48418B044E07818B458F89C44EC9440F0A05004B8),
    .INIT_61(256'h5CA0E0286CB800509CEC4094E8409CF854B41478DC44AC1884F064D448C034B0),
    .INIT_62(256'h141C20283440506070849CB4CCE80828486C90B8E00C386498C8FC346CA4E01C),
    .INIT_63(256'h602CF4C08C5C3004D8B08864402000E4C8B098806C5C4C403028201814101014),
    .INIT_64(256'h50DC68F8881CB044DC7814B050F4983CE49038E89848FCB06820D8985414D89C),
    .INIT_65(256'hF44090E03488DC3890EC4CAC1074D840AC1884F468DC50C840BC38B838BC44C8),
    .INIT_66(256'h5060708498B0C8E4001C406084ACD4FC285888B8EC245C94D00C4C90D01860A8),
    .INIT_67(256'h7C4C20F4C8A07C583818F8DCC0A894806C5C5040383028242020202428303844),
    .INIT_68(256'h8010A438D06C04A440E4882CD47C28D48434E89C500CC4804000C4885018E0B0),
    .INIT_69(256'h64B80C64BC1470D03090F458C02C980474E85CD048C43CBC3CBC40C44CD864F0),
    .INIT_6A(256'h3C4C647890ACC8E808284C749CC8F4205084B8F028609CDC1C60A4E8307CC814),
    .INIT_6B(256'h0CDCB488603C18F8D8B8A0846C58443424140C00F8F4F0F0F0F4F8FC04101C2C),
    .INIT_6C(256'hE070049C34D06C08A84CF09840E89444F4A85C10C8844000C080480CD4A06C3C),
    .INIT_6D(256'hC41468BC1470CC2888E84CB41C84F060D040B42CA41C981898189C24AC34C050),
    .INIT_6E(256'hBCCCE0F40C24405C7C9CC0E40C34608CBCEC20548CC400407CC0044890D82474),
    .INIT_6F(256'hD8A8784C20F8D4B08C6C50341800ECD8C4B4A89C908C8480808084888C98A0AC),
    .INIT_70(256'h1CA838CC60F49028C46404A84CF49C44F4A05404BC702CE8A46424E8AC743C08),
    .INIT_71(256'h8CDC2878CC2078D02C88E848AC1078E44CBC2C9C1084FC78F470F070F47C048C),
    .INIT_72(256'h38445060708498B0C8E4002040648CB4DC08346498CC003874B0EC2C70B4FC44),
    .INIT_73(256'h24ECB4845024F4CCA07C583414F4D8BCA4907C68584C40342C28242424242830),
    .INIT_74(256'h50D860E878049828BC54F08828C4680CB05800AC5C0CBC7028E0985414D4985C),
    .INIT_75(256'hC80C509CE43080D02478D02880E03CA00468D038A41080F468DC54D04CC848CC),
    .INIT_76(256'h90909498A0ACB8C4D4E8FC102C446080A0C4E810386490C0F0245C94CC084484),
    .INIT_77(256'hA86828ECB078400CD8A87C5024FCD8B4907054381C08F0DCCCBCB0A49C949090),
    .INIT_78(256'h18941494149C20A834C050E07408A038D47414B458FCA450FCAC5C0CC07830EC),
    .INIT_79(256'hE41C5898D8185CA0E83480CC1C70C41C74CC2C88EC4CB41884F05CCC3CB028A0),
    .INIT_7A(256'h0C04FCF8F4F4F4F4FC000C18243444586C84A0BCD8F81C406890B8E4144478AC),
    .INIT_7B(256'h984C00B8702CE8A86C30F4BC885424F4C89C704C2404E4C4A88C745C48382818),
    .INIT_7C(256'h80F064D850C840C03CC040C84CD864F08010A43CD46C08A848EC9034E08838E4),
    .INIT_7D(256'hD0FC2C5C90C4FC3870B0F03074BC04509CEC3C8CE43894EC4CAC0C70D43CA814),
    .INIT_7E(256'h8068544434241C1008040000000408101C24344454688098B0CCEC0C30547CA4),
    .INIT_7F(256'h9438E48C3CEC9C5004BC7430F0B07038FCC4905C2CFCD0A47C54300CECCCB098),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized26
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h00000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000),
    .INITP_02(256'hFFFF0000000000000000000007FFFFFFFFFFFFFFFFFFFFFFF800000000000000),
    .INITP_03(256'h000000000000007FFFFFFFFFFFFFFFE000000000000000001FFFFFFFFFFFFFFF),
    .INITP_04(256'hFFFFFFC0000000000001FFFFFFFFFFFFF800000000000003FFFFFFFFFFFFFFC0),
    .INITP_05(256'h00000000003FFFFFFFFFFE000000000003FFFFFFFFFFFC000000000001FFFFFF),
    .INITP_06(256'hF0000000000FFFFFFFFFF80000000003FFFFFFFFFE00000000003FFFFFFFFFF8),
    .INITP_07(256'hFFFFFFFF8000000003FFFFFFFFE0000000007FFFFFFFFE0000000007FFFFFFFF),
    .INITP_08(256'h3FFFFFFFFFE0000000001FFFFFFFFFC000000000FFFFFFFFFC000000000FFFFF),
    .INITP_09(256'hFFFFFFFFFFF800000000003FFFFFFFFFF80000000000FFFFFFFFFF8000000000),
    .INITP_0A(256'hFF00000000000007FFFFFFFFFFFF0000000000007FFFFFFFFFFF800000000000),
    .INITP_0B(256'hFFFFFFFC0000000000000007FFFFFFFFFFFFFF800000000000003FFFFFFFFFFF),
    .INITP_0C(256'h000000000000000001FFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFF),
    .INITP_0D(256'hFFFFFFFF80000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFC000),
    .INITP_0E(256'h000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0B0B0A0A0A0A0909090808080807070707060606060505050505040404040403),
    .INIT_01(256'h171716161515141414131312121211111010100F0F0F0E0E0E0D0D0D0C0C0C0B),
    .INIT_02(256'h27262625252424232322222121201F1F1E1E1D1D1C1C1C1B1B1A1A1919181817),
    .INIT_03(256'h3B3B3A3939383737363535343333323131302F2F2E2E2D2C2C2B2B2A29292828),
    .INIT_04(256'h5453525151504F4E4D4D4C4B4A49494847464645444343424140403F3E3D3D3C),
    .INIT_05(256'h70706F6E6D6C6B6A69686766656463636261605F5E5D5C5B5B5A595857565555),
    .INIT_06(256'h91908F8E8D8C8B8A8988878684838281807F7E7D7C7B7A797877767574737271),
    .INIT_07(256'hB6B5B4B3B1B0AFAEADABAAA9A8A7A6A4A3A2A1A09F9E9C9B9A99989796959392),
    .INIT_08(256'hDFDEDDDBDAD9D7D6D5D3D2D1CFCECDCCCAC9C8C6C5C4C3C1C0BFBEBCBBBAB9B7),
    .INIT_09(256'h0D0B0A080705040201FFFEFDFBFAF8F7F5F4F3F1F0EEEDECEAE9E8E6E5E3E2E1),
    .INIT_0A(256'h3E3C3B393836343331302E2D2B292826252322201F1D1C1A191716141311100E),
    .INIT_0B(256'h7372706E6D6B6967666462615F5D5C5A5857555352504E4D4B49484644434140),
    .INIT_0C(256'hADABA9A8A6A4A2A09E9C9B9997959392908E8C8A8987858382807E7C7A797775),
    .INIT_0D(256'hEBE9E7E5E3E1DFDDDBD9D7D5D3D1CFCDCBC9C8C6C4C2C0BEBCBAB8B6B5B3B1AF),
    .INIT_0E(256'h2D2A28262422201E1C1A181513110F0D0B0907050301FFFDFBF9F7F5F3F1EFED),
    .INIT_0F(256'h72706E6C69676563605E5C5A585553514F4D4B48464442403E3B39373533312F),
    .INIT_10(256'hBCBAB7B5B3B0AEACA9A7A5A2A09E9B99979492908E8B89878482807E7B797775),
    .INIT_11(256'h0A07050300FEFBF9F6F4F1EFECEAE7E5E3E0DEDBD9D6D4D2CFCDCAC8C6C3C1BF),
    .INIT_12(256'h5C595754514F4C4A4744423F3D3A383532302D2B282623211E1C191614110F0C),
    .INIT_13(256'hB2AFACA9A7A4A19F9C999694918E8C898684817E7C797674716E6C696664615E),
    .INIT_14(256'h0B09060300FDFAF7F5F2EFECE9E6E4E1DEDBD8D6D3D0CDCAC8C5C2BFBDBAB7B4),
    .INIT_15(256'h696663605D5A5754514E4B484543403D3A3734312E2B282522201D1A1714110E),
    .INIT_16(256'hCBC8C4C1BEBBB8B5B2AFACA9A6A3A09C999693908D8A8784817E7B7875726F6C),
    .INIT_17(256'h302D2A2623201D1A1613100D0A060300FDFAF7F3F0EDEAE7E4E1DDDAD7D4D1CE),
    .INIT_18(256'h9996938F8C8985827F7B7875726E6B6864615E5A5754514D4A4744403D3A3733),
    .INIT_19(256'h070300FCF9F5F2EEEBE8E4E1DDDAD6D3D0CCC9C5C2BFBBB8B4B1AEAAA7A4A09D),
    .INIT_1A(256'h7874706D6966625F5B5754504D4946423F3B3834312D2A26231F1C1815110E0A),
    .INIT_1B(256'hECE9E5E1DEDAD6D2CFCBC7C4C0BCB9B5B2AEAAA7A39F9C9894918D8A86827F7B),
    .INIT_1C(256'h65615D5956524E4A46433F3B3733302C2824211D1915120E0A0603FFFBF7F4F0),
    .INIT_1D(256'hE1DDD9D5D1CDC9C6C2BEBAB6B2AEAAA6A29F9B97938F8B8784807C7874706C69),
    .INIT_1E(256'h615D5955514D4945413D3834302C2824201C1814110D090501FDF9F5F1EDE9E5),
    .INIT_1F(256'hE4E0DCD8D4D0CBC7C3BFBBB7B3AEAAA6A29E9A96928E8A85817D7975716D6965),
    .INIT_20(256'h6B67635F5A56524E4945413D3834302C27231F1B17120E0A0602FDF9F5F1EDE9),
    .INIT_21(256'hF6F2EDE9E5E0DCD8D3CFCAC6C2BDB9B5B0ACA8A39F9B96928E8985817D787470),
    .INIT_22(256'h84807B77726E6965605C58534F4A46413D3834302B27221E1915110C0803FFFB),
    .INIT_23(256'h16120D0804FFFBF6F1EDE8E4DFDAD6D1CDC8C4BFBBB6B2ADA8A49F9B96928D89),
    .INIT_24(256'hABA7A29D98948F8A86817C78736E6A65605C57524E4944403B36322D29241F1B),
    .INIT_25(256'h443F3A36312C27221D19140F0A0601FCF7F2EEE9E4DFDBD6D1CCC8C3BEB9B5B0),
    .INIT_26(256'hE0DBD6D1CCC7C2BEB9B4AFAAA5A09B96928D88837E79746F6B66615C57524E49),
    .INIT_27(256'h7F7A75706B66615C57524D48433E39342F2A25201B16110C0803FEF9F4EFEAE5),
    .INIT_28(256'h221D18130E0803FEF9F4EFEAE5E0DAD5D0CBC6C1BCB7B2ADA8A39E99948E8984),
    .INIT_29(256'hC8C3BEB8B3AEA9A39E99948F89847F7A756F6A65605B56504B46413C37312C27),
    .INIT_2A(256'h716C66615C57514C47413C37312C27211C17120C0702FDF7F2EDE7E2DDD8D2CD),
    .INIT_2B(256'h1E18130D0802FDF8F2EDE7E2DDD7D2CCC7C2BCB7B1ACA7A19C97918C87817C77),
    .INIT_2C(256'hCDC7C2BCB7B1ACA6A19B96908B85807A756F6A645F59544F49443E39332E2823),
    .INIT_2D(256'h807A746F69645E58534D47423C37312C26201B15100A04FFF9F4EEE9E3DED8D3),
    .INIT_2E(256'h35302A241E19130D0802FCF6F1EBE5E0DAD4CFC9C3BEB8B2ADA7A19C96918B85),
    .INIT_2F(256'hEEE8E2DCD7D1CBC5BFBAB4AEA8A39D97918B86807A746F69635D58524C46413B),
    .INIT_30(256'hA9A49E98928C86807A746F69635D57514B45403A342E28221D17110B05FFFAF4),
    .INIT_31(256'h68625C56504A443E38322C26201A140E0802FCF7F1EBE5DFD9D3CDC7C1BBB5AF),
    .INIT_32(256'h29231D17110B05FFF9F3EDE7E0DAD4CEC8C2BCB6B0AAA49E98928C86807A746E),
    .INIT_33(256'hEDE7E1DBD5CFC8C2BCB6B0AAA49D97918B857F79736C66605A544E48423C362F),
    .INIT_34(256'hB4AEA8A29B958F89827C767069635D57514A443E38322B251F19130C0600FAF4),
    .INIT_35(256'h7E78716B655E58524B453F38322C251F19130C0600F9F3EDE7E0DAD4CDC7C1BB),
    .INIT_36(256'h4A443D37312A241D17110A04FDF7F1EAE4DDD7D1CAC4BEB7B1AAA49E97918B84),
    .INIT_37(256'h19130C06FFF9F2ECE5DFD8D2CBC5BEB8B1ABA59E98918B847E77716A645E5751),
    .INIT_38(256'hEBE4DDD7D0CAC3BDB6AFA9A29C958F88827B756E67615A544D47403A332D2620),
    .INIT_39(256'hBFB8B1ABA49D979089837C756F68625B544E47403A332D261F19120C05FEF8F1),
    .INIT_3A(256'h958E87817A736D665F58524B443E373029231C150F0801FBF4EDE7E0D9D3CCC5),
    .INIT_3B(256'h6E676059524C453E37302A231C150F0801FAF3EDE6DFD8D2CBC4BDB7B0A9A29C),
    .INIT_3C(256'h49423B342D261F19120B04FDF6EFE9E2DBD4CDC6C0B9B2ABA49D979089827B74),
    .INIT_3D(256'h261F18110A03FCF5EFE8E1DAD3CCC5BEB7B0A9A29B958E878079726B645D5750),
    .INIT_3E(256'h06FFF8F1EAE3DCD5CEC7C0B9B2ABA49D968F88817A736C655E575049423B342D),
    .INIT_3F(256'hE7E0D9D2CBC4BDB6AFA8A19A938B847D766F68615A534C453E373029221B140D),
    .INIT_40(256'hB2B9C0C6CDD4DBE2E9EFF6FD040B12191F262D343B424950575D646B72798087),
    .INIT_41(256'hD8DFE6EDF3FA01080F151C232A30373E454C525960676E747B828990979DA4AB),
    .INIT_42(256'h01080F151C232930373E444B52585F666D737A81878E959CA2A9B0B7BDC4CBD2),
    .INIT_43(256'h2D333A40474E545B62686F757C838990979DA4ABB1B8BFC5CCD3D9E0E7EDF4FB),
    .INIT_44(256'h5A61676E757B82888F959CA2A9AFB6BDC3CAD0D7DDE4EBF1F8FE050C12191F26),
    .INIT_45(256'h8B91989EA5ABB1B8BEC5CBD2D8DFE5ECF2F9FF060C131920262D333A40474D54),
    .INIT_46(256'hBEC4CAD1D7DDE4EAF1F7FD040A11171D242A31373D444A51575E646A71777E84),
    .INIT_47(256'hF3F900060C13191F252C32383F454B52585E656B71787E848B91979EA4AAB1B7),
    .INIT_48(256'h2B32383E444A51575D636970767C82898F959BA2A8AEB4BBC1C7CDD4DAE0E7ED),
    .INIT_49(256'h666C73797F858B91979DA4AAB0B6BCC2C8CFD5DBE1E7EDF4FA00060C13191F25),
    .INIT_4A(256'hA4AAB0B6BCC2C8CED4DAE0E7EDF3F9FF050B11171D23292F363C42484E545A60),
    .INIT_4B(256'hE5EBF1F7FC02080E141A20262C32383E444A50565C62686E747A80868C92989E),
    .INIT_4C(256'h282E343A40454B51575D63696F747A80868C92989EA4A9AFB5BBC1C7CDD3D9DF),
    .INIT_4D(256'h6F747A80868B91979DA3A8AEB4BABFC5CBD1D7DCE2E8EEF4FAFF050B11171D22),
    .INIT_4E(256'hB8BEC3C9CFD4DAE0E5EBF1F6FC02080D13191E242A30353B41464C52585D6369),
    .INIT_4F(256'h040A10151B20262C31373C42474D53585E64696F747A80858B91969CA1A7ADB2),
    .INIT_50(256'h54595F646A6F757A80858B90969BA1A6ACB1B7BCC2C7CDD3D8DEE3E9EEF4F9FF),
    .INIT_51(256'hA7ACB1B7BCC2C7CCD2D7DDE2E7EDF2F8FD02080D13181E23282E33393E44494F),
    .INIT_52(256'hFD02070C12171C21272C31373C41474C51575C61666C71777C81878C91979CA1),
    .INIT_53(256'h565B60656A6F757A7F84898F94999EA3A9AEB3B8BEC3C8CDD2D8DDE2E7EDF2F7),
    .INIT_54(256'hB2B7BCC1C6CBD0D5DAE0E5EAEFF4F9FE03080E13181D22272C31373C41464B50),
    .INIT_55(256'h11161B20252A2F34393E43484D52575C61666B70757A7F84898E94999EA3A8AD),
    .INIT_56(256'h74797E83888D92969BA0A5AAAFB4B9BEC2C7CCD1D6DBE0E5EAEFF4F9FE03080C),
    .INIT_57(256'hDBDFE4E9EEF2F7FC01060A0F14191D22272C31363A3F44494E52575C61666B6F),
    .INIT_58(256'h44494E52575C60656A6E73787C81868A8F94989DA2A7ABB0B5B9BEC3C8CCD1D6),
    .INIT_59(256'hB2B6BBBFC4C8CDD1D6DADFE4E8EDF1F6FBFF04080D12161B1F24292D32363B40),
    .INIT_5A(256'h22272B3034383D41464A4F53585C6065696E72777B8084898D92969B9FA4A8AD),
    .INIT_5B(256'h969B9FA3A8ACB0B5B9BDC2C6CACFD3D8DCE0E5E9EDF2F6FBFF03080C1115191E),
    .INIT_5C(256'h0E12171B1F23272C3034383D4145494E52565A5F63676B7074787D8185898E92),
    .INIT_5D(256'h8A8E92969A9EA2A6AAAEB3B7BBBFC3C7CBD0D4D8DCE0E4E9EDF1F5F9FD02060A),
    .INIT_5E(256'h090D1114181C2024282C3034383D4145494D5155595D6165696D7175797D8185),
    .INIT_5F(256'h8B8F93979B9FA2A6AAAEB2B6BABEC2C6C9CDD1D5D9DDE1E5E9EDF1F5F9FD0105),
    .INIT_60(256'h1215191D2124282C3033373B3F43464A4E5256595D6165696C7074787C808487),
    .INIT_61(256'h9C9FA3A7AAAEB2B5B9BCC0C4C7CBCFD2D6DADEE1E5E9ECF0F4F7FBFF03060A0E),
    .INIT_62(256'h2A2D3134383B3F4246494D5054575B5F6266696D7074787B7F82868A8D919498),
    .INIT_63(256'hBBBFC2C5C9CCD0D3D6DADDE1E4E8EBEEF2F5F9FC0003070A0E1115181C1F2326),
    .INIT_64(256'h5154575A5E6164686B6E7275787B7F8285898C8F9396999DA0A4A7AAAEB1B4B8),
    .INIT_65(256'hEAEDF0F3F7FAFD0003060A0D1013161A1D2023262A2D3033373A3D4044474A4D),
    .INIT_66(256'h878A8D909396999CA0A3A6A9ACAFB2B5B8BBBEC1C4C8CBCED1D4D7DADDE1E4E7),
    .INIT_67(256'h282B2E3134373A3D404345484B4E5154575A5D606366696C6F7275787B7E8184),
    .INIT_68(256'hCDD0D3D6D8DBDEE1E4E6E9ECEFF2F5F7FAFD000306090B0E1114171A1D202225),
    .INIT_69(256'h76797C7E818486898C8E919496999C9FA1A4A7A9ACAFB2B4B7BABDBFC2C5C8CA),
    .INIT_6A(256'h2326282B2D303235383A3D3F4244474A4C4F515457595C5E616466696C6E7174),
    .INIT_6B(256'hD4D6D9DBDEE0E3E5E7EAECEFF1F4F6F9FBFE000305070A0C0F111416191C1E21),
    .INIT_6C(256'h898B8E90929497999B9EA0A2A5A7A9ACAEB0B3B5B7BABCBFC1C3C6C8CACDCFD2),
    .INIT_6D(256'h424446484B4D4F515355585A5C5E60636567696C6E70727577797B7E80828487),
    .INIT_6E(256'hFF01030507090B0D0F111315181A1C1E20222426282A2D2F31333537393B3E40),
    .INIT_6F(256'hC0C2C4C6C8C9CBCDCFD1D3D5D7D9DBDDDFE1E3E5E7E9EBEDEFF1F3F5F7F9FBFD),
    .INIT_70(256'h8587898A8C8E9092939597999B9C9EA0A2A4A6A8A9ABADAFB1B3B5B6B8BABCBE),
    .INIT_71(256'h4E5052535557585A5C5D5F6162646667696B6D6E7072737577797A7C7E808283),
    .INIT_72(256'h1C1D1F202223252628292B2D2E303133343638393B3C3E404143444648494B4D),
    .INIT_73(256'hEDEEF0F1F3F4F5F7F8FAFBFDFEFF0102040507080A0B0D0E101113141617191A),
    .INIT_74(256'hC3C4C5C6C8C9CACCCDCECFD1D2D3D5D6D7D9DADBDDDEDFE1E2E3E5E6E8E9EAEC),
    .INIT_75(256'h9C9E9FA0A1A2A3A4A6A7A8A9AAABADAEAFB0B1B3B4B5B6B7B9BABBBCBEBFC0C1),
    .INIT_76(256'h7A7B7C7D7E7F8081828384868788898A8B8C8D8E8F9091929395969798999A9B),
    .INIT_77(256'h5C5D5E5F60616263636465666768696A6B6C6D6E6F7070717273747576777879),
    .INIT_78(256'h434344454646474849494A4B4C4D4D4E4F5051515253545555565758595A5B5B),
    .INIT_79(256'h2D2E2E2F2F3031313233333435353637373839393A3B3B3C3D3D3E3F40404142),
    .INIT_7A(256'h1C1C1C1D1D1E1E1F1F2021212222232324242525262627282829292A2B2B2C2C),
    .INIT_7B(256'h0E0F0F0F1010101111121212131314141415151616171717181819191A1A1B1B),
    .INIT_7C(256'h05050606060607070707080808080909090A0A0A0A0B0B0B0C0C0C0D0D0D0E0E),
    .INIT_7D(256'h0000010101010101010101020202020202020303030303030404040404050505),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0303030303020202020202020101010101010101010000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized27
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h335D660066AACE01C956C7FC6D5B381E4AA47039A5A63F8CAA99FF1B559820CD),
    .INITP_01(256'h0399559820ED25B800C956CFFE6556C7F8DAAD8FF195533FF1B56CE0792AB380),
    .INITP_02(256'hC3255338073514CFFE24ADB800CD55987864AB67C79955B1FE32D69C0064AA67),
    .INITP_03(256'h69C7E192AB33FF196B4CF9F325698FF8C95247CF9B553380E25548E079B54987),
    .INITP_04(256'h4C7FF192A923C0F36AA99C01CDAAB31FF8DA5A4E00392AA4E1C19A56D87F8C95),
    .INITP_05(256'h555B3810392552CC3F872575261FF1DB55267C1E66AA931FF8CD2B48E00736AA),
    .INITP_06(256'hF8FC6495524E3FFC765AA5B1C003992AADB1F07CCD2A9271FF0CDAAA4CF00F32),
    .INITP_07(256'h64B5569338FFF0E6DA92B6C707E0E64B55A4CE020399A554B33C00799A554931),
    .INITP_08(256'h1B4AB4DC7FF8E495524C7E3F192554B33C00799A554B338080E64B55A4CE0FC1),
    .INITP_09(256'h5D49C3F8669549381039B55499E01E64AAB661FF1C92A9667C1F1B6AA9338007),
    .INITP_0A(256'h00732AAD9E07892A931FFC64AAD9C00E25A9663FF192AACCF07CC955B71FF0C9),
    .INITP_0B(256'h9F3E65AD31FF99AA930FC72D5263FC36D4B3070E4AA93800E4B4B63FF19AAB67),
    .INITP_0C(256'h3B6A48FFE65159C039954987C3255B3C0E25548E039955B3E7C495263FE32D49),
    .INITP_0D(256'hE6D526003B496E0833553381CCAA4C0072D698FF1B5533C7CDAA4C3C33556600),
    .INITP_0E(256'h7FC6D52700E6AACC00CD7598039AA93C0E6D5B1FF995531FE36AB63FC6D54CFF),
    .INITP_0F(256'h56C7F8DAA4C0064AB63FC6D566083355B1FF32AA63F8CB4B381C4AA4F039B56C),
    .INIT_00(256'h806040404060A0E02080008020C060400000E000004080C0208000A040E0A080),
    .INIT_01(256'hA06040202020206080E040A020A040E0A0806040406080C00060C020C04000A0),
    .INIT_02(256'hC0804000E0C0C0E0002060C020A020C06000E0A0A0A0A0C0E02060C040C040E0),
    .INIT_03(256'hE08020C0A06060406060A0E02080E06000A04000E0C0C0C0C0E02060C020A020),
    .INIT_04(256'hE060E08020E0C0A0A0A0A0C00040A0008000A04000E0A0A0A0A0C0004080E060),
    .INIT_05(256'h80E060E08040E0C0A08080A0C0E02080E040C06000C080604040406080C00060),
    .INIT_06(256'hE02080008020C0804020000000206080E040A020C06000C0A080808080A0E020),
    .INIT_07(256'hA0E040A0008020C080402000E000004080C02080008020E080604020204060A0),
    .INIT_08(256'hC0004080E060E06000C080604020404080A0E040C020C04000C0806040406080),
    .INIT_09(256'h204060A00060C040E08020E0C0A0A0A0C0E00060A0208020A06020E0C0A0A0A0),
    .INIT_0A(256'h6080A0C00060C020A040E0A06020000000204080C02080008020C08040202020),
    .INIT_0B(256'h8080A0C0E02080E060E06020C080604040406080C00060C040E06020E0A08060),
    .INIT_0C(256'h202020406080E020A000A020E0A060402020204080A00060C040E06020E0A080),
    .INIT_0D(256'h20000000206080E040A020C06000C0A080606080A0C00060C040C040E0A06040),
    .INIT_0E(256'h2101E101012161A10161E16101A1612101E1E1E1012160A00060E06000C06040),
    .INIT_0F(256'hE1C1C1C1C1E12161A10181018121E1A1614141416181A1E141A121A121C18141),
    .INIT_10(256'h4101010101214181E141A121A14101C1816141416181A1E12181018101A16121),
    .INIT_11(256'hC1A1818181A1C10161A121A121C1612101C1C1C1C1E10141A1E161E16101A161),
    .INIT_12(256'h2101E1E1E1014181C1218101A141E1A161414141416181C12161E161E18121E1),
    .INIT_13(256'h01E1E1E1E1214181E141A121C16101C1A181818181A1E12161C121A141E18141),
    .INIT_14(256'h22020202226181E141A121A141E1A16141210121216181C12181018121C16121),
    .INIT_15(256'h020202224282C20282E26202A24202E2C2C2C2C2E20242A20262E28202C28242),
    .INIT_16(256'h626282A2E22262C242C242E2A262220202E202224282C22282028222C2824222),
    .INIT_17(256'hC2E2024282E242A222C26202C2A28262626282C2E242A2028202A24202C28282),
    .INIT_18(256'hC2024282E242C242C28222E2C2A28282A2C2E22282E242C242E2824222E2C2C2),
    .INIT_19(256'h0343830262E26202C2824222020202224282C22282E26202A26202E2C2A2A2C2),
    .INIT_1A(256'hE343C323C36303C383634343434363A3E32383038303A34303E3A3A38383A3C3),
    .INIT_1B(256'h238323C36323E3C3A3A3A3C3E30343A30363E38303C363430303E3E3032363A3),
    .INIT_1C(256'h03A36323E3C3C3A3C3C3032383C323A323A34303C383634343436383A30343A3),
    .INIT_1D(256'h4323E3E3C3C3E3032363C32383038323C383432303E3E303034383C323830383),
    .INIT_1E(256'h4444446484A4E444A4048404A444E4A484644444446484C40463C343C343E383),
    .INIT_1F(256'hA4C4044484E464C46404A44424E4C4C4C4C4E4044484E444C444C46404C4A464),
    .INIT_20(256'hA40464C464E4844404C4A4848484A4C4E42484E444C444E48424E4C4A4848484),
    .INIT_21(256'hC46404A4642404E4E4E404244484C42484048424C4642404C4C4A4A4C4E40444),
    .INIT_22(256'h854525252525456585C52585E565E58525C5856525250525254585C50565C444),
    .INIT_23(256'h454585A5E52585E565E56505A5652505E5E5E5E5052565A50565C545C56505C5),
    .INIT_24(256'h65C525A525A54505C58545452525254565A5E545A5058505A54505C585654545),
    .INIT_25(256'h6606A6664626060606264666A6E646A5058525C56525E5A58585658585A5E525),
    .INIT_26(256'h86666686A6C60646A60686E68626C6662606E6C6C6C6C6E60646A6E646C646C6),
    .INIT_27(256'h2666C626A626A64606A6664626060606264666A60646A626A626C66626E6A686),
    .INIT_28(256'hA74707C787674746466686A6E62686E646C646E68626E6C6866666666686A6E6),
    .INIT_29(256'h47676787C70747A70787078727C7672707C7C7A7A7C7C7072767C70787E76707),
    .INIT_2A(256'h87078707A747E7A7674727272727276787C70767C747A747C76727E7A7876747),
    .INIT_2B(256'hA8686848486887A7E72767C727A727A747E7A7672707E7C7C7E7E7274787C727),
    .INIT_2C(256'hC80868C848C848E8884808C8A88868688888A8E80868A80888E86808A84808C8),
    .INIT_2D(256'h6828E8C8C8A8A8C8E8082868C82888E868E88828E88868280808E80808284888),
    .INIT_2E(256'h89C92989E969E96909A94808C8A88868686888A8C8084888E848A828C848E8A8),
    .INIT_2F(256'hC989492929090929496989C92969C949A949C96909C98949290909E909092969),
    .INIT_30(256'h0969C9298909A929C9894909C9A9A9898989A9C9092989C92989098909A94909),
    .INIT_31(256'hCAAA8A8A8A8AAACA0A4A8AEA4AAA2AAA2ACA6909C989694929292929496989C9),
    .INIT_32(256'h0A8A0AAA2ACA8A4A0ACAAA8A8A8A8AAACAEA2A6AAA0A6AEA4AEA6A0AAA6A2AEA),
    .INIT_33(256'h2A4A6A8ACA0A4AAA0A6AEA6AEA8A2AEA8A6A2A0AEAEACAEAEA0A2A6AAAEA4AAA),
    .INIT_34(256'h6B0BCBAB6B4B4B4B4B4B6B8BABEB2B8BCB4BAB2BAB2BCB6B2BEBAB8B4B4B2A2A),
    .INIT_35(256'hAB0B8BEB6B0B8B2BEB8B4B2BEBCBCBABABCBCBEB0B4B8BCB2B8BEB6BEB8B0BAB),
    .INIT_36(256'h6C8C8CCCEC2C6CAC0C6CEC6CEB6B0BAB4B0BCB8B6B4B4B2B2B4B4B6BABCB0B6B),
    .INIT_37(256'hAC6C4C2C0CECECEC0C0C4C6CACEC2C8CEC4CCC4CCC6C0CAC6C2CECAC8C6C6C6C),
    .INIT_38(256'h8C0CAC4CEC8C4C0CCCAC8C6C6C6C6C8C8CCCEC2C6CAC0C6CEC4CCC6CEC8C2CEC),
    .INIT_39(256'h4DADED4DAD2DAD2DAD4DED8D4D0DCDAD8D6D6D4D6D6D8DADCD0D4D8DED4CAC0C),
    .INIT_3A(256'h0D2D4D6D8DCD0D6DCD2D8D0D8D0DAD2DED8D4D0DCDAD8D6D6D6D6D6D8DADED0D),
    .INIT_3B(256'hCEAEAEAEAEAECEEE0E4E8ECE2E6ECE4EAE2DCD4DED8D2DEDAD6D4D2D0D0DED0D),
    .INIT_3C(256'hCE8E4E2E0EEECECECEEEEE0E2E6EAEEE2E8EEE4EAE2EAE4ECE6E2ECE8E4E0EEE),
    .INIT_3D(256'h0FAF4F0FCF8F6E2E0E0EEEEEEE0E2E4E6E8ECE0E6ECE2E8EEE6EEE8E0EAE4E0E),
    .INIT_3E(256'hAF2FCF6F0FAF6F2FEFCFAF8F6F6F6F6F6F8FAFCF0F4F8FCF2F8FEF4FCF4FCF6F),
    .INIT_3F(256'hAF2FAF4FCF6F2FCF8F4F0FCFAF8F8F6F6F6F6F8FAFCF0F2F6FCF0F6FCF2FAF2F),
    .INIT_40(256'h0E4E8ECE2E6ECE4EAE2EAE4EEE8E2EEEAE6E2E0EEEEECECECEEE0E2E4E8ECE0E),
    .INIT_41(256'hED0D0D2D4D6DAEEE2E8EEE4ECE2EAE4ECE6E2ECE8E4E0EEECEAEAEAEAEAECEEE),
    .INIT_42(256'hEDAD8D6D6D6D6D6D8DADCD0D4D8DED2DAD0D8D0D8D2DCD6D0DCD8D6D4D2D0D0D),
    .INIT_43(256'hAD4DED8D4D0DCDAD8D6D6D4D6D6D8DADCD0D4D8DED4DAD2DAD2DAD4DEDAD4D0D),
    .INIT_44(256'h2C8CEC6CCC4CEC6C0CAC6C2CECCC8C8C6C6C6C6C8CACCC0C4C8CED4DAD0D8D0D),
    .INIT_45(256'h6C6C8CACEC2C6CAC0C6CCC4CCC4CEC8C2CECAC6C4C0C0CECECEC0C2C4C6CACEC),
    .INIT_46(256'h0BCBAB6B4B4B2B2B4B4B6B8CCC0C4CAC0C6CEC6CEC6C0CAC6C2CECCC8C8C6C6C),
    .INIT_47(256'h0B8BEB6BEB8B2BCB8B4B0BEBCBCBABABCBCBEB2B4B8BEB2B8B0B6BEB8B0BAB6B),
    .INIT_48(256'h2B4B4B8BABEB2B6BCB2BAB2BAB4BCB8B2BEBAB8B6B4B4B4B4B4B6BABCB0B6BAB),
    .INIT_49(256'h4AEAAA6A2A0AEAEACAEAEA0A2A6A8AEA2A8AEA6AEA6A0AAA4A0BCB8B6B4B2B2B),
    .INIT_4A(256'h2A6AAA0A6AEA4AEA6A0AAA6A2AEACAAA8A8A8A8AAACA0A4A8ACA2AAA0A8A0AAA),
    .INIT_4B(256'h89694929292A2A4A6A8ACA0A6ACA2AAA2AAA4AEA8A4A0ACAAA8A8A8A8AAACAEA),
    .INIT_4C(256'h49A90989098929C9892909C9A9898989A9A9C9094989C929A9098929C96909C9),
    .INIT_4D(256'h290909E90909294989C90969C949A949C96929C989694929090929294989C909),
    .INIT_4E(256'hE848C828A848E8884808C8A88869696989A9C90949A90969E969E98929C98969),
    .INIT_4F(256'h48280808E80808286888E82888E868E88828C8682808E8C8A8A8C8C8E82868A8),
    .INIT_50(256'h0848A80868E88808A86808E8A88888686888A8C8084888E848C848C86808C888),
    .INIT_51(256'hC7874727E7E7C7C7E7072767A7E747A727A828C86828E8A8886848486868A8C8),
    .INIT_52(256'h6787A7E72767C747A747C76707C7876727272727274767A7E747A70787078727),
    .INIT_53(256'h67E78707C7672707C7C7A7A7C7C7072767C72787078707A74707C78767674747),
    .INIT_54(256'hA6866666666686C6E62686E646C646E68727E7A787674747476787C70747A707),
    .INIT_55(256'hA6E62666C626A626A64606A6664626060606264666A60646A626A626C66626E6),
    .INIT_56(256'h46C646E6A64606E6C6C6C6C6E6062666C62686E68606A64606C6A68666668686),
    .INIT_57(256'hE5A58585658585A5E62666C6268606A646E6A6664626060606264666A60666C6),
    .INIT_58(256'h456585C50545A5058505A545E5A56545252525454585C50545A525A525C56525),
    .INIT_59(256'h0565C545C56505A5652505E5E5E5E5052565A50565E565E58525E5A585454545),
    .INIT_5A(256'hC56505C5854525250525256585C52585E565E58525C5856545252525254585C5),
    .INIT_5B(256'h04E4C4A4A4C4C4042464C42484048424C484442404E4E4E4042565A50565C545),
    .INIT_5C(256'h8484A4C4E42484E444C444E48424E4C4A4848484A4C4044484E464C46404A444),
    .INIT_5D(256'hA4C40464C444C444E4844404E4C4C4C4C4E42444A40464C464E4844404C4A484),
    .INIT_5E(256'hE444C444C46404C484644444446484A4E444A4048404A444E4A4846444444464),
    .INIT_5F(256'h038323C383430303E3E303234383C32383038323C3632303E3C3C3E3E3234484),
    .INIT_60(256'h4303A383634343436383C30343A323A323C3832303C3C3A3C3C3E32363A30383),
    .INIT_61(256'h632303E3E303034363C30383E36303A34303E3C3A3A3A3C3E32363C3238323A3),
    .INIT_62(256'hA38383A3A3E30343A30383038323E3A363434343436383C30363C323C343E3A3),
    .INIT_63(256'hA2A2C2E20262A20262E28222C2824222020202224383C30363E36303834303C3),
    .INIT_64(256'hC2E2224282E242C242E28222E2C2A28282A2C2E22282C242C242E2824202C2C2),
    .INIT_65(256'h82C20242A2028202A242E2C28262626282A2C20262C222A242E2824202E2C2C2),
    .INIT_66(256'h4282C22282028222C282422202E202022262A2E242C242C26222E2A282626282),
    .INIT_67(256'h82C20282E26202A24202E2C2C2C2C2E20242A20262E28202C282422202020222),
    .INIT_68(256'h61C12181018121C18161212101214161A1E142A222A242E28262220202022242),
    .INIT_69(256'h81E141A121C16121E1A181818181A1C10161C121A141E1814121E1E1E1E10121),
    .INIT_6A(256'h2181E161E16121C181614141414161A1E141A1018121C1814101E1E1E1012141),
    .INIT_6B(256'hA10161E161E1A14101E1C1C1C1C1012161C121A121A16101C1A1818181A1C1E1),
    .INIT_6C(256'h61A10181018121E1A1816141416181C10141A121A141E1814121010101014161),
    .INIT_6D(256'h81C121A121A141E1A1816141414161A1E12181018101A16121E1C1C1C1C1E121),
    .INIT_6E(256'h60C10161E16101A1612101E1E1E1012161A10161E16101A161210101E1012141),
    .INIT_6F(256'h60A0E040C040C06000C0A080606080A0C00060C020A040E08060200000002040),
    .INIT_70(256'hA0E02060E040C06000A080402020204060A0E020A000A020E080604020202040),
    .INIT_71(256'h80A0E02060E040C06000C080604040406080C02060E060E08020E0C0A0808080),
    .INIT_72(256'h20204080C02080008020C08040200000002060A0E040A020C06000C0A0806060),
    .INIT_73(256'hA0A0C0E02060A0208020A06000E0C0A0A0A0C0E02080E040C06000A060402020),
    .INIT_74(256'h6040406080C00040C020C040E0A080404020406080C00060E060E0804000C0A0),
    .INIT_75(256'h60402020406080E02080008020C080400000E000204080C0208000A040E0A080),
    .INIT_76(256'hE0A080808080A0C00060C020A040E0806020000000204080C02080008020E0A0),
    .INIT_77(256'h00C080604040406080C00060C040E08020E0C0A08080A0C0E04080E060E08020),
    .INIT_78(256'hE0804000C0A0A0A0A0E00040A0008000A04000C0A0A0A0A0C0E02080E060E060),
    .INIT_79(256'hA020C06020E0C0C0C0C0E00040A00060E08020E0A06060406060A0C02080E060),
    .INIT_7A(256'h40C040C06020E0C0A0A0A0A0E00060C020A020C0602000E0C0C0E0004080C020),
    .INIT_7B(256'h0040C020C06000C0806040406080A0E040A020A040E08060202020204060A0E0),
    .INIT_7C(256'hA0E040A0008020C080400000E000004060C02080008020E0A0604040406080A0),
    .INIT_7D(256'h80A0E02080E060E08020E0C080808080A0E02080E060E06020C0A06060606080),
    .INIT_7E(256'h606080C00040A020A020E0804020000000204080E020A020A04000C080606060),
    .INIT_7F(256'h60606060A0C02060E060E08020E0A080808080C0E02080E060E08020E0A08060),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized28
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hFFFFFF00000007FFFFFFFE0000000001FFFFFFFFFFFFC0000000000000000000),
    .INITP_01(256'hFFF8000FFFE0003FFFC0001FFFF00003FFFF00000FFFFF000007FFFFF0000007),
    .INITP_02(256'hFFC00FFC00FFE007FF001FFC007FF001FFE001FFE001FFF0007FFC001FFF8001),
    .INITP_03(256'hFC07F807F807F807F807F803FC03FE01FF007F803FE00FF801FF007FE00FFC00),
    .INITP_04(256'h0FE07F03F81FC0FE07F01F80FE03F80FC07F01FC03F80FE03FC07F00FE01FC03),
    .INITP_05(256'h7E0FC0F81F03F07E07C0FC1F81F81F03F03F03F03F03F03F03F03F81F81FC0FC),
    .INITP_06(256'hF0F83E0F83E0F83E0F83E07C1F07C1F03E0F83F07C1F83E07C1F83E07C0F81F0),
    .INITP_07(256'h7C3E1F0F87C1E0F0783E1F0783C1F0783E1F0783E0F07C1F0F83E0F07C1F07C1),
    .INITP_08(256'h7C0F83E0F83E0F83E0F83E1F07C1F07C1E0F83E1F07C1E0F83C1F0F83C1F0783),
    .INITP_09(256'hF07E07C0FC1F81F03E07E0FC1F03E07C0F83F07C0F83F07C1F83E0F81F07C1F0),
    .INITP_0A(256'h03F01FC0FE07F03F81FC0FE07E07F03F03F81F81F81F81F81F81F81F81F03F03),
    .INITP_0B(256'h803FC03FC03FC03FC03FC07F807F00FE01FC07F80FE03F807F01FC07E03F80FE),
    .INITP_0C(256'h7FF001FFC00FFE007FE007FE007FE00FFC01FF003FE00FF803FC01FF00FF807F),
    .INITP_0D(256'hF00007FFF8000FFFE0003FFF0003FFF0007FFC001FFF000FFF000FFF001FFC00),
    .INITP_0E(256'h00FFFFFFFFC0000001FFFFFFC000001FFFFFC00001FFFFE00001FFFF80001FFF),
    .INITP_0F(256'h00000000000000000000000000000000000000000007FFFFFFFFFFFF00000000),
    .INIT_00(256'h6663615E5C59575452504E4B49474543413F3C3B39373533312F2E2C2A282725),
    .INIT_01(256'hC9C6C2BFBBB8B4B1ADAAA7A3A09D9A9693908D8A8784817E7C797673706E6B68),
    .INIT_02(256'h4E4A45413C37332E2A26211D1914100C0804FFFBF7F3EFEBE7E4E0DCD8D4D1CD),
    .INIT_03(256'hF5EFEAE4DED9D3CEC8C3BDB8B3ADA8A39E98938E89847F7A75706B66615D5853),
    .INIT_04(256'hBDB6AFA9A29B958E88817B746E68615B554F48423C36302A241E18120C0600FB),
    .INIT_05(256'hA69E978F8780787069615A524B433C352D261F18110902FBF4EDE6DFD8D1CBC4),
    .INIT_06(256'hB1A89F978E857C746B635A5249413830271F170F06FEF6EEE6DED6CEC6BEB6AE),
    .INIT_07(256'hDDD3C9BFB6ACA2988F857C72685F564C433930271E140B02F9F0E7DED5CCC3BA),
    .INIT_08(256'h2A1F1409FEF4E9DED4C9BEB4A99F948A7F756A60564C41372D23190F05FBF1E7),
    .INIT_09(256'h988C8074695D5145392E22160BFFF4E8DDD1C6BBAFA4998E82776C61564B4035),
    .INIT_0A(256'h271A0D00F4E7DACDC0B4A79A8E8175685C4F43362A1E1105F9EDE0D4C8BCB0A4),
    .INIT_0B(256'hD7C9BBAD9F928476685A4D3F31241609FBEEE0D3C5B8AB9D908376695B4E4134),
    .INIT_0C(256'hA8998A7B6C5D4E4031221305F6E7D9CABCAD9F9082736557483A2C1E1001F3E5),
    .INIT_0D(256'h9A8A7A6A5A4A3A2A1A0AFBEBDBCCBCAC9D8D7E6E5F4F4031211203F4E4D5C6B7),
    .INIT_0E(256'hAC9B897968574635241302F2E1D0C0AF9F8E7D6D5D4C3C2B1B0BFAEADACABAAA),
    .INIT_0F(256'hDECCBAA8968472604E3D2B1907F6E4D2C1AF9E8C7B69584635241301F0DFCEBD),
    .INIT_10(256'h311E0BF8E5D2BFAC998673614E3B291603F1DECCB9A79482705D4B39271402F0),
    .INIT_11(256'hA38F7B67533F2B1804F0DCC8B5A18D7A66523F2B1804F1DECAB7A4907D6A5744),
    .INIT_12(256'h36210CF7E2CDB8A38E7A65503B2712FDE9D4C0AB97826E5945311C08F4E0CCB7),
    .INIT_13(256'hE9D2BCA6907B654F39230DF8E2CCB7A18B76604B35200AF5E0CAB5A08B75604B),
    .INIT_14(256'hBBA48D765F48311A03ECD6BFA8917B644E37210AF4DDC7B09A846E57412B15FF),
    .INIT_15(256'hAC947C644C341C05EDD5BDA68E765F472F1800E9D1BAA38B745D452E1700E9D2),
    .INIT_16(256'hBDA48B725940270EF6DDC4AB937A6249311800E7CFB69E866D553D250CF4DCC4),
    .INIT_17(256'hECD2B99F856B51371E04EAD1B79D846A51371E04EBD2B89F866D533A2108EFD6),
    .INIT_18(256'h3B2005EACFB59A7F644A2F14FADFC5AA90755B41260CF2D7BDA3896F553A2006),
    .INIT_19(256'hA88C7055391D01E6CAAE93775C402509EED2B79C80654A2F13F8DDC2A78C7156),
    .INIT_1A(256'h3317FADDC0A4876A4E3115F8DCBFA3866A4E3115F9DDC0A4886C503418FCE0C4),
    .INIT_1B(256'hDDBFA28466492B0DF0D2B5977A5D3F2205E7CAAD907255381BFEE1C4A78A6D50),
    .INIT_1C(256'hA58667492A0BEDCEB09173553618FADBBD9F8163442608EACCAE9072543719FB),
    .INIT_1D(256'h8A6A4A2B0BECCCAD8E6E4F3010F1D2B39374553617F8D9BA9B7C5D3E2001E2C3),
    .INIT_1E(256'h8C6C4B2B0AEAC9A98969482808E8C7A78767472707E7C7A78868482808E9C9A9),
    .INIT_1F(256'hAC8A69482605E4C2A1805F3E1DFBDAB99877563615F4D3B29171502F0FEECDAD),
    .INIT_20(256'hE8C6A4815F3D1BF9D7B492704E2C0AE8C6A583613F1DFCDAB89775533210EFCD),
    .INIT_21(256'h411EFBD8B5926F4B2805E3C09D7A573411EFCCA98664411FFCD9B794724F2D0B),
    .INIT_22(256'hB7936F4B2703DFBB97734F2B07E4C09C7955310EEAC7A3805C3915F2CFAB8865),
    .INIT_23(256'h4823FED9B4906B4621FCD8B38E6A4521FCD7B38E6A4621FDD9B4906C4723FFDB),
    .INIT_24(256'hF5CFAA845E3813EDC7A27C57310CE6C19B76502B06E1BB96714C2701DCB7926D),
    .INIT_25(256'hBE97704A23FDD6B089633C16EFC9A37C56300AE3BD97714B25FFD9B38D67411B),
    .INIT_26(256'hA17A522B03DCB58D663F17F0C9A27A532C05DEB79069421BF4CDA67F59320BE4),
    .INIT_27(256'hA0774F27FED6AE865E350DE5BD956D451DF5CDA57E562E06DEB78F674018F0C9),
    .INIT_28(256'hB88F663D14EBC29970471EF5CCA37A512900D7AE865D340CE3BB926A4119F0C8),
    .INIT_29(256'hEBC1976D431AF0C69C72491FF5CBA2784F25FBD2A87F552C03D9B0875D340BE2),
    .INIT_2A(256'h380DE2B88D62370DE2B88D63380EE3B98E643A0FE5BB90663C12E8BD93693F15),
    .INIT_2B(256'h9E72471BF0C4996D4217EBC095693E13E8BC91663B10E5BA8F64390EE3B88D63),
    .INIT_2C(256'h1DF1C4986C3F13E7BB8F63360ADEB2865A2E02D6AB7F5327FBCFA4784C21F5C9),
    .INIT_2D(256'hB5885A2D00D3A6794C20F3C6996C3F12E6B98C603306DAAD815427FBCFA27649),
    .INIT_2E(256'h653709DBAD805224F7C99B6E4012E5B78A5C2F01D4A7794C1EF1C497693C0FE2),
    .INIT_2F(256'h2DFED0A1734416E7B98A5C2DFFD1A2744618E9BB8D5F3103D5A7794A1DEFC193),
    .INIT_30(256'h0DDDAE7F4F20F1C292633405D6A778491AEBBC8D5E2F00D1A3744516E8B98A5C),
    .INIT_31(256'h03D3A3734313E3B3845424F4C494653505D5A6764617E7B8885929FACA9B6B3C),
    .INIT_32(256'h11E0B07F4E1DEDBC8C5B2AFAC999683807D7A7764616E5B5855424F4C4946433),
    .INIT_33(256'h3504D2A16F3E0DDBAA794716E5B4835120EFBE8D5C2BFAC998673605D4A37342),
    .INIT_34(256'h6F3D0BD9A7754311DFAD7B4917E5B3814F1EECBA885725F3C2905E2DFBCA9867),
    .INIT_35(256'hBF8C5926F4C18E5C29F6C4915E2CF9C794622FFDCB98663301CF9C6A3806D4A1),
    .INIT_36(256'h24F0BD895623EFBC895522EFBB885522EFBC885522EFBC895623F0BD8A5725F2),
    .INIT_37(256'h9D693501CD996531FDC995612DF9C6925E2AF6C38F5B28F4C08D5925F2BE8B57),
    .INIT_38(256'h2BF7C28D5824EFBB86511DE8B47F4B16E2AD794510DCA8733F0BD6A26E3A06D1),
    .INIT_39(256'hCD98622DF8C38D5823EEB8834E19E4AF7A4510DBA6713C07D29D6833FECA9560),
    .INIT_3A(256'h834D17E1AB753F09D39E6832FCC7915B25F0BA854F19E4AE79430ED8A36D3803),
    .INIT_3B(256'h4B14DEA7713A04CD97612AF4BE87511BE4AE78420BD59F6933FDC7915B25EEB9),
    .INIT_3C(256'h26EFB8814A12DBA46D36FFC9925B24EDB67F4812DBA46D3700C9935C25EFB881),
    .INIT_3D(256'h13DBA46C34FDC58E561FE7AF784109D29A632BF4BD854E17DFA8713A03CB945D),
    .INIT_3E(256'h12DAA16931F9C1895018E0A8703800C8905820E8B1794109D199612AF2BA824B),
    .INIT_3F(256'h22E9B0783F06CE955C24EBB27A4109D0985F27EEB67D450DD49C642BF3BB824A),
    .INIT_40(256'hB8EF255C93C900376DA4DB12487FB6ED245B92C9FF366DA4DB124A81B8EF265D),
    .INIT_41(256'hEE255B91C7FD33699FD50B4278AEE41B5187BEF42A6197CD043A71A7DE144B81),
    .INIT_42(256'h386DA3D80E4379AEE4194F85BAF0255B91C7FC32689ED3093F75ABE1174D83B9),
    .INIT_43(256'h95CAFE33689DD2073C71A6DB10457AAFE4194E83B8EE23588DC3F82D6298CD03),
    .INIT_44(256'h063A6EA2D60B3F73A8DC104579ADE2164B7FB4E81D5186BBEF24588DC2F72B60),
    .INIT_45(256'h8BBEF225598DC0F4285B8FC3F62A5E92C6F92D6195C9FD316599CD0135699DD1),
    .INIT_46(256'h25578ABDF0235689BCEF225588BCEF225588BBEF225589BCEF235689BDF02457),
    .INIT_47(256'hD406386A9CCF01336698CBFD2F6294C7F92C5E91C4F6295C8EC1F426598CBFF2),
    .INIT_48(256'h98CAFB2D5E90C2F3255788BAEC1E4F81B3E517497BADDF114375A7D90B3D6FA1),
    .INIT_49(256'h73A3D405366798C9FA2B5C8DBEEF205183B4E5164779AADB0D3E6FA1D2043567),
    .INIT_4A(256'h6494C4F4245485B5E5164676A7D707386899C9FA2A5B8CBCED1D4E7FB0E01142),
    .INIT_4B(256'h6B9BCAFA295988B8E7174676A6D505356594C4F4245484B3E3134373A3D30333),
    .INIT_4C(256'h8AB9E8164574A3D1002F5E8DBCEB1A4978A7D605346392C2F1204F7FAEDD0D3C),
    .INIT_4D(256'hC1EF1D4A79A7D503315F8DBBE9184674A2D1FF2D5C8AB9E7164473A1D0FE2D5C),
    .INIT_4E(256'h0F3C6997C4F11E4C79A7D4012F5C8AB7E512406E9BC9F7245280ADDB09376593),
    .INIT_4F(256'h76A2CFFB275481ADDA0633608CB9E6123F6C99C6F3204C79A6D3002D5A88B5E2),
    .INIT_50(256'hF5214C78A4CFFB27537FABD6022E5A86B2DE0A36638FBBE7133F6C98C4F11D49),
    .INIT_51(256'h8DB8E30E39648FBAE5103B6691BCE8133E6995C0EB17426D99C4F01B47729EC9),
    .INIT_52(256'h3F6993BDE8123C6690BBE50F3A648EB9E30E38638DB8E20D37628DB8E20D3863),
    .INIT_53(256'h0B345D87B0D9032C557FA8D2FB254F78A2CBF51F49729CC6F01A436D97C1EB15),
    .INIT_54(256'hF019416A92BBE30C345D86AED70029517AA3CCF51E477099C2EB143D668FB8E2),
    .INIT_55(256'hF01840678FB7DE062E567EA5CDF51D456D95BDE50D355E86AED6FE274F77A0C8),
    .INIT_56(256'h0B32597FA6CDF41B426990B7DE052C537AA2C9F0173F668DB5DC032B527AA1C9),
    .INIT_57(256'h41678DB3D9FF254B7197BDE30A30567CA3C9EF163C6389B0D6FD234A7097BEE4),
    .INIT_58(256'h92B7DC01274C7196BBE1062B50769BC1E60C31577CA2C7ED13385E84AACFF51B),
    .INIT_59(256'hFF23476C90B4D9FD21466A8EB3D7FC21456A8EB3D8FC21466B90B4D9FE23486D),
    .INIT_5A(256'h88ABCFF215395C80A3C7EA0E3155799CC0E4072B4F7397BBDF03274B6F93B7DB),
    .INIT_5B(256'h2D4F7294B7D9FC1F416486A9CCEF1134577A9DC0E305284B6F92B5D8FB1E4165),
    .INIT_5C(256'hEF1032537597B8DAFC1D3F6183A5C6E80A2C4E7092B4D7F91B3D5F81A4C6E80B),
    .INIT_5D(256'hCDEE0F2F507191B2D3F41536567798B9DAFB1D3E5F80A1C2E4052648698AACCD),
    .INIT_5E(256'hC9E90828486888A7C7E70727476787A7C7E80828486989A9C9EA0A2B4B6C8CAD),
    .INIT_5F(256'hE201203E5D7C9BBAD9F81736557493B3D2F110304F6E8EADCCEC0B2B4A6A8AA9),
    .INIT_60(256'h1937547290AECCEA08264463819FBDDBFA1836557391B0CEED0B2A496786A5C3),
    .INIT_61(256'h6D8AA7C4E1FE1B38557290ADCAE705223F5D7A97B5D2F00D2B496684A2BFDDFB),
    .INIT_62(256'hE0FC1834506C88A4C0DDF915314E6A86A3BFDCF815314E6A87A4C0DDFA173350),
    .INIT_63(256'h718CA7C2DDF8132F4A65809CB7D2EE0925405C7793AECAE6011D3955708CA8C4),
    .INIT_64(256'h203A556F89A3BDD7F20C26415B7590AAC5DFFA142F4A647F9AB5CFEA05203B56),
    .INIT_65(256'hEF08213A536D869FB8D2EB041E37516A849DB7D1EA041E37516B859FB9D2EC06),
    .INIT_66(256'hDCF40C253D556D869EB6CFE700183149627A93ABC4DDF60E274059728BA4BDD6),
    .INIT_67(256'hE900172E455D748BA3BAD1E900182F475F768EA6BDD5ED051C344C647C94ACC4),
    .INIT_68(256'h152B41576E849AB0C7DDF40A21374E647B91A8BFD6EC031A31485F768DA4BBD2),
    .INIT_69(256'h60758BA0B5CAE0F50A20354B60768BA1B7CCE2F80D23394F657B90A6BCD2E9FF),
    .INIT_6A(256'hCCE0F4081C3145596E8297ABC0D4E9FD12273B50657A8EA3B8CDE2F70C21364B),
    .INIT_6B(256'h576A7D90A4B7CADEF104182B3F52667A8DA1B5C8DCF004182B3F53677B8FA3B7),
    .INIT_6C(256'h021427394B5D708294A7B9CCDEF10316293B4E61738699ACBFD2E5F80B1E3144),
    .INIT_6D(256'hCEDFF0011324354658697B8C9EAFC1D2E4F607192B3D4E60728496A8BACCDEF0),
    .INIT_6E(256'hBACADAEAFA0B1B2B3C4C5D6D7D8E9FAFC0D0E1F20213243546576879899BACBD),
    .INIT_6F(256'hC6D5E4F403122131404F5F6E7E8D9DACBCCCDBEBFB0A1A2A3A4A5A6A7A8A9AAA),
    .INIT_70(256'hF301101E2C3A4857657382909FADBCCAD9E7F605132231404E5D6C7B8A99A8B7),
    .INIT_71(256'h414E5B697683909DABB8C5D3E0EEFB091624313F4D5A687684929FADBBC9D7E5),
    .INIT_72(256'hB0BCC8D4E0EDF905111E2A36434F5C6875818E9AA7B4C0CDDAE7F4000D1A2734),
    .INIT_73(256'h404B56616C77828E99A4AFBBC6D1DDE8F4FF0B16222E3945515D6974808C98A4),
    .INIT_74(256'hF1FB050F19232D37414C56606A757F8A949FA9B4BEC9D4DEE9F4FE09141F2A35),
    .INIT_75(256'hC3CCD5DEE7F0F9020B141E273039434C565F68727C858F98A2ACB6BFC9D3DDE7),
    .INIT_76(256'hB6BEC6CED6DEE6EEF6FE060F171F2730384149525A636B747C858E979FA8B1BA),
    .INIT_77(256'hCBD1D8DFE6EDF4FB020911181F262D353C434B525A6169707880878F979EA6AE),
    .INIT_78(256'h00060C12181E242A30363C42484F555B61686E747B81888E959BA2A9AFB6BDC4),
    .INIT_79(256'h585D61666B70757A7F84898E93989EA3A8ADB3B8BDC3C8CED3D9DEE4EAEFF5FB),
    .INIT_7A(256'hD1D4D8DCE0E4E7EBEFF3F7FBFF04080C1014191D21262A2E33373C41454A4E53),
    .INIT_7B(256'h6B6E707376797C7E8184878A8D9093969A9DA0A3A7AAADB1B4B8BBBFC2C6C9CD),
    .INIT_7C(256'h27282A2C2E2F31333537393B3C3F41434547494B4E50525457595C5E61636668),
    .INIT_7D(256'h04050506070708090A0B0B0C0D0E0F10111214151617181A1B1C1E1F21222425),
    .INIT_7E(256'h0303020202010101000000000000000000000000000000010101020202030304),
    .INIT_7F(256'h2422211F1E1C1B1A18171615141211100F0E0D0C0B0B0A090807070605050404),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized29
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h2D228235BC446B48772969111EDE227C3AEFB4877096EBEE1DF5DA451478A209),
    .INITP_01(256'h787878B78A77777728D69690EEBBEE53E2DF2227DA5BC4447B4B75DF70F14641),
    .INITP_02(256'hC45A5CA20DE1E5111106909608D775C8B7CB487B4B84B84FB44FB04FB04B84B8),
    .INITP_03(256'h886AC2213168B444272D116087913C229AEE962379443C2720E1116D60834B44),
    .INITP_04(256'hBDE68B3DE6895A130B45D2F27BC0EE8841DEEF7443D2E9D845DD11634AC22D16),
    .INITP_05(256'h4194C90932F45D0B3E6B5DB24210B3668E5EF7A710BBF127A610B18C6845D167),
    .INITP_06(256'hBD71E85F78F7D0999999990B9982E3DE3A17B6DBD18217B6F4217A2F45306248),
    .INITP_07(256'h4E609E997D7AFA0A1433A8AE4146FAE617D0CCBD2514385F66FA16D0A285E3D9),
    .INITP_08(256'h33A13333333217DE3DF42F1D7B378F428A16D0BECDF43851497A6617D0CEBEC5),
    .INITP_09(256'h849B75ACF9A1745E9921265304248C1945E8BD085EDBD08317B6DBD0B8F78E83),
    .INITP_0A(256'hBC9E9745A190B522CF79A2CF7BCD17442C631A10CBC91FBA11CBDEF4E2CD9A10),
    .INITP_0B(256'hC20D1169C8445A2D190886AC22D16886A58D117744372E97845DEEF70422EE07),
    .INITP_0C(256'h20D212C111114F0F608A74B44645A5820D6D110E09C878453D88D2EEB2887913),
    .INITP_0D(256'hEE12D2D629DDDDDCA3DA3C3C3C3A43A41BE41BE45BE43A43A5BC25A7DA275DD6),
    .INITP_0E(256'hF1112D29DC25AC447B5882896904C51E1DF75DA5BC4447B4B7C889F68F94EFBA),
    .INITP_0F(256'h6FBEE0DDDC3E44F877760EFBED208A3C5144B75F70EFAED21DC25BEEB87C88F6),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0101010101010101010101010101010101010101010101010101010101010100),
    .INIT_05(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_06(256'h0202020202020202020202020202020202020202020101010101010101010101),
    .INIT_07(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_08(256'h0303030303030303030303030303030303030303030303030303030303020202),
    .INIT_09(256'h0404040404040404040404040403030303030303030303030303030303030303),
    .INIT_0A(256'h0505050504040404040404040404040404040404040404040404040404040404),
    .INIT_0B(256'h0505050505050505050505050505050505050505050505050505050505050505),
    .INIT_0C(256'h0606060606060606060606060606060606060606060606060606060606060505),
    .INIT_0D(256'h0707070707070707070707070707070707070707070707070707070606060606),
    .INIT_0E(256'h0808080808080808080808080808080808080808080808080808070707070707),
    .INIT_0F(256'h0909090909090909090909090909090909090909090909090909090908080808),
    .INIT_10(256'h0B0B0B0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A09),
    .INIT_11(256'h0C0C0C0C0C0C0C0C0C0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B),
    .INIT_12(256'h0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C),
    .INIT_13(256'h0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0D0D0D0D0D0D0D0D0D),
    .INIT_14(256'h1010101010101010100F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0E),
    .INIT_15(256'h1111111111111111111111111111111111111110101010101010101010101010),
    .INIT_16(256'h1313131313131313121212121212121212121212121212121212121212111111),
    .INIT_17(256'h1414141414141414141414141414141414141414131313131313131313131313),
    .INIT_18(256'h1616161616161616161616161515151515151515151515151515151515151515),
    .INIT_19(256'h1818181818181817171717171717171717171717171717171716161616161616),
    .INIT_1A(256'h1A1A191919191919191919191919191919191919181818181818181818181818),
    .INIT_1B(256'h1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A),
    .INIT_1C(256'h1D1D1D1D1D1D1D1D1D1D1D1D1D1D1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1B),
    .INIT_1D(256'h1F1F1F1F1F1F1F1F1F1F1F1F1F1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1D1D),
    .INIT_1E(256'h21212121212121212121212121202020202020202020202020202020201F1F1F),
    .INIT_1F(256'h2323232323232323232323232322222222222222222222222222222222212121),
    .INIT_20(256'h2525252525252525252525252525252424242424242424242424242424242323),
    .INIT_21(256'h2828272727272727272727272727272727262626262626262626262626262626),
    .INIT_22(256'h2A2A2A2A2A2A2929292929292929292929292929282828282828282828282828),
    .INIT_23(256'h2C2C2C2C2C2C2C2C2C2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A2A2A2A2A2A2A2A2A),
    .INIT_24(256'h2E2E2E2E2E2E2E2E2E2E2E2E2E2E2D2D2D2D2D2D2D2D2D2D2D2D2D2D2C2C2C2C),
    .INIT_25(256'h313131313130303030303030303030303030302F2F2F2F2F2F2F2F2F2F2F2F2F),
    .INIT_26(256'h3333333333333333333333323232323232323232323232323131313131313131),
    .INIT_27(256'h3636363635353535353535353535353535343434343434343434343434343333),
    .INIT_28(256'h3838383838383838383838373737373737373737373737373636363636363636),
    .INIT_29(256'h3B3B3B3B3B3B3A3A3A3A3A3A3A3A3A3A3A3A3939393939393939393939393938),
    .INIT_2A(256'h3E3E3D3D3D3D3D3D3D3D3D3D3D3D3C3C3C3C3C3C3C3C3C3C3C3C3B3B3B3B3B3B),
    .INIT_2B(256'h404040404040404040403F3F3F3F3F3F3F3F3F3F3F3F3E3E3E3E3E3E3E3E3E3E),
    .INIT_2C(256'h4343434343434342424242424242424242424241414141414141414141414040),
    .INIT_2D(256'h4646464646454545454545454545454544444444444444444444444343434343),
    .INIT_2E(256'h4949494848484848484848484848474747474747474747474746464646464646),
    .INIT_2F(256'h4C4B4B4B4B4B4B4B4B4B4B4B4A4A4A4A4A4A4A4A4A4A4A4A4949494949494949),
    .INIT_30(256'h4F4E4E4E4E4E4E4E4E4E4E4E4D4D4D4D4D4D4D4D4D4D4D4C4C4C4C4C4C4C4C4C),
    .INIT_31(256'h525151515151515151515150505050505050505050504F4F4F4F4F4F4F4F4F4F),
    .INIT_32(256'h5554545454545454545454535353535353535353535352525252525252525252),
    .INIT_33(256'h5858575757575757575757575656565656565656565655555555555555555555),
    .INIT_34(256'h5B5B5B5A5A5A5A5A5A5A5A5A5A59595959595959595959585858585858585858),
    .INIT_35(256'h5E5E5E5E5D5D5D5D5D5D5D5D5D5D5C5C5C5C5C5C5C5C5C5C5C5B5B5B5B5B5B5B),
    .INIT_36(256'h616161616161606060606060606060605F5F5F5F5F5F5F5F5F5F5E5E5E5E5E5E),
    .INIT_37(256'h6464646464646464636363636363636363636262626262626262626261616161),
    .INIT_38(256'h6867676767676767676767666666666666666666666565656565656565656564),
    .INIT_39(256'h6B6B6B6B6A6A6A6A6A6A6A6A6A6A696969696969696969696868686868686868),
    .INIT_3A(256'h6E6E6E6E6E6E6E6E6D6D6D6D6D6D6D6D6D6C6C6C6C6C6C6C6C6C6C6B6B6B6B6B),
    .INIT_3B(256'h7272717171717171717171707070707070707070706F6F6F6F6F6F6F6F6F6E6E),
    .INIT_3C(256'h7575757575757474747474747474747373737373737373737372727272727272),
    .INIT_3D(256'h7978787878787878787877777777777777777776767676767676767676757575),
    .INIT_3E(256'h7C7C7C7C7C7B7B7B7B7B7B7B7B7B7B7A7A7A7A7A7A7A7A7A7979797979797979),
    .INIT_3F(256'h807F7F7F7F7F7F7F7F7F7E7E7E7E7E7E7E7E7E7D7D7D7D7D7D7D7D7D7C7C7C7C),
    .INIT_40(256'h7272727272727373737373737373737374747474747474747475757575757575),
    .INIT_41(256'h6E6F6F6F6F6F6F6F6F6F70707070707070707070717171717171717171727272),
    .INIT_42(256'h6B6B6B6B6C6C6C6C6C6C6C6C6C6C6D6D6D6D6D6D6D6D6D6E6E6E6E6E6E6E6E6E),
    .INIT_43(256'h68686868686868696969696969696969696A6A6A6A6A6A6A6A6A6A6B6B6B6B6B),
    .INIT_44(256'h6565656565656565656566666666666666666666676767676767676767676868),
    .INIT_45(256'h6161616262626262626262626263636363636363636363646464646464646464),
    .INIT_46(256'h5E5E5E5E5E5F5F5F5F5F5F5F5F5F5F6060606060606060606061616161616161),
    .INIT_47(256'h5B5B5B5B5B5B5C5C5C5C5C5C5C5C5C5C5C5D5D5D5D5D5D5D5D5D5D5E5E5E5E5E),
    .INIT_48(256'h5858585858585858595959595959595959595A5A5A5A5A5A5A5A5A5A5B5B5B5B),
    .INIT_49(256'h5555555555555555555656565656565656565657575757575757575757585858),
    .INIT_4A(256'h5252525252525252525353535353535353535353545454545454545454545555),
    .INIT_4B(256'h4F4F4F4F4F4F4F4F4F5050505050505050505050515151515151515151515252),
    .INIT_4C(256'h4C4C4C4C4C4C4C4C4D4D4D4D4D4D4D4D4D4D4D4E4E4E4E4E4E4E4E4E4E4E4F4F),
    .INIT_4D(256'h494949494949494A4A4A4A4A4A4A4A4A4A4A4A4B4B4B4B4B4B4B4B4B4B4B4C4C),
    .INIT_4E(256'h4646464646464747474747474747474747484848484848484848484849494949),
    .INIT_4F(256'h4343434344444444444444444444444545454545454545454545464646464646),
    .INIT_50(256'h4041414141414141414141414242424242424242424242424343434343434343),
    .INIT_51(256'h3E3E3E3E3E3E3E3E3E3F3F3F3F3F3F3F3F3F3F3F3F4040404040404040404040),
    .INIT_52(256'h3B3B3B3B3B3C3C3C3C3C3C3C3C3C3C3C3C3D3D3D3D3D3D3D3D3D3D3D3D3E3E3E),
    .INIT_53(256'h393939393939393939393939393A3A3A3A3A3A3A3A3A3A3A3A3B3B3B3B3B3B3B),
    .INIT_54(256'h3636363636363637373737373737373737373737383838383838383838383838),
    .INIT_55(256'h3334343434343434343434343434353535353535353535353535353636363636),
    .INIT_56(256'h3131313131313132323232323232323232323232333333333333333333333333),
    .INIT_57(256'h2F2F2F2F2F2F2F2F2F2F2F2F3030303030303030303030303030313131313131),
    .INIT_58(256'h2C2C2C2D2D2D2D2D2D2D2D2D2D2D2D2D2D2E2E2E2E2E2E2E2E2E2E2E2E2E2E2F),
    .INIT_59(256'h2A2A2A2A2A2A2A2A2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C2C2C2C2C2C2C2C2C2C),
    .INIT_5A(256'h282828282828282828282829292929292929292929292929292A2A2A2A2A2A2A),
    .INIT_5B(256'h2626262626262626262626262626272727272727272727272727272727282828),
    .INIT_5C(256'h2324242424242424242424242424242425252525252525252525252525252526),
    .INIT_5D(256'h2121222222222222222222222222222222222323232323232323232323232323),
    .INIT_5E(256'h1F1F202020202020202020202020202020202121212121212121212121212121),
    .INIT_5F(256'h1D1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1F1F1F1F1F1F1F1F1F1F1F1F1F1F),
    .INIT_60(256'h1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D),
    .INIT_61(256'h1A1A1A1A1A1A1A1A1A1A1A1A1A1A1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B),
    .INIT_62(256'h18181818181818181818181919191919191919191919191919191919191A1A1A),
    .INIT_63(256'h1616161616161717171717171717171717171717171717171818181818181818),
    .INIT_64(256'h1515151515151515151515151515151515151516161616161616161616161616),
    .INIT_65(256'h1313131313131313131313141414141414141414141414141414141414141415),
    .INIT_66(256'h1111121212121212121212121212121212121212121212131313131313131313),
    .INIT_67(256'h1010101010101010101010101111111111111111111111111111111111111111),
    .INIT_68(256'h0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F10101010101010101010),
    .INIT_69(256'h0D0D0D0D0D0D0D0D0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E),
    .INIT_6A(256'h0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D),
    .INIT_6B(256'h0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0C0C0C0C0C0C0C0C0C0C),
    .INIT_6C(256'h0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0B0B0B0B),
    .INIT_6D(256'h0808080909090909090909090909090909090909090909090909090909090909),
    .INIT_6E(256'h0707070707080808080808080808080808080808080808080808080808080808),
    .INIT_6F(256'h0606060607070707070707070707070707070707070707070707070707070707),
    .INIT_70(256'h0506060606060606060606060606060606060606060606060606060606060606),
    .INIT_71(256'h0505050505050505050505050505050505050505050505050505050505050505),
    .INIT_72(256'h0404040404040404040404040404040404040404040404040404040505050505),
    .INIT_73(256'h0303030303030303030303030303030303030404040404040404040404040404),
    .INIT_74(256'h0202030303030303030303030303030303030303030303030303030303030303),
    .INIT_75(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_76(256'h0101010101010101010102020202020202020202020202020202020202020202),
    .INIT_77(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_78(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized3
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'hFFFF800000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_05(256'h000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_06(256'hF000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_08(256'h1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000),
    .INITP_0B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0C(256'h00000000000000000000000000000000000000000000000000000000000003FF),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0101010101000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0202020202020202020202010101010101010101010101010101010101010101),
    .INIT_03(256'h0404040404040303030303030303030303030303030303020202020202020202),
    .INIT_04(256'h0606060606060606060605050505050505050505050505050404040404040404),
    .INIT_05(256'h0909090909090909080808080808080808080807070707070707070707070606),
    .INIT_06(256'h0D0D0C0C0C0C0C0C0C0C0C0B0B0B0B0B0B0B0B0B0A0A0A0A0A0A0A0A0A0A0909),
    .INIT_07(256'h1111101010101010100F0F0F0F0F0F0F0F0E0E0E0E0E0E0E0E0E0D0D0D0D0D0D),
    .INIT_08(256'h1515151515141414141414141313131313131312121212121212111111111111),
    .INIT_09(256'h1A1A1A1A19191919191919181818181818171717171717161616161616161515),
    .INIT_0A(256'h201F1F1F1F1F1F1E1E1E1E1E1E1D1D1D1D1D1D1C1C1C1C1C1B1B1B1B1B1B1A1A),
    .INIT_0B(256'h2626252525252524242424242323232323222222222222212121212120202020),
    .INIT_0C(256'h2C2C2C2C2B2B2B2B2B2A2A2A2A2A292929292928282828282727272727262626),
    .INIT_0D(256'h3333333332323232323131313130303030302F2F2F2F2E2E2E2E2E2D2D2D2D2C),
    .INIT_0E(256'h3B3B3A3A3A3A3939393939383838383737373736363636353535353534343434),
    .INIT_0F(256'h43434342424241414141404040403F3F3F3F3E3E3E3E3D3D3D3D3C3C3C3C3B3B),
    .INIT_10(256'h4C4B4B4B4B4A4A4A494949494848484847474746464646454545454444444443),
    .INIT_11(256'h555454545453535352525252515151505050504F4F4F4E4E4E4E4D4D4D4C4C4C),
    .INIT_12(256'h5E5E5E5D5D5D5C5C5C5C5B5B5B5A5A5A59595959585858575757565656565555),
    .INIT_13(256'h68686867676767666666656565646464636363626262616161606060605F5F5F),
    .INIT_14(256'h73737272727171717070706F6F6F6E6E6E6D6D6D6C6C6C6B6B6B6A6A6A696969),
    .INIT_15(256'h7E7E7D7D7D7C7C7C7B7B7B7A7A7A797979787877777776767675757574747473),
    .INIT_16(256'h8A898989888888878786868685858584848483838282828181818080807F7F7E),
    .INIT_17(256'h96959595949494939392929291919090908F8F8F8E8E8D8D8D8C8C8C8B8B8A8A),
    .INIT_18(256'hA2A2A2A1A1A0A0A09F9F9E9E9E9D9D9C9C9C9B9B9A9A9A999999989897979796),
    .INIT_19(256'hAFAFAFAEAEADADADACACABABAAAAAAA9A9A8A8A8A7A7A6A6A6A5A5A4A4A4A3A3),
    .INIT_1A(256'hBDBDBCBCBBBBBABABAB9B9B8B8B7B7B7B6B6B5B5B4B4B4B3B3B2B2B1B1B1B0B0),
    .INIT_1B(256'hCBCACACAC9C9C8C8C7C7C7C6C6C5C5C4C4C3C3C3C2C2C1C1C0C0C0BFBFBEBEBD),
    .INIT_1C(256'hD9D9D8D8D8D7D7D6D6D5D5D4D4D3D3D3D2D2D1D1D0D0CFCFCFCECECDCDCCCCCB),
    .INIT_1D(256'hE8E8E7E7E6E6E6E5E5E4E4E3E3E2E2E1E1E0E0DFDFDEDEDEDDDDDCDCDBDBDADA),
    .INIT_1E(256'hF8F7F7F6F6F5F5F4F4F3F3F2F2F1F1F0F0F0EFEFEEEEEDEDECECEBEBEAEAE9E9),
    .INIT_1F(256'h0807070606050504040303020201010000FFFFFEFEFDFDFCFCFBFBFAFAF9F9F8),
    .INIT_20(256'h18171716161515141413131212111110100F0F0E0E0D0D0C0C0B0B0A0A090908),
    .INIT_21(256'h2928282727262625242423232222212120201F1F1E1E1D1D1C1C1B1B1A1A1918),
    .INIT_22(256'h3A393938383737363635353433333232313130302F2F2E2E2D2C2C2B2B2A2A29),
    .INIT_23(256'h4C4B4B4A4949484847474646454444434342424141403F3F3E3E3D3D3C3C3B3B),
    .INIT_24(256'h5E5D5D5C5B5B5A5A5959585757565655555453535252515150504F4E4E4D4D4C),
    .INIT_25(256'h70706F6F6E6D6D6C6C6B6A6A69696868676666656564636362626161605F5F5E),
    .INIT_26(256'h838382818180807F7E7E7D7D7C7B7B7A7A797978777776767574747373727171),
    .INIT_27(256'h9796959594949392929191908F8F8E8D8D8C8C8B8A8A89898887878686858484),
    .INIT_28(256'hAAAAA9A9A8A7A7A6A5A5A4A4A3A2A2A1A1A09F9F9E9D9D9C9C9B9A9A99989897),
    .INIT_29(256'hBFBEBDBDBCBCBBBABAB9B8B8B7B6B6B5B5B4B3B3B2B1B1B0AFAFAEAEADACACAB),
    .INIT_2A(256'hD3D3D2D1D1D0CFCFCECDCDCCCCCBCACAC9C8C8C7C6C6C5C4C4C3C3C2C1C1C0BF),
    .INIT_2B(256'hE8E8E7E6E6E5E4E4E3E2E2E1E0E0DFDEDEDDDCDCDBDBDAD9D9D8D7D7D6D5D5D4),
    .INIT_2C(256'hFEFDFCFCFBFAFAF9F8F8F7F6F6F5F4F4F3F2F2F1F0F0EFEEEEEDECECEBEAEAE9),
    .INIT_2D(256'h141312121110100F0E0D0D0C0B0B0A090908070706050504030302010100FFFF),
    .INIT_2E(256'h2A29282827262625242423222221201F1F1E1D1D1C1B1B1A1918181716161514),
    .INIT_2F(256'h41403F3E3E3D3C3C3B3A3939383737363534343332323130302F2E2D2D2C2B2B),
    .INIT_30(256'h5757565555545352525150504F4E4D4D4C4B4B4A494848474646454443434241),
    .INIT_31(256'h6F6E6D6D6C6B6A6A696868676665656463626261605F5F5E5D5D5C5B5A5A5958),
    .INIT_32(256'h878685848483828181807F7E7E7D7C7B7B7A7978787776757574737372717070),
    .INIT_33(256'h9F9E9D9C9C9B9A99999897969695949393929190908F8E8D8D8C8B8A8A898887),
    .INIT_34(256'hB7B6B6B5B4B3B2B2B1B0AFAFAEADACACABAAA9A9A8A7A6A5A5A4A3A2A2A1A09F),
    .INIT_35(256'hD0CFCECECDCCCBCACAC9C8C7C7C6C5C4C3C3C2C1C0C0BFBEBDBCBCBBBAB9B9B8),
    .INIT_36(256'hE9E8E7E7E6E5E4E3E3E2E1E0E0DFDEDDDCDCDBDAD9D8D8D7D6D5D5D4D3D2D1D1),
    .INIT_37(256'h02020100FFFEFEFDFCFBFAFAF9F8F7F6F6F5F4F3F2F2F1F0EFEFEEEDECEBEBEA),
    .INIT_38(256'h1C1B1B1A1918171716151413131211100F0E0E0D0C0B0A0A0908070606050403),
    .INIT_39(256'h3635353433323131302F2E2D2C2C2B2A2928282726252424232221201F1F1E1D),
    .INIT_3A(256'h51504F4E4D4D4C4B4A4948484746454443434241403F3F3E3D3C3B3A3A393837),
    .INIT_3B(256'h6B6B6A6968676666656463626161605F5E5D5C5C5B5A59585757565554535252),
    .INIT_3C(256'h8686858483828181807F7E7D7C7B7B7A7978777676757473727170706F6E6D6C),
    .INIT_3D(256'hA2A1A09F9E9D9D9C9B9A9998989796959493929291908F8E8D8C8C8B8A898887),
    .INIT_3E(256'hBDBDBCBBBAB9B8B7B6B6B5B4B3B2B1B0B0AFAEADACABAAAAA9A8A7A6A5A4A3A3),
    .INIT_3F(256'hD9D8D8D7D6D5D4D3D2D1D1D0CFCECDCCCBCACAC9C8C7C6C5C4C3C3C2C1C0BFBE),
    .INIT_40(256'hB8B9BABBBCBDBDBEBFC0C1C2C3C3C4C5C6C7C8C9CACACBCCCDCECFD0D1D1D2D3),
    .INIT_41(256'h9D9D9E9FA0A1A2A3A3A4A5A6A7A8A9AAAAABACADAEAFB0B0B1B2B3B4B5B6B6B7),
    .INIT_42(256'h818283848586868788898A8B8C8C8D8E8F9091929293949596979898999A9B9C),
    .INIT_43(256'h666768696A6B6B6C6D6E6F7070717273747576767778797A7B7B7C7D7E7F8081),
    .INIT_44(256'h4C4D4D4E4F5051525253545556575758595A5B5C5C5D5E5F6061616263646566),
    .INIT_45(256'h313233343535363738393A3A3B3C3D3E3F3F4041424343444546474848494A4B),
    .INIT_46(256'h1718191A1B1B1C1D1E1F1F2021222324242526272828292A2B2C2C2D2E2F3031),
    .INIT_47(256'hFEFEFF0001020203040506060708090A0A0B0C0D0E0E0F101112131314151617),
    .INIT_48(256'hE4E5E6E7E7E8E9EAEBEBECEDEEEFEFF0F1F2F2F3F4F5F6F6F7F8F9FAFAFBFCFD),
    .INIT_49(256'hCBCCCDCECECFD0D1D1D2D3D4D5D5D6D7D8D8D9DADBDCDCDDDEDFE0E0E1E2E3E3),
    .INIT_4A(256'hB2B3B4B5B6B6B7B8B9B9BABBBCBCBDBEBFC0C0C1C2C3C3C4C5C6C7C7C8C9CACA),
    .INIT_4B(256'h9A9B9C9C9D9E9F9FA0A1A2A2A3A4A5A5A6A7A8A9A9AAABACACADAEAFAFB0B1B2),
    .INIT_4C(256'h828384848586878788898A8A8B8C8D8D8E8F9090919293939495969697989999),
    .INIT_4D(256'h6A6B6C6D6D6E6F70707172737374757576777878797A7B7B7C7D7E7E7F808181),
    .INIT_4E(256'h5354555556575758595A5A5B5C5D5D5E5F5F606162626364656566676868696A),
    .INIT_4F(256'h3C3D3E3E3F40414142434344454646474848494A4B4B4C4D4D4E4F5050515252),
    .INIT_50(256'h2626272828292A2B2B2C2D2D2E2F3030313232333434353637373839393A3B3C),
    .INIT_51(256'h1010111212131414151616171818191A1B1B1C1D1D1E1F1F2021222223242425),
    .INIT_52(256'hFAFAFBFCFCFDFEFFFF0001010203030405050607070809090A0B0B0C0D0D0E0F),
    .INIT_53(256'hE4E5E6E6E7E8E8E9EAEAEBECECEDEEEEEFF0F0F1F2F2F3F4F4F5F6F6F7F8F8F9),
    .INIT_54(256'hCFD0D1D1D2D3D3D4D5D5D6D7D7D8D9D9DADBDBDCDCDDDEDEDFE0E0E1E2E2E3E4),
    .INIT_55(256'hBBBCBCBDBDBEBFBFC0C1C1C2C3C3C4C4C5C6C6C7C8C8C9CACACBCCCCCDCDCECF),
    .INIT_56(256'hA7A7A8A9A9AAAAABACACADAEAEAFAFB0B1B1B2B3B3B4B5B5B6B6B7B8B8B9BABA),
    .INIT_57(256'h93949495959697979898999A9A9B9C9C9D9D9E9F9FA0A1A1A2A2A3A4A4A5A5A6),
    .INIT_58(256'h80808181828383848485868687878889898A8A8B8C8C8D8D8E8F8F9091919292),
    .INIT_59(256'h6D6D6E6F6F70707171727373747475767677777879797A7A7B7B7C7D7D7E7E7F),
    .INIT_5A(256'h5A5B5B5C5D5D5E5E5F5F60616162626363646565666667686869696A6A6B6C6C),
    .INIT_5B(256'h4849494A4B4B4C4C4D4D4E4E4F5050515152525353545555565657575859595A),
    .INIT_5C(256'h3737383839393A3B3B3C3C3D3D3E3E3F3F404141424243434444454646474748),
    .INIT_5D(256'h26262727282829292A2A2B2B2C2C2D2E2E2F2F30303131323233333435353636),
    .INIT_5E(256'h1515161617171818191A1A1B1B1C1C1D1D1E1E1F1F2020212122222323242425),
    .INIT_5F(256'h050506060707080809090A0A0B0B0C0C0D0D0E0E0F0F10101111121213131414),
    .INIT_60(256'hF5F5F6F6F7F7F8F8F9F9FAFAFBFBFCFCFDFDFEFEFFFF00000101020203030404),
    .INIT_61(256'hE6E6E6E7E7E8E8E9E9EAEAEBEBECECEDEDEEEEEFEFF0F0F0F1F1F2F2F3F3F4F4),
    .INIT_62(256'hD7D7D8D8D8D9D9DADADBDBDCDCDDDDDEDEDEDFDFE0E0E1E1E2E2E3E3E4E4E5E5),
    .INIT_63(256'hC8C9C9CACACACBCBCCCCCDCDCECECFCFCFD0D0D1D1D2D2D3D3D3D4D4D5D5D6D6),
    .INIT_64(256'hBABBBBBCBCBDBDBDBEBEBFBFC0C0C0C1C1C2C2C3C3C3C4C4C5C5C6C6C7C7C7C8),
    .INIT_65(256'hADADAEAEAFAFAFB0B0B1B1B1B2B2B3B3B4B4B4B5B5B6B6B7B7B7B8B8B9B9BABA),
    .INIT_66(256'hA0A0A1A1A2A2A2A3A3A4A4A4A5A5A6A6A6A7A7A8A8A8A9A9AAAAAAABABACACAD),
    .INIT_67(256'h949494959595969697979798989999999A9A9A9B9B9C9C9C9D9D9E9E9E9F9FA0),
    .INIT_68(256'h8888888989898A8A8A8B8B8C8C8C8D8D8D8E8E8F8F8F90909091919292929393),
    .INIT_69(256'h7C7C7D7D7D7E7E7E7F7F80808081818182828283838484848585858686868787),
    .INIT_6A(256'h717172727273737374747475757576767677777778787979797A7A7A7B7B7B7C),
    .INIT_6B(256'h676767676868686969696A6A6A6B6B6B6C6C6C6D6D6D6E6E6E6F6F6F70707071),
    .INIT_6C(256'h5C5D5D5D5E5E5E5F5F5F60606060616161626262636363646464656565666666),
    .INIT_6D(256'h53535454545455555556565656575757585858595959595A5A5A5B5B5B5C5C5C),
    .INIT_6E(256'h4A4A4B4B4B4B4C4C4C4C4D4D4D4E4E4E4E4F4F4F505050505151515252525253),
    .INIT_6F(256'h414242424343434344444444454545454646464647474748484848494949494A),
    .INIT_70(256'h393A3A3A3A3B3B3B3B3C3C3C3C3D3D3D3D3E3E3E3E3F3F3F3F40404040414141),
    .INIT_71(256'h3232323333333334343434353535353536363636373737373838383839393939),
    .INIT_72(256'h2B2B2B2C2C2C2C2C2D2D2D2D2E2E2E2E2E2F2F2F2F3030303030313131313232),
    .INIT_73(256'h252525252526262626262727272727282828282829292929292A2A2A2A2A2B2B),
    .INIT_74(256'h1F1F1F1F1F1F2020202020212121212122222222222223232323232424242424),
    .INIT_75(256'h1919191A1A1A1A1A1A1B1B1B1B1B1B1C1C1C1C1C1D1D1D1D1D1D1E1E1E1E1E1E),
    .INIT_76(256'h1414151515151515151616161616161617171717171718181818181819191919),
    .INIT_77(256'h1010101010111111111111111112121212121212131313131313131414141414),
    .INIT_78(256'h0C0C0C0C0C0D0D0D0D0D0D0D0D0E0E0E0E0E0E0E0E0E0F0F0F0F0F0F0F0F1010),
    .INIT_79(256'h0909090909090909090A0A0A0A0A0A0A0A0A0A0B0B0B0B0B0B0B0B0B0C0C0C0C),
    .INIT_7A(256'h0606060606060606060707070707070707070707080808080808080808080809),
    .INIT_7B(256'h0304040404040404040404040404040505050505050505050505050505060606),
    .INIT_7C(256'h0202020202020202020202020202020203030303030303030303030303030303),
    .INIT_7D(256'h0000010101010101010101010101010101010101010101010101010102020202),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized30
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h71E3870E1C3C3878783C3C1E0F83E0F81F03F03F80FE03FE01FF801FFE0007FF),
    .INITP_01(256'h933664CC9999933333333319998CCC663319CE6318C6318E718E71C718E38F1C),
    .INITP_02(256'h6A5294A5AD29694B4B4B49692D25B6925B6DB6DB6DB6D924DB64DB26C9B364C9),
    .INITP_03(256'hAAA955555555555555555555AAAAA95556AAB556AA556A956A952A54AD5A94AD),
    .INITP_04(256'h2DA5A5A52D2D694B5AD6B5AD4A56A56A56AD5AB56A954AA552AA554AAA5555AA),
    .INITP_05(256'hCD99B3264C993264D9366D9364DB24DB64926DB6DB6DB6DB6924B6D25B49692D),
    .INITP_06(256'hC639C631CE739CE7398C67319CC6633199CCCE6667333333333333333366664C),
    .INITP_07(256'h0F8783C3C3C3C3C3C3C387870E1E3C70E1C78E1C71E38E38E38E38E39C71CE39),
    .INITP_08(256'h7319CC6339CE739CE718C738C738E71C738E38E38E38E38F1C70E3C70E1C78F0),
    .INITP_09(256'h4D936CD9364C993264C99B336664CCCD9999999999999999CCCCE66733198CC6),
    .INITP_0A(256'hA56B5AD6B5A52D69694B4B4B69692D25B496DA492DB6DB6DB6DB6C924DB649B6),
    .INITP_0B(256'hAB555555555555555555552AAAAB5554AAA554AA954AA552AD5AB56AD4AD4AD4),
    .INITP_0C(256'h692D25A5A5A52D296B4A5294AD6A52B56A54A952AD52AD54AAD55AAAD5552AAA),
    .INITP_0D(256'h333199999999933332664CD993264D9B26C9B64DB64936DB6DB6DB6DB492DB49),
    .INITP_0E(256'hE0F078783C3C387870E1C38F1C71E38E31C71CE31CE318C6318CE73198CC6663),
    .INITP_0F(256'hFE0000000000000000000000FFFFC000FFF003FF00FF80FE03F81F81F03E0F83),
    .INIT_00(256'h87715C46321D09F5E1CDBAA79482705E4C3B291908F8E7D8C8B9AA9B8C7E7062),
    .INIT_01(256'hC5A7896B4E3115F8DCC0A4896E53391E04EAD1B79E866D553D250EF7E0C9B39C),
    .INIT_02(256'h0FE8C29C77522D08E3BF9B7754300DEBC8A684634120FFDEBE9E7E5F3F2001E3),
    .INIT_03(256'h653607D9AB7E5023F6C99D714519EEC3986E4319F0C69D744B23FBD3AB835C35),
    .INIT_04(256'hC68F5821EBB57F4A14DFAB76420EDAA774410EDCAA784615E4B3825222F2C394),
    .INIT_05(256'h32F2B37436F7B97B3E01C3874A0ED2965B1FE4AA6F35FBC1884F16DDA56D35FD),
    .INIT_06(256'hA86119D28B44FEB8722CE7A25D18D4904C08C5823FFDBA7837F5B47332F2B272),
    .INIT_07(256'h29D9893AEA9B4DFEB06214C77A2DE09447FCB0651ACF843AF0A65C13CA8139F0),
    .INIT_08(256'hB35A02AA53FCA54EF7A14BF5A04BF6A14DF8A551FEAA5705B3600FBD6C1BCA79),
    .INIT_09(256'h45E48424C46505A648E98B2DCF7214B75BFEA246EB8F34D97E24CA7016BD640B),
    .INIT_0A(256'hDF760EA53DD66E07A039D36C06A13BD6710CA844E07C19B653F08E2CCA6807A6),
    .INIT_0B(256'h800F9E2EBE4EDE6FFF9022B345D769FC8F22B549DD71059A2FC459EF851BB148),
    .INIT_0C(256'h27AE35BD44CC54DD65EE77008A149E28B33EC955E06CF885129E2CB947D563F1),
    .INIT_0D(256'hD453D151D050D050D051D253D556D85BDD60E366EA6DF176FA7F04890F951BA1),
    .INIT_0E(256'h84FB72E960D84FC840B831AA249D17910C86017CF873EF6BE864E15EDC59D755),
    .INIT_0F(256'h38A61584F363D343B323940576E85ACC3EB02396097DF165D94EC237AD22980E),
    .INIT_10(256'hED54BA2189F058C02890F962CB359E0872DD48B31E89F561CD3AA61380EE5CCA),
    .INIT_11(256'hA30260BF1F7EDE3E9EFE5FC02182E446A80B6DD03497FB5FC3278CF156BB2187),
    .INIT_12(256'h59AF065DB40C63BB136CC51E77D02A84DE3993EE49A5015DB91572CF2C89E745),
    .INIT_13(256'h0C5BA9F84897E73787D8297ACB1D6EC01365B80B5EB2065AAE0257AC0157AC02),
    .INIT_14(256'hBC034A91D82068B0F8418AD31D66B0FA458FDA2571BC0854A0ED3A87D42270BE),
    .INIT_15(256'h67A6E52464A4E42465A6E7286AABED3072B5F83B7FC3074B8FD4195EA4E92F76),
    .INIT_16(256'h0C437AB2E9215A92CB043D77B1EB255F9AD5104C88C3003C79B6F3306EACEA28),
    .INIT_17(256'hA8D707376797C8F92A5B8DBEF0235588BBEE225589BDF2275B91C6FC32689ED5),
    .INIT_18(256'h3B628AB3DB042D567FA9D3FD27527CA7D3FE2A5682AFDB08366391BFED1B4A79),
    .INIT_19(256'hC2E20223446586A8C9EB0D30537699BCE004284C7196BBE0062B51789EC5EC13),
    .INIT_1A(256'h3C546D869FB9D3EC07213C57728DA9C5E1FD193653708EACCAE80625446382A2),
    .INIT_1B(256'hA6B8C9DAECFE102335485B6F8296AABFD3E8FD12283D536A8097ADC5DCF40B23),
    .INIT_1C(256'h000A141E28323D48535F6B76838F9CA8B6C3D0DEECFA09182736455565758596),
    .INIT_1D(256'h47494B4E5154575B5F63676C70757B80868C92989FA6ADB4BCC3CBD4DCE5EEF7),
    .INIT_1E(256'h78736E6965615D5956524F4D4A48464442413F3E3E3D3D3D3D3D3E3F40414345),
    .INIT_1F(256'h92867A6E62574C41362B21170D04FAF1E8DFD7CFC7BFB7B0A9A29B958F89837D),
    .INIT_20(256'h937F6C594634210FFDEBDAC9B8A796867666564737281A0BFDEFE1D3C6B8AB9F),
    .INIT_21(256'h785D43290FF5DCC2A990785F472F1800E9D2BBA48E78624C37210CF8E3CFBAA6),
    .INIT_22(256'h3F1DFCDBBA9978583818F9D9BA9B7C5E3F2103E6C8AB8E7154381C00E4C9AE93),
    .INIT_23(256'hE6BD956D451DF6CEA7805A330DE7C19C76512C08E3BF9B7754300DEAC8A58361),
    .INIT_24(256'h6A3A0BDCAD7F5022F4C6996C3F12E5B98C603409DDB2875D3208DEB48A61380F),
    .INIT_25(256'hC9935D27F1BC86511DE8B4804C18E5B17E4B19E6B482501FEDBC8B5A2AFAC99A),
    .INIT_26(256'h00C3864A0ED1955A1EE3A86D32F8BE834A10D79D642CF3BB834B13DBA46D36FF),
    .INIT_27(256'h0DCA864300BD7B38F6B47231F0AE6D2DECAC6C2CECAD6D2EF0B17234F6B87B3D),
    .INIT_28(256'hEEA35910C67D34EBA25911C98139F2AA631CD68F4903BD7732ECA7631ED99551),
    .INIT_29(256'h9E4EFDAD5D0DBD6E1FD08132E49648FAAC5F11C4782BDF9246FBAF6418CD8338),
    .INIT_2A(256'h1DC66F18C26C16C06A15BF6A15C16C18C4701CC97623D07D2BD98635E39140EF),
    .INIT_2B(256'h6609AC4FF29539DD8125CA6E13B85D03A84EF49A41E78E35DC842BD37B23CB74),
    .INIT_2C(256'h7814B14EEB8825C361FF9D3BDA7918B756F69535D67616B758F99A3CDE7F21C4),
    .INIT_2D(256'h4FE57B12A940D76F079E36CF67009831CB64FE9731CB66009B36D16C08A43FDC),
    .INIT_2E(256'hE87808992ABB4CDE6F019325B84ADD7003962ABD51E57A0EA338CD62F78D22B8),
    .INIT_2F(256'h40CA55E06BF6810C9824B03CC955E26FFC8917A532C14FDD6CFB8A19A838C857),
    .INIT_30(256'h55D95EE368ED73F87E048A10971EA52CB33AC24AD25AE26BF47C068F18A22CB6),
    .INIT_31(256'h22A1209F1F9E1E9E1E9E1E9F20A122A325A628AA2CAF31B437BA3DC144C84CD0),
    .INIT_32(256'hA61F99128C0680FA74EF6AE560DB56D24ECA46C23FBB38B532B02DAB29A725A4),
    .INIT_33(256'hDD51C438AC21950A7FF469DE54C93FB52BA2188F067DF46CE35BD34BC33CB42D),
    .INIT_34(256'hC332A00E7DEC5BCA39A91989F969D94ABA2B9C0E7FF162D446B82B9D1083F66A),
    .INIT_35(256'h56BF2891FA64CE37A10C76E04BB6218CF763CF3AA6137FEB58C5329F0C7AE755),
    .INIT_36(256'h92F559BD2186EA4FB3187DE248AD1379DF45AC1279E047AE157DE54CB41D85ED),
    .INIT_37(256'h73D2308FEE4DAD0C6CCB2B8BEC4CAD0D6ECF3092F355B7197BDD40A20568CB2E),
    .INIT_38(256'hF751AA045EB8126DC7227DD8338EEA45A1FD59B6126FCB2885E2409DFB59B715),
    .INIT_39(256'h1A6EC3186DC2176DC2186EC41A71C71E75CC237AD22981D93189E23A93EC459E),
    .INIT_3A(256'hD82877C81868B9095AABFC4D9FF04294E6388ADC2F82D5287BCE2275C91D71C5),
    .INIT_3B(256'h2E79C40F5BA6F23E8AD6236FBC0956A3F03D8BD92674C3115FAEFC4B9AE93988),
    .INIT_3C(256'h185EA5EC337AC1085097DF276FB7FF4890D9226BB4FE4791DA246EB9034D98E3),
    .INIT_3D(256'h93D517599CDE2164A7EA2D70B4F83B7FC3084C91D51A5FA4E92F74BAFF458BD2),
    .INIT_3E(256'h9BD8165492D00F4D8CCA094887C7064685C5054585C6064788C90A4B8CCE0F51),
    .INIT_3F(256'h2C659FD9124C86C1FB3570ABE6215C97D20E4A85C1FD3976B2EF2C68A5E3205D),
    .INIT_40(256'hF2A65B0FC4792EE3984D03B96E24DA9147FEB46B22D99048FFB76F27DF975008),
    .INIT_41(256'hB96818C87728D88839E99A4BFCAE5F11C37426D98B3DF0A35609BC6F23D68A3E),
    .INIT_42(256'h17C26D18C36E1AC5711DC97522CE7B28D5822FDC8A38E69442F09F4DFCAB5A09),
    .INIT_43(256'h12B85E04AA51F79E45EC933AE28931D98129D27A23CC751EC7711AC46E18C26D),
    .INIT_44(256'hAD4DEE8F30D27315B759FB9D40E28528CB6F12B659FDA145EA8E33D87D22C76D),
    .INIT_45(256'hEA8621BD59F5922ECB6805A240DD7B19B755F39230CF6E0DAD4CEC8B2BCB6C0C),
    .INIT_46(256'hCE64FA9128BF56ED851DB44CE57D15AE47E07912AC45DF7913AD48E27D18B34F),
    .INIT_47(256'h5BEC7D0EA032C355E77A0C9F32C558EB7F13A63ACF63F78C21B64BE0760CA137),
    .INIT_48(256'h9521AC38C451DD6AF683109D2BB846D462F17F0E9C2BBA4AD969F98919A939CA),
    .INIT_49(256'h80068C12991FA62DB43CC34BD35BE36CF47D068F18A22BB53FC954DE69F47F0A),
    .INIT_4A(256'h1E9E1F9F20A122A425A729AB2DB032B538BB3FC246CA4ED256DB60E56AEF74FA),
    .INIT_4B(256'h73ED68E35ED955D04CC844C13DBA37B431AF2CAA28A625A322A1209F1E9E1E9E),
    .INIT_4C(256'h81F66BE055CA40B62CA2188F067CF46BE25AD24AC23AB32CA51E97108A047EF8),
    .INIT_4D(256'h4CBB2A990878E857C838A8198AFB6CDD4FC132A51789FC6FE255C93CB024980C),
    .INIT_4E(256'hD740A9127BE54FB8228DF762CD38A30E7AE551BD2A960370DD4AB82593016FDE),
    .INIT_4F(256'h2588EB4EB11478DC3FA4086CD1369B0066CB3197FE64CB31980067CF369E076F),
    .INIT_50(256'h3995F24FAC0966C4217FDE3C9AF958B71676D63595F656B71879DA3B9DFF61C3),
    .INIT_51(256'h166CC2186FC61D74CB237BD32B84DC358EE7419AF44EA8035DB8136ECA2581DD),
    .INIT_52(256'hBD0D5DADFD4E9EEF4091E33586D92B7DD02376C91C70C4186CC1156ABF156AC0),
    .INIT_53(256'h347DC61059A3EE3883CD1864AFFB4692DF2B78C4115FACFA4896E43281D01F6E),
    .INIT_54(256'h7BBD004386CA0D5195D91E63A7EC3277BD03498FD61C63AAF23981C91159A2EB),
    .INIT_55(256'h95D10E4A86C3003D7BB8F63472B1F02E6DADEC2C6CACEC2D6DAEF03172B4F638),
    .INIT_56(256'h86BCF1275D93C9FF366DA4DB134B83BBF32C649DD7104A83BEF8326DA8E31E5A),
    .INIT_57(256'h507FADDC0B3A6A9AC9FA2A5A8BBCED1F5082B4E6194B7EB1E5184C80B4E81D51),
    .INIT_58(256'hF61D456D95BDE60F38618AB4DE08325D87B2DD0934608CB9E5123F6C99C6F422),
    .INIT_59(256'h7899BADBFC1D3F6183A5C8EA0D3054779BBFE3082C51769CC1E70D335A80A7CE),
    .INIT_5A(256'hDCF50F29435D7893AEC9E4001C3854718EABC8E603213F5E7C9BBAD9F9183858),
    .INIT_5B(256'h213446596C7F93A6BACFE3F80C21374C62788EA4BBD2E900182F475F7890A9C2),
    .INIT_5C(256'h4C57626E7A86929FABB8C6D3E1EFFD0B1A2837475666768696A7B8C9DAEBFD0F),
    .INIT_5D(256'h5D6165696E73787D83898F959BA2A9B0B7BFC7CFD7DFE8F1FA040D17212B3641),
    .INIT_5E(256'h5754514E4B4947454341403F3E3D3D3D3D3D3E3E3F41424446484A4D4F525659),
    .INIT_5F(256'h3D32281E140A00F7EEE5DCD4CBC3BCB4ADA69F98928C86807B75706C67635F5B),
    .INIT_60(256'h10FEECDAC9B8A696857565554536271809FAECDED0C3B6A89C8F83766B5F5348),
    .INIT_61(256'hD3B99F866D543C230BF4DCC5AD97806A533D2812FDE8D3BFAA96826F5B483523),
    .INIT_62(256'h8665442302E2C2A28263442506E8CAAC8E70533619FDE1C5A98D72573C2107EC),
    .INIT_63(256'h2D04DBB38A623B13ECC59E78512B06E0BB96714C2804E0BC997653300DEBC9A8),
    .INIT_64(256'hC897673707D7A8794A1BEDBF91633608DBAF82562AFED3A77C5227FDD3A97F56),
    .INIT_65(256'h5A21E9B27A430CD59E6832FCC6915B27F2BD895522EEBB885523F0BE8D5B2AF9),
    .INIT_66(256'hE4A46424E5A66728EAAC6E30F3B6793C00C3884C10D59A5F25EBB1773D04CB92),
    .INIT_67(256'h6820D8914A03BC762FE9A45E19D48F4B07C37F3BF8B57230EDAB6A28E7A66524),
    .INIT_68(256'hE79748F8A95B0CBE7022D4873AEDA05408BC7125DA8F45FAB0661DD38A41F8B0),
    .INIT_69(256'h630CB45D06AF5902AC5701AC5702AE5A06B25E0BB86513C06E1DCB7A29D88737),
    .INIT_6A(256'hDE7E1FBF6002A345E7892CCF7215B95D01A549EE9339DE842AD0771EC56C13BB),
    .INIT_6B(256'h58F08921BA54ED8721BB56F18C27C35FFB9734D06D0BA846E48221C05FFE9E3E),
    .INIT_6C(256'hD363F38415A638CA5CEE8013A63ACD61F5891EB348DD72089E35CB62F99028C0),
    .INIT_6D(256'h4FD860E972FB840E9822AD37C24ED965F17D099623B03ECC5AE876059423B343),
    .INIT_6E(256'hD050D051D153D455D759DC5EE164E86BEF73F87C01860C91179D24AA31B840C8),
    .INIT_6F(256'h54CC44BD35AE27A11B950F89047FFA76F16DEA66E360DD5BD856D553D251D050),
    .INIT_70(256'hDE4EBE2E9E0F80F163D547B92C9E1285F86CE055C93EB3289E148A0077EE65DD),
    .INIT_71(256'h6ED63DA50E76DF48B11B85EF59C42F9A0571DD49B5228FFC69D745B32290FF6F),
    .INIT_72(256'h0565C42484E445A60768CA2C8EF053B6197CE044A80C71D63BA1066CD339A007),
    .INIT_73(256'hA5FC53AA025AB30B64BD1670CA247ED9348FEB46A2FE5BB71472CF2D8BE948A6),
    .INIT_74(256'h4D9BEA3A89D92979CA1B6CBD0F60B30557AAFE51A5F84DA1F64BA0F54BA1F74E),
    .INIT_75(256'hFE448BD21961A8F03981CA135CA6F03A84CF1A65B0FC4794E02D7AC71462B0FE),
    .INIT_76(256'hB9F73674B3F23272B2F23273B4F53778BAFD3F82C5084C90D4185DA2E72C72B8),
    .INIT_77(256'h7FB5EB21588FC6FD356DA5DD164F88C1FB356FAAE41F5B96D20E4A87C3013E7B),
    .INIT_78(256'h507EABD907366594C3F2225282B3E4154678AADC0E4174A7DA0E4276ABDF144A),
    .INIT_79(256'h2D52779CC2E80F355C83ABD3FB234B749DC6F019436E98C3EE1945719DC9F623),
    .INIT_7A(256'h15314E6B89A7C5E301203F5F7E9EBEDEFF20416384A6C8EB0D3054779BBFE308),
    .INIT_7B(256'h091D32465C71879CB3C9E0F70E253D556D869EB7D1EA041E39536E89A4C0DCF8),
    .INIT_7C(256'h0915212E3A475562707E8C9BAAB9C8D8E7F80819293B4C5E708294A7BACDE1F5),
    .INIT_7D(256'h16191D21262A2F343A3F454B52585F676E767E868E97A0AAB3BDC7D1DCE7F2FD),
    .INIT_7E(256'h2F2A26211D191613100D0A08060403020100000000000102030406080A0D1013),
    .INIT_7F(256'h55473A2E211509FDF2E7DCD1C7BDB3AAA0978E867E766E675F58524B453F3A34),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized31
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'hFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000),
    .INITP_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_05(256'h000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_06(256'hFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_08(256'h0000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000),
    .INITP_0B(256'h0000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0101010101010101010101010101010101010101010000000000000000000000),
    .INIT_01(256'h0303030303030302020202020202020202020202020202020202020101010101),
    .INIT_02(256'h0505050505050505040404040404040404040404040403030303030303030303),
    .INIT_03(256'h0808080707070707070707070707060606060606060606060606050505050505),
    .INIT_04(256'h0B0B0B0B0A0A0A0A0A0A0A0A0A0A090909090909090909090808080808080808),
    .INIT_05(256'h0F0E0E0E0E0E0E0E0E0E0D0D0D0D0D0D0D0D0C0C0C0C0C0C0C0C0C0B0B0B0B0B),
    .INIT_06(256'h131313121212121212121111111111111111101010101010100F0F0F0F0F0F0F),
    .INIT_07(256'h1817171717171716161616161616151515151515151414141414141413131313),
    .INIT_08(256'h1D1D1D1C1C1C1C1C1B1B1B1B1B1B1A1A1A1A1A1A191919191919191818181818),
    .INIT_09(256'h23222222222222212121212120202020201F1F1F1F1F1F1E1E1E1E1E1E1D1D1D),
    .INIT_0A(256'h2929292828282828272727272726262626262525252525242424242423232323),
    .INIT_0B(256'h30302F2F2F2F2E2E2E2E2E2D2D2D2D2C2C2C2C2C2B2B2B2B2B2A2A2A2A2A2929),
    .INIT_0C(256'h3737373636363635353535353434343433333333323232323231313131303030),
    .INIT_0D(256'h3F3F3E3E3E3E3D3D3D3D3C3C3C3C3B3B3B3B3A3A3A3A39393939393838383837),
    .INIT_0E(256'h4747474646464645454545444444444343434342424241414141404040403F3F),
    .INIT_0F(256'h5050504F4F4F4E4E4E4E4D4D4D4C4C4C4C4B4B4B4B4A4A4A4949494948484848),
    .INIT_10(256'h5959595958585857575756565656555555545454545353535252525251515150),
    .INIT_11(256'h636363626262616161606060605F5F5F5E5E5E5D5D5D5C5C5C5C5B5B5B5A5A5A),
    .INIT_12(256'h6E6D6D6D6C6C6C6B6B6B6A6A6A69696968686867676767666666656565646464),
    .INIT_13(256'h7978787777777676767575757474747373737272727171717070706F6F6F6E6E),
    .INIT_14(256'h848483838282828181818080807F7F7E7E7E7D7D7D7C7C7C7B7B7B7A7A7A7979),
    .INIT_15(256'h908F8F8F8E8E8D8D8D8C8C8C8B8B8A8A8A898989888888878786868685858584),
    .INIT_16(256'h9C9C9B9B9A9A9A99999998989797979696959595949494939392929291919090),
    .INIT_17(256'hA9A8A8A8A7A7A6A6A6A5A5A4A4A4A3A3A2A2A2A1A1A0A0A09F9F9E9E9E9D9D9C),
    .INIT_18(256'hB6B6B5B5B4B4B4B3B3B2B2B1B1B1B0B0AFAFAFAEAEADADADACACABABAAAAAAA9),
    .INIT_19(256'hC4C3C3C3C2C2C1C1C0C0C0BFBFBEBEBDBDBDBCBCBBBBBABABAB9B9B8B8B7B7B7),
    .INIT_1A(256'hD2D2D1D1D0D0CFCFCFCECECDCDCCCCCBCBCACACAC9C9C8C8C7C7C7C6C6C5C5C4),
    .INIT_1B(256'hE1E0E0DFDFDEDEDEDDDDDCDCDBDBDADAD9D9D8D8D8D7D7D6D6D5D5D4D4D3D3D3),
    .INIT_1C(256'hF0F0EFEFEEEEEDEDECECEBEBEAEAE9E9E8E8E7E7E6E6E6E5E5E4E4E3E3E2E2E1),
    .INIT_1D(256'h00FFFFFEFEFDFDFCFCFBFBFAFAF9F9F8F8F7F7F6F6F5F5F4F4F3F3F2F2F1F1F0),
    .INIT_1E(256'h100F0F0E0E0D0D0C0C0B0B0A0A09090808070706060505040403030202010100),
    .INIT_1F(256'h20201F1F1E1E1D1D1C1C1B1B1A1A191818171716161515141413131212111110),
    .INIT_20(256'h313130302F2F2E2E2D2C2C2B2B2A2A2929282827272626252424232322222121),
    .INIT_21(256'h4342424141403F3F3E3E3D3D3C3C3B3B3A393938383737363635353433333232),
    .INIT_22(256'h555453535252515150504F4E4E4D4D4C4C4B4B4A494948484747464645444443),
    .INIT_23(256'h676666656564636362626161605F5F5E5E5D5D5C5B5B5A5A5959585757565655),
    .INIT_24(256'h7A79797877777676757474737372717170706F6F6E6D6D6C6C6B6A6A69696868),
    .INIT_25(256'h8D8C8C8B8A8A89898887878686858484838382818180807F7E7E7D7D7C7B7B7A),
    .INIT_26(256'hA1A09F9F9E9D9D9C9C9B9A9A999898979796959594949392929191908F8F8E8D),
    .INIT_27(256'hB5B4B3B3B2B1B1B0AFAFAEAEADACACABAAAAA9A9A8A7A7A6A5A5A4A4A3A2A2A1),
    .INIT_28(256'hC9C8C8C7C6C6C5C4C4C3C3C2C1C1C0BFBFBEBDBDBCBCBBBABAB9B8B8B7B6B6B5),
    .INIT_29(256'hDEDDDCDCDBDBDAD9D9D8D7D7D6D5D5D4D3D3D2D1D1D0CFCFCECDCDCCCCCBCACA),
    .INIT_2A(256'hF3F2F2F1F0F0EFEEEEEDECECEBEAEAE9E8E8E7E6E6E5E4E4E3E2E2E1E0E0DFDE),
    .INIT_2B(256'h0908070706050504030302010100FFFFFEFDFCFCFBFAFAF9F8F8F7F6F6F5F4F4),
    .INIT_2C(256'h1F1E1D1D1C1B1B1A1918181716161514141312121110100F0E0D0D0C0B0B0A09),
    .INIT_2D(256'h3534343332323130302F2E2D2D2C2B2B2A29282827262625242423222221201F),
    .INIT_2E(256'h4C4B4B4A49484847464645444343424141403F3E3E3D3C3C3B3A393938373736),
    .INIT_2F(256'h63626261605F5F5E5D5D5C5B5A5A59585757565555545352525150504F4E4D4D),
    .INIT_30(256'h7B7A79787877767575747373727170706F6E6D6D6C6B6A6A6968686766656564),
    .INIT_31(256'h93929190908F8E8D8D8C8B8A8A898887878685848483828181807F7E7E7D7C7B),
    .INIT_32(256'hABAAA9A9A8A7A6A5A5A4A3A2A2A1A09F9F9E9D9C9C9B9A999998979696959493),
    .INIT_33(256'hC3C3C2C1C0C0BFBEBDBCBCBBBAB9B9B8B7B6B6B5B4B3B2B2B1B0AFAFAEADACAC),
    .INIT_34(256'hDCDCDBDAD9D8D8D7D6D5D5D4D3D2D1D1D0CFCECECDCCCBCACAC9C8C7C7C6C5C4),
    .INIT_35(256'hF6F5F4F3F2F2F1F0EFEFEEEDECEBEBEAE9E8E7E7E6E5E4E3E3E2E1E0E0DFDEDD),
    .INIT_36(256'h0F0E0E0D0C0B0A0A090807060605040302020100FFFEFEFDFCFBFAFAF9F8F7F6),
    .INIT_37(256'h2928282726252424232221201F1F1E1D1C1B1B1A191817171615141313121110),
    .INIT_38(256'h43434241403F3F3E3D3C3B3A3A3938373635353433323131302F2E2D2C2C2B2A),
    .INIT_39(256'h5E5D5C5C5B5A5958575756555453525251504F4E4D4D4C4B4A49484847464544),
    .INIT_3A(256'h7978777676757473727170706F6E6D6C6B6B6A6968676666656463626161605F),
    .INIT_3B(256'h9493929291908F8E8D8C8C8B8A8988878686858483828181807F7E7D7C7B7B7A),
    .INIT_3C(256'hB0AFAEADACABAAAAA9A8A7A6A5A4A3A3A2A1A09F9E9D9D9C9B9A999898979695),
    .INIT_3D(256'hCBCACAC9C8C7C6C5C4C3C3C2C1C0BFBEBDBDBCBBBAB9B8B7B6B6B5B4B3B2B1B0),
    .INIT_3E(256'hE7E6E6E5E4E3E2E1E0DFDFDEDDDCDBDAD9D8D8D7D6D5D4D3D2D1D1D0CFCECDCC),
    .INIT_3F(256'h0403020100FFFEFDFCFCFBFAF9F8F7F6F5F5F4F3F2F1F0EFEEEDEDECEBEAE9E8),
    .INIT_40(256'h8F9091929293949596979898999A9B9C9D9D9E9FA0A1A2A3A3A4A5A6A7A8A9AA),
    .INIT_41(256'h747576767778797A7B7B7C7D7E7F8081818283848586868788898A8B8C8C8D8E),
    .INIT_42(256'h595A5B5C5C5D5E5F6061616263646566666768696A6B6B6C6D6E6F7070717273),
    .INIT_43(256'h3F3F4041424343444546474848494A4B4C4D4D4E4F5051525253545556575758),
    .INIT_44(256'h242526272828292A2B2C2C2D2E2F3031313233343535363738393A3A3B3C3D3E),
    .INIT_45(256'h0A0B0C0D0E0E0F1011121313141516171718191A1B1B1C1D1E1F1F2021222324),
    .INIT_46(256'hF1F2F2F3F4F5F6F6F7F8F9FAFAFBFCFDFEFEFF0001020203040506060708090A),
    .INIT_47(256'hD8D8D9DADBDCDCDDDEDFE0E0E1E2E3E3E4E5E6E7E7E8E9EAEBEBECEDEEEFEFF0),
    .INIT_48(256'hBFC0C0C1C2C3C3C4C5C6C7C7C8C9CACACBCCCDCECECFD0D1D1D2D3D4D5D5D6D7),
    .INIT_49(256'hA6A7A8A9A9AAABACACADAEAFAFB0B1B2B2B3B4B5B6B6B7B8B9B9BABBBCBCBDBE),
    .INIT_4A(256'h8E8F90909192939394959696979899999A9B9C9C9D9E9F9FA0A1A2A2A3A4A5A5),
    .INIT_4B(256'h76777878797A7B7B7C7D7E7E7F808181828384848586878788898A8A8B8C8D8D),
    .INIT_4C(256'h5F5F606162626364656566676868696A6A6B6C6D6D6E6F707071727373747575),
    .INIT_4D(256'h4848494A4B4B4C4D4D4E4F50505152525354555556575758595A5A5B5C5D5D5E),
    .INIT_4E(256'h313232333434353637373839393A3B3C3C3D3E3E3F4041414243434445464647),
    .INIT_4F(256'h1B1B1C1D1D1E1F1F20212222232424252626272828292A2B2B2C2D2D2E2F3030),
    .INIT_50(256'h05050607070809090A0B0B0C0D0D0E0F1010111212131414151616171818191A),
    .INIT_51(256'hEFF0F0F1F2F2F3F4F4F5F6F6F7F8F8F9FAFAFBFCFCFDFEFFFF00010102030304),
    .INIT_52(256'hDADBDBDCDCDDDEDEDFE0E0E1E2E2E3E4E4E5E6E6E7E8E8E9EAEAEBECECEDEEEE),
    .INIT_53(256'hC5C6C6C7C8C8C9CACACBCCCCCDCDCECFCFD0D1D1D2D3D3D4D5D5D6D7D7D8D9D9),
    .INIT_54(256'hB1B1B2B3B3B4B5B5B6B6B7B8B8B9BABABBBCBCBDBDBEBFBFC0C1C1C2C3C3C4C4),
    .INIT_55(256'h9D9D9E9F9FA0A1A1A2A2A3A4A4A5A5A6A7A7A8A9A9AAAAABACACADAEAEAFAFB0),
    .INIT_56(256'h898A8A8B8C8C8D8D8E8F8F909191929293949495959697979898999A9A9B9C9C),
    .INIT_57(256'h7677777879797A7A7B7B7C7D7D7E7E7F80808181828383848485868687878889),
    .INIT_58(256'h63646565666667686869696A6A6B6C6C6D6D6E6F6F7070717172737374747576),
    .INIT_59(256'h5152525353545555565657575859595A5A5B5B5C5D5D5E5E5F5F606161626263),
    .INIT_5A(256'h3F4041414242434344444546464747484849494A4B4B4C4C4D4D4E4E4F505051),
    .INIT_5B(256'h2E2F2F303031313232333334353536363737383839393A3B3B3C3C3D3D3E3E3F),
    .INIT_5C(256'h1D1E1E1F1F202021212222232324242526262727282829292A2A2B2B2C2C2D2E),
    .INIT_5D(256'h0D0D0E0E0F0F101011111212131314141515161617171818191A1A1B1B1C1C1D),
    .INIT_5E(256'hFDFDFEFEFFFF00000101020203030404050506060707080809090A0A0B0B0C0C),
    .INIT_5F(256'hEDEEEEEFEFF0F0F0F1F1F2F2F3F3F4F4F5F5F6F6F7F7F8F8F9F9FAFAFBFBFCFC),
    .INIT_60(256'hDEDEDFDFE0E0E1E1E2E2E3E3E4E4E5E5E6E6E6E7E7E8E8E9E9EAEAEBEBECECED),
    .INIT_61(256'hCFD0D0D1D1D2D2D3D3D3D4D4D5D5D6D6D7D7D8D8D8D9D9DADADBDBDCDCDDDDDE),
    .INIT_62(256'hC1C2C2C3C3C3C4C4C5C5C6C6C7C7C7C8C8C9C9CACACACBCBCCCCCDCDCECECFCF),
    .INIT_63(256'hB4B4B4B5B5B6B6B7B7B7B8B8B9B9BABABABBBBBCBCBDBDBDBEBEBFBFC0C0C0C1),
    .INIT_64(256'hA6A7A7A8A8A8A9A9AAAAAAABABACACADADADAEAEAFAFAFB0B0B1B1B1B2B2B3B3),
    .INIT_65(256'h9A9A9A9B9B9C9C9C9D9D9E9E9E9F9FA0A0A0A1A1A2A2A2A3A3A4A4A4A5A5A6A6),
    .INIT_66(256'h8D8E8E8F8F8F9090909191929292939394949495959596969797979898999999),
    .INIT_67(256'h828282838384848485858586868687878888888989898A8A8A8B8B8C8C8C8D8D),
    .INIT_68(256'h7677777778787979797A7A7A7B7B7B7C7C7C7D7D7D7E7E7E7F7F808080818181),
    .INIT_69(256'h6C6C6C6D6D6D6E6E6E6F6F6F7070707171717272727373737474747575757676),
    .INIT_6A(256'h61626262636363646464656565666666676767676868686969696A6A6A6B6B6B),
    .INIT_6B(256'h585858595959595A5A5A5B5B5B5C5C5C5C5D5D5D5E5E5E5F5F5F606060606161),
    .INIT_6C(256'h4E4F4F4F50505050515151525252525353535454545455555556565656575757),
    .INIT_6D(256'h4646464647474748484848494949494A4A4A4B4B4B4B4C4C4C4C4D4D4D4E4E4E),
    .INIT_6E(256'h3D3E3E3E3E3F3F3F3F4040404041414141424242434343434444444445454545),
    .INIT_6F(256'h36363636373737373838383839393939393A3A3A3A3B3B3B3B3C3C3C3C3D3D3D),
    .INIT_70(256'h2E2F2F2F2F303030303031313131323232323233333333343434343535353535),
    .INIT_71(256'h2828282829292929292A2A2A2A2A2B2B2B2B2B2C2C2C2C2C2D2D2D2D2E2E2E2E),
    .INIT_72(256'h2222222222222323232323242424242425252525252626262626272727272728),
    .INIT_73(256'h1C1C1C1C1D1D1D1D1D1D1E1E1E1E1E1E1F1F1F1F1F1F20202020202121212121),
    .INIT_74(256'h171717171717181818181818191919191919191A1A1A1A1A1A1B1B1B1B1B1B1C),
    .INIT_75(256'h1212121213131313131313141414141414141515151515151516161616161616),
    .INIT_76(256'h0E0E0E0E0E0E0F0F0F0F0F0F0F0F101010101010101111111111111111121212),
    .INIT_77(256'h0A0A0A0B0B0B0B0B0B0B0B0B0C0C0C0C0C0C0C0C0C0D0D0D0D0D0D0D0D0E0E0E),
    .INIT_78(256'h070707070808080808080808080808090909090909090909090A0A0A0A0A0A0A),
    .INIT_79(256'h0505050505050505050505050506060606060606060606060607070707070707),
    .INIT_7A(256'h0303030303030303030303030303030303040404040404040404040404040405),
    .INIT_7B(256'h0101010101010101010101010202020202020202020202020202020202020202),
    .INIT_7C(256'h0000000000000000000000000000000000000101010101010101010101010101),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized32
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h999CE71C3C3F00FFFFF803E0F1E318CCCCD9249694AD556AD552A52DA49B2666),
    .INITP_01(256'h4CCCCE71C78781FE000007FC1F0E1CE3319B364925AD6AD5555556AD696DB6CD),
    .INITP_02(256'h9999CE71C3C3E01FFFFFFF80F87871CE7333264924B4A54AAB54AAA56B4B4936),
    .INITP_03(256'h638E387C1FC003FF0007E07870E318CCCCD936D25A56AD5555555AB52D2DB64D),
    .INITP_04(256'h007FFFFFF007E0F0F1C6398CCCD9B6496D294AD55AAAAD54A94A4B6DB66CCCCE),
    .INITP_05(256'h71C7398CCCC9936DB69296A56AAD55556AA54A52D24B24D9933198C71C78783F),
    .INITP_06(256'hAAAAAAA955AB52D696DB6DB266CCCCE631CE3C78781F80FFFE00FFFE01F83C3C),
    .INITP_07(256'h3C0FC07FE0000000003FF01F83C1E3C71C739CC666664C9B24924B4B4A56AD54),
    .INITP_08(256'h66CC9B6DB6D2D695AB552AAAAAAA556AD4A5A5A49249B264CCCCC6739C71C78F),
    .INITP_09(256'h6AAD4AD292DB6D9326666339C71C78783F00FFFE00FFFE03F03C3C78E718CE66),
    .INITP_0A(256'h36666338C71E1E0FC01FFFFFFC01F83C3C71C63319933649A49694A54AAD5555),
    .INITP_0B(256'h8E1C3C0FC001FF8007F07C38E38CE6666CDB6DA4A52A556AAAB556A5296D24DB),
    .INITP_0C(256'h3C3E03FFFFFFF00F87871CE7333364DB69695AB55555556AD4B496D936666631),
    .INITP_0D(256'hE1F07FC00000FF03C3C71CE66664D925A5AD4AAA55AAA54A5A4924C9999CE71C),
    .INITP_0E(256'h8F1E0F803FFFFE01F87871CE733366DB6D2D6AD5555556AD6B4924D9B3198E70),
    .INITP_0F(256'h739C78F07F00000001FC1E3C739CCCC9B24B694A9556AD556A52D24936666631),
    .INIT_00(256'h004C98E43888E03490EC48A8086CD43CA41080F064D84CC840C03CC040C850D8),
    .INIT_01(256'hFC040C182838485C748CA8C4E404244C709CC8F4245488BCF4306CA8E82C70B8),
    .INIT_02(256'h581CE4AC784414E4B89068401CF8D8BCA0846C58443424180C00FCF4F4F4F4F8),
    .INIT_03(256'h149418A028B03CCC5CF08418B44CEC882CCC741CC4701CCC8034E8A05C18D898),
    .INIT_04(256'h2868A8EC3078C00C5CACFC50A4FC58B41474D438A00874E050C034A8209C1494),
    .INIT_05(256'h9490909090949CA4B0BCCCDCF0081C38547090B4D8FC24507CA8D80C4078B0EC),
    .INIT_06(256'h500CC8844408CC945C24F0C090643810E8C4A08060442C10FCE8D4C4B8ACA098),
    .INIT_07(256'h60D850CC48C84CD054DC68F48010A438D06804A03CE08028D07824D08030E49C),
    .INIT_08(256'hB4EC245C98D4145498E02870BC0C5CAC0058B00C68C42888F054BC28980478E8),
    .INIT_09(256'h504438302824242424282C34404C58687C90A4BCD8F41434587CA0CCF4245084),
    .INIT_0A(256'h28DC8C44FCB4702CECB0743800CC98643408DCB48C64402000E4C8B098847060),
    .INIT_0B(256'h38A81C8C047CF470F070F478FC84109C2CBC4CE47810AC48E8882CD07820CC78),
    .INIT_0C(256'h78A8D8083C74ACE82464A4E82C70BC0454A0F4449CF44CA80464C42890F460CC),
    .INIT_0D(256'hE0CCBCACA0988C8884808080848C909CA8B4C4D8EC001834506C8CB0D4F8204C),
    .INIT_0E(256'h6814C47424D8904804C07C4000C48C5420ECBC8C60340CE4C09C7C5C40240CF4),
    .INIT_0F(256'h0470E050C034AC249C189818981CA42CB440D060F0841CB44CE88828CC7014BC),
    .INIT_10(256'hB4DC0C3C6CA0D40C4880C0004084C8105CA8F44494E84098F04CA8086CD0349C),
    .INIT_11(256'h644C3C2C1C1004FCF8F4F0F0F0F4F8000C14243444586C84A0B8D8F8183C6088),
    .INIT_12(256'h0CB86414C87C30E8A4601CDC9C6028F0B8845020F4C89C744C2808E8C8AC9078),
    .INIT_13(256'hA41080F064D84CC440BC3CBC3CC448D05CE87404982CC058F49030D07014BC64),
    .INIT_14(256'h204C7CB0E0185088C4004080C40C509CE83484D4287CD42C88E440A4046CD038),
    .INIT_15(256'h70605044383028242020202428303840505C6C8094A8C0DCF81838587CA0C8F4),
    .INIT_16(256'h9040F4A86018D0904C0CD0945C24ECB8885828FCD4AC8460401C00E4C8B09884),
    .INIT_17(256'h68DC50C844BC38B838BC40C850DC68F48418AC40D87410AC4CEC9038DC8834E0),
    .INIT_18(256'hF42C609CD8145498D82068B0FC4898E83890E43C98F450B01478DC44B01C88F8),
    .INIT_19(256'h201C141410101418202830404C5C6C8098B0C8E40020406488B0D804305C8CC0),
    .INIT_1A(256'hE0A05C1CE0A46C34FCC89864380CE0B8906C482808E8CCB49C84706050403428),
    .INIT_1B(256'h28A82CB034C048D464F08418AC44DC7814B454F89C40E89440EC9C5000B86C28),
    .INIT_1C(256'hE02870B80450A0F04094EC449CF858B41878E044B01884F464D448C038B02CA8),
    .INIT_1D(256'h000C182838485C7088A4BCDCF8183C6084ACD804306090C4F83068A4E01C5CA0),
    .INIT_1E(256'h784818ECC098744C2808E8CCB0947C68544030201408FCF4F0ECE8E8E8ECF0F8),
    .INIT_1F(256'h30C45CF89430D07014B86008B05C0CB86C20D48844FCB87838F8BC804810DCA8),
    .INIT_20(256'h1878D43498FC60C834A00C78EC5CD048C038B430B030B438BC44D05CE878089C),
    .INIT_21(256'h24487098C0EC184878ACE0144C84C0FC3C7CC0044890D82470C01064B80C64C0),
    .INIT_22(256'h402C1808F8ECE0D8D0C8C4C4C4C4C8CCD0D8E4F0FC0C1C304458708CA4C4E004),
    .INIT_23(256'h5408C07830ECA86828E8B0743C04D09C6C3C0CE0B48C64401CFCDCBCA0846C54),
    .INIT_24(256'h50D04CD050D45CE46CF88414A434C860F89028C86404A448EC943CE89440F0A0),
    .INIT_25(256'h2068B4FC4898E8388CE0348CE844A00060C4288CF45CC834A01084F86CE45CD4),
    .INIT_26(256'hB4C4D8EC041C3450708CACD0F418406894C0F01C5084B8EC24609CD8185898DC),
    .INIT_27(256'hF0CCAC8C6C50341C04ECD8C4B4A4948880746C686460606064686C74808894A4),
    .INIT_28(256'hC46C18C0701CCC8034E8A05810CC884808CC90541CE4B07C4818E8BC90643C18),
    .INIT_29(256'h188C047CF470EC68E86CEC70F880089420B040D064F88C24C058F89434D87820),
    .INIT_2A(256'hD81C5CA4E83078C41060B00054A8FC54AC0864C42484E44CB01880EC58C834A8),
    .INIT_2B(256'hF1FD0D213549617995ADCDE9092D50749CC4EC184474A4D4083C74ACE4205C98),
    .INIT_2C(256'h4121FDE1C1A5897159412D1909F9E9DDD1C9C1B9B5B1ADADB1B1B5BDC1CDD5E1),
    .INIT_2D(256'hBD6915C57929DD954D05BD7939F5B5793D01C58D5925F1BD8D5D3105DDB18D65),
    .INIT_2E(256'h45C141BD41C145CD51D965F17D0D9D2DC155E98119B551ED8D2DD17519C16911),
    .INIT_2F(256'hC51161B10155A90159B10965C52181E549AD1179E54DB929950979ED61D951C9),
    .INIT_30(256'h25456585A9CDF119416D99C5F1215585B9F1296199D5155191D11559A1E53179),
    .INIT_31(256'h493D2D21150D05FDF9F5F1F1F1F1F5F90109111D2935455565798DA1B9D1ED09),
    .INIT_32(256'h1DE1A56D35FDC995613101D5A579512901D9B59171513115F9DDC5AD95816D5D),
    .INIT_33(256'h851DB551E98925C56509AD51F9A149F5A14DFDAD5D11C57D31EDA5611DDD995D),
    .INIT_34(256'h65D13DAD1D910175ED61D955CD49C949C949CD51D55DE571F98915A535C559ED),
    .INIT_35(256'hA5E9296DB5F9418DD52171BD0D61B1055DB10965BD1979D53599F95DC12991F9),
    .INIT_36(256'h2D455D7591ADCDED0D3151799DC5ED15416D9DCDFD2D6195C9013971ADE92965),
    .INIT_37(256'hDDC9B9AD9D91857D756D6965615D5D5D6161696D757D85919DA9B9C9D9ED0115),
    .INIT_38(256'h996129F1BD895521F1C195693D11E9C19975512D0DEDCDB191795D452D1501ED),
    .INIT_39(256'h4DED8D2DD1751DC16915BD6915C57525D5893DF1A96119D5914D0DC98D4D11D5),
    .INIT_3A(256'hD54DC945C141BD41C145C94DD159E16DF98511A131C155E57D11A941D97511AD),
    .INIT_3B(256'h196DC51971C9217DD93595F555B5197DE149B11985ED59C935A51989FD71E55D),
    .INIT_3C(256'hFD2D5D91C1F5296199D1094581BDF93979BDFD4185CD155DA5F13D89D52575C9),
    .INIT_3D(256'h65717D8999A9B9CDE1F5092139516D85A1C1DDFD1D416589ADD1F9214D79A5D1),
    .INIT_3E(256'h2D1901EDD9C5B5A59585796D61554D453D3935312D2D2D2D2D31353941495159),
    .INIT_3F(256'h420AD29A622DF9C995653509D9AD85593109E1B99571512D0DEDCDB195796145),
    .INIT_40(256'h19B555F59535D97D21C97119C56D19C97525D5893DF1A55D15CD8541FDBD7939),
    .INIT_41(256'hE159D14DC945C141BD41C145C94DD55DE571FD8919A535C959ED8519B149E17D),
    .INIT_42(256'h75C51569BD1569C11D75D12D8DED4DAD1175D941A9117DE555C131A11185F96D),
    .INIT_43(256'hE9113D6995C1F1215589BDF1296199D5114D8DC90D4D91D51961A9F13D89D525),
    .INIT_44(256'h5D5D6165696D757D85919DADB9C9DDED01152D455D7991B1CDED0D2D517599C1),
    .INIT_45(256'hEDC59D7951310DEDCDAD91755D452D1501EDD9C9B9A99D91857D756D6961615D),
    .INIT_46(256'hB1610DBD7121D58D41F9B56D29E9A56529E9AD713901C995612DFDCD9D6D4115),
    .INIT_47(256'hC949CD55D961ED7501911DAD3DD165F99129C15DF99935D57919BD6509B15D05),
    .INIT_48(256'h49A1F951AD0965C52589E951B51D85ED59C535A51589F971E55DD551CD49C949),
    .INIT_49(256'h5179A5D501316195C9FD356DA5E11D5D99DD1D61A5ED317DC5115DADFD4DA1F5),
    .INIT_4A(256'hF5F1F1F1F1F5F9FD050D15212D3D495D6D8195ADC5DDF91531517191B5D90129),
    .INIT_4B(256'h5521F1C5996D4119F1CDA98565452509EDD1B9A18D7965554535291D110901F9),
    .INIT_4C(256'h8121C56509B15901A95501B16111C57931E5A15915D1915115D5996129F1B985),
    .INIT_4D(256'h9D0D7DF165D951CD45C141BD41C145C951D961ED79099529B94DE57911AD49E5),
    .INIT_4E(256'hB5F53979BD054D95DD2979C51569BD1169C11975D12D8DED51B51981E955C12D),
    .INIT_4F(256'hE9F909192D41597189A5C1E1FD2141658DB1DD05315D8DBDF125598DC5013D79),
    .INIT_50(256'h512D09E9CDAD9579614935210DFDF1E1D5CDC1BDB5B1B1ADADB1B5B9C1C9D1DD),
    .INIT_51(256'hFCA85400B06010C47830E8A45C1CD8985C20E4AC743C08D4A4754519EDC59D75),
    .INIT_52(256'h0880F870EC6CE868EC70F47C048C18A834C858EC8018B04CE48424C46408AC54),
    .INIT_53(256'h88CC1058A0E83480CC1C70C0186CC42078D83494F858C0248CF864D040B02094),
    .INIT_54(256'h94A4B4C4D8EC041C34506C8CACCCF0183C6490BCE818487CB0E41C5490CC0848),
    .INIT_55(256'h4018F4D0AC8C7050341C04ECD8C4B4A4948880746C686460606064686C748088),
    .INIT_56(256'hA044E88C34E08C38E89848FCB46820DC985818D89C6024ECB884501CF0C09468),
    .INIT_57(256'hC834A41484F86CE45CD450D04CD050D45CE46CF88410A034C85CF48C28C46000),
    .INIT_58(256'hD0043C74B0E82868A8EC3078C00854A0F04094E83C94EC48A40464C82890F860),
    .INIT_59(256'hC8C4C4C4C4C8D0D8E0ECF808182C40546C84A0BCDCFC1C40648CB4E00C3C6C9C),
    .INIT_5A(256'hC0844C14E0AC784818ECC09870482404E0C4A48C705844301C0CFCF0E4D8D0CC),
    .INIT_5B(256'hD05CEC780CA034C860FC9834D47818C0640CB86410C07024D8904804C07C3CFC),
    .INIT_5C(256'h0C5CB00860B81470D03094F85CC4309C0878E85CD044BC38B430B030B438C048),
    .INIT_5D(256'h7C94B0CCE808284C7498C0EC184878A8DC104880BCF83878B8FC4488D4206CB8),
    .INIT_5E(256'h3C18F8DCBCA488705C483828180C00F8F0ECE8E8E8ECF0F4FC08142030405468),
    .INIT_5F(256'h58F89C44EC9440F0A05004B87028E0A05C1CE0A46830F8C490603004D8AC8460),
    .INIT_60(256'hDC44AC1884F064D448C034B02CA828A82CB038C048D464F48418B044E07818B4),
    .INIT_61(256'hE00C386498C8FC346CA4E01C5CA0E0286CB800509CEC4094E8409CF854B41478),
    .INIT_62(256'h6C5C4C403028201814101014141C20283440506070849CB4CCE80828486C90B8),
    .INIT_63(256'h9848FCB06820D8985414D89C602CF4C08C5C3004D8B08864402000E4C8B09880),
    .INIT_64(256'h68DC50C840BC38B838BC44C850DC68F8881CB044DC7814B050F4983CE49038E8),
    .INIT_65(256'hEC245C94D00C4C90D01860A8F44090E03488DC3890EC4CAC1074D840AC1884F4),
    .INIT_66(256'h3830282420202024283038445060708498B0C8E4001C406084ACD4FC285888B8),
    .INIT_67(256'h500CC4804000C4885018E0B07C4C20F4C8A07C583818F8DCC0A894806C5C5040),
    .INIT_68(256'h48C43CBC3CBC40C44CD864F08010A438D06C04A440E4882CD47C28D48434E89C),
    .INIT_69(256'h28609CDC1C60A4E8307CC81464B80C64BC1470D03090F458C02C980474E85CD0),
    .INIT_6A(256'hF8F4F0F0F0F4F8FC04101C2C3C4C647890ACC8E808284C749CC8F4205084B8F0),
    .INIT_6B(256'hC8844000C080480CD4A06C3C0CDCB488603C18F8D8B8A0846C58443424140C00),
    .INIT_6C(256'hA41C981898189C24AC34C050E070049C34D06C08A84CF09840E89444F4A85C10),
    .INIT_6D(256'h8CC400407CC0044890D82474C41468BC1470CC2888E84CB41C84F060D040B42C),
    .INIT_6E(256'h908C8480808084888C98A0ACBCCCE0F40C24405C7C9CC0E40C34608CBCEC2054),
    .INIT_6F(256'hBC702CE8A46424E8AC743C08D8A8784C20F8D4B08C6C50341800ECD8C4B4A89C),
    .INIT_70(256'h1084FC78F470F070F47C048C1CA838CC60F49028C46404A84CF49C44F4A05404),
    .INIT_71(256'h98CC003874B0EC2C70B4FC448CDC2878CC2078D02C88E848AC1078E44CBC2C9C),
    .INIT_72(256'h584C40342C2824242424283038445060708498B0C8E4002040648CB4DC083464),
    .INIT_73(256'h5C0CBC7028E0985414D4985C24ECB4845024F4CCA07C583414F4D8BCA4907C68),
    .INIT_74(256'hA41080F468DC54D04CC848CC50D860E878049828BC54F08828C4680CB05800AC),
    .INIT_75(256'h386490C0F0245C94CC084484C80C509CE43080D02478D02880E03CA00468D038),
    .INIT_76(256'h1C08F0DCCCBCB0A49C94909090909498A0ACB8C4D4E8FC102C446080A0C4E810),
    .INIT_77(256'h58FCA450FCAC5C0CC07830ECA86828ECB078400CD8A87C5024FCD8B490705438),
    .INIT_78(256'hEC4CB41884F05CCC3CB028A018941494149C20A834C050E07408A038D47414B4),
    .INIT_79(256'hD8F81C406890B8E4144478ACE41C5898D8185CA0E83480CC1C70C41C74CC2C88),
    .INIT_7A(256'h2404E4C4A88C745C483828180C04FCF8F4F4F4F4FC000C18243444586C84A0BC),
    .INIT_7B(256'hD46C08A848EC9034E08838E4984C00B8702CE8A86C30F4BC885424F4C89C704C),
    .INIT_7C(256'hE43894EC4CAC0C70D43CA81480F064D850C840C03CC040C84CD864F08010A43C),
    .INIT_7D(256'h54688098B0CCEC0C30547CA4D0FC2C5C90C4FC3870B0F03074BC04509CEC3C8C),
    .INIT_7E(256'h2CFCD0A47C54300CECCCB0988068544434241C1008040000000408101C243444),
    .INIT_7F(256'h64F08014A83CD4700CAC4CEC9438E48C3CEC9C5004BC7430F0B07038FCC4905C),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized33
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h00000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000),
    .INITP_02(256'hFFFFFFF0000000000000000000007FFFFFFFFFFFFFFFFFFFFFFF800000000000),
    .INITP_03(256'hFC0000000000000007FFFFFFFFFFFFFFFE000000000000000001FFFFFFFFFFFF),
    .INITP_04(256'hFFFFFFFFFC0000000000001FFFFFFFFFFFFF800000000000003FFFFFFFFFFFFF),
    .INITP_05(256'hFF800000000003FFFFFFFFFFE000000000003FFFFFFFFFFFC000000000001FFF),
    .INITP_06(256'hFFFF0000000000FFFFFFFFFF80000000003FFFFFFFFFE00000000003FFFFFFFF),
    .INITP_07(256'h007FFFFFFFF8000000003FFFFFFFFE0000000007FFFFFFFFE0000000007FFFFF),
    .INITP_08(256'h0003FFFFFFFFFE0000000001FFFFFFFFFC000000000FFFFFFFFFC000000000FF),
    .INITP_09(256'h000FFFFFFFFFFF800000000003FFFFFFFFFF80000000000FFFFFFFFFF8000000),
    .INITP_0A(256'hFFFFF00000000000007FFFFFFFFFFFF0000000000007FFFFFFFFFFF800000000),
    .INITP_0B(256'hFFFFFFFFFFC0000000000000007FFFFFFFFFFFFFF800000000000003FFFFFFFF),
    .INITP_0C(256'h000000000000000000001FFFFFFFFFFFFFFFFFFF000000000000000000FFFFFF),
    .INITP_0D(256'hFFFFFFFFFFF80000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFC),
    .INITP_0E(256'h000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0F0F0E0E0E0D0D0D0C0C0C0B0B0B0A0A0A0A0909090808080807070707060606),
    .INIT_01(256'h1C1C1C1B1B1A1A1919181817171716161515141414131312121211111010100F),
    .INIT_02(256'h2E2E2D2C2C2B2B2A2929282827262625252424232322222121201F1F1E1E1D1D),
    .INIT_03(256'h444343424140403F3E3D3D3C3B3B3A3939383737363535343333323131302F2F),
    .INIT_04(256'h5E5D5C5B5B5A5958575655555453525151504F4E4D4D4C4B4A49494847464645),
    .INIT_05(256'h7C7B7A79787776757473727170706F6E6D6C6B6A69686766656463636261605F),
    .INIT_06(256'h9F9E9C9B9A9998979695939291908F8E8D8C8B8A8988878684838281807F7E7D),
    .INIT_07(256'hC5C4C3C1C0BFBEBCBBBAB9B7B6B5B4B3B1B0AFAEADABAAA9A8A7A6A4A3A2A1A0),
    .INIT_08(256'hF0EEEDECEAE9E8E6E5E3E2E1DFDEDDDBDAD9D7D6D5D3D2D1CFCECDCCCAC9C8C6),
    .INIT_09(256'h1F1D1C1A191716141311100E0D0B0A080705040201FFFEFDFBFAF8F7F5F4F3F1),
    .INIT_0A(256'h52504E4D4B494846444341403E3C3B393836343331302E2D2B29282625232220),
    .INIT_0B(256'h8987858382807E7C7A7977757372706E6D6B6967666462615F5D5C5A58575553),
    .INIT_0C(256'hC4C2C0BEBCBAB8B6B5B3B1AFADABA9A8A6A4A2A09E9C9B9997959392908E8C8A),
    .INIT_0D(256'h0301FFFDFBF9F7F5F3F1EFEDEBE9E7E5E3E1DFDDDBD9D7D5D3D1CFCDCBC9C8C6),
    .INIT_0E(256'h464442403E3B39373533312F2D2A28262422201E1C1A181513110F0D0B090705),
    .INIT_0F(256'h8E8B89878482807E7B79777572706E6C69676563605E5C5A585553514F4D4B48),
    .INIT_10(256'hD9D6D4D2CFCDCAC8C6C3C1BFBCBAB7B5B3B0AEACA9A7A5A2A09E9B9997949290),
    .INIT_11(256'h282623211E1C191614110F0C0A07050300FEFBF9F6F4F1EFECEAE7E5E3E0DEDB),
    .INIT_12(256'h7C797674716E6C696664615E5C595754514F4C4A4744423F3D3A383532302D2B),
    .INIT_13(256'hD3D0CDCAC8C5C2BFBDBAB7B4B2AFACA9A7A4A19F9C999694918E8C898684817E),
    .INIT_14(256'h2E2B282522201D1A1714110E0B09060300FDFAF7F5F2EFECE9E6E4E1DEDBD8D6),
    .INIT_15(256'h8D8A8784817E7B7875726F6C696663605D5A5754514E4B484543403D3A373431),
    .INIT_16(256'hF0EDEAE7E4E1DDDAD7D4D1CECBC8C4C1BEBBB8B5B2AFACA9A6A3A09C99969390),
    .INIT_17(256'h5754514D4A4744403D3A3733302D2A2623201D1A1613100D0A060300FDFAF7F3),
    .INIT_18(256'hC2BFBBB8B4B1AEAAA7A4A09D9996938F8C8985827F7B7875726E6B6864615E5A),
    .INIT_19(256'h312D2A26231F1C1815110E0A070300FCF9F5F2EEEBE8E4E1DDDAD6D3D0CCC9C5),
    .INIT_1A(256'hA39F9C9894918D8A86827F7B7874706D6966625F5B5754504D4946423F3B3834),
    .INIT_1B(256'h1915120E0A0603FFFBF7F4F0ECE9E5E1DEDAD6D2CFCBC7C4C0BCB9B5B2AEAAA7),
    .INIT_1C(256'h938F8B8784807C7874706C6965615D5956524E4A46433F3B3733302C2824211D),
    .INIT_1D(256'h110D090501FDF9F5F1EDE9E5E1DDD9D5D1CDC9C6C2BEBAB6B2AEAAA6A29F9B97),
    .INIT_1E(256'h928E8A85817D7975716D6965615D5955514D4945413D3834302C2824201C1814),
    .INIT_1F(256'h17120E0A0602FDF9F5F1EDE9E4E0DCD8D4D0CBC7C3BFBBB7B3AEAAA6A29E9A96),
    .INIT_20(256'h9F9B96928E8985817D7874706B67635F5A56524E4945413D3834302C27231F1B),
    .INIT_21(256'h2B27221E1915110C0803FFFBF6F2EDE9E5E0DCD8D3CFCAC6C2BDB9B5B0ACA8A3),
    .INIT_22(256'hBBB6B2ADA8A49F9B96928D8984807B77726E6965605C58534F4A46413D383430),
    .INIT_23(256'h4E4944403B36322D29241F1B16120D0804FFFBF6F1EDE8E4DFDAD6D1CDC8C4BF),
    .INIT_24(256'hE4DFDBD6D1CCC8C3BEB9B5B0ABA7A29D98948F8A86817C78736E6A65605C5752),
    .INIT_25(256'h7E79746F6B66615C57524E49443F3A36312C27221D19140F0A0601FCF7F2EEE9),
    .INIT_26(256'h1B16110C0803FEF9F4EFEAE5E0DBD6D1CCC7C2BEB9B4AFAAA5A09B96928D8883),
    .INIT_27(256'hBCB7B2ADA8A39E99948E89847F7A75706B66615C57524D48433E39342F2A2520),
    .INIT_28(256'h605B56504B46413C37312C27221D18130E0803FEF9F4EFEAE5E0DAD5D0CBC6C1),
    .INIT_29(256'h0702FDF7F2EDE7E2DDD8D2CDC8C3BEB8B3AEA9A39E99948F89847F7A756F6A65),
    .INIT_2A(256'hB1ACA7A19C97918C87817C77716C66615C57514C47413C37312C27211C17120C),
    .INIT_2B(256'h5F59544F49443E39332E28231E18130D0802FDF8F2EDE7E2DDD7D2CCC7C2BCB7),
    .INIT_2C(256'h100A04FFF9F4EEE9E3DED8D3CDC7C2BCB7B1ACA6A19B96908B85807A756F6A64),
    .INIT_2D(256'hC3BEB8B2ADA7A19C96918B85807A746F69645E58534D47423C37312C26201B15),
    .INIT_2E(256'h7A746F69635D58524C46413B35302A241E19130D0802FCF6F1EBE5E0DAD4CFC9),
    .INIT_2F(256'h342E28221D17110B05FFFAF4EEE8E2DCD7D1CBC5BFBAB4AEA8A39D97918B8680),
    .INIT_30(256'hF1EBE5DFD9D3CDC7C1BBB5AFA9A49E98928C86807A746F69635D57514B45403A),
    .INIT_31(256'hB0AAA49E98928C86807A746E68625C56504A443E38322C26201A140E0802FCF7),
    .INIT_32(256'h736C66605A544E48423C362F29231D17110B05FFF9F3EDE7E0DAD4CEC8C2BCB6),
    .INIT_33(256'h38322B251F19130C0600FAF4EDE7E1DBD5CFC8C2BCB6B0AAA49D97918B857F79),
    .INIT_34(256'h00F9F3EDE7E0DAD4CDC7C1BBB4AEA8A29B958F89827C767069635D57514A443E),
    .INIT_35(256'hCAC4BEB7B1AAA49E97918B847E78716B655E58524B453F38322C251F19130C06),
    .INIT_36(256'h98918B847E77716A645E57514A443D37312A241D17110A04FDF7F1EAE4DDD7D1),
    .INIT_37(256'h67615A544D47403A332D262019130C06FFF9F2ECE5DFD8D2CBC5BEB8B1ABA59E),
    .INIT_38(256'h3A332D261F19120C05FEF8F1EBE4DDD7D0CAC3BDB6AFA9A29C958F88827B756E),
    .INIT_39(256'h0F0801FBF4EDE7E0D9D3CCC5BFB8B1ABA49D979089837C756F68625B544E4740),
    .INIT_3A(256'hE6DFD8D2CBC4BDB7B0A9A29C958E87817A736D665F58524B443E373029231C15),
    .INIT_3B(256'hC0B9B2ABA49D979089827B746E676059524C453E37302A231C150F0801FAF3ED),
    .INIT_3C(256'h9B958E878079726B645D575049423B342D261F19120B04FDF6EFE9E2DBD4CDC6),
    .INIT_3D(256'h7A736C655E575049423B342D261F18110A03FCF5EFE8E1DAD3CCC5BEB7B0A9A2),
    .INIT_3E(256'h5A534C453E373029221B140D06FFF8F1EAE3DCD5CEC7C0B9B2ABA49D968F8881),
    .INIT_3F(256'h3D362E272019120B04FDF6EFE7E0D9D2CBC4BDB6AFA8A19A938B847D766F6861),
    .INIT_40(256'h60676E747B828990979DA4ABB2B9C0C6CDD4DBE2E9EFF6FD040B12191F262D34),
    .INIT_41(256'h878E959CA2A9B0B7BDC4CBD2D8DFE6EDF3FA01080F151C232A30373E454C5259),
    .INIT_42(256'hB1B8BFC5CCD3D9E0E7EDF4FB01080F151C232930373E444B52585F666D737A81),
    .INIT_43(256'hDDE4EBF1F8FE050C12191F262D333A40474E545B62686F757C838990979DA4AB),
    .INIT_44(256'h0C131920262D333A40474D545A61676E757B82888F959CA2A9AFB6BDC3CAD0D7),
    .INIT_45(256'h3D444A51575E646A71777E848B91989EA5ABB1B8BEC5CBD2D8DFE5ECF2F9FF06),
    .INIT_46(256'h71787E848B91979EA4AAB1B7BEC4CAD1D7DDE4EAF1F7FD040A11171D242A3137),
    .INIT_47(256'hA8AEB4BBC1C7CDD4DAE0E7EDF3F900060C13191F252C32383F454B52585E656B),
    .INIT_48(256'hE1E7EDF4FA00060C13191F252B32383E444A51575D636970767C82898F959BA2),
    .INIT_49(256'h1D23292F363C42484E545A60666C73797F858B91979DA4AAB0B6BCC2C8CFD5DB),
    .INIT_4A(256'h5C62686E747A80868C92989EA4AAB0B6BCC2C8CED4DAE0E7EDF3F9FF050B1117),
    .INIT_4B(256'h9EA4A9AFB5BBC1C7CDD3D9DFE5EBF1F7FC02080E141A20262C32383E444A5056),
    .INIT_4C(256'hE2E8EEF4FAFF050B11171D22282E343A40454B51575D63696F747A80868C9298),
    .INIT_4D(256'h2A30353B41464C52585D63696F747A80868B91979DA3A8AEB4BABFC5CBD1D7DC),
    .INIT_4E(256'h747A80858B91969CA1A7ADB2B8BEC3C9CFD4DAE0E5EBF1F6FC02080D13191E24),
    .INIT_4F(256'hC2C7CDD3D8DEE3E9EEF4F9FF040A10151B20262C31373C42474D53585E64696F),
    .INIT_50(256'h13181E23282E33393E44494F54595F646A6F757A80858B90969BA1A6ACB1B7BC),
    .INIT_51(256'h666C71777C81878C91979CA1A7ACB1B7BCC2C7CCD2D7DDE2E7EDF2F8FD02080D),
    .INIT_52(256'hBEC3C8CDD2D8DDE2E7EDF2F7FD02070C12171C21272C31373C41474C51575C61),
    .INIT_53(256'h181D22272C31373C41464B50565B60656A6F757A7F84898F94999EA3A9AEB3B8),
    .INIT_54(256'h757A7F84898E94999EA3A8ADB2B7BCC1C6CBD0D5DAE0E5EAEFF4F9FE03080E13),
    .INIT_55(256'hD6DBE0E5EAEFF4F9FE03080C11161B20252A2F34393E43484D52575C61666B70),
    .INIT_56(256'h3A3F44494E52575C61666B6F74797E83888D92969BA0A5AAAFB4B9BEC2C7CCD1),
    .INIT_57(256'hA2A7ABB0B5B9BEC3C8CCD1D6DBDFE4E9EEF2F7FC01060A0F14191D22272C3136),
    .INIT_58(256'h0D12161B1F24292D32363B4044494E52575C60656A6E73787C81868A8F94989D),
    .INIT_59(256'h7B8084898D92969B9FA4A8ADB2B6BBBFC4C8CDD1D6DADFE4E8EDF1F6FBFF0408),
    .INIT_5A(256'hEDF2F6FBFF03080C1115191E22272B3034383D41464A4F53585C6065696E7277),
    .INIT_5B(256'h63676B7074787D8185898E92969B9FA3A8ACB0B5B9BDC2C6CACFD3D8DCE0E5E9),
    .INIT_5C(256'hDCE0E4E9EDF1F5F9FD02060A0E12171B1F23272C3034383D4145494E52565A5F),
    .INIT_5D(256'h595D6165696D7175797D81858A8E92969A9EA2A6AAAEB3B7BBBFC3C7CBD0D4D8),
    .INIT_5E(256'hD9DDE1E5E9EDF1F5F9FD0105090D1114181C2024282C3034383D4145494D5155),
    .INIT_5F(256'h5D6165696C7074787C8084878B8F93979B9FA2A6AAAEB2B6BABEC2C6C9CDD1D5),
    .INIT_60(256'hE5E9ECF0F4F7FBFF03060A0E1215191D2124282C3033373B3F43464A4E525659),
    .INIT_61(256'h7074787B7F82868A8D9194989C9FA3A7AAAEB2B5B9BCC0C4C7CBCFD2D6DADEE1),
    .INIT_62(256'h0003070A0E1115181C1F23262A2D3134383B3F4246494D5054575B5F6266696D),
    .INIT_63(256'h9396999DA0A4A7AAAEB1B4B8BBBFC2C5C9CCD0D3D6DADDE1E4E8EBEEF2F5F9FC),
    .INIT_64(256'h2A2D3033373A3D4044474A4D5154575A5E6164686B6E7275787B7F8285898C8F),
    .INIT_65(256'hC4C8CBCED1D4D7DADDE1E4E7EAEDF0F3F7FAFD0003060A0D1013161A1D202326),
    .INIT_66(256'h6366696C6F7275787B7E8184878A8D909396999CA0A3A6A9ACAFB2B5B8BBBEC1),
    .INIT_67(256'h06090B0E1114171A1D202225282B2E3134373A3D404345484B4E5154575A5D60),
    .INIT_68(256'hACAFB2B4B7BABDBFC2C5C8CACDD0D3D6D8DBDEE1E4E6E9ECEFF2F5F7FAFD0003),
    .INIT_69(256'h57595C5E616466696C6E717476797C7E818486898C8E919496999C9FA1A4A7A9),
    .INIT_6A(256'h05070A0C0F111416191C1E212326282B2D303235383A3D3F4244474A4C4F5154),
    .INIT_6B(256'hB7BABCBFC1C3C6C8CACDCFD2D4D6D9DBDEE0E3E5E7EAECEFF1F4F6F9FBFE0003),
    .INIT_6C(256'h6E70727577797B7E80828487898B8E90929497999B9EA0A2A5A7A9ACAEB0B3B5),
    .INIT_6D(256'h282A2D2F31333537393B3E40424446484B4D4F515355585A5C5E60636567696C),
    .INIT_6E(256'hE7E9EBEDEFF1F3F5F7F9FBFDFF01030507090B0D0F111315181A1C1E20222426),
    .INIT_6F(256'hA9ABADAFB1B3B5B6B8BABCBEC0C2C4C6C8C9CBCDCFD1D3D5D7D9DBDDDFE1E3E5),
    .INIT_70(256'h7072737577797A7C7E8082838587898A8C8E9092939597999B9C9EA0A2A4A6A8),
    .INIT_71(256'h3B3C3E404143444648494B4D4E5052535557585A5C5D5F6162646667696B6D6E),
    .INIT_72(256'h0A0B0D0E101113141617191A1C1D1F202223252628292B2D2E30313334363839),
    .INIT_73(256'hDDDEDFE1E2E3E5E6E8E9EAECEDEEF0F1F3F4F5F7F8FAFBFDFEFF010204050708),
    .INIT_74(256'hB4B5B6B7B9BABBBCBEBFC0C1C3C4C5C6C8C9CACCCDCECFD1D2D3D5D6D7D9DADB),
    .INIT_75(256'h8F9091929395969798999A9B9C9E9FA0A1A2A3A4A6A7A8A9AAABADAEAFB0B1B3),
    .INIT_76(256'h6F70707172737475767778797A7B7C7D7E7F8081828384868788898A8B8C8D8E),
    .INIT_77(256'h5253545555565758595A5B5B5C5D5E5F60616263636465666768696A6B6C6D6E),
    .INIT_78(256'h3A3B3B3C3D3D3E3F40404142434344454646474849494A4B4C4D4D4E4F505151),
    .INIT_79(256'h262627282829292A2B2B2C2C2D2E2E2F2F303131323333343535363737383939),
    .INIT_7A(256'h16171717181819191A1A1B1B1C1C1C1D1D1E1E1F1F2021212222232324242525),
    .INIT_7B(256'h0A0B0B0B0C0C0C0D0D0D0E0E0E0F0F0F10101011111212121313141414151516),
    .INIT_7C(256'h03030303040404040405050505050606060607070707080808080909090A0A0A),
    .INIT_7D(256'h0000000000000000000000000000010101010101010101020202020202020303),
    .INIT_7E(256'h0100000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0605050505050404040404030303030303020202020202020101010101010101),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized34
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h380335D660066AACE01C956C7FC6D5B381E4AA47039A5A63F8CAA99FF1B55982),
    .INITP_01(256'hA670399559820ED25B800C956CFFE6556C7F8DAAD8FF195533FF1B56CE0792AB),
    .INITP_02(256'h987C3255338073514CFFE24ADB800CD55987864AB67C79955B1FE32D69C0064A),
    .INITP_03(256'hC9569C7E192AB33FF196B4CF9F325698FF8C95247CF9B553380E25548E079B54),
    .INITP_04(256'h6AA4C7FF192A923C0F36AA99C01CDAAB31FF8DA5A4E00392AA4E1C19A56D87F8),
    .INITP_05(256'hF32555B3810392552CC3F872575261FF1DB55267C1E66AA931FF8CD2B48E0073),
    .INITP_06(256'h931F8FC6495524E3FFC765AA5B1C003992AADB1F07CCD2A9271FF0CDAAA4CF00),
    .INITP_07(256'hF0E64B5569338FFF0E6DA92B6C707E0E64B55A4CE020399A554B33C00799A554),
    .INITP_08(256'h0071B4AB4DC7FF8E495524C7E3F192554B33C00799A554B338080E64B55A4CE0),
    .INITP_09(256'h0C95D49C3F8669549381039B55499E01E64AAB661FF1C92A9667C1F1B6AA9338),
    .INITP_0A(256'hB6700732AAD9E07892A931FFC64AAD9C00E25A9663FF192AACCF07CC955B71FF),
    .INITP_0B(256'hD499F3E65AD31FF99AA930FC72D5263FC36D4B3070E4AA93800E4B4B63FF19AA),
    .INITP_0C(256'h6003B6A48FFE65159C039954987C3255B3C0E25548E039955B3E7C495263FE32),
    .INITP_0D(256'hCFFE6D526003B496E0833553381CCAA4C0072D698FF1B5533C7CDAA4C3C33556),
    .INITP_0E(256'h56C7FC6D52700E6AACC00CD7598039AA93C0E6D5B1FF995531FE36AB63FC6D54),
    .INITP_0F(256'h0CD56C7F8DAA4C0064AB63FC6D566083355B1FF32AA63F8CB4B381C4AA4F039B),
    .INIT_00(256'h406080C00060C020C04000A0806040404060A0E02080008020C060400000E000),
    .INIT_01(256'hA0A0A0C0E02060C040C040E0A06040202020206080E040A020A040E0A0806040),
    .INIT_02(256'hE0C0C0C0C0E02060C020A020C0804000E0C0C0E0002060C020A020C06000E0A0),
    .INIT_03(256'h00E0A0A0A0A0C0004080E060E08020C0A06060406060A0E02080E06000A04000),
    .INIT_04(256'h00C080604040406080C00060E060E08020E0C0A0A0A0A0C00040A0008000A040),
    .INIT_05(256'hC06000C0A080808080A0E02080E060E08040E0C0A08080A0C0E02080E040C060),
    .INIT_06(256'h008020E080604020204060A0E02080008020C0804020000000206080E040A020),
    .INIT_07(256'hC020C04000C0806040406080A0E040A0008020C080402000E000004080C02080),
    .INIT_08(256'hA0208020A06020E0C0A0A0A0C0004080E060E06000C080604020404080A0E040),
    .INIT_09(256'hC02080008020C08040202020204060A00060C040E08020E0C0A0A0A0C0E00060),
    .INIT_0A(256'hC00060C040E06020E0A080606080A0C00060C020A040E0A06020000000204080),
    .INIT_0B(256'h80A00060C040E06020E0A0808080A0C0E02080E060E06020C080604040406080),
    .INIT_0C(256'hA0C00060C040C040E0A06040202020406080E020A000A020E0A0604020202040),
    .INIT_0D(256'h012160A00060E06000C0604020000000206080E040A020C06000C0A080606080),
    .INIT_0E(256'h6181A1E141A121A121C181412101E101012161A10161E16101A1612101E1E1E1),
    .INIT_0F(256'h6181A1E12181018101A16121E1C1C1C1C1E12161A10181018121E1A161414141),
    .INIT_10(256'hC1E10141A1E161E16101A1614101010101214181E141A121A14101C181614141),
    .INIT_11(256'h416181C12161E161E18121E1C1A1818181A1C10161A121A121C1612101C1C1C1),
    .INIT_12(256'h81A1E12161C121A141E181412101E1E1E1014181C1218101A141E1A161414141),
    .INIT_13(256'h216181C12181018121C1612101E1E1E1E1214181E141A121C16101C1A1818181),
    .INIT_14(256'hE20242A20262E28202C2824222020202226181E141A121A141E1A16141210121),
    .INIT_15(256'h4282C22282028222C2824222020202224282C20282E26202A24202E2C2C2C2C2),
    .INIT_16(256'hE242A2028202A24202C28282626282A2E22262C242C242E2A262220202E20222),
    .INIT_17(256'h82E242C242E2824222E2C2C2C2E2024282E242A222C26202C2A28262626282C2),
    .INIT_18(256'h82E26202A26202E2C2A2A2C2C2024282E242C242C28222E2C2A28282A2C2E222),
    .INIT_19(256'h8303A34303E3A3A38383A3C30343830262E26202C2824222020202224282C222),
    .INIT_1A(256'h03C363430303E3E3032363A3E343C323C36303C383634343434363A3E3238303),
    .INIT_1B(256'hC383634343436383A30343A3238323C36323E3C3A3A3A3C3E30343A30363E383),
    .INIT_1C(256'h03E3E303034383C32383038303A36323E3C3C3A3C3C3032383C323A323A34303),
    .INIT_1D(256'h446484C40463C343C343E3834323E3E3C3C3E3032363C32383038323C3834323),
    .INIT_1E(256'h4484E444C444C46404C4A4644444446484A4E444A4048404A444E4A484644444),
    .INIT_1F(256'h44C444E48424E4C4A4848484A4C4044484E464C46404A44424E4C4C4C4C4E404),
    .INIT_20(256'hC4642404C4C4A4A4C4E40444A40464C464E4844404C4A4848484A4C4E42484E4),
    .INIT_21(256'h25250525254585C50565C444C46404A4642404E4E4E404244484C42484048424),
    .INIT_22(256'h052565A50565C545C56505C5854525252525456585C52585E565E58525C58565),
    .INIT_23(256'hA5058505A54505C585654545454585A5E52585E565E56505A5652505E5E5E5E5),
    .INIT_24(256'h6525E5A58585658585A5E52565C525A525A54505C58545452525254565A5E545),
    .INIT_25(256'hC6C6C6E60646A6E646C646C66606A6664626060606264666A6E646A5058525C5),
    .INIT_26(256'h0646A626A626C66626E6A68686666686A6C60646A60686E68626C6662606E6C6),
    .INIT_27(256'h8626E6C6866666666686A6E62666C626A626A64606A6664626060606264666A6),
    .INIT_28(256'hA7C7C7072767C70787E76707A74707C787674746466686A6E62686E646C646E6),
    .INIT_29(256'hC747A747C76727E7A787674747676787C70747A70787078727C7672707C7C7A7),
    .INIT_2A(256'h2707E7C7C7E7E7274787C72787078707A747E7A7674727272727276787C70767),
    .INIT_2B(256'h0868A80888E86808A84808C8A8686848486887A7E72767C727A727A747E7A767),
    .INIT_2C(256'hE88868280808E80808284888C80868C848C848E8884808C8A88868688888A8E8),
    .INIT_2D(256'hC8084888E848A828C848E8A86828E8C8C8A8A8C8E8082868C82888E868E88828),
    .INIT_2E(256'h09C98949290909E90909296989C92989E969E96909A94808C8A88868686888A8),
    .INIT_2F(256'h092989C92989098909A94909C989492929090929496989C92969C949A949C969),
    .INIT_30(256'hC989694929292929496989C90969C9298909A929C9894909C9A9A9898989A9C9),
    .INIT_31(256'hAA0A6AEA4AEA6A0AAA6A2AEACAAA8A8A8A8AAACA0A4A8AEA4AAA2AAA2ACA6909),
    .INIT_32(256'hEAEACAEAEA0A2A6AAAEA4AAA0A8A0AAA2ACA8A4A0ACAAA8A8A8A8AAACAEA2A6A),
    .INIT_33(256'hAB2BCB6B2BEBAB8B4B4B2A2A2A4A6A8ACA0A4AAA0A6AEA6AEA8A2AEA8A6A2A0A),
    .INIT_34(256'h0B4B8BCB2B8BEB6BEB8B0BAB6B0BCBAB6B4B4B4B4B4B6B8BABEB2B8BCB4BAB2B),
    .INIT_35(256'h6B4B4B2B2B4B4B6BABCB0B6BAB0B8BEB6B0B8B2BEB8B4B2BEBCBCBABABCBCBEB),
    .INIT_36(256'hCC6C0CAC6C2CECAC8C6C6C6C6C8C8CCCEC2C6CAC0C6CEC6CEB6B0BAB4B0BCB8B),
    .INIT_37(256'h6CAC0C6CEC4CCC6CEC8C2CECAC6C4C2C0CECECEC0C0C4C6CACEC2C8CEC4CCC4C),
    .INIT_38(256'h6D6D8DADCD0D4D8DED4CAC0C8C0CAC4CEC8C4C0CCCAC8C6C6C6C6C8C8CCCEC2C),
    .INIT_39(256'hCDAD8D6D6D6D6D6D8DADED0D4DADED4DAD2DAD2DAD4DED8D4D0DCDAD8D6D6D4D),
    .INIT_3A(256'hED8D2DEDAD6D4D2D0D0DED0D0D2D4D6D8DCD0D6DCD2D8D0D8D0DAD2DED8D4D0D),
    .INIT_3B(256'hAE2EAE4ECE6E2ECE8E4E0EEECEAEAEAEAEAECEEE0E4E8ECE2E6ECE4EAE2DCD4D),
    .INIT_3C(256'h6ECE2E8EEE6EEE8E0EAE4E0ECE8E4E2E0EEECECECEEEEE0E2E6EAEEE2E8EEE4E),
    .INIT_3D(256'h0F4F8FCF2F8FEF4FCF4FCF6F0FAF4F0FCF8F6E2E0E0EEEEEEE0E2E4E6E8ECE0E),
    .INIT_3E(256'hAFCF0F2F6FCF0F6FCF2FAF2FAF2FCF6F0FAF6F2FEFCFAF8F6F6F6F6F6F8FAFCF),
    .INIT_3F(256'h7090B0D0105090D0106FCF4FAF2FAF4FCF6F2FCF8F4F0FCFAF8F8F6F6F6F6F8F),
    .INIT_40(256'h8E4E0EEECEAEAEAEAEAECEEE0E4E8ECE2E6ECE4EAE2EAE4EEE8E2EEEAE6E2E0E),
    .INIT_41(256'h8D2DCD6D0DCD8D6D4D2D0D0DED0D0D2D4D6DAEEE2E8EEE4ECE2EAE4ECE6E2ECE),
    .INIT_42(256'hED4DAD2DAD2DAD4DEDAD4D0DEDAD8D6D6D6D6D6D8DADCD0D4D8DED2DAD0D8D0D),
    .INIT_43(256'h8CACCC0C4C8CED4DAD0D8D0DAD4DED8D4D0DCDAD8D6D6D4D6D6D8DADCD0D4D8D),
    .INIT_44(256'h4C0C0CECECEC0C2C4C6CACEC2C8CEC6CCC4CEC6C0CAC6C2CECCC8C8C6C6C6C6C),
    .INIT_45(256'hEC6C0CAC6C2CECCC8C8C6C6C6C6C8CACEC2C6CAC0C6CCC4CCC4CEC8C2CECAC6C),
    .INIT_46(256'h4B8BEB2B8B0B6BEB8B0BAB6B0BCBAB6B4B4B2B2B4B4B6B8CCC0C4CAC0C6CEC6C),
    .INIT_47(256'h6B4B4B4B4B4B6BABCB0B6BAB0B8BEB6BEB8B2BCB8B4B0BEBCBCBABABCBCBEB2B),
    .INIT_48(256'hEA6A0AAA4A0BCB8B6B4B2B2B2B4B4B8BABEB2B6BCB2BAB2BAB4BCB8B2BEBAB8B),
    .INIT_49(256'hAACA0A4A8ACA2AAA0A8A0AAA4AEAAA6A2A0AEAEACAEAEA0A2A6A8AEA2A8AEA6A),
    .INIT_4A(256'h8A4A0ACAAA8A8A8A8AAACAEA2A6AAA0A6AEA4AEA6A0AAA6A2AEACAAA8A8A8A8A),
    .INIT_4B(256'h4989C929A9098929C96909C989694929292A2A4A6A8ACA0A6ACA2AAA2AAA4AEA),
    .INIT_4C(256'h89694929090929294989C90949A90989098929C9892909C9A9898989A9A9C909),
    .INIT_4D(256'h49A90969E969E98929C98969290909E90909294989C90969C949A949C96929C9),
    .INIT_4E(256'h2808E8C8A8A8C8C8E82868A8E848C828A848E8884808C8A88869696989A9C909),
    .INIT_4F(256'h084888E848C848C86808C88848280808E80808286888E82888E868E88828C868),
    .INIT_50(256'h6828E8A8886848486868A8C80848A80868E88808A86808E8A88888686888A8C8),
    .INIT_51(256'h274767A7E747A70787078727C7874727E7E7C7C7E7072767A7E747A727A828C8),
    .INIT_52(256'h078707A74707C787676747476787A7E72767C747A747C76707C7876727272727),
    .INIT_53(256'h87674747476787C70747A70767E78707C7672707C7C7A7A7C7C7072767C72787),
    .INIT_54(256'h66A60646A626A626C66626E6A6866666666686C6E62686E646C646E68727E7A7),
    .INIT_55(256'h8606A64606C6A68666668686A6E62666C626A626A64606A66646260606062646),
    .INIT_56(256'h4626060606264666A60666C646C646E6A64606E6C6C6C6C6E6062666C62686E6),
    .INIT_57(256'h4585C50545A525A525C56525E5A58585658585A5E62666C6268606A646E6A666),
    .INIT_58(256'h65E565E58525E5A585454545456585C50545A5058505A545E5A5654525252545),
    .INIT_59(256'h25C5856545252525254585C50565C545C56505A5652505E5E5E5E5052565A505),
    .INIT_5A(256'h04E4E4E4042565A50565C545C56505C5854525250525256585C52585E565E585),
    .INIT_5B(256'hA4C4044484E464C46404A44404E4C4A4A4C4C4042464C42484048424C4844424),
    .INIT_5C(256'hA40464C464E4844404C4A4848484A4C4E42484E444C444E48424E4C4A4848484),
    .INIT_5D(256'h8404A444E4A4846444444464A4C40464C444C444E4844404E4C4C4C4C4E42444),
    .INIT_5E(256'hC3632303E3C3C3E3E3234484E444C444C46404C484644444446484A4E444A404),
    .INIT_5F(256'h03C3C3A3C3C3E32363A30383038323C383430303E3E303234383C32383038323),
    .INIT_60(256'hA3A3A3C3E32363C3238323A34303A383634343436383C30343A323A323C38323),
    .INIT_61(256'h436383C30363C323C343E3A3632303E3E303034363C30383E36303A34303E3C3),
    .INIT_62(256'h4383C30363E36303834303C3A38383A3A3E30343A30383038323E3A363434343),
    .INIT_63(256'h2282C242C242E2824202C2C2A2A2C2E20262A20262E28222C282422202020222),
    .INIT_64(256'h62C222A242E2824202E2C2C2C2E2224282E242C242E28222E2C2A28282A2C2E2),
    .INIT_65(256'h42C242C26222E2A28262628282C20242A2028202A242E2C28262626282A2C202),
    .INIT_66(256'h62E28202C2824222020202224282C22282028222C282422202E202022262A2E2),
    .INIT_67(256'h22A242E2826222020202224282C20282E26202A24202E2C2C2C2C2E20242A202),
    .INIT_68(256'hA141E1814121E1E1E1E1012161C12181018121C18161212101214161A1E142A2),
    .INIT_69(256'h8121C1814101E1E1E101214181E141A121C16121E1A181818181A1C10161C121),
    .INIT_6A(256'h21A16101C1A1818181A1C1E12181E161E16121C181614141414161A1E141A101),
    .INIT_6B(256'hA141E1814121010101014161A10161E161E1A14101E1C1C1C1C1012161C121A1),
    .INIT_6C(256'h8101A16121E1C1C1C1C1E12161A10181018121E1A1816141416181C10141A121),
    .INIT_6D(256'hE16101A161210101E101214181C121A121A141E1A1816141414161A1E1218101),
    .INIT_6E(256'h20A040E0806020000000204060C10161E16101A1612101E1E1E1012161A10161),
    .INIT_6F(256'hA000A020E08060402020204060A0E040C040C06000C0A080606080A0C00060C0),
    .INIT_70(256'h60E060E08020E0C0A0808080A0E02060E040C06000A080402020204060A0E020),
    .INIT_71(256'hE040A020C06000C0A080606080A0E02060E040C06000C080604040406080C020),
    .INIT_72(256'h2080E040C06000A06040202020204080C02080008020C08040200000002060A0),
    .INIT_73(256'h80C00060E060E0804000C0A0A0A0C0E02060A0208020A06000E0C0A0A0A0C0E0),
    .INIT_74(256'h204080C0208000A040E0A0806040406080C00040C020C040E0A0804040204060),
    .INIT_75(256'h00204080C02080008020E0A060402020406080E02080008020C080400000E000),
    .INIT_76(256'h8080A0C0E04080E060E08020E0A080808080A0C00060C020A040E08060200000),
    .INIT_77(256'hA0A0A0A0C0E02080E060E06000C080604040406080C00060C040E08020E0C0A0),
    .INIT_78(256'hA06060406060A0C02080E060E0804000C0A0A0A0A0E00040A0008000A04000C0),
    .INIT_79(256'h602000E0C0C0E0004080C020A020C06020E0C0C0C0C0E00040A00060E08020E0),
    .INIT_7A(256'h40E08060202020204060A0E040C040C06020E0C0A0A0A0A0E00060C020A020C0),
    .INIT_7B(256'h008020E0A0604040406080A00040C020C06000C0806040406080A0E040A020A0),
    .INIT_7C(256'hE060E06020C0A06060606080A0E040A0008020C080400000E000004060C02080),
    .INIT_7D(256'hE020A020A04000C08060606080A0E02080E060E08020E0C080808080A0E02080),
    .INIT_7E(256'hE02080E060E08020E0A08060606080C00040A020A020E0804020000000204080),
    .INIT_7F(256'h004080C0208000A040E0A08060606060A0C02060E060E08020E0A080808080C0),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized35
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h007FFFFFF00000007FFFFFFFE0000000001FFFFFFFFFFFFC0000000000000000),
    .INITP_01(256'h001FFF8000FFFE0003FFFC0001FFFF00003FFFF00000FFFFF000007FFFFF0000),
    .INITP_02(256'hC00FFC00FFC00FFE007FF001FFC007FF001FFE001FFE001FFF0007FFC001FFF8),
    .INITP_03(256'hC03FC07F807F807F807F807F803FC03FE01FF007F803FE00FF801FF007FE00FF),
    .INITP_04(256'h0FC0FE07F03F81FC0FE07F01F80FE03F80FC07F01FC03F80FE03FC07F00FE01F),
    .INITP_05(256'h1F07E0FC0F81F03F07E07C0FC1F81F81F03F03F03F03F03F03F03F03F81F81FC),
    .INITP_06(256'h7C1F0F83E0F83E0F83E0F83E07C1F07C1F03E0F83F07C1F83E07C1F83E07C0F8),
    .INITP_07(256'h0F87C3E1F0F87C1E0F0783E1F0783C1F0783E1F0783E0F07C1F0F83E0F07C1F0),
    .INITP_08(256'h1F07C0F83E0F83E0F83E0F83E1F07C1F07C1E0F83E1F07C1E0F83C1F0F83C1F0),
    .INITP_09(256'hF03F07E07C0FC1F81F03E07E0FC1F03E07C0F83F07C0F83F07C1F83E0F81F07C),
    .INITP_0A(256'h0FE03F01FC0FE07F03F81FC0FE07E07F03F03F81F81F81F81F81F81F81F81F03),
    .INITP_0B(256'h07F803FC03FC03FC03FC03FC07F807F00FE01FC07F80FE03F807F01FC07E03F8),
    .INITP_0C(256'hC007FF001FFC00FFE007FE007FE007FE00FFC01FF003FE00FF803FC01FF00FF8),
    .INITP_0D(256'hFFFF00007FFF8000FFFE0003FFF0003FFF0007FFC001FFF000FFF000FFF001FF),
    .INITP_0E(256'h00000FFFFFFFFC0000001FFFFFFC000001FFFFFC00001FFFFE00001FFFF80001),
    .INITP_0F(256'h00000000000000000000000000000000000000000000007FFFFFFFFFFFF00000),
    .INIT_00(256'h8784817E7C797673706E6B686663615E5C59575452504E4B49474543413F3C3B),
    .INIT_01(256'hF7F3EFEBE7E4E0DCD8D4D1CDC9C6C2BFBBB8B4B1ADAAA7A3A09D9A9693908D8A),
    .INIT_02(256'h89847F7A75706B66615D58534E4A45413C37332E2A26211D1914100C0804FFFB),
    .INIT_03(256'h3C36302A241E18120C0600FBF5EFEAE4DED9D3CEC8C3BDB8B3ADA8A39E98938E),
    .INIT_04(256'h110902FBF4EDE6DFD8D1CBC4BDB6AFA9A29B958E88817B746E68615B554F4842),
    .INIT_05(256'h06FEF6EEE6DED6CEC6BEB6AEA69E978F8780787069615A524B433C352D261F18),
    .INIT_06(256'h1E140B02F9F0E7DED5CCC3BAB1A89F978E857C746B635A5249413830271F170F),
    .INIT_07(256'h564C41372D23190F05FBF1E7DDD3C9BFB6ACA2988F857C72685F564C43393027),
    .INIT_08(256'hAFA4998E82776C61564B40352A1F1409FEF4E9DED4C9BEB4A99F948A7F756A60),
    .INIT_09(256'h2A1E1105F9EDE0D4C8BCB0A4988C8074695D5145392E22160BFFF4E8DDD1C6BB),
    .INIT_0A(256'hC5B8AB9D908376695B4E4134271A0D00F4E7DACDC0B4A79A8E8175685C4F4336),
    .INIT_0B(256'h82736557483A2C1E1001F3E5D7C9BBAD9F928476685A4D3F31241609FBEEE0D3),
    .INIT_0C(256'h5F4F4031211203F4E4D5C6B7A8998A7B6C5D4E4031221305F6E7D9CABCAD9F90),
    .INIT_0D(256'h5D4C3C2B1B0BFAEADACABAAA9A8A7A6A5A4A3A2A1A0AFBEBDBCCBCAC9D8D7E6E),
    .INIT_0E(256'h7B69584635241301F0DFCEBDAC9B897968574635241302F2E1D0C0AF9F8E7D6D),
    .INIT_0F(256'hB9A79482705D4B39271402F0DECCBAA8968472604E3D2B1907F6E4D2C1AF9E8C),
    .INIT_10(256'h1804F1DECAB7A4907D6A5744311E0BF8E5D2BFAC998673614E3B291603F1DECC),
    .INIT_11(256'h97826E5945311C08F4E0CCB7A38F7B67533F2B1804F0DCC8B5A18D7A66523F2B),
    .INIT_12(256'h35200AF5E0CAB5A08B75604B36210CF7E2CDB8A38E7A65503B2712FDE9D4C0AB),
    .INIT_13(256'hF4DDC7B09A846E57412B15FFE9D2BCA6907B654F39230DF8E2CCB7A18B76604B),
    .INIT_14(256'hD1BAA38B745D452E1700E9D2BBA48D765F48311A03ECD6BFA8917B644E37210A),
    .INIT_15(256'hCFB69E866D553D250CF4DCC4AC947C644C341C05EDD5BDA68E765F472F1800E9),
    .INIT_16(256'hEBD2B89F866D533A2108EFD6BDA48B725940270EF6DDC4AB937A6249311800E7),
    .INIT_17(256'h260CF2D7BDA3896F553A2006ECD2B99F856B51371E04EAD1B79D846A51371E04),
    .INIT_18(256'h80654A2F13F8DDC2A78C71563B2005EACFB59A7F644A2F14FADFC5AA90755B41),
    .INIT_19(256'hF9DDC0A4886C503418FCE0C4A88C7055391D01E6CAAE93775C402509EED2B79C),
    .INIT_1A(256'h907255381BFEE1C4A78A6D503317FADDC0A4876A4E3115F8DCBFA3866A4E3115),
    .INIT_1B(256'h442608EACCAE9072543719FBDDBFA28466492B0DF0D2B5977A5D3F2205E7CAAD),
    .INIT_1C(256'h17F8D9BA9B7C5D3E2001E2C3A58667492A0BEDCEB09173553618FADBBD9F8163),
    .INIT_1D(256'h07E7C7A78868482808E9C9A98A6A4A2B0BECCCAD8E6E4F3010F1D2B393745536),
    .INIT_1E(256'h15F4D3B29171502F0FEECDAD8C6C4B2B0AEAC9A98969482808E8C7A787674727),
    .INIT_1F(256'h3F1DFCDAB89775533210EFCDAC8A69482605E4C2A1805F3E1DFBDAB998775636),
    .INIT_20(256'h8664411FFCD9B794724F2D0BE8C6A4815F3D1BF9D7B492704E2C0AE8C6A58361),
    .INIT_21(256'hEAC7A3805C3915F2CFAB8865411EFBD8B5926F4B2805E3C09D7A573411EFCCA9),
    .INIT_22(256'h6A4621FDD9B4906C4723FFDBB7936F4B2703DFBB97734F2B07E4C09C7955310E),
    .INIT_23(256'h06E1BB96714C2701DCB7926D4823FED9B4906B4621FCD8B38E6A4521FCD7B38E),
    .INIT_24(256'hBD97714B25FFD9B38D67411BF5CFAA845E3813EDC7A27C57310CE6C19B76502B),
    .INIT_25(256'h9069421BF4CDA67F59320BE4BE97704A23FDD6B089633C16EFC9A37C56300AE3),
    .INIT_26(256'h7E562E06DEB78F674018F0C9A17A522B03DCB58D663F17F0C9A27A532C05DEB7),
    .INIT_27(256'h865D340CE3BB926A4119F0C8A0774F27FED6AE865E350DE5BD956D451DF5CDA5),
    .INIT_28(256'hA87F552C03D9B0875D340BE2B88F663D14EBC29970471EF5CCA37A512900D7AE),
    .INIT_29(256'hE5BB90663C12E8BD93693F15EBC1976D431AF0C69C72491FF5CBA2784F25FBD2),
    .INIT_2A(256'h3B10E5BA8F64390EE3B88D63380DE2B88D62370DE2B88D63380EE3B98E643A0F),
    .INIT_2B(256'hAB7F5327FBCFA4784C21F5C99E72471BF0C4996D4217EBC095693E13E8BC9166),
    .INIT_2C(256'h3306DAAD815427FBCFA276491DF1C4986C3F13E7BB8F63360ADEB2865A2E02D6),
    .INIT_2D(256'hD4A7794C1EF1C497693C0FE2B5885A2D00D3A6794C20F3C6996C3F12E6B98C60),
    .INIT_2E(256'h8D5F3103D5A7794A1DEFC193653709DBAD805224F7C99B6E4012E5B78A5C2F01),
    .INIT_2F(256'h5E2F00D1A3744516E8B98A5C2DFED0A1734416E7B98A5C2DFFD1A2744618E9BB),
    .INIT_30(256'h4617E7B8885929FACA9B6B3C0DDDAE7F4F20F1C292633405D6A778491AEBBC8D),
    .INIT_31(256'h4616E5B5855424F4C494643303D3A3734313E3B3845424F4C494653505D5A676),
    .INIT_32(256'h5C2BFAC998673605D4A3734211E0B07F4E1DEDBC8C5B2AFAC999683807D7A776),
    .INIT_33(256'h885725F3C2905E2DFBCA98673504D2A16F3E0DDBAA794716E5B4835120EFBE8D),
    .INIT_34(256'hCB98663301CF9C6A3806D4A16F3D0BD9A7754311DFAD7B4917E5B3814F1EECBA),
    .INIT_35(256'h22EFBC895623F0BD8A5725F2BF8C5926F4C18E5C29F6C4915E2CF9C794622FFD),
    .INIT_36(256'h8F5B28F4C08D5925F2BE8B5724F0BD895623EFBC895522EFBB885522EFBC8855),
    .INIT_37(256'h10DCA8733F0BD6A26E3A06D19D693501CD996531FDC995612DF9C6925E2AF6C3),
    .INIT_38(256'hA6713C07D29D6833FECA95602BF7C28D5824EFBB86511DE8B47F4B16E2AD7945),
    .INIT_39(256'h4F19E4AE79430ED8A36D3803CD98622DF8C38D5823EEB8834E19E4AF7A4510DB),
    .INIT_3A(256'h0BD59F6933FDC7915B25EEB9834D17E1AB753F09D39E6832FCC7915B25F0BA85),
    .INIT_3B(256'hDBA46D3700C9935C25EFB8814B14DEA7713A04CD97612AF4BE87511BE4AE7842),
    .INIT_3C(256'hBD854E17DFA8713A03CB945D26EFB8814A12DBA46D36FFC9925B24EDB67F4812),
    .INIT_3D(256'hB1794109D199612AF2BA824B13DBA46C34FDC58E561FE7AF784109D29A632BF4),
    .INIT_3E(256'hB67D450DD49C642BF3BB824A12DAA16931F9C1895018E0A8703800C8905820E8),
    .INIT_3F(256'hCC935A21E9B0773E05CC935B22E9B0783F06CE955C24EBB27A4109D0985F27EE),
    .INIT_40(256'h2A6197CD043A71A7DE144B81B8EF255C93C900376DA4DB12487FB6ED245B92C9),
    .INIT_41(256'h689ED3093F75ABE1174D83B9EE255B91C7FD33699FD50B4278AEE41B5187BEF4),
    .INIT_42(256'hB8EE23588DC3F82D6298CD03386DA3D80E4379AEE4194F85BAF0255B91C7FC32),
    .INIT_43(256'h1D5186BBEF24588DC2F72B6095CAFE33689DD2073C71A6DB10457AAFE4194E83),
    .INIT_44(256'h95C9FD316599CD0135699DD1063A6EA2D60B3F73A8DC104579ADE2164B7FB4E8),
    .INIT_45(256'h225589BCEF235689BDF024578BBEF225598DC0F4285B8FC3F62A5E92C6F92D61),
    .INIT_46(256'hC4F6295C8EC1F426598CBFF225578ABDF0235689BCEF225588BCEF225588BBEF),
    .INIT_47(256'h7BADDF114375A7D90B3D6FA1D406386A9CCF01336698CBFD2F6294C7F92C5E91),
    .INIT_48(256'h4779AADB0D3E6FA1D204356798CAFB2D5E90C2F3255788BAEC1E4F81B3E51749),
    .INIT_49(256'h2A5B8CBCED1D4E7FB0E0114273A3D405366798C9FA2B5C8DBEEF205183B4E516),
    .INIT_4A(256'h245484B3E3134373A3D303336494C4F4245485B5E5164676A7D707386899C9FA),
    .INIT_4B(256'h346392C2F1204F7FAEDD0D3C6B9BCAFA295988B8E7174676A6D505356594C4F4),
    .INIT_4C(256'h5C8AB9E7164473A1D0FE2D5C8AB9E8164574A3D1002F5E8DBCEB1A4978A7D605),
    .INIT_4D(256'h9BC9F7245280ADDB09376593C1EF1D4A79A7D503315F8DBBE9184674A2D1FF2D),
    .INIT_4E(256'hF3204C79A6D3002D5A88B5E20F3C6997C4F11E4C79A7D4012F5C8AB7E512406E),
    .INIT_4F(256'h638FBBE7133F6C98C4F11D4976A2CFFB275481ADDA0633608CB9E6123F6C99C6),
    .INIT_50(256'hEB17426D99C4F01B47729EC9F5214C78A4CFFB27537FABD6022E5A86B2DE0A36),
    .INIT_51(256'h8DB8E20D37628DB8E20D38638DB8E30E39648FBAE5103B6691BCE8133E6995C0),
    .INIT_52(256'h49729CC6F01A436D97C1EB153F6993BDE8123C6690BBE50F3A648EB9E30E3863),
    .INIT_53(256'h1E477099C2EB143D668FB8E20B345D87B0D9032C557FA8D2FB254F78A2CBF51F),
    .INIT_54(256'h0D355E86AED6FE274F77A0C8F019416A92BBE30C345D86AED70029517AA3CCF5),
    .INIT_55(256'h173F668DB5DC032B527AA1C9F01840678FB7DE062E567EA5CDF51D456D95BDE5),
    .INIT_56(256'h3C6389B0D6FD234A7097BEE40B32597FA6CDF41B426990B7DE052C537AA2C9F0),
    .INIT_57(256'h7CA2C7ED13385E84AACFF51B41678DB3D9FF254B7197BDE30A30567CA3C9EF16),
    .INIT_58(256'hD8FC21466B90B4D9FE23486D92B7DC01274C7196BBE1062B50769BC1E60C3157),
    .INIT_59(256'h4F7397BBDF03274B6F93B7DBFF23476C90B4D9FD21466A8EB3D7FC21456A8EB3),
    .INIT_5A(256'hE305284B6F92B5D8FB1E416588ABCFF215395C80A3C7EA0E3155799CC0E4072B),
    .INIT_5B(256'h92B4D7F91B3D5F81A4C6E80B2D4F7294B7D9FC1F416486A9CCEF1134577A9DC0),
    .INIT_5C(256'h5F80A1C2E4052648698AACCDEF1032537597B8DAFC1D3F6183A5C6E80A2C4E70),
    .INIT_5D(256'h486989A9C9EA0A2B4B6C8CADCDEE0F2F507191B2D3F41536567798B9DAFB1D3E),
    .INIT_5E(256'h4F6E8EADCCEC0B2B4A6A8AA9C9E90828486888A7C7E70727476787A7C7E80828),
    .INIT_5F(256'h7391B0CEED0B2A496786A5C3E201203E5D7C9BBAD9F81736557493B3D2F11030),
    .INIT_60(256'hB5D2F00D2B496684A2BFDDFB1937547290AECCEA08264463819FBDDBFA183655),
    .INIT_61(256'h15314E6A87A4C0DDFA1733506D8AA7C4E1FE1B38557290ADCAE705223F5D7A97),
    .INIT_62(256'h93AECAE6011D3955708CA8C4E0FC1834506C88A4C0DDF915314E6A86A3BFDCF8),
    .INIT_63(256'h2F4A647F9AB5CFEA05203B56718CA7C2DDF8132F4A65809CB7D2EE0925405C77),
    .INIT_64(256'hEA041E37516B859FB9D2EC06203A556F89A3BDD7F20C26415B7590AAC5DFFA14),
    .INIT_65(256'hC4DDF60E274059728BA4BDD6EF08213A536D869FB8D2EB041E37516A849DB7D1),
    .INIT_66(256'hBDD5ED051C344C647C94ACC4DCF40C253D556D869EB6CFE700183149627A93AB),
    .INIT_67(256'hD6EC031A31485F768DA4BBD2E900172E455D748BA3BAD1E900182F475F768EA6),
    .INIT_68(256'h0D23394F657B90A6BCD2E9FF152B41576E849AB0C7DDF40A21374E647B91A8BF),
    .INIT_69(256'h657A8EA3B8CDE2F70C21364B60758BA0B5CAE0F50A20354B60768BA1B7CCE2F8),
    .INIT_6A(256'hDCF004182B3F53677B8FA3B7CCE0F4081C3145596E8297ABC0D4E9FD12273B50),
    .INIT_6B(256'h738699ACBFD2E5F80B1E3144576A7D90A4B7CADEF104182B3F52667A8DA1B5C8),
    .INIT_6C(256'h2B3D4E60728496A8BACCDEF0021427394B5D708294A7B9CCDEF10316293B4E61),
    .INIT_6D(256'h0213243546576879899BACBDCEDFF0011324354658697B8C9EAFC1D2E4F60719),
    .INIT_6E(256'hFB0A1A2A3A4A5A6A7A8A9AAABACADAEAFA0B1B2B3C4C5D6D7D8E9FAFC0D0E1F2),
    .INIT_6F(256'h132231404E5D6C7B8A99A8B7C6D5E4F403122131404F5F6E7E8D9DACBCCCDBEB),
    .INIT_70(256'h4D5A687684929FADBBC9D7E5F301101E2C3A4857657382909FADBCCAD9E7F605),
    .INIT_71(256'hA7B4C0CDDAE7F4000D1A2734414E5B697683909DABB8C5D3E0EEFB091624313F),
    .INIT_72(256'h222E3945515D6974808C98A4B0BCC8D4E0EDF905111E2A36434F5C6875818E9A),
    .INIT_73(256'hBEC9D4DEE9F4FE09141F2A35404B56616C77828E99A4AFBBC6D1DDE8F4FF0B16),
    .INIT_74(256'h7C858F98A2ACB6BFC9D3DDE7F1FB050F19232D37414C56606A757F8A949FA9B4),
    .INIT_75(256'h5A636B747C858E979FA8B1BAC3CCD5DEE7F0F9020B141E273039434C565F6872),
    .INIT_76(256'h5A6169707880878F979EA6AEB6BEC6CED6DEE6EEF6FE060F171F273038414952),
    .INIT_77(256'h7B81888E959BA2A9AFB6BDC4CBD1D8DFE6EDF4FB020911181F262D353C434B52),
    .INIT_78(256'hBDC3C8CED3D9DEE4EAEFF5FB00060C12181E242A30363C42484F555B61686E74),
    .INIT_79(256'h21262A2E33373C41454A4E53585D61666B70757A7F84898E93989EA3A8ADB3B8),
    .INIT_7A(256'hA7AAADB1B4B8BBBFC2C6C9CDD1D4D8DCE0E4E7EBEFF3F7FBFF04080C1014191D),
    .INIT_7B(256'h4E50525457595C5E616366686B6E707376797C7E8184878A8D9093969A9DA0A3),
    .INIT_7C(256'h1617181A1B1C1E1F2122242527282A2C2E2F31333537393B3C3F41434547494B),
    .INIT_7D(256'h00000001010102020203030404050506070708090A0B0B0C0D0E0F1011121415),
    .INIT_7E(256'h0B0B0A0908070706050504040303020202010101000000000000000000000000),
    .INIT_7F(256'h39373533312F2E2C2A2827252422211F1E1C1B1A18171615141211100F0E0D0C),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized36
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [7:0]douta;
  output [7:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_143 ;
  wire \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_147 ;
  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [7:0]douta;
  wire [7:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0101010101010101010101000000000000000000000000000000000000000000),
    .INIT_04(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_05(256'h0201010101010101010101010101010101010101010101010101010101010101),
    .INIT_06(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_07(256'h0303030303030303030202020202020202020202020202020202020202020202),
    .INIT_08(256'h0303030303030303030303030303030303030303030303030303030303030303),
    .INIT_09(256'h0404040404040404040404040404040404040404040404040403030303030303),
    .INIT_0A(256'h0505050505050505050505050505050504040404040404040404040404040404),
    .INIT_0B(256'h0606060606060606060605050505050505050505050505050505050505050505),
    .INIT_0C(256'h0707070707070706060606060606060606060606060606060606060606060606),
    .INIT_0D(256'h0808080808080707070707070707070707070707070707070707070707070707),
    .INIT_0E(256'h0909090909090909080808080808080808080808080808080808080808080808),
    .INIT_0F(256'h0A0A0A0A0A0A0A0A0A0A0A090909090909090909090909090909090909090909),
    .INIT_10(256'h0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A),
    .INIT_11(256'h0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0B0B0B0B0B0B0B0B0B0B0B),
    .INIT_12(256'h0E0E0E0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0C0C0C0C0C),
    .INIT_13(256'h0F0F0F0F0F0F0F0F0F0F0F0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E),
    .INIT_14(256'h1010101010101010101010101010101010101010100F0F0F0F0F0F0F0F0F0F0F),
    .INIT_15(256'h1212121212121212121111111111111111111111111111111111111111111110),
    .INIT_16(256'h1313131313131313131313131313131313131313121212121212121212121212),
    .INIT_17(256'h1515151515151515151515151414141414141414141414141414141414141414),
    .INIT_18(256'h1717171717161616161616161616161616161616161616161515151515151515),
    .INIT_19(256'h1818181818181818181818181818181818181817171717171717171717171717),
    .INIT_1A(256'h1A1A1A1A1A1A1A1A1A1A1A1A1A1A191919191919191919191919191919191919),
    .INIT_1B(256'h1C1C1C1C1C1C1C1C1C1C1C1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1A1A1A),
    .INIT_1C(256'h1E1E1E1E1E1E1E1E1E1E1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1C1C1C1C1C1C),
    .INIT_1D(256'h2020202020202020201F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1E1E1E1E1E1E1E),
    .INIT_1E(256'h2222222222222222222121212121212121212121212121212120202020202020),
    .INIT_1F(256'h2424242424242424242423232323232323232323232323232322222222222222),
    .INIT_20(256'h2626262626262626262626262525252525252525252525252525252424242424),
    .INIT_21(256'h2828282828282828282828282828272727272727272727272727272727262626),
    .INIT_22(256'h2B2B2B2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2929292929292929292929292929),
    .INIT_23(256'h2D2D2D2D2D2D2D2D2C2C2C2C2C2C2C2C2C2C2C2C2C2B2B2B2B2B2B2B2B2B2B2B),
    .INIT_24(256'h2F2F2F2F2F2F2F2F2F2F2F2F2E2E2E2E2E2E2E2E2E2E2E2E2E2E2D2D2D2D2D2D),
    .INIT_25(256'h323232323131313131313131313131313130303030303030303030303030302F),
    .INIT_26(256'h3434343434343434343433333333333333333333333333323232323232323232),
    .INIT_27(256'h3737373736363636363636363636363635353535353535353535353535343434),
    .INIT_28(256'h3939393939393939393939383838383838383838383838373737373737373737),
    .INIT_29(256'h3C3C3C3C3C3C3B3B3B3B3B3B3B3B3B3B3B3B3A3A3A3A3A3A3A3A3A3A3A3A3939),
    .INIT_2A(256'h3F3F3E3E3E3E3E3E3E3E3E3E3E3E3D3D3D3D3D3D3D3D3D3D3D3D3C3C3C3C3C3C),
    .INIT_2B(256'h414141414141414141414040404040404040404040403F3F3F3F3F3F3F3F3F3F),
    .INIT_2C(256'h4444444444444443434343434343434343434342424242424242424242424241),
    .INIT_2D(256'h4747474747464646464646464646464646454545454545454545454544444444),
    .INIT_2E(256'h4A4A4A4A49494949494949494949494848484848484848484848474747474747),
    .INIT_2F(256'h4D4D4D4C4C4C4C4C4C4C4C4C4C4B4B4B4B4B4B4B4B4B4B4B4A4A4A4A4A4A4A4A),
    .INIT_30(256'h50504F4F4F4F4F4F4F4F4F4F4F4E4E4E4E4E4E4E4E4E4E4E4D4D4D4D4D4D4D4D),
    .INIT_31(256'h5353525252525252525252525251515151515151515151505050505050505050),
    .INIT_32(256'h5656555555555555555555555554545454545454545454535353535353535353),
    .INIT_33(256'h5959595858585858585858585858575757575757575757575656565656565656),
    .INIT_34(256'h5C5C5C5C5C5B5B5B5B5B5B5B5B5B5B5A5A5A5A5A5A5A5A5A5A59595959595959),
    .INIT_35(256'h5F5F5F5F5F5F5E5E5E5E5E5E5E5E5E5E5D5D5D5D5D5D5D5D5D5D5C5C5C5C5C5C),
    .INIT_36(256'h626262626262626261616161616161616161606060606060606060605F5F5F5F),
    .INIT_37(256'h6665656565656565656565646464646464646464636363636363636363636262),
    .INIT_38(256'h6969696968686868686868686867676767676767676767666666666666666666),
    .INIT_39(256'h6C6C6C6C6C6C6C6B6B6B6B6B6B6B6B6B6A6A6A6A6A6A6A6A6A6A696969696969),
    .INIT_3A(256'h706F6F6F6F6F6F6F6F6F6E6E6E6E6E6E6E6E6E6E6D6D6D6D6D6D6D6D6D6C6C6C),
    .INIT_3B(256'h7373737373727272727272727272717171717171717171707070707070707070),
    .INIT_3C(256'h7676767676767676767575757575757575757474747474747474747373737373),
    .INIT_3D(256'h7A7A7A7A79797979797979797978787878787878787877777777777777777776),
    .INIT_3E(256'h7D7D7D7D7D7D7D7D7C7C7C7C7C7C7C7C7C7B7B7B7B7B7B7B7B7B7B7A7A7A7A7A),
    .INIT_3F(256'h818181818080808080808080807F7F7F7F7F7F7F7F7F7E7E7E7E7E7E7E7E7E7D),
    .INIT_40(256'h7171717171717171717272727272727272727373737373737373737374747474),
    .INIT_41(256'h6D6D6D6E6E6E6E6E6E6E6E6E6E6F6F6F6F6F6F6F6F6F70707070707070707070),
    .INIT_42(256'h6A6A6A6A6A6A6A6B6B6B6B6B6B6B6B6B6C6C6C6C6C6C6C6C6C6C6D6D6D6D6D6D),
    .INIT_43(256'h67676767676767676767686868686868686868696969696969696969696A6A6A),
    .INIT_44(256'h6363636464646464646464646565656565656565656566666666666666666666),
    .INIT_45(256'h6060606060616161616161616161616262626262626262626263636363636363),
    .INIT_46(256'h5D5D5D5D5D5D5D5E5E5E5E5E5E5E5E5E5E5F5F5F5F5F5F5F5F5F5F6060606060),
    .INIT_47(256'h5A5A5A5A5A5A5A5A5B5B5B5B5B5B5B5B5B5B5C5C5C5C5C5C5C5C5C5C5C5D5D5D),
    .INIT_48(256'h5757575757575757575858585858585858585858595959595959595959595A5A),
    .INIT_49(256'h5454545454545454545455555555555555555555555656565656565656565657),
    .INIT_4A(256'h5151515151515151515152525252525252525252525353535353535353535353),
    .INIT_4B(256'h4E4E4E4E4E4E4E4E4E4E4F4F4F4F4F4F4F4F4F4F4F5050505050505050505050),
    .INIT_4C(256'h4B4B4B4B4B4B4B4B4B4B4C4C4C4C4C4C4C4C4C4C4D4D4D4D4D4D4D4D4D4D4D4E),
    .INIT_4D(256'h484848484848484849494949494949494949494A4A4A4A4A4A4A4A4A4A4A4A4B),
    .INIT_4E(256'h4545454545454646464646464646464646464747474747474747474747484848),
    .INIT_4F(256'h4242424243434343434343434343434344444444444444444444444545454545),
    .INIT_50(256'h3F40404040404040404040404041414141414141414141414242424242424242),
    .INIT_51(256'h3D3D3D3D3D3D3D3D3D3E3E3E3E3E3E3E3E3E3E3E3E3F3F3F3F3F3F3F3F3F3F3F),
    .INIT_52(256'h3A3A3A3A3A3B3B3B3B3B3B3B3B3B3B3B3B3C3C3C3C3C3C3C3C3C3C3C3C3D3D3D),
    .INIT_53(256'h383838383838383838383838393939393939393939393939393A3A3A3A3A3A3A),
    .INIT_54(256'h3535353535353536363636363636363636363637373737373737373737373737),
    .INIT_55(256'h3333333333333333333333333334343434343434343434343434353535353535),
    .INIT_56(256'h3030303030303131313131313131313131313132323232323232323232323232),
    .INIT_57(256'h2E2E2E2E2E2E2E2E2E2E2E2F2F2F2F2F2F2F2F2F2F2F2F2F3030303030303030),
    .INIT_58(256'h2B2B2C2C2C2C2C2C2C2C2C2C2C2C2C2D2D2D2D2D2D2D2D2D2D2D2D2D2D2E2E2E),
    .INIT_59(256'h29292929292A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2B2B2B2B2B2B2B2B2B2B2B2B),
    .INIT_5A(256'h2727272727272727272828282828282828282828282828292929292929292929),
    .INIT_5B(256'h2525252525252525252525262626262626262626262626262626272727272727),
    .INIT_5C(256'h2323232323232323232323232324242424242424242424242424242425252525),
    .INIT_5D(256'h2121212121212121212121212121222222222222222222222222222222222323),
    .INIT_5E(256'h1F1F1F1F1F1F1F1F1F1F1F1F1F1F202020202020202020202020202020202121),
    .INIT_5F(256'h1D1D1D1D1D1D1D1D1D1D1D1D1D1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1F1F),
    .INIT_60(256'h1B1B1B1B1B1B1B1B1B1B1B1B1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D),
    .INIT_61(256'h1919191919191919191A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1B1B1B1B1B1B),
    .INIT_62(256'h1717171718181818181818181818181818181818181818191919191919191919),
    .INIT_63(256'h1616161616161616161616161616161616161717171717171717171717171717),
    .INIT_64(256'h1414141414141414141414151515151515151515151515151515151515151516),
    .INIT_65(256'h1212121313131313131313131313131313131313131313141414141414141414),
    .INIT_66(256'h1111111111111111111111111111121212121212121212121212121212121212),
    .INIT_67(256'h0F0F101010101010101010101010101010101010101010101111111111111111),
    .INIT_68(256'h0E0E0E0E0E0E0E0E0E0E0E0E0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F),
    .INIT_69(256'h0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0E0E0E0E0E0E0E0E0E0E0E0E),
    .INIT_6A(256'h0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0D0D0D0D),
    .INIT_6B(256'h0A0A0A0A0A0A0A0A0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B),
    .INIT_6C(256'h0909090909090909090909090A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A),
    .INIT_6D(256'h0808080808080808080808080808080909090909090909090909090909090909),
    .INIT_6E(256'h0707070707070707070707070707070707080808080808080808080808080808),
    .INIT_6F(256'h0606060606060606060606060606060607070707070707070707070707070707),
    .INIT_70(256'h0505050505050505050505050506060606060606060606060606060606060606),
    .INIT_71(256'h0404040404040405050505050505050505050505050505050505050505050505),
    .INIT_72(256'h0404040404040404040404040404040404040404040404040404040404040404),
    .INIT_73(256'h0303030303030303030303030303030303030303030303030303030303030404),
    .INIT_74(256'h0202020202020202020202020202030303030303030303030303030303030303),
    .INIT_75(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_76(256'h0101010101010101010101010101010101010101010102020202020202020202),
    .INIT_77(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_78(256'h0000000000000000000000000101010101010101010101010101010101010101),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],\DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_143 }),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],\DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_n_147 }),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized4
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h00FFFFF803E0F1E318CCCCD9249694AD556AD552A52DA49B2666739C78F07F00),
    .INITP_01(256'h81FE000007FC1F0E1CE3319B364925AD6AD5555556AD696DB6CD999CE71C3C3F),
    .INITP_02(256'hE01FFFFFFF80F87871CE7333264924B4A54AAB54AAA56B4B49364CCCCE71C787),
    .INITP_03(256'h03FF0007E07870E318CCCCD936D25A56AD5555555AB52D2DB64D9999CE71C3C3),
    .INITP_04(256'hE0F0F1C6398CCCD9B6496D294AD55AAAAD54A94A4B6DB66CCCCE638E387C1FC0),
    .INITP_05(256'h936DB69296A56AAD55556AA54A52D24B24D9933198C71C78783F007FFFFFF007),
    .INITP_06(256'h52D696DB6DB266CCCCE631CE3C78781F80FFFE00FFFE01F83C3C71C7398CCCC9),
    .INITP_07(256'h0000003FF01F83C1E3C71C739CC666664C9B24924B4B4A56AD54AAAAAAA955AB),
    .INITP_08(256'hD695AB552AAAAAAA556AD4A5A5A49249B264CCCCC6739C71C78F0783F01FF800),
    .INITP_09(256'h6D9326666339C71C78783F00FFFE00FFFE03F03C3C78E718CE6666CC9B6DB6D2),
    .INITP_0A(256'h1E0FC01FFFFFFC01F83C3C71C63319933649A49694A54AAD55556AAD4AD292DB),
    .INITP_0B(256'hFF8007F07C38E38CE6666CDB6DA4A52A556AAAB556A5296D24DB36666338C71E),
    .INITP_0C(256'hF00F87871CE7333364DB69695AB55555556AD4B496D9366666318E1C3C0FC001),
    .INITP_0D(256'hFF03C3C71CE66664D925A5AD4AAA55AAA54A5A4924C9999CE71C3C3E03FFFFFF),
    .INITP_0E(256'hFE01F87871CE733366DB6D2D6AD5555556AD6B4924D9B3198E70E1F07FC00000),
    .INITP_0F(256'h000001FC1E3C739CCCC9B24B694A9556AD556A52D249366666318F1E0F803FFF),
    .INIT_00(256'h3CEC9C5004BC7430F0B07038FCC4905C2CFCD0A47C54300CECCCB09880685444),
    .INIT_01(256'hA41080F064D84CC840C03CC040C850D864F08014A83CD4700CAC4CEC9438E48C),
    .INIT_02(256'h709CC8F4245488BCF4306CA8E82C70B8004C98E43888E03490EC48A8086CD43C),
    .INIT_03(256'hA0846C58443424180C00FCF4F4F4F4F8FC040C182838485C748CA8C4E404244C),
    .INIT_04(256'h2CCC741CC4701CCC8034E8A05C18D898581CE4AC784414E4B89068401CF8D8BC),
    .INIT_05(256'h1474D438A00874E050C034A8209C1494149418A028B03CCC5CF08418B44CEC88),
    .INIT_06(256'h547090B4D8FC24507CA8D80C4078B0EC2868A8EC3078C00C5CACFC50A4FC58B4),
    .INIT_07(256'hE8C4A08060442C10FCE8D4C4B8ACA0989490909090949CA4B0BCCCDCF0081C38),
    .INIT_08(256'hD06804A03CE08028D07824D08030E49C500CC8844408CC945C24F0C090643810),
    .INIT_09(256'h0058B00C68C42888F054BC28980478E860D850CC48C84CD054DC68F48010A438),
    .INIT_0A(256'h7C90A4BCD8F41434587CA0CCF4245084B4EC245C98D4145498E02870BC0C5CAC),
    .INIT_0B(256'h3408DCB48C64402000E4C8B098847060504438302824242424282C34404C5868),
    .INIT_0C(256'h2CBC4CE47810AC48E8882CD07820CC7828DC8C44FCB4702CECB0743800CC9864),
    .INIT_0D(256'h54A0F4449CF44CA80464C42890F460CC38A81C8C047CF470F070F478FC84109C),
    .INIT_0E(256'hA8B4C4D8EC001834506C8CB0D4F8204C78A8D8083C74ACE82464A4E82C70BC04),
    .INIT_0F(256'h20ECBC8C60340CE4C09C7C5C40240CF4E0CCBCACA0988C8884808080848C909C),
    .INIT_10(256'hB440D060F0841CB44CE88828CC7014BC6814C47424D8904804C07C4000C48C54),
    .INIT_11(256'h5CA8F44494E84098F04CA8086CD0349C0470E050C034AC249C189818981CA42C),
    .INIT_12(256'h0C14243444586C84A0B8D8F8183C6088B4DC0C3C6CA0D40C4880C0004084C810),
    .INIT_13(256'hB8845020F4C89C744C2808E8C8AC9078644C3C2C1C1004FCF8F4F0F0F0F4F800),
    .INIT_14(256'h5CE87404982CC058F49030D07014BC640CB86414C87C30E8A4601CDC9C6028F0),
    .INIT_15(256'hE83484D4287CD42C88E440A4046CD038A41080F064D84CC440BC3CBC3CC448D0),
    .INIT_16(256'h505C6C8094A8C0DCF81838587CA0C8F4204C7CB0E0185088C4004080C40C509C),
    .INIT_17(256'h885828FCD4AC8460401C00E4C8B0988470605044383028242020202428303840),
    .INIT_18(256'h8418AC40D87410AC4CEC9038DC8834E09040F4A86018D0904C0CD0945C24ECB8),
    .INIT_19(256'h3890E43C98F450B01478DC44B01C88F868DC50C844BC38B838BC40C850DC68F4),
    .INIT_1A(256'h98B0C8E40020406488B0D804305C8CC0F42C609CD8145498D82068B0FC4898E8),
    .INIT_1B(256'h906C482808E8CCB49C84706050403428201C141410101418202830404C5C6C80),
    .INIT_1C(256'h14B454F89C40E89440EC9C5000B86C28E0A05C1CE0A46C34FCC89864380CE0B8),
    .INIT_1D(256'h1878E044B01884F464D448C038B02CA828A82CB034C048D464F08418AC44DC78),
    .INIT_1E(256'h84ACD804306090C4F83068A4E01C5CA0E02870B80450A0F04094EC449CF858B4),
    .INIT_1F(256'h544030201408FCF4F0ECE8E8E8ECF0F8000C182838485C7088A4BCDCF8183C60),
    .INIT_20(256'h6C20D48844FCB87838F8BC804810DCA8784818ECC098744C2808E8CCB0947C68),
    .INIT_21(256'hC038B430B030B438BC44D05CE878089C30C45CF89430D07014B86008B05C0CB8),
    .INIT_22(256'h3C7CC0044890D82470C01064B80C64C01878D43498FC60C834A00C78EC5CD048),
    .INIT_23(256'hD0D8E4F0FC0C1C304458708CA4C4E00424487098C0EC184878ACE0144C84C0FC),
    .INIT_24(256'h6C3C0CE0B48C64401CFCDCBCA0846C54402C1808F8ECE0D8D0C8C4C4C4C4C8CC),
    .INIT_25(256'hF89028C86404A448EC943CE89440F0A05408C07830ECA86828E8B0743C04D09C),
    .INIT_26(256'h60C4288CF45CC834A01084F86CE45CD450D04CD050D45CE46CF88414A434C860),
    .INIT_27(256'h94C0F01C5084B8EC24609CD8185898DC2068B4FC4898E8388CE0348CE844A000),
    .INIT_28(256'h80746C686460606064686C74808894A4B4C4D8EC041C3450708CACD0F4184068),
    .INIT_29(256'h08CC90541CE4B07C4818E8BC90643C18F0CCAC8C6C50341C04ECD8C4B4A49488),
    .INIT_2A(256'h20B040D064F88C24C058F89434D87820C46C18C0701CCC8034E8A05810CC8848),
    .INIT_2B(256'hAC0864C42484E44CB01880EC58C834A8188C047CF470EC68E86CEC70F8800894),
    .INIT_2C(256'h9CC4EC184474A4D4083C74ACE4205C98D81C5CA4E83078C41060B00054A8FC54),
    .INIT_2D(256'hD1C9C1B9B5B1ADADB1B1B5BDC1CDD5E1F1FD0D213549617995ADCDE9092D5074),
    .INIT_2E(256'h3D01C58D5925F1BD8D5D3105DDB18D654121FDE1C1A5897159412D1909F9E9DD),
    .INIT_2F(256'hC155E98119B551ED8D2DD17519C16911BD6915C57929DD954D05BD7939F5B579),
    .INIT_30(256'h49AD1179E54DB929950979ED61D951C945C141BD41C145CD51D965F17D0D9D2D),
    .INIT_31(256'hB9F1296199D5155191D11559A1E53179C51161B10155A90159B10965C52181E5),
    .INIT_32(256'h0109111D2935455565798DA1B9D1ED0925456585A9CDF119416D99C5F1215585),
    .INIT_33(256'h01D9B59171513115F9DDC5AD95816D5D493D2D21150D05FDF9F5F1F1F1F1F5F9),
    .INIT_34(256'hA14DFDAD5D11C57D31EDA5611DDD995D1DE1A56D35FDC995613101D5A5795129),
    .INIT_35(256'hC949CD51D55DE571F98915A535C559ED851DB551E98925C56509AD51F9A149F5),
    .INIT_36(256'h5DB10965BD1979D53599F95DC12991F965D13DAD1D910175ED61D955CD49C949),
    .INIT_37(256'h416D9DCDFD2D6195C9013971ADE92965A5E9296DB5F9418DD52171BD0D61B105),
    .INIT_38(256'h6161696D757D85919DA9B9C9D9ED01152D455D7591ADCDED0D3151799DC5ED15),
    .INIT_39(256'h9975512D0DEDCDB191795D452D1501EDDDC9B9AD9D91857D756D6965615D5D5D),
    .INIT_3A(256'hD5893DF1A96119D5914D0DC98D4D11D5996129F1BD895521F1C195693D11E9C1),
    .INIT_3B(256'hF98511A131C155E57D11A941D97511AD4DED8D2DD1751DC16915BD6915C57525),
    .INIT_3C(256'hE149B11985ED59C935A51989FD71E55DD54DC945C141BD41C145C94DD159E16D),
    .INIT_3D(256'h79BDFD4185CD155DA5F13D89D52575C9196DC51971C9217DD93595F555B5197D),
    .INIT_3E(256'hA1C1DDFD1D416589ADD1F9214D79A5D1FD2D5D91C1F5296199D1094581BDF939),
    .INIT_3F(256'h3D3935312D2D2D2D2D3135394149515965717D8999A9B9CDE1F5092139516D85),
    .INIT_40(256'hA5794D21F9D1AD8965411DFDDDC1A1856D51392109F5E1CDB9A999897D716559),
    .INIT_41(256'h7525D5893DF1A55D15CD8541FDBD7939F9BD814509D1996129F5C1915D2DFDD1),
    .INIT_42(256'hE571FD8919A535C959ED8519B149E17D19B555F59535D97D21C97119C56D19C9),
    .INIT_43(256'h1175D941A9117DE555C131A11185F96DE159D14DC945C141BD41C145C94DD55D),
    .INIT_44(256'h114D8DC90D4D91D51961A9F13D89D52575C51569BD1569C11D75D12D8DED4DAD),
    .INIT_45(256'h01152D455D7991B1CDED0D2D517599C1E9113D6995C1F1215589BDF1296199D5),
    .INIT_46(256'h01EDD9C9B9A99D91857D756D6961615D5D5D6165696D757D85919DADB9C9DDED),
    .INIT_47(256'h29E9AD713901C995612DFDCD9D6D4115EDC59D7951310DEDCDAD91755D452D15),
    .INIT_48(256'h9129C15DF99935D57919BD6509B15D05B1610DBD7121D58D41F9B56D29E9A565),
    .INIT_49(256'h59C535A51589F971E55DD551CD49C949C949CD55D961ED7501911DAD3DD165F9),
    .INIT_4A(256'h99DD1D61A5ED317DC5115DADFD4DA1F549A1F951AD0965C52589E951B51D85ED),
    .INIT_4B(256'h6D8195ADC5DDF91531517191B5D901295179A5D501316195C9FD356DA5E11D5D),
    .INIT_4C(256'hEDD1B9A18D7965554535291D110901F9F5F1F1F1F1F5F9FD050D15212D3D495D),
    .INIT_4D(256'h31E5A15915D1915115D5996129F1B9855521F1C5996D4119F1CDA98565452509),
    .INIT_4E(256'h51D961ED79099529B94DE57911AD49E58121C56509B15901A95501B16111C579),
    .INIT_4F(256'h69C11975D12D8DED51B51981E955C12D9D0D7DF165D951CD45C141BD41C145C9),
    .INIT_50(256'h8DB1DD05315D8DBDF125598DC5013D79B5F53979BD054D95DD2979C51569BD11),
    .INIT_51(256'hD5CDC1BDB5B1B1ADADB1B5B9C1C9D1DDE9F909192D41597189A5C1E1FD214165),
    .INIT_52(256'h5C20E4AC743C08D4A4754519EDC59D75512D09E9CDAD9579614935210DFDF1E1),
    .INIT_53(256'h34C858EC8018B04CE48424C46408AC54FCA85400B06010C47830E8A45C1CD898),
    .INIT_54(256'h78D83494F858C0248CF864D040B020940880F870EC6CE868EC70F47C048C18A8),
    .INIT_55(256'h3C6490BCE818487CB0E41C5490CC084888CC1058A0E83480CC1C70C0186CC420),
    .INIT_56(256'h948880746C686460606064686C74808894A4B4C4D8EC041C34506C8CACCCF018),
    .INIT_57(256'h985818D89C6024ECB884501CF0C094684018F4D0AC8C7050341C04ECD8C4B4A4),
    .INIT_58(256'h5CE46CF88410A034C85CF48C28C46000A044E88C34E08C38E89848FCB46820DC),
    .INIT_59(256'hF04094E83C94EC48A40464C82890F860C834A41484F86CE45CD450D04CD050D4),
    .INIT_5A(256'h6C84A0BCDCFC1C40648CB4E00C3C6C9CD0043C74B0E82868A8EC3078C00854A0),
    .INIT_5B(256'hE0C4A48C705844301C0CFCF0E4D8D0CCC8C4C4C4C4C8D0D8E0ECF808182C4054),
    .INIT_5C(256'h640CB86410C07024D8904804C07C3CFCC0844C14E0AC784818ECC09870482404),
    .INIT_5D(256'h0878E85CD044BC38B430B030B438C048D05CEC780CA034C860FC9834D47818C0),
    .INIT_5E(256'hDC104880BCF83878B8FC4488D4206CB80C5CB00860B81470D03094F85CC4309C),
    .INIT_5F(256'hF0ECE8E8E8ECF0F4FC081420304054687C94B0CCE808284C7498C0EC184878A8),
    .INIT_60(256'h5C1CE0A46830F8C490603004D8AC84603C18F8DCBCA488705C483828180C00F8),
    .INIT_61(256'h2CB038C048D464F48418B044E07818B458F89C44EC9440F0A05004B87028E0A0),
    .INIT_62(256'h6CB800509CEC4094E8409CF854B41478DC44AC1884F064D448C034B02CA828A8),
    .INIT_63(256'h3440506070849CB4CCE80828486C90B8E00C386498C8FC346CA4E01C5CA0E028),
    .INIT_64(256'h8C5C3004D8B08864402000E4C8B098806C5C4C403028201814101014141C2028),
    .INIT_65(256'h881CB044DC7814B050F4983CE49038E89848FCB06820D8985414D89C602CF4C0),
    .INIT_66(256'h3488DC3890EC4CAC1074D840AC1884F468DC50C840BC38B838BC44C850DC68F8),
    .INIT_67(256'h98B0C8E4001C406084ACD4FC285888B8EC245C94D00C4C90D01860A8F44090E0),
    .INIT_68(256'hC8A07C583818F8DCC0A894806C5C504038302824202020242830384450607084),
    .INIT_69(256'hD06C04A440E4882CD47C28D48434E89C500CC4804000C4885018E0B07C4C20F4),
    .INIT_6A(256'hBC1470D03090F458C02C980474E85CD048C43CBC3CBC40C44CD864F08010A438),
    .INIT_6B(256'h90ACC8E808284C749CC8F4205084B8F028609CDC1C60A4E8307CC81464B80C64),
    .INIT_6C(256'h603C18F8D8B8A0846C58443424140C00F8F4F0F0F0F4F8FC04101C2C3C4C6478),
    .INIT_6D(256'h34D06C08A84CF09840E89444F4A85C10C8844000C080480CD4A06C3C0CDCB488),
    .INIT_6E(256'h1470CC2888E84CB41C84F060D040B42CA41C981898189C24AC34C050E070049C),
    .INIT_6F(256'h0C24405C7C9CC0E40C34608CBCEC20548CC400407CC0044890D82474C41468BC),
    .INIT_70(256'h20F8D4B08C6C50341800ECD8C4B4A89C908C8480808084888C98A0ACBCCCE0F4),
    .INIT_71(256'h60F49028C46404A84CF49C44F4A05404BC702CE8A46424E8AC743C08D8A8784C),
    .INIT_72(256'hCC2078D02C88E848AC1078E44CBC2C9C1084FC78F470F070F47C048C1CA838CC),
    .INIT_73(256'h708498B0C8E4002040648CB4DC08346498CC003874B0EC2C70B4FC448CDC2878),
    .INIT_74(256'h5024F4CCA07C583414F4D8BCA4907C68584C40342C2824242424283038445060),
    .INIT_75(256'h78049828BC54F08828C4680CB05800AC5C0CBC7028E0985414D4985C24ECB484),
    .INIT_76(256'hE43080D02478D02880E03CA00468D038A41080F468DC54D04CC848CC50D860E8),
    .INIT_77(256'hA0ACB8C4D4E8FC102C446080A0C4E810386490C0F0245C94CC084484C80C509C),
    .INIT_78(256'hB078400CD8A87C5024FCD8B4907054381C08F0DCCCBCB0A49C94909090909498),
    .INIT_79(256'h149C20A834C050E07408A038D47414B458FCA450FCAC5C0CC07830ECA86828EC),
    .INIT_7A(256'hD8185CA0E83480CC1C70C41C74CC2C88EC4CB41884F05CCC3CB028A018941494),
    .INIT_7B(256'hF4F4F4F4FC000C18243444586C84A0BCD8F81C406890B8E4144478ACE41C5898),
    .INIT_7C(256'h702CE8A86C30F4BC885424F4C89C704C2404E4C4A88C745C483828180C04FCF8),
    .INIT_7D(256'h50C840C03CC040C84CD864F08010A43CD46C08A848EC9034E08838E4984C00B8),
    .INIT_7E(256'h90C4FC3870B0F03074BC04509CEC3C8CE43894EC4CAC0C70D43CA81480F064D8),
    .INIT_7F(256'h34241C1008040000000408101C24344454688098B0CCEC0C30547CA4D0FC2C5C),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized5
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h00003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000),
    .INITP_02(256'h00000000000000007FFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000),
    .INITP_03(256'h000007FFFFFFFFFFFFFFFE000000000000000001FFFFFFFFFFFFFFFFFFF00000),
    .INITP_04(256'h00000000001FFFFFFFFFFFFF800000000000003FFFFFFFFFFFFFFC0000000000),
    .INITP_05(256'h03FFFFFFFFFFE000000000003FFFFFFFFFFFC000000000001FFFFFFFFFFFFC00),
    .INITP_06(256'h00FFFFFFFFFF80000000003FFFFFFFFFE00000000003FFFFFFFFFF8000000000),
    .INITP_07(256'h000000003FFFFFFFFE0000000007FFFFFFFFE0000000007FFFFFFFFF00000000),
    .INITP_08(256'hFE0000000001FFFFFFFFFC000000000FFFFFFFFFC000000000FFFFFFFFF80000),
    .INITP_09(256'hFF800000000003FFFFFFFFFF80000000000FFFFFFFFFF80000000003FFFFFFFF),
    .INITP_0A(256'h0000007FFFFFFFFFFFF0000000000007FFFFFFFFFFF800000000000FFFFFFFFF),
    .INITP_0B(256'h000000000000007FFFFFFFFFFFFFF800000000000003FFFFFFFFFFFFF0000000),
    .INITP_0C(256'h000000001FFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFC0),
    .INITP_0D(256'h0000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFC000000000000),
    .INITP_0E(256'h000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0302020202020202010101010101010101000000000000000000000000000000),
    .INIT_01(256'h0A0A090909080808080707070706060606050505050504040404040303030303),
    .INIT_02(256'h1515141414131312121211111010100F0F0F0E0E0E0D0D0D0C0C0C0B0B0B0A0A),
    .INIT_03(256'h252424232322222121201F1F1E1E1D1D1C1C1C1B1B1A1A191918181717171616),
    .INIT_04(256'h39383737363535343333323131302F2F2E2E2D2C2C2B2B2A2929282827262625),
    .INIT_05(256'h51504F4E4D4D4C4B4A49494847464645444343424140403F3E3D3D3C3B3B3A39),
    .INIT_06(256'h6D6C6B6A69686766656463636261605F5E5D5C5B5B5A59585756555554535251),
    .INIT_07(256'h8D8C8B8A8988878684838281807F7E7D7C7B7A79787776757473727170706F6E),
    .INIT_08(256'hB1B0AFAEADABAAA9A8A7A6A4A3A2A1A09F9E9C9B9A9998979695939291908F8E),
    .INIT_09(256'hDAD9D7D6D5D3D2D1CFCECDCCCAC9C8C6C5C4C3C1C0BFBEBCBBBAB9B7B6B5B4B3),
    .INIT_0A(256'h0705040201FFFEFDFBFAF8F7F5F4F3F1F0EEEDECEAE9E8E6E5E3E2E1DFDEDDDB),
    .INIT_0B(256'h3836343331302E2D2B292826252322201F1D1C1A191716141311100E0D0B0A08),
    .INIT_0C(256'h6D6B6967666462615F5D5C5A5857555352504E4D4B494846444341403E3C3B39),
    .INIT_0D(256'hA6A4A2A09E9C9B9997959392908E8C8A8987858382807E7C7A7977757372706E),
    .INIT_0E(256'hE3E1DFDDDBD9D7D5D3D1CFCDCBC9C8C6C4C2C0BEBCBAB8B6B5B3B1AFADABA9A8),
    .INIT_0F(256'h2422201E1C1A181513110F0D0B0907050301FFFDFBF9F7F5F3F1EFEDEBE9E7E5),
    .INIT_10(256'h69676563605E5C5A585553514F4D4B48464442403E3B39373533312F2D2A2826),
    .INIT_11(256'hB3B0AEACA9A7A5A2A09E9B99979492908E8B89878482807E7B79777572706E6C),
    .INIT_12(256'h00FEFBF9F6F4F1EFECEAE7E5E3E0DEDBD9D6D4D2CFCDCAC8C6C3C1BFBCBAB7B5),
    .INIT_13(256'h514F4C4A4744423F3D3A383532302D2B282623211E1C191614110F0C0A070503),
    .INIT_14(256'hA7A4A19F9C999694918E8C898684817E7C797674716E6C696664615E5C595754),
    .INIT_15(256'h00FDFAF7F5F2EFECE9E6E4E1DEDBD8D6D3D0CDCAC8C5C2BFBDBAB7B4B2AFACA9),
    .INIT_16(256'h5D5A5754514E4B484543403D3A3734312E2B282522201D1A1714110E0B090603),
    .INIT_17(256'hBEBBB8B5B2AFACA9A6A3A09C999693908D8A8784817E7B7875726F6C69666360),
    .INIT_18(256'h23201D1A1613100D0A060300FDFAF7F3F0EDEAE7E4E1DDDAD7D4D1CECBC8C4C1),
    .INIT_19(256'h8C8985827F7B7875726E6B6864615E5A5754514D4A4744403D3A3733302D2A26),
    .INIT_1A(256'hF9F5F2EEEBE8E4E1DDDAD6D3D0CCC9C5C2BFBBB8B4B1AEAAA7A4A09D9996938F),
    .INIT_1B(256'h6966625F5B5754504D4946423F3B3834312D2A26231F1C1815110E0A070300FC),
    .INIT_1C(256'hDEDAD6D2CFCBC7C4C0BCB9B5B2AEAAA7A39F9C9894918D8A86827F7B7874706D),
    .INIT_1D(256'h56524E4A46433F3B3733302C2824211D1915120E0A0603FFFBF7F4F0ECE9E5E1),
    .INIT_1E(256'hD1CDC9C6C2BEBAB6B2AEAAA6A29F9B97938F8B8784807C7874706C6965615D59),
    .INIT_1F(256'h514D4945413D3834302C2824201C1814110D090501FDF9F5F1EDE9E5E1DDD9D5),
    .INIT_20(256'hD4D0CBC7C3BFBBB7B3AEAAA6A29E9A96928E8A85817D7975716D6965615D5955),
    .INIT_21(256'h5A56524E4945413D3834302C27231F1B17120E0A0602FDF9F5F1EDE9E4E0DCD8),
    .INIT_22(256'hE5E0DCD8D3CFCAC6C2BDB9B5B0ACA8A39F9B96928E8985817D7874706B67635F),
    .INIT_23(256'h726E6965605C58534F4A46413D3834302B27221E1915110C0803FFFBF6F2EDE9),
    .INIT_24(256'h04FFFBF6F1EDE8E4DFDAD6D1CDC8C4BFBBB6B2ADA8A49F9B96928D8984807B77),
    .INIT_25(256'h98948F8A86817C78736E6A65605C57524E4944403B36322D29241F1B16120D08),
    .INIT_26(256'h312C27221D19140F0A0601FCF7F2EEE9E4DFDBD6D1CCC8C3BEB9B5B0ABA7A29D),
    .INIT_27(256'hCCC7C2BEB9B4AFAAA5A09B96928D88837E79746F6B66615C57524E49443F3A36),
    .INIT_28(256'h6B66615C57524D48433E39342F2A25201B16110C0803FEF9F4EFEAE5E0DBD6D1),
    .INIT_29(256'h0E0803FEF9F4EFEAE5E0DAD5D0CBC6C1BCB7B2ADA8A39E99948E89847F7A7570),
    .INIT_2A(256'hB3AEA9A39E99948F89847F7A756F6A65605B56504B46413C37312C27221D1813),
    .INIT_2B(256'h5C57514C47413C37312C27211C17120C0702FDF7F2EDE7E2DDD8D2CDC8C3BEB8),
    .INIT_2C(256'h0802FDF8F2EDE7E2DDD7D2CCC7C2BCB7B1ACA7A19C97918C87817C77716C6661),
    .INIT_2D(256'hB7B1ACA6A19B96908B85807A756F6A645F59544F49443E39332E28231E18130D),
    .INIT_2E(256'h69645E58534D47423C37312C26201B15100A04FFF9F4EEE9E3DED8D3CDC7C2BC),
    .INIT_2F(256'h1E19130D0802FCF6F1EBE5E0DAD4CFC9C3BEB8B2ADA7A19C96918B85807A746F),
    .INIT_30(256'hD7D1CBC5BFBAB4AEA8A39D97918B86807A746F69635D58524C46413B35302A24),
    .INIT_31(256'h928C86807A746F69635D57514B45403A342E28221D17110B05FFFAF4EEE8E2DC),
    .INIT_32(256'h504A443E38322C26201A140E0802FCF7F1EBE5DFD9D3CDC7C1BBB5AFA9A49E98),
    .INIT_33(256'h110B05FFF9F3EDE7E0DAD4CEC8C2BCB6B0AAA49E98928C86807A746E68625C56),
    .INIT_34(256'hD5CFC8C2BCB6B0AAA49D97918B857F79736C66605A544E48423C362F29231D17),
    .INIT_35(256'h9B958F89827C767069635D57514A443E38322B251F19130C0600FAF4EDE7E1DB),
    .INIT_36(256'h655E58524B453F38322C251F19130C0600F9F3EDE7E0DAD4CDC7C1BBB4AEA8A2),
    .INIT_37(256'h312A241D17110A04FDF7F1EAE4DDD7D1CAC4BEB7B1AAA49E97918B847E78716B),
    .INIT_38(256'hFFF9F2ECE5DFD8D2CBC5BEB8B1ABA59E98918B847E77716A645E57514A443D37),
    .INIT_39(256'hD0CAC3BDB6AFA9A29C958F88827B756E67615A544D47403A332D262019130C06),
    .INIT_3A(256'hA49D979089837C756F68625B544E47403A332D261F19120C05FEF8F1EBE4DDD7),
    .INIT_3B(256'h7A736D665F58524B443E373029231C150F0801FBF4EDE7E0D9D3CCC5BFB8B1AB),
    .INIT_3C(256'h524C453E37302A231C150F0801FAF3EDE6DFD8D2CBC4BDB7B0A9A29C958E8781),
    .INIT_3D(256'h2D261F19120B04FDF6EFE9E2DBD4CDC6C0B9B2ABA49D979089827B746E676059),
    .INIT_3E(256'h0A03FCF5EFE8E1DAD3CCC5BEB7B0A9A29B958E878079726B645D575049423B34),
    .INIT_3F(256'hEAE3DCD5CEC7C0B9B2ABA49D968F88817A736C655E575049423B342D261F1811),
    .INIT_40(256'hA9B0B7BEC5CCD3DAE1E8EFF5FC030A11181F262D343B424950575E656C737A81),
    .INIT_41(256'hCDD4DBE2E9EFF6FD040B12191F262D343B424950575D646B727980878E959BA2),
    .INIT_42(256'hF3FA01080F151C232A30373E454C525960676E747B828990979DA4ABB2B9C0C6),
    .INIT_43(256'h1C232930373E444B52585F666D737A81878E959CA2A9B0B7BDC4CBD2D8DFE6ED),
    .INIT_44(256'h474E545B62686F757C838990979DA4ABB1B8BFC5CCD3D9E0E7EDF4FB01080F15),
    .INIT_45(256'h757B82888F959CA2A9AFB6BDC3CAD0D7DDE4EBF1F8FE050C12191F262D333A40),
    .INIT_46(256'hA5ABB1B8BEC5CBD2D8DFE5ECF2F9FF060C131920262D333A40474D545A61676E),
    .INIT_47(256'hD7DDE4EAF1F7FD040A11171D242A31373D444A51575E646A71777E848B91989E),
    .INIT_48(256'h0C13191F252C32383F454B52585E656B71787E848B91979EA4AAB1B7BEC4CAD1),
    .INIT_49(256'h444A51575D636970767C82898F959BA2A8AEB4BBC1C7CDD4DAE0E7EDF3F90006),
    .INIT_4A(256'h7F858B91979DA4AAB0B6BCC2C8CFD5DBE1E7EDF4FA00060C13191F252B32383E),
    .INIT_4B(256'hBCC2C8CED4DAE0E7EDF3F9FF050B11171D23292F363C42484E545A60666C7379),
    .INIT_4C(256'hFC02080E141A20262C32383E444A50565C62686E747A80868C92989EA4AAB0B6),
    .INIT_4D(256'h40454B51575D63696F747A80868C92989EA4A9AFB5BBC1C7CDD3D9DFE5EBF1F7),
    .INIT_4E(256'h868B91979DA3A8AEB4BABFC5CBD1D7DCE2E8EEF4FAFF050B11171D22282E343A),
    .INIT_4F(256'hCFD4DAE0E5EBF1F6FC02080D13191E242A30353B41464C52585D63696F747A80),
    .INIT_50(256'h1B20262C31373C42474D53585E64696F747A80858B91969CA1A7ADB2B8BEC3C9),
    .INIT_51(256'h6A6F757A80858B90969BA1A6ACB1B7BCC2C7CDD3D8DEE3E9EEF4F9FF040A1015),
    .INIT_52(256'hBCC2C7CCD2D7DDE2E7EDF2F8FD02080D13181E23282E33393E44494F54595F64),
    .INIT_53(256'h12171C21272C31373C41474C51575C61666C71777C81878C91979CA1A7ACB1B7),
    .INIT_54(256'h6A6F757A7F84898F94999EA3A9AEB3B8BEC3C8CDD2D8DDE2E7EDF2F7FD02070C),
    .INIT_55(256'hC6CBD0D5DAE0E5EAEFF4F9FE03080E13181D22272C31373C41464B50565B6065),
    .INIT_56(256'h252A2F34393E43484D52575C61666B70757A7F84898E94999EA3A8ADB2B7BCC1),
    .INIT_57(256'h888D92969BA0A5AAAFB4B9BEC2C7CCD1D6DBE0E5EAEFF4F9FE03080C11161B20),
    .INIT_58(256'hEEF2F7FC01060A0F14191D22272C31363A3F44494E52575C61666B6F74797E83),
    .INIT_59(256'h575C60656A6E73787C81868A8F94989DA2A7ABB0B5B9BEC3C8CCD1D6DBDFE4E9),
    .INIT_5A(256'hC4C8CDD1D6DADFE4E8EDF1F6FBFF04080D12161B1F24292D32363B4044494E52),
    .INIT_5B(256'h34383D41464A4F53585C6065696E72777B8084898D92969B9FA4A8ADB2B6BBBF),
    .INIT_5C(256'hA8ACB0B5B9BDC2C6CACFD3D8DCE0E5E9EDF2F6FBFF03080C1115191E22272B30),
    .INIT_5D(256'h1F23272C3034383D4145494E52565A5F63676B7074787D8185898E92969B9FA3),
    .INIT_5E(256'h9A9EA2A6AAAEB3B7BBBFC3C7CBD0D4D8DCE0E4E9EDF1F5F9FD02060A0E12171B),
    .INIT_5F(256'h181C2024282C3034383D4145494D5155595D6165696D7175797D81858A8E9296),
    .INIT_60(256'h9B9FA2A6AAAEB2B6BABEC2C6C9CDD1D5D9DDE1E5E9EDF1F5F9FD0105090D1114),
    .INIT_61(256'h2124282C3033373B3F43464A4E5256595D6165696C7074787C8084878B8F9397),
    .INIT_62(256'hAAAEB2B5B9BCC0C4C7CBCFD2D6DADEE1E5E9ECF0F4F7FBFF03060A0E1215191D),
    .INIT_63(256'h383B3F4246494D5054575B5F6266696D7074787B7F82868A8D9194989C9FA3A7),
    .INIT_64(256'hC9CCD0D3D6DADDE1E4E8EBEEF2F5F9FC0003070A0E1115181C1F23262A2D3134),
    .INIT_65(256'h5E6164686B6E7275787B7F8285898C8F9396999DA0A4A7AAAEB1B4B8BBBFC2C5),
    .INIT_66(256'hF7FAFD0003060A0D1013161A1D2023262A2D3033373A3D4044474A4D5154575A),
    .INIT_67(256'h9396999CA0A3A6A9ACAFB2B5B8BBBEC1C4C8CBCED1D4D7DADDE1E4E7EAEDF0F3),
    .INIT_68(256'h34373A3D404345484B4E5154575A5D606366696C6F7275787B7E8184878A8D90),
    .INIT_69(256'hD8DBDEE1E4E6E9ECEFF2F5F7FAFD000306090B0E1114171A1D202225282B2E31),
    .INIT_6A(256'h818486898C8E919496999C9FA1A4A7A9ACAFB2B4B7BABDBFC2C5C8CACDD0D3D6),
    .INIT_6B(256'h2D303235383A3D3F4244474A4C4F515457595C5E616466696C6E717476797C7E),
    .INIT_6C(256'hDEE0E3E5E7EAECEFF1F4F6F9FBFE000305070A0C0F111416191C1E212326282B),
    .INIT_6D(256'h929497999B9EA0A2A5A7A9ACAEB0B3B5B7BABCBFC1C3C6C8CACDCFD2D4D6D9DB),
    .INIT_6E(256'h4B4D4F515355585A5C5E60636567696C6E70727577797B7E80828487898B8E90),
    .INIT_6F(256'h07090B0D0F111315181A1C1E20222426282A2D2F31333537393B3E4042444648),
    .INIT_70(256'hC8C9CBCDCFD1D3D5D7D9DBDDDFE1E3E5E7E9EBEDEFF1F3F5F7F9FBFDFF010305),
    .INIT_71(256'h8C8E9092939597999B9C9EA0A2A4A6A8A9ABADAFB1B3B5B6B8BABCBEC0C2C4C6),
    .INIT_72(256'h5557585A5C5D5F6162646667696B6D6E7072737577797A7C7E8082838587898A),
    .INIT_73(256'h2223252628292B2D2E303133343638393B3C3E404143444648494B4D4E505253),
    .INIT_74(256'hF3F4F5F7F8FAFBFDFEFF0102040507080A0B0D0E101113141617191A1C1D1F20),
    .INIT_75(256'hC8C9CACCCDCECFD1D2D3D5D6D7D9DADBDDDEDFE1E2E3E5E6E8E9EAECEDEEF0F1),
    .INIT_76(256'hA1A2A3A4A6A7A8A9AAABADAEAFB0B1B3B4B5B6B7B9BABBBCBEBFC0C1C3C4C5C6),
    .INIT_77(256'h7E7F8081828384868788898A8B8C8D8E8F9091929395969798999A9B9C9E9FA0),
    .INIT_78(256'h60616263636465666768696A6B6C6D6E6F70707172737475767778797A7B7C7D),
    .INIT_79(256'h4646474849494A4B4C4D4D4E4F5051515253545555565758595A5B5B5C5D5E5F),
    .INIT_7A(256'h2F3031313233333435353637373839393A3B3B3C3D3D3E3F4040414243434445),
    .INIT_7B(256'h1D1E1E1F1F2021212222232324242525262627282829292A2B2B2C2C2D2E2E2F),
    .INIT_7C(256'h1010101111121212131314141415151616171717181819191A1A1B1B1C1C1C1D),
    .INIT_7D(256'h060607070707080808080909090A0A0A0A0B0B0B0C0C0C0D0D0D0E0E0E0F0F0F),
    .INIT_7E(256'h0101010101010102020202020202030303030303040404040405050505050606),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000101),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized6
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h6AACE01C956C7FC6D5B381E4AA47039A5A63F8CAA99FF1B559820CD56C7F8DAA),
    .INITP_01(256'h0ED25B800C956CFFE6556C7F8DAAD8FF195533FF1B56CE0792AB380335D66006),
    .INITP_02(256'h73514CFFE24ADB800CD55987864AB67C79955B1FE32D69C0064AA67039955982),
    .INITP_03(256'hB33FF196B4CF9F325698FF8C95247CF9B553380E25548E079B54987C32553380),
    .INITP_04(256'h923C0F36AA99C01CDAAB31FF8DA5A4E00392AA4E1C19A56D87F8C9569C7E192A),
    .INITP_05(256'h92552CC3F872575261FF1DB55267C1E66AA931FF8CD2B48E00736AA4C7FF192A),
    .INITP_06(256'h24E3FFC765AA5B1C003992AADB1F07CCD2A9271FF0CDAAA4CF00F32555B38103),
    .INITP_07(256'h8FFF0E6DA92B6C707E0E64B55A4CE020399A554B33C00799A554931F8FC64955),
    .INITP_08(256'hFF8E495524C7E3F192554B33C00799A554B338080E64B55A4CE0FC1C6DA92B6C),
    .INITP_09(256'h69549381039B55499E01E64AAB661FF1C92A9667C1F1B6AA93380071B4AB4DC7),
    .INITP_0A(256'hE07892A931FFC64AAD9C00E25A9663FF192AACCF07CC955B71FF0C95D49C3F86),
    .INITP_0B(256'h1FF99AA930FC72D5263FC36D4B3070E4AA93800E4B4B63FF19AAB6700732AAD9),
    .INITP_0C(256'h65159C039954987C3255B3C0E25548E039955B3E7C495263FE32D499F3E65AD3),
    .INITP_0D(256'hB496E0833553381CCAA4C0072D698FF1B5533C7CDAA4C3C335566003B6A48FFE),
    .INITP_0E(256'h0E6AACC00CD7598039AA93C0E6D5B1FF995531FE36AB63FC6D54CFFE6D526003),
    .INITP_0F(256'h4C0064AB63FC6D566083355B1FF32AA63F8CB4B381C4AA4F039B56C7FC6D5270),
    .INIT_00(256'hA0C02060E060E08020E0A080808080C0E02080E060E08020E0A08060606080C0),
    .INIT_01(256'h4060A0E02080008020C060400000E000004080C0208000A040E0A08060606060),
    .INIT_02(256'h2020206080E040A020A040E0A0806040406080C00060C020C04000A080604040),
    .INIT_03(256'hE0C0C0E0002060C020A020C06000E0A0A0A0A0C0E02060C040C040E0A0604020),
    .INIT_04(256'hA06060406060A0E02080E06000A04000E0C0C0C0C0E02060C020A020C0804000),
    .INIT_05(256'h20E0C0A0A0A0A0C00040A0008000A04000E0A0A0A0A0C0004080E060E08020C0),
    .INIT_06(256'h8040E0C0A08080A0C0E02080E040C06000C080604040406080C00060E060E080),
    .INIT_07(256'h8020C0804020000000206080E040A020C06000C0A080808080A0E02080E060E0),
    .INIT_08(256'h008020C080402000E000004080C02080008020E080604020204060A0E0208000),
    .INIT_09(256'hE060E06000C080604020404080A0E040C020C04000C0806040406080A0E040A0),
    .INIT_0A(256'h0060C040E08020E0C0A0A0A0C0E00060A0208020A06020E0C0A0A0A0C0004080),
    .INIT_0B(256'h0060C020A040E0A06020000000204080C02080008020C08040202020204060A0),
    .INIT_0C(256'hE02080E060E06020C080604040406080C00060C040E06020E0A080606080A0C0),
    .INIT_0D(256'h6080E020A000A020E0A060402020204080A00060C040E06020E0A0808080A0C0),
    .INIT_0E(256'h206080E040A020C06000C0A080606080A0C00060C040C040E0A0604020202040),
    .INIT_0F(256'h012161A10161E16101A1612101E1E1E1012160A00060E06000C0604020000000),
    .INIT_10(256'hC1E12161A10181018121E1A1614141416181A1E141A121A121C181412101E101),
    .INIT_11(256'h01214181E141A121A14101C1816141416181A1E12181018101A16121E1C1C1C1),
    .INIT_12(256'h81A1C10161A121A121C1612101C1C1C1C1E10141A1E161E16101A16141010101),
    .INIT_13(256'hE1014181C1218101A141E1A161414141416181C12161E161E18121E1C1A18181),
    .INIT_14(256'hE1214181E141A121C16101C1A181818181A1E12161C121A141E181412101E1E1),
    .INIT_15(256'h226181E141A121A141E1A16141210121216181C12181018121C1612101E1E1E1),
    .INIT_16(256'h4282C20282E26202A24202E2C2C2C2C2E20242A20262E28202C2824222020202),
    .INIT_17(256'hE22262C242C242E2A262220202E202224282C22282028222C282422202020222),
    .INIT_18(256'h82E242A222C26202C2A28262626282C2E242A2028202A24202C28282626282A2),
    .INIT_19(256'hE242C242C28222E2C2A28282A2C2E22282E242C242E2824222E2C2C2C2E20242),
    .INIT_1A(256'h62E26202C2824222020202224282C22282E26202A26202E2C2A2A2C2C2024282),
    .INIT_1B(256'hC36303C383634343434363A3E32383038303A34303E3A3A38383A3C303438302),
    .INIT_1C(256'h6323E3C3A3A3A3C3E30343A30363E38303C363430303E3E3032363A3E343C323),
    .INIT_1D(256'hE3C3C3A3C3C3032383C323A323A34303C383634343436383A30343A3238323C3),
    .INIT_1E(256'hC3C3E3032363C32383038323C383432303E3E303034383C32383038303A36323),
    .INIT_1F(256'h84A4E444A4048404A444E4A484644444446484C40463C343C343E3834323E3E3),
    .INIT_20(256'h84E464C46404A44424E4C4C4C4C4E4044484E444C444C46404C4A46444444464),
    .INIT_21(256'h64E4844404C4A4848484A4C4E42484E444C444E48424E4C4A4848484A4C40444),
    .INIT_22(256'h642404E4E4E404244484C42484048424C4642404C4C4A4A4C4E40444A40464C4),
    .INIT_23(256'h2525456585C52585E565E58525C5856525250525254585C50565C444C46404A4),
    .INIT_24(256'hE52585E565E56505A5652505E5E5E5E5052565A50565C545C56505C585452525),
    .INIT_25(256'h25A54505C58545452525254565A5E545A5058505A54505C585654545454585A5),
    .INIT_26(256'h4626060606264666A6E646A5058525C56525E5A58585658585A5E52565C525A5),
    .INIT_27(256'hA6C60646A60686E68626C6662606E6C6C6C6C6E60646A6E646C646C66606A666),
    .INIT_28(256'hA626A64606A6664626060606264666A60646A626A626C66626E6A68686666686),
    .INIT_29(256'h87674746466686A6E62686E646C646E68626E6C6866666666686A6E62666C626),
    .INIT_2A(256'hC70747A70787078727C7672707C7C7A7A7C7C7072767C70787E76707A74707C7),
    .INIT_2B(256'hA747E7A7674727272727276787C70767C747A747C76727E7A787674747676787),
    .INIT_2C(256'h486887A7E72767C727A727A747E7A7672707E7C7C7E7E7274787C72787078707),
    .INIT_2D(256'h48C848E8884808C8A88868688888A8E80868A80888E86808A84808C8A8686848),
    .INIT_2E(256'hC8A8A8C8E8082868C82888E868E88828E88868280808E80808284888C80868C8),
    .INIT_2F(256'hE969E96909A94808C8A88868686888A8C8084888E848A828C848E8A86828E8C8),
    .INIT_30(256'h29090929496989C92969C949A949C96909C98949290909E90909296989C92989),
    .INIT_31(256'h8909A929C9894909C9A9A9898989A9C9092989C92989098909A94909C9894929),
    .INIT_32(256'h8A8AAACA0A4A8AEA4AAA2AAA2ACA6909C989694929292929496989C90969C929),
    .INIT_33(256'h2ACA8A4A0ACAAA8A8A8A8AAACAEA2A6AAA0A6AEA4AEA6A0AAA6A2AEACAAA8A8A),
    .INIT_34(256'hCA0A4AAA0A6AEA6AEA8A2AEA8A6A2A0AEAEACAEAEA0A2A6AAAEA4AAA0A8A0AAA),
    .INIT_35(256'h6B4B4B4B4B4B6B8BABEB2B8BCB4BAB2BAB2BCB6B2BEBAB8B4B4B2A2A2A4A6A8A),
    .INIT_36(256'h6B0B8B2BEB8B4B2BEBCBCBABABCBCBEB0B4B8BCB2B8BEB6BEB8B0BAB6B0BCBAB),
    .INIT_37(256'hEC2C6CAC0C6CEC6CEB6B0BAB4B0BCB8B6B4B4B2B2B4B4B6BABCB0B6BAB0B8BEB),
    .INIT_38(256'h0CECECEC0C0C4C6CACEC2C8CEC4CCC4CCC6C0CAC6C2CECAC8C6C6C6C6C8C8CCC),
    .INIT_39(256'hEC8C4C0CCCAC8C6C6C6C6C8C8CCCEC2C6CAC0C6CEC4CCC6CEC8C2CECAC6C4C2C),
    .INIT_3A(256'hAD2DAD2DAD4DED8D4D0DCDAD8D6D6D4D6D6D8DADCD0D4D8DED4CAC0C8C0CAC4C),
    .INIT_3B(256'h8DCD0D6DCD2D8D0D8D0DAD2DED8D4D0DCDAD8D6D6D6D6D6D8DADED0D4DADED4D),
    .INIT_3C(256'hAEAECEEE0E4E8ECE2E6ECE4EAE2DCD4DED8D2DEDAD6D4D2D0D0DED0D0D2D4D6D),
    .INIT_3D(256'h0EEECECECEEEEE0E2E6EAEEE2E8EEE4EAE2EAE4ECE6E2ECE8E4E0EEECEAEAEAE),
    .INIT_3E(256'hCF8F6E2E0E0EEEEEEE0E2E4E6E8ECE0E6ECE2E8EEE6EEE8E0EAE4E0ECE8E4E2E),
    .INIT_3F(256'h0FAF6F2FEFCFAF8F6F6F6F6F6F8FAFCF0F4F8FCF2F8FEF4FCF4FCF6F0FAF4F0F),
    .INIT_40(256'hEE6EEE8E2ECE6E0ECE8E6E4E2E0FEFEFEF0F0F2F6F8FCF0F4FAF0F6FCF4FCF4F),
    .INIT_41(256'h2E6ECE4EAE2EAE4EEE8E2EEEAE6E2E0EEEEECECECEEE0E2E4E8ECE0E4EAE0E8E),
    .INIT_42(256'h4D6DAEEE2E8EEE4ECE2EAE4ECE6E2ECE8E4E0EEECEAEAEAEAEAECEEE0E4E8ECE),
    .INIT_43(256'h6D6D6D6D8DADCD0D4D8DED2DAD0D8D0D8D2DCD6D0DCD8D6D4D2D0D0DED0D0D2D),
    .INIT_44(256'h4D0DCDAD8D6D6D4D6D6D8DADCD0D4D8DED4DAD2DAD2DAD4DEDAD4D0DEDAD8D6D),
    .INIT_45(256'hCC4CEC6C0CAC6C2CECCC8C8C6C6C6C6C8CACCC0C4C8CED4DAD0D8D0DAD4DED8D),
    .INIT_46(256'hEC2C6CAC0C6CCC4CCC4CEC8C2CECAC6C4C0C0CECECEC0C2C4C6CACEC2C8CEC6C),
    .INIT_47(256'h4B4B2B2B4B4B6B8CCC0C4CAC0C6CEC6CEC6C0CAC6C2CECCC8C8C6C6C6C6C8CAC),
    .INIT_48(256'hEB8B2BCB8B4B0BEBCBCBABABCBCBEB2B4B8BEB2B8B0B6BEB8B0BAB6B0BCBAB6B),
    .INIT_49(256'hABEB2B6BCB2BAB2BAB4BCB8B2BEBAB8B6B4B4B4B4B4B6BABCB0B6BAB0B8BEB6B),
    .INIT_4A(256'h2A0AEAEACAEAEA0A2A6A8AEA2A8AEA6AEA6A0AAA4A0BCB8B6B4B2B2B2B4B4B8B),
    .INIT_4B(256'h6AEA4AEA6A0AAA6A2AEACAAA8A8A8A8AAACA0A4A8ACA2AAA0A8A0AAA4AEAAA6A),
    .INIT_4C(256'h292A2A4A6A8ACA0A6ACA2AAA2AAA4AEA8A4A0ACAAA8A8A8A8AAACAEA2A6AAA0A),
    .INIT_4D(256'h098929C9892909C9A9898989A9A9C9094989C929A9098929C96909C989694929),
    .INIT_4E(256'h0909294989C90969C949A949C96929C989694929090929294989C90949A90989),
    .INIT_4F(256'hA848E8884808C8A88869696989A9C90949A90969E969E98929C98969290909E9),
    .INIT_50(256'hE80808286888E82888E868E88828C8682808E8C8A8A8C8C8E82868A8E848C828),
    .INIT_51(256'h68E88808A86808E8A88888686888A8C8084888E848C848C86808C88848280808),
    .INIT_52(256'hE7E7C7C7E7072767A7E747A727A828C86828E8A8886848486868A8C80848A808),
    .INIT_53(256'h2767C747A747C76707C7876727272727274767A7E747A70787078727C7874727),
    .INIT_54(256'hC7672707C7C7A7A7C7C7072767C72787078707A74707C787676747476787A7E7),
    .INIT_55(256'h666686C6E62686E646C646E68727E7A787674747476787C70747A70767E78707),
    .INIT_56(256'hC626A626A64606A6664626060606264666A60646A626A626C66626E6A6866666),
    .INIT_57(256'hA64606E6C6C6C6C6E6062666C62686E68606A64606C6A68666668686A6E62666),
    .INIT_58(256'h658585A5E62666C6268606A646E6A6664626060606264666A60666C646C646E6),
    .INIT_59(256'h0545A5058505A545E5A56545252525454585C50545A525A525C56525E5A58585),
    .INIT_5A(256'hC56505A5652505E5E5E5E5052565A50565E565E58525E5A585454545456585C5),
    .INIT_5B(256'h854525250525256585C52585E565E58525C5856545252525254585C50565C545),
    .INIT_5C(256'hA4C4C4042464C42484048424C484442404E4E4E4042565A50565C545C56505C5),
    .INIT_5D(256'hE42484E444C444E48424E4C4A4848484A4C4044484E464C46404A44404E4C4A4),
    .INIT_5E(256'hC444C444E4844404E4C4C4C4C4E42444A40464C464E4844404C4A4848484A4C4),
    .INIT_5F(256'hC46404C484644444446484A4E444A4048404A444E4A4846444444464A4C40464),
    .INIT_60(256'h83430303E3E303234383C32383038323C3632303E3C3C3E3E3234484E444C444),
    .INIT_61(256'h634343436383C30343A323A323C3832303C3C3A3C3C3E32363A30383038323C3),
    .INIT_62(256'hE303034363C30383E36303A34303E3C3A3A3A3C3E32363C3238323A34303A383),
    .INIT_63(256'hA3E30343A30383038323E3A363434343436383C30363C323C343E3A3632303E3),
    .INIT_64(256'h0262A20262E28222C2824222020202224383C30363E36303834303C3A38383A3),
    .INIT_65(256'h82E242C242E28222E2C2A28282A2C2E22282C242C242E2824202C2C2A2A2C2E2),
    .INIT_66(256'hA2028202A242E2C28262626282A2C20262C222A242E2824202E2C2C2C2E22242),
    .INIT_67(256'h82028222C282422202E202022262A2E242C242C26222E2A28262628282C20242),
    .INIT_68(256'hE26202A24202E2C2C2C2C2E20242A20262E28202C2824222020202224282C222),
    .INIT_69(256'h018121C18161212101214161A1E142A222A242E2826222020202224282C20282),
    .INIT_6A(256'h21C16121E1A181818181A1C10161C121A141E1814121E1E1E1E1012161C12181),
    .INIT_6B(256'hE16121C181614141414161A1E141A1018121C1814101E1E1E101214181E141A1),
    .INIT_6C(256'h61E1A14101E1C1C1C1C1012161C121A121A16101C1A1818181A1C1E12181E161),
    .INIT_6D(256'h018121E1A1816141416181C10141A121A141E1814121010101014161A10161E1),
    .INIT_6E(256'h21A141E1A1816141414161A1E12181018101A16121E1C1C1C1C1E12161A10181),
    .INIT_6F(256'hE16101A1612101E1E1E1012161A10161E16101A161210101E101214181C121A1),
    .INIT_70(256'hC040C06000C0A080606080A0C00060C020A040E0806020000000204060C10161),
    .INIT_71(256'hE040C06000A080402020204060A0E020A000A020E08060402020204060A0E040),
    .INIT_72(256'h60E040C06000C080604040406080C02060E060E08020E0C0A0808080A0E02060),
    .INIT_73(256'hC02080008020C08040200000002060A0E040A020C06000C0A080606080A0E020),
    .INIT_74(256'h2060A0208020A06000E0C0A0A0A0C0E02080E040C06000A06040202020204080),
    .INIT_75(256'h80C00040C020C040E0A080404020406080C00060E060E0804000C0A0A0A0C0E0),
    .INIT_76(256'h406080E02080008020C080400000E000204080C0208000A040E0A08060404060),
    .INIT_77(256'h8080A0C00060C020A040E0806020000000204080C02080008020E0A060402020),
    .INIT_78(256'h4040406080C00060C040E08020E0C0A08080A0C0E04080E060E08020E0A08080),
    .INIT_79(256'hC0A0A0A0A0E00040A0008000A04000C0A0A0A0A0C0E02080E060E06000C08060),
    .INIT_7A(256'h20E0C0C0C0C0E00040A00060E08020E0A06060406060A0C02080E060E0804000),
    .INIT_7B(256'h6020E0C0A0A0A0A0E00060C020A020C0602000E0C0C0E0004080C020A020C060),
    .INIT_7C(256'hC06000C0806040406080A0E040A020A040E08060202020204060A0E040C040C0),
    .INIT_7D(256'h008020C080400000E000004060C02080008020E0A0604040406080A00040C020),
    .INIT_7E(256'h80E060E08020E0C080808080A0E02080E060E06020C0A06060606080A0E040A0),
    .INIT_7F(256'h0040A020A020E0804020000000204080E020A020A04000C08060606080A0E020),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized7
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h00007FFFFFFFE0000000001FFFFFFFFFFFFC0000000000000000000000000000),
    .INITP_01(256'hFE0003FFFC0001FFFF00003FFFF00000FFFFF000007FFFFF0000007FFFFFF000),
    .INITP_02(256'h0FFE007FF001FFC007FF001FFE001FFE001FFF0007FFC001FFF8001FFF8000FF),
    .INITP_03(256'h807F807F807F803FC03FE01FF007F803FE00FF801FF007FE00FFC00FFC00FFC0),
    .INITP_04(256'h81FC0FE07F01F80FE03F80FC07F01FC03F80FE03FC07F00FE01FC03FC07F807F),
    .INITP_05(256'hF03F07E07C0FC1F81F81F03F03F03F03F03F03F03F03F81F81FC0FC0FE07F03F),
    .INITP_06(256'h3E0F83E0F83E07C1F07C1F03E0F83F07C1F83E07C1F83E07C0F81F07E0FC0F81),
    .INITP_07(256'h7C1E0F0783E1F0783C1F0783E1F0783E0F07C1F0F83E0F07C1F07C1F0F83E0F8),
    .INITP_08(256'h83E0F83E0F83E1F07C1F07C1E0F83E1F07C1E0F83C1F0F83C1F0783C1F0F83C1),
    .INITP_09(256'hC1F81F03E07E0FC1F03E07C0F83F07C0F83F07C1F83E0F81F07C1F07C0F83E0F),
    .INITP_0A(256'hE07F03F81FC0FE07E07F03F03F81F81F81F81F81F81F81F81F03F03F07E07C0F),
    .INITP_0B(256'h03FC03FC03FC07F807F00FE01FC07F80FE03F807F01FC07E03F80FE03F01FC0F),
    .INITP_0C(256'h00FFE007FE007FE007FE00FFC01FF003FE00FF803FC01FF00FF807F803FC03FC),
    .INITP_0D(256'h8000FFFE0003FFF0003FFF0007FFC001FFF000FFF000FFF001FFC007FF001FFC),
    .INITP_0E(256'hFC0000001FFFFFFC000001FFFFFC00001FFFFE00001FFFF80001FFFF00007FFF),
    .INITP_0F(256'h00000000000000000000000000000000007FFFFFFFFFFFF0000000000FFFFFFF),
    .INIT_00(256'h1E1C1B1A18171615141211100F0E0D0C0B0B0A09080707060505040403030202),
    .INIT_01(256'h5C59575452504E4B49474543413F3C3B39373533312F2E2C2A2827252422211F),
    .INIT_02(256'hBBB8B4B1ADAAA7A3A09D9A9693908D8A8784817E7C797673706E6B686663615E),
    .INIT_03(256'h3C37332E2A26211D1914100C0804FFFBF7F3EFEBE7E4E0DCD8D4D1CDC9C6C2BF),
    .INIT_04(256'hDED9D3CEC8C3BDB8B3ADA8A39E98938E89847F7A75706B66615D58534E4A4541),
    .INIT_05(256'hA29B958E88817B746E68615B554F48423C36302A241E18120C0600FBF5EFEAE4),
    .INIT_06(256'h8780787069615A524B433C352D261F18110902FBF4EDE6DFD8D1CBC4BDB6AFA9),
    .INIT_07(256'h8E857C746B635A5249413830271F170F06FEF6EEE6DED6CEC6BEB6AEA69E978F),
    .INIT_08(256'hB6ACA2988F857C72685F564C433930271E140B02F9F0E7DED5CCC3BAB1A89F97),
    .INIT_09(256'hFEF4E9DED4C9BEB4A99F948A7F756A60564C41372D23190F05FBF1E7DDD3C9BF),
    .INIT_0A(256'h695D5145392E22160BFFF4E8DDD1C6BBAFA4998E82776C61564B40352A1F1409),
    .INIT_0B(256'hF4E7DACDC0B4A79A8E8175685C4F43362A1E1105F9EDE0D4C8BCB0A4988C8074),
    .INIT_0C(256'h9F928476685A4D3F31241609FBEEE0D3C5B8AB9D908376695B4E4134271A0D00),
    .INIT_0D(256'h6C5D4E4031221305F6E7D9CABCAD9F9082736557483A2C1E1001F3E5D7C9BBAD),
    .INIT_0E(256'h5A4A3A2A1A0AFBEBDBCCBCAC9D8D7E6E5F4F4031211203F4E4D5C6B7A8998A7B),
    .INIT_0F(256'h68574635241302F2E1D0C0AF9F8E7D6D5D4C3C2B1B0BFAEADACABAAA9A8A7A6A),
    .INIT_10(256'h968472604E3D2B1907F6E4D2C1AF9E8C7B69584635241301F0DFCEBDAC9B8979),
    .INIT_11(256'hE5D2BFAC998673614E3B291603F1DECCB9A79482705D4B39271402F0DECCBAA8),
    .INIT_12(256'h533F2B1804F0DCC8B5A18D7A66523F2B1804F1DECAB7A4907D6A5744311E0BF8),
    .INIT_13(256'hE2CDB8A38E7A65503B2712FDE9D4C0AB97826E5945311C08F4E0CCB7A38F7B67),
    .INIT_14(256'h907B654F39230DF8E2CCB7A18B76604B35200AF5E0CAB5A08B75604B36210CF7),
    .INIT_15(256'h5F48311A03ECD6BFA8917B644E37210AF4DDC7B09A846E57412B15FFE9D2BCA6),
    .INIT_16(256'h4C341C05EDD5BDA68E765F472F1800E9D1BAA38B745D452E1700E9D2BBA48D76),
    .INIT_17(256'h5940270EF6DDC4AB937A6249311800E7CFB69E866D553D250CF4DCC4AC947C64),
    .INIT_18(256'h856B51371E04EAD1B79D846A51371E04EBD2B89F866D533A2108EFD6BDA48B72),
    .INIT_19(256'hCFB59A7F644A2F14FADFC5AA90755B41260CF2D7BDA3896F553A2006ECD2B99F),
    .INIT_1A(256'h391D01E6CAAE93775C402509EED2B79C80654A2F13F8DDC2A78C71563B2005EA),
    .INIT_1B(256'hC0A4876A4E3115F8DCBFA3866A4E3115F9DDC0A4886C503418FCE0C4A88C7055),
    .INIT_1C(256'h66492B0DF0D2B5977A5D3F2205E7CAAD907255381BFEE1C4A78A6D503317FADD),
    .INIT_1D(256'h2A0BEDCEB09173553618FADBBD9F8163442608EACCAE9072543719FBDDBFA284),
    .INIT_1E(256'h0BECCCAD8E6E4F3010F1D2B39374553617F8D9BA9B7C5D3E2001E2C3A5866749),
    .INIT_1F(256'h0AEAC9A98969482808E8C7A78767472707E7C7A78868482808E9C9A98A6A4A2B),
    .INIT_20(256'h2605E4C2A1805F3E1DFBDAB99877563615F4D3B29171502F0FEECDAD8C6C4B2B),
    .INIT_21(256'h5F3D1BF9D7B492704E2C0AE8C6A583613F1DFCDAB89775533210EFCDAC8A6948),
    .INIT_22(256'hB5926F4B2805E3C09D7A573411EFCCA98664411FFCD9B794724F2D0BE8C6A481),
    .INIT_23(256'h2703DFBB97734F2B07E4C09C7955310EEAC7A3805C3915F2CFAB8865411EFBD8),
    .INIT_24(256'hB4906B4621FCD8B38E6A4521FCD7B38E6A4621FDD9B4906C4723FFDBB7936F4B),
    .INIT_25(256'h5E3813EDC7A27C57310CE6C19B76502B06E1BB96714C2701DCB7926D4823FED9),
    .INIT_26(256'h23FDD6B089633C16EFC9A37C56300AE3BD97714B25FFD9B38D67411BF5CFAA84),
    .INIT_27(256'h03DCB58D663F17F0C9A27A532C05DEB79069421BF4CDA67F59320BE4BE97704A),
    .INIT_28(256'hFED6AE865E350DE5BD956D451DF5CDA57E562E06DEB78F674018F0C9A17A522B),
    .INIT_29(256'h14EBC29970471EF5CCA37A512900D7AE865D340CE3BB926A4119F0C8A0774F27),
    .INIT_2A(256'h431AF0C69C72491FF5CBA2784F25FBD2A87F552C03D9B0875D340BE2B88F663D),
    .INIT_2B(256'h8D62370DE2B88D63380EE3B98E643A0FE5BB90663C12E8BD93693F15EBC1976D),
    .INIT_2C(256'hF0C4996D4217EBC095693E13E8BC91663B10E5BA8F64390EE3B88D63380DE2B8),
    .INIT_2D(256'h6C3F13E7BB8F63360ADEB2865A2E02D6AB7F5327FBCFA4784C21F5C99E72471B),
    .INIT_2E(256'h00D3A6794C20F3C6996C3F12E6B98C603306DAAD815427FBCFA276491DF1C498),
    .INIT_2F(256'hAD805224F7C99B6E4012E5B78A5C2F01D4A7794C1EF1C497693C0FE2B5885A2D),
    .INIT_30(256'h734416E7B98A5C2DFFD1A2744618E9BB8D5F3103D5A7794A1DEFC193653709DB),
    .INIT_31(256'h4F20F1C292633405D6A778491AEBBC8D5E2F00D1A3744516E8B98A5C2DFED0A1),
    .INIT_32(256'h4313E3B3845424F4C494653505D5A6764617E7B8885929FACA9B6B3C0DDDAE7F),
    .INIT_33(256'h4E1DEDBC8C5B2AFAC999683807D7A7764616E5B5855424F4C494643303D3A373),
    .INIT_34(256'h6F3E0DDBAA794716E5B4835120EFBE8D5C2BFAC998673605D4A3734211E0B07F),
    .INIT_35(256'hA7754311DFAD7B4917E5B3814F1EECBA885725F3C2905E2DFBCA98673504D2A1),
    .INIT_36(256'hF4C18E5C29F6C4915E2CF9C794622FFDCB98663301CF9C6A3806D4A16F3D0BD9),
    .INIT_37(256'h5623EFBC895522EFBB885522EFBC885522EFBC895623F0BD8A5725F2BF8C5926),
    .INIT_38(256'hCD996531FDC995612DF9C6925E2AF6C38F5B28F4C08D5925F2BE8B5724F0BD89),
    .INIT_39(256'h5824EFBB86511DE8B47F4B16E2AD794510DCA8733F0BD6A26E3A06D19D693501),
    .INIT_3A(256'hF8C38D5823EEB8834E19E4AF7A4510DBA6713C07D29D6833FECA95602BF7C28D),
    .INIT_3B(256'hAB753F09D39E6832FCC7915B25F0BA854F19E4AE79430ED8A36D3803CD98622D),
    .INIT_3C(256'h713A04CD97612AF4BE87511BE4AE78420BD59F6933FDC7915B25EEB9834D17E1),
    .INIT_3D(256'h4A12DBA46D36FFC9925B24EDB67F4812DBA46D3700C9935C25EFB8814B14DEA7),
    .INIT_3E(256'h34FDC58E561FE7AF784109D29A632BF4BD854E17DFA8713A03CB945D26EFB881),
    .INIT_3F(256'h31F9C1895018E0A8703800C8905820E8B1794109D199612AF2BA824B13DBA46C),
    .INIT_40(256'h71A8DF174E85BDF42B639AD2094178AFE71F568EC5FD346CA4DB134B82BAF22A),
    .INIT_41(256'h93C900376DA4DB12487FB6ED245B92C9FF366DA4DB124A81B8EF265D94CB033A),
    .INIT_42(256'hC7FD33699FD50B4278AEE41B5187BEF42A6197CD043A71A7DE144B81B8EF255C),
    .INIT_43(256'h0E4379AEE4194F85BAF0255B91C7FC32689ED3093F75ABE1174D83B9EE255B91),
    .INIT_44(256'h689DD2073C71A6DB10457AAFE4194E83B8EE23588DC3F82D6298CD03386DA3D8),
    .INIT_45(256'hD60B3F73A8DC104579ADE2164B7FB4E81D5186BBEF24588DC2F72B6095CAFE33),
    .INIT_46(256'h598DC0F4285B8FC3F62A5E92C6F92D6195C9FD316599CD0135699DD1063A6EA2),
    .INIT_47(256'hF0235689BCEF225588BCEF225588BBEF225589BCEF235689BDF024578BBEF225),
    .INIT_48(256'h9CCF01336698CBFD2F6294C7F92C5E91C4F6295C8EC1F426598CBFF225578ABD),
    .INIT_49(256'h5E90C2F3255788BAEC1E4F81B3E517497BADDF114375A7D90B3D6FA1D406386A),
    .INIT_4A(256'h366798C9FA2B5C8DBEEF205183B4E5164779AADB0D3E6FA1D204356798CAFB2D),
    .INIT_4B(256'h245485B5E5164676A7D707386899C9FA2A5B8CBCED1D4E7FB0E0114273A3D405),
    .INIT_4C(256'h295988B8E7174676A6D505356594C4F4245484B3E3134373A3D303336494C4F4),
    .INIT_4D(256'h4574A3D1002F5E8DBCEB1A4978A7D605346392C2F1204F7FAEDD0D3C6B9BCAFA),
    .INIT_4E(256'h79A7D503315F8DBBE9184674A2D1FF2D5C8AB9E7164473A1D0FE2D5C8AB9E816),
    .INIT_4F(256'hC4F11E4C79A7D4012F5C8AB7E512406E9BC9F7245280ADDB09376593C1EF1D4A),
    .INIT_50(256'h275481ADDA0633608CB9E6123F6C99C6F3204C79A6D3002D5A88B5E20F3C6997),
    .INIT_51(256'hA4CFFB27537FABD6022E5A86B2DE0A36638FBBE7133F6C98C4F11D4976A2CFFB),
    .INIT_52(256'h39648FBAE5103B6691BCE8133E6995C0EB17426D99C4F01B47729EC9F5214C78),
    .INIT_53(256'hE8123C6690BBE50F3A648EB9E30E38638DB8E20D37628DB8E20D38638DB8E30E),
    .INIT_54(256'hB0D9032C557FA8D2FB254F78A2CBF51F49729CC6F01A436D97C1EB153F6993BD),
    .INIT_55(256'h92BBE30C345D86AED70029517AA3CCF51E477099C2EB143D668FB8E20B345D87),
    .INIT_56(256'h8FB7DE062E567EA5CDF51D456D95BDE50D355E86AED6FE274F77A0C8F019416A),
    .INIT_57(256'hA6CDF41B426990B7DE052C537AA2C9F0173F668DB5DC032B527AA1C9F0184067),
    .INIT_58(256'hD9FF254B7197BDE30A30567CA3C9EF163C6389B0D6FD234A7097BEE40B32597F),
    .INIT_59(256'h274C7196BBE1062B50769BC1E60C31577CA2C7ED13385E84AACFF51B41678DB3),
    .INIT_5A(256'h90B4D9FD21466A8EB3D7FC21456A8EB3D8FC21466B90B4D9FE23486D92B7DC01),
    .INIT_5B(256'h15395C80A3C7EA0E3155799CC0E4072B4F7397BBDF03274B6F93B7DBFF23476C),
    .INIT_5C(256'hB7D9FC1F416486A9CCEF1134577A9DC0E305284B6F92B5D8FB1E416588ABCFF2),
    .INIT_5D(256'h7597B8DAFC1D3F6183A5C6E80A2C4E7092B4D7F91B3D5F81A4C6E80B2D4F7294),
    .INIT_5E(256'h507191B2D3F41536567798B9DAFB1D3E5F80A1C2E4052648698AACCDEF103253),
    .INIT_5F(256'h486888A7C7E70727476787A7C7E80828486989A9C9EA0A2B4B6C8CADCDEE0F2F),
    .INIT_60(256'h5D7C9BBAD9F81736557493B3D2F110304F6E8EADCCEC0B2B4A6A8AA9C9E90828),
    .INIT_61(256'h90AECCEA08264463819FBDDBFA1836557391B0CEED0B2A496786A5C3E201203E),
    .INIT_62(256'hE1FE1B38557290ADCAE705223F5D7A97B5D2F00D2B496684A2BFDDFB19375472),
    .INIT_63(256'h506C88A4C0DDF915314E6A86A3BFDCF815314E6A87A4C0DDFA1733506D8AA7C4),
    .INIT_64(256'hDDF8132F4A65809CB7D2EE0925405C7793AECAE6011D3955708CA8C4E0FC1834),
    .INIT_65(256'h89A3BDD7F20C26415B7590AAC5DFFA142F4A647F9AB5CFEA05203B56718CA7C2),
    .INIT_66(256'h536D869FB8D2EB041E37516A849DB7D1EA041E37516B859FB9D2EC06203A556F),
    .INIT_67(256'h3D556D869EB6CFE700183149627A93ABC4DDF60E274059728BA4BDD6EF08213A),
    .INIT_68(256'h455D748BA3BAD1E900182F475F768EA6BDD5ED051C344C647C94ACC4DCF40C25),
    .INIT_69(256'h6E849AB0C7DDF40A21374E647B91A8BFD6EC031A31485F768DA4BBD2E900172E),
    .INIT_6A(256'hB5CAE0F50A20354B60768BA1B7CCE2F80D23394F657B90A6BCD2E9FF152B4157),
    .INIT_6B(256'h1C3145596E8297ABC0D4E9FD12273B50657A8EA3B8CDE2F70C21364B60758BA0),
    .INIT_6C(256'hA4B7CADEF104182B3F52667A8DA1B5C8DCF004182B3F53677B8FA3B7CCE0F408),
    .INIT_6D(256'h4B5D708294A7B9CCDEF10316293B4E61738699ACBFD2E5F80B1E3144576A7D90),
    .INIT_6E(256'h1324354658697B8C9EAFC1D2E4F607192B3D4E60728496A8BACCDEF002142739),
    .INIT_6F(256'hFA0B1B2B3C4C5D6D7D8E9FAFC0D0E1F20213243546576879899BACBDCEDFF001),
    .INIT_70(256'h03122131404F5F6E7E8D9DACBCCCDBEBFB0A1A2A3A4A5A6A7A8A9AAABACADAEA),
    .INIT_71(256'h2C3A4857657382909FADBCCAD9E7F605132231404E5D6C7B8A99A8B7C6D5E4F4),
    .INIT_72(256'h7683909DABB8C5D3E0EEFB091624313F4D5A687684929FADBBC9D7E5F301101E),
    .INIT_73(256'hE0EDF905111E2A36434F5C6875818E9AA7B4C0CDDAE7F4000D1A2734414E5B69),
    .INIT_74(256'h6C77828E99A4AFBBC6D1DDE8F4FF0B16222E3945515D6974808C98A4B0BCC8D4),
    .INIT_75(256'h19232D37414C56606A757F8A949FA9B4BEC9D4DEE9F4FE09141F2A35404B5661),
    .INIT_76(256'hE7F0F9020B141E273039434C565F68727C858F98A2ACB6BFC9D3DDE7F1FB050F),
    .INIT_77(256'hD6DEE6EEF6FE060F171F2730384149525A636B747C858E979FA8B1BAC3CCD5DE),
    .INIT_78(256'hE6EDF4FB020911181F262D353C434B525A6169707880878F979EA6AEB6BEC6CE),
    .INIT_79(256'h181E242A30363C42484F555B61686E747B81888E959BA2A9AFB6BDC4CBD1D8DF),
    .INIT_7A(256'h6B70757A7F84898E93989EA3A8ADB3B8BDC3C8CED3D9DEE4EAEFF5FB00060C12),
    .INIT_7B(256'hE0E4E7EBEFF3F7FBFF04080C1014191D21262A2E33373C41454A4E53585D6166),
    .INIT_7C(256'h76797C7E8184878A8D9093969A9DA0A3A7AAADB1B4B8BBBFC2C6C9CDD1D4D8DC),
    .INIT_7D(256'h2E2F31333537393B3C3F41434547494B4E50525457595C5E616366686B6E7073),
    .INIT_7E(256'h070708090A0B0B0C0D0E0F10111214151617181A1B1C1E1F2122242527282A2C),
    .INIT_7F(256'h0201010100000000000000000000000000000001010102020203030404050506),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized8
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hC446B48772969111EDE227C3AEFB4877096EBEE1DF5DA451478A2096FBEE0DDD),
    .INITP_01(256'hA77777728D69690EEBBEE53E2DF2227DA5BC4447B4B75DF70F146412D228235B),
    .INITP_02(256'hDE1E5111106909608D775C8B7CB487B4B84B84FB44FB04FB04B84B8787878B78),
    .INITP_03(256'h168B444272D116087913C229AEE962379443C2720E1116D60834B44C45A5CA20),
    .INITP_04(256'h6895A130B45D2F27BC0EE8841DEEF7443D2E9D845DD11634AC22D16886AC2213),
    .INITP_05(256'h2F45D0B3E6B5DB24210B3668E5EF7A710BBF127A610B18C6845D167BDE68B3DE),
    .INITP_06(256'h8F7D0999999990B9982E3DE3A17B6DBD18217B6F4217A2F453062484194C9093),
    .INITP_07(256'hD7AFA0A1433A8AE4146FAE617D0CCBD2514385F66FA16D0A285E3D9BD71E85F7),
    .INITP_08(256'h33217DE3DF42F1D7B378F428A16D0BECDF43851497A6617D0CEBEC504EA2B985),
    .INITP_09(256'h9A1745E9921265304248C1945E8BD085EDBD08317B6DBD0B8F78E8333A133333),
    .INITP_0A(256'h190B522CF79A2CF7BCD17442C631A10CBC91FBA11CBDEF4E2CD9A10849B75ACF),
    .INITP_0B(256'h8445A2D190886AC22D16886A58D117744372E97845DEEF70422EE07BC9E9745A),
    .INITP_0C(256'h1114F0F608A74B44645A5820D6D110E09C878453D88D2EEB2887913C20D1169C),
    .INITP_0D(256'h9DDDDDCA3DA3C3C3C3A43A41BE41BE45BE43A43A5BC25A7DA275DD620D212C11),
    .INITP_0E(256'hC25AC447B5882896904C51E1DF75DA5BC4447B4B7C889F68F94EFBAEE12D2D62),
    .INITP_0F(256'hC3E44F877760EFBED208A3C5144B75F70EFAED21DC25BEEB87C88F6F1112D29D),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0101010101010101010101010101010101010101010101010101010000000000),
    .INIT_06(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_07(256'h0202020202020202020202020202020202010101010101010101010101010101),
    .INIT_08(256'h0202020202020202020202020202020202020202020202020202020202020202),
    .INIT_09(256'h0303030303030303030303030303030303030303030303030302020202020202),
    .INIT_0A(256'h0404040404040404040303030303030303030303030303030303030303030303),
    .INIT_0B(256'h0404040404040404040404040404040404040404040404040404040404040404),
    .INIT_0C(256'h0505050505050505050505050505050505050505050505050505050505050505),
    .INIT_0D(256'h0606060606060606060606060606060606060606060606060606050505050505),
    .INIT_0E(256'h0707070707070707070707070707070707070707070707060606060606060606),
    .INIT_0F(256'h0808080808080808080808080808080808080808080807070707070707070707),
    .INIT_10(256'h0909090909090909090909090909090909090909090909090808080808080808),
    .INIT_11(256'h0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0909090909),
    .INIT_12(256'h0C0C0C0C0C0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0A),
    .INIT_13(256'h0D0D0D0D0D0D0D0D0D0D0D0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C),
    .INIT_14(256'h0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0D0D0D0D0D0D0D0D0D0D0D0D0D),
    .INIT_15(256'h10101010100F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0E0E0E0E0E),
    .INIT_16(256'h1111111111111111111111111111111010101010101010101010101010101010),
    .INIT_17(256'h1313131312121212121212121212121212121212121212121211111111111111),
    .INIT_18(256'h1414141414141414141414141414141413131313131313131313131313131313),
    .INIT_19(256'h1616161616161616151515151515151515151515151515151515151514141414),
    .INIT_1A(256'h1818181717171717171717171717171717171717171616161616161616161616),
    .INIT_1B(256'h1919191919191919191919191919191918181818181818181818181818181818),
    .INIT_1C(256'h1B1B1B1B1B1B1B1B1B1B1B1B1B1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1919),
    .INIT_1D(256'h1D1D1D1D1D1D1D1D1D1D1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1B1B1B1B1B),
    .INIT_1E(256'h1F1F1F1F1F1F1F1F1F1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1D1D1D1D1D1D),
    .INIT_1F(256'h212121212121212121202020202020202020202020202020201F1F1F1F1F1F1F),
    .INIT_20(256'h2323232323232323232222222222222222222222222222222221212121212121),
    .INIT_21(256'h2525252525252525252525242424242424242424242424242424232323232323),
    .INIT_22(256'h2727272727272727272727272726262626262626262626262626262625252525),
    .INIT_23(256'h2A2A292929292929292929292929292928282828282828282828282828282727),
    .INIT_24(256'h2C2C2C2C2C2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A2A2A2A2A2A2A2A2A2A2A2A2A),
    .INIT_25(256'h2E2E2E2E2E2E2E2E2E2E2D2D2D2D2D2D2D2D2D2D2D2D2D2D2C2C2C2C2C2C2C2C),
    .INIT_26(256'h3130303030303030303030303030302F2F2F2F2F2F2F2F2F2F2F2F2F2E2E2E2E),
    .INIT_27(256'h3333333333333332323232323232323232323232313131313131313131313131),
    .INIT_28(256'h3535353535353535353535353534343434343434343434343434333333333333),
    .INIT_29(256'h3838383838383837373737373737373737373737363636363636363636363636),
    .INIT_2A(256'h3B3B3A3A3A3A3A3A3A3A3A3A3A3A393939393939393939393939393838383838),
    .INIT_2B(256'h3D3D3D3D3D3D3D3D3D3D3C3C3C3C3C3C3C3C3C3C3C3C3B3B3B3B3B3B3B3B3B3B),
    .INIT_2C(256'h4040404040403F3F3F3F3F3F3F3F3F3F3F3F3E3E3E3E3E3E3E3E3E3E3E3E3D3D),
    .INIT_2D(256'h4343434242424242424242424242424141414141414141414141404040404040),
    .INIT_2E(256'h4645454545454545454545454444444444444444444444434343434343434343),
    .INIT_2F(256'h4848484848484848484847474747474747474747474646464646464646464646),
    .INIT_30(256'h4B4B4B4B4B4B4B4B4A4A4A4A4A4A4A4A4A4A4A4A494949494949494949494948),
    .INIT_31(256'h4E4E4E4E4E4E4E4E4D4D4D4D4D4D4D4D4D4D4D4C4C4C4C4C4C4C4C4C4C4B4B4B),
    .INIT_32(256'h5151515151515150505050505050505050504F4F4F4F4F4F4F4F4F4F4F4E4E4E),
    .INIT_33(256'h5454545454545453535353535353535353535252525252525252525252515151),
    .INIT_34(256'h5757575757575757565656565656565656565555555555555555555555545454),
    .INIT_35(256'h5A5A5A5A5A5A5A5A5A5959595959595959595958585858585858585858585757),
    .INIT_36(256'h5D5D5D5D5D5D5D5D5D5D5C5C5C5C5C5C5C5C5C5C5C5B5B5B5B5B5B5B5B5B5B5A),
    .INIT_37(256'h6161606060606060606060605F5F5F5F5F5F5F5F5F5F5E5E5E5E5E5E5E5E5E5E),
    .INIT_38(256'h6464646463636363636363636363626262626262626262626161616161616161),
    .INIT_39(256'h6767676767676766666666666666666666656565656565656565656464646464),
    .INIT_3A(256'h6A6A6A6A6A6A6A6A6A6A69696969696969696969686868686868686868676767),
    .INIT_3B(256'h6E6E6E6E6D6D6D6D6D6D6D6D6D6C6C6C6C6C6C6C6C6C6C6B6B6B6B6B6B6B6B6B),
    .INIT_3C(256'h71717171717171707070707070707070706F6F6F6F6F6F6F6F6F6E6E6E6E6E6E),
    .INIT_3D(256'h7575747474747474747474737373737373737373737272727272727272727171),
    .INIT_3E(256'h7878787878787777777777777777777676767676767676767675757575757575),
    .INIT_3F(256'h7C7B7B7B7B7B7B7B7B7B7B7A7A7A7A7A7A7A7A7A797979797979797979787878),
    .INIT_40(256'h7676767676767676777777777777777777787878787878787878797979797979),
    .INIT_41(256'h7272737373737373737373737474747474747474747575757575757575757676),
    .INIT_42(256'h6F6F6F6F6F6F7070707070707070707071717171717171717172727272727272),
    .INIT_43(256'h6C6C6C6C6C6C6C6C6C6C6D6D6D6D6D6D6D6D6D6E6E6E6E6E6E6E6E6E6E6F6F6F),
    .INIT_44(256'h686868696969696969696969696A6A6A6A6A6A6A6A6A6A6B6B6B6B6B6B6B6B6B),
    .INIT_45(256'h6565656565656666666666666666666667676767676767676767686868686868),
    .INIT_46(256'h6262626262626262626363636363636363636364646464646464646465656565),
    .INIT_47(256'h5E5F5F5F5F5F5F5F5F5F5F606060606060606060606161616161616161616162),
    .INIT_48(256'h5B5B5C5C5C5C5C5C5C5C5C5C5C5D5D5D5D5D5D5D5D5D5D5E5E5E5E5E5E5E5E5E),
    .INIT_49(256'h58585858595959595959595959595A5A5A5A5A5A5A5A5A5A5B5B5B5B5B5B5B5B),
    .INIT_4A(256'h5555555555565656565656565656565757575757575757575758585858585858),
    .INIT_4B(256'h5252525252535353535353535353535354545454545454545454555555555555),
    .INIT_4C(256'h4F4F4F4F4F505050505050505050505051515151515151515151525252525252),
    .INIT_4D(256'h4C4C4C4C4D4D4D4D4D4D4D4D4D4D4D4E4E4E4E4E4E4E4E4E4E4E4F4F4F4F4F4F),
    .INIT_4E(256'h4949494A4A4A4A4A4A4A4A4A4A4A4A4B4B4B4B4B4B4B4B4B4B4B4C4C4C4C4C4C),
    .INIT_4F(256'h4646474747474747474747474748484848484848484848484949494949494949),
    .INIT_50(256'h4444444444444444444444454545454545454545454546464646464646464646),
    .INIT_51(256'h4141414141414141424242424242424242424242434343434343434343434343),
    .INIT_52(256'h3E3E3E3E3E3F3F3F3F3F3F3F3F3F3F3F3F404040404040404040404040414141),
    .INIT_53(256'h3B3C3C3C3C3C3C3C3C3C3C3C3C3D3D3D3D3D3D3D3D3D3D3D3D3E3E3E3E3E3E3E),
    .INIT_54(256'h3939393939393939393A3A3A3A3A3A3A3A3A3A3A3A3B3B3B3B3B3B3B3B3B3B3B),
    .INIT_55(256'h3636363737373737373737373737373738383838383838383838383839393939),
    .INIT_56(256'h3434343434343434343435353535353535353535353535363636363636363636),
    .INIT_57(256'h3131313232323232323232323232323233333333333333333333333333343434),
    .INIT_58(256'h2F2F2F2F2F2F2F2F303030303030303030303030303031313131313131313131),
    .INIT_59(256'h2D2D2D2D2D2D2D2D2D2D2D2D2D2E2E2E2E2E2E2E2E2E2E2E2E2E2E2F2F2F2F2F),
    .INIT_5A(256'h2A2A2A2A2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C2C2C2C2C2C2C2C2C2C2C2C2C2D),
    .INIT_5B(256'h2828282828282829292929292929292929292929292A2A2A2A2A2A2A2A2A2A2A),
    .INIT_5C(256'h2626262626262626262627272727272727272727272727272728282828282828),
    .INIT_5D(256'h2424242424242424242424242525252525252525252525252525252626262626),
    .INIT_5E(256'h2222222222222222222222222222232323232323232323232323232323242424),
    .INIT_5F(256'h2020202020202020202020202020212121212121212121212121212121212222),
    .INIT_60(256'h1E1E1E1E1E1E1E1E1E1E1E1E1E1E1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F2020),
    .INIT_61(256'h1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1D1E1E1E),
    .INIT_62(256'h1A1A1A1A1A1A1A1A1A1A1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1C1C1C1C),
    .INIT_63(256'h181818181818181919191919191919191919191919191919191A1A1A1A1A1A1A),
    .INIT_64(256'h1616171717171717171717171717171717171717181818181818181818181818),
    .INIT_65(256'h1515151515151515151515151515151616161616161616161616161616161616),
    .INIT_66(256'h1313131313131314141414141414141414141414141414141414141515151515),
    .INIT_67(256'h1212121212121212121212121212121212121213131313131313131313131313),
    .INIT_68(256'h1010101010101010111111111111111111111111111111111111111111111212),
    .INIT_69(256'h0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F1010101010101010101010101010),
    .INIT_6A(256'h0D0D0D0D0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0F0F0F0F),
    .INIT_6B(256'h0C0C0C0C0C0C0C0C0C0C0C0C0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D),
    .INIT_6C(256'h0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C),
    .INIT_6D(256'h0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0B0B0B0B0B0B0B0B),
    .INIT_6E(256'h090909090909090909090909090909090909090909090909090909090A0A0A0A),
    .INIT_6F(256'h0708080808080808080808080808080808080808080808080808080808080809),
    .INIT_70(256'h0707070707070707070707070707070707070707070707070707070707070707),
    .INIT_71(256'h0606060606060606060606060606060606060606060606060606060606060606),
    .INIT_72(256'h0505050505050505050505050505050505050505050505050505050505060606),
    .INIT_73(256'h0404040404040404040404040404040404040404040404050505050505050505),
    .INIT_74(256'h0303030303030303030303030303040404040404040404040404040404040404),
    .INIT_75(256'h0303030303030303030303030303030303030303030303030303030303030303),
    .INIT_76(256'h0202020202020202020202020202020202020202020202020202020202020303),
    .INIT_77(256'h0101010101010202020202020202020202020202020202020202020202020202),
    .INIT_78(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_79(256'h0101010101010101010101010101010101010101010101010101010101010101),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000001010101),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module cosine_rom_blk_mem_gen_prim_wrapper_init__parameterized9
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [8:0]douta;
  output [8:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [8:0]douta;
  wire [8:0]doutb;
  wire sleep;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("NONE"),
    .CASCADE_ORDER_B("NONE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(1),
    .DOB_REG(1),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hC3C3878783C3C1E0F83E0F81F03F03F80FE03FE01FF801FFE0007FFFE0000000),
    .INITP_01(256'h999933333333319998CCC663319CE6318C6318E718E71C718E38F1C71E3870E1),
    .INITP_02(256'hD29694B4B4B49692D25B6925B6DB6DB6DB6D924DB64DB26C9B364C9933664CC9),
    .INITP_03(256'h555555555555555AAAAA95556AAB556AA556A956A952A54AD5A94AD6A5294A5A),
    .INITP_04(256'hD2D694B5AD6B5AD4A56A56A56AD5AB56A954AA552AA554AAA5555AAAAA955555),
    .INITP_05(256'hC993264D9366D9364DB24DB64926DB6DB6DB6DB6924B6D25B49692D2DA5A5A52),
    .INITP_06(256'hE739CE7398C67319CC6633199CCCE6667333333333333333366664CCD99B3264),
    .INITP_07(256'h3C3C3C3C3C387870E1E3C70E1C78E1C71E38E38E38E38E39C71CE39C639C631C),
    .INITP_08(256'h9CE739CE718C738C738E71C738E38E38E38E38F1C70E3C70E1C78F0E1C3C3878),
    .INITP_09(256'h64C993264C99B336664CCCD9999999999999999CCCCE66733198CC67319CC633),
    .INITP_0A(256'h5A52D69694B4B4B69692D25B496DA492DB6DB6DB6DB6C924DB649B64D936CD93),
    .INITP_0B(256'h55555555555552AAAAB5554AAA554AA954AA552AD5AB56AD4AD4AD4A56B5AD6B),
    .INITP_0C(256'h5A52D296B4A5294AD6A52B56A54A952AD52AD54AAD55AAAD5552AAAAB5555555),
    .INITP_0D(256'h999933332664CD993264D9B26C9B64DB64936DB6DB6DB6DB492DB49692D25A5A),
    .INITP_0E(256'hC3C387870E1C38F1C71E38E31C71CE31CE318C6318CE73198CC6663333199999),
    .INITP_0F(256'h000000000000000FFFFC000FFF003FF00FF80FE03F81F81F03E0F83E0F078783),
    .INIT_00(256'h211509FDF2E7DCD1C7BDB3AAA0978E867E766E675F58524B453F3A342F2A2621),
    .INIT_01(256'h321D09F5E1CDBAA79482705E4C3B291908F8E7D8C8B9AA9B8C7E706255473A2E),
    .INIT_02(256'h4E3115F8DCC0A4896E53391E04EAD1B79E866D553D250EF7E0C9B39C87715C46),
    .INIT_03(256'h77522D08E3BF9B7754300DEBC8A684634120FFDEBE9E7E5F3F2001E3C5A7896B),
    .INIT_04(256'hAB7E5023F6C99D714519EEC3986E4319F0C69D744B23FBD3AB835C350FE8C29C),
    .INIT_05(256'hEBB57F4A14DFAB76420EDAA774410EDCAA784615E4B3825222F2C394653607D9),
    .INIT_06(256'h36F7B97B3E01C3874A0ED2965B1FE4AA6F35FBC1884F16DDA56D35FDC68F5821),
    .INIT_07(256'h8B44FEB8722CE7A25D18D4904C08C5823FFDBA7837F5B47332F2B27232F2B374),
    .INIT_08(256'hEA9B4DFEB06214C77A2DE09447FCB0651ACF843AF0A65C13CA8139F0A86119D2),
    .INIT_09(256'h53FCA54EF7A14BF5A04BF6A14DF8A551FEAA5705B3600FBD6C1BCA7929D9893A),
    .INIT_0A(256'hC46505A648E98B2DCF7214B75BFEA246EB8F34D97E24CA7016BD640BB35A02AA),
    .INIT_0B(256'h3DD66E07A039D36C06A13BD6710CA844E07C19B653F08E2CCA6807A645E48424),
    .INIT_0C(256'hBE4EDE6FFF9022B345D769FC8F22B549DD71059A2FC459EF851BB148DF760EA5),
    .INIT_0D(256'h44CC54DD65EE77008A149E28B33EC955E06CF885129E2CB947D563F1800F9E2E),
    .INIT_0E(256'hD050D050D051D253D556D85BDD60E366EA6DF176FA7F04890F951BA127AE35BD),
    .INIT_0F(256'h60D84FC840B831AA249D17910C86017CF873EF6BE864E15EDC59D755D453D151),
    .INIT_10(256'hF363D343B323940576E85ACC3EB02396097DF165D94EC237AD22980E84FB72E9),
    .INIT_11(256'h89F058C02890F962CB359E0872DD48B31E89F561CD3AA61380EE5CCA38A61584),
    .INIT_12(256'h1F7EDE3E9EFE5FC02182E446A80B6DD03497FB5FC3278CF156BB2187ED54BA21),
    .INIT_13(256'hB40C63BB136CC51E77D02A84DE3993EE49A5015DB91572CF2C89E745A30260BF),
    .INIT_14(256'h4897E73787D8297ACB1D6EC01365B80B5EB2065AAE0257AC0157AC0259AF065D),
    .INIT_15(256'hD82068B0F8418AD31D66B0FA458FDA2571BC0854A0ED3A87D42270BE0C5BA9F8),
    .INIT_16(256'h64A4E42465A6E7286AABED3072B5F83B7FC3074B8FD4195EA4E92F76BC034A91),
    .INIT_17(256'hE9215A92CB043D77B1EB255F9AD5104C88C3003C79B6F3306EACEA2867A6E524),
    .INIT_18(256'h6797C8F92A5B8DBEF0235588BBEE225589BDF2275B91C6FC32689ED50C437AB2),
    .INIT_19(256'hDB042D567FA9D3FD27527CA7D3FE2A5682AFDB08366391BFED1B4A79A8D70737),
    .INIT_1A(256'h446586A8C9EB0D30537699BCE004284C7196BBE0062B51789EC5EC133B628AB3),
    .INIT_1B(256'h9FB9D3EC07213C57728DA9C5E1FD193653708EACCAE80625446382A2C2E20223),
    .INIT_1C(256'hECFE102335485B6F8296AABFD3E8FD12283D536A8097ADC5DCF40B233C546D86),
    .INIT_1D(256'h28323D48535F6B76838F9CA8B6C3D0DEECFA09182736455565758596A6B8C9DA),
    .INIT_1E(256'h5154575B5F63676C70757B80868C92989FA6ADB4BCC3CBD4DCE5EEF7000A141E),
    .INIT_1F(256'h65615D5956524F4D4A48464442413F3E3E3D3D3D3D3D3E3F4041434547494B4E),
    .INIT_20(256'h62574C41362B21170D04FAF1E8DFD7CFC7BFB7B0A9A29B958F89837D78736E69),
    .INIT_21(256'h4634210FFDEBDAC9B8A796867666564737281A0BFDEFE1D3C6B8AB9F92867A6E),
    .INIT_22(256'h0FF5DCC2A990785F472F1800E9D2BBA48E78624C37210CF8E3CFBAA6937F6C59),
    .INIT_23(256'hBA9978583818F9D9BA9B7C5E3F2103E6C8AB8E7154381C00E4C9AE93785D4329),
    .INIT_24(256'h451DF6CEA7805A330DE7C19C76512C08E3BF9B7754300DEAC8A583613F1DFCDB),
    .INIT_25(256'hAD7F5022F4C6996C3F12E5B98C603409DDB2875D3208DEB48A61380FE6BD956D),
    .INIT_26(256'hF1BC86511DE8B4804C18E5B17E4B19E6B482501FEDBC8B5A2AFAC99A6A3A0BDC),
    .INIT_27(256'h0ED1955A1EE3A86D32F8BE834A10D79D642CF3BB834B13DBA46D36FFC9935D27),
    .INIT_28(256'h00BD7B38F6B47231F0AE6D2DECAC6C2CECAD6D2EF0B17234F6B87B3D00C3864A),
    .INIT_29(256'hC67D34EBA25911C98139F2AA631CD68F4903BD7732ECA7631ED995510DCA8643),
    .INIT_2A(256'h5D0DBD6E1FD08132E49648FAAC5F11C4782BDF9246FBAF6418CD8338EEA35910),
    .INIT_2B(256'hC26C16C06A15BF6A15C16C18C4701CC97623D07D2BD98635E39140EF9E4EFDAD),
    .INIT_2C(256'hF29539DD8125CA6E13B85D03A84EF49A41E78E35DC842BD37B23CB741DC66F18),
    .INIT_2D(256'hEB8825C361FF9D3BDA7918B756F69535D67616B758F99A3CDE7F21C46609AC4F),
    .INIT_2E(256'hA940D76F079E36CF67009831CB64FE9731CB66009B36D16C08A43FDC7814B14E),
    .INIT_2F(256'h2ABB4CDE6F019325B84ADD7003962ABD51E57A0EA338CD62F78D22B84FE57B12),
    .INIT_30(256'h6BF6810C9824B03CC955E26FFC8917A532C14FDD6CFB8A19A838C857E8780899),
    .INIT_31(256'h68ED73F87E048A10971EA52CB33AC24AD25AE26BF47C068F18A22CB640CA55E0),
    .INIT_32(256'h1F9E1E9E1E9E1E9F20A122A325A628AA2CAF31B437BA3DC144C84CD055D95EE3),
    .INIT_33(256'h8C0680FA74EF6AE560DB56D24ECA46C23FBB38B532B02DAB29A725A422A1209F),
    .INIT_34(256'hAC21950A7FF469DE54C93FB52BA2188F067DF46CE35BD34BC33CB42DA61F9912),
    .INIT_35(256'h7DEC5BCA39A91989F969D94ABA2B9C0E7FF162D446B82B9D1083F66ADD51C438),
    .INIT_36(256'hFA64CE37A10C76E04BB6218CF763CF3AA6137FEB58C5329F0C7AE755C332A00E),
    .INIT_37(256'h2186EA4FB3187DE248AD1379DF45AC1279E047AE157DE54CB41D85ED56BF2891),
    .INIT_38(256'hEE4DAD0C6CCB2B8BEC4CAD0D6ECF3092F355B7197BDD40A20568CB2E92F559BD),
    .INIT_39(256'h5EB8126DC7227DD8338EEA45A1FD59B6126FCB2885E2409DFB59B71573D2308F),
    .INIT_3A(256'h6DC2176DC2186EC41A71C71E75CC237AD22981D93189E23A93EC459EF751AA04),
    .INIT_3B(256'h1868B9095AABFC4D9FF04294E6388ADC2F82D5287BCE2275C91D71C51A6EC318),
    .INIT_3C(256'h5BA6F23E8AD6236FBC0956A3F03D8BD92674C3115FAEFC4B9AE93988D82877C8),
    .INIT_3D(256'h337AC1085097DF276FB7FF4890D9226BB4FE4791DA246EB9034D98E32E79C40F),
    .INIT_3E(256'h9CDE2164A7EA2D70B4F83B7FC3084C91D51A5FA4E92F74BAFF458BD2185EA5EC),
    .INIT_3F(256'h92D00F4D8CCA094887C7064685C5054585C6064788C90A4B8CCE0F5193D51759),
    .INIT_40(256'hA55E18D28B45FFBA742FE9A45F1AD5914C08C37F3BF8B4702DEAA76421DE9C59),
    .INIT_41(256'hC4792EE3984D03B96E24DA9147FEB46B22D99048FFB76F27DF975008C17A33EC),
    .INIT_42(256'h7728D88839E99A4BFCAE5F11C37426D98B3DF0A35609BC6F23D68A3EF2A65B0F),
    .INIT_43(256'hC36E1AC5711DC97522CE7B28D5822FDC8A38E69442F09F4DFCAB5A09B96818C8),
    .INIT_44(256'hAA51F79E45EC933AE28931D98129D27A23CC751EC7711AC46E18C26D17C26D18),
    .INIT_45(256'h30D27315B759FB9D40E28528CB6F12B659FDA145EA8E33D87D22C76D12B85E04),
    .INIT_46(256'h59F5922ECB6805A240DD7B19B755F39230CF6E0DAD4CEC8B2BCB6C0CAD4DEE8F),
    .INIT_47(256'h28BF56ED851DB44CE57D15AE47E07912AC45DF7913AD48E27D18B34FEA8621BD),
    .INIT_48(256'hA032C355E77A0C9F32C558EB7F13A63ACF63F78C21B64BE0760CA137CE64FA91),
    .INIT_49(256'hC451DD6AF683109D2BB846D462F17F0E9C2BBA4AD969F98919A939CA5BEC7D0E),
    .INIT_4A(256'h991FA62DB43CC34BD35BE36CF47D068F18A22BB53FC954DE69F47F0A9521AC38),
    .INIT_4B(256'h20A122A425A729AB2DB032B538BB3FC246CA4ED256DB60E56AEF74FA80068C12),
    .INIT_4C(256'h5ED955D04CC844C13DBA37B431AF2CAA28A625A322A1209F1E9E1E9E1E9E1F9F),
    .INIT_4D(256'h55CA40B62CA2188F067CF46BE25AD24AC23AB32CA51E97108A047EF873ED68E3),
    .INIT_4E(256'h0878E857C838A8198AFB6CDD4FC132A51789FC6FE255C93CB024980C81F66BE0),
    .INIT_4F(256'h7BE54FB8228DF762CD38A30E7AE551BD2A960370DD4AB82593016FDE4CBB2A99),
    .INIT_50(256'hB11478DC3FA4086CD1369B0066CB3197FE64CB31980067CF369E076FD740A912),
    .INIT_51(256'hAC0966C4217FDE3C9AF958B71676D63595F656B71879DA3B9DFF61C32588EB4E),
    .INIT_52(256'h6FC61D74CB237BD32B84DC358EE7419AF44EA8035DB8136ECA2581DD3995F24F),
    .INIT_53(256'hFD4E9EEF4091E33586D92B7DD02376C91C70C4186CC1156ABF156AC0166CC218),
    .INIT_54(256'h59A3EE3883CD1864AFFB4692DF2B78C4115FACFA4896E43281D01F6EBD0D5DAD),
    .INIT_55(256'h86CA0D5195D91E63A7EC3277BD03498FD61C63AAF23981C91159A2EB347DC610),
    .INIT_56(256'h86C3003D7BB8F63472B1F02E6DADEC2C6CACEC2D6DAEF03172B4F6387BBD0043),
    .INIT_57(256'h5D93C9FF366DA4DB134B83BBF32C649DD7104A83BEF8326DA8E31E5A95D10E4A),
    .INIT_58(256'h0B3A6A9AC9FA2A5A8BBCED1F5082B4E6194B7EB1E5184C80B4E81D5186BCF127),
    .INIT_59(256'h95BDE60F38618AB4DE08325D87B2DD0934608CB9E5123F6C99C6F422507FADDC),
    .INIT_5A(256'hFC1D3F6183A5C8EA0D3054779BBFE3082C51769CC1E70D335A80A7CEF61D456D),
    .INIT_5B(256'h435D7893AEC9E4001C3854718EABC8E603213F5E7C9BBAD9F91838587899BADB),
    .INIT_5C(256'h6C7F93A6BACFE3F80C21374C62788EA4BBD2E900182F475F7890A9C2DCF50F29),
    .INIT_5D(256'h7A86929FABB8C6D3E1EFFD0B1A2837475666768696A7B8C9DAEBFD0F21344659),
    .INIT_5E(256'h6E73787D83898F959BA2A9B0B7BFC7CFD7DFE8F1FA040D17212B36414C57626E),
    .INIT_5F(256'h4B4947454341403F3E3D3D3D3D3D3E3E3F41424446484A4D4F5256595D616569),
    .INIT_60(256'h140A00F7EEE5DCD4CBC3BCB4ADA69F98928C86807B75706C67635F5B5754514E),
    .INIT_61(256'hC9B8A696857565554536271809FAECDED0C3B6A89C8F83766B5F53483D32281E),
    .INIT_62(256'h6D543C230BF4DCC5AD97806A533D2812FDE8D3BFAA96826F5B48352310FEECDA),
    .INIT_63(256'h02E2C2A28263442506E8CAAC8E70533619FDE1C5A98D72573C2107ECD3B99F86),
    .INIT_64(256'h8A623B13ECC59E78512B06E0BB96714C2804E0BC997653300DEBC9A886654423),
    .INIT_65(256'h07D7A8794A1BEDBF91633608DBAF82562AFED3A77C5227FDD3A97F562D04DBB3),
    .INIT_66(256'h7A430CD59E6832FCC6915B27F2BD895522EEBB885523F0BE8D5B2AF9C8976737),
    .INIT_67(256'hE5A66728EAAC6E30F3B6793C00C3884C10D59A5F25EBB1773D04CB925A21E9B2),
    .INIT_68(256'h4A03BC762FE9A45E19D48F4B07C37F3BF8B57230EDAB6A28E7A66524E4A46424),
    .INIT_69(256'hA95B0CBE7022D4873AEDA05408BC7125DA8F45FAB0661DD38A41F8B06820D891),
    .INIT_6A(256'h06AF5902AC5701AC5702AE5A06B25E0BB86513C06E1DCB7A29D88737E79748F8),
    .INIT_6B(256'h6002A345E7892CCF7215B95D01A549EE9339DE842AD0771EC56C13BB630CB45D),
    .INIT_6C(256'hBA54ED8721BB56F18C27C35FFB9734D06D0BA846E48221C05FFE9E3EDE7E1FBF),
    .INIT_6D(256'h15A638CA5CEE8013A63ACD61F5891EB348DD72089E35CB62F99028C058F08921),
    .INIT_6E(256'h72FB840E9822AD37C24ED965F17D099623B03ECC5AE876059423B343D363F384),
    .INIT_6F(256'hD153D455D759DC5EE164E86BEF73F87C01860C91179D24AA31B840C84FD860E9),
    .INIT_70(256'h35AE27A11B950F89047FFA76F16DEA66E360DD5BD856D553D251D050D050D051),
    .INIT_71(256'h9E0F80F163D547B92C9E1285F86CE055C93EB3289E148A0077EE65DD54CC44BD),
    .INIT_72(256'h0E76DF48B11B85EF59C42F9A0571DD49B5228FFC69D745B32290FF6FDE4EBE2E),
    .INIT_73(256'h84E445A60768CA2C8EF053B6197CE044A80C71D63BA1066CD339A0076ED63DA5),
    .INIT_74(256'h025AB30B64BD1670CA247ED9348FEB46A2FE5BB71472CF2D8BE948A60565C424),
    .INIT_75(256'h89D92979CA1B6CBD0F60B30557AAFE51A5F84DA1F64BA0F54BA1F74EA5FC53AA),
    .INIT_76(256'h1961A8F03981CA135CA6F03A84CF1A65B0FC4794E02D7AC71462B0FE4D9BEA3A),
    .INIT_77(256'hB3F23272B2F23273B4F53778BAFD3F82C5084C90D4185DA2E72C72B8FE448BD2),
    .INIT_78(256'h588FC6FD356DA5DD164F88C1FB356FAAE41F5B96D20E4A87C3013E7BB9F73674),
    .INIT_79(256'h07366594C3F2225282B3E4154678AADC0E4174A7DA0E4276ABDF144A7FB5EB21),
    .INIT_7A(256'hC2E80F355C83ABD3FB234B749DC6F019436E98C3EE1945719DC9F623507EABD9),
    .INIT_7B(256'h89A7C5E301203F5F7E9EBEDEFF20416384A6C8EB0D3054779BBFE3082D52779C),
    .INIT_7C(256'h5C71879CB3C9E0F70E253D556D869EB7D1EA041E39536E89A4C0DCF815314E6B),
    .INIT_7D(256'h3A475562707E8C9BAAB9C8D8E7F80819293B4C5E708294A7BACDE1F5091D3246),
    .INIT_7E(256'h262A2F343A3F454B52585F676E767E868E97A0AAB3BDC7D1DCE7F2FD0915212E),
    .INIT_7F(256'h1D191613100D0A08060403020100000000000102030406080A0D101316191D21),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({addra,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({addrb,1'b0,1'b0,1'b0}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b0),
        .CASDOMUXEN_B(1'b0),
        .CASDOUTA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTA_UNCONNECTED [31:0]),
        .CASDOUTB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTB_UNCONNECTED [31:0]),
        .CASDOUTPA(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPA_UNCONNECTED [3:0]),
        .CASDOUTPB(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASDOUTPB_UNCONNECTED [3:0]),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b0),
        .CASOREGIMUXEN_B(1'b0),
        .CASOUTDBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTDBITERR_UNCONNECTED ),
        .CASOUTSBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASOUTSBITERR_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clkb),
        .DBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTADOUT_UNCONNECTED [31:8],douta[7:0]}),
        .DOUTBDOUT({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTBDOUT_UNCONNECTED [31:8],doutb[7:0]}),
        .DOUTPADOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPADOUTP_UNCONNECTED [3:1],douta[8]}),
        .DOUTPBDOUTP({\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOUTPBDOUTP_UNCONNECTED [3:1],doutb[8]}),
        .ECCPARITY(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ECCPIPECE(1'b0),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_8SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .SLEEP(sleep),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_top" *) 
module cosine_rom_blk_mem_gen_top
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [335:0]douta;
  output [335:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [335:0]douta;
  wire [335:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_generic_cstr \valid.cstr 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* C_ADDRA_WIDTH = "12" *) (* C_ADDRB_WIDTH = "12" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "0" *) (* C_COUNT_18K_BRAM = "1" *) 
(* C_COUNT_36K_BRAM = "37" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     122.949432 mW" *) 
(* C_FAMILY = "kintexuplus" *) (* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "0" *) 
(* C_HAS_ENB = "0" *) (* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
(* C_HAS_MEM_OUTPUT_REGS_B = "1" *) (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
(* C_HAS_REGCEA = "0" *) (* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) 
(* C_HAS_RSTB = "0" *) (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
(* C_INITA_VAL = "0" *) (* C_INITB_VAL = "0" *) (* C_INIT_FILE = "cosine_rom.mem" *) 
(* C_INIT_FILE_NAME = "cosine_rom.mif" *) (* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "1" *) 
(* C_MEM_TYPE = "4" *) (* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) 
(* C_READ_DEPTH_A = "4096" *) (* C_READ_DEPTH_B = "4096" *) (* C_READ_WIDTH_A = "336" *) 
(* C_READ_WIDTH_B = "336" *) (* C_RSTRAM_A = "0" *) (* C_RSTRAM_B = "0" *) 
(* C_RST_PRIORITY_A = "CE" *) (* C_RST_PRIORITY_B = "CE" *) (* C_SIM_COLLISION_CHECK = "ALL" *) 
(* C_USE_BRAM_BLOCK = "0" *) (* C_USE_BYTE_WEA = "0" *) (* C_USE_BYTE_WEB = "0" *) 
(* C_USE_DEFAULT_DATA = "0" *) (* C_USE_ECC = "0" *) (* C_USE_SOFTECC = "0" *) 
(* C_USE_URAM = "0" *) (* C_WEA_WIDTH = "1" *) (* C_WEB_WIDTH = "1" *) 
(* C_WRITE_DEPTH_A = "4096" *) (* C_WRITE_DEPTH_B = "4096" *) (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
(* C_WRITE_MODE_B = "WRITE_FIRST" *) (* C_WRITE_WIDTH_A = "336" *) (* C_WRITE_WIDTH_B = "336" *) 
(* C_XDEVICEFAMILY = "kintexuplus" *) (* ORIG_REF_NAME = "blk_mem_gen_v8_4_1" *) (* downgradeipidentifiedwarnings = "yes" *) 
module cosine_rom_blk_mem_gen_v8_4_1
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [11:0]addra;
  input [335:0]dina;
  output [335:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [11:0]addrb;
  input [335:0]dinb;
  output [335:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [11:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [335:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [335:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [11:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [335:0]douta;
  wire [335:0]doutb;
  wire sleep;

  assign dbiterr = \<const0> ;
  assign rdaddrecc[11] = \<const0> ;
  assign rdaddrecc[10] = \<const0> ;
  assign rdaddrecc[9] = \<const0> ;
  assign rdaddrecc[8] = \<const0> ;
  assign rdaddrecc[7] = \<const0> ;
  assign rdaddrecc[6] = \<const0> ;
  assign rdaddrecc[5] = \<const0> ;
  assign rdaddrecc[4] = \<const0> ;
  assign rdaddrecc[3] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[11] = \<const0> ;
  assign s_axi_rdaddrecc[10] = \<const0> ;
  assign s_axi_rdaddrecc[9] = \<const0> ;
  assign s_axi_rdaddrecc[8] = \<const0> ;
  assign s_axi_rdaddrecc[7] = \<const0> ;
  assign s_axi_rdaddrecc[6] = \<const0> ;
  assign s_axi_rdaddrecc[5] = \<const0> ;
  assign s_axi_rdaddrecc[4] = \<const0> ;
  assign s_axi_rdaddrecc[3] = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[335] = \<const0> ;
  assign s_axi_rdata[334] = \<const0> ;
  assign s_axi_rdata[333] = \<const0> ;
  assign s_axi_rdata[332] = \<const0> ;
  assign s_axi_rdata[331] = \<const0> ;
  assign s_axi_rdata[330] = \<const0> ;
  assign s_axi_rdata[329] = \<const0> ;
  assign s_axi_rdata[328] = \<const0> ;
  assign s_axi_rdata[327] = \<const0> ;
  assign s_axi_rdata[326] = \<const0> ;
  assign s_axi_rdata[325] = \<const0> ;
  assign s_axi_rdata[324] = \<const0> ;
  assign s_axi_rdata[323] = \<const0> ;
  assign s_axi_rdata[322] = \<const0> ;
  assign s_axi_rdata[321] = \<const0> ;
  assign s_axi_rdata[320] = \<const0> ;
  assign s_axi_rdata[319] = \<const0> ;
  assign s_axi_rdata[318] = \<const0> ;
  assign s_axi_rdata[317] = \<const0> ;
  assign s_axi_rdata[316] = \<const0> ;
  assign s_axi_rdata[315] = \<const0> ;
  assign s_axi_rdata[314] = \<const0> ;
  assign s_axi_rdata[313] = \<const0> ;
  assign s_axi_rdata[312] = \<const0> ;
  assign s_axi_rdata[311] = \<const0> ;
  assign s_axi_rdata[310] = \<const0> ;
  assign s_axi_rdata[309] = \<const0> ;
  assign s_axi_rdata[308] = \<const0> ;
  assign s_axi_rdata[307] = \<const0> ;
  assign s_axi_rdata[306] = \<const0> ;
  assign s_axi_rdata[305] = \<const0> ;
  assign s_axi_rdata[304] = \<const0> ;
  assign s_axi_rdata[303] = \<const0> ;
  assign s_axi_rdata[302] = \<const0> ;
  assign s_axi_rdata[301] = \<const0> ;
  assign s_axi_rdata[300] = \<const0> ;
  assign s_axi_rdata[299] = \<const0> ;
  assign s_axi_rdata[298] = \<const0> ;
  assign s_axi_rdata[297] = \<const0> ;
  assign s_axi_rdata[296] = \<const0> ;
  assign s_axi_rdata[295] = \<const0> ;
  assign s_axi_rdata[294] = \<const0> ;
  assign s_axi_rdata[293] = \<const0> ;
  assign s_axi_rdata[292] = \<const0> ;
  assign s_axi_rdata[291] = \<const0> ;
  assign s_axi_rdata[290] = \<const0> ;
  assign s_axi_rdata[289] = \<const0> ;
  assign s_axi_rdata[288] = \<const0> ;
  assign s_axi_rdata[287] = \<const0> ;
  assign s_axi_rdata[286] = \<const0> ;
  assign s_axi_rdata[285] = \<const0> ;
  assign s_axi_rdata[284] = \<const0> ;
  assign s_axi_rdata[283] = \<const0> ;
  assign s_axi_rdata[282] = \<const0> ;
  assign s_axi_rdata[281] = \<const0> ;
  assign s_axi_rdata[280] = \<const0> ;
  assign s_axi_rdata[279] = \<const0> ;
  assign s_axi_rdata[278] = \<const0> ;
  assign s_axi_rdata[277] = \<const0> ;
  assign s_axi_rdata[276] = \<const0> ;
  assign s_axi_rdata[275] = \<const0> ;
  assign s_axi_rdata[274] = \<const0> ;
  assign s_axi_rdata[273] = \<const0> ;
  assign s_axi_rdata[272] = \<const0> ;
  assign s_axi_rdata[271] = \<const0> ;
  assign s_axi_rdata[270] = \<const0> ;
  assign s_axi_rdata[269] = \<const0> ;
  assign s_axi_rdata[268] = \<const0> ;
  assign s_axi_rdata[267] = \<const0> ;
  assign s_axi_rdata[266] = \<const0> ;
  assign s_axi_rdata[265] = \<const0> ;
  assign s_axi_rdata[264] = \<const0> ;
  assign s_axi_rdata[263] = \<const0> ;
  assign s_axi_rdata[262] = \<const0> ;
  assign s_axi_rdata[261] = \<const0> ;
  assign s_axi_rdata[260] = \<const0> ;
  assign s_axi_rdata[259] = \<const0> ;
  assign s_axi_rdata[258] = \<const0> ;
  assign s_axi_rdata[257] = \<const0> ;
  assign s_axi_rdata[256] = \<const0> ;
  assign s_axi_rdata[255] = \<const0> ;
  assign s_axi_rdata[254] = \<const0> ;
  assign s_axi_rdata[253] = \<const0> ;
  assign s_axi_rdata[252] = \<const0> ;
  assign s_axi_rdata[251] = \<const0> ;
  assign s_axi_rdata[250] = \<const0> ;
  assign s_axi_rdata[249] = \<const0> ;
  assign s_axi_rdata[248] = \<const0> ;
  assign s_axi_rdata[247] = \<const0> ;
  assign s_axi_rdata[246] = \<const0> ;
  assign s_axi_rdata[245] = \<const0> ;
  assign s_axi_rdata[244] = \<const0> ;
  assign s_axi_rdata[243] = \<const0> ;
  assign s_axi_rdata[242] = \<const0> ;
  assign s_axi_rdata[241] = \<const0> ;
  assign s_axi_rdata[240] = \<const0> ;
  assign s_axi_rdata[239] = \<const0> ;
  assign s_axi_rdata[238] = \<const0> ;
  assign s_axi_rdata[237] = \<const0> ;
  assign s_axi_rdata[236] = \<const0> ;
  assign s_axi_rdata[235] = \<const0> ;
  assign s_axi_rdata[234] = \<const0> ;
  assign s_axi_rdata[233] = \<const0> ;
  assign s_axi_rdata[232] = \<const0> ;
  assign s_axi_rdata[231] = \<const0> ;
  assign s_axi_rdata[230] = \<const0> ;
  assign s_axi_rdata[229] = \<const0> ;
  assign s_axi_rdata[228] = \<const0> ;
  assign s_axi_rdata[227] = \<const0> ;
  assign s_axi_rdata[226] = \<const0> ;
  assign s_axi_rdata[225] = \<const0> ;
  assign s_axi_rdata[224] = \<const0> ;
  assign s_axi_rdata[223] = \<const0> ;
  assign s_axi_rdata[222] = \<const0> ;
  assign s_axi_rdata[221] = \<const0> ;
  assign s_axi_rdata[220] = \<const0> ;
  assign s_axi_rdata[219] = \<const0> ;
  assign s_axi_rdata[218] = \<const0> ;
  assign s_axi_rdata[217] = \<const0> ;
  assign s_axi_rdata[216] = \<const0> ;
  assign s_axi_rdata[215] = \<const0> ;
  assign s_axi_rdata[214] = \<const0> ;
  assign s_axi_rdata[213] = \<const0> ;
  assign s_axi_rdata[212] = \<const0> ;
  assign s_axi_rdata[211] = \<const0> ;
  assign s_axi_rdata[210] = \<const0> ;
  assign s_axi_rdata[209] = \<const0> ;
  assign s_axi_rdata[208] = \<const0> ;
  assign s_axi_rdata[207] = \<const0> ;
  assign s_axi_rdata[206] = \<const0> ;
  assign s_axi_rdata[205] = \<const0> ;
  assign s_axi_rdata[204] = \<const0> ;
  assign s_axi_rdata[203] = \<const0> ;
  assign s_axi_rdata[202] = \<const0> ;
  assign s_axi_rdata[201] = \<const0> ;
  assign s_axi_rdata[200] = \<const0> ;
  assign s_axi_rdata[199] = \<const0> ;
  assign s_axi_rdata[198] = \<const0> ;
  assign s_axi_rdata[197] = \<const0> ;
  assign s_axi_rdata[196] = \<const0> ;
  assign s_axi_rdata[195] = \<const0> ;
  assign s_axi_rdata[194] = \<const0> ;
  assign s_axi_rdata[193] = \<const0> ;
  assign s_axi_rdata[192] = \<const0> ;
  assign s_axi_rdata[191] = \<const0> ;
  assign s_axi_rdata[190] = \<const0> ;
  assign s_axi_rdata[189] = \<const0> ;
  assign s_axi_rdata[188] = \<const0> ;
  assign s_axi_rdata[187] = \<const0> ;
  assign s_axi_rdata[186] = \<const0> ;
  assign s_axi_rdata[185] = \<const0> ;
  assign s_axi_rdata[184] = \<const0> ;
  assign s_axi_rdata[183] = \<const0> ;
  assign s_axi_rdata[182] = \<const0> ;
  assign s_axi_rdata[181] = \<const0> ;
  assign s_axi_rdata[180] = \<const0> ;
  assign s_axi_rdata[179] = \<const0> ;
  assign s_axi_rdata[178] = \<const0> ;
  assign s_axi_rdata[177] = \<const0> ;
  assign s_axi_rdata[176] = \<const0> ;
  assign s_axi_rdata[175] = \<const0> ;
  assign s_axi_rdata[174] = \<const0> ;
  assign s_axi_rdata[173] = \<const0> ;
  assign s_axi_rdata[172] = \<const0> ;
  assign s_axi_rdata[171] = \<const0> ;
  assign s_axi_rdata[170] = \<const0> ;
  assign s_axi_rdata[169] = \<const0> ;
  assign s_axi_rdata[168] = \<const0> ;
  assign s_axi_rdata[167] = \<const0> ;
  assign s_axi_rdata[166] = \<const0> ;
  assign s_axi_rdata[165] = \<const0> ;
  assign s_axi_rdata[164] = \<const0> ;
  assign s_axi_rdata[163] = \<const0> ;
  assign s_axi_rdata[162] = \<const0> ;
  assign s_axi_rdata[161] = \<const0> ;
  assign s_axi_rdata[160] = \<const0> ;
  assign s_axi_rdata[159] = \<const0> ;
  assign s_axi_rdata[158] = \<const0> ;
  assign s_axi_rdata[157] = \<const0> ;
  assign s_axi_rdata[156] = \<const0> ;
  assign s_axi_rdata[155] = \<const0> ;
  assign s_axi_rdata[154] = \<const0> ;
  assign s_axi_rdata[153] = \<const0> ;
  assign s_axi_rdata[152] = \<const0> ;
  assign s_axi_rdata[151] = \<const0> ;
  assign s_axi_rdata[150] = \<const0> ;
  assign s_axi_rdata[149] = \<const0> ;
  assign s_axi_rdata[148] = \<const0> ;
  assign s_axi_rdata[147] = \<const0> ;
  assign s_axi_rdata[146] = \<const0> ;
  assign s_axi_rdata[145] = \<const0> ;
  assign s_axi_rdata[144] = \<const0> ;
  assign s_axi_rdata[143] = \<const0> ;
  assign s_axi_rdata[142] = \<const0> ;
  assign s_axi_rdata[141] = \<const0> ;
  assign s_axi_rdata[140] = \<const0> ;
  assign s_axi_rdata[139] = \<const0> ;
  assign s_axi_rdata[138] = \<const0> ;
  assign s_axi_rdata[137] = \<const0> ;
  assign s_axi_rdata[136] = \<const0> ;
  assign s_axi_rdata[135] = \<const0> ;
  assign s_axi_rdata[134] = \<const0> ;
  assign s_axi_rdata[133] = \<const0> ;
  assign s_axi_rdata[132] = \<const0> ;
  assign s_axi_rdata[131] = \<const0> ;
  assign s_axi_rdata[130] = \<const0> ;
  assign s_axi_rdata[129] = \<const0> ;
  assign s_axi_rdata[128] = \<const0> ;
  assign s_axi_rdata[127] = \<const0> ;
  assign s_axi_rdata[126] = \<const0> ;
  assign s_axi_rdata[125] = \<const0> ;
  assign s_axi_rdata[124] = \<const0> ;
  assign s_axi_rdata[123] = \<const0> ;
  assign s_axi_rdata[122] = \<const0> ;
  assign s_axi_rdata[121] = \<const0> ;
  assign s_axi_rdata[120] = \<const0> ;
  assign s_axi_rdata[119] = \<const0> ;
  assign s_axi_rdata[118] = \<const0> ;
  assign s_axi_rdata[117] = \<const0> ;
  assign s_axi_rdata[116] = \<const0> ;
  assign s_axi_rdata[115] = \<const0> ;
  assign s_axi_rdata[114] = \<const0> ;
  assign s_axi_rdata[113] = \<const0> ;
  assign s_axi_rdata[112] = \<const0> ;
  assign s_axi_rdata[111] = \<const0> ;
  assign s_axi_rdata[110] = \<const0> ;
  assign s_axi_rdata[109] = \<const0> ;
  assign s_axi_rdata[108] = \<const0> ;
  assign s_axi_rdata[107] = \<const0> ;
  assign s_axi_rdata[106] = \<const0> ;
  assign s_axi_rdata[105] = \<const0> ;
  assign s_axi_rdata[104] = \<const0> ;
  assign s_axi_rdata[103] = \<const0> ;
  assign s_axi_rdata[102] = \<const0> ;
  assign s_axi_rdata[101] = \<const0> ;
  assign s_axi_rdata[100] = \<const0> ;
  assign s_axi_rdata[99] = \<const0> ;
  assign s_axi_rdata[98] = \<const0> ;
  assign s_axi_rdata[97] = \<const0> ;
  assign s_axi_rdata[96] = \<const0> ;
  assign s_axi_rdata[95] = \<const0> ;
  assign s_axi_rdata[94] = \<const0> ;
  assign s_axi_rdata[93] = \<const0> ;
  assign s_axi_rdata[92] = \<const0> ;
  assign s_axi_rdata[91] = \<const0> ;
  assign s_axi_rdata[90] = \<const0> ;
  assign s_axi_rdata[89] = \<const0> ;
  assign s_axi_rdata[88] = \<const0> ;
  assign s_axi_rdata[87] = \<const0> ;
  assign s_axi_rdata[86] = \<const0> ;
  assign s_axi_rdata[85] = \<const0> ;
  assign s_axi_rdata[84] = \<const0> ;
  assign s_axi_rdata[83] = \<const0> ;
  assign s_axi_rdata[82] = \<const0> ;
  assign s_axi_rdata[81] = \<const0> ;
  assign s_axi_rdata[80] = \<const0> ;
  assign s_axi_rdata[79] = \<const0> ;
  assign s_axi_rdata[78] = \<const0> ;
  assign s_axi_rdata[77] = \<const0> ;
  assign s_axi_rdata[76] = \<const0> ;
  assign s_axi_rdata[75] = \<const0> ;
  assign s_axi_rdata[74] = \<const0> ;
  assign s_axi_rdata[73] = \<const0> ;
  assign s_axi_rdata[72] = \<const0> ;
  assign s_axi_rdata[71] = \<const0> ;
  assign s_axi_rdata[70] = \<const0> ;
  assign s_axi_rdata[69] = \<const0> ;
  assign s_axi_rdata[68] = \<const0> ;
  assign s_axi_rdata[67] = \<const0> ;
  assign s_axi_rdata[66] = \<const0> ;
  assign s_axi_rdata[65] = \<const0> ;
  assign s_axi_rdata[64] = \<const0> ;
  assign s_axi_rdata[63] = \<const0> ;
  assign s_axi_rdata[62] = \<const0> ;
  assign s_axi_rdata[61] = \<const0> ;
  assign s_axi_rdata[60] = \<const0> ;
  assign s_axi_rdata[59] = \<const0> ;
  assign s_axi_rdata[58] = \<const0> ;
  assign s_axi_rdata[57] = \<const0> ;
  assign s_axi_rdata[56] = \<const0> ;
  assign s_axi_rdata[55] = \<const0> ;
  assign s_axi_rdata[54] = \<const0> ;
  assign s_axi_rdata[53] = \<const0> ;
  assign s_axi_rdata[52] = \<const0> ;
  assign s_axi_rdata[51] = \<const0> ;
  assign s_axi_rdata[50] = \<const0> ;
  assign s_axi_rdata[49] = \<const0> ;
  assign s_axi_rdata[48] = \<const0> ;
  assign s_axi_rdata[47] = \<const0> ;
  assign s_axi_rdata[46] = \<const0> ;
  assign s_axi_rdata[45] = \<const0> ;
  assign s_axi_rdata[44] = \<const0> ;
  assign s_axi_rdata[43] = \<const0> ;
  assign s_axi_rdata[42] = \<const0> ;
  assign s_axi_rdata[41] = \<const0> ;
  assign s_axi_rdata[40] = \<const0> ;
  assign s_axi_rdata[39] = \<const0> ;
  assign s_axi_rdata[38] = \<const0> ;
  assign s_axi_rdata[37] = \<const0> ;
  assign s_axi_rdata[36] = \<const0> ;
  assign s_axi_rdata[35] = \<const0> ;
  assign s_axi_rdata[34] = \<const0> ;
  assign s_axi_rdata[33] = \<const0> ;
  assign s_axi_rdata[32] = \<const0> ;
  assign s_axi_rdata[31] = \<const0> ;
  assign s_axi_rdata[30] = \<const0> ;
  assign s_axi_rdata[29] = \<const0> ;
  assign s_axi_rdata[28] = \<const0> ;
  assign s_axi_rdata[27] = \<const0> ;
  assign s_axi_rdata[26] = \<const0> ;
  assign s_axi_rdata[25] = \<const0> ;
  assign s_axi_rdata[24] = \<const0> ;
  assign s_axi_rdata[23] = \<const0> ;
  assign s_axi_rdata[22] = \<const0> ;
  assign s_axi_rdata[21] = \<const0> ;
  assign s_axi_rdata[20] = \<const0> ;
  assign s_axi_rdata[19] = \<const0> ;
  assign s_axi_rdata[18] = \<const0> ;
  assign s_axi_rdata[17] = \<const0> ;
  assign s_axi_rdata[16] = \<const0> ;
  assign s_axi_rdata[15] = \<const0> ;
  assign s_axi_rdata[14] = \<const0> ;
  assign s_axi_rdata[13] = \<const0> ;
  assign s_axi_rdata[12] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  cosine_rom_blk_mem_gen_v8_4_1_synth inst_blk_mem_gen
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_4_1_synth" *) 
module cosine_rom_blk_mem_gen_v8_4_1_synth
   (douta,
    doutb,
    clka,
    clkb,
    sleep,
    addra,
    addrb);
  output [335:0]douta;
  output [335:0]doutb;
  input clka;
  input clkb;
  input sleep;
  input [11:0]addra;
  input [11:0]addrb;

  wire [11:0]addra;
  wire [11:0]addrb;
  wire clka;
  wire clkb;
  wire [335:0]douta;
  wire [335:0]doutb;
  wire sleep;

  cosine_rom_blk_mem_gen_top \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .douta(douta),
        .doutb(doutb),
        .sleep(sleep));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
