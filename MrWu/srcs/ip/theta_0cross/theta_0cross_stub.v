// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Tue Sep  4 15:24:34 2018
// Host        : baby running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub D:/L0MDT/srcs/ip/theta_0cross/theta_0cross_stub.v
// Design      : theta_0cross
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku15p-ffva1760-1-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_1,Vivado 2018.2" *)
module theta_0cross(clka, addra, douta)
/* synthesis syn_black_box black_box_pad_pin="clka,addra[8:0],douta[20:0]" */;
  input clka;
  input [8:0]addra;
  output [20:0]douta;
endmodule
