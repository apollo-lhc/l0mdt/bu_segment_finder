How to rebuild the project:
check out the L0MDT directory
edit the l0mdt.tcl in tcl folder.
Change the origin_dir to whatever the
path to the l0mdt.tcl is.
start vivado IDE and in the tcl command window at the bottom 
change to the directory where
you want to rebuild the project. enter
source PATH/l0mdt.tcl where PATH points to where
the tcl script is located.
Hit return key and the script will rebuild the project.
