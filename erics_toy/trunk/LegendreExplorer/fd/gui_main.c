#include "gui.h"

/* make global for convenience */
FD_seg *fd_seg;

int
main( int    argc,
      char * argv[ ] )
{
    fl_initialize( &argc, argv, 0, 0, 0 );
    fd_seg = create_form_seg( );

    /* Fill-in form initialization code */

    /* Show the first form */

    fl_show_form( fd_seg->seg, FL_PLACE_CENTERFREE, FL_FULLBORDER, "seg" );

    fl_do_forms( );

    if ( fl_form_is_visible( fd_seg->seg ) )
        fl_hide_form( fd_seg->seg );
    fl_free( fd_seg );
    fl_finish( );

    return 0;
}
