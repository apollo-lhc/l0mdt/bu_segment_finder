/* Header file generated by fdesign on Mon Nov 21 13:37:24 2016 */

#ifndef FD_seg_h_
#define FD_seg_h_

#include <forms.h>

/* Callbacks, globals and object handlers */

extern void val_cb( FL_OBJECT *, long );
extern void cb_Button( FL_OBJECT *, long );
int draw_handler( FL_OBJECT * ob,
		  int         event,
		  FL_Coord    mx,
		  FL_Coord    my,
		  int         key,
		  void      * xev  FL_UNUSED_ARG );


/* Forms and Objects */

typedef struct {
    FL_FORM   * seg;
    void      * vdata;
    char      * cdata;
    long        ldata;
    FL_OBJECT * th_steps;
    FL_OBJECT * DrawArea;
    FL_OBJECT * Histo;
    FL_OBJECT * Title;
    FL_OBJECT * VersionText;
    FL_OBJECT * th_range;
    FL_OBJECT * r_steps;
    FL_OBJECT * text_xmin;
    FL_OBJECT * text_xmax;
    FL_OBJECT * text_ymax;
    FL_OBJECT * text_ymin;
} FD_seg;

extern FD_seg * create_form_seg( void );
extern FD_seg *fd_seg;

#endif /* FD_seg_h_ */
