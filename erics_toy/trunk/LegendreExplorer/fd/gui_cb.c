#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <forms.h>
#include <strings.h>

#include "track.h"
#include "tube.h"
#include "chamber.h"

#include "mdt.h"
#include "gui.h"

FL_COLOR hist_colr[] = { FL_BLACK, FL_RED, FL_TOMATO, FL_DARKORANGE,
			 FL_YELLOW, FL_GREEN, FL_SLATEBLUE, FL_DARKVIOLET };
#define HIST_MAX_COLR (sizeof(hist_colr)/sizeof(FL_COLOR))
#define HIST_HIGHEST_COLR FL_WHITE

static a_track* trk;
static a_chamber *cbr;

void val_cb( FL_OBJECT * ob,
         long        data )
{
    /* Fill-in code for callback here */
}

void cb_Button( FL_OBJECT * ob,
         long        data )
{
  int hits;

  switch( data) {
  case 1:			/* BUTTON 1 - new track */
    /* throw a new track */
    cbr = make_chamber( BM_Z0, BM_R, 2, 3, BM_TUBES_PER_LAYER, BM_SPC, 15);
    trk = gen_track();
    do {
      trk = gen_track();
      hits = find_track_hits( cbr, trk);
    } while( hits < 4);
    fl_redraw_object( fd_seg->DrawArea);
    break;

  case 2:			/* BUTTON 2 - run LeGendre */
    {
      double th_deg = atof( fl_get_input( fd_seg->th_range));
      double th_range = to_rad( th_deg);
      double th_steps = atof( fl_get_input( fd_seg->th_steps));
      double r_steps = atof( fl_get_input( fd_seg->r_steps));

      /* set origin - somewhat arbitrary for now */
      set_legendre_origin( cbr->bb_min_x - 100.0, cbr->bb_min_y - 100.0);

      /* get legendre params for track */
      double trk_th, trk_r;
      get_legendre_track( trk, &trk_th, &trk_r);
      printf("Track th=%f  r=%f\n", trk_th, trk_r);

      //      printf("Got theta range %g in %g steps\n", th_range, th_steps);
      /* loop over theta, derived from track angle */
      double th_track = -(PI/2.0-trk->alpha);
      double th_min = th_track - th_range/2.0;
      double th_max = th_track + th_range/2.0;
      double th_step = th_range / th_steps;
      //      printf("Track angle: %g deg  Theta angle: %g deg (range %g-%g)\n",
      //	     to_deg( trk->alpha), to_deg( th_track), to_deg( th_min), to_deg(th_max));

      FL_Coord x, y, w, h, sdim;
      fl_get_object_geometry( fd_seg->Histo, &x, &y, &w, &h);

      /* two passes for now to get min/max R for scaling */
      double min_r = 99999;
      double max_r = -99999;

      for( double th=th_min; th<th_max; th += th_step) {
	//	printf(" Theta=%g  trk = %g (deg)\n", to_deg(th), to_deg(PI/2.0 - th) );
	double r0, r1;
	/* loop over all hit tubes */
	for( int k=0; k < cbr->nlayers; k++) {
	  a_layer* lay = &(cbr->layers[k]);
	  for( int i=0; i < lay->ntubes; i++) {
	    if( lay->tubes[i].drift_r) {
	      eval_legendre( &lay->tubes[i], th, &r0, &r1);
	      if( r0 < min_r) min_r = r0;
	      if( r1 < min_r) min_r = r1;
	      if( r0 > max_r) max_r = r0;
	      if( r1 > max_r) max_r = r1;
	      //	      printf("  layr: %d tube: %d R=%g r0=%g r1=%g\n",
	      //		     k, i, lay->tubes[i].drift_r, r0, r1);
	    }
	  }
	}
      }	/* theta loop first pass */

      double yscal = (double)h/r_steps;
      double xscal = (double)w/th_steps;
      double r_step = (max_r-min_r)/r_steps;

      int histo[(int)th_steps][(int)r_steps];
      bzero( histo, sizeof(int)*th_steps*r_steps);

      int ith;
      double th;
      int hist_max = 0;
      int num_hist_max = 0;

      for( th=th_min, ith=0; ith < th_steps; ith++, th += th_step) {
	double r0, r1;
	/* loop over all hit tubes */
	for( int k=0; k < cbr->nlayers; k++) {
	  a_layer* lay = &(cbr->layers[k]);
	  for( int i=0; i < lay->ntubes; i++) {
	    if( lay->tubes[i].drift_r) {
	      eval_legendre( &lay->tubes[i], th, &r0, &r1);
	      int ir0 = r_steps*(r0-min_r)/(max_r-min_r);
	      if( ir0 >= 0 && ir0 < r_steps) {
		++histo[ith][ir0];
		if( histo[ith][ir0] > hist_max) {
		  hist_max = histo[ith][ir0];
		  ++num_hist_max;
		}
	      }
	      int ir1 = r_steps*(r1-min_r)/(max_r-min_r);
	      if( ir1 >= 0 && ir1 < r_steps) {
		++histo[ith][ir1];
		if( histo[ith][ir1] > hist_max) {
		  hist_max = histo[ith][ir1];
		  ++num_hist_max;
		}
	      }
	    }
	  }
	}
      }	/* theta loop second pass, display histogram */

      /* erase border around draw area, blacken background */
      fl_rectf( x-xscal-1, y-yscal-1, w+2*xscal+2, h+2*yscal+2, FL_WHITE);
      fl_rectbound( x, y, w, h, FL_BLACK);

      for( int k=0; k<th_steps; k++) {
	for( int i=0; i<r_steps; i++) {
	  int n = histo[k][i];
	  FL_COLOR c = hist_colr[HIST_MAX_COLR-1];
	  if( n < HIST_MAX_COLR)
	    c = hist_colr[n];
	  if( n == hist_max) {
	    c = HIST_HIGHEST_COLR;
	    fl_line( x + xscal*(double)k + xscal/2, y-yscal, x + xscal*(double)k + xscal/2, y, FL_RED);
    	    fl_line( x-xscal, y + yscal*(double)i + yscal/2, x, y + yscal*(double)i + yscal/2, FL_RED);
	    printf("Bin with %d counts at r=%g th=%g\n", hist_max,
		   min_r + (max_r-min_r)*(double)i,
		   th_min + (th_max-th_min)*(double)k );
	  }
	  fl_rectf( x + xscal*(double)k, y + yscal*(double)i, xscal, yscal, c);
	}
      }

      char tmp[10];
      sprintf( tmp, "%4.2g", th_min);
      fl_set_object_label( fd_seg->text_xmin, tmp);
      sprintf( tmp, "%4.2g", th_max);
      fl_set_object_label( fd_seg->text_xmax, tmp);
      sprintf( tmp, "%8.1f", min_r);
      fl_set_object_label( fd_seg->text_ymin, tmp);
      sprintf( tmp, "%8.1f", max_r);
      fl_set_object_label( fd_seg->text_ymax, tmp);

    }
    break;
  case 3:
    break;
  case 4:
    break;
  case 5:
    break;
  case 6:
    break;
  case 7:
    break;
  case 8:
    exit(1);
  }
}



int draw_handler( FL_OBJECT * ob,
		  int         event,
		  FL_Coord    mx,
		  FL_Coord    my,
		  int         key,
		  void      * xev  FL_UNUSED_ARG )
{
  //  printf("draw_handler()...\n");
  if( cbr == NULL) {
    //    printf("(no chamber)\n");
    return 0;
  }
  //  printf("Event: %d\n", event);
  switch( event) {
  case FL_DRAW:
    {
      FL_Coord x, y, w, h, sdim;
      //      fl_get_object_geometry( fd_seg->DrawArea, &x, &y, &w, &h);
      x = ob->x;
      y = ob->y;
      w = ob->w;
      h = ob->h;

      sdim = (w>h) ? h : w;	/* shortest dim in drawing area */
      double ch_w = cbr->bb_max_x - cbr->bb_min_x;
      double ch_h = cbr->bb_max_y - cbr->bb_min_y;
      double ch_ldim = (ch_w<ch_h) ? ch_h : ch_w; /* largest chamber dim */
      double scale = (double)sdim / ch_ldim;	/* scale */

      //      printf("Geom: %d %d %d %d\n", x, y, w, h);

      fl_rectbound( x, y, w, h, FL_BLANCHEDALMOND);

      for( int k=0; k < cbr->nlayers; k++) {
	a_layer* lay = &(cbr->layers[k]);
	for( int i=0; i < lay->ntubes; i++) {
	  if( lay->tubes[i].drift_r) {
	    int cx = (int)((lay->tubes[i].x0-cbr->bb_min_x)*scale) + x;
	    int cy = y+h-(int)((lay->tubes[i].y0-cbr->bb_min_y)*scale);
	    int cr = lay->tubes[i].drift_r * scale;
	    fl_circ( cx, cy, cr, FL_BLUE);
	  }
	}
      }

      /* output two points for track */
      double tx0, ty0, tx1, ty1;
      ty0 = cbr->bb_min_y;
      ty1 = cbr->bb_max_y;
      tx0 = (ty0 - trk->intercept) / trk->slope;
      tx1 = (ty1 - trk->intercept) / trk->slope;

      tx0 = (tx0-cbr->bb_min_x)*scale + x;
      tx1 = (tx1-cbr->bb_min_x)*scale + x;
      ty0 = y+h-(ty0-cbr->bb_min_y)*scale; 
      ty1 = y+h-(ty1-cbr->bb_min_y)*scale;

      fl_line( (int)tx0, (int)ty0, (int)tx1, (int)ty1, FL_GREEN);
    }
    break;

  FL_PUSH:
    fl_redraw_object( ob);
    break;
  }
  return 0;
}
