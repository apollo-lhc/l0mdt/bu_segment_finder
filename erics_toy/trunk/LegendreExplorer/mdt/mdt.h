/*
 * MDT utility functions
 */

int find_track_hits( a_chamber* ch, a_track* t);
int eval_legendre( a_tube* t, double theta, double *r0, double *r1);
void set_legendre_origin( double x0, double y0);
void get_legendre_track( a_track *t, double *th, double *r);
