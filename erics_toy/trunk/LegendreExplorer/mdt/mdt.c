#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "track.h"
#include "tube.h"
#include "chamber.h"
#include "mdt.h"

static double x_origin;
static double y_origin;

/*
 * set origin for future calls to eval_legendre (0,0 = IP)
 */
void set_legendre_origin( double x0, double y0) {
  x_origin = x0;
  y_origin = y0;
}

/*
 * evaluate Legendre transform for a tube (must be a hit tube)
 * given an angle theta in radians (which is just pi/2-track_angle)
 * set r0, r1 to two results.  Return 0 on success, 1 on error
 */

int eval_legendre( a_tube* t, double theta, double *r0, double *r1) {
  if( t->drift_r <= 0 || t->drift_r > TUBE_RADIUS)
    return 1;
  double term = (t->x0-x_origin) * cos(theta) + (t->y0-y_origin) * sin(theta);
  *r0 = term + t->drift_r;
  *r1 = term - t->drift_r;
  return 0;
}

/*
 * calculate LeGendre parameters for track based on origin set
 * via set_legendre_origin.  For this, we need one arbitrary
 * point on the track.
 */
void get_legendre_track( a_track *t, double *th, double *r) {
  double tkx, tky;
  printf("get_legendre_track() - ");
  
  tkx = t->beam_intercept;
  tky = 0;

  //  *th = PI/2.0 - atan( t->slope);
  *th = atan( t->slope) - PI/2.0;
  printf("slope=%f  atan()=%f  th=%f\n",
	 t->slope, atan(t->slope), *th);

  *r = (tkx-x_origin) * cos(*th) + (tky-y_origin) * sin(*th);
}

/*
 * find hits (where track is closer than one radius to a tube)
 * set the drift_r for each hit tube, update hit_min/max_x/y
 * return number of hits
 */
int find_track_hits( a_chamber* ch, a_track* t) {
  int hits = 0;
  for( int k=0; k < ch->nlayers; k++) {
    a_layer* lay = &(ch->layers[k]);
    for( int i=0; i < lay->ntubes; i++) {
      double x0 = lay->tubes[i].x0;
      double y0 = lay->tubes[i].y0;
      double d = track_dist( t, x0, y0);
      if( d <= TUBE_RADIUS) {	/* was this tube hit? */
	lay->tubes[i].drift_r = d; /* for now radius is exact distance */
	/* find x/y range for hit tubes */
	if( x0 < ch->hit_min_x) ch->hit_min_x = x0;
	if( y0 < ch->hit_min_y) ch->hit_min_y = y0;
	if( x0 > ch->hit_max_x) ch->hit_max_x = x0;
	if( y0 > ch->hit_max_y) ch->hit_max_y = y0;
	++ch->nhit;
	++hits;
      }
    }
  }

  /* bounding box for drawing; expand by tube radius */
  ch->bb_min_x = ch->hit_min_x - TUBE_RADIUS;
  ch->bb_max_x = ch->hit_max_x + TUBE_RADIUS;
  ch->bb_min_y = ch->hit_min_y - TUBE_RADIUS;
  ch->bb_max_y = ch->hit_max_y + TUBE_RADIUS;

  return hits;
}
