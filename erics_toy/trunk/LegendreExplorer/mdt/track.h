
#define PI 3.1415926
#define to_rad(x) (((x)/360.0)*2.0*PI)
#define to_deg(x) (((x)/(2.0*PI))*360.0)

// one track
typedef struct {
  double alpha;			/* angle in rad WRT beam */
  double slope;			/* slope */
  double intercept;		/* intercept on Y axis (vertially through IP) */
  double beam_intercept;	/* intercept on X axis */
} a_track;

a_track* gen_track();
void print_track( a_track* t);
double track_dist( a_track* t, double x, double y);
