#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <forms.h>

#include "track.h"
#include "tube.h"
#include "chamber.h"

#include "mdt.h"

int main( int argc, char *argv[] ) {

  a_track* t;

  srand( time( NULL));

  a_chamber *ch = make_chamber( BM_Z0, BM_R, 2, 3, BM_TUBES_PER_LAYER, BM_SPC, 15);
  int hits = 0;

  do {
    t = gen_track();
    hits = find_track_hits( ch, t);
  } while( hits < 4);

  /* output some postscript for the hits */
  printf("%f %f %f %f setXYrange\n", ch->hit_min_x, ch->hit_max_x, ch->hit_min_y, ch->hit_max_y);
  for( int k=0; k < ch->nlayers; k++) {
    a_layer* lay = &(ch->layers[k]);
    for( int i=0; i < lay->ntubes; i++) {
      if( lay->tubes[i].drift_r)
	printf("%f %f %f drawCirc\n",
	       lay->tubes[i].x0, lay->tubes[i].y0, lay->tubes[i].drift_r);
    }
  }

  /* output two points for track */
  double tx0, ty0, tx1, ty1;
  ty0 = ch->hit_min_y - 15;
  ty1 = ch->hit_max_y + 15;
  tx0 = (ty0 - t->intercept) / t->slope;
  tx1 = (ty1 - t->intercept) / t->slope;
  printf("%f %f %f %f drawTrack\n", tx0, ty0, tx1, ty1);
}

