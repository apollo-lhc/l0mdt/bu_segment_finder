//
// generate a random track and associated ROI
//
#define DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "track.h"

// track parameters:  angle range
#define TRACK_ALPHA_MIN (PI/3)
#define TRACK_ALPHA_MAX (PI/1.9)
// track parameters:  offset from IP (mm in +z direction for now)
#define TRACK_Z_MIN 0
#define TRACK_Z_MAX 1000

// generate a random track
a_track* gen_track()
{
  a_track* t = calloc( 1, sizeof(a_track));
  double beam_intercept;
  t->alpha = ((double)rand()/(double)RAND_MAX) * (TRACK_ALPHA_MAX-TRACK_ALPHA_MIN) + TRACK_ALPHA_MIN;
  t->beam_intercept = ((double)rand()/(double)RAND_MAX) * (TRACK_Z_MAX-TRACK_Z_MIN) + TRACK_Z_MIN;
  t->slope = tan( t->alpha);
  /* calculate traditional intercept */
  t->intercept = t->slope * t->beam_intercept;
  return t;
}

// print track info
void print_track( a_track* t) {
  printf("Random y = %g * x + %g (alpha = %g deg)\n",
	 t->slope, t->intercept, t->alpha*(360.0/(2.0*PI)));
}

// calculate distance track to any point
double track_dist( a_track* t, double x0, double y0) {
  // find closest point
  double xc = (x0 + t->slope*y0 - t->slope*t->intercept) / (t->slope*t->slope+1);
  double yc = t->slope * xc + t->intercept;
  double d = sqrt( pow(xc-x0,2) + pow(yc-y0,2));
  return d;
}
