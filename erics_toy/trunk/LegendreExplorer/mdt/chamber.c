//
// generate all tubes for specified chamber
// (really a station with all chambers)
// simplified geometry ignores gaps between chambers
//
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "tube.h"
#include "chamber.h"

a_chamber* make_chamber( double z0, double r0,
			 int multilayers, int layers_per_ml,
			 int tubes_per_layer, double spacer_thickness,
			 double tube_radius)
{
  double layer_spacing = floor((1.732*tube_radius)+0.5);
  a_chamber *c = calloc( 1, sizeof(a_chamber));
  c->r0 = r0;
  c->z0 = z0;
  c->spacer = spacer_thickness;
  c->nlayers = multilayers * layers_per_ml;
  c->layers = calloc( c->nlayers, sizeof(a_layer));
  c->min_x = z0;
  c->max_x = z0 + (tubes_per_layer-1)*2*tube_radius;
  c->min_y = r0-spacer_thickness/2 - 2*tube_radius - layer_spacing * (layers_per_ml-1);
  c->max_y = r0 + spacer_thickness/2 + 2*tube_radius + layer_spacing * (layers_per_ml-1);
  c->hit_min_x = c->hit_min_y = 99999;
  c->hit_max_x = c->hit_max_y = 0;
  for( int i=0; i < c->nlayers; i++) {
    int stack;
    if( i < layers_per_ml) {	/* bottom */
      stack = layers_per_ml-i; /* stack out from spacer */
      c->layers[i].y0 = r0 - spacer_thickness/2 - tube_radius - stack * layer_spacing;
    } else {
      stack = i-layers_per_ml;
      c->layers[i].y0 = r0 + spacer_thickness/2 + tube_radius + stack * layer_spacing;
    }
    c->layers[i].x0 = z0 + ((stack % 2) ? 0 : tube_radius);
    //    printf("Stack: %d (stack %% 2): %d x0: %g\n", stack, (stack % 2), c->layers[i].x0);
    c->layers[i].tubes = calloc( tubes_per_layer, sizeof(a_tube));
    c->layers[i].ntubes = tubes_per_layer;
    for( int k=0; k<tubes_per_layer; k++) {
      c->layers[i].tubes[k].x0 = c->layers[i].x0 + tube_radius*2*k;
      c->layers[i].tubes[k].y0 = c->layers[i].y0;
    }
  }
  return c;
}

void print_chamber( a_chamber* c) {
  printf("Chamber at (%g, %g) with %d layers\n",
	 c->z0, c->r0, c->nlayers);
  for( int i=0; i < c->nlayers; i++)
    print_layer( &(c->layers[i]) );
}
