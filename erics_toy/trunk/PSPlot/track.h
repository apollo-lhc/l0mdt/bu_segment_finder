
#define PI 3.1415926

// one track
typedef struct {
  double alpha;			/* angle in rad WRT beam */
  double slope;			/* slope */
  double intercept;		/* intercept on Y axis (vertially through IP) */
} a_track;

a_track* gen_track();
void print_track( a_track* t);
double track_dist( a_track* t, double x, double y);
