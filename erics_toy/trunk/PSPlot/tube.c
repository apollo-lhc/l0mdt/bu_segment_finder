
#include <stdio.h>
#include "tube.h"

void print_tube( a_tube* t)
{
  printf("(%g, %g) ", t->x0, t->y0);
  if( t->hit_t != 0 || t->drift_r != 0)
    printf("T=%g R=%g", t->hit_t, t->drift_r);
  printf("\n");
}


void print_layer( a_layer* l)
{
  printf("Layer (%g, %g) with %d tubes\n", l->x0, l->y0, l->ntubes);
  for( int i=0; i < l->ntubes; i++)
    print_tube( &(l->tubes[i]) );
}
