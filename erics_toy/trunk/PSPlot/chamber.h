
// define chamber / layer geometry for testing

#define BM_Z0 390
#define BM_R 7139
#define BM_SPC 317

#define BM_TUBES_PER_LAYER 288
#define BM_LAYERS_PER_ML 3
#define BM_TUBE_RADIUS 15
// that odd value is sqrt(3)
#define BM_LAYER_SPACING ((int)((1.732*(double)BM_TUBE_RADIUS)+0.5))

typedef struct {
  int nlayers;			/* number of layers */
  double r0, z0;		/* origin:  middle of spacer at minimum Z */
  a_layer *layers;		/* array of layers */
  double spacer;		/* spacer thickness */
  double min_x, max_x;		/* min/max x/y (aka r/z) */
  double min_y, max_y;
  double hit_min_x, hit_max_x;	/* min/max x/y for hit tubes */
  double hit_min_y, hit_max_y;
  int nhit;
} a_chamber;

a_chamber* make_chamber( double z0, double r0,
			 int multilayers, int layers_per_ml,
			 int tubes_per_layer, double spacer_thickness,
			 double tube_radius);
void print_chamber( a_chamber* c);

