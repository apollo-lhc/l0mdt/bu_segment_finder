
#define TUBE_RADIUS 15.0

typedef struct {
  double x0;
  double y0;
  double hit_t;
  double drift_r;
} a_tube;


typedef struct {
  double x0;			/* wire position of first tube */
  double y0;
  int ntubes;
  a_tube* tubes;
} a_layer;

void print_tube( a_tube* t);
void print_layer( a_layer* l);
