#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <stdint.h>

#include <math.h>
#include <stdlib.h>     /* abs */

#include "hist.hpp"
#include "canvas.hpp"

#include "mdt.h"
#define PI 3.14159

void read_pos_file(std::vector<tube_pos> & pos, const char * const filename){
  if( filename == NULL){
    throw std::string("Bad filename in read_pos_file\n");
  }
  std::ifstream inFile(filename);
  while(!inFile.eof()){
    double x,y,ht;
    inFile >> x;
    inFile >> y;
    inFile >> ht;
    if(!inFile.eof()){
      pos.push_back(tube_pos(x,y,ht));
    }
  }
}

void read_track_file(double & m,double &alpha, double & x0, const char * const filename){
  std::ifstream inFile(filename);
  inFile >> alpha;
  inFile >> x0;
  m = tan(alpha*PI/180.0);

}



void draw_hit(tube_pos tube,double hit_time,hist & h){
  //stolen from https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
  double x = hit_time;
  double x2;
  double r2 = hit_time*hit_time;
  double y = 0;
  double y2;
  //  int err = 0;
  while(x >= y){
    
    //compute 1/8 of the circle and mirror to the other octrants
    h.Set(tube.x + x, tube.y + y,1);
    h.Set(tube.x + y, tube.y + x,1);
    h.Set(tube.x - y, tube.y + x,1);
    h.Set(tube.x - x, tube.y + y,1);    
    h.Set(tube.x - x, tube.y - y,1);    
    h.Set(tube.x - y, tube.y - x,1);    
    h.Set(tube.x + y, tube.y - x,1);
    h.Set(tube.x + x, tube.y - y,1);
    
    //We are in the first octant, so we'll be increasing in y faster than x
    y+=h.GetY()->GetDelta();
    x2=x*x;
    y2=y*y;
    if(fabs(x2 + y2 - r2) > fabs(x2 -2*x +y2 + h.GetX()->GetDelta() - r2)){
      x-=h.GetX()->GetDelta();
    }
  }
}







void Add_l_segment(double x, double y1, double y2, hist & h, int incr = 0){  
  //Fill in the bins in y between the old and new draw points.
  //This currently is dumb about the X coordinate as all new points are drawn on the new x coord isntead of transitioning between the two acording to the line.

  double yMax = h.GetY()->GetMax();
  double yMin = h.GetY()->GetMin();

  if( (y1 > yMax) && (y2 > yMax)){
    return;
  } else if ( (y1 < yMin) && (y2 < yMin)){
    return;
  }
  
  if( y1 > yMax){
    y1 =  yMax - h.GetY()->GetDelta()*0.5;
  } else if( y1 < yMin){
    y1 =  yMin;
  }

  if( y2 > yMax){
    y2 =  yMax - h.GetY()->GetDelta()*0.5;
  } else if( y2 < yMin){
    y2 =  yMin;
  }

  
  //Fingure out how many inbetween y points we have  
  size_t bin_y2 = h.GetY()->FindBin(y2);
  size_t bin_y1 = h.GetY()->FindBin(y1);
  int steps = abs(bin_y2 -  bin_y1) ;  

  
  //Find the starting point (lesser bin number) 
  int bin = (bin_y2>bin_y1) ? bin_y1: bin_y2 ;
  
  
  double delta_x = h.GetX()->GetDelta();
  double delta_y = h.GetY()->GetDelta();

  int mult = 5;

  // do while to draw atleast one point
  do {
    //compute inbetween y points
    double y = h.GetY()->GetBinCenter(bin+steps);
    //move to the next point
    steps--;
    //Simple minded plotting options
    try{
      if (incr == 2){
	//a simple cross pattern of filling
	h.Fill(x        ,y        ,2*mult);
	h.Fill(x        ,y-delta_y,mult);
	h.Fill(x        ,y+delta_y,mult);
	h.Fill(x+delta_x,y        ,mult);
	h.Fill(x-delta_x,y        ,mult);
      }else if (incr == 1){
	//normal bin incriment, but scaled for viewing
	h.Fill(x,y,incr);
      }else{
	//Fix value to one
	h.Set(x,y,1);
      }
    }catch(std::string & e){
      //Do nothing
    }
  }while(steps >= 0);

}

void gen_legendre(tube_pos tube,
		  hist & h){
  //Add one tubes two legendre space lines into h
  int i_theta = 0;
  double r[2];
  double r_old[2];
  double theta = h.GetX()->GetBinCenter(i_theta);
  r_old[0] = (tube.x*cos(theta*PI/180.0) + tube.y*sin(theta*PI/180.0) + tube.hit_time);
  r_old[1] = (tube.x*cos(theta*PI/180.0) + tube.y*sin(theta*PI/180.0) - tube.hit_time);
  //Only process hits that aren't points
  if (tube.hit_time > 0.01){
    //Loop over all theta values (bins)
    while(i_theta < int(h.GetX()->GetBinCount())){
      //find theta value for theta bin
      theta = h.GetX()->GetBinCenter(i_theta);
      //Compute this theta's two r values
      r[0] = (tube.x*cos(theta*PI/180.0) + tube.y*sin(theta*PI/180.0) + tube.hit_time);
      r[1] = (tube.x*cos(theta*PI/180.0) + tube.y*sin(theta*PI/180.0) - tube.hit_time);
      //draw this part of the function
      Add_l_segment(theta,r[0],r_old[0],h,1);
      Add_l_segment(theta,r[1],r_old[1],h,1);
      i_theta++;
      r_old[0] = r[0];
      r_old[1] = r[1];
    }
  }
}

void draw_track(hist & h,double m, double x0){
  printf("y = tan(%f) (x %+f)\n",atan(m)*180.0/PI,x0);
  for( int iX = 0; iX < int(h.GetX()->GetBinCount()); iX++){
    double x = h.GetX()->GetBinCenter(iX);
    double y = m*(x - x0);
    h.Set(x,y,1);
  }
}


int main(int argc, char ** argv){


  double true_x0 = 0;
  double true_m = 0;
  double true_alpha = 0;
  double true_theta = 0;
  double true_r0 = 0;
  read_track_file(true_m,true_alpha,true_x0,"track.dat");  

  true_theta = true_alpha - 90;// 90 - true_alpha;
  true_r0 = true_x0*cos((true_theta)*PI/180);
  printf("true_r0 = %f\n",true_r0);

  
  hist hTube(600,-150,150,  // bins x, x min,x max
	     900,400,850); // bins y ,y min, y max
  hist hTrack(hTube); // Setup the same as hTube
  hist hFitTrack(hTube); // Setup the same as hTube

  double smeared_theta = true_theta*(1.0 + 0.1*double(rand())/double(RAND_MAX));
  printf("theta/smeared theta: %f/%f\n",true_theta,smeared_theta);
  
  hist hLegendre(128,smeared_theta - 5,smeared_theta +5,
		 128,true_r0 - 15,true_r0 + 15);
  hist hLegendre_fit(hLegendre);
  




  
  std::vector<tube_pos> tube;
  read_pos_file(tube,"layer1.dat");
  read_pos_file(tube,"layer2.dat");
  
  //All the magic is here
  try{    
    for (int iTube = 0; iTube < int(tube.size());iTube++){
      draw_hit(tube[iTube],tube[iTube].hit_time,hTube);
      gen_legendre(tube[iTube],hLegendre);
    }
  }catch (std::string & e){
    std::cout << e << std::endl;
  }

  //Blindly take the max bin as the answer
  size_t fit_theta_i,fit_r0_i;
  uint8_t val;
  hLegendre.GetMaxBin(fit_theta_i,fit_r0_i,val);
  
  //Generate the track segment for this bin
  double fit_theta =  hLegendre.GetX()->GetBinCenter(fit_theta_i);
  double fit_r0 = hLegendre.GetY()->GetBinCenter(fit_r0_i);
  //  double fit_x = fit_r0* cos(fit_theta * PI/180.0);
  //  double fit_y = fit_r0* sin(fit_theta * PI/180.0);

  printf("Fit (theta/r0): %f/%f\n",fit_theta,fit_r0);
  
  double fit_alpha = 90.0 + fit_theta;//90.0 - fit_theta;
  double fit_m     = tan(fit_alpha * PI/180.0);
  double fit_x0    = fit_r0/cos(fit_theta * PI/180.0);// fit_x - fit_y/fit_m;
  
  //  printf("y = -cot(%f)*(x  %+f)\n",90+fit_theta,-1.0*fit_x0);

  //Draw the real track
  printf("real: ");
  draw_track(hTrack,true_m,true_x0);
  printf("fit : ");
  //Draw the fit track
  draw_track(hFitTrack,fit_m,fit_x0);

  for(int iCircle = 0; iCircle < 2;iCircle++){
    draw_hit(tube_pos(fit_theta,
		      fit_r0,1.0)
	     ,iCircle,hLegendre_fit);
  }
  
  //Draw tube and legendre bitmaps
  //canvas(&hTube).DrawBitmap("tubes.bmp");
  canvas tube_canvas = canvas(hTube.GetX(),hTube.GetY());
  tube_canvas.Add(&hTube,0);
  tube_canvas.Add(&hTube,1);
  tube_canvas.Add(&hTube,2);
  tube_canvas.Add(&hTrack,0);
  tube_canvas.Add(&hTrack,1);
  tube_canvas.Add(&hFitTrack,1);
  tube_canvas.Add(&hFitTrack,2);

  tube_canvas.WriteBitmap("tubes.bmp");
  canvas legendre_canvas = canvas(hLegendre.GetX(),hLegendre.GetY());
  legendre_canvas.Add(&hLegendre,1);
  hLegendre.write("leg.txt");
  
  legendre_canvas.Add(&hLegendre_fit,0);
  legendre_canvas.Add(&hLegendre_fit,1);
  legendre_canvas.Add(&hLegendre_fit,2);

  legendre_canvas.WriteBitmap("legendre.bmp");

  
  
  return 0;
}
