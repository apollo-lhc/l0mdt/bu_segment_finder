#include "hist.hpp"
#include "bitmap/bitmap_image.hpp"
#include <cstdio>

#include <string.h> //memset
#include <fstream>

hist::hist(size_t x_size,double x_min, double x_max,
	   size_t y_size,double y_min, double y_max) : data(NULL), x_axis(x_size,x_min,x_max) , y_axis(y_size,y_min,y_max){
  data=new uint8_t*[x_axis.GetBinCount()];
  for (size_t iX = 0; iX < x_axis.GetBinCount();iX++){
    //Now that we have local allocations, catch any allocation exceptions.
    try{
      data[iX] = new uint8_t[y_axis.GetBinCount()];
      memset(data[iX],0,y_axis.GetBinCount());
    }catch (std::bad_alloc & e){
      //Bad allocation, delallocate everything we've allocated and re-throw
      for(size_t iDeAlloc = 0; iDeAlloc < iX;iDeAlloc++){
	delete [] data[iDeAlloc];
      }
      delete [] data;
      throw;
    }    
  }
}

hist::hist(const hist &rhs) : data(NULL), x_axis(1,0,0) , y_axis(1,0,0) {
  copy(rhs);
}


void hist::clear(){
  if( data != NULL){
    for(size_t iX = 0; iX < x_axis.GetBinCount();iX++){
      if(data[iX] != NULL){
	delete [] data[iX];
      }
    }
    delete [] data;
  }
}

void hist::Fill(double x, double y, uint8_t weight){
  try{
    size_t iX = x_axis.FindBin(x);
    size_t iY = y_axis.FindBin(y);
    data[iX][iY] += weight;
  }catch(std::string &e){}
}

void hist::Set(double x, double y, uint8_t val){
  try{
    size_t iX = x_axis.FindBin(x);
    size_t iY = y_axis.FindBin(y);
    //    printf("%zd %f, %zd %f\n",iX,x,iY,y);
    data[iX][iY] = val;
  }catch(std::string &e){}
}



void hist::GetMaxBin(size_t &x, size_t &y, uint8_t &max){
  for(size_t iX = 0;iX < x_axis.GetBinCount();iX++){
    for(size_t iY = 0 ;iY < y_axis.GetBinCount();iY++){
      if ((data[iX][iY] != 255) && ( data[iX][iY] > max)){
	max = data[iX][iY];
	x = iX;
	y = iY;
      }
    }    
  }
}



void hist::copy(const hist &rhs){
  clear(); //reset this structure
  x_axis = rhs.x_axis;
  y_axis = rhs.y_axis;
  
  data=new uint8_t*[x_axis.GetBinCount()];
  for (size_t iX = 0; iX < x_axis.GetBinCount();iX++){
    //Now that we have local allocations, catch any allocation exceptions.
    try{
      data[iX] = new uint8_t[y_axis.GetBinCount()];
      memcpy(data[iX],rhs.data[iX],y_axis.GetBinCount());
    }catch (std::bad_alloc & e){
      //Bad allocation, delallocate everything we've allocated and re-throw
      for(size_t iDeAlloc = 0; iDeAlloc < iX;iDeAlloc++){
	delete [] data[iDeAlloc];
      }
      delete [] data;
      throw;
    }    
  }
}

void hist::write(std::string filename){
  std::ofstream outfile(filename.c_str());
  for(size_t iY = 0 ;iY < y_axis.GetBinCount();iY++){
    for(size_t iX = 0;iX < x_axis.GetBinCount();iX++){
      outfile << int(data[iX][iY]) << " ";
    }
    outfile << "\n";
  }
  outfile.close();
}
