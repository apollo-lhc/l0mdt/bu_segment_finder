#ifndef __MDT_H__
#define __MDT_H__

struct tube_pos {
  tube_pos(float _x, float _y, float _ht){
    x = _x;
    y = _y;
    hit_time = _ht;
  }
  float x;
  float y;
  float hit_time;
};

#endif
