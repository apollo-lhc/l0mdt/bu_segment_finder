#!/usr/bin/python
import random
import math
class tube:
    def __init__(self,x,y):
        self.x0 = x
        self.y0 = y
    def hit(self,m,b):
        x = (self.x0 + m*(self.y0 - b))/(m*m + 1)
        y = m*x + b
        r = math.sqrt((x-self.x0)*(x-self.x0) + (y - self.y0)*(y - self.y0))
        return r





noise_level = 1.00
noise_mult = 15.0/noise_level
alpha=72
slope = math.tan( alpha * (3.14159/180.0))
x0 = -200
intercept = -x0*slope
tube_r = 15

trackFile = open("track.dat","w")
trackFile.write(str(alpha))
trackFile.write(" ")
#trackFile.write(str(math.fabs(math.cos(alpha)*x0)))
trackFile.write(str(x0))


tubeFile1 = open("layer1.dat","w")
tubeFile2 = open("layer2.dat","w")

tubeFile = tubeFile1
# hit tubes
for y in [800,748.03,500,448.03]:
    if y > 600:
        tubeFile = tubeFile1
    else:
        tubeFile = tubeFile2
    for x in range(-135,135,30):
        t = tube(x,y)
        r = t.hit(slope,intercept)
        if r > tube_r:
            r = random.random()
            if (r < noise_level):
                tubeFile.write(str(x)+" "+str(y)+" "+str(r*noise_mult)+"\n")
            else:
                tubeFile.write(str(x)+" "+str(y)+" 0\n")
        else:
            tubeFile.write(str(x)+" "+str(y)+" "+str(r)+"\n")

for y in [774.02,474.02]:
    if y > 600:
        tubeFile = tubeFile1
    else:
        tubeFile = tubeFile2

    for x in range(-150,150,30):
        t = tube(x,y)
        r = t.hit(slope,intercept)
        if r > tube_r:
            r = random.random()
            if (r < noise_level):
                tubeFile.write(str(x)+" "+str(y)+" "+str(r*noise_mult)+"\n")
            else:
                tubeFile.write(str(x)+" "+str(y)+" 0\n")
        else:
            tubeFile.write(str(x)+" "+str(y)+" "+str(r)+"\n")


#ROIs

tubeFile2 = open("layer2.dat","w");

rpc_2_y = 825
rpc_1_y = 425

rpc_delta = 1.0

rpc_2_x = rpc_2_y/slope + x0
rpc_1_x = rpc_1_y/slope + x0

