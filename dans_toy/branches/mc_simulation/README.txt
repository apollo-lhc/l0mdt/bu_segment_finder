Running instructions

First run: gen_hits.py
  Simulation parameters are around line 20
  This python script generates layer1/2.dat with the tube positions and hits.

Then run: find_track
  This will print out the track for the highest legendra bin.
  It will also generate tubes.bmp and legendre.bmp
