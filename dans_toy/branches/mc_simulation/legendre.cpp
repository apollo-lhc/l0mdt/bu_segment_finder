#include "legendre.hpp"
#include "mdt.h"
#include <math.h>
#include <string.h>
#include <algorithm>
#include <stdio.h>

int32_t size_int( uint32_t bits,int32_t val){
  //Assure that nothing is beyond 32bits
  assert(bits <= 32);

  //Build a mask that will mask out bits beyond bit bits
  uint32_t mask = uint32_t(uint64_t(0x1<<bits) -1);
  //Apply mask to the int32 and return it.
  return int32_t(uint32_t(val)&mask);
}

//#define SIAW(V)  size_int(ANGLE_WIDTH,(V))
//#define SITW(V)  size_int(TRIG_WIDTH,(V))
//#define SISCW(V) size_int(SCALED_COORDINATE_WIDTH,(V))
//#define SIMW(V)  size_int(TRIG_WIDTH+SCALED_COORDINATE_WIDTH,(V))
#define SIAW(V)  (V)
#define SITW(V)  (V)
#define SISCW(V) (V)
#define SIMW(V)  (V)


void LEGENDRE_FPGA::legendre::Process_ROI(int16_t ROI_alpha,int16_t ROI_z0){
  //Get theta angle from ROI alpha angle by subtracting pi/2
  int16_t theta = SIAW(ROI_alpha - int16_t(PI*500)); //mrad
  //Find the theta of the lowest and highest bins
  theta_low  = SIAW(theta - (N_THETA_SLICES >> 1));
  theta_high = SIAW(theta + (N_THETA_SLICES >> 1) -1 );

  //Get the r0 value
  int32_t r = SISCW(int32_t(ROI_z0) * int32_t(trig_table.Get_sine(ROI_alpha)));
  //Get the value of the lowest r bin (hardcoded to 2*1/8 mm per bin)
  //  r_low = SISCW(r - (128 << (TRIG_WIDTH-1)));
  r_low = SISCW(r - (128<<(TRIG_WIDTH-1)));

#ifdef LEGENDRE_DEBUG
  //Dump the range parameters to disk
  FILE * outFile = fopen("legendre_ranges.txt","w");
  fprintf(outFile,"Theta %d mrad gives range %d to %d\n",theta,theta_low,theta_high);
  fprintf(outFile,"r %d gives lowest r bin: %d\n",r,r_low);
  fclose(outFile);
#endif
  
  
}

void LEGENDRE_FPGA::legendre::Process_trig_table(){
  //Fill the cosine and sine values for each theta slice
  for(int iBin = 0; iBin < N_THETA_SLICES;iBin++){
    slice_theta[iBin] = theta_low + iBin;
    slice_cosine[iBin] = trig_table.Get_cosine(slice_theta[iBin]);
    slice_sine[iBin] = trig_table.Get_sine(slice_theta[iBin]);
  }
  
#ifdef LEGENDRE_DEBUG
  //Dump the trig tables to disk
  FILE * outFile = fopen("legendre_trig_table.txt","w");
  for(int iBin = 0; iBin < N_THETA_SLICES;iBin++){
    fprintf(outFile,"%d %d %d %d\n",
	    iBin,
	    slice_theta[iBin],
	    slice_cosine[iBin],
	    slice_sine[iBin]);
  }
  fclose(outFile);
#endif
}


void LEGENDRE_FPGA::legendre::Process_hit(const MDT_hit_t &hit){
  int32_t R_r0;
  int32_t mult_x;
  int32_t mult_x_sub;
  int32_t mult_y;
  int32_t mult_y_add;
  int8_t  bin;
  int8_t  bin_round_bit;
  bool bin_out_of_range;
#ifdef LEGENDRE_DEBUG
  FILE * outFile = fopen("Slice_127.txt","a");
#endif  
  //Loop over theta slices (parallel in vhdl)
  for(int iSlice = 0; iSlice < N_THETA_SLICES;iSlice++){
    //Loop over + and - R
    for(int iSign = -1; iSign <= 1; iSign+=2){
      //Multiply byt 2^(TRIG_WIDTH-1) because we need to scale to the x*cos scaling, but since trig width is signed, we scale by one less
      //Handle +R
      R_r0 = SIMW(r_low +   
		  iSign*(int32_t(hit.R) << (TRIG_WIDTH-1)));

      //This math is done this way to look like the VHDL
      mult_x     = SIMW(int32_t(hit.x) * slice_cosine[iSlice]);
      mult_x_sub = SIMW(mult_x - R_r0);
      mult_y     = SIMW(int32_t(hit.y) * slice_sine[iSlice]);
      mult_y_add = SIMW(mult_x_sub + mult_y);

      //Generate the r-bin this would fall in
      bin = (mult_y_add >> (TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT))& 0x7F ;
      bin_round_bit = (mult_y_add >> (TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT -1))& 0x1 ;
      //Check if this is in range
      bin_out_of_range = !!(mult_y_add >> (7+TRIG_WIDTH-1 + BINNING_BITSHIFT_COUNT)); //7 is the bin width in R

#ifdef LEGENDRE_DEBUG
      if(iSlice == 127){
	fprintf(outFile,"%d %d %d %d %d %d %d %d %d %d\n",
		r_low,
		hit.R,
		iSign,
		R_r0,
		mult_x,
		mult_x_sub,
		mult_y,
		mult_y_add,
		bin,
		bin_out_of_range);
      }
#endif      
      if( ! bin_out_of_range ) {
	//Update the max value (setup to match VHDL)
#ifdef DOUBLE_LINE
	if((bin_round_bit == 1) && (bin < 127) &&  //CHecking for if NN is up
	   ( slice_max_val[iSlice] == legendre_matrix[iSlice][bin+1])){
	  //Give priority to the actual bin value
	  if( slice_max_val[iSlice] == legendre_matrix[iSlice][bin]){
	    slice_max_val[iSlice]++;
	    slice_max_val_r[iSlice] = bin;	    
	  }else{
	    //if the actual bin isn't a new max, check the rounded bin
	    slice_max_val[iSlice]++;
	    slice_max_val_r[iSlice] = bin+1;	    	    
	  }

	}else if((bin_round_bit == 0) && (bin > 0) && //Checking if NN is down
		 ( slice_max_val[iSlice] == legendre_matrix[iSlice][bin-1])){
	  //Give priority to the actual bin value
	  if( slice_max_val[iSlice] == legendre_matrix[iSlice][bin]){
	    slice_max_val[iSlice]++;
	    slice_max_val_r[iSlice] = bin;	    
	  }else{
	    //if the actual bin isn't a new max, check the rounded bin
	    slice_max_val[iSlice]++;
	    slice_max_val_r[iSlice] = bin-1;	    	    
	  }

	}else if( slice_max_val[iSlice] == legendre_matrix[iSlice][bin]){
	  slice_max_val[iSlice]++;
	  slice_max_val_r[iSlice] = bin;	    
	}
#else	
	if( slice_max_val[iSlice] == legendre_matrix[iSlice][bin]){
	  slice_max_val[iSlice]++;
	  slice_max_val_r[iSlice] = bin;	    
	}
#endif
	//Increment bin
	if( legendre_matrix[iSlice][bin] != 0xF ) {
	  legendre_matrix[iSlice][bin]++;
#ifdef DOUBLE_LINE
	  //increment the rounded bin as well
	  if((bin_round_bit == 1) && (bin < 127)){
	    legendre_matrix[iSlice][bin+1]++;
	  }
	  if((bin_round_bit == 0) && (bin > 0)){
	    legendre_matrix[iSlice][bin-1]++;
	  }
#endif
	}
      }
    }
  }
#ifdef LEGENDRE_DEBUG
  fclose(outFile);
#endif  
}

void LEGENDRE_FPGA::legendre::Process_search(){

#ifdef LEGENDRE_DEBUG
  //Dump the the search to disk
  FILE * outFile1 = fopen("final_search_theta.txt","w");
  FILE * outFile2 = fopen("final_search_r.txt","w");
  FILE * outFile3 = fopen("final_search_val.txt","w");
  for(int iSlice = 0; iSlice < N_THETA_SLICES;iSlice++){
    fprintf(outFile1,"%03d ",slice_theta[iSlice]);
    fprintf(outFile2,"%03d ",slice_max_val_r[iSlice]);
    fprintf(outFile3,"%03d ",slice_max_val[iSlice]);
  }
  fprintf(outFile1,"\n"); fprintf(outFile2,"\n"); fprintf(outFile3,"\n");
#endif
  
  //Do a binary search to move the maximum value into the 0th entry
  for(int iRound = (N_THETA_SLICES>>1); iRound > 0 ; iRound>>=1){
    for(int iSlice = 0; iSlice < iRound;iSlice++){
      if(slice_max_val[iSlice] < slice_max_val[iSlice + iRound]){
	slice_max_val[iSlice]   = slice_max_val[iSlice + iRound];
	slice_max_val_r[iSlice] = slice_max_val_r[iSlice+ iRound];
	slice_theta[iSlice] = slice_theta[iSlice+ iRound];	
      }
#ifdef LEGENDRE_DEBUG
      fprintf(outFile1,"%03d ",slice_theta[iSlice]);
      fprintf(outFile2,"%03d ",slice_max_val_r[iSlice]);
      fprintf(outFile3,"%03d ",slice_max_val[iSlice]);
#endif
    }
#ifdef LEGENDRE_DEBUG
    fprintf(outFile1,"\n"); fprintf(outFile2,"\n"); fprintf(outFile3,"\n");
#endif
  }
#ifdef LEGENDRE_DEBUG
  fclose(outFile1); fclose(outFile2); fclose(outFile3);
#endif
}

//Sorting function to return hit1 time < hit2 time
bool MDT_sort(const MDT_hit_t & hit1, const MDT_hit_t & hit2){
  return hit1.arrival_time < hit2.arrival_time;
}


int LEGENDRE_FPGA::legendre::Run(int16_t ROI_alpha,int16_t ROI_z0, std::vector<MDT_hit_t> hit) {
  time = 0;

  //reset module
  memset(slice_theta    ,0,N_THETA_SLICES*sizeof(int16_t));
  memset(slice_cosine   ,0,N_THETA_SLICES*sizeof(int16_t));
  memset(slice_sine     ,0,N_THETA_SLICES*sizeof(int16_t));
  memset(slice_max_val  ,0,N_THETA_SLICES*sizeof(int16_t));
  memset(slice_max_val_r,0,N_THETA_SLICES*sizeof(int16_t));
  for(int iSlice = 0; iSlice < N_THETA_SLICES;iSlice++){
    memset(legendre_matrix[iSlice],0,N_R_BINS);
  }
  
  
  //Use the ROI to setup the r and theta ranges
  Process_ROI(ROI_alpha,ROI_z0);
  time += 3;



  //Load the trig table
  Process_trig_table();
  time += N_THETA_SLICES/TRIG_LOOKUP_COUNT;


#ifdef LEGENDRE_DEBUG
  //Delete this file(sorta)
  FILE * outFile = fopen("Slice_127.txt","w");
  fclose(outFile);
#endif  
  //Sort the hits by arrival_time (this take no time in the FPGA because... arrival time)
  std::sort(hit.begin(),hit.end(),MDT_sort);
  //Process the hits
  for(size_t iMDT_hit = 0; iMDT_hit < hit.size();){
    //CHeck to see if the FPGA time is late enough that we have the hit
    //THis is only for the latency (time) measurement
    if(hit[iMDT_hit].arrival_time < time){
      Process_hit(hit[iMDT_hit]);
      time += 2;
      iMDT_hit++;
    }else{
      time++;
    }
  }
  //Add the time for the last hit's pipelining
  time += 7;
  //Dump the legendre table
  dump_matrix("matrix.txt");

  

  //Do the final binary search
  Process_search();
  time+=int(ceil(log2(N_THETA_SLICES)));

  //Generate output variables
  return 1;
}

int16_t LEGENDRE_FPGA::legendre::Get_fit_alpha(){
  return slice_theta[0] + 500.0*PI;
}

int16_t LEGENDRE_FPGA::legendre::Get_fit_z0(){
  return ((slice_max_val_r[0]<<(TRIG_WIDTH)) + r_low)/(trig_table.Get_sine(this->Get_fit_alpha()));
}

void LEGENDRE_FPGA::legendre::dump_matrix(std::string outFileName){
  //Dump the matrix to a file
  FILE * outFile = fopen(outFileName.c_str(),"w");
  for(int iTheta = 0; iTheta < N_THETA_SLICES;iTheta++){
    for(int irbin = 0; irbin < N_R_BINS;irbin++){
      fprintf(outFile,"%d %d %u\n",iTheta,irbin,legendre_matrix[iTheta][irbin]);
    }
  }
  fclose(outFile);
}

LEGENDRE_FPGA::legendre::legendre():slice_theta(NULL),
				    slice_cosine(NULL),
				    slice_sine(NULL),
				    slice_max_val(NULL),
				    slice_max_val_r(NULL),
				    legendre_matrix(NULL){
  //alloate the pointers to rbin arrays
  legendre_matrix = new uint8_t*[N_THETA_SLICES];
  //zero the valid pointer
  memset(legendre_matrix,0,N_THETA_SLICES * sizeof(uint8_t*));  
  for(int i = 0; i < N_THETA_SLICES;i++){
    //allocate the rbin array
    legendre_matrix[i] = new uint8_t[N_R_BINS];
  }

  slice_theta     = new int16_t[N_THETA_SLICES];
  slice_cosine    = new int16_t[N_THETA_SLICES];
  slice_sine      = new int16_t[N_THETA_SLICES];
  slice_max_val   = new int16_t[N_THETA_SLICES];
  slice_max_val_r = new int16_t[N_THETA_SLICES];
  


  trig_table.Init_trig_tables(ANGLE_WIDTH,TRIG_WIDTH);
  
}

LEGENDRE_FPGA::legendre::~legendre(){
  //only process an allocated matrix
  if(legendre_matrix != NULL){
    //deallocate valid rbin arrays
    for(int i = 0; i < N_THETA_SLICES;i++){
      //check if this rbin array is already allocated
      if(legendre_matrix[i] != NULL){
	//deallocate
	delete [] legendre_matrix[i];
      }
    }
    //deallocate array of pointers to rbin arrays
    delete [] legendre_matrix;
  }

  if(slice_theta != NULL){
    delete [] slice_theta;
  }
  if(slice_cosine != NULL){
    delete [] slice_cosine;
  }
  if(slice_sine != NULL){
    delete [] slice_sine;
  }
  if(slice_max_val != NULL){
    delete [] slice_max_val;
  }
  if(slice_max_val_r != NULL){
    delete [] slice_max_val_r;
  }

}





