//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov 29 15:51:21 2016 by ROOT version 6.06/08
// from TTree mdt/mdt
// found on file: output.root
//////////////////////////////////////////////////////////

#ifndef mySegments_h
#define mySegments_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
using std::vector;

class mySegments {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

  // begin jb stuff

  TFile *hfile;

  //  TH1 *h_seg_pos_glob_z_b;
  TH1F * slope;
  TH1F * slope_mrad;
  TH1F * intercept;
  TH1F * intercept_mm;
  TH1F * exec_time;
  TH2F * slope_intercept;

  // end jb stuff

  // Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<TString> *chamber_b;
   vector<double>  *seg_slope_b;
   vector<double>  *seg_intercept_b;
   vector<double>  *seg_pos_loc_x_b;
   vector<double>  *seg_pos_loc_y_b;
   vector<double>  *seg_pos_loc_z_b;
   vector<double>  *seg_pos_glob_x_b;
   vector<double>  *seg_pos_glob_y_b;
   vector<double>  *seg_pos_glob_z_b;
   vector<vector<double> > *hit_id_b;
   vector<vector<double> > *hit_pos_loc_x_b;
   vector<vector<double> > *hit_pos_loc_y_b;
   vector<vector<double> > *hit_pos_loc_z_b;
   vector<vector<double> > *hit_pos_glob_x_b;
   vector<vector<double> > *hit_pos_glob_y_b;
   vector<vector<double> > *hit_pos_glob_z_b;
   vector<vector<double> > *hit_r_b;
   vector<vector<double> > *hit_on_segment_b;

   // List of branches
   TBranch        *b_chamber_b;   //!
   TBranch        *b_seg_slope_b;   //!
   TBranch        *b_seg_intercept_b;   //!
   TBranch        *b_seg_pos_loc_x_b;   //!
   TBranch        *b_seg_pos_loc_y_b;   //!
   TBranch        *b_seg_pos_loc_z_b;   //!
   TBranch        *b_seg_pos_glob_x_b;   //!
   TBranch        *b_seg_pos_glob_y_b;   //!
   TBranch        *b_seg_pos_glob_z_b;   //!
   TBranch        *b_hit_id_b;   //!
   TBranch        *b_hit_pos_loc_x_b;   //!
   TBranch        *b_hit_pos_loc_y_b;   //!
   TBranch        *b_hit_pos_loc_z_b;   //!
   TBranch        *b_hit_pos_glob_x_b;   //!
   TBranch        *b_hit_pos_glob_y_b;   //!
   TBranch        *b_hit_pos_glob_z_b;   //!
   TBranch        *b_hit_r_b;   //!
   TBranch        *b_hit_on_segment_b;   //!

   mySegments(TTree *tree=0);
   virtual ~mySegments();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef mySegments_cxx
mySegments::mySegments(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("monte_carlo.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("monte_carlo.root");
      }
      f->GetObject("mdt",tree);

   }
   Init(tree);
}

mySegments::~mySegments()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t mySegments::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t mySegments::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void mySegments::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   chamber_b = 0;
   seg_slope_b = 0;
   seg_intercept_b = 0;
   seg_pos_loc_x_b = 0;
   seg_pos_loc_y_b = 0;
   seg_pos_loc_z_b = 0;
   seg_pos_glob_x_b = 0;
   seg_pos_glob_y_b = 0;
   seg_pos_glob_z_b = 0;
   hit_id_b = 0;
   hit_pos_loc_x_b = 0;
   hit_pos_loc_y_b = 0;
   hit_pos_loc_z_b = 0;
   hit_pos_glob_x_b = 0;
   hit_pos_glob_y_b = 0;
   hit_pos_glob_z_b = 0;
   hit_r_b = 0;
   hit_on_segment_b = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("chamber_b", &chamber_b, &b_chamber_b);
   fChain->SetBranchAddress("seg_slope_b", &seg_slope_b, &b_seg_slope_b);
   fChain->SetBranchAddress("seg_intercept_b", &seg_intercept_b, &b_seg_intercept_b);
   fChain->SetBranchAddress("seg_pos_loc_x_b", &seg_pos_loc_x_b, &b_seg_pos_loc_x_b);
   fChain->SetBranchAddress("seg_pos_loc_y_b", &seg_pos_loc_y_b, &b_seg_pos_loc_y_b);
   fChain->SetBranchAddress("seg_pos_loc_z_b", &seg_pos_loc_z_b, &b_seg_pos_loc_z_b);
   fChain->SetBranchAddress("seg_pos_glob_x_b", &seg_pos_glob_x_b, &b_seg_pos_glob_x_b);
   fChain->SetBranchAddress("seg_pos_glob_y_b", &seg_pos_glob_y_b, &b_seg_pos_glob_y_b);
   fChain->SetBranchAddress("seg_pos_glob_z_b", &seg_pos_glob_z_b, &b_seg_pos_glob_z_b);
   fChain->SetBranchAddress("hit_id_b", &hit_id_b, &b_hit_id_b);
   fChain->SetBranchAddress("hit_pos_loc_x_b", &hit_pos_loc_x_b, &b_hit_pos_loc_x_b);
   fChain->SetBranchAddress("hit_pos_loc_y_b", &hit_pos_loc_y_b, &b_hit_pos_loc_y_b);
   fChain->SetBranchAddress("hit_pos_loc_z_b", &hit_pos_loc_z_b, &b_hit_pos_loc_z_b);
   fChain->SetBranchAddress("hit_pos_glob_x_b", &hit_pos_glob_x_b, &b_hit_pos_glob_x_b);
   fChain->SetBranchAddress("hit_pos_glob_y_b", &hit_pos_glob_y_b, &b_hit_pos_glob_y_b);
   fChain->SetBranchAddress("hit_pos_glob_z_b", &hit_pos_glob_z_b, &b_hit_pos_glob_z_b);
   fChain->SetBranchAddress("hit_r_b", &hit_r_b, &b_hit_r_b);
   fChain->SetBranchAddress("hit_on_segment_b", &hit_on_segment_b, &b_hit_on_segment_b);
   Notify();
}

Bool_t mySegments::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void mySegments::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t mySegments::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef mySegments_cxx
