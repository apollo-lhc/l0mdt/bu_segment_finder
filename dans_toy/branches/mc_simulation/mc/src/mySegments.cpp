#define mySegments_cxx
#include "mySegments.h"
#include "MuonFixedId.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
using std::cout;
using std::endl;
#include <fstream>
using std::ofstream;

#include <math.h>
#include <stdint.h>

#include "legendre.hpp"
#include "mdt.h"

void mySegments::Loop()
{
  MuonCalib::MuonFixedId myID;

  TFile * OutFile = new TFile("legendre_output.root","RECREATE");
  slope = new TH1F("slope","slope error",100,-1,1);
  slope_mrad = new TH1F("slope_mrad","slope error (mrad)",100,-0.05,0.05);
  
  intercept = new TH1F("intercept","intercept error",100,-1,1);
  intercept_mm = new TH1F("intercept_mm","intercept error (mm)",100,-10,10);
  
  slope_intercept = new TH2F("slope_intercept","error (urad,mm)",100,-0.05,0.05,100,-10,10);
  
  exec_time = new TH1F("exec_time","Execution time (ns@360Mhz)",100,0,1000);

  LEGENDRE_FPGA::legendre * Legendre_Transform = new LEGENDRE_FPGA::legendre;
  
  if (fChain == 0) return;
  
  Long64_t nentries = fChain->GetEntriesFast();
  cout << nentries << " events to process." << endl;
  
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    //for (Long64_t jentry=0; jentry<20;jentry++) {
  //  for (Long64_t jentry=0; jentry<1000;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    fChain->GetEntry(jentry);

    for (int iSeg = 0; iSeg < seg_pos_glob_z_b->size();iSeg++){
      //      printf("Event: %d Segment: %d\n",jentry,iSeg);

      const size_t buffer_size = 256;
      char buffer[buffer_size + 1];
      buffer[buffer_size] = 0;

      //Get the ROI
      //Generate the angle in mrad relative to the z axis
      double alpha_truth = atan(seg_slope_b->at(iSeg))* 1000.0; 
      //Intercept of the z axis (where y is zero) for the track
      double z0_truth    = XY_SCALING*(seg_pos_loc_y_b->at(iSeg) - (seg_pos_loc_z_b->at(iSeg)/seg_slope_b->at(iSeg)));

      double alpha_smeared = alpha_truth;
      double z0_smeared = z0_truth;
      

      //Get the hits
      std::vector<MDT_hit_t> hit;
      for(int iHit = 0;iHit < hit_pos_loc_z_b->at(iSeg).size();iHit++){
	hit.push_back(MDT_hit_t(XY_SCALING*hit_pos_loc_y_b->at(iSeg)[iHit],
				XY_SCALING*hit_pos_loc_z_b->at(iSeg)[iHit],
				XY_SCALING*hit_r_b->at(iSeg)[iHit]));
      }

      Legendre_Transform->Run(alpha_smeared,z0_smeared,hit);
      
      slope->Fill((alpha_truth - Legendre_Transform->Get_fit_alpha())/alpha_truth);
      slope_mrad->Fill(0.001*(alpha_truth - Legendre_Transform->Get_fit_alpha()));

      intercept->Fill((z0_truth - Legendre_Transform->Get_fit_z0())/z0_truth);
      intercept_mm->Fill((z0_truth - Legendre_Transform->Get_fit_z0())/(XY_SCALING));

      slope_intercept->Fill(0.001*(alpha_truth - Legendre_Transform->Get_fit_alpha()),
			    (z0_truth - Legendre_Transform->Get_fit_z0())/(XY_SCALING));
      
      exec_time->Fill(Legendre_Transform->Get_run_time()*1.0/(0.360));
      
      //
      //
      //snprintf(buffer,buffer_size,"ROI_%d_%d.txt",jentry,iSeg);
      //FILE * track_file = fopen(buffer,"w");
      //fprintf(track_file,"%e %e\n",
      //	      atan(seg_slope_b->at(iSeg))*180.0/3.1415,
      //	      seg_pos_loc_y_b->at(iSeg) - (seg_pos_loc_z_b->at(iSeg)/seg_slope_b->at(iSeg)) );
      //
      //fclose(track_file);

//      //Generate the hits file      
//      snprintf(buffer,buffer_size,"tubes_%d_%d.txt",jentry,iSeg);
//      FILE * hits_file = fopen(buffer,"w");            
//      for(int iHit = 0;iHit < hit_pos_loc_z_b->at(iSeg).size();iHit++){	
//	fprintf(hits_file,"%e %e %e\n",
//		hit_pos_loc_y_b->at(iSeg)[iHit],
//		hit_pos_loc_z_b->at(iSeg)[iHit],
//		hit_r_b->at(iSeg)[iHit]);
//      }
//      fclose(hits_file);
    }
  } // end of event loop
  
  // All done, time to clean up  
  OutFile->Write();
  OutFile->Close();
  delete OutFile;
  //  delete slope;
  //  delete intercept;
  //  delete exec_time;
}
