#include <vector>
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <assert.h>
#include "mdt.h"
#include "trig_table.hpp"

#define PI 3.14159

#define N_THETA_SLICES 128
#define N_R_BINS 128
#define BINNING_BITSHIFT_COUNT 1

#define TRIG_WIDTH 12
#define ANGLE_WIDTH 12
#define SCALING_WIDTH 3
#define COORDINATE_WIDTH 12
#define SCALED_COORDINATE_WIDTH (SCALING_WIDTH + COORDINATE_WIDTH)
#define XY_SCALING 8

#define TRIG_LOOKUP_COUNT 16

namespace LEGENDRE_FPGA {
  
  class legendre{
  public:
    legendre();
    ~legendre();
    int  Run(int16_t ROI_alpha, // signed mrad
	     int16_t ROI_z0,    // units: mm * 8  (i.e. a value of 1 means 0.125mm
	     std::vector<MDT_hit_t> hit); // units: mm * 8
    int16_t Get_fit_alpha(); // units: mm * 8
    int16_t Get_fit_z0(); // units: mm * 8
    uint16_t Get_run_time(){return time;};    
  private:
    void dump_matrix(std::string outFileName);
    
    void Process_ROI(int16_t ROI_alpha,int16_t ROI_z0);
    void Process_trig_table();
    void Process_hit(const MDT_hit_t & hit);
    void Process_search();
    
    //Estimated run time
    uint16_t time;
    
    //
    int16_t theta_low;
    int16_t theta_high;
    int32_t r_low;

    //Legendre compute arrays
    int16_t * slice_theta;
    int16_t * slice_cosine;
    int16_t * slice_sine;
    int16_t * slice_max_val;
    int16_t * slice_max_val_r;

    //Legendre matrix
    uint8_t ** legendre_matrix;
    
    //Trig lookup tables
    trig_table_t trig_table;
  }; //class legendre
}; //namespace LEGENDRE_FPGA

  
