#include "trig_table.hpp"
#include <math.h>


LEGENDRE_FPGA::trig_table_t::trig_table_t():angle_bit_width(0),output_bit_width(0),trig_table_cosine(NULL),trig_table_sine(NULL){}

LEGENDRE_FPGA::trig_table_t::~trig_table_t(){
  clear();
}
void LEGENDRE_FPGA::trig_table_t::clear(){
  //Deallocate trig tables if they are set
  if( trig_table_cosine != NULL){
    delete [] trig_table_cosine;
  }
  if( trig_table_sine != NULL){
    delete [] trig_table_sine;
  }
}

void LEGENDRE_FPGA::trig_table_t::Init_trig_tables(size_t angle_bit_width, size_t output_bit_width){
  this->angle_bit_width = angle_bit_width;
  this->output_bit_width = output_bit_width;
  angle_count = 0x1<<angle_bit_width;

  //allocate the arrays
  trig_table_cosine = new int16_t[angle_count];
  trig_table_sine   = new int16_t[angle_count];

  uint16_t mask = (1<<angle_bit_width)-1;
  int16_t scaling_factor = (1<<(output_bit_width-1));
  
  for(int i = -1 << (angle_bit_width-1) ; i < 1<<(angle_bit_width-1);i++){
    uint16_t iLookup = uint16_t(i & mask);

    trig_table_cosine[iLookup] = scaling_factor*cos(i*0.001);
    if(trig_table_cosine[iLookup] >= scaling_factor){
      trig_table_cosine[iLookup] = scaling_factor-1;
    }

    trig_table_sine[iLookup]   = (1<<(output_bit_width-1))*sin(i*0.001);
    if(trig_table_sine[iLookup] >= scaling_factor){
      trig_table_sine[iLookup] = scaling_factor-1;
    }
  }
}

int16_t LEGENDRE_FPGA::trig_table_t::Get_cosine(int16_t angle){
  uint16_t mask = (1<<angle_bit_width)-1;
  uint16_t iLookup = uint16_t(angle & mask);
  return trig_table_cosine[iLookup];
}

int16_t LEGENDRE_FPGA::trig_table_t::Get_sine(int16_t angle){
  uint16_t mask = (1<<angle_bit_width)-1;
  uint16_t iLookup = uint16_t(angle & mask);
  return trig_table_sine[iLookup];
}
