#!/usr/bin/env python

import argparse

from math import *

class hP:
    def __init__(self):
        self.theta_bin = -1
        self.theta_bin_end = -1
        self.hit_id = -1
        self.r_add  = 0


def bitRange(val,msb,lsb):
    #drop the bits below the lsb
    val = int(val) >> int(lsb)
    #remove the upper bits
    msb = int(msb) - int(lsb)
    mask = 0
    for ibit in range(0,msb):
        mask = int(mask | (0x1 << int(ibit)))
    val = int(val & mask)
    return int(val)
    
    
def main():
    parser = argparse.ArgumentParser(description='Helper to determine hit processor mappings')

    parser.add_argument('-n','--hproc', help='hit processor count', type=int, default=128)
    parser.add_argument('-m','--mdt_hits', help='hit count', type=int, default=8)
    parser.add_argument('-t','--theta_bins', help='theta bins', type=int, default=128)
    #    parser.add_argument('-p','--parallelism', help='parallelism', type=int, default=1)
    args = vars(parser.parse_args())

    hp_count   = int(args["hproc"])
    hit_count  = int(args["mdt_hits"])
    theta_bins = int(args["theta_bins"])    
    #parallel   = int(log(args["parallelism"],2))

    if hp_count < 2*hit_count:
        print "Too many hits for hp_count!\n"
        return



    #compute parallelization.  4 subtractions in parallel + result look up
    parallel   = 0
    if   hp_count >= hit_count << (1+5):
        parallel = 5    
    elif hp_count >= hit_count << (1+4):
        parallel = 4
    elif hp_count >= hit_count << (1+3):
        parallel = 3
    elif hp_count >= hit_count << (1+2):
        parallel = 2
    elif hp_count >= hit_count << (1+1):
        parallel = 1
    else:
        parallel = 0
        

    parallel = 1
    if hit_count << (1 + parallel) > hp_count:
        print "Too many hits(%d) for hp_count(%d) with fixed parallelization x %d!\n" % (hit_count,hp_count,1<<parallel)
        return
        
        
    print "Parallelizing by ",1<<parallel
    
    hps = [hP() for i in range(hp_count)]

    hp_count_in_use = hit_count << (parallel+1)
    
    for ihp in range(0,hp_count_in_use):         
        hps[ihp].r_add = ihp&0x1 # lsb is the +/- degen
        hps[ihp].hit_id = ihp >> (1+( parallel)) # 1 for +/- bit, parallel for parallization

        # ihp&0x1 to give each +/- a separate theta
        # bitRange(ihp,1+parallel,1) gives the id of the parallization of this hit
        # Shifting that by the log2(thetabins) - parallel is the same as multiplying by theta_bins/parallel since theta_bins is a power of 2
        # Adding the 1+parrallel right bitshift of ihp and multiplying by two gives an offset for each half-hit-pair so that they start on their own thetas
        hps[ihp].theta_bin     = ihp&0x1 | (int(bitRange(ihp  ,1+parallel,1)) << ( int(log(theta_bins,2)) - parallel)) + int(bitRange(ihp,32,1+parallel)<<1)
        # this is the same as theta_bin, but we have ihp+2 instead of ihp to skip the adjacent +/- pair and get the parallelization bound from the next parallel hit
        hps[ihp].theta_bin_end = ihp&0x1 | (int(bitRange(ihp+2,1+parallel,1)) << ( int(log(theta_bins,2)) - parallel)) + int(bitRange(ihp,32,1+parallel)<<1)

        #print deubbging info
        
        #print str(ihp).zfill(3),                                                
        #print str(hps[ihp].hit_id).zfill(3),
        #print str(hps[ihp].r_add),
        #print str(hps[ihp].theta_bin).zfill(3),
        #print str(hps[ihp].theta_bin_end).zfill(3)

        
    print "iTheta   Hp     hit_ID   +/-    iTheta_start  iTheta_end"
    for iTheta in range(0,theta_bins):
    #for iTheta in range(0,10):
        print str(iTheta).zfill(3),
        this_hp = -1
        for iHP in range(0,len(hps)):
            if int(hps[iHP].theta_bin) == int(iTheta):
                this_hp = iHP
                break
            
        if this_hp != -1:
            print "    ",
            print str(this_hp).zfill(3),
            print ": ",
            print str(hps[iHP].hit_id).zfill(3),
            print "     ",
            print str(hps[iHP].r_add),
            print "        ",
            print str(hps[iHP].theta_bin).zfill(3),
            print "        ",
            print str(hps[iHP].theta_bin_end).zfill(3)
        else:
            print " "
            

            

                
                



        
if __name__ == '__main__':
    main()
