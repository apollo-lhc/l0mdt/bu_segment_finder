#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <stdint.h>

#include <math.h>
#include <stdlib.h>     /* abs */

#include "mdt.h"
#define PI 3.14159

#include "legendre.hpp"

void read_pos_file(std::vector<MDT_hit_t> & pos, const char * const filename){
  if( filename == NULL){
    throw std::string("Bad filename in read_pos_file\n");
  }
  std::ifstream inFile(filename);
  while(!inFile.eof()){
    double x,y,ht;
    inFile >> x;
    inFile >> y;
    inFile >> ht;
    ht = fabs(ht);
    if(!inFile.eof()){
      pos.push_back(MDT_hit_t(x*XY_SCALING,
			      y*XY_SCALING,
			      ht*XY_SCALING));
    }
  }
}

void read_track_file(double & m,double &alpha, double & x0, const char * const filename){
  std::ifstream inFile(filename);
  inFile >> alpha;
  inFile >> x0;
  x0 *= XY_SCALING;  
  m = tan(alpha*PI/180.0);
  alpha*= 1000.0*PI/180.0;

}

void generate_ROI(double theta,double r0,
		  double theta_smear, double r0_smear,
		  double & theta_smeared, double & r0_smeared){
  theta_smeared = theta + theta_smear*(1.0-2*double(rand())/double(RAND_MAX));
  r0_smeared    = r0 + r0_smear*(1.0-2*double(rand())/double(RAND_MAX));
  //  printf("Theta %f smeared to %f\n",theta,theta_smeared);
  //  printf("r0    %f smeared to %f\n",r0,r0_smeared);
}

int main(int argc, char ** argv){

  //Parsing of real track information from track.dat
  double z0_truth    = 0;     //Intercept of the z axis (where y is zero) for the track
  double m_truth     = 0;      //Slope of the track
  double alpha_truth = 0;  //Angle between track and the chamber Z axis
  //Read in the track values
  read_track_file(m_truth,alpha_truth,z0_truth,"ROI.dat");  

  printf("True track at alpha=%f ; z0=%f\n",alpha_truth,z0_truth);

  double alpha_smeared;
  double z0_smeared;
  generate_ROI(alpha_truth,z0_truth,
	       20, 20,
	       alpha_smeared, z0_smeared);
  printf("Smeared ROI alpha=%f ; z0=%f\n",alpha_smeared,z0_smeared);

  
  //Load hits
  std::vector<MDT_hit_t> tube;
  read_pos_file(tube,"super_module.dat");
  std::vector<MDT_hit_t> hit_tube;
  for(size_t iHit = 0; iHit < tube.size();iHit++){
    if(tube[iHit].R > 0.01){
      hit_tube.push_back(tube[iHit]);
    }
  }
  printf("Processing %d hits\n",int(hit_tube.size()));
  

  LEGENDRE_FPGA::legendre * Legendre_Transform = new LEGENDRE_FPGA::legendre;
  
  Legendre_Transform->Run(alpha_truth,z0_truth,hit_tube);
  //Legendre_Transform->Run(alpha_smeared,z0_smeared,hit_tube);
  
  printf("Track at alpha=%d ; z0=%d\n",Legendre_Transform->Get_fit_alpha(),Legendre_Transform->Get_fit_z0());
  printf("Computation took %d clock ticks (%f ns)\n",Legendre_Transform->Get_run_time(),Legendre_Transform->Get_run_time()*1.0/(0.360));
  



  
  return 0;
  
}
