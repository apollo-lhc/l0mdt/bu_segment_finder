#ifndef __MDT_H__
#define __MDT_H__

struct MDT_hit_t {
  MDT_hit_t(float _x, float _y, float _R,float _arrival_time = 0){
    x = _x;
    y = _y;
    R = _R;
    arrival_time = _arrival_time;
  }
  int32_t x;
  int32_t y;
  int32_t R;
  int32_t arrival_time;
};

#endif
