#include <stdlib.h>
#include <stdint.h>

namespace LEGENDRE_FPGA {

  class trig_table_t{
  public:    
    trig_table_t();
    ~trig_table_t();
    void Init_trig_tables(size_t angle_bit_width, size_t output_bit_width);
    int16_t Get_cosine(int16_t angle);
    int16_t Get_sine(int16_t angle);
    uint16_t Get_time();
  private:
    void clear();
    //Lookup tables for sine and cosine
    size_t angle_bit_width;
    size_t output_bit_width;
    size_t angle_count;
    int16_t * trig_table_cosine;
    int16_t * trig_table_sine;
  }; //class trig_tables
  
}; //namespace LEGENDRE_FPGA

  
