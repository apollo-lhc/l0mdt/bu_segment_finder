#ifndef __CANVAS_HPP__
#define __CANVAS_HPP__

#include "hist.hpp"
#include "bitmap/bitmap_image.hpp"

class canvas{
public:
  canvas(hist_axis * _x, hist_axis * _y);
  void Add(hist * h,int color);
  ~canvas();
  void WriteBitmap(const std::string & filename);
private:
  static const uint8_t bg_color[3];
  static const uint8_t tick_color[3];
  static const int padding;  
  static const int ticks;

  canvas();
  hist_axis x_axis;
  hist_axis y_axis;  
  bitmap_image * image;
};
#endif
