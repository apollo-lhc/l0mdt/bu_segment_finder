#ifndef __HIST_AXIS_HPP__
#define __HIST_AXIS_HPP__

#include <iostream>

class hist_axis{
public:
  hist_axis(size_t _size,double _min, double _max);
  hist_axis(const hist_axis & rhs){copy(rhs);};
  hist_axis & operator=(const hist_axis & rhs){copy(rhs);return *this;};
  size_t FindBin(double x);
  size_t GetBinCount(){return size;};
  double GetBinCenter(size_t i){return ((scale*double(i)) + shift);};
  double GetMax(){return max;};
  double GetMin(){return min;};
  double GetDelta(){return scale;};
private:
  void copy(const hist_axis & rhs);
  size_t size;
  double  max;
  double  min;
  double  scale; // real = bin * scale + shift
  double  shift;
};
#endif
