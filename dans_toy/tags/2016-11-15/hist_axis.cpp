#include "hist_axis.hpp"

#include <string>
#include <exception>

#include <stdio.h>

hist_axis::hist_axis(size_t _size,double _min, double _max){
  if (size == 0){
    throw std::string("hist_axis::hist_axis: zero size");    
  } else if (_min > _max){
    throw std::string("hist_axis::hist_axis: min > max");    
  }
  size = _size;
  max = _max;
  min = _min;

  scale = (max-min)/double(size);
  shift = min;
}

size_t hist_axis::FindBin(double x){
  if (( x >= max) || (x < min)){
    throw std::string("hist_axis::FindBin: Out of range");
  }
  double ret = x;
  ret = (ret - shift)/scale;
  return ret;
}


void hist_axis::copy(const hist_axis &rhs){
  size  = rhs.size;
  max   = rhs.max;
  min   = rhs.min;
  scale = rhs.scale;
  shift = rhs.shift;
}
