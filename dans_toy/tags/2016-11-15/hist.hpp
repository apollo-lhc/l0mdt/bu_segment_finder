#ifndef __HIST_HPP__
#define __HIST_HPP__

#include "hist_axis.hpp"

#include <stdint.h>
#include <string>

class hist{
public:
  hist(size_t x_size,double x_min, double x_max,
       size_t y_size,double y_min, double y_max);
  hist(const hist &rhs);
  hist & operator=(const hist &rhs){copy(rhs); return *this;}
  ~hist(){  clear();}
  void Fill(double x, double y, uint8_t weight);
  void Set(double x, double y, uint8_t val);
  hist_axis * GetX(){return &x_axis;}
  hist_axis * GetY(){return &y_axis;}
  void GetMaxBin(size_t &x, size_t &y,uint8_t &max);
  uint8_t GetBin(size_t x, size_t y){return data[x][y];};
  void write(std::string filename);
private:
  void copy(const hist & rhs);
  hist();
  void clear();
  uint8_t ** data;
  hist_axis x_axis;
  hist_axis y_axis;  
};
#endif
