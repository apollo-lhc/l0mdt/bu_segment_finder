#ifndef __HIST_HPP__
#define __HIST_HPP__

#include "hist_axis.hpp"

#include <stdint.h>
#include <string>
#include <string.h> //for memset
#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdio.h>

template<class T>
class hist{
public:
  hist(size_t x_size,T x_min, T x_max,
       size_t y_size,T y_min, T y_max)     : data(NULL), x_axis(x_size,x_min,x_max) , y_axis(y_size,y_min,y_max){
    
    data=new uint8_t*[x_axis.GetBinCount()];
    for (size_t iX = 0; iX < x_axis.GetBinCount();iX++){
      //Now that we have local allocations, catch any allocation exceptions.
      try{
	data[iX] = new uint8_t[y_axis.GetBinCount()];
	memset(data[iX],0,y_axis.GetBinCount());
      }catch (std::bad_alloc & e){
	//Bad allocation, delallocate everything we've allocated and re-throw
	for(size_t iDeAlloc = 0; iDeAlloc < iX;iDeAlloc++){
	  delete [] data[iDeAlloc];
	}
	delete [] data;
	throw;
      }    
    }
  }
  hist(const hist &rhs) : data(NULL), x_axis(1,0,1) , y_axis(1,0,1){ 
    copy(rhs);
  };
  hist & operator=(const hist &rhs){copy(rhs); return *this;}
  ~hist(){  clear();}

  void FillBin(int16_t iX,int16_t iY, uint8_t weight){
    if ( (iX >= 0) && (iX < int16_t(x_axis.GetBinCount())) &&
	 (iY >= 0) && (iY < int16_t(y_axis.GetBinCount()))){
      uint16_t updated_value = data[iX][iY] + weight; //do the addition
      if (updated_value & 0xFF00){ // if there was a rollover above 8 bits, use max value
	data[iX][iY] = 255;
      }else{
	data[iX][iY] += updated_value;
      }
    }    
  }

  void Fill(T x, T y, uint8_t weight){
    int iX = x_axis.FindBin(x);
    int iY = y_axis.FindBin(y);      

    if ((iX != -1) && (iY != -1)){
      uint16_t updated_value = data[iX][iY] + weight; //do the addition
      if (updated_value & 0xFF00){ // if there was a rollover above 8 bits, use max value
	data[iX][iY] = 255;
      }else{
	data[iX][iY] += updated_value;
      }
    }
  }
  void SetBin(int16_t iX,int16_t iY, uint8_t val){
    if ( (iX >= 0) && (iX < int16_t(x_axis.GetBinCount())) &&
	 (iY >= 0) && (iY < int16_t(y_axis.GetBinCount()))){
      data[iX][iY] = val;
    }    
  }
  void Set(T x, T y, uint8_t val){
    int iX = x_axis.FindBin(x);
    int iY = y_axis.FindBin(y);
    if ((iX != -1) && (iY != -1)){
      data[iX][iY] = val;
    }
  }
  hist_axis<T> * GetX(){return &x_axis;}
  hist_axis<T> * GetY(){return &y_axis;}
  void GetMaxBin(size_t &x, size_t &y,uint8_t &max){
    for(size_t iX = 0;iX < x_axis.GetBinCount();iX++){
      for(size_t iY = 0 ;iY < y_axis.GetBinCount();iY++){
	//if ((data[iX][iY] != 255) && ( data[iX][iY] > max)){
	if ( data[iX][iY] > max){
	  max = data[iX][iY];
	  x = iX;
	  y = iY;
	}
      }    
    }
  }
  uint8_t GetBin(size_t x, size_t y){return data[x][y];};
  void write(std::string filename){
    //    std::ofstream outfile(filename.c_str());
    FILE * outfile = fopen(filename.c_str(),"w");
    //    outfile << std::setfill('0') << std::setw(2);
    for(size_t iY = 0 ;iY < y_axis.GetBinCount();iY++){
      for(size_t iX = 0;iX < x_axis.GetBinCount();iX++){
	fprintf(outfile,"%d %d %d\n",iX,iY,data[iX][iY]);
	//	outfile << std::hex << int(data[iX][iY])/10 << " ";
	//int val = int(data[iX][iY]) ;
//	if (val > 0xF){
//	  val = 0xF;
//	}
//	if (data[iX][iY] == 0){
//	  fprintf(outfile,"   ");
//	}else{
//	  fprintf(outfile,"%02X ",data[iX][iY]);
//	}
	//	outfile << std::setw(2) << std::hex << " " << val;
      }
      //      outfile << "\n";
      //      fprintf(outfile,"\n");
    }
    //    outfile.close();
    fclose(outfile);
  }
private:
  void copy(const hist & rhs){
    clear(); //reset this structure
    x_axis = rhs.x_axis;
    y_axis = rhs.y_axis;
  
    data=new uint8_t*[x_axis.GetBinCount()];
    for (size_t iX = 0; iX < x_axis.GetBinCount();iX++){
      //Now that we have local allocations, catch any allocation exceptions.
      try{
	data[iX] = new uint8_t[y_axis.GetBinCount()];
	memcpy(data[iX],rhs.data[iX],y_axis.GetBinCount());
      }catch (std::bad_alloc & e){
	//Bad allocation, delallocate everything we've allocated and re-throw
	for(size_t iDeAlloc = 0; iDeAlloc < iX;iDeAlloc++){
	  delete [] data[iDeAlloc];
	}
	delete [] data;
	throw;
      }    
    }
  }
  hist();
  void clear(){
    if( data != NULL){
      for(size_t iX = 0; iX < x_axis.GetBinCount();iX++){
	if(data[iX] != NULL){
	  delete [] data[iX];
	}
      }
      delete [] data;
    }
  }
  uint8_t ** data;
  hist_axis<T> x_axis;
  hist_axis<T> y_axis;  
};
#endif
