#include "legendre_FPGA.hpp"
#include <math.h>

//Compute things that have been pre-computed on the FPGA
int16_t cosine_table[trig_table_size];
int16_t sine_table[trig_table_size];
void init_trig_tables(){
  for(int16_t i = 0;i < int16_t(trig_table_size);i++){
    // i is in mrad + 1000*Pi/2
    // i = mrad + 1000*PI/2
    cosine_table[i] = int(double(0x1<<TRIG_SCALED_INT) * cos(0.001*double(i) - 0.5*PI));
    sine_table[i]   = double(0x1<<TRIG_SCALED_INT) * sin(0.001*double(i) - 0.5*PI);
    //    printf("%f(%d) %d %d\n",0.001*double(i) - 0.5*PI,i,cosine_table[i],sine_table[i]);
  }    
}

template<class T>
T bitRange(T slv,int msb, int lsb){
  //Drop the bits below the lsb
  slv >>= lsb;
  //Compute the number of bits above zero we are saving (now that we shifted)
  msb = msb - lsb;
  T mask = 0;
  for( int iBit = 0; iBit < msb;iBit++){
    mask |= 0x1<<iBit;
  }
  return slv&mask;
}


//This call runs the algorithm
int Legendre(const std::vector<tube_pos> & tube, hist<int16_t> & h,
	     int16_t & fit_theta_bin,int16_t & fit_r0_bin,stats_t & stats){
  
  //The number of theta bins in the histograms
  uint8_t theta_bin_count = h.GetX()->GetBinCount();
  uint8_t log2_theta_bin_count = log2(theta_bin_count);
  
  //hack, fix later
  //  int16_t theta_bin = h.GetX()->GetBinCenter(0); /*mrad*/ //+ int16_t((1000*PI/2.0)) /*Pi/2 Radians in mradians*/;
  int16_t theta_bin = h.GetX()->GetMin(); /*mrad*/ //+ int16_t((1000*PI/2.0)) /*Pi/2 Radians in mradians*/;
  if (theta_bin > int32_t(trig_table_size)){
    printf("Theta out of range\n");
    exit(0);
  }
  
  int16_t * cosine    = new int16_t[theta_bin_count];
  int16_t * sine      = new int16_t[theta_bin_count];
  int16_t * max_val   = new int16_t[theta_bin_count];
  int16_t * max_val_r = new int16_t[theta_bin_count];
  int16_t * max_theta = new int16_t[theta_bin_count];  

  memset(max_val,   0, theta_bin_count*sizeof(int16_t));
  memset(max_val_r, 0, theta_bin_count*sizeof(int16_t));
  
  //Number of parallel processing units
  uint8_t processors = theta_bin_count;

  hit_processor_t * hit_proc = new hit_processor_t[processors+1]; //+1 for simulating parallelism with wrap around

  int t = 0;

  //=============================================================================
  //== These are in parallel
  //=============================================================================
  //Setup the trig tables.
  t += SetupTrigTables(theta_bin,
		       theta_bin_count,
		       cosine,sine,max_theta);
  //Setup the hits
  int tp = SetupHits(tube,
		     h.GetY()->GetMin()/*0.1mms*/,
		     log2_theta_bin_count,
		     hit_proc,processors);
  if( tp == -1)
    {
      return -1;
    }
  t += tp;
  //=============================================================================

  // Everything is ready to go, start the hit processors
  t += LegendreFill(h,
		    theta_bin_count,
		    cosine,sine,
		    max_val,max_val_r,
		    hit_proc,processors);

  t += LegendreFind(max_val,max_val_r,max_theta,theta_bin_count);

  //===
  fit_theta_bin = max_theta[0];
  fit_r0_bin = max_val_r[0];
  t++;
  //Noww add theta0 and r0 to these  (skipping for now in this code, but we'll have to do it.)
  t++;
  //====

  for(int i=0;i < processors;i++){
    stats.incr_time_total += hit_proc[i].incr_time;
    if(stats.incr_time_hp_max < hit_proc[i].incr_time){
      stats.incr_time_hp_max = hit_proc[i].incr_time;
    }
  }
  
  printf("Total of %d clock ticks\n",t);
  delete [] hit_proc;
  delete [] cosine   ;
  delete [] sine     ;
  delete [] max_theta;
  delete [] max_val  ;
  delete [] max_val_r;


  return t;
}


int LegendreFill(hist<int16_t> & h,
		  uint8_t theta_bin_count,		 
		  int16_t *cosine, int16_t * sine,
		  int16_t * max_val, int16_t * max_val_r,
		  hit_processor_t * hp, uint8_t hit_proc_count /*there is one more in this for wrap around*/){

  int16_t fill_weight = 10;
  int32_t * r = new int32_t[hit_proc_count+1];
  int32_t * rbin = new int32_t[hit_proc_count+1];
  int t = 0;
  bool running = true;
  unsigned __int128 incriment_done = 0x0;
  unsigned __int128 ALL_HITS_UPDATED = 0;
  for(int i=0;i<hit_proc_count;i++){
    ALL_HITS_UPDATED |= 0x1<<i;
  }

  //Fill the tables
  for(;running;t++){
    //parallel processing
    for(uint8_t iP = 0; iP < hit_proc_count;iP++){
      switch (hp[iP].state) {
	case COMPUTE_R_1:
	  //Do the x0 * cosine(theta), get back an 32bit number
	  //the cosine theta term is a scaled integer and
	  //we will keep the scaled result until we add r_add
	  r[iP]  = int32_t(hp[iP].x) * int32_t(cosine[hp[iP].theta_bin]);
	  //if (iP == 0){	   
	    //	    printf("R1: %d = %d*%d\n",r[iP],hp[iP].x,cosine[hp[iP].theta_bin]);	    
	    //}
	  hp[iP].state = COMPUTE_R_2;
//	  if(hp[iP].theta_bin == 127){
	    printf("r1: %d\n",r[iP]);
//	  }

	  break;
	case COMPUTE_R_2:
	  //Do the y0 * sine(theta) + r[iP] , get back an 32bit number
	  //remove the scaled integer
	  r[iP] += (int32_t(hp[iP].y) * int32_t(sine[hp[iP].theta_bin]));
	  r[iP] >>= TRIG_SCALED_INT;
	  //	  r[iP] += (int32_t(hp[iP].y) * int32_t(sine[hp[iP].theta_bin]))>>TRIG_SCALED_INT;
	  //if (iP == 0){
	    //	    printf("R2: %d = (r + (%d * %d)>>%d)\n",r[iP],hp[iP].y,sine[hp[iP].theta_bin],TRIG_SCALED_INT);
	  //}
	  hp[iP].state = COMPUTE_R_3;
	  //	  if(hp[iP].theta_bin == 127){
	    printf("r2: %d\n",r[iP]);
	    // }

	  break;
	case COMPUTE_R_3:
	  //Add r_add which has the properly signed hit time and the -r0
	  r[iP] = (r[iP] + hp[iP].r_add);
	  //	  if(hp[iP].theta_bin == 127){
	    printf("r: %d\n",r[iP]);
	    // }

	  //	  r[iP] >>=TRIG_SCALED_INT;
	  //if (iP == 0){
	    //	    printf("R3: %d = (r + %d)\n",r[iP],hp[iP].r_add);
	    //}	  
	  r[iP] >>=1; //drop the last bit to get the bin number;
	  //if(hp[iP].theta_bin == 127){
	    printf("bin: %d\n",r[iP]);
	    //}
	  hp[iP].state = INCR_R;
	  break;	  
	case INCR_R:
	  if (!(uint16_t(r[iP])&uint16_t(~int16_t(0x7F)))){
//	  printf("%d %03d/%03d  %03d/%03d\n",iP,
//		   hp[iP].theta_bin, int16_t(h.GetX()->GetBinCount()),
//		   r[iP]           , int16_t(h.GetY()->GetBinCount()));
	    h.FillBin(hp[iP].theta_bin,r[iP],fill_weight);
	    printf("fill bin: %d %d\n",hp[iP].theta_bin,r[iP]);
	    
	    //Update the max (this may need another clock tick)
	    if ( h.GetBin(hp[iP].theta_bin,r[iP])  >  max_val[hp[iP].theta_bin]){
	      max_val[hp[iP].theta_bin] = h.GetBin(hp[iP].theta_bin,r[iP]);
	      max_val_r[hp[iP].theta_bin] = r[iP];
	    }


	    
	    if (!(hp[iP].r_old_valid)){
	      hp[iP].state = UPDATE_OLD;
	      incriment_done |= (0x1 << iP);
	    }else if(r[iP] == hp[iP].r_old){
	      hp[iP].state = UPDATE_OLD;
	      incriment_done |= (0x1 << iP);
	    }else if (r[iP] < hp[iP].r_old){
	      //We will incriment in the positive
	      hp[iP].state = INCR_R_P;
	      rbin[iP]= r[iP] + 1;
	    }else{
	      //We will incriment in the negative (or not at all)
	      hp[iP].state = INCR_R_N;
	      rbin[iP]= r[iP] - 1;
	    }	    
	  }else{
	    hp[iP].state = UPDATE_OLD;
	    incriment_done |= (0x1 << iP);
	  }
	  break;
	case INCR_R_P:
	  hp[iP].incr_time++;
	  if ((uint16_t(rbin[iP])&uint16_t(~int16_t(0x7F)))){
	    //We are in an invalid bin
	    //Call it done
	    hp[iP].state = UPDATE_OLD;
	    incriment_done |= (0x1 << iP);
	  }else{
	    if(rbin[iP] != hp[iP].r_old){
	      h.FillBin(hp[iP].theta_bin,rbin[iP],fill_weight);	   
	    }else{
	      hp[iP].state = UPDATE_OLD;
	      incriment_done |= (0x1 << iP);
	    }

	    //Update the max (this may need another clock tick)
	    if ( h.GetBin(hp[iP].theta_bin,rbin[iP]) > max_val[hp[iP].theta_bin]){
	      max_val[hp[iP].theta_bin] = h.GetBin(hp[iP].theta_bin,rbin[iP]);
	      max_val_r[hp[iP].theta_bin] = rbin[iP];
	    }

	    //Update the r bin
	    rbin[iP]++;
	  }
	  break;
	case INCR_R_N:
	  hp[iP].incr_time++;
	  if ((uint16_t(rbin[iP])&uint16_t(~int16_t(0x7F)))){
	    //We are in an invalid bin
	    //Call it done
	    incriment_done |= (0x1 << iP);
	    hp[iP].state = UPDATE_OLD;
	  }else{	  
	    if(rbin[iP] != hp[iP].r_old){
	      h.FillBin(hp[iP].theta_bin,rbin[iP],fill_weight);
	    }else{
	      incriment_done |= (0x1 << iP);
	      hp[iP].state = UPDATE_OLD;
	    }
	    
	    //Update the max (this may need another clock tick)
	    if ( h.GetBin(hp[iP].theta_bin,rbin[iP]) > max_val[hp[iP].theta_bin] ){
	      max_val[hp[iP].theta_bin] = h.GetBin(hp[iP].theta_bin,rbin[iP]);
	      max_val_r[hp[iP].theta_bin] = rbin[iP];
	    }
	  
	    //Update teh r bin
	    rbin[iP]--;
	  }
	  break;
	case UPDATE_OLD:	  
	  hp[iP].r_old = r[iP];
	  hp[iP].r_old_valid = !(uint16_t(r[iP])&uint16_t(~int16_t(0x7F)));  // only valid if it is in the correct range	  
	  //to simplify the wrap around copy hp[0] into hp[hit_proc_count]
	  if (iP == 0){
	    hp[hit_proc_count] = hp[iP];
	  }

	  if( incriment_done == ALL_HITS_UPDATED ){
	    hp[iP].state = MOVE_THETA;
	    incriment_done = 0x0;
	  }
	  
	case MOVE_THETA:
	  //The iP+1 is save because we have one extra
	  hp[iP].theta_bin = (hp[iP].theta_bin + 1)&0x7F;
	  if(hp[iP].theta_bin == 0){
	    hp[iP].r_old_valid = false;
	  }
	  
	  hp[iP].state = COMPUTE_R_1;
	  if ( hp[iP].theta_bin == hp[iP].theta_bin_end ){
	    hp[iP].state = DONE;
	  }

	case DONE:
	  break;
	default:
	  printf("Bad state!\n");
	  exit(0);
	}
    }


    //handle stopping
    running = false;
    for(uint8_t iP = 0; iP < hit_proc_count;iP++){
      if(hp[iP].state != DONE){
	running = true;
      }      
    }
  }
  delete [] r;
  return t;
}



int SetupTrigTables(int16_t theta_start_bin /*mrad*/,
		    uint8_t theta_bin_count,
		    int16_t * cosine, int16_t * sine, int16_t *theta){
  int t = 0;
  printf("theta0: %d\n",theta_start_bin);
  //fill the cos and sine tables 4 entries at a time
  for(int iBin = 0; iBin < (theta_bin_count >> 2);iBin++){
    t++;
    for(int i = 0; i < 4;i++){
//      cosine[(4*iBin)+i] = cosine_table[(4*iBin)+i+theta_start_bin + (trig_table_size>>1)];
//        sine[(4*iBin)+i] =   sine_table[(4*iBin)+i+theta_start_bin + (trig_table_size>>1)];
      cosine[(4*iBin)+i] = cosine_table[(4*iBin)+i + theta_start_bin + int16_t(500*PI)];
        sine[(4*iBin)+i] =   sine_table[(4*iBin)+i + theta_start_bin + int16_t(500*PI)];
	theta[(4*iBin)+i]= (4*iBin)+i;
//      printf("cos(%d);[%d] = %d; sin(%d);[%d] = %d\n",
//	     (4*iBin)+i + theta_start_bin,
//	     (4*iBin)+i + theta_start_bin + int16_t(500*PI),
//	     cosine[(4*iBin)+i],
//	     (4*iBin)+i + theta_start_bin,
//	     (4*iBin)+i + theta_start_bin + int16_t(500*PI),
//	     sine[(4*iBin)+i]);
    }

  }
  for(int i = 0; i < theta_bin_count;i++){
    printf("%d %d %d %d\n",i,theta_start_bin + i,cosine[i],sine[i]);
  }
  return t;
}



//==================================
//=== Helpers for bit-wise magic
int GetHitID(int hp,int parallel_level){
  return hp >> (1+parallel_level);
}
int GetRsign(int hp){
  return hp&0x1;
}
int GetTheta(int hp,int log_theta_bins,int parallel_level){
  //First part, the bitRange gives us the parrallelization index  
  // This needs to be multiplied by the # theta bins for each HP to process
  // luckily, since we will always parallize by factors of two and are always a multiple of 2 theta bins,
  // id * n_bins and be converted to id << (log_theta_bins -parallel)
  // we also or in the lsb in parallel, this is the sign of the +/- r value
  int16_t ret = bitRange(hp,1+parallel_level,1) << (log_theta_bins-parallel_level) | (hp&0x1);
  // We must add to this value, the bits above the 1+parallel bits of ihp, multiplied by two (bitshift)
  ret += bitRange(hp,16,1+parallel_level)<<1;
  return ret;
}

int SetupHits(const std::vector<tube_pos> & tube /*in mms*/,
	      int16_t r0 /*0.1mms*/,
	      int16_t log2_theta_bin_count,
	      hit_processor_t * hit_proc,uint8_t processor_count){
  int t = 0;
  //=============================================================================
  t+=2; //Maybe only one clock tick
  //  int hit_lines = 2*tube.size();
  //  printf("There are %d half-hits to process\n",hit_lines);
  int hit_count = int(tube.size());
  int processor_scaling_bitshift = 0;
  if (processor_count < (hit_count << 1)){
    printf("hit overflows processors\n");
    exit(0);      
  }else if( processor_count >= (hit_count << (1+5))){
    processor_scaling_bitshift = 5;
  }else if( processor_count >= (hit_count << (1+4))){
    processor_scaling_bitshift = 4;
  }else if( processor_count >= (hit_count << (1+3))){
    processor_scaling_bitshift = 3;
  }else if( processor_count >= (hit_count << (1+2))){
    processor_scaling_bitshift = 2;
  }else if( processor_count >= (hit_count << (1+1))){
    processor_scaling_bitshift = 1;
  }else{
    processor_scaling_bitshift = 0;
  }
  printf("Parallelizing over theta %d ways\n",0x1<<processor_scaling_bitshift);

  processor_scaling_bitshift = 1;

  printf("Sorry, scratch that, fixed parallelization at %d\n",1<<processor_scaling_bitshift);
  if (hit_count << (1+processor_scaling_bitshift) > processor_count){
    printf("Too many hits(%d) for hp_count(%d) with fixed parallelization x %d!\n",hit_count,processor_count,1<<processor_scaling_bitshift);
    return -1;
  }

  //reset structures
  for(int iP = 0;iP < processor_count;iP++){
    hit_proc[iP].theta_bin = 0;
    hit_proc[iP].theta_bin_end = 0;

    hit_proc[iP].x = 0;
    hit_proc[iP].y = 0;

    hit_proc[iP].r_add = 0;
    
    hit_proc[iP].r_old = 0;    
    hit_proc[iP].r_old_valid = false;

    hit_proc[iP].state = DONE;
    hit_proc[iP].incr_time = 0;
  }
  
  //=============================================================================
  int used_processors = hit_count << (processor_scaling_bitshift + 1);
  printf("Using %d processors\n",used_processors);
  t++;
  //=============================================================================    
  t++;

  //!!!!!!!!!!!!!!!!!!!! CHANGE THESE ALL TO BIT-SHIFTS WHEN WE GET CODE WORKING!!!!!!
  // set x coordinates (these are separate right now because they multiply
  for(int iP = 0;iP < used_processors;iP++){
    hit_proc[iP].x = int(tube[GetHitID(iP,processor_scaling_bitshift)].x);// * XY_SCALING;
    hit_proc[iP].y = int(tube[GetHitID(iP,processor_scaling_bitshift)].y);// * XY_SCALING;    
    hit_proc[iP].r_add = int(tube[GetHitID(iP,processor_scaling_bitshift)].hit_time);//*XY_SCALING;
    //    hit_proc[iP].r0 = r0;
  }
  //Setup the theta start bin
  for(int iP = 0;iP < (used_processors);iP++){
    hit_proc[iP].theta_bin     = GetTheta(iP,log2_theta_bin_count,processor_scaling_bitshift);
    hit_proc[iP].theta_bin_end = GetTheta(iP+2,log2_theta_bin_count,processor_scaling_bitshift);  //iP+2 is free since it is hardcoded in the parallelization
    hit_proc[iP].state = COMPUTE_R_1;    
  }
 
  //=============================================================================
  t++;
  //fix the wrong,signed r_add values
  for(int iP = 0;iP < used_processors;iP++){    
    if (GetRsign(iP)){ // 1 for negative, 0 for positive
      hit_proc[iP].r_add = -1*(hit_proc[iP].r_add) -r0;      
    }else{
      hit_proc[iP].r_add = (hit_proc[iP].r_add) -r0;      
    }
      
  }

  //debuggin
  for(int iP = 0; iP < used_processors; iP++){
    printf("HP: %d (%d,%d,%d) doing t-bins %d - %d\n",
	   iP,
	   hit_proc[iP].x,hit_proc[iP].y,hit_proc[iP].r_add,
	   hit_proc[iP].theta_bin,hit_proc[iP].theta_bin_end);

  }
  return t;
  
}

int LegendreFind(int16_t * max_val, int16_t * max_val_r, int16_t * max_theta, uint8_t theta_bin_count){
  int t = 0;



  
  for(; theta_bin_count != 1 ;theta_bin_count>>=1){
    t++;
    for(int iComp = 0; iComp < theta_bin_count;iComp+=2){
      //      printf("%03d(%03d,%03d) ? %03d(%03d,%03d)  ; ",max_val[iComp],max_theta[iComp],max_val_r[iComp],max_val[iComp+1],max_theta[iComp+1],max_val_r[iComp+1]);
      if(max_val[iComp+1] > max_val[iComp]){	
	max_val[iComp>>1] = max_val[iComp+1];
	max_val_r[iComp>>1] = max_val_r[iComp+1];
	max_theta[iComp>>1] = max_theta[iComp+1];
      }else{
	max_val[iComp>>1] = max_val[iComp];
	max_val_r[iComp>>1] = max_val_r[iComp];
	max_theta[iComp>>1] = max_theta[iComp];
	
      }
    }
    //    printf("\n");
  }  
  return t;
}
