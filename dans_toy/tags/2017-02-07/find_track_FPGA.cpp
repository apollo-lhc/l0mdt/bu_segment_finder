#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <stdint.h>

#include <math.h>
#include <stdlib.h>     /* abs */

#include "hist.hpp"
#include "canvas.hpp"

#include "mdt.h"
#define PI 3.14159

#include "legendre_FPGA.hpp"
#include "geometry.hpp"

void read_pos_file(std::vector<tube_pos> & pos, const char * const filename){
  if( filename == NULL){
    throw std::string("Bad filename in read_pos_file\n");
  }
  std::ifstream inFile(filename);
  while(!inFile.eof()){
    double x,y,ht;
    inFile >> x;
    inFile >> y;
    inFile >> ht;
    ht = fabs(ht);
    if(!inFile.eof()){
      pos.push_back(tube_pos(x*XY_SCALING,
			     y*XY_SCALING,
			     ht*XY_SCALING));
    }
  }
}

void read_track_file(double & m,double &alpha, double & x0, const char * const filename){
  std::ifstream inFile(filename);
  inFile >> alpha;
  inFile >> x0;
  x0 *= XY_SCALING;
  m = tan(alpha*PI/180.0);

}

void generate_ROI(double theta,double r0,
		  double theta_smear, double r0_smear,
		  double & theta_smeared, double & r0_smeared){
  theta_smeared = theta + theta_smear*(1-2*double(rand())/double(RAND_MAX));
  r0_smeared    = r0 + r0_smear*(1-2*double(rand())/double(RAND_MAX));
  printf("Theta %f smeared to %f\n",theta,theta_smeared);
  printf("r0    %f smeared to %f\n",r0,r0_smeared);
}

int main(int argc, char ** argv){

  //Parsing of real track information from track.dat
  double z0_truth    = 0;     //Intercept of the z axis (where y is zero) for the track
  double m_truth     = 0;      //Slope of the track
  double alpha_truth = 0;  //Angle between track and the chamber Z axis
  double theta_truth = 0;  //Angle in legendre space, angle from Z axis perpendicular to the track
  double r0_truth    = 0;     //Distance at angle theta from the origin to the track
  //Read in the track values
  read_track_file(m_truth,alpha_truth,z0_truth,"track.dat");  
  theta_truth        = alpha_truth - 90;
  r0_truth           = z0_truth*sin((alpha_truth)*PI/180.0);
  //Print the truth
  printf("true track:              y = tan(%f)*(z  %+f)\n",alpha_truth,-1*z0_truth);
  printf("true legendre (r,theta):     (%f,%f)\n",r0_truth,theta_truth);  

  //Generate the ROI like values (smear the truth)
  double theta_smeared = 0;
  double r0_smeared    = 0;  
  generate_ROI(theta_truth,r0_truth, //truth
	       1.0,2.0, //smearing parameter
	       theta_smeared,r0_smeared);

  //DEBUGGING
  theta_smeared = theta_truth;
  r0_smeared = r0_truth;

  
  //Load hits
  std::vector<tube_pos> tube;
  read_pos_file(tube,"layer1.dat");
  std::vector<tube_pos> hit_tube;
  for(size_t iHit = 0; iHit < tube.size();iHit++){
    if(tube[iHit].hit_time > 0.01){
      hit_tube.push_back(tube[iHit]);
    }
  }
  printf("Processing %d tracks\n",int(hit_tube.size()));
  
  //Convert the floating point bounds to integers
  //milliradians
  int16_t theta_center = int16_t(1000.0*PI*(theta_smeared/180.0));
  int16_t theta_range = 64; //3.67 degrees, each bin is 0.057 degrees
  //hundreds of micrometers   
  //  int16_t r0_center = int16_t(XY_SCALING*r0_smeared); // units 0.1mms
  int16_t r0_center = int16_t(r0_smeared); // units 0.1mms
  int16_t r0_range = int16_t(128);  //12.8mm, each bin is 0.2mm
  printf("Theta: %+4d to %+4d centered @ %+4d\n",theta_center-theta_range,theta_center+theta_range,theta_center);
  printf("R0: %+4d to %+4d centered @ %+4d (0.1-mm s)\n",r0_center-r0_range,r0_center+r0_range,r0_center);
  
  //Create legendre histogram
  hist<int16_t> hLegendre(128,theta_center - theta_range , theta_center + theta_range,
			  128,r0_center - r0_range       , r0_center + r0_range);
  hist<int16_t> hLegendre_fit(hLegendre);
  
  
  //Run the legendre algorithm
  init_trig_tables(); //alread in RAM on the FPGA
  int16_t theta_bin_fit = 0x8000;
  int16_t r0_bin_fit    = 0x8000;
  stats_t stats;
  int t = Legendre(hit_tube,hLegendre,
		   theta_bin_fit,r0_bin_fit,stats);

  hLegendre.write("lFPGA.txt");
  
  hLegendre_fit.SetBin(theta_bin_fit,r0_bin_fit,254);
  
  //Process the results
  double theta_fit = double(hLegendre.GetX()->GetBinCenter(theta_bin_fit))*0.001*(180.0/PI);
  double alpha_fit = 90.0 + theta_fit;
  double r0_fit    = double(0.1*hLegendre.GetY()->GetBinCenter(r0_bin_fit));
  double z0_fit    = r0_fit/sin(alpha_fit*(PI/180.0));

  printf("fit track:               y = tan(%f)*(z %+f)\n",alpha_fit,-1*z0_fit);
  printf("fit legendre (r,theta) :     (%f,%f)\n",r0_fit,theta_fit);  


  
  


  //Generate the tube plotting histogram
  hist<double> hTube(6000,-1500,1500,  // bins x, x min,x max
		     3600,-900,900); // bins y ,y min, y max
  hist<double> hTrack_truth(hTube); // Setup the same as hTube
  hist<double> hTrack_fit(hTube);   // Setup the same as hTube
  for (int iTube = 0; iTube < int(tube.size());iTube++){
    draw_hit(tube[iTube],tube[iTube].hit_time,hTube);
  }
  //  double m_truth = tan((PI/180.0)*alpha_truth);
  double m_fit   = tan((PI/180.0)*alpha_fit);    
  draw_track(hTrack_truth, m_truth, z0_truth);
  draw_track(hTrack_fit,   m_fit,   z0_fit);

  //Draw tubes
  canvas<double> tube_canvas = canvas<double>(hTube.GetX(),hTube.GetY());
  tube_canvas.Add(&hTube,0); //Draw in the R channel
  tube_canvas.Add(&hTube,1); //Draw in the G channel
  tube_canvas.Add(&hTube,2); //Draw in the B channel
  tube_canvas.Add(&hTrack_truth,1); //Draw in the G channel
  tube_canvas.Add(&hTrack_fit  ,2); //DRaw in the B channel
  tube_canvas.WriteBitmap("tubes.bmp");


  //Draw the legendre histogram
  canvas<int16_t> legendre_canvas = canvas<int16_t>(hLegendre.GetX(),hLegendre.GetY());
  legendre_canvas.Add(&hLegendre,1); //Draw in the G channel
  legendre_canvas.Add(&hLegendre_fit,0);
  legendre_canvas.WriteBitmap("legendre_FPGA.bmp");



  FILE* results_file = fopen("run_data.dat","a");  
  fprintf(results_file,"%+3.2f %+4.2f %03d %+4d %+3.2f %+4.2f %05d %03d\n",alpha_truth,z0_truth,int(hit_tube.size()),t,alpha_fit,z0_fit,stats.incr_time_total,stats.incr_time_hp_max);
  fclose(results_file);

  
  return 0;
  
}
