#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <stdint.h>

#include <math.h>
#include <stdlib.h>     /* abs */

#include "hist.hpp"
#include "canvas.hpp"

#include "mdt.h"
#define PI 3.14159

void Add_l_segment(double x, double y1, double y2, hist<double> & h, int incr = 0){  
  //Fill in the bins in y between the old and new draw points.
  //This currently is dumb about the X coordinate as all new points are drawn on the new x coord isntead of transitioning between the two acording to the line.

  double yMax = h.GetY()->GetMax();
  double yMin = h.GetY()->GetMin();

  if( (y1 > yMax) && (y2 > yMax)){
    return;
  } else if ( (y1 < yMin) && (y2 < yMin)){
    return;
  }
  
  if( y1 > yMax){
    y1 =  yMax - h.GetY()->GetDelta()*0.5;
  } else if( y1 < yMin){
    y1 =  yMin;
  }

  if( y2 > yMax){
    y2 =  yMax - h.GetY()->GetDelta()*0.5;
  } else if( y2 < yMin){
    y2 =  yMin;
  }

  
  //Fingure out how many inbetween y points we have  
  int bin_y2 = h.GetY()->FindBin(y2);
  int bin_y1 = h.GetY()->FindBin(y1);
  int steps = abs(bin_y2 -  bin_y1) ;  

  
  //Find the starting point (lesser bin number) 
  int bin = (bin_y2>bin_y1) ? bin_y1: bin_y2 ;
  
  
  double delta_x = h.GetX()->GetDelta();
  double delta_y = h.GetY()->GetDelta();

  int mult = 10;

  // do while to draw atleast one point
  do {
    //compute inbetween y points
    double y = h.GetY()->GetBinCenter(bin+steps);
    //move to the next point
    steps--;
    //Simple minded plotting options
    try{
      if (incr == 2){
	//a simple cross pattern of filling
	h.Fill(x        ,y        ,2*mult);
	h.Fill(x        ,y-delta_y,mult);
	h.Fill(x        ,y+delta_y,mult);
	h.Fill(x+delta_x,y        ,mult);
	h.Fill(x-delta_x,y        ,mult);
      }else if (incr == 1){
	//normal bin incriment, but scaled for viewing
	//h.Fill(x,y,incr);
	h.Fill(x,y,mult);
      }else{
	//Fix value to one
	h.Set(x,y,1);
      }
    }catch(std::string & e){
      //Do nothing
    }
  }while(steps >= 0);

}

void gen_legendre(tube_pos tube,
		  hist<double> & h){
  //Add one tubes two legendre space lines into h
  int i_theta = 0;
  double r[2];
  double r_old[2];
  double theta = h.GetX()->GetBinCenter(i_theta);
  r_old[0] = (tube.x*cos(theta*PI/180.0) + tube.y*sin(theta*PI/180.0) + tube.hit_time);
  r_old[1] = (tube.x*cos(theta*PI/180.0) + tube.y*sin(theta*PI/180.0) - tube.hit_time);
  //Only process hits that aren't points
  if (tube.hit_time > 0.01){
    //Loop over all theta values (bins)
    while(i_theta < int(h.GetX()->GetBinCount())){
      //find theta value for theta bin
      theta = h.GetX()->GetBinCenter(i_theta);
      //Compute this theta's two r values
      r[0] = (tube.x*cos(theta*PI/180.0) + tube.y*sin(theta*PI/180.0) + tube.hit_time);
      r[1] = (tube.x*cos(theta*PI/180.0) + tube.y*sin(theta*PI/180.0) - tube.hit_time);
      //draw this part of the function
      Add_l_segment(theta,r[0],r_old[0],h,1);
      Add_l_segment(theta,r[1],r_old[1],h,1);
      i_theta++;
      r_old[0] = r[0];
      r_old[1] = r[1];
    }
  }
}


//-------------------------------------------------------------------------------
void Add_l_segment(int16_t x, int16_t y1, int16_t y2, hist<int16_t> & h, int incr = 0){  
  //Fill in the bins in y between the old and new draw points.
  //This currently is dumb about the X coordinate as all new points are drawn on the new x coord isntead of transitioning between the two acording to the line.

  int16_t yMax = h.GetY()->GetMax();
  int16_t yMin = h.GetY()->GetMin();

  //skip if the segment is completely off screen
  if( (y1 > yMax) && (y2 > yMax)){
    return;
  } else if ( (y1 < yMin) && (y2 < yMin)){
    return;
  }

  //Now that we know atleast one value is on the screen, move the other one on screen
  
  //Move y1 onto the edge if it isn't
  if( y1 > yMax){
    y1 =  yMax - h.GetY()->GetDelta()*0.5;
  } else if( y1 < yMin){
    y1 =  yMin + h.GetY()->GetDelta()*0.5;
  }

  //Move y2 onto the edge if it isn't
  if( y2 > yMax){
    y2 =  yMax - h.GetY()->GetDelta()*0.5;
  } else if( y2 < yMin){
    y2 =  yMin + h.GetY()->GetDelta()*0.5;
  }

  
  //Fingure out how many inbetween y points we have  
  int bin_y2 = h.GetY()->FindBin(y2);
  int bin_y1 = h.GetY()->FindBin(y1);
  int steps = abs(bin_y2 -  bin_y1) ;  

  
  //Find the starting point (lesser bin number) 
  int bin = (bin_y2>bin_y1) ? bin_y1: bin_y2 ;
  
  
  int16_t delta_x = h.GetX()->GetDelta();
  int16_t delta_y = h.GetY()->GetDelta();

  int mult = 10;

  // do while to draw atleast one point
  do {
    //compute inbetween y points
    int16_t y = h.GetY()->GetBinCenter(bin+steps);
    //move to the next point
    steps--;
    //Simple minded plotting options
    try{
      if (incr == 2){
	//a simple cross pattern of filling
	h.Fill(x        ,y        ,2*mult);
	h.Fill(x        ,y-delta_y,mult);
	h.Fill(x        ,y+delta_y,mult);
	h.Fill(x+delta_x,y        ,mult);
	h.Fill(x-delta_x,y        ,mult);
      }else if (incr == 1){
	//normal bin incriment, but scaled for viewing
	h.Fill(x,y,mult);
      }else{
	//Fix value to one
	h.Set(x,y,1);
      }
    }catch(std::string & e){
      //Do nothing
    }
  }while(steps >= 0);

}

#define TRIG_SCALED_INT 11

int32_t icos(int16_t milliradian){
  double c = double(0x1<<TRIG_SCALED_INT)*cos(double(milliradian) * 0.001);
    
  return int32_t(c);
}
int32_t isin(int16_t milliradian){
  double s = double(0x1<<TRIG_SCALED_INT)*sin(double(milliradian) * 0.001);
  return int32_t(s);
}

int16_t imult_cos(int16_t x,int16_t milliradian){
  int32_t val = x*icos(milliradian);
  if (val > (0x1 << (16+TRIG_SCALED_INT))){
    printf("imult_cos too big: %08X\n",val);
  }
  return val >> TRIG_SCALED_INT;
}
int16_t imult_sin(int16_t x,int16_t milliradian){
  int32_t val = x*isin(milliradian);
  if (val > (0x1 << (16+TRIG_SCALED_INT))){
    printf("imult_cos too big: %08X\n",val);
  }
  return val >> TRIG_SCALED_INT;
}


void gen_legendre(tube_pos tube,
		  hist<int16_t> & h){
  int16_t fill_weight = 10;
  
  //Add one tube to legendre space
  int16_t bin_theta = 0;
  int16_t offset_theta_bin = h.GetX()->GetBinCenter(0);

  int16_t tube_x = int16_t(tube.x * 10.0); //convert from mm into 100s of um
  int16_t tube_y = int16_t(tube.y * 10.0); //convert from mm into 100s of um
  int16_t hit_time = int16_t(tube.hit_time * 10.0); //convert "time" from mm to 100s of um
  
  // My toy had a small hit for non-hit channels, so skipping this one.
  if(tube.hit_time <= 0.01){
    hit_time = 0;
  }
  int16_t r_old[2];
  r_old[0] = r_old[1] =  0x8000; //largest magnitude negative number;

  if(hit_time > 0){
    //Do the + hit time curves
    int16_t r_add = hit_time - h.GetY()->GetMin();
    for(bin_theta = 0;bin_theta < int16_t(h.GetX()->GetBinCount());bin_theta++){
      //Compute r
      int16_t theta = offset_theta_bin + bin_theta;
      int16_t r = (imult_cos(tube_x,theta) + imult_sin(tube_y,theta) + r_add);
      //      printf("cos(%d) = %d ; sin(%d) = %d\n",theta,icos(theta),theta,isin(theta));
      //printf("R: %d = %d + %d + %d\n",r,imult_cos(tube_x,theta), imult_sin(tube_y,theta), r_add);
      //Convert to bin number
      int16_t bin_r = r>>1;
      if (r_old[0] == 0x8000){
	h.FillBin(bin_theta,bin_r,fill_weight);
	printf("line_loop: %d\n",1);
      }else{	
	int16_t bin_r_old = r_old[0]>>1;
	
	//swap bins if bin_r is greater than bin_r_old
	// Also add one to each to no fill in the bin next to r_old and to fill in the bin r
	if(bin_r > bin_r_old){
	  uint16_t temp = bin_r_old+1;
	  bin_r_old = bin_r + 1; 
	  bin_r = temp;
	}else if (bin_r == bin_r_old){
	  //if the old and current are equal, we need to increase old by one to allow current to draw
	  bin_r_old++;
	}
	//bin goes from bin_r to bin_r_old in the positive direction
	//	printf("line_loop: %d\n",bin_r_old-bin_r);
	for(uint16_t bin = bin_r;bin <  bin_r_old;bin++){
//	  printf("%03d/%03d  %03d/%03d\n",
//		 bin_theta, int16_t(h.GetX()->GetBinCount()),
//		 bin      , int16_t(h.GetY()->GetBinCount()));

	  h.FillBin(bin_theta,bin,fill_weight);
	}
      }
      r_old[0] = r;
    }

    // Do the - curves
    r_add = -1*hit_time - h.GetY()->GetMin();
    for(bin_theta = 0;bin_theta < int16_t(h.GetX()->GetBinCount());bin_theta++){
      //Compute r
      int16_t theta = offset_theta_bin + bin_theta;
      int16_t r = (imult_cos(tube_x,theta) + imult_sin(tube_y,theta) + r_add);
      //Convert to bin number
      int16_t bin_r = r>>1;
      if (r_old[1] == 0x8000){
	h.FillBin(bin_theta,bin_r,fill_weight);
      }else{	
	int16_t bin_r_old = r_old[1]>>1;
	
	//swap bins if bin_r is greater than bin_r_old
	// Also add one to each to no fill in the bin next to r_old and to fill in the bin r
	if(bin_r > bin_r_old){
	  uint16_t temp = bin_r_old+1;
	  bin_r_old = bin_r + 1; 
	  bin_r = temp;
	}else if (bin_r == bin_r_old){
	  //if the old and current are equal, we need to increase old by one to allow current to draw
	  bin_r_old++;
	}
	//bin goes from bin_r to bin_r_old in the positive direction
	for(uint16_t bin = bin_r;bin <  bin_r_old;bin++){
	  h.FillBin(bin_theta,bin,fill_weight);
	}
      }
      r_old[1] = r;
    }
  }
  
//  
//  //Add one tubes to legendre space lines into h
//  int i_theta = 0;
//  int16_t r[2];
//  int16_t r_old[2];
//  int16_t theta = h.GetX()->GetBinCenter(i_theta);
//
//  int16_t tube_x = int16_t(tube.x * 10.0); //convert from mm into 100s of um
//  int16_t tube_y = int16_t(tube.y * 10.0); //convert from mm into 100s of um
//  int16_t hit_time = int16_t(tube.hit_time * 10.0); //convert "time" from mm to 100s of um
//  if(tube.hit_time <= 0.01){
//    hit_time = 0;
//  }
//
//  //  printf("theta: (%d<%d); r: (%d<%d)\n",h.GetX()->GetMin(),h.GetX()->GetMax(),h.GetY()->GetMin(),h.GetY()->GetMax());
//  //  printf("%d,%d\n",tube_x,tube_y);
//  r_old[0] = imult_cos(tube_x,theta) + imult_sin(tube_y,theta) + hit_time;
//  r_old[1] = imult_cos(tube_x,theta) + imult_sin(tube_y,theta) - hit_time;
//  //Only process hits that aren't points
//  if (hit_time > 0){
//    //Loop over all theta values (bins)
//    while(i_theta < int(h.GetX()->GetBinCount())){
//      //find theta value for theta bin
//      theta = h.GetX()->GetBinCenter(i_theta);
//
//      //Compute this theta's two r values
//      r[0] = (imult_cos(tube_x,theta) + imult_sin(tube_y,theta) + hit_time);
//      r[1] = (imult_cos(tube_x,theta) + imult_sin(tube_y,theta) - hit_time);
//      //      printf("theta/r0: %d,%d\n",theta,r[0]);
//      //draw this part of the function
//      Add_l_segment(theta,r[0],r_old[0],h,1);
//      Add_l_segment(theta,r[1],r_old[1],h,1);
//      i_theta++;
//      r_old[0] = r[0];
//      r_old[1] = r[1];
//    }
//  }
}
void gen_local_legendre(tube_pos tube,
			int16_t x0, int16_t y0,
			hist<int16_t> & h){
  tube.x -= x0;
  tube.y -= y0;
  gen_legendre(tube,h);
}

