#include <vector>
#include <string>

#include <stdint.h>

#include <math.h>
#include <stdlib.h>     /* abs */

#include "hist.hpp"

#include "mdt.h"
#define PI 3.14159




void draw_hit(tube_pos tube,double hit_time,hist<double> & h);
void draw_hit(tube_pos tube,double hit_time,hist<int16_t> & h);
void draw_track(hist<double> & h,double m, double x0);
