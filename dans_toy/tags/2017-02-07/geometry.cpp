#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <stdint.h>

#include <math.h>
#include <stdlib.h>     /* abs */

#include "hist.hpp"
#include "canvas.hpp"

#include "mdt.h"
#define PI 3.14159
void draw_hit(tube_pos tube,double hit_time,hist<double> & h){
  //stolen from https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
  double x = hit_time;
  double x2;
  double r2 = hit_time*hit_time;
  double y = 0;
  double y2;
  //  int err = 0;
  const uint8_t color = 255;
  while(x >= y){
    
    //compute 1/8 of the circle and mirror to the other octrants
    h.Set(tube.x + x, tube.y + y,color);
    h.Set(tube.x + y, tube.y + x,color);
    h.Set(tube.x - y, tube.y + x,color);
    h.Set(tube.x - x, tube.y + y,color);    
    h.Set(tube.x - x, tube.y - y,color);    
    h.Set(tube.x - y, tube.y - x,color);    
    h.Set(tube.x + y, tube.y - x,color);
    h.Set(tube.x + x, tube.y - y,color);
    
    //We are in the first octant, so we'll be increasing in y faster than x
    y+=h.GetY()->GetDelta();
    x2=x*x;
    y2=y*y;
    if(fabs(x2 + y2 - r2) > fabs(x2 -2*x +y2 + h.GetX()->GetDelta() - r2)){
      x-=h.GetX()->GetDelta();
    }
  }
}

void draw_hit(tube_pos tube,double hit_time,hist<int16_t> & h){
  //stolen from https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
  double x = hit_time;
  double x2;
  double r2 = hit_time*hit_time;
  double y = 0;
  double y2;
  //  int err = 0;
  const uint8_t color = 255;
  while(x >= y){
    
    //compute 1/8 of the circle and mirror to the other octrants
    h.Set(tube.x + x, tube.y + y,color);
    h.Set(tube.x + y, tube.y + x,color);
    h.Set(tube.x - y, tube.y + x,color);
    h.Set(tube.x - x, tube.y + y,color);    
    h.Set(tube.x - x, tube.y - y,color);    
    h.Set(tube.x - y, tube.y - x,color);    
    h.Set(tube.x + y, tube.y - x,color);
    h.Set(tube.x + x, tube.y - y,color);
    
    //We are in the first octant, so we'll be increasing in y faster than x
    y+=h.GetY()->GetDelta();
    x2=x*x;
    y2=y*y;
    if(fabs(x2 + y2 - r2) > fabs(x2 -2*x +y2 + h.GetX()->GetDelta() - r2)){
      x-=h.GetX()->GetDelta();
    }
  }
}






void draw_track(hist<double> & h,double m, double x0){
  printf("y = tan(%f) (x %+f)\n",atan(m)*180.0/PI,x0);

  double y_last = m*(h.GetX()->GetBinCenter(0) - x0);
  int iy_last = h.GetY()->FindBin(y_last);

  double x,y;
  for( int iX = 1; iX < int(h.GetX()->GetBinCount()); iX++){
    //Get X and Y
    x = h.GetX()->GetBinCenter(iX);    
    y = m*(x - x0);

    //Draw a simple appoximation between the old y and new y at x
    int iy = h.GetY()->FindBin(y);    
    if((iy != -1) && (iy_last != -1)){ //skip lines that go off screen (probably should do something better)
      int iy_p = iy;
      int iy_n = iy_last;
      if( iy_p < iy_n){
	int iy_temp = iy_p;
	iy_p = iy_n;
	iy_n = iy_temp;
      }
      //loop over the pixes between the two points
      for(;iy_n <= iy_p;iy_n++){	
	h.Set(x,h.GetY()->GetBinCenter(iy_n),255);      
      }
    }
    iy_last = iy;

  }
}
