#ifndef __HIST_AXIS_HPP__
#define __HIST_AXIS_HPP__

#include <iostream>

template <class T>
class hist_axis{
public:

  hist_axis(size_t _size,T _min, T _max){
  if (_size == 0){
    throw std::string("hist_axis::hist_axis: zero size");    
  } else if (_min > _max){
    throw std::string("hist_axis::hist_axis: min > max");    
  }
  size = _size;
  max = _max;
  min = _min;

  scale = (max-min)/T(size);
  shift = min;
}
  
  hist_axis(const hist_axis & rhs){copy(rhs);};
  hist_axis & operator=(const hist_axis & rhs){copy(rhs);return *this;};

  int FindBin(T x){
  if (( x >= max) || (x < min)){
    //    throw std::string("hist_axis::FindBin: Out of range");
    return -1;
  }
  T ret = x;
  ret = (ret - shift)/scale;
  return ret;
}
  size_t GetBinCount(){return size;};
  T GetBinCenter(size_t i){return ((scale*T(i)) + shift);};
  T GetMax(){return max;};
  T GetMin(){return min;};
  T GetDelta(){return scale;};
private:
  void copy(const hist_axis & rhs){
  size  = rhs.size;
  max   = rhs.max;
  min   = rhs.min;
  scale = rhs.scale;
  shift = rhs.shift;
}
  size_t size;
  T  max;
  T  min;
  T  scale; // real = bin * scale + shift
  T  shift;
};
#endif
