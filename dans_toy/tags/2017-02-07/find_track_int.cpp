#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <stdint.h>

#include <math.h>
#include <stdlib.h>     /* abs */

#include "hist.hpp"
#include "canvas.hpp"

#include "mdt.h"
#define PI 3.14159

#include "legendre.hpp"
#include "geometry.hpp"

void read_pos_file(std::vector<tube_pos> & pos, const char * const filename){
  if( filename == NULL){
    throw std::string("Bad filename in read_pos_file\n");
  }
  std::ifstream inFile(filename);
  while(!inFile.eof()){
    double x,y,ht;
    inFile >> x;
    inFile >> y;
    inFile >> ht;
    ht = fabs(ht);
    if(!inFile.eof()){
      pos.push_back(tube_pos(x,y,ht));
    }
  }
}

void read_track_file(double & m,double &alpha, double & x0, const char * const filename){
  std::ifstream inFile(filename);
  inFile >> alpha;
  inFile >> x0;
  m = tan(alpha*PI/180.0);

}



int main(int argc, char ** argv){

  //Parsing of real track information from track.dat
  double true_x0 = 0;
  double true_m = 0;
  double true_alpha = 0;
  double true_theta = 0;
  double true_r0 = 0;
  read_track_file(true_m,true_alpha,true_x0,"track.dat");  

  true_theta = true_alpha - 90;
  //  true_r0 = true_x0*cos((true_theta)*PI/180);
  true_r0 = true_x0*sin((true_alpha)*PI/180);
  printf("true_r0 = %f\n",true_r0);



  //Generate a smeared theta angle for initial guess
  double smeared_theta = true_theta + 0.1*(1-2*double(rand())/double(RAND_MAX));
  printf("theta/smeared theta: %f/%f\n",true_theta,smeared_theta);
  double smeared_r0 = true_r0 + 10*(1-2*double(rand())/double(RAND_MAX));
  printf("r0/smeared r0: %f/%f\n",true_r0,smeared_r0);


  try{


    //Parsing the tube postion files and generating the histograms used to plot the tubes/tracks
    hist<double> hTube(6000,-1500,1500,  // bins x, x min,x max
		       1800,0,900); // bins y ,y min, y max
    hist<double> hTrack(hTube); // Setup the same as hTube
    hist<double> hfpFitTrack(hTube); // Setup the same as hTube
    hist<double> hintFitTrack(hTube); // Setup the same as hTube
    hist<double> hlocal_intFitTrack(hTube); // Setup the same as hTube
    std::vector<tube_pos> tube;
    read_pos_file(tube,"layer1.dat");
    read_pos_file(tube,"layer2.dat");



    //Generate the legendre histograms used for the floating point version of the algorithm
    hist<double> hLegendre(128,smeared_theta - 3.7,smeared_theta +3.7,
			   128,smeared_r0 - 13,smeared_r0 + 13);
    hist<double> hLegendre_fit(hLegendre);


    //Convert the floating point legendre bounds to integers
    //milliradians
    int16_t intTheta_start = int16_t(1000.0*PI*(smeared_theta/180.0));
    int16_t intTheta_range = 64; //3.67 degrees
    //hundreds of micrometers   
    int16_t intR0_start = int16_t(10.0*smeared_r0);
    int16_t intR0_range = int16_t(128);  //12.8mm 
    hist<int16_t> hLegendre_int(128,intTheta_start - intTheta_range,intTheta_start + intTheta_range,
				128,intR0_start - intR0_range,intR0_start + intR0_range);
    hist<int16_t> hLegendre_fit_int(hLegendre_int);



    // Build a new integer histogram for local coordinates
    int32_t x_ave = 0;
    int32_t y_ave = 0;
    for (int iTube = 0;iTube < int(tube.size());iTube++){
      x_ave += tube[iTube].x;
      y_ave += tube[iTube].y;
    }
    int16_t x0_shift = (x_ave/(int(tube.size())));
    int16_t y0_shift = (y_ave/(int(tube.size())));
    printf("local shift by %d,%d\n",x0_shift,y0_shift);
    //Compute new R0 guess for the local coordinates
    int16_t intR0_local = intR0_start - int16_t(10.0*sqrt( //10.0 to convert to 100um
							  pow(double(x0_shift)*cos(smeared_theta*PI/180.0),2) +
							  pow(double(y0_shift)*sin(smeared_theta*PI/180.0),2)
							  ));
    //    int16_t intR0_local = (fabs(intR0_start) - sqrt(pow(double(x0_shift) * sin(90.0+smeared_theta/180.0),2) + pow(double(y0_shift) * cos(90+smeared_theta/180.0),2))) * (intR0_start/fabs(intR0_start));
    //Create the repositioned legendre diagram
    hist<int16_t> hLegendre_local_int(128,intTheta_start - intTheta_range,intTheta_start + intTheta_range,
				128,-1*intR0_range + intR0_local,intR0_range+intR0_local);
    hist<int16_t> hLegendre_fit_local_int(hLegendre_local_int);









    
    //Process all the hits and generate the real world histograms of hits and tracks and the legendre histograms
    //Generate the geometry and the old legendre code
    for (int iTube = 0; iTube < int(tube.size());iTube++){
      draw_hit(tube[iTube],tube[iTube].hit_time,hTube);
      gen_legendre(tube[iTube],hLegendre);
      gen_legendre(tube[iTube],hLegendre_int);
      gen_local_legendre(tube[iTube],x0_shift,y0_shift,hLegendre_local_int);
    }


    //Write out the legendre 2d hist 
    hLegendre.write("leg_fp.txt");
    hLegendre_int.write("leg_int.txt");
    hLegendre_local_int.write("leg_local_int.txt");

    //Process the Legendre transforms
    
    //Blindly take the max bin in the legendre histograms as the as the answer
    size_t  fp_fit_theta_i = 0;
    size_t  fp_fit_r0_i = 0;
    uint8_t fp_val = 0;
    //floating point legendre
    hLegendre.GetMaxBin(fp_fit_theta_i,fp_fit_r0_i,fp_val);  
    //Generate the track segment for this bin
    double fp_fit_theta =  hLegendre.GetX()->GetBinCenter(fp_fit_theta_i);
    double fp_fit_r0    = hLegendre.GetY()->GetBinCenter(fp_fit_r0_i);
    double fp_fit_alpha = 90.0 + fp_fit_theta;
    double fp_fit_m     = tan(fp_fit_alpha * PI/180.0);
    double fp_fit_x0    = fp_fit_r0/cos(fp_fit_theta * PI/180.0);
    printf("Fit (theta/r0): %f/%f\n",fp_fit_theta,fp_fit_r0);

    //integer legendre
    size_t  int_fit_theta_i = 0;
    size_t  int_fit_r0_i = 0;
    uint8_t int_val = 0;    
    hLegendre_int.GetMaxBin(int_fit_theta_i,int_fit_r0_i,int_val);  
    //Generate the track segment for this bin
    double int_fit_theta = (180.0/PI)*(0.001*double(hLegendre_int.GetX()->GetBinCenter(int_fit_theta_i)));
    double int_fit_r0    = 0.1*double(hLegendre_int.GetY()->GetBinCenter(int_fit_r0_i));
    double int_fit_alpha = 90.0 + int_fit_theta;
    double int_fit_m     = tan(int_fit_alpha * PI/180.0);
    double int_fit_x0    = int_fit_r0/cos(int_fit_theta * PI/180.0);
    printf("Fit (theta/r0): %f/%f\n",int_fit_theta,int_fit_r0);

    //local integer legendre
    size_t  local_int_fit_theta_i = 0;
    size_t  local_int_fit_r0_i = 0;
    uint8_t local_int_val = 0;    
    hLegendre_local_int.GetMaxBin(local_int_fit_theta_i,local_int_fit_r0_i,local_int_val);  
    //Generate the track segment for this bin
    double local_int_fit_theta = (180.0/PI)*(0.001*double(hLegendre_local_int.GetX()->GetBinCenter(local_int_fit_theta_i)));
    double local_int_fit_r0    = 0.1*double(hLegendre_local_int.GetY()->GetBinCenter(local_int_fit_r0_i)); //in mm after teh 0.1
    printf("%f\n",local_int_fit_r0);
    //Convert the local r0 into the real r0
    local_int_fit_r0 = sqrt(
			    pow(x0_shift + local_int_fit_r0*cos(-1*local_int_fit_theta*PI/180.0),2) +
			    pow(y0_shift + local_int_fit_r0*sin(-1*local_int_fit_theta*PI/180.0),2)); 

    double local_int_fit_alpha = 90.0 + local_int_fit_theta;
    //    double local_int_fit_m     = tan(local_int_fit_alpha * PI/180.0);
    //    double local_int_fit_x0    = local_int_fit_r0/cos(local_int_fit_theta * PI/180.0);
    printf("Fit (theta/r0): %f/%f\n",local_int_fit_theta,local_int_fit_r0);





    //Draw the real space histograms showing tracks and hits
  
    //Draw the real track
    printf("real: ");
    draw_track(hTrack,true_m,true_x0);
    //Draw the fit track (floating point)
    printf("fit fp : ");
    draw_track(hfpFitTrack,fp_fit_m,fp_fit_x0);
    //Draw the fit track (int)
    printf("fit int : ");
    draw_track(hintFitTrack,int_fit_m,int_fit_x0);


    
    //Draw tubes
    canvas<double> tube_canvas = canvas<double>(hTube.GetX(),hTube.GetY());
    tube_canvas.Add(&hTube,0); //Draw in the R channel
    tube_canvas.Add(&hTube,1); //Draw in the G channel
    tube_canvas.Add(&hTube,2); //Draw in the B channel
    tube_canvas.Add(&hTrack,0); //Draw in the R channel
    tube_canvas.Add(&hfpFitTrack,1); //Draw in the G channel
    tube_canvas.Add(&hintFitTrack,2); //DRaw in the B channel
    tube_canvas.WriteBitmap("tubes.bmp");



    //Draw the floating point legendre histogram
    canvas<double> legendre_canvas = canvas<double>(hLegendre.GetX(),hLegendre.GetY());
    legendre_canvas.Add(&hLegendre,1); //Draw in the G channel
    hLegendre_fit.Set(fp_fit_theta,fp_fit_r0,254);
    legendre_canvas.Add(&hLegendre_fit,0);
    //Write the image to disk
    legendre_canvas.WriteBitmap("legendre.bmp");
  

    //Draw the integer legendre histogram
    canvas<int16_t> legendre_canvas_int = canvas<int16_t>(hLegendre_int.GetX(),hLegendre_int.GetY());
    legendre_canvas_int.Add(&hLegendre_int,1); //Draw in the G channel
    hLegendre_fit_int.Set(hLegendre_int.GetX()->GetBinCenter(int_fit_theta_i),hLegendre_int.GetY()->GetBinCenter(int_fit_r0_i),254);
    // hLegendre_fit_int.Set(int_fit_theta,10*int_fit_r0,254);
    legendre_canvas_int.Add(&hLegendre_fit_int,0); //Draw in red
    //Write the image to disk
    legendre_canvas_int.WriteBitmap("legendre_int.bmp");



    //Draw the local integer legendre histogram    
    canvas<int16_t> legendre_canvas_local_int = canvas<int16_t>(hLegendre_local_int.GetX(),hLegendre_local_int.GetY());
    legendre_canvas_local_int.Add(&hLegendre_local_int,1); //Draw in the G channel
    hLegendre_fit_local_int.Set(hLegendre_local_int.GetX()->GetBinCenter(local_int_fit_theta_i),
				hLegendre_local_int.GetY()->GetBinCenter(local_int_fit_r0_i),254);
    legendre_canvas_local_int.Add(&hLegendre_fit_local_int,0); //Draw in red
    legendre_canvas_local_int.WriteBitmap("legendre_local_int.bmp");


    
  }catch (std::string & e){
    std::cout << e << std::endl;
  }
  
  
  return 0;
}
