#ifndef __CANVAS_HPP__
#define __CANVAS_HPP__

#include "hist.hpp"
#include "bitmap/bitmap_image.hpp"


class canvas_constants{
private:
  static const uint8_t bg_color[3];
  static const uint8_t tick_color[3];
  static const int padding;  
  static const int ticks;
};

template <class T>
class canvas : public canvas_constants {
public:
  canvas(hist_axis<T> * _x, hist_axis<T> * _y): x_axis(*_x),y_axis(*_y),image(NULL)
  {
    int x_size = 2*padding + x_axis.GetBinCount();
    int y_size = 2*padding + y_axis.GetBinCount();

    image = new bitmap_image(x_size,y_size);
    for( int iY = 0; iY < y_size;iY++){
      for( int iX = 0; iX < x_size;iX++){
	image->set_pixel(iX,iY,0,0,0);
      }
    }
  }
  void Add(hist<T> * h,int color){

    //  int x_size = 2*padding + x_axis.GetBinCount();
    int y_size = 2*padding + y_axis.GetBinCount();
  
    for(int iY = 0  ; iY < int(y_axis.GetBinCount()) ; iY++){
      for(int iX = 0; iX < int(x_axis.GetBinCount());iX++){
	uint8_t col[3];
	image->get_pixel(iX + padding,y_size-padding-iY,col[0],col[1],col[2]);
	col[color] = h->GetBin(iX,iY);	
	if (col[color] != 0 ){
	  image->set_pixel(iX + padding,y_size-padding-iY,col[0],col[1],col[2]);
	}
      }       
    }
  }
  ~canvas(){ if ( image != NULL){    delete image;}}

  void WriteBitmap(const std::string & filename = "out.bmp"){
    int x_size = 2*padding + x_axis.GetBinCount();
    int y_size = 2*padding + y_axis.GetBinCount();


    //top/bottom border
    for(int iBorder = 0; iBorder <= padding;iBorder++){
      for( int iX = padding; iX < x_size-padding;iX++){
	//top/bottom      
	if( iX % ticks == 0){
	  image->set_pixel(iX,y_size -1 - iBorder,tick_color[0],tick_color[1],tick_color[2]);
	  image->set_pixel(iX,iBorder,tick_color[0],tick_color[1],tick_color[2]);
	} else {
	  image->set_pixel(iX,y_size -1 - iBorder,bg_color[0],bg_color[1],bg_color[2]);
	  image->set_pixel(iX,iBorder            ,bg_color[0],bg_color[1],bg_color[2]);
	}
      }
    }
  
    for(int iY = 0  ; iY < int(y_axis.GetBinCount()) ; iY++){
    
      //Do ticks on the side
      for(int iX = 0 ; iX < padding;iX++){
	if(iY % ticks == 0){
	  image->set_pixel(iX,y_size-padding-iY,tick_color[0],tick_color[1],tick_color[2]);
	  image->set_pixel(x_size-1-iX,y_size-padding-iY,tick_color[0],tick_color[1],tick_color[2]);
	}else{
	  image->set_pixel(         iX,y_size-padding-iY,bg_color[0],bg_color[1],bg_color[2]);
	  image->set_pixel(x_size-1-iX,y_size-padding-iY,bg_color[0],bg_color[1],bg_color[2]);
	}
      }

      // set zero pixels (no data, to bg color)
      for(int iX = 0; iX < int(x_axis.GetBinCount());iX++){
	uint8_t col[3];
	image->get_pixel(iX + padding,y_size-padding-iY,col[0],col[1],col[2]);
	if ((col[0] == 0) && (col[1] == 0) && (col[2] == 0) ){
	  image->set_pixel(iX+padding,y_size-padding-iY,bg_color[0],bg_color[1],bg_color[2]);
	}
      }
      
    }
    image->save_image(filename.c_str());
  }

private:

  canvas();
  hist_axis<T> x_axis;
  hist_axis<T> y_axis;  
  bitmap_image * image;
};
#endif
