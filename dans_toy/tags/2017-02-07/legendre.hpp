#include <string>

#include "hist.hpp"

#include "mdt.h"

void Add_l_segment(double x, double y1, double y2, hist<double> & h, int incr = 0);
void gen_legendre(tube_pos tube,hist<double> & h);

void Add_l_segment(int16_t x, int16_t y1, int16_t y2, hist<int16_t> & h, int incr = 0);
void gen_legendre(tube_pos tube,hist<int16_t> & h);
void gen_local_legendre(tube_pos tube,int16_t x0, int16_t y0,hist<int16_t> & h);

int16_t icos(int16_t milliradian);
int16_t isin(int16_t milliradian);
