#include <vector>
#include <stdint.h>
#include <stdlib.h>
#include "mdt.h"
#include "hist.hpp"

#define PI 3.14159
#define XY_SCALING 8 //10
#define TRIG_SCALED_INT 11
const size_t trig_table_size = 1<<12;



enum processor_state_t {DONE=0,
			COMPUTE_R_1, COMPUTE_R_2,COMPUTE_R_3,
			INCR_R,
			INCR_R_P,INCR_R_N,
			UPDATE_OLD,
			MOVE_THETA};
struct hit_processor_t {
  int16_t theta_bin;
  int16_t theta_bin_end;
  int16_t x;
  int16_t y;
  int16_t r_add;
  int16_t r_old;
  int16_t r0;
  bool    r_old_valid;
  processor_state_t state;
  int16_t incr_time;
};


struct stats_t{
  int incr_time_total;
  int incr_time_hp_max;
};

int Legendre(const std::vector<tube_pos> & tube, hist<int16_t> & h,
	     int16_t & fit_theta_bin,int16_t & fit_r0_bin,stats_t &stats);

int LegendreFill(hist<int16_t> & h,
		 uint8_t theta_bin_count,		 
		 int16_t *cosine, int16_t * sine,
		 int16_t * max_val, int16_t * max_val_r,
		 hit_processor_t * hp, uint8_t hit_proc_count /*there is one more in this for wrap around*/);

void init_trig_tables();
int SetupTrigTables(int16_t theta_start,
		    uint8_t theta_bin_count,
		    int16_t * cosine, int16_t * sine,int16_t * theta);


int SetupHits(const std::vector<tube_pos> & tube,
	      int16_t r0,
	      int16_t log2_theta_bin_count,
	      hit_processor_t * hit_proc,uint8_t processors);


int LegendreFind(int16_t * max_val, int16_t * max_val_r, int16_t * max_theta,uint8_t theta_bin_count);
