#!/usr/bin/python
import random
import math
import argparse

class tube:
    def __init__(self,x,y):
        self.x0 = x
        self.y0 = y
    def hit(self,m,b):
        x = (self.x0 + m*(self.y0 - b))/(m*m + 1)
        y = m*x + b
        r = math.sqrt((x-self.x0)*(x-self.x0) + (y - self.y0)*(y - self.y0))
        return r


def main():
    parser = argparse.ArgumentParser(description='generate tracks and hits for MDT')

    parser.add_argument('-a','--alpha', help='track alpha', type=float, default=72)
    parser.add_argument('-i','--intercept', help='track intercept', type=float, default=-100)
    parser.add_argument('-n','--noise', help='track intercept', type=float, default=0.01)
    parser.add_argument('-x','--move_x',help='cms to move X origin', type=float,default=0)
    parser.add_argument('-y','--move_y',help='cms to move Y origin', type=float,default=0)

    args = vars(parser.parse_args())
    x_shift = args["move_x"]
    y_shift = args["move_y"]

    noise_level = args["noise"]    
    noise_mult = 15.0/noise_level
    alpha=args["alpha"]
    slope = math.tan( alpha * (3.14159/180.0))
    #x0, is where the track crosses the x axis (y = 0)
    x0 = args["intercept"]
    intercept = -x0*slope
    tube_r = 15

    
    
    trackFile = open("track.dat","w")
    trackFile.write(str(alpha))
    trackFile.write(" ")
    #trackFile.write(str(math.fabs(math.cos(alpha)*x0)))
    trackFile.write(str(x0))
    
    
    tubeFile1 = open("layer1.dat","w")
#    tubeFile2 = open("layer2.dat","w")
    
    tubeFile = tubeFile1
    # hit tubes
    for y in [400,348.03,100,48.03]:
        y=y-200
 #       if y > 200:
 #           tubeFile = tubeFile1
 #       else:
 #           tubeFile = tubeFile2
        for x in range(-135,135,30):
            t = tube(x-x_shift,y-y_shift)
            r = t.hit(slope,intercept)
            if r > tube_r:
                r = random.random()
                if (r < noise_level):
                    tubeFile.write(str(x-x_shift)+" "+str(y-y_shift)+" "+str(r*noise_mult)+"\n")
                else:
                    tubeFile.write(str(x-x_shift)+" "+str(y-y_shift)+" 0\n")
            else:
                tubeFile.write(str(x-x_shift)+" "+str(y-y_shift)+" "+str(r)+"\n")
    
    for y in [374.02,74.02]:
        y=y-200
  #      if y > 200:
  #          tubeFile = tubeFile1
  #      else:
  #          tubeFile = tubeFile2
    
        for x in range(-150,150,30):
            t = tube(x-x_shift,y-y_shift)
            r = t.hit(slope,intercept)
            if r > tube_r:
                r = random.random()
                if (r < noise_level):
                    tubeFile.write(str(x-x_shift)+" "+str(y-y_shift)+" "+str(r*noise_mult)+"\n")
                else:
                    tubeFile.write(str(x-x_shift)+" "+str(y-y_shift)+" 0\n")
            else:
                tubeFile.write(str(x-x_shift)+" "+str(y-y_shift)+" "+str(r)+"\n")
    
    
    #ROIs
    
    tubeFile2 = open("layer2.dat","w");
    
    rpc_2_y = 825
    rpc_1_y = 425
    
    rpc_delta = 1.0
    
    rpc_2_x = rpc_2_y/slope + x0
    rpc_1_x = rpc_1_y/slope + x0


if __name__ == '__main__':
    main()


