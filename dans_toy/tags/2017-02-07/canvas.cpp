#include "canvas.hpp"
#include "bitmap/bitmap_image.hpp"
#include "hist_axis.hpp"

const uint8_t canvas_constants::bg_color[3] = {0,0,0} ;//{255,255,255};//
const uint8_t canvas_constants::tick_color[3] = {255,255,255};//{0,0,0} ;
const int     canvas_constants::padding = 5;  
const int     canvas_constants::ticks = 10;
