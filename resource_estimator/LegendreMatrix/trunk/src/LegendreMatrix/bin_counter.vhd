-------------------------------------------------------------------------------
-- Legendre simple counter
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;


entity bin_counter is

  generic (
    DATA_WIDTH  : integer          := 4
    );
  port (
    clk         : in  std_logic;
    reset_async : in  std_logic;
    reset_sync  : in  std_logic;
    increment   : in  std_logic;
    count       : out unsigned(DATA_WIDTH-1 downto 0)
    );

end entity bin_counter;

architecture behavioral of bin_counter is
  constant ZERO_VALUE : unsigned(DATA_WIDTH-1 downto 0) := (others => '0');
  signal local_count  : unsigned(DATA_WIDTH-1 downto 0) := ZERO_VALUE;

begin  -- architecture behavioral

  count <= local_count;
  
  event_counter : process (clk, reset_async)
  begin  -- process malformed_counter
    if reset_async = '1' then           -- asynchronous reset (active high)
      local_count <= ZERO_VALUE;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if reset_sync = '1' then
        -- synchronous reset
        local_count <= ZERO_VALUE;
      else
        -- count
        if increment  = '1' then
          if and_reduce(std_logic_vector(local_count)) = '0' then
            local_count <= local_count + 1;
          end if;
        end if;
      end if;
    end if;
  end process event_counter;

end architecture behavioral;

