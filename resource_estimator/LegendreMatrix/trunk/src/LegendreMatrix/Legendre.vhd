-------------------------------------------------------------------------------
-- This module contains the theta slices for the legendre transform and the
-- module for the pipelining of the computation of the legendre transform
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.hit_processor.all;
use ieee.numeric_std.all;
use work.hit_processor.all;
use ieee.std_logic_misc.all;

entity Legendre is
  
  generic (
    R_BIN_COUNT_WIDTH      : integer := 7;
    COUNT_BIT_WIDTH        : integer := 4;
    THETA_BIN_COUNT_WIDTH  : integer := 7);
  port (
    clk         : in std_logic;
    reset_async : in std_logic;
    fake_input  : in std_logic;
    r : out std_logic;
    theta : out std_logic);
--    r           : out std_logic_vector(2**THETA_BIN_COUNT_WIDTH - 1 downto 0);
--    theta       : out std_logic_vector(2**THETA_BIN_COUNT_WIDTH - 1 downto 0));
--    r           : out unsigned(R_BIN_COUNT_WIDTH - 1 downto 0);
--    theta       : out unsigned(THETA_BIN_COUNT_WIDTH - 1 downto 0));
  
end entity Legendre;

architecture behavioral of Legendre is

  -------------------------------------------------------------------------------
  -- theta slice objects
  -------------------------------------------------------------------------------
  -------------------------------------------------------------------------------
  -- constants
  constant THETA_BIN_COUNT : integer := 2**THETA_BIN_COUNT_WIDTH;
  constant R_BIN_COUNT2     : integer := 2**R_BIN_COUNT_WIDTH;
  component legendre_theta_slice is
    generic (
      R_BIN_COUNT_WIDTH : integer;
      COUNT_BIT_WIDTH   : integer;
      PIPELINE_DELAY    : integer);
    port (
      clk           : in  std_logic;
      reset_async   : in  std_logic;
      start_pulse   : in  std_logic;
      done          : out std_logic;
      hit_processor : in  hit_processor_t := DEFAULT_hit_processor_t;
      max_value     : out unsigned(COUNT_BIT_WIDTH   - 1 downto 0);
      max_bin       : out unsigned(R_BIN_COUNT_WIDTH - 1 downto 0));
  end component legendre_theta_slice;
  signal job_start : std_logic_vector(THETA_BIN_COUNT -1 downto 0) := (others => '0');
  signal job_done  : std_logic_vector(THETA_BIN_COUNT -1 downto 0) := (others => '0');
  signal hit_processor : hit_processor_array_t(THETA_BIN_COUNT -1 downto 0) := (others => DEFAULT_hit_processor_t);
  type unsigned_array_value_t is array (THETA_BIN_COUNT-1 downto 0) of unsigned(COUNT_BIT_WIDTH - 1 downto 0);
  type unsigned_array_bin_t   is array (THETA_BIN_COUNT-1 downto 0) of unsigned(R_BIN_COUNT_WIDTH - 1 downto 0);
  signal max_value : unsigned_array_value_t := (others => (others => '0'));
  signal max_bin   : unsigned_array_bin_t := (others => (others => '0'));


  -------------------------
  -- other

  signal fake_data_state : integer range -4 to 128 := -4;
  signal fake_r           : std_logic_vector(2**THETA_BIN_COUNT_WIDTH - 1 downto 0);
  signal fake_theta       : std_logic_vector(2**THETA_BIN_COUNT_WIDTH - 1 downto 0);

  signal fake_data_sr : std_logic_vector(R_BIN_COUNT_WIDTH * THETA_BIN_COUNT - 1 downto 0) := (others => '0');
  
begin  -- architecture behavioral

  theta_slice_generator: for iTheta in THETA_BIN_COUNT-1 downto 0 generate
    legendre_theta_slice_1: entity work.legendre_theta_slice
      generic map (
        R_BIN_COUNT_WIDTH => R_BIN_COUNT_WIDTH,
        COUNT_BIT_WIDTH   => COUNT_BIT_WIDTH,
        PIPELINE_DELAY    => 3)
      port map (
        clk           => clk,
        reset_async   => reset_async,
        start_pulse   => job_start(iTheta),
        done          => job_done(iTheta),
        hit_processor => hit_processor(iTheta),
        max_value     => max_value(iTheta),
        max_bin       => max_bin(iTheta));
  end generate theta_slice_generator;
  

  hit_processor_control: process (clk, reset_async) is
  begin  -- process hit_processor_control
    if reset_async = '1' then           -- asynchronous reset (active high)
      job_start  <= (others => '0');
      hit_processor <= (others => DEFAULT_hit_processor_t);
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- reset job_start pulses
      job_start <= (others => '0');

      fake_data_state <= fake_data_state + 1;
      if fake_data_state = 127 then
        fake_data_state <= -4;
      end if;
      
      case fake_data_state is
        when -4 =>
          hit_processor <= (others => DEFAULT_hit_processor_t);
          for iTheta in THETA_BIN_COUNT-1 downto 0 loop
            hit_processor(iTheta).on_last_bin <= '0';
            job_start(iTheta) <= '1';
          end loop;  -- iTheta
          hit_processor(0).on_last_bin <= '0';
        when -1 downto -3 =>
          --do nothing
        when 126 downto 0 =>
          for iTheta in THETA_BIN_COUNT-1 downto 0 loop
            hit_processor(iTheta).bin_in_range <= '1';
            hit_processor(iTheta).on_last_bin  <= '0';
            hit_processor(iTheta).rbin <= unsigned(fake_data_sr(R_BIN_COUNT_WIDTH * (iTheta+1)-1 downto R_BIN_COUNT_WIDTH * iTheta));
          end loop;
        when 127 =>
          for iTheta in THETA_BIN_COUNT-1 downto 0 loop
            hit_processor(iTheta).bin_in_range <= '1';
            hit_processor(iTheta).on_last_bin  <= '1';
            hit_processor(iTheta).rbin <= unsigned(fake_data_sr(R_BIN_COUNT_WIDTH * (iTheta+1)-1 downto R_BIN_COUNT_WIDTH * iTheta)); 
          end loop;
        when 128 =>
          for iTheta in THETA_BIN_COUNT-1 downto 0 loop
            fake_r(iTheta)     <=  or_reduce(std_logic_vector(max_bin  (iTheta)));
            fake_theta(iTheta) <= and_reduce(std_logic_vector(max_value(iTheta)));
          end loop;
        when others => null;
      end case;
      r <= or_reduce(fake_r);
      theta <= and_reduce(fake_theta);
      fake_data_sr(R_BIN_COUNT_WIDTH*THETA_BIN_COUNT -1 downto 0) <= fake_data_sr(R_BIN_COUNT_WIDTH*THETA_BIN_COUNT -2 downto 0) & fake_input;
    end if;
  end process hit_processor_control;
  
  
end architecture behavioral;
