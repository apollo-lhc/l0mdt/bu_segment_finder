----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
--
-- package for interfacing with the theta_slice module
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;


package hit_processor is

  type hit_processor_t is record
    bin_in_range : std_logic;
    rbin         : unsigned(6 downto 0);
    on_last_bin  : std_logic;
  end record hit_processor_t;
  constant DEFAULT_hit_processor_t : hit_processor_t := (bin_in_range => '0',
                                                         rbin => (others => '0'),
                                                         on_last_bin => '1');
  type hit_processor_array_t is array (integer range <>) of hit_processor_t;
  
end package hit_processor;
