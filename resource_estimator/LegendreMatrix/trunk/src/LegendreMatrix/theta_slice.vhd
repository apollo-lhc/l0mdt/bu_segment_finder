-------------------------------------------------------------------------------
-- This is a container for the rbin counters for a specific theta value.
-- It receives a pipeline of r values and incrments the appropriate bin,
-- keeping track of the maximum bin.
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.hit_processor.all;
use ieee.numeric_std.all;

entity legendre_theta_slice is
  
  generic (
    R_BIN_COUNT_WIDTH : integer := 7;
    COUNT_BIT_WIDTH : integer := 4;
    PIPELINE_DELAY : integer := 5
    );

  port (
    clk            : in std_logic;
    reset_async    : in std_logic;
    start_pulse    : in std_logic;
    done           : out std_logic;
    hit_processor  : in  hit_processor_t := DEFAULT_hit_processor_t;
    max_value      : out unsigned(COUNT_BIT_WIDTH - 1   downto 0);
    max_bin        : out unsigned(R_BIN_COUNT_WIDTH - 1 downto 0));

end entity legendre_theta_slice;

architecture behavioral of legendre_theta_slice is
  --Counter for each bin of this theta slice 
  component bin_counter is
    generic (
      DATA_WIDTH : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      increment   : in  std_logic;
      count       : out unsigned(DATA_WIDTH-1 downto 0));
  end component bin_counter;

  -------------------------------------------------------------------------------
  -- bin control signals
  -------------------------------------------------------------------------------
  constant R_BIN_COUNT : integer := 2**R_BIN_COUNT_WIDTH;
  --Accessor to the bin contents
  type count_array_t is array (R_BIN_COUNT-1 downto 0) of unsigned(COUNT_BIT_WIDTH-1 downto 0);
  signal bin_content : count_array_t := (others => (others =>'0'));

  --mask for incrimenting a bin
  signal r_increment : std_logic_vector(R_BIN_COUNT -1 downto 0) := (others => '0');

  -- reset counters to zero
  signal reset_theta_slice : std_logic := '0';

  constant MAX_COUNT_VALUE : unsigned(COUNT_BIT_WIDTH -1 downto 0) := (others => '1');
  -------------------------------------------------------------------------------
  -- bin control signals
  -------------------------------------------------------------------------------
  type process_compute_state_t is (PROCESS_COMPUTE_PIPELINE_WAIT, -- wait for
                                                                  -- pipeline
                                                                  -- data to be
                                                                  -- valid
                                   PROCESS_COMPUTE_PIPELINE_PROCESS,--process
                                                                    --pipeline data
                                   PROCESS_COMPUTE_DONE);         -- finished
                                                                  -- computing
                                                                  -- IDLE
  signal process_compute_state : process_compute_state_t := PROCESS_COMPUTE_DONE;
  signal pipeline_wait_countdown : integer := 0;

  -------------------------------------------------------------------------------
  -- Count processing signals
  -------------------------------------------------------------------------------
  signal max_value_buffer : unsigned(COUNT_BIT_WIDTH   - 1 downto 0) := (others => '0');
  signal max_value_local  : unsigned(COUNT_BIT_WIDTH   - 1 downto 0) := (others => '0');
  signal max_bin_buffer   : unsigned(R_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');
  signal max_bin_local    : unsigned(R_BIN_COUNT_WIDTH - 1 downto 0) := (others => '0');

  -------------------------------------------------------------------------------
  -- statistics signals
  -------------------------------------------------------------------------------
  signal ERROR_PULSE_at_max_value : std_logic := '0';
  
begin  -- architecture behavioral

  --Array of counters to act like bins
  r_bin_counters: for iBin in R_BIN_COUNT -1 downto 0 generate
    bin_counter_1: entity work.bin_counter
      generic map (
        DATA_WIDTH => COUNT_BIT_WIDTH)
      port map (
        clk         => clk,
        reset_async => reset_async,
        reset_sync  => reset_theta_slice,
        increment   => r_increment(iBin),
        count       => bin_content(iBin));
  end generate r_bin_counters;



  --Output the max value and max_bin when in the done state
  max_value <= max_value_buffer;
  max_bin   <= max_bin_buffer;
  
  fill_process: process (clk, reset_async) is
  begin  -- process fill_process
    if reset_async = '1' then                 -- asynchronous reset (active high)
      process_compute_state <= PROCESS_COMPUTE_DONE;
    elsif clk'event and clk = '1' then  -- rising clock edge
      --Reset pulses from this process
      reset_theta_slice <= '0';
      ERROR_PULSE_at_max_value <= '0';

      --reset the count increment mask
      r_increment <= (others => '0');

      -- reset the outputted state to not done unless we are in the done state
      done <= '0';

      -- State machine 
      if start_pulse = '1' then        
        --Reset our bins
        reset_theta_slice <= '1';

        --Reset the max values
        max_value_buffer <= (others => '0');
        max_bin_buffer <= (others => '0');

        --Move to the pipeline wait state
        pipeline_wait_countdown <= PIPELINE_DELAY; --start the pipeline countdown
        process_compute_state <= PROCESS_COMPUTE_PIPELINE_WAIT;
      else
        case process_compute_state is
          ------------------------------------------------------------
          when PROCESS_COMPUTE_PIPELINE_WAIT =>
            --Wait for the pipelined computations to read the pipeline output
            if pipeline_wait_countdown = 0 then
              process_compute_state <= PROCESS_COMPUTE_PIPELINE_PROCESS;
            else
              pipeline_wait_countdown <= pipeline_wait_countdown - 1;
            end if;
          ------------------------------------------------------------
          when PROCESS_COMPUTE_PIPELINE_PROCESS =>
            -- Check if this bin is in range and process it if it is
            if hit_processor.bin_in_range = '1' then
              --Increment the bin (counter handles overflow)
              r_increment(to_integer(hit_processor.rbin)) <= '1';

              --If the previous max value was the incremented bin's last value,
              --then we know this bin is now the (new) maximum
              if bin_content(to_integer(hit_processor.rbin)) = max_value_local then
                -- save this bin as the maximum r bin
                max_bin_local <= hit_processor.rbin;

                --Save the new maximum value (rbin's + 1), but we should check
                --if we overflow.
                if max_value_local = MAX_COUNT_VALUE then
                  --send an error pulse to count the number of times we reach the
                  --max bin count, we NEVER should.                  
                  ERROR_PULSE_at_max_value <= '1';
                else
                  --only incrment the max value if we can
                  max_value_local <= max_value_local + 1;                  
                end if;                
              end if;
            end if;

            --CHeck if this is the last bin we are suppose to update
            if hit_processor.on_last_bin = '1' then
              --move to the done state
              process_compute_state <= PROCESS_COMPUTE_DONE;
              max_value_buffer <= max_value_local;
              max_bin_buffer <= max_bin_local;
              done <= '1';
            end if;

            
          ------------------------------------------------------------
          when PROCESS_COMPUTE_DONE =>
            done <= '1';
          when others => process_compute_state <= PROCESS_COMPUTE_DONE;
        end case;
      end if;
    end if;
  end process fill_process;

end architecture behavioral;
