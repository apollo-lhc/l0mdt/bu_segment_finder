
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

                         
entity compute_pipeline is
  
  generic (
    COORDINATE_WIDTH       : integer := 12;
    TRIG_WIDTH             : integer := 12;
    BINNING_BITSHIFT_COUNT : integer := 1;
    BIN_WIDTH              : integer := 7);

  port (
    clk              : in std_logic;
    data_in_valid    : in std_logic;
    tube_x           : in  signed(COORDINATE_WIDTH-1 downto 0);
    cos              : in  signed(TRIG_WIDTH-1 downto 0);
    tube_y           : in  signed(COORDINATE_WIDTH-1 downto 0);
    sin              : in  signed(TRIG_WIDTH-1 downto 0);
    R                : in  signed(COORDINATE_WIDTH- 1 downto 0);
    R_pm             : in  std_logic;
    r0               : in  signed(COORDINATE_WIDTH- 1 downto 0);
    data_out_valid   : out std_logic;
    bin              : out unsigned(BIN_WIDTH-1 downto 0);
    bin_out_of_range : out std_logic
    );

end entity compute_pipeline;

architecture arch of compute_pipeline is

  type coordinate_delay_t is array (integer range <>) of signed(COORDINATE_WIDTH -1 downto 0);
  type trig_delay_t is array (integer range <>) of signed(TRIG_WIDTH -1 downto 0);
  type mult_delay_t is array (integer range <>) of signed(COORDINATE_WIDTH+TRIG_WIDTH -1 downto 0);

  
  signal tube_x_delay : coordinate_delay_t(1 downto 0) := (others => (others =>'0'));
  signal tube_y_delay : coordinate_delay_t(2 downto 0) := (others => (others =>'0'));
  signal cosine_delay : trig_delay_t      (1 downto 0) := (others => (others =>'0'));
  signal sine_delay   : trig_delay_t      (2 downto 0) := (others => (others =>'0'));
  signal R_r0_delay      : mult_delay_t      (2 downto 0) := (others => (others =>'0'));
  signal data_valid_delay : std_logic_vector(5 downto 0) := (others => '0');
  
    
  signal mult_x         : signed(COORDINATE_WIDTH+TRIG_WIDTH-1 downto 0);
  signal mult_x_sub     : signed(COORDINATE_WIDTH+TRIG_WIDTH-1 downto 0);
  signal mult_y         : signed(COORDINATE_WIDTH+TRIG_WIDTH-1 downto 0);
  signal mult_y_add     : signed(COORDINATE_WIDTH+TRIG_WIDTH-1 downto 0);
  
  signal final_r : std_logic_vector(COORDINATE_WIDTH+TRIG_WIDTH-1 downto 0);


  attribute use_dsp48          : string;
  attribute use_dsp48 of mult_y_add : signal is "yes";
  attribute use_dsp48 of mult_x_sub : signal is "yes";
  
begin  -- architecture arch

  computation: process (clk) is
  begin  -- process mult
    if clk'event and clk = '1' then  -- rising clock edge

      -- pipeline delays
      tube_x_delay(1)          <= tube_x_delay(0);
      tube_x_delay(0)          <= tube_x;           
      tube_y_delay(2 downto 1) <= tube_y_delay(1 downto 0);
      tube_y_delay(0)          <= tube_y;
      cosine_delay(1)          <= cosine_delay(0);
      cosine_delay(0)          <= cos;
      sine_delay(2 downto 1)   <= sine_delay(1 downto 0);
      sine_delay(0)            <= sin;
      -- The R_r0 delay is a little different since we have to bitshift the
      -- incomming value to account for it being added on before we divide by
      -- the trig scaling (bitshift)
      R_r0_delay(2 downto 1) <= R_r0_delay(1 downto 0);           
      R_r0_delay(0)(TRIG_WIDTH-1 downto 0) <= (others => '0');     
      if R_pm = '0' then
        -- Handle the xcos + ysin +R - r0 case
        -- We will do xcos + ysin - (r0 - R)
        R_r0_delay(0)(COORDINATE_WIDTH+TRIG_WIDTH -1 downto TRIG_WIDTH) <= r0 - R;
      else
        -- Handle the xcos + ysin -R - r0 case
        -- We will do xcos + ysin - (r0 + R)
        R_r0_delay(0)(COORDINATE_WIDTH+TRIG_WIDTH -1 downto TRIG_WIDTH) <= r0 + R;
      end if;

      data_valid_delay <= data_valid_delay(4 downto 0) & data_in_valid;
      
      --Pipelines are used to max out the speed of the DSPs
      
      -- pipeline layer 1
      --   just pipeline
      -- pipeline layer 2
      --   just pipeline
      -- pipeline layer 3
      mult_x <= (tube_x_delay(1) * cosine_delay(1));
      -- pipeline layer 4
      mult_x_sub <= mult_x - R_r0_delay(2);
      mult_y     <= tube_y_delay(2) * sine_delay(2);
      -- pipeline layer 5
      mult_y_add <= mult_x_sub + mult_y;
      --pipeline layer 6
      final_r <= std_logic_vector(mult_y_add);

      --Turn this into a bin number in "r".
      --This is done by bitshifting away the TRIG_WIDTH that scaled the trig
      --functions and bitshifting by a bin width scaling factor (power of 2, BINNING_BITSHIFT_COUNT)
      bin <= unsigned(final_r(BIN_WIDTH + TRIG_WIDTH + BINNING_BITSHIFT_COUNT -1  downto TRIG_WIDTH + BINNING_BITSHIFT_COUNT));
      --The bin is our of range if it is positive and any bit above the highest bit
      --used for "bin" is a 1, so or_reduce detects this
      --This is also out of range if it is negative, in which case atleast the MSB
      --must be a 1, so an or_reduce will detect this
      bin_out_of_range <= or_reduce(final_r(COORDINATE_WIDTH+TRIG_WIDTH - 1 downto BIN_WIDTH + TRIG_WIDTH + BINNING_BITSHIFT_COUNT));
      data_out_valid <= data_valid_delay(5);

      
    end if;
  end process computation;

  
end architecture arch;
