---
-- Multiplier test design
-- multiply two 8-bit operands from switches and display on 16 LEDs
---

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;


--The IEEE.std_logic_unsigned contains definitions that allow 
--std_logic_vector types to be used with the + operator to instantiate a 
--counter.
use IEEE.std_logic_unsigned.all;

entity top is
  port (
    btnCpuReset : in  std_logic;
    btn_c       : in  std_logic;
    SW          : in  std_logic_vector(15 downto 0);
    tube_x0     : in signed(11 downto 0);
    tube_y0     : in signed(11 downto 0);
    data_valid  : in std_logic;
    tube_R      : in signed(11 downto 0);
    tube_R_pm   : in std_logic;
    r0          : in signed(11 downto 0);
    cosine      : in signed(11 downto 0);
    sine        : in signed(11 downto 0);    
    CLK         : in  std_logic;
    LED         : out std_logic_vector (15 downto 0)
    );    
end top;

architecture Behavioral of top is

  constant MWID : integer := 13;        -- this is the width of the DSP inputs

  component debounce is
    generic (
      counter_size : integer);
    port (
      clk    : in  std_logic;
      button : in  std_logic;
      result : out std_logic);
  end component debounce;

  component compute_pipeline is
    generic (
      COORDINATE_WIDTH       : integer;
      TRIG_WIDTH             : integer;
      BINNING_BITSHIFT_COUNT : integer;
      BIN_WIDTH              : integer);
    port (
      clk              : in  std_logic;
      data_in_valid    : in  std_logic;          
      tube_x           : in  signed(COORDINATE_WIDTH-1 downto 0);
      cos              : in  signed(TRIG_WIDTH-1 downto 0);
      tube_y           : in  signed(COORDINATE_WIDTH-1 downto 0);
      sin              : in  signed(TRIG_WIDTH-1 downto 0);
      R                : in  signed(COORDINATE_WIDTH- 1 downto 0);
      R_pm             : in  std_logic;
      r0               : in  signed(COORDINATE_WIDTH- 1 downto 0);
      data_out_valid   : out std_logic;
      bin              : out unsigned(BIN_WIDTH-1 downto 0);
      bin_out_of_range : out std_logic);
  end component compute_pipeline;
  
  signal rst   : std_logic;
  signal rst_n : std_logic;
  
  
  signal res                   : unsigned(MWID*2-1 downto 0);


--  attribute use_dsp48          : string;
--  attribute use_dsp48 of LED : signal is "yes";
  
begin

  rst_n <= not rst;

  debounce_1 : entity work.debounce
    generic map (
      counter_size => 17)
    port map (
      clk    => CLK,
      button => btnCpuReset,
      result => rst);


  
  
  hp_compute_pipeline_1: entity work.compute_pipeline
    generic map (
      COORDINATE_WIDTH => 12,
      TRIG_WIDTH       => 12,
      BINNING_BITSHIFT_COUNT => 1,
      BIN_WIDTH       => 7)
    port map (
      clk              => clk,
      data_in_valid    => data_valid,
      tube_x           => tube_x0,
      cos              => cosine,
      tube_y           => tube_y0,
      sin              => sine,
      R                => tube_R,
      R_pm             => tube_R_pm,
      r0               => r0,
      data_out_valid   => res(8),
      bin              => res(6 downto 0),
      bin_out_of_range => res(7));

  LED(8 downto 0) <= std_logic_vector(res(8 downto 0));

end Behavioral;
