-------------------------------------------------------------------------------
-- generic synchronizer per Mr Wu
-- s is rising-edge sensitive input
-- y is one-clock wide synch'd output
--   output occurs 2-3 clocks after input edge
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity synch is
  port (
    clk, rst, s : in  std_logic;
    y           : out std_logic);
end synch;


architecture synch_a of synch is

  signal st, s1, s2, s3, s4 : std_logic;

begin  -- synch_a

  -- toggle st on rising edge of s
  process (s, rst)
  begin
    if rst = '0' then
      st <= '0';
    elsif rising_edge(s) then
      st <= not st;
    end if;
  end process;
  
  -- synchronize st through s1, st, s3
  -- form output as (s2 xor s3) and resynchronize
  process (clk, rst)
  begin  -- process
    if rising_edge(clk) then
      s1 <= st;
      s2 <= s1;
      s3 <= s2;
      s4 <= s2 xor s3;
    end if;
  end process;

  y <= s4;

end synch_a;

