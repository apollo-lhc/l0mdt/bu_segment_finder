#

create_project -force -part xc7a100tcsg324-1 top ./proj

read_xdc src/top.xdc
read_vhdl src/top.vhd
read_vhdl src/debounce.vhd
read_vhdl src/synch.vhd
#read_vhdl src/mult.vhd
read_vhdl src/hp_compute_pipeline.vhd
# read_vhdl ip/fifo_8x1024/synth/fifo_8x1024.vhd
# read_ip ip/fifo_8x1024/fifo_8x1024.xci
